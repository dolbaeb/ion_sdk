#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ErrorMessage.WBP_ErrorMessage_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature
struct UWBP_ErrorMessage_C_BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_ErrorMessage.WBP_ErrorMessage_C.Construct
struct UWBP_ErrorMessage_C_Construct_Params
{
};

// Function WBP_ErrorMessage.WBP_ErrorMessage_C.ExecuteUbergraph_WBP_ErrorMessage
struct UWBP_ErrorMessage_C_ExecuteUbergraph_WBP_ErrorMessage_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
