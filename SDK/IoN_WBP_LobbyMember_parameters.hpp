#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LobbyMember.WBP_LobbyMember_C.On_AvatarImage_MouseButtonDown_1
struct UWBP_LobbyMember_C_On_AvatarImage_MouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.Construct
struct UWBP_LobbyMember_C_Construct_Params
{
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature
struct UWBP_LobbyMember_C_BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature
struct UWBP_LobbyMember_C_BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.ExecuteUbergraph_WBP_LobbyMember
struct UWBP_LobbyMember_C_ExecuteUbergraph_WBP_LobbyMember_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.PartyAddBtnClicked__DelegateSignature
struct UWBP_LobbyMember_C_PartyAddBtnClicked__DelegateSignature_Params
{
};

// Function WBP_LobbyMember.WBP_LobbyMember_C.PartyMemberClicked__DelegateSignature
struct UWBP_LobbyMember_C_PartyMemberClicked__DelegateSignature_Params
{
	class APlayerState*                                MemberPlayerState;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
