#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SKeyActionSave.SKeyActionSave
// 0x0052
struct FSKeyActionSave
{
	struct FString                                     ActionName_27_5885F13F40BEDA6C8B8F749EAEC64BBA;           // 0x0000(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	struct FString                                     Category_10_6A9C009143C58E50DF299AA2409B06BD;             // 0x0010(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	struct FString                                     Name_12_284B813542040EB40314D6A629BCF640;                 // 0x0020(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	float                                              Scale_25_55DAC9F048835ECFBB2123B59FBB6835;                // 0x0030(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               Primary_15_7375BAE94E1228E31B871684158CC790;              // 0x0034(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0035(0x0003) MISSED OFFSET
	struct FKey                                        KeyInput_26_669892994CD4745AD57CF4BDF708E2A0;             // 0x0038(0x0018) (Edit, BlueprintVisible)
	bool                                               NegativeAxis_22_17736552415A5EA125B225BE5B8415AE;         // 0x0050(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               EmptyCombination_29_A96B2E8F49C8DDD0067652A02233A112;     // 0x0051(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
