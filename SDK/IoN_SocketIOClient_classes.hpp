#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class SocketIOClient.SIOMessageConvert
// 0x0000 (0x0030 - 0x0030)
class USIOMessageConvert : public UObject
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SocketIOClient.SIOMessageConvert");
		return ptr;
	}

};


// Class SocketIOClient.SocketIOClientComponent
// 0x0108 (0x0200 - 0x00F8)
class USocketIOClientComponent : public UActorComponent
{
public:
	struct FScriptMulticastDelegate                    OnEvent;                                                  // 0x00F8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnConnected;                                              // 0x0108(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnDisconnected;                                           // 0x0118(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnConnectionProblems;                                     // 0x0128(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSocketNamespaceConnected;                               // 0x0138(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSocketNamespaceDisconnected;                            // 0x0148(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnFail;                                                   // 0x0158(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AddressAndPort;                                           // 0x0168(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	bool                                               bShouldAutoConnect;                                       // 0x0178(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0179(0x0003) MISSED OFFSET
	int                                                ReconnectionDelayInMs;                                    // 0x017C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                MaxReconnectionAttempts;                                  // 0x0180(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              ReconnectionTimeout;                                      // 0x0184(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0188(0x0008) MISSED OFFSET
	bool                                               bVerboseConnectionLog;                                    // 0x0190(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bLimitConnectionToGameWorld;                              // 0x0191(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bPluginScopedConnection;                                  // 0x0192(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x5];                                       // 0x0193(0x0005) MISSED OFFSET
	struct FString                                     PluginScopedId;                                           // 0x0198(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	bool                                               bIsConnected;                                             // 0x01A8(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x7];                                       // 0x01A9(0x0007) MISSED OFFSET
	struct FString                                     SessionId;                                                // 0x01B0(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	bool                                               bIsHavingConnectionProblems;                              // 0x01C0(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData04[0x3F];                                      // 0x01C1(0x003F) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SocketIOClient.SocketIOClientComponent");
		return ptr;
	}


	void EmitWithCallBack(const struct FString& EventName, class USIOJsonValue* Message, const struct FString& CallbackFunctionName, class UObject* Target, const struct FString& Namespace);
	void Emit(const struct FString& EventName, class USIOJsonValue* Message, const struct FString& Namespace);
	void Disconnect();
	void Connect(const struct FString& InAddressAndPort, class USIOJsonObject* Query, class USIOJsonObject* Headers);
	void BindEventToFunction(const struct FString& EventName, const struct FString& FunctionName, class UObject* Target, const struct FString& Namespace);
	void BindEvent(const struct FString& EventName, const struct FString& Namespace);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
