// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_GrenadesLabel_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Inventory_C::Get_TextBlock_GrenadesLabel_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_GrenadesLabel_Text_1");

	UWBP_Inventory_C_Get_TextBlock_GrenadesLabel_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_BoostersLabel_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Inventory_C::Get_TextBlock_BoostersLabel_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_BoostersLabel_Text_1");

	UWBP_Inventory_C_Get_TextBlock_BoostersLabel_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDoubleClick
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              InMyGeometry                   (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          InMouseEvent                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_Inventory_C::OnMouseButtonDoubleClick(struct FGeometry* InMyGeometry, struct FPointerEvent* InMouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDoubleClick");

	UWBP_Inventory_C_OnMouseButtonDoubleClick_Params params;
	params.InMyGeometry = InMyGeometry;
	params.InMouseEvent = InMouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.OnPaint
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, Const)
// Parameters:
// struct FPaintContext           Context                        (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_Inventory_C::OnPaint(struct FPaintContext* Context)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnPaint");

	UWBP_Inventory_C_OnPaint_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Context != nullptr)
		*Context = params.Context;
}


// Function WBP_Inventory.WBP_Inventory_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Inventory_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.GetText_1");

	UWBP_Inventory_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.OnKeyUp
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_Inventory_C::OnKeyUp(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnKeyUp");

	UWBP_Inventory_C_OnKeyUp_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.Get_AmmoCountText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Inventory_C::Get_AmmoCountText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Get_AmmoCountText_Text_1");

	UWBP_Inventory_C_Get_AmmoCountText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.UpdateExtraCapacitySlotVisibility
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::UpdateExtraCapacitySlotVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.UpdateExtraCapacitySlotVisibility");

	UWBP_Inventory_C_UpdateExtraCapacitySlotVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_Inventory_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDown");

	UWBP_Inventory_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.OnKeyDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_Inventory_C::OnKeyDown(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnKeyDown");

	UWBP_Inventory_C_OnKeyDown_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_Inventory_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnDrop");

	UWBP_Inventory_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Inventory.WBP_Inventory_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Inventory_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Tick");

	UWBP_Inventory_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.CloseInventory
// (BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::CloseInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.CloseInventory");

	UWBP_Inventory_C_CloseInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.ShowInventory
// (Event, Public, BlueprintEvent)

void UWBP_Inventory_C::ShowInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.ShowInventory");

	UWBP_Inventory_C_ShowInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.HideInventory
// (Event, Public, BlueprintEvent)

void UWBP_Inventory_C::HideInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.HideInventory");

	UWBP_Inventory_C_HideInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.ShowInventoryBar
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bInventoryFull                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Inventory_C::ShowInventoryBar(bool* bInventoryFull)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.ShowInventoryBar");

	UWBP_Inventory_C_ShowInventoryBar_Params params;
	params.bInventoryFull = bInventoryFull;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.OnCollapsedInventory
// (BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::OnCollapsedInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnCollapsedInventory");

	UWBP_Inventory_C_OnCollapsedInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.Init Attachment Nodes
// (BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::Init_Attachment_Nodes()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Init Attachment Nodes");

	UWBP_Inventory_C_Init_Attachment_Nodes_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.Clear Attachment Nodes
// (BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::Clear_Attachment_Nodes()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.Clear Attachment Nodes");

	UWBP_Inventory_C_Clear_Attachment_Nodes_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.OnInventoryOpened
// (BlueprintCallable, BlueprintEvent)

void UWBP_Inventory_C::OnInventoryOpened()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.OnInventoryOpened");

	UWBP_Inventory_C_OnInventoryOpened_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Inventory.WBP_Inventory_C.ExecuteUbergraph_WBP_Inventory
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Inventory_C::ExecuteUbergraph_WBP_Inventory(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Inventory.WBP_Inventory_C.ExecuteUbergraph_WBP_Inventory");

	UWBP_Inventory_C_ExecuteUbergraph_WBP_Inventory_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
