#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_ActionManager.BP_ActionManager_C
// 0x00E1 (0x01D9 - 0x00F8)
class UBP_ActionManager_C : public UActorComponent
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x00F8(0x0008) (Transient, DuplicateTransient)
	struct FScriptMulticastDelegate                    MoveForward;                                              // 0x0100(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	class APlayerController*                           Player_Controller;                                        // 0x0110(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, IsPlainOldData)
	class UBP_GameSettings_C*                          Game_Settings;                                            // 0x0118(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Move_Forward;                                             // 0x0120(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Move_Right;                                               // 0x0128(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FScriptMulticastDelegate                    MoveRight;                                                // 0x0130(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    LookUp;                                                   // 0x0140(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    LookRight;                                                // 0x0150(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    JumpPressed;                                              // 0x0160(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    RunPressed;                                               // 0x0170(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    RunReleased;                                              // 0x0180(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	class UBP_KeyAction_C*                             Look_Up;                                                  // 0x0190(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Look_Right;                                               // 0x0198(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Jump;                                                     // 0x01A0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Run;                                                      // 0x01A8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Open_Menu;                                                // 0x01B0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FScriptMulticastDelegate                    MenuPressed;                                              // 0x01B8(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	TScriptInterface<class UBPI_RegisterKeyBinding_C>  Key_Binding_Listener;                                     // 0x01C8(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               Listens_For_Bindings;                                     // 0x01D8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_ActionManager.BP_ActionManager_C");
		return ptr;
	}


	void Any_Key_Press(const struct FKey& Any_Key);
	void Input_Analog_Update();
	void Listens_New_Mouse_Bindings();
	void Register_New_Binding(const struct FSKeyInput& New_Keybinding);
	void ReceiveBeginPlay();
	void Tick_Action_Manager();
	void ReceiveTick(float* DeltaSeconds);
	void ExecuteUbergraph_BP_ActionManager(int EntryPoint);
	void MenuPressed__DelegateSignature();
	void RunReleased__DelegateSignature();
	void RunPressed__DelegateSignature();
	void JumpPressed__DelegateSignature();
	void LookRight__DelegateSignature(float Axis);
	void LookUp__DelegateSignature(float Axis);
	void MoveRight__DelegateSignature(float Axis);
	void MoveForward__DelegateSignature(float Axis);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
