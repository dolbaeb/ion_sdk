#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum ECVARType.ECVARType
enum class ECVARType : uint8_t
{
	ECVARType__NewEnumerator0      = 0,
	ECVARType__NewEnumerator1      = 1,
	ECVARType__ECVARType_MAX       = 2
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
