#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_IONKnife.BP_IONKnife_C.UserConstructionScript
struct ABP_IONKnife_C_UserConstructionScript_Params
{
};

// Function BP_IONKnife.BP_IONKnife_C.EventBP_HitSurfaceType
struct ABP_IONKnife_C_EventBP_HitSurfaceType_Params
{
	TEnumAsByte<EPhysicalSurface>*                     SurfaceTypeHit;                                           // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_IONKnife.BP_IONKnife_C.ResetBloodmask
struct ABP_IONKnife_C_ResetBloodmask_Params
{
};

// Function BP_IONKnife.BP_IONKnife_C.ReceiveBeginPlay
struct ABP_IONKnife_C_ReceiveBeginPlay_Params
{
};

// Function BP_IONKnife.BP_IONKnife_C.ExecuteUbergraph_BP_IONKnife
struct ABP_IONKnife_C_ExecuteUbergraph_BP_IONKnife_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
