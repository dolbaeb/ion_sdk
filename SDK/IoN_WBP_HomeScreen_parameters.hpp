#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnFail_5A0F3C1F41374D769909778BE8D3E051
struct UWBP_HomeScreen_C_OnFail_5A0F3C1F41374D769909778BE8D3E051_Params
{
	class UTexture2DDynamic*                           Texture;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnSuccess_5A0F3C1F41374D769909778BE8D3E051
struct UWBP_HomeScreen_C_OnSuccess_5A0F3C1F41374D769909778BE8D3E051_Params
{
	class UTexture2DDynamic*                           Texture;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00
struct UWBP_HomeScreen_C_OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00_Params
{
	struct FString                                     Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               bHasErrors;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_A0F42E9749E8C110926964B48F717ABB
struct UWBP_HomeScreen_C_OnResponse_A0F42E9749E8C110926964B48F717ABB_Params
{
	struct FString                                     ImageURL;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     LinkURL;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               bHasErrors;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnShow
struct UWBP_HomeScreen_C_OnShow_Params
{
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.On Profile Updated
struct UWBP_HomeScreen_C_On_Profile_Updated_Params
{
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature
struct UWBP_HomeScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.LoadToasterMessage
struct UWBP_HomeScreen_C_LoadToasterMessage_Params
{
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.LoadMenuNews
struct UWBP_HomeScreen_C_LoadMenuNews_Params
{
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.Connected
struct UWBP_HomeScreen_C_Connected_Params
{
	struct FString*                                    UserId;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString*                                    AuthToken;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_HomeScreen.WBP_HomeScreen_C.ExecuteUbergraph_WBP_HomeScreen
struct UWBP_HomeScreen_C_ExecuteUbergraph_WBP_HomeScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
