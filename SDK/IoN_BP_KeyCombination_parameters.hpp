#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_KeyCombination.BP_KeyCombination_C.Get Key Combination Display Name
struct UBP_KeyCombination_C_Get_Key_Combination_Display_Name_Params
{
	struct FString                                     Separator;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     No_Key_Display;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<EKeyCombinationDisplay>                Display_Type;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Display_Name;                                             // (Parm, OutParm, ZeroConstructor)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Add Key Input
struct UBP_KeyCombination_C_Add_Key_Input_Params
{
	struct FSKeyInput                                  InputPin;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UBP_KeyInput_C*                              Input;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Equal All Keys
struct UBP_KeyCombination_C_Equal_All_Keys_Params
{
	TArray<struct FSKeyInput>                          Combination;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
	bool                                               Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Equal All Conflicts
struct UBP_KeyCombination_C_Equal_All_Conflicts_Params
{
	TArray<struct FSKeyConflict>                       Conflicts;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
	bool                                               Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Evaluate Blocking Combinations
struct UBP_KeyCombination_C_Evaluate_Blocking_Combinations_Params
{
	class APlayerController*                           Player_Controller;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Is_Active;                                                // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Clear Conflicting Combinations
struct UBP_KeyCombination_C_Clear_Conflicting_Combinations_Params
{
};

// Function BP_KeyCombination.BP_KeyCombination_C.Add Conflicting Combination
struct UBP_KeyCombination_C_Add_Conflicting_Combination_Params
{
	class UBP_KeyCombination_C*                        Conflicted_Combination;                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	TEnumAsByte<EKeyConflict>                          Conflicted_;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Detect Conflict
struct UBP_KeyCombination_C_Detect_Conflict_Params
{
	class UBP_KeyCombination_C*                        Input_Combination;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	TEnumAsByte<EKeyConflict>                          Conflict_Type;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Replace Key Combination
struct UBP_KeyCombination_C_Replace_Key_Combination_Params
{
	TArray<struct FSKeyInput>                          Key_Combination;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Load Key Combination
struct UBP_KeyCombination_C_Load_Key_Combination_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Action_Name;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     Category;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     Name;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               Primary;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Save Key Combination
struct UBP_KeyCombination_C_Save_Key_Combination_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FSKeyActionSave                             KeySave;                                                  // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Key Combination Current State
struct UBP_KeyCombination_C_Key_Combination_Current_State_Params
{
	class APlayerController*                           Player_Controller;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Ignore_Conflicts;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Axis_Value;                                               // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Is_Active;                                                // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Init Key Combination
struct UBP_KeyCombination_C_Init_Key_Combination_Params
{
	struct FString                                     Name;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               Can_t_Be_None;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	TArray<struct FSKeyInput>                          Key_Combination;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
	class UBP_KeyCombination_C*                        Combination;                                              // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyCombination.BP_KeyCombination_C.Combination Updated__DelegateSignature
struct UBP_KeyCombination_C_Combination_Updated__DelegateSignature_Params
{
	class UBP_KeyCombination_C*                        Combination;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
