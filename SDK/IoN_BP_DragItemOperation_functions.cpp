// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_DragItemOperation.BP_DragItemOperation_C.Drop
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          PointerEvent                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_DragItemOperation_C::Drop(struct FPointerEvent* PointerEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_DragItemOperation.BP_DragItemOperation_C.Drop");

	UBP_DragItemOperation_C_Drop_Params params;
	params.PointerEvent = PointerEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_DragItemOperation.BP_DragItemOperation_C.DragCancelled
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          PointerEvent                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_DragItemOperation_C::DragCancelled(struct FPointerEvent* PointerEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_DragItemOperation.BP_DragItemOperation_C.DragCancelled");

	UBP_DragItemOperation_C_DragCancelled_Params params;
	params.PointerEvent = PointerEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_DragItemOperation.BP_DragItemOperation_C.ExecuteUbergraph_BP_DragItemOperation
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_DragItemOperation_C::ExecuteUbergraph_BP_DragItemOperation(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_DragItemOperation.BP_DragItemOperation_C.ExecuteUbergraph_BP_DragItemOperation");

	UBP_DragItemOperation_C_ExecuteUbergraph_BP_DragItemOperation_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_DragItemOperation.BP_DragItemOperation_C.DragFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_DragItemOperation_C::DragFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_DragItemOperation.BP_DragItemOperation_C.DragFinished__DelegateSignature");

	UBP_DragItemOperation_C_DragFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
