// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameButton_C::BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature");

	UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameButton_C::BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature");

	UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameButton_C::BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature");

	UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameButton.WBP_IngameButton_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_IngameButton_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.PreConstruct");

	UWBP_IngameButton_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameButton.WBP_IngameButton_C.ExecuteUbergraph_WBP_IngameButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_IngameButton_C::ExecuteUbergraph_WBP_IngameButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.ExecuteUbergraph_WBP_IngameButton");

	UWBP_IngameButton_C_ExecuteUbergraph_WBP_IngameButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameButton.WBP_IngameButton_C.OnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_IngameButton_C::OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameButton.WBP_IngameButton_C.OnClicked__DelegateSignature");

	UWBP_IngameButton_C_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
