// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CrateScreen.WBP_CrateScreen_C.SpawnAllIconWidgets
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::SpawnAllIconWidgets()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.SpawnAllIconWidgets");

	UWBP_CrateScreen_C_SpawnAllIconWidgets_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.CloseCrateScreen
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::CloseCrateScreen()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.CloseCrateScreen");

	UWBP_CrateScreen_C_CloseCrateScreen_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenFailed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::CrateOpenFailed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenFailed");

	UWBP_CrateScreen_C_CrateOpenFailed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.TickAnimSpeed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::TickAnimSpeed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.TickAnimSpeed");

	UWBP_CrateScreen_C_TickAnimSpeed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.PrepareItemsInCrateArray
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamItem*           GrantedItem                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateScreen_C::PrepareItemsInCrateArray(class UIONSteamItem* GrantedItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.PrepareItemsInCrateArray");

	UWBP_CrateScreen_C_PrepareItemsInCrateArray_Params params;
	params.GrantedItem = GrantedItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.AnimateIcons
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::AnimateIcons()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.AnimateIcons");

	UWBP_CrateScreen_C_AnimateIcons_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.SetSteamItem
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_CrateScreen_C::SetSteamItem(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.SetSteamItem");

	UWBP_CrateScreen_C_SetSteamItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.TickSingleIconAnim
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 Icon                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            Idx                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateScreen_C::TickSingleIconAnim(class UWidget* Icon, int Idx)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.TickSingleIconAnim");

	UWBP_CrateScreen_C_TickSingleIconAnim_Params params;
	params.Icon = Icon;
	params.Idx = Idx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CrateScreen_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.Construct");

	UWBP_CrateScreen_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateScreen_C::BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature");

	UWBP_CrateScreen_C_BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateScreen_C::BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature");

	UWBP_CrateScreen_C_BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenConfirmed
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Result                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateScreen_C::CrateOpenConfirmed(int Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenConfirmed");

	UWBP_CrateScreen_C_CrateOpenConfirmed_Params params;
	params.Result = Result;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateScreen_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.Tick");

	UWBP_CrateScreen_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.TransitionOutEvent
// (BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::TransitionOutEvent()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.TransitionOutEvent");

	UWBP_CrateScreen_C_TransitionOutEvent_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateScreen_C::BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature");

	UWBP_CrateScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateScreen_C::BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature");

	UWBP_CrateScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.ExecuteUbergraph_WBP_CrateScreen
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateScreen_C::ExecuteUbergraph_WBP_CrateScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.ExecuteUbergraph_WBP_CrateScreen");

	UWBP_CrateScreen_C_ExecuteUbergraph_WBP_CrateScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.BackToInventory__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::BackToInventory__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.BackToInventory__DelegateSignature");

	UWBP_CrateScreen_C_BackToInventory__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateScreen.WBP_CrateScreen_C.ClearActionsMenu__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_CrateScreen_C::ClearActionsMenu__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateScreen.WBP_CrateScreen_C.ClearActionsMenu__DelegateSignature");

	UWBP_CrateScreen_C_ClearActionsMenu__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
