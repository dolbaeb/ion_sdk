#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_M9A1_Elite.BP_M9A1_Elite_C
// 0x0008 (0x1178 - 0x1170)
class ABP_M9A1_Elite_C : public AIONFirearm
{
public:
	class UMaterialInterface*                          M9A1_Material_Override;                                   // 0x1170(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_M9A1_Elite.BP_M9A1_Elite_C");
		return ptr;
	}


	void UserConstructionScript();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
