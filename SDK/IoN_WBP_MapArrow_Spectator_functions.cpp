// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.GetVisibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_MapArrow_Spectator_C::GetVisibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.GetVisibility_1");

	UWBP_MapArrow_Spectator_C_GetVisibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MapArrow_Spectator_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Construct");

	UWBP_MapArrow_Spectator_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapArrow_Spectator_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Tick");

	UWBP_MapArrow_Spectator_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Toggle Highlight
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Highlight                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapArrow_Spectator_C::Toggle_Highlight(bool Highlight)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Toggle Highlight");

	UWBP_MapArrow_Spectator_C_Toggle_Highlight_Params params;
	params.Highlight = Highlight;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MapArrow_Spectator_C::BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature");

	UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MapArrow_Spectator_C::BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature");

	UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MapArrow_Spectator_C::BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature");

	UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.ExecuteUbergraph_WBP_MapArrow_Spectator
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapArrow_Spectator_C::ExecuteUbergraph_WBP_MapArrow_Spectator(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.ExecuteUbergraph_WBP_MapArrow_Spectator");

	UWBP_MapArrow_Spectator_C_ExecuteUbergraph_WBP_MapArrow_Spectator_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.OnPlayerLeave__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_MapArrow_Spectator_C* Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MapArrow_Spectator_C::OnPlayerLeave__DelegateSignature(class UWBP_MapArrow_Spectator_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.OnPlayerLeave__DelegateSignature");

	UWBP_MapArrow_Spectator_C_OnPlayerLeave__DelegateSignature_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
