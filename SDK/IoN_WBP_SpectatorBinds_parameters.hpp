#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.PreConstruct
struct UWBP_SpectatorBinds_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Construct
struct UWBP_SpectatorBinds_C_Construct_Params
{
};

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Tick
struct UWBP_SpectatorBinds_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Set Output Text
struct UWBP_SpectatorBinds_C_Set_Output_Text_Params
{
	struct FText                                       Text;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.ExecuteUbergraph_WBP_SpectatorBinds
struct UWBP_SpectatorBinds_C_ExecuteUbergraph_WBP_SpectatorBinds_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
