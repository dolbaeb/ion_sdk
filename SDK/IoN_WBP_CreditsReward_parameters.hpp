#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CreditsReward.WBP_CreditsReward_C.GetText_1
struct UWBP_CreditsReward_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_CreditsReward.WBP_CreditsReward_C.Construct
struct UWBP_CreditsReward_C_Construct_Params
{
};

// Function WBP_CreditsReward.WBP_CreditsReward_C.ExecuteUbergraph_WBP_CreditsReward
struct UWBP_CreditsReward_C_ExecuteUbergraph_WBP_CreditsReward_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
