// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_GameOverlay.WBP_GameOverlay_C.DisplayAchievementUnlock
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FText                   Achievement                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_GameOverlay_C::DisplayAchievementUnlock(const struct FText& Achievement)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.DisplayAchievementUnlock");

	UWBP_GameOverlay_C_DisplayAchievementUnlock_Params params;
	params.Achievement = Achievement;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_Disclaimer_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::Get_TextBlock_Disclaimer_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_Disclaimer_Visibility_1");

	UWBP_GameOverlay_C_Get_TextBlock_Disclaimer_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_KillsBox_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::Get_KillsBox_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Get_KillsBox_Visibility_1");

	UWBP_GameOverlay_C_Get_KillsBox_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Kills
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_Kills()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Kills");

	UWBP_GameOverlay_C_GetText_Kills_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetCreditBoxVisibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::GetCreditBoxVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetCreditBoxVisibility");

	UWBP_GameOverlay_C_GetCreditBoxVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Score
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_Score()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Score");

	UWBP_GameOverlay_C_GetText_Score_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetSurvivorsBoxVisibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::GetSurvivorsBoxVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetSurvivorsBoxVisibility");

	UWBP_GameOverlay_C_GetSurvivorsBoxVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_SurvivorCount
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_SurvivorCount()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_SurvivorCount");

	UWBP_GameOverlay_C_GetText_SurvivorCount_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetServerInfoVisibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::GetServerInfoVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetServerInfoVisibility");

	UWBP_GameOverlay_C_GetServerInfoVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Tooltip
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_Tooltip()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Tooltip");

	UWBP_GameOverlay_C_GetText_Tooltip_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetVisibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::GetVisibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetVisibility_1");

	UWBP_GameOverlay_C_GetVisibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_PlayerCount
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::Get_TextBlock_PlayerCount()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_PlayerCount");

	UWBP_GameOverlay_C_Get_TextBlock_PlayerCount_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_MatchTimer
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_MatchTimer()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_MatchTimer");

	UWBP_GameOverlay_C_GetText_MatchTimer_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_QueueInfo_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_GameOverlay_C::Get_QueueInfo_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Get_QueueInfo_Visibility_1");

	UWBP_GameOverlay_C_Get_QueueInfo_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Debug
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_GameOverlay_C::GetText_Debug()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Debug");

	UWBP_GameOverlay_C_GetText_Debug_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.StartMessage
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FName*                  Type                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UWBP_GameOverlay_C::StartMessage(struct FName* Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.StartMessage");

	UWBP_GameOverlay_C_StartMessage_Params params;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnReceivedChatMessage
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// class AIONPlayerState**        From                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString*                Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FName*                  Type                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UWBP_GameOverlay_C::OnReceivedChatMessage(class AIONPlayerState** From, struct FString* Message, struct FName* Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnReceivedChatMessage");

	UWBP_GameOverlay_C_OnReceivedChatMessage_Params params;
	params.From = From;
	params.Message = Message;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_GameOverlay_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Construct");

	UWBP_GameOverlay_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.SecondTimer
// (BlueprintCallable, BlueprintEvent)

void UWBP_GameOverlay_C::SecondTimer()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.SecondTimer");

	UWBP_GameOverlay_C_SecondTimer_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.SetChatVisibility
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bInVisible                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::SetChatVisibility(bool* bInVisible)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.SetChatVisibility");

	UWBP_GameOverlay_C_SetChatVisibility_Params params;
	params.bInVisible = bInVisible;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnMicrophoneStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bEnabled                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::OnMicrophoneStateChanged(bool* bEnabled)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnMicrophoneStateChanged");

	UWBP_GameOverlay_C_OnMicrophoneStateChanged_Params params;
	params.bEnabled = bEnabled;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.SetMapTooltipVisibility
// (Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool*                          bInVisible                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::SetMapTooltipVisibility(bool* bInVisible)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.SetMapTooltipVisibility");

	UWBP_GameOverlay_C_SetMapTooltipVisibility_Params params;
	params.bInVisible = bInVisible;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Update Voice Icon
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Proximity                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Activated                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::Update_Voice_Icon(bool Proximity, bool Activated)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Update Voice Icon");

	UWBP_GameOverlay_C_Update_Voice_Icon_Params params;
	params.Proximity = Proximity;
	params.Activated = Activated;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.Tick");

	UWBP_GameOverlay_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnScoreEventReceived
// (Event, Public, BlueprintEvent)
// Parameters:
// EScoreEvent*                   Type                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int*                           Points                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::OnScoreEventReceived(EScoreEvent* Type, int* Points)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnScoreEventReceived");

	UWBP_GameOverlay_C_OnScoreEventReceived_Params params;
	params.Type = Type;
	params.Points = Points;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnKillConfirmed
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo*               LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_GameOverlay_C::OnKillConfirmed(struct FHitInfo* LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnKillConfirmed");

	UWBP_GameOverlay_C_OnKillConfirmed_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnDownConfirmed
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo*               LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_GameOverlay_C::OnDownConfirmed(struct FHitInfo* LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnDownConfirmed");

	UWBP_GameOverlay_C_OnDownConfirmed_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDied
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo*               LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_GameOverlay_C::OnPlayerDied(struct FHitInfo* LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDied");

	UWBP_GameOverlay_C_OnPlayerDied_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDowned
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo*               LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_GameOverlay_C::OnPlayerDowned(struct FHitInfo* LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDowned");

	UWBP_GameOverlay_C_OnPlayerDowned_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.DisplaySubtitle
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FText*                  SubtitleText                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_GameOverlay_C::DisplaySubtitle(struct FText* SubtitleText)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.DisplaySubtitle");

	UWBP_GameOverlay_C_DisplaySubtitle_Params params;
	params.SubtitleText = SubtitleText;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_GameOverlay.WBP_GameOverlay_C.ExecuteUbergraph_WBP_GameOverlay
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_GameOverlay_C::ExecuteUbergraph_WBP_GameOverlay(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_GameOverlay.WBP_GameOverlay_C.ExecuteUbergraph_WBP_GameOverlay");

	UWBP_GameOverlay_C_ExecuteUbergraph_WBP_GameOverlay_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
