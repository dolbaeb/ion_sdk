// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Kills
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Kills()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Kills");

	UWBP_PostMatchTabTeams_C_Sort_by_Kills_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Place
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Place()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Place");

	UWBP_PostMatchTabTeams_C_Sort_by_Place_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Team
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Team()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Team");

	UWBP_PostMatchTabTeams_C_Sort_by_Team_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Damage
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Damage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Damage");

	UWBP_PostMatchTabTeams_C_Sort_by_Damage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Accuracy
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Accuracy()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Accuracy");

	UWBP_PostMatchTabTeams_C_Sort_by_Accuracy_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Time Alive
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Time_Alive()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Time Alive");

	UWBP_PostMatchTabTeams_C_Sort_by_Time_Alive_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Headshot Accuracy
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Sort_by_Headshot_Accuracy()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Headshot Accuracy");

	UWBP_PostMatchTabTeams_C_Sort_by_Headshot_Accuracy_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Minimize All Tabs
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Minimize_All_Tabs()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Minimize All Tabs");

	UWBP_PostMatchTabTeams_C_Minimize_All_Tabs_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Team Widgets
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Create_Team_Widgets()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Team Widgets");

	UWBP_PostMatchTabTeams_C_Create_Team_Widgets_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Construct");

	UWBP_PostMatchTabTeams_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Change Mode
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Show_Teams                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchTabTeams_C::Change_Mode(bool Show_Teams)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Change Mode");

	UWBP_PostMatchTabTeams_C_Change_Mode_Params params;
	params.Show_Teams = Show_Teams;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Solo Players
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Create_Solo_Players()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Solo Players");

	UWBP_PostMatchTabTeams_C_Create_Solo_Players_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Repopulate Fields
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTabTeams_C::Repopulate_Fields()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Repopulate Fields");

	UWBP_PostMatchTabTeams_C_Repopulate_Fields_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.ExecuteUbergraph_WBP_PostMatchTabTeams
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchTabTeams_C::ExecuteUbergraph_WBP_PostMatchTabTeams(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.ExecuteUbergraph_WBP_PostMatchTabTeams");

	UWBP_PostMatchTabTeams_C_ExecuteUbergraph_WBP_PostMatchTabTeams_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
