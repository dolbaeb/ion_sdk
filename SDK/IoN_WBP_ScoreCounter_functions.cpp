// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.Get_Label_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ScoreCounter_C::Get_Label_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreCounter.WBP_ScoreCounter_C.Get_Label_Text_1");

	UWBP_ScoreCounter_C_Get_Label_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ScoreCounter.WBP_ScoreCounter_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreCounter_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreCounter.WBP_ScoreCounter_C.Tick");

	UWBP_ScoreCounter_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreCounter.WBP_ScoreCounter_C.BeginAnimation
// (BlueprintCallable, BlueprintEvent)

void UWBP_ScoreCounter_C::BeginAnimation()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreCounter.WBP_ScoreCounter_C.BeginAnimation");

	UWBP_ScoreCounter_C_BeginAnimation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreCounter.WBP_ScoreCounter_C.ExecuteUbergraph_WBP_ScoreCounter
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreCounter_C::ExecuteUbergraph_WBP_ScoreCounter(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreCounter.WBP_ScoreCounter_C.ExecuteUbergraph_WBP_ScoreCounter");

	UWBP_ScoreCounter_C_ExecuteUbergraph_WBP_ScoreCounter_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreCounter.WBP_ScoreCounter_C.OnValueUpdated__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ScoreCounter_C::OnValueUpdated__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreCounter.WBP_ScoreCounter_C.OnValueUpdated__DelegateSignature");

	UWBP_ScoreCounter_C_OnValueUpdated__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
