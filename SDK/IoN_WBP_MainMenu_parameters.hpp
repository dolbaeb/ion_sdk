#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MainMenu.WBP_MainMenu_C.ShowKickMessage
struct UWBP_MainMenu_C_ShowKickMessage_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.HideCompletely
struct UWBP_MainMenu_C_HideCompletely_Params
{
	bool                                               bHide;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenu.WBP_MainMenu_C.CrateOpening
struct UWBP_MainMenu_C_CrateOpening_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.OnCreditsUpdated
struct UWBP_MainMenu_C_OnCreditsUpdated_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.TransitionToInventory
struct UWBP_MainMenu_C_TransitionToInventory_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.Set Play Btn Image
struct UWBP_MainMenu_C_Set_Play_Btn_Image_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.OnMouseButtonDown
struct UWBP_MainMenu_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MainMenu.WBP_MainMenu_C.ChangeZoom
struct UWBP_MainMenu_C_ChangeZoom_Params
{
	int                                                NewZoomLevel;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenu.WBP_MainMenu_C.ShowSubmenu
struct UWBP_MainMenu_C_ShowSubmenu_Params
{
	int                                                SubmenuIdx;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenu.WBP_MainMenu_C.GetVisibility_1
struct UWBP_MainMenu_C_GetVisibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_MainMenu.WBP_MainMenu_C.On_ShadeOverlay_MouseButtonDown_1
struct UWBP_MainMenu_C_On_ShadeOverlay_MouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MainMenu.WBP_MainMenu_C.OnSettingsMenuClosed
struct UWBP_MainMenu_C_OnSettingsMenuClosed_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.ShowPartyInvite
struct UWBP_MainMenu_C_ShowPartyInvite_Params
{
	struct FString                                     PlayerName;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.ShowUIDebug
struct UWBP_MainMenu_C_ShowUIDebug_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.Construct
struct UWBP_MainMenu_C_Construct_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.ShowOKNotice
struct UWBP_MainMenu_C_ShowOKNotice_Params
{
	struct FString                                     Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.On Settings Menu Closed
struct UWBP_MainMenu_C_On_Settings_Menu_Closed_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature
struct UWBP_MainMenu_C_BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_MainMenu.WBP_MainMenu_C.ExecuteUbergraph_WBP_MainMenu
struct UWBP_MainMenu_C_ExecuteUbergraph_WBP_MainMenu_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenu.WBP_MainMenu_C.Profile Updated__DelegateSignature
struct UWBP_MainMenu_C_Profile_Updated__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
