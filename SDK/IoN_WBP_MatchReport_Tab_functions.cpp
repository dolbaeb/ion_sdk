// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_MatchReport_Tab_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseButtonDown");

	UWBP_MatchReport_Tab_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MatchReport_Tab_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Construct");

	UWBP_MatchReport_Tab_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Show Placeholder
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Tab_C::Show_Placeholder()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Show Placeholder");

	UWBP_MatchReport_Tab_C_Show_Placeholder_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Activate
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Tab_C::Activate()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Activate");

	UWBP_MatchReport_Tab_C_Activate_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseEnter
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_MatchReport_Tab_C::OnMouseEnter(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseEnter");

	UWBP_MatchReport_Tab_C_OnMouseEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_MatchReport_Tab_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseLeave");

	UWBP_MatchReport_Tab_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Select
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Tab_C::Select()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Select");

	UWBP_MatchReport_Tab_C_Select_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_Tab_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.PreConstruct");

	UWBP_MatchReport_Tab_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.On Unhover
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Tab_C::On_Unhover()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.On Unhover");

	UWBP_MatchReport_Tab_C_On_Unhover_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.ExecuteUbergraph_WBP_MatchReport_Tab
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_Tab_C::ExecuteUbergraph_WBP_MatchReport_Tab(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.ExecuteUbergraph_WBP_MatchReport_Tab");

	UWBP_MatchReport_Tab_C_ExecuteUbergraph_WBP_MatchReport_Tab_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Clicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Tab_C::Clicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Clicked__DelegateSignature");

	UWBP_MatchReport_Tab_C_Clicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
