#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class GameSparks.GameSparksComponent
// 0x0020 (0x02C0 - 0x02A0)
class UGameSparksComponent : public USceneComponent
{
public:
	struct FScriptMulticastDelegate                    OnGameSparksAvailableDelegate;                            // 0x02A0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGameSparksDebugLog;                                     // 0x02B0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GameSparksComponent");
		return ptr;
	}


	void SetApiStage(const struct FString& stage);
	void SetApiDomain(const struct FString& domain);
	void SetApiCredential(const struct FString& credential);
	void OnGameSparksLogEvent__DelegateSignature(const struct FString& logMessage);
	void OnGameSparksAvailable__DelegateSignature(bool available);
	void Logout();
	bool IsAvailable();
	bool IsAuthenticated();
	void Disconnect();
	void Connect(const struct FString& apikey, const struct FString& secret, bool previewServer, bool clearCachedAuthentication);
};


// Class GameSparks.GameSparksLogEventData
// 0x00F0 (0x0120 - 0x0030)
class UGameSparksLogEventData : public UObject
{
public:
	TMap<struct FString, struct FString>               m_strings;                                                // 0x0030(0x0050) (ZeroConstructor)
	TMap<struct FString, int>                          m_numbers;                                                // 0x0080(0x0050) (ZeroConstructor)
	TMap<struct FString, class UGameSparksScriptData*> m_objects;                                                // 0x00D0(0x0050) (ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GameSparksLogEventData");
		return ptr;
	}


	class UGameSparksLogEventData* SetString(const struct FString& Name, const struct FString& Value);
	class UGameSparksLogEventData* SetObject(const struct FString& Name, class UGameSparksScriptData* Value);
	class UGameSparksLogEventData* SetNumber(const struct FString& Name, int Value);
	class UGameSparksLogEventData* STATIC_CreateGameSparksLogEventAttributes(class UObject* WorldContextObject);
};


// Class GameSparks.GameSparksObject
// 0x0020 (0x0050 - 0x0030)
class UGameSparksObject : public UObject
{
public:
	struct FScriptMulticastDelegate                    OnGameSparksAvailableDelegate;                            // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGameSparksDebugLog;                                     // 0x0040(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GameSparksObject");
		return ptr;
	}


	void SetApiStage(const struct FString& stage);
	void SetApiDomain(const struct FString& domain);
	void SetApiCredential(const struct FString& credential);
	void OnGameSparksLogEvent__DelegateSignature(const struct FString& logMessage);
	void OnGameSparksAvailable__DelegateSignature(bool available);
	void Logout();
	bool IsAvailable();
	bool IsAuthenticated();
	void Disconnect();
	void Connect(const struct FString& apikey, const struct FString& secret, bool previewServer, bool clearCachedAuthentication);
};


// Class GameSparks.GameSparksRequestArray
// 0x0010 (0x0040 - 0x0030)
class UGameSparksRequestArray : public UObject
{
public:
	TArray<struct FString>                             StringArray;                                              // 0x0030(0x0010) (BlueprintVisible, ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GameSparksRequestArray");
		return ptr;
	}


	class UGameSparksRequestArray* STATIC_CreateGameSparksRequestArray(class UObject* WorldContextObject);
};


// Class GameSparks.GameSparksScriptData
// 0x0010 (0x0040 - 0x0030)
class UGameSparksScriptData : public UObject
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0030(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GameSparksScriptData");
		return ptr;
	}


	struct FString ToString();
	class UGameSparksScriptData* SetStringArray(const struct FString& Name, TArray<struct FString> Value);
	class UGameSparksScriptData* SetString(const struct FString& Name, const struct FString& Value);
	class UGameSparksScriptData* SetNumberArray(const struct FString& Name, TArray<int> Value);
	class UGameSparksScriptData* SetNumber(const struct FString& Name, int Value);
	class UGameSparksScriptData* SetGSDataArray(const struct FString& Name, TArray<class UGameSparksScriptData*> Value);
	class UGameSparksScriptData* SetGSData(const struct FString& Name, class UGameSparksScriptData* Value);
	class UGameSparksScriptData* SetFloatArray(const struct FString& Name, TArray<float> Value);
	class UGameSparksScriptData* SetFloat(const struct FString& Name, float Value);
	class UGameSparksScriptData* SetBoolean(const struct FString& Name, bool Value);
	struct FString JSONString();
	bool HasStringArray(const struct FString& Name);
	bool HasString(const struct FString& Name);
	bool HasNumberArray(const struct FString& Name);
	bool HasNumber(const struct FString& Name);
	bool HasGSData(const struct FString& Name);
	bool HasFloatArray(const struct FString& Name);
	bool HasFloat(const struct FString& Name);
	bool HasBoolean(const struct FString& Name);
	TArray<struct FString> GetStringArray(const struct FString& Name);
	struct FString GetString(const struct FString& Name);
	TArray<int> GetNumberArray(const struct FString& Name);
	int GetNumber(const struct FString& Name);
	TArray<struct FString> GetKeys();
	TArray<class UGameSparksScriptData*> GetGSDataArray(const struct FString& Name);
	class UGameSparksScriptData* GetGSData(const struct FString& Name);
	TArray<float> GetFloatArray(const struct FString& Name);
	float GetFloat(const struct FString& Name);
	bool GetBoolean(const struct FString& Name);
	class UGameSparksScriptData* STATIC_CreateGameSparksScriptData(class UObject* WorldContextObject);
};


// Class GameSparks.GSAcceptChallengeRequest
// 0x0040 (0x0070 - 0x0030)
class UGSAcceptChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Message;                                                  // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAcceptChallengeRequest");
		return ptr;
	}


	class UGSAcceptChallengeRequest* STATIC_SendAcceptChallengeRequest(const struct FString& ChallengeInstanceId, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAccountDetailsRequest
// 0x0020 (0x0050 - 0x0030)
class UGSAccountDetailsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAccountDetailsRequest");
		return ptr;
	}


	class UGSAccountDetailsRequest* STATIC_SendAccountDetailsRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAmazonBuyGoodsRequest
// 0x0058 (0x0088 - 0x0030)
class UGSAmazonBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AmazonUserId;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     CurrencyCode;                                             // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     ReceiptId;                                                // 0x0060(0x0010) (ZeroConstructor)
	float                                              SubUnitPrice;                                             // 0x0070(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0074(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0075(0x0003) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAmazonBuyGoodsRequest");
		return ptr;
	}


	class UGSAmazonBuyGoodsRequest* STATIC_SendAmazonBuyGoodsRequest(const struct FString& AmazonUserId, const struct FString& CurrencyCode, const struct FString& ReceiptId, float SubUnitPrice, bool UniqueTransactionByPlayer, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAmazonConnectRequest
// 0x0048 (0x0078 - 0x0030)
class UGSAmazonConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAmazonConnectRequest");
		return ptr;
	}


	class UGSAmazonConnectRequest* STATIC_SendAmazonConnectRequest(const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAnalyticsRequest
// 0x0048 (0x0078 - 0x0030)
class UGSAnalyticsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       Data;                                                     // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               End;                                                      // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0049(0x0007) MISSED OFFSET
	struct FString                                     Key;                                                      // 0x0050(0x0010) (ZeroConstructor)
	bool                                               Start;                                                    // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0061(0x0007) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAnalyticsRequest");
		return ptr;
	}


	class UGSAnalyticsRequest* STATIC_SendAnalyticsRequest(class UGameSparksScriptData* Data, bool End, const struct FString& Key, bool Start, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAroundMeLeaderboardRequest
// 0x0080 (0x00B0 - 0x0030)
class UGSAroundMeLeaderboardRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       CustomIdFilter;                                           // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               DontErrorOnNotSocial;                                     // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                EntryCount;                                               // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // 0x0068(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0071(0x0007) MISSED OFFSET
	struct FString                                     LeaderboardShortCode;                                     // 0x0078(0x0010) (ZeroConstructor)
	bool                                               Social;                                                   // 0x0088(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0089(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamIds;                                                  // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0098(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x00A0(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00A8(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x3];                                       // 0x00A9(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00AC(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAroundMeLeaderboardRequest");
		return ptr;
	}


	class UGSAroundMeLeaderboardRequest* STATIC_SendAroundMeLeaderboardRequest(const struct FString& ChallengeInstanceId, class UGameSparksScriptData* CustomIdFilter, bool DontErrorOnNotSocial, int EntryCount, class UGameSparksRequestArray* FriendIds, int IncludeFirst, int IncludeLast, bool InverseSocial, const struct FString& LeaderboardShortCode, bool Social, class UGameSparksRequestArray* TeamIds, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSAuthenticationRequest
// 0x0040 (0x0070 - 0x0030)
class UGSAuthenticationRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     Password;                                                 // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     UserName;                                                 // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSAuthenticationRequest");
		return ptr;
	}


	class UGSAuthenticationRequest* STATIC_SendAuthenticationRequest(const struct FString& Password, const struct FString& UserName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSBatchAdminRequest
// 0x0030 (0x0060 - 0x0030)
class UGSBatchAdminRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     PlayerIds;                                                // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Request;                                                  // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSBatchAdminRequest");
		return ptr;
	}


	class UGSBatchAdminRequest* STATIC_SendBatchAdminRequest(class UGameSparksRequestArray* PlayerIds, class UGameSparksScriptData* Request, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSBuyVirtualGoodsRequest
// 0x0038 (0x0068 - 0x0030)
class UGSBuyVirtualGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                CurrencyType;                                             // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Quantity;                                                 // 0x0044(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FString                                     ShortCode;                                                // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0061(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSBuyVirtualGoodsRequest");
		return ptr;
	}


	class UGSBuyVirtualGoodsRequest* STATIC_SendBuyVirtualGoodsRequest(int CurrencyType, int Quantity, const struct FString& ShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSCancelBulkJobAdminRequest
// 0x0028 (0x0058 - 0x0030)
class UGSCancelBulkJobAdminRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     BulkJobIds;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSCancelBulkJobAdminRequest");
		return ptr;
	}


	class UGSCancelBulkJobAdminRequest* STATIC_SendCancelBulkJobAdminRequest(class UGameSparksRequestArray* BulkJobIds, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSChangeUserDetailsRequest
// 0x0070 (0x00A0 - 0x0030)
class UGSChangeUserDetailsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DisplayName;                                              // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Language;                                                 // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     NewPassword;                                              // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     OldPassword;                                              // 0x0070(0x0010) (ZeroConstructor)
	struct FString                                     UserName;                                                 // 0x0080(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0098(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0099(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x009C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSChangeUserDetailsRequest");
		return ptr;
	}


	class UGSChangeUserDetailsRequest* STATIC_SendChangeUserDetailsRequest(const struct FString& DisplayName, const struct FString& Language, const struct FString& NewPassword, const struct FString& OldPassword, const struct FString& UserName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSChatOnChallengeRequest
// 0x0040 (0x0070 - 0x0030)
class UGSChatOnChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Message;                                                  // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSChatOnChallengeRequest");
		return ptr;
	}


	class UGSChatOnChallengeRequest* STATIC_SendChatOnChallengeRequest(const struct FString& ChallengeInstanceId, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSConsumeVirtualGoodRequest
// 0x0038 (0x0068 - 0x0030)
class UGSConsumeVirtualGoodRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                Quantity;                                                 // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0044(0x0004) MISSED OFFSET
	struct FString                                     ShortCode;                                                // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0061(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSConsumeVirtualGoodRequest");
		return ptr;
	}


	class UGSConsumeVirtualGoodRequest* STATIC_SendConsumeVirtualGoodRequest(int Quantity, const struct FString& ShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSCreateChallengeRequest
// 0x00C0 (0x00F0 - 0x0030)
class UGSCreateChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessType;                                               // 0x0040(0x0010) (ZeroConstructor)
	bool                                               AutoStartJoinedChallengeOnMaxPlayers;                     // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0051(0x0007) MISSED OFFSET
	struct FString                                     ChallengeMessage;                                         // 0x0058(0x0010) (ZeroConstructor)
	struct FString                                     ChallengeShortCode;                                       // 0x0068(0x0010) (ZeroConstructor)
	int                                                Currency1Wager;                                           // 0x0078(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Currency2Wager;                                           // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Currency3Wager;                                           // 0x0080(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Currency4Wager;                                           // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Currency5Wager;                                           // 0x0088(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Currency6Wager;                                           // 0x008C(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       EligibilityCriteria;                                      // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     EndTime;                                                  // 0x0098(0x0010) (ZeroConstructor)
	struct FString                                     ExpiryTime;                                               // 0x00A8(0x0010) (ZeroConstructor)
	int                                                MaxAttempts;                                              // 0x00B8(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                MaxPlayers;                                               // 0x00BC(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                MinPlayers;                                               // 0x00C0(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               Silent;                                                   // 0x00C4(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x00C5(0x0003) MISSED OFFSET
	struct FString                                     StartTime;                                                // 0x00C8(0x0010) (ZeroConstructor)
	class UGameSparksRequestArray*                     UsersToChallenge;                                         // 0x00D8(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x00E0(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00E8(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x00E9(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00EC(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSCreateChallengeRequest");
		return ptr;
	}


	class UGSCreateChallengeRequest* STATIC_SendCreateChallengeRequest(const struct FString& AccessType, bool AutoStartJoinedChallengeOnMaxPlayers, const struct FString& ChallengeMessage, const struct FString& ChallengeShortCode, int Currency1Wager, int Currency2Wager, int Currency3Wager, int Currency4Wager, int Currency5Wager, int Currency6Wager, class UGameSparksScriptData* EligibilityCriteria, const struct FString& EndTime, const struct FString& ExpiryTime, int MaxAttempts, int MaxPlayers, int MinPlayers, bool Silent, const struct FString& StartTime, class UGameSparksRequestArray* UsersToChallenge, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSCreateTeamRequest
// 0x0050 (0x0080 - 0x0030)
class UGSCreateTeamRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     TeamId;                                                   // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     TeamName;                                                 // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSCreateTeamRequest");
		return ptr;
	}


	class UGSCreateTeamRequest* STATIC_SendCreateTeamRequest(const struct FString& TeamId, const struct FString& TeamName, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSDeclineChallengeRequest
// 0x0040 (0x0070 - 0x0030)
class UGSDeclineChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Message;                                                  // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSDeclineChallengeRequest");
		return ptr;
	}


	class UGSDeclineChallengeRequest* STATIC_SendDeclineChallengeRequest(const struct FString& ChallengeInstanceId, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSDeviceAuthenticationRequest
// 0x0098 (0x00C8 - 0x0030)
class UGSDeviceAuthenticationRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DeviceID;                                                 // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     DeviceModel;                                              // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     DeviceName;                                               // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     DeviceOS;                                                 // 0x0070(0x0010) (ZeroConstructor)
	struct FString                                     DeviceType;                                               // 0x0080(0x0010) (ZeroConstructor)
	struct FString                                     DisplayName;                                              // 0x0090(0x0010) (ZeroConstructor)
	struct FString                                     OperatingSystem;                                          // 0x00A0(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x00B0(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x00B8(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00C0(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x00C1(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00C4(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSDeviceAuthenticationRequest");
		return ptr;
	}


	class UGSDeviceAuthenticationRequest* STATIC_SendDeviceAuthenticationRequest(const struct FString& DeviceID, const struct FString& DeviceModel, const struct FString& DeviceName, const struct FString& DeviceOS, const struct FString& DeviceType, const struct FString& DisplayName, const struct FString& OperatingSystem, class UGameSparksScriptData* Segments, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSDismissMessageRequest
// 0x0030 (0x0060 - 0x0030)
class UGSDismissMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MessageId;                                                // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSDismissMessageRequest");
		return ptr;
	}


	class UGSDismissMessageRequest* STATIC_SendDismissMessageRequest(const struct FString& MessageId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSDismissMultipleMessagesRequest
// 0x0028 (0x0058 - 0x0030)
class UGSDismissMultipleMessagesRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     MessageIds;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSDismissMultipleMessagesRequest");
		return ptr;
	}


	class UGSDismissMultipleMessagesRequest* STATIC_SendDismissMultipleMessagesRequest(class UGameSparksRequestArray* MessageIds, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSDropTeamRequest
// 0x0050 (0x0080 - 0x0030)
class UGSDropTeamRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     OwnerId;                                                  // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSDropTeamRequest");
		return ptr;
	}


	class UGSDropTeamRequest* STATIC_SendDropTeamRequest(const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSEndSessionRequest
// 0x0020 (0x0050 - 0x0030)
class UGSEndSessionRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSEndSessionRequest");
		return ptr;
	}


	class UGSEndSessionRequest* STATIC_SendEndSessionRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSFacebookConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSFacebookConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Code;                                                     // 0x0050(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0072(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSFacebookConnectRequest");
		return ptr;
	}


	class UGSFacebookConnectRequest* STATIC_SendFacebookConnectRequest(const struct FString& AccessToken, const struct FString& Code, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSFindChallengeRequest
// 0x0050 (0x0080 - 0x0030)
class UGSFindChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessType;                                               // 0x0040(0x0010) (ZeroConstructor)
	int                                                Count;                                                    // 0x0050(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0054(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       Eligibility;                                              // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
	class UGameSparksRequestArray*                     ShortCode;                                                // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSFindChallengeRequest");
		return ptr;
	}


	class UGSFindChallengeRequest* STATIC_SendFindChallengeRequest(const struct FString& AccessType, int Count, class UGameSparksScriptData* Eligibility, int Offset, class UGameSparksRequestArray* ShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSFindMatchRequest
// 0x0058 (0x0088 - 0x0030)
class UGSFindMatchRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     Action;                                                   // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     MatchGroup;                                               // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // 0x0060(0x0010) (ZeroConstructor)
	int                                                Skill;                                                    // 0x0070(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0074(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSFindMatchRequest");
		return ptr;
	}


	class UGSFindMatchRequest* STATIC_SendFindMatchRequest(const struct FString& Action, const struct FString& MatchGroup, const struct FString& MatchShortCode, int Skill, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSFindPendingMatchesRequest
// 0x0048 (0x0078 - 0x0030)
class UGSFindPendingMatchesRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MatchGroup;                                               // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // 0x0050(0x0010) (ZeroConstructor)
	int                                                MaxMatchesToFind;                                         // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSFindPendingMatchesRequest");
		return ptr;
	}


	class UGSFindPendingMatchesRequest* STATIC_SendFindPendingMatchesRequest(const struct FString& MatchGroup, const struct FString& MatchShortCode, int MaxMatchesToFind, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGameCenterConnectRequest
// 0x0088 (0x00B8 - 0x0030)
class UGSGameCenterConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DisplayName;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	struct FString                                     ExternalPlayerId;                                         // 0x0058(0x0010) (ZeroConstructor)
	struct FString                                     PublicKeyUrl;                                             // 0x0068(0x0010) (ZeroConstructor)
	struct FString                                     Salt;                                                     // 0x0078(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0088(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     Signature;                                                // 0x0090(0x0010) (ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // 0x00A0(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x00A1(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x2];                                       // 0x00A2(0x0002) MISSED OFFSET
	int                                                Timestamp;                                                // 0x00A4(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x00A8(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00B0(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x00B1(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00B4(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGameCenterConnectRequest");
		return ptr;
	}


	class UGSGameCenterConnectRequest* STATIC_SendGameCenterConnectRequest(const struct FString& DisplayName, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& ExternalPlayerId, const struct FString& PublicKeyUrl, const struct FString& Salt, class UGameSparksScriptData* Segments, const struct FString& Signature, bool SwitchIfPossible, bool SyncDisplayName, int Timestamp, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetChallengeRequest
// 0x0040 (0x0070 - 0x0030)
class UGSGetChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Message;                                                  // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetChallengeRequest");
		return ptr;
	}


	class UGSGetChallengeRequest* STATIC_SendGetChallengeRequest(const struct FString& ChallengeInstanceId, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetDownloadableRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetDownloadableRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ShortCode;                                                // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetDownloadableRequest");
		return ptr;
	}


	class UGSGetDownloadableRequest* STATIC_SendGetDownloadableRequest(const struct FString& ShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetLeaderboardEntriesRequest
// 0x0058 (0x0088 - 0x0030)
class UGSGetLeaderboardEntriesRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     Challenges;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0049(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     Leaderboards;                                             // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     Player;                                                   // 0x0058(0x0010) (ZeroConstructor)
	bool                                               Social;                                                   // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0069(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetLeaderboardEntriesRequest");
		return ptr;
	}


	class UGSGetLeaderboardEntriesRequest* STATIC_SendGetLeaderboardEntriesRequest(class UGameSparksRequestArray* Challenges, bool InverseSocial, class UGameSparksRequestArray* Leaderboards, const struct FString& Player, bool Social, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetMessageRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MessageId;                                                // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetMessageRequest");
		return ptr;
	}


	class UGSGetMessageRequest* STATIC_SendGetMessageRequest(const struct FString& MessageId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetMyTeamsRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetMyTeamsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               OwnedOnly;                                                // 0x0040(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0041(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetMyTeamsRequest");
		return ptr;
	}


	class UGSGetMyTeamsRequest* STATIC_SendGetMyTeamsRequest(bool OwnedOnly, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetPropertyRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetPropertyRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     PropertyShortCode;                                        // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetPropertyRequest");
		return ptr;
	}


	class UGSGetPropertyRequest* STATIC_SendGetPropertyRequest(const struct FString& PropertyShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetPropertySetRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetPropertySetRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     PropertySetShortCode;                                     // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetPropertySetRequest");
		return ptr;
	}


	class UGSGetPropertySetRequest* STATIC_SendGetPropertySetRequest(const struct FString& PropertySetShortCode, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetTeamRequest
// 0x0050 (0x0080 - 0x0030)
class UGSGetTeamRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     OwnerId;                                                  // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetTeamRequest");
		return ptr;
	}


	class UGSGetTeamRequest* STATIC_SendGetTeamRequest(const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetUploadUrlRequest
// 0x0028 (0x0058 - 0x0030)
class UGSGetUploadUrlRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       UploadData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetUploadUrlRequest");
		return ptr;
	}


	class UGSGetUploadUrlRequest* STATIC_SendGetUploadUrlRequest(class UGameSparksScriptData* UploadData, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGetUploadedRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetUploadedRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     UploadId;                                                 // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGetUploadedRequest");
		return ptr;
	}


	class UGSGetUploadedRequest* STATIC_SendGetUploadedRequest(const struct FString& UploadId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGooglePlayBuyGoodsRequest
// 0x0058 (0x0088 - 0x0030)
class UGSGooglePlayBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     CurrencyCode;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Signature;                                                // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     SignedData;                                               // 0x0060(0x0010) (ZeroConstructor)
	float                                              SubUnitPrice;                                             // 0x0070(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0074(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0075(0x0003) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGooglePlayBuyGoodsRequest");
		return ptr;
	}


	class UGSGooglePlayBuyGoodsRequest* STATIC_SendGooglePlayBuyGoodsRequest(const struct FString& CurrencyCode, const struct FString& Signature, const struct FString& SignedData, float SubUnitPrice, bool UniqueTransactionByPlayer, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGooglePlayConnectRequest
// 0x0078 (0x00A8 - 0x0030)
class UGSGooglePlayConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Code;                                                     // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     DisplayName;                                              // 0x0060(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               GooglePlusScope;                                          // 0x0072(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ProfileScope;                                             // 0x0073(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0074(0x0004) MISSED OFFSET
	struct FString                                     RedirectUri;                                              // 0x0078(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0088(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0090(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0091(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0092(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0098(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00A0(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x00A1(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00A4(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGooglePlayConnectRequest");
		return ptr;
	}


	class UGSGooglePlayConnectRequest* STATIC_SendGooglePlayConnectRequest(const struct FString& AccessToken, const struct FString& Code, const struct FString& DisplayName, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, bool GooglePlusScope, bool ProfileScope, const struct FString& RedirectUri, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSGooglePlusConnectRequest
// 0x0068 (0x0098 - 0x0030)
class UGSGooglePlusConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Code;                                                     // 0x0050(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	struct FString                                     RedirectUri;                                              // 0x0068(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0081(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0082(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0088(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0090(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0091(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0094(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSGooglePlusConnectRequest");
		return ptr;
	}


	class UGSGooglePlusConnectRequest* STATIC_SendGooglePlusConnectRequest(const struct FString& AccessToken, const struct FString& Code, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& RedirectUri, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSIOSBuyGoodsRequest
// 0x0050 (0x0080 - 0x0030)
class UGSIOSBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     CurrencyCode;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Receipt;                                                  // 0x0050(0x0010) (ZeroConstructor)
	bool                                               Sandbox;                                                  // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0061(0x0003) MISSED OFFSET
	float                                              SubUnitPrice;                                             // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0069(0x0007) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSIOSBuyGoodsRequest");
		return ptr;
	}


	class UGSIOSBuyGoodsRequest* STATIC_SendIOSBuyGoodsRequest(const struct FString& CurrencyCode, const struct FString& Receipt, bool Sandbox, float SubUnitPrice, bool UniqueTransactionByPlayer, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSJoinChallengeRequest
// 0x0048 (0x0078 - 0x0030)
class UGSJoinChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Eligibility;                                              // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // 0x0058(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSJoinChallengeRequest");
		return ptr;
	}


	class UGSJoinChallengeRequest* STATIC_SendJoinChallengeRequest(const struct FString& ChallengeInstanceId, class UGameSparksScriptData* Eligibility, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSJoinPendingMatchRequest
// 0x0050 (0x0080 - 0x0030)
class UGSJoinPendingMatchRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MatchGroup;                                               // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     PendingMatchId;                                           // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSJoinPendingMatchRequest");
		return ptr;
	}


	class UGSJoinPendingMatchRequest* STATIC_SendJoinPendingMatchRequest(const struct FString& MatchGroup, const struct FString& MatchShortCode, const struct FString& PendingMatchId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSJoinTeamRequest
// 0x0050 (0x0080 - 0x0030)
class UGSJoinTeamRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     OwnerId;                                                  // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSJoinTeamRequest");
		return ptr;
	}


	class UGSJoinTeamRequest* STATIC_SendJoinTeamRequest(const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSKongregateConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSKongregateConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0040(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0041(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0042(0x0006) MISSED OFFSET
	struct FString                                     GameAuthToken;                                            // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	struct FString                                     UserId;                                                   // 0x0068(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSKongregateConnectRequest");
		return ptr;
	}


	class UGSKongregateConnectRequest* STATIC_SendKongregateConnectRequest(bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& GameAuthToken, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, const struct FString& UserId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSLeaderboardDataRequest
// 0x0078 (0x00A8 - 0x0030)
class UGSLeaderboardDataRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DontErrorOnNotSocial;                                     // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                EntryCount;                                               // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0069(0x0007) MISSED OFFSET
	struct FString                                     LeaderboardShortCode;                                     // 0x0070(0x0010) (ZeroConstructor)
	int                                                Offset;                                                   // 0x0080(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               Social;                                                   // 0x0084(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0085(0x0003) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamIds;                                                  // 0x0088(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0098(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00A0(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x3];                                       // 0x00A1(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00A4(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSLeaderboardDataRequest");
		return ptr;
	}


	class UGSLeaderboardDataRequest* STATIC_SendLeaderboardDataRequest(const struct FString& ChallengeInstanceId, bool DontErrorOnNotSocial, int EntryCount, class UGameSparksRequestArray* FriendIds, int IncludeFirst, int IncludeLast, bool InverseSocial, const struct FString& LeaderboardShortCode, int Offset, bool Social, class UGameSparksRequestArray* TeamIds, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSLeaderboardsEntriesRequest
// 0x0058 (0x0088 - 0x0030)
class UGSLeaderboardsEntriesRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     Challenges;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0049(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     Leaderboards;                                             // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     Player;                                                   // 0x0058(0x0010) (ZeroConstructor)
	bool                                               Social;                                                   // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0069(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSLeaderboardsEntriesRequest");
		return ptr;
	}


	class UGSLeaderboardsEntriesRequest* STATIC_SendLeaderboardsEntriesRequest(class UGameSparksRequestArray* Challenges, bool InverseSocial, class UGameSparksRequestArray* Leaderboards, const struct FString& Player, bool Social, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSLeaveTeamRequest
// 0x0050 (0x0080 - 0x0030)
class UGSLeaveTeamRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     OwnerId;                                                  // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSLeaveTeamRequest");
		return ptr;
	}


	class UGSLeaveTeamRequest* STATIC_SendLeaveTeamRequest(const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListAchievementsRequest
// 0x0020 (0x0050 - 0x0030)
class UGSListAchievementsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListAchievementsRequest");
		return ptr;
	}


	class UGSListAchievementsRequest* STATIC_SendListAchievementsRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListBulkJobsAdminRequest
// 0x0028 (0x0058 - 0x0030)
class UGSListBulkJobsAdminRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     BulkJobIds;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListBulkJobsAdminRequest");
		return ptr;
	}


	class UGSListBulkJobsAdminRequest* STATIC_SendListBulkJobsAdminRequest(class UGameSparksRequestArray* BulkJobIds, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListChallengeRequest
// 0x0050 (0x0080 - 0x0030)
class UGSListChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // 0x0044(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FString                                     ShortCode;                                                // 0x0048(0x0010) (ZeroConstructor)
	struct FString                                     State;                                                    // 0x0058(0x0010) (ZeroConstructor)
	class UGameSparksRequestArray*                     States;                                                   // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListChallengeRequest");
		return ptr;
	}


	class UGSListChallengeRequest* STATIC_SendListChallengeRequest(int EntryCount, int Offset, const struct FString& ShortCode, const struct FString& State, class UGameSparksRequestArray* States, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListChallengeTypeRequest
// 0x0020 (0x0050 - 0x0030)
class UGSListChallengeTypeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListChallengeTypeRequest");
		return ptr;
	}


	class UGSListChallengeTypeRequest* STATIC_SendListChallengeTypeRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListGameFriendsRequest
// 0x0020 (0x0050 - 0x0030)
class UGSListGameFriendsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListGameFriendsRequest");
		return ptr;
	}


	class UGSListGameFriendsRequest* STATIC_SendListGameFriendsRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListInviteFriendsRequest
// 0x0020 (0x0050 - 0x0030)
class UGSListInviteFriendsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListInviteFriendsRequest");
		return ptr;
	}


	class UGSListInviteFriendsRequest* STATIC_SendListInviteFriendsRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListLeaderboardsRequest
// 0x0020 (0x0050 - 0x0030)
class UGSListLeaderboardsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListLeaderboardsRequest");
		return ptr;
	}


	class UGSListLeaderboardsRequest* STATIC_SendListLeaderboardsRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListMessageDetailRequest
// 0x0050 (0x0080 - 0x0030)
class UGSListMessageDetailRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0044(0x0004) MISSED OFFSET
	struct FString                                     Include;                                                  // 0x0048(0x0010) (ZeroConstructor)
	int                                                Offset;                                                   // 0x0058(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x005C(0x0004) MISSED OFFSET
	struct FString                                     Status;                                                   // 0x0060(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0078(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0079(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListMessageDetailRequest");
		return ptr;
	}


	class UGSListMessageDetailRequest* STATIC_SendListMessageDetailRequest(int EntryCount, const struct FString& Include, int Offset, const struct FString& Status, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListMessageRequest
// 0x0040 (0x0070 - 0x0030)
class UGSListMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0044(0x0004) MISSED OFFSET
	struct FString                                     Include;                                                  // 0x0048(0x0010) (ZeroConstructor)
	int                                                Offset;                                                   // 0x0058(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x005C(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListMessageRequest");
		return ptr;
	}


	class UGSListMessageRequest* STATIC_SendListMessageRequest(int EntryCount, const struct FString& Include, int Offset, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListMessageSummaryRequest
// 0x0028 (0x0058 - 0x0030)
class UGSListMessageSummaryRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // 0x0044(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListMessageSummaryRequest");
		return ptr;
	}


	class UGSListMessageSummaryRequest* STATIC_SendListMessageSummaryRequest(int EntryCount, int Offset, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListTeamChatRequest
// 0x0058 (0x0088 - 0x0030)
class UGSListTeamChatRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // 0x0044(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FString                                     OwnerId;                                                  // 0x0048(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0058(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0068(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListTeamChatRequest");
		return ptr;
	}


	class UGSListTeamChatRequest* STATIC_SendListTeamChatRequest(int EntryCount, int Offset, const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListTeamsRequest
// 0x0048 (0x0078 - 0x0030)
class UGSListTeamsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                EntryCount;                                               // 0x0040(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // 0x0044(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FString                                     TeamNameFilter;                                           // 0x0048(0x0010) (ZeroConstructor)
	struct FString                                     TeamTypeFilter;                                           // 0x0058(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListTeamsRequest");
		return ptr;
	}


	class UGSListTeamsRequest* STATIC_SendListTeamsRequest(int EntryCount, int Offset, const struct FString& TeamNameFilter, const struct FString& TeamTypeFilter, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListTransactionsRequest
// 0x0060 (0x0090 - 0x0030)
class UGSListTransactionsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DateFrom;                                                 // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     DateTo;                                                   // 0x0050(0x0010) (ZeroConstructor)
	int                                                EntryCount;                                               // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
	struct FString                                     Include;                                                  // 0x0068(0x0010) (ZeroConstructor)
	int                                                Offset;                                                   // 0x0078(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x007C(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0080(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0088(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0089(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x008C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListTransactionsRequest");
		return ptr;
	}


	class UGSListTransactionsRequest* STATIC_SendListTransactionsRequest(const struct FString& DateFrom, const struct FString& DateTo, int EntryCount, const struct FString& Include, int Offset, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSListVirtualGoodsRequest
// 0x0030 (0x0060 - 0x0030)
class UGSListVirtualGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               IncludeDisabled;                                          // 0x0040(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0041(0x0007) MISSED OFFSET
	class UGameSparksRequestArray*                     Tags;                                                     // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSListVirtualGoodsRequest");
		return ptr;
	}


	class UGSListVirtualGoodsRequest* STATIC_SendListVirtualGoodsRequest(bool IncludeDisabled, class UGameSparksRequestArray* Tags, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSLogChallengeEventRequest
// 0x0040 (0x0070 - 0x0030)
class UGSLogChallengeEventRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     EventKey;                                                 // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksLogEventData*                     LogEventData;                                             // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSLogChallengeEventRequest");
		return ptr;
	}


	class UGSLogChallengeEventRequest* STATIC_SendLogChallengeEventRequest(const struct FString& ChallengeInstanceId, const struct FString& EventKey, class UGameSparksLogEventData* LogEventData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSLogEventRequest
// 0x0030 (0x0060 - 0x0030)
class UGSLogEventRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     EventKey;                                                 // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksLogEventData*                     LogEventData;                                             // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSLogEventRequest");
		return ptr;
	}


	class UGSLogEventRequest* STATIC_SendLogEventRequest(const struct FString& EventKey, class UGameSparksLogEventData* LogEventData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSMatchDetailsRequest
// 0x0038 (0x0068 - 0x0030)
class UGSMatchDetailsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MatchId;                                                  // 0x0040(0x0010) (ZeroConstructor)
	bool                                               RealtimeEnabled;                                          // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0051(0x0007) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0061(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSMatchDetailsRequest");
		return ptr;
	}


	class UGSMatchDetailsRequest* STATIC_SendMatchDetailsRequest(const struct FString& MatchId, bool RealtimeEnabled, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSMatchmakingRequest
// 0x0070 (0x00A0 - 0x0030)
class UGSMatchmakingRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     Action;                                                   // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       CustomQuery;                                              // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       MatchData;                                                // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     MatchGroup;                                               // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // 0x0070(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ParticipantData;                                          // 0x0080(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                Skill;                                                    // 0x0088(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x008C(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0098(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0099(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x009C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSMatchmakingRequest");
		return ptr;
	}


	class UGSMatchmakingRequest* STATIC_SendMatchmakingRequest(const struct FString& Action, class UGameSparksScriptData* CustomQuery, class UGameSparksScriptData* MatchData, const struct FString& MatchGroup, const struct FString& MatchShortCode, class UGameSparksScriptData* ParticipantData, int Skill, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSPSNConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSPSNConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AuthorizationCode;                                        // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	struct FString                                     RedirectUri;                                              // 0x0058(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0072(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSPSNConnectRequest");
		return ptr;
	}


	class UGSPSNConnectRequest* STATIC_SendPSNConnectRequest(const struct FString& AuthorizationCode, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& RedirectUri, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSPsnBuyGoodsRequest
// 0x0070 (0x00A0 - 0x0030)
class UGSPsnBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AuthorizationCode;                                        // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     CurrencyCode;                                             // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     EntitlementLabel;                                         // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     RedirectUri;                                              // 0x0070(0x0010) (ZeroConstructor)
	float                                              SubUnitPrice;                                             // 0x0080(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0084(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0085(0x0003) MISSED OFFSET
	int                                                UseCount;                                                 // 0x0088(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x008C(0x0004) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0098(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0099(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x009C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSPsnBuyGoodsRequest");
		return ptr;
	}


	class UGSPsnBuyGoodsRequest* STATIC_SendPsnBuyGoodsRequest(const struct FString& AuthorizationCode, const struct FString& CurrencyCode, const struct FString& EntitlementLabel, const struct FString& RedirectUri, float SubUnitPrice, bool UniqueTransactionByPlayer, int UseCount, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSPushRegistrationRequest
// 0x0040 (0x0070 - 0x0030)
class UGSPushRegistrationRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DeviceOS;                                                 // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     PushId;                                                   // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSPushRegistrationRequest");
		return ptr;
	}


	class UGSPushRegistrationRequest* STATIC_SendPushRegistrationRequest(const struct FString& DeviceOS, const struct FString& PushId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSQQConnectRequest
// 0x0048 (0x0078 - 0x0030)
class UGSQQConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSQQConnectRequest");
		return ptr;
	}


	class UGSQQConnectRequest* STATIC_SendQQConnectRequest(const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSRegistrationRequest
// 0x0058 (0x0088 - 0x0030)
class UGSRegistrationRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DisplayName;                                              // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Password;                                                 // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     UserName;                                                 // 0x0068(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSRegistrationRequest");
		return ptr;
	}


	class UGSRegistrationRequest* STATIC_SendRegistrationRequest(const struct FString& DisplayName, const struct FString& Password, class UGameSparksScriptData* Segments, const struct FString& UserName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSRevokePurchaseGoodsRequest
// 0x0048 (0x0078 - 0x0030)
class UGSRevokePurchaseGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     PlayerId;                                                 // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     StoreType;                                                // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksRequestArray*                     TransactionIds;                                           // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSRevokePurchaseGoodsRequest");
		return ptr;
	}


	class UGSRevokePurchaseGoodsRequest* STATIC_SendRevokePurchaseGoodsRequest(const struct FString& PlayerId, const struct FString& StoreType, class UGameSparksRequestArray* TransactionIds, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSScheduleBulkJobAdminRequest
// 0x0060 (0x0090 - 0x0030)
class UGSScheduleBulkJobAdminRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       Data;                                                     // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     ModuleShortCode;                                          // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       PlayerQuery;                                              // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     ScheduledTime;                                            // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     Script;                                                   // 0x0070(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0080(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0088(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0089(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x008C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSScheduleBulkJobAdminRequest");
		return ptr;
	}


	class UGSScheduleBulkJobAdminRequest* STATIC_SendScheduleBulkJobAdminRequest(class UGameSparksScriptData* Data, const struct FString& ModuleShortCode, class UGameSparksScriptData* PlayerQuery, const struct FString& ScheduledTime, const struct FString& Script, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSendFriendMessageRequest
// 0x0038 (0x0068 - 0x0030)
class UGSSendFriendMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksRequestArray*                     FriendIds;                                                // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0061(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSendFriendMessageRequest");
		return ptr;
	}


	class UGSSendFriendMessageRequest* STATIC_SendSendFriendMessageRequest(class UGameSparksRequestArray* FriendIds, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSendTeamChatMessageRequest
// 0x0060 (0x0090 - 0x0030)
class UGSSendTeamChatMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     Message;                                                  // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     OwnerId;                                                  // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     TeamId;                                                   // 0x0060(0x0010) (ZeroConstructor)
	struct FString                                     TeamType;                                                 // 0x0070(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0080(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0088(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0089(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x008C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSendTeamChatMessageRequest");
		return ptr;
	}


	class UGSSendTeamChatMessageRequest* STATIC_SendSendTeamChatMessageRequest(const struct FString& Message, const struct FString& OwnerId, const struct FString& TeamId, const struct FString& TeamType, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSocialDisconnectRequest
// 0x0030 (0x0060 - 0x0030)
class UGSSocialDisconnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     SystemId;                                                 // 0x0040(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0050(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0058(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0059(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSocialDisconnectRequest");
		return ptr;
	}


	class UGSSocialDisconnectRequest* STATIC_SendSocialDisconnectRequest(const struct FString& SystemId, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSocialLeaderboardDataRequest
// 0x0078 (0x00A8 - 0x0030)
class UGSSocialLeaderboardDataRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DontErrorOnNotSocial;                                     // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0051(0x0003) MISSED OFFSET
	int                                                EntryCount;                                               // 0x0054(0x0004) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0069(0x0007) MISSED OFFSET
	struct FString                                     LeaderboardShortCode;                                     // 0x0070(0x0010) (ZeroConstructor)
	int                                                Offset;                                                   // 0x0080(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               Social;                                                   // 0x0084(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0085(0x0003) MISSED OFFSET
	class UGameSparksRequestArray*                     TeamIds;                                                  // 0x0088(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // 0x0090(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0098(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x00A0(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x3];                                       // 0x00A1(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x00A4(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSocialLeaderboardDataRequest");
		return ptr;
	}


	class UGSSocialLeaderboardDataRequest* STATIC_SendSocialLeaderboardDataRequest(const struct FString& ChallengeInstanceId, bool DontErrorOnNotSocial, int EntryCount, class UGameSparksRequestArray* FriendIds, int IncludeFirst, int IncludeLast, bool InverseSocial, const struct FString& LeaderboardShortCode, int Offset, bool Social, class UGameSparksRequestArray* TeamIds, class UGameSparksRequestArray* TeamTypes, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSocialStatusRequest
// 0x0020 (0x0050 - 0x0030)
class UGSSocialStatusRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0040(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0048(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x004C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSocialStatusRequest");
		return ptr;
	}


	class UGSSocialStatusRequest* STATIC_SendSocialStatusRequest(class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSteamBuyGoodsRequest
// 0x0048 (0x0078 - 0x0030)
class UGSSteamBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     CurrencyCode;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     OrderId;                                                  // 0x0050(0x0010) (ZeroConstructor)
	float                                              SubUnitPrice;                                             // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0064(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0065(0x0003) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSteamBuyGoodsRequest");
		return ptr;
	}


	class UGSSteamBuyGoodsRequest* STATIC_SendSteamBuyGoodsRequest(const struct FString& CurrencyCode, const struct FString& OrderId, float SubUnitPrice, bool UniqueTransactionByPlayer, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSSteamConnectRequest
// 0x0048 (0x0078 - 0x0030)
class UGSSteamConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0040(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0041(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0042(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0048(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     SessionTicket;                                            // 0x0050(0x0010) (ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSSteamConnectRequest");
		return ptr;
	}


	class UGSSteamConnectRequest* STATIC_SendSteamConnectRequest(bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, const struct FString& SessionTicket, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSTwitchConnectRequest
// 0x0048 (0x0078 - 0x0030)
class UGSTwitchConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSTwitchConnectRequest");
		return ptr;
	}


	class UGSTwitchConnectRequest* STATIC_SendTwitchConnectRequest(const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSTwitterConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSTwitterConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessSecret;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     AccessToken;                                              // 0x0050(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0072(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSTwitterConnectRequest");
		return ptr;
	}


	class UGSTwitterConnectRequest* STATIC_SendTwitterConnectRequest(const struct FString& AccessSecret, const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSUpdateMessageRequest
// 0x0040 (0x0070 - 0x0030)
class UGSUpdateMessageRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     MessageId;                                                // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Status;                                                   // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSUpdateMessageRequest");
		return ptr;
	}


	class UGSUpdateMessageRequest* STATIC_SendUpdateMessageRequest(const struct FString& MessageId, const struct FString& Status, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSViberConnectRequest
// 0x0048 (0x0078 - 0x0030)
class UGSViberConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               DoNotRegisterForPush;                                     // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0052(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x5];                                       // 0x0053(0x0005) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0071(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0074(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSViberConnectRequest");
		return ptr;
	}


	class UGSViberConnectRequest* STATIC_SendViberConnectRequest(const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool DoNotRegisterForPush, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSWeChatConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSWeChatConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     AccessToken;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	struct FString                                     OpenId;                                                   // 0x0058(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0072(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSWeChatConnectRequest");
		return ptr;
	}


	class UGSWeChatConnectRequest* STATIC_SendWeChatConnectRequest(const struct FString& AccessToken, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& OpenId, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSWindowsBuyGoodsRequest
// 0x0058 (0x0088 - 0x0030)
class UGSWindowsBuyGoodsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     CurrencyCode;                                             // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Platform;                                                 // 0x0050(0x0010) (ZeroConstructor)
	struct FString                                     Receipt;                                                  // 0x0060(0x0010) (ZeroConstructor)
	float                                              SubUnitPrice;                                             // 0x0070(0x0004) (ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // 0x0074(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0075(0x0003) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSWindowsBuyGoodsRequest");
		return ptr;
	}


	class UGSWindowsBuyGoodsRequest* STATIC_SendWindowsBuyGoodsRequest(const struct FString& CurrencyCode, const struct FString& Platform, const struct FString& Receipt, float SubUnitPrice, bool UniqueTransactionByPlayer, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSWithdrawChallengeRequest
// 0x0040 (0x0070 - 0x0030)
class UGSWithdrawChallengeRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     ChallengeInstanceId;                                      // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     Message;                                                  // 0x0050(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0060(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0068(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0069(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x006C(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSWithdrawChallengeRequest");
		return ptr;
	}


	class UGSWithdrawChallengeRequest* STATIC_SendWithdrawChallengeRequest(const struct FString& ChallengeInstanceId, const struct FString& Message, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSXBOXLiveConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSXBOXLiveConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FString                                     DisplayName;                                              // 0x0040(0x0010) (ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0050(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0051(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0052(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     StsTokenString;                                           // 0x0060(0x0010) (ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // 0x0070(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0071(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0072(0x0006) MISSED OFFSET
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSXBOXLiveConnectRequest");
		return ptr;
	}


	class UGSXBOXLiveConnectRequest* STATIC_SendXBOXLiveConnectRequest(const struct FString& DisplayName, bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, class UGameSparksScriptData* Segments, const struct FString& StsTokenString, bool SwitchIfPossible, bool SyncDisplayName, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSXboxOneConnectRequest
// 0x0058 (0x0088 - 0x0030)
class UGSXboxOneConnectRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               DoNotLinkToCurrentPlayer;                                 // 0x0040(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // 0x0041(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x0042(0x0006) MISSED OFFSET
	struct FString                                     Sandbox;                                                  // 0x0048(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // 0x0058(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // 0x0060(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // 0x0061(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0062(0x0006) MISSED OFFSET
	struct FString                                     token;                                                    // 0x0068(0x0010) (ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0078(0x0008) (ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // 0x0080(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0081(0x0003) MISSED OFFSET
	int                                                RequestTimeoutSeconds;                                    // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSXboxOneConnectRequest");
		return ptr;
	}


	class UGSXboxOneConnectRequest* STATIC_SendXboxOneConnectRequest(bool DoNotLinkToCurrentPlayer, bool ErrorOnSwitch, const struct FString& Sandbox, class UGameSparksScriptData* Segments, bool SwitchIfPossible, bool SyncDisplayName, const struct FString& token, class UGameSparksScriptData* ScriptData, bool Durable, int RequestTimeoutSeconds);
};


// Class GameSparks.GSMessageListeners
// 0x01D0 (0x0470 - 0x02A0)
class UGSMessageListeners : public USceneComponent
{
public:
	struct FScriptMulticastDelegate                    OnAchievementEarnedMessage;                               // 0x02A0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeAcceptedMessage;                               // 0x02B0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeChangedMessage;                                // 0x02C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeChatMessage;                                   // 0x02D0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeDeclinedMessage;                               // 0x02E0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeDrawnMessage;                                  // 0x02F0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeExpiredMessage;                                // 0x0300(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeIssuedMessage;                                 // 0x0310(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeJoinedMessage;                                 // 0x0320(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeLapsedMessage;                                 // 0x0330(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeLostMessage;                                   // 0x0340(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeStartedMessage;                                // 0x0350(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeTurnTakenMessage;                              // 0x0360(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeWaitingMessage;                                // 0x0370(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeWithdrawnMessage;                              // 0x0380(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnChallengeWonMessage;                                    // 0x0390(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnFriendMessage;                                          // 0x03A0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGlobalRankChangedMessage;                               // 0x03B0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnMatchFoundMessage;                                      // 0x03C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnMatchNotFoundMessage;                                   // 0x03D0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnMatchUpdatedMessage;                                    // 0x03E0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnNewHighScoreMessage;                                    // 0x03F0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnNewTeamScoreMessage;                                    // 0x0400(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnScriptMessage;                                          // 0x0410(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSessionTerminatedMessage;                               // 0x0420(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSocialRankChangedMessage;                               // 0x0430(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnTeamChatMessage;                                        // 0x0440(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnTeamRankChangedMessage;                                 // 0x0450(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnUploadCompleteMessage;                                  // 0x0460(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSMessageListeners");
		return ptr;
	}

};


// Class GameSparks.GSRTData
// 0x1800 (0x1830 - 0x0030)
class UGSRTData : public UObject
{
public:
	unsigned char                                      UnknownData00[0x1800];                                    // 0x0030(0x1800) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSRTData");
		return ptr;
	}


	struct FString ToString();
	class UGSRTData* SetVector(int Index, class UGSRTVector* Value);
	class UGSRTData* SetString(int Index, const struct FString& Value);
	class UGSRTData* SetInt(int Index, int Value);
	class UGSRTData* SetFVector(int Index, const struct FVector& Value);
	class UGSRTData* SetFloat(int Index, float Value);
	class UGSRTData* SetData(int Index, class UGSRTData* Value);
	bool HasVector(int Index);
	bool HasString(int Index);
	bool HasInt(int Index);
	bool HasFloat(int Index);
	bool HasData(int Index);
	class UGSRTVector* GetVector(int Index);
	struct FString GetString(int Index);
	int GetInt(int Index);
	struct FVector GetFVector(int Index);
	float GetFloat(int Index);
	class UGSRTData* GetData(int Index);
	class UGSRTData* STATIC_CreateRTData(class UObject* WorldContextObject);
};


// Class GameSparks.GSRTSession
// 0x00B8 (0x00E8 - 0x0030)
class UGSRTSession : public UObject
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0030(0x0008) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnReadyDelegate;                                          // 0x0038(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnPlayerConnectDelegate;                                  // 0x0048(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnPlayerDisconnectDelegate;                               // 0x0058(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnDataDelegate;                                           // 0x0068(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               IsReady;                                                  // 0x0078(0x0001) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6F];                                      // 0x0079(0x006F) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSRTSession");
		return ptr;
	}


	void Stop();
	void Start();
	void Send(int opCode, EDeliveryIntent intent, class UGSRTData* Data, TArray<int> peerIds);
	void OnReady__DelegateSignature(class UGSRTSession* session, bool ready);
	void OnPlayerDisconnect__DelegateSignature(class UGSRTSession* session, int PeerId);
	void OnPlayerConnect__DelegateSignature(class UGSRTSession* session, int PeerId);
	void OnData__DelegateSignature(class UGSRTSession* session, int sender, int opCode, class UGSRTData* Data);
	int GetPeerId();
	TArray<int> GetActivePeers();
	class UGSRTSession* STATIC_CreateRTSession(class UObject* WorldContextObject, const struct FString& Host, const struct FString& Port, const struct FString& token);
};


// Class GameSparks.GSRTVector
// 0x0020 (0x0050 - 0x0030)
class UGSRTVector : public UObject
{
public:
	unsigned char                                      UnknownData00[0x20];                                      // 0x0030(0x0020) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class GameSparks.GSRTVector");
		return ptr;
	}


	class UGSRTVector* SetZ(float Z);
	class UGSRTVector* SetY(float Y);
	class UGSRTVector* SetXYZW(float X, float Y, float Z, float W);
	class UGSRTVector* SetXYZ(float X, float Y, float Z);
	class UGSRTVector* SetXY(float X, float Y);
	class UGSRTVector* SetX(float X);
	class UGSRTVector* SetW(float W);
	class UGSRTVector* SetFromFVector(const struct FVector& V);
	bool HasZ();
	bool HasY();
	bool HasX();
	bool HasW();
	float GetZ();
	float GetY();
	float GetX();
	float GetW();
	class UGSRTVector* STATIC_CreateRTVector(class UObject* WorldContextObject);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
