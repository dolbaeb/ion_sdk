// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function SocketIOClient.SocketIOClientComponent.EmitWithCallBack
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 EventName                      (Parm, ZeroConstructor)
// class USIOJsonValue*           Message                        (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 CallbackFunctionName           (Parm, ZeroConstructor)
// class UObject*                 Target                         (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Namespace                      (Parm, ZeroConstructor)

void USocketIOClientComponent::EmitWithCallBack(const struct FString& EventName, class USIOJsonValue* Message, const struct FString& CallbackFunctionName, class UObject* Target, const struct FString& Namespace)
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.EmitWithCallBack");

	USocketIOClientComponent_EmitWithCallBack_Params params;
	params.EventName = EventName;
	params.Message = Message;
	params.CallbackFunctionName = CallbackFunctionName;
	params.Target = Target;
	params.Namespace = Namespace;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SocketIOClient.SocketIOClientComponent.Emit
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 EventName                      (Parm, ZeroConstructor)
// class USIOJsonValue*           Message                        (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Namespace                      (Parm, ZeroConstructor)

void USocketIOClientComponent::Emit(const struct FString& EventName, class USIOJsonValue* Message, const struct FString& Namespace)
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.Emit");

	USocketIOClientComponent_Emit_Params params;
	params.EventName = EventName;
	params.Message = Message;
	params.Namespace = Namespace;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SocketIOClient.SocketIOClientComponent.Disconnect
// (Final, Native, Public, BlueprintCallable)

void USocketIOClientComponent::Disconnect()
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.Disconnect");

	USocketIOClientComponent_Disconnect_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SocketIOClient.SocketIOClientComponent.Connect
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 InAddressAndPort               (Parm, ZeroConstructor)
// class USIOJsonObject*          Query                          (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonObject*          Headers                        (Parm, ZeroConstructor, IsPlainOldData)

void USocketIOClientComponent::Connect(const struct FString& InAddressAndPort, class USIOJsonObject* Query, class USIOJsonObject* Headers)
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.Connect");

	USocketIOClientComponent_Connect_Params params;
	params.InAddressAndPort = InAddressAndPort;
	params.Query = Query;
	params.Headers = Headers;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SocketIOClient.SocketIOClientComponent.BindEventToFunction
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 EventName                      (Parm, ZeroConstructor)
// struct FString                 FunctionName                   (Parm, ZeroConstructor)
// class UObject*                 Target                         (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Namespace                      (Parm, ZeroConstructor)

void USocketIOClientComponent::BindEventToFunction(const struct FString& EventName, const struct FString& FunctionName, class UObject* Target, const struct FString& Namespace)
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.BindEventToFunction");

	USocketIOClientComponent_BindEventToFunction_Params params;
	params.EventName = EventName;
	params.FunctionName = FunctionName;
	params.Target = Target;
	params.Namespace = Namespace;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SocketIOClient.SocketIOClientComponent.BindEvent
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 EventName                      (Parm, ZeroConstructor)
// struct FString                 Namespace                      (Parm, ZeroConstructor)

void USocketIOClientComponent::BindEvent(const struct FString& EventName, const struct FString& Namespace)
{
	static auto fn = UObject::FindObject<UFunction>("Function SocketIOClient.SocketIOClientComponent.BindEvent");

	USocketIOClientComponent_BindEvent_Params params;
	params.EventName = EventName;
	params.Namespace = Namespace;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
