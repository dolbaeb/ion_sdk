#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Controls_C_BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.ExecuteUbergraph_WBP_SettingsPage_Controls
struct UWBP_SettingsPage_Controls_C_ExecuteUbergraph_WBP_SettingsPage_Controls_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
