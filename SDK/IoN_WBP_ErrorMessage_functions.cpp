// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ErrorMessage.WBP_ErrorMessage_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_ErrorMessage_C::BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ErrorMessage.WBP_ErrorMessage_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature");

	UWBP_ErrorMessage_C_BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_29_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ErrorMessage.WBP_ErrorMessage_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ErrorMessage_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ErrorMessage.WBP_ErrorMessage_C.Construct");

	UWBP_ErrorMessage_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ErrorMessage.WBP_ErrorMessage_C.ExecuteUbergraph_WBP_ErrorMessage
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ErrorMessage_C::ExecuteUbergraph_WBP_ErrorMessage(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ErrorMessage.WBP_ErrorMessage_C.ExecuteUbergraph_WBP_ErrorMessage");

	UWBP_ErrorMessage_C_ExecuteUbergraph_WBP_ErrorMessage_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
