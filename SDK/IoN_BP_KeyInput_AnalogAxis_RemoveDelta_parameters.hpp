#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_KeyInput_AnalogAxis_RemoveDelta.BP_KeyInput_AnalogAxis_RemoveDelta_C.Key Input Current State
struct UBP_KeyInput_AnalogAxis_RemoveDelta_C_Key_Input_Current_State_Params
{
	class APlayerController**                          Controller;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Axis_Value;                                               // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Down;                                                     // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
