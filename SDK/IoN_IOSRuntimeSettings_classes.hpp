#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class IOSRuntimeSettings.IOSRuntimeSettings
// 0x0190 (0x01C0 - 0x0030)
class UIOSRuntimeSettings : public UObject
{
public:
	unsigned char                                      bEnableGameCenterSupport : 1;                             // 0x0030(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bEnableCloudKitSupport : 1;                               // 0x0030(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bEnableRemoteNotificationsSupport : 1;                    // 0x0030(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0031(0x0003) MISSED OFFSET
	bool                                               bSupportsMetal;                                           // 0x0034(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bSupportsMetalMRT;                                        // 0x0035(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bCookPVRTCTextures;                                       // 0x0036(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bCookASTCTextures;                                        // 0x0037(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bSupportsOpenGLES2;                                       // 0x0038(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               EnableRemoteShaderCompile;                                // 0x0039(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bGeneratedSYMFile;                                        // 0x003A(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bGeneratedSYMBundle;                                      // 0x003B(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bGenerateCrashReportSymbols;                              // 0x003C(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bGenerateXCArchive;                                       // 0x003D(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bDevForArmV7;                                             // 0x003E(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bDevForArm64;                                             // 0x003F(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bDevForArmV7S;                                            // 0x0040(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bShipForArmV7;                                            // 0x0041(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bShipForArm64;                                            // 0x0042(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bShipForArmV7S;                                           // 0x0043(0x0001) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bShipForBitcode;                                          // 0x0044(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0045(0x0003) MISSED OFFSET
	struct FString                                     AdditionalLinkerFlags;                                    // 0x0048(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     AdditionalShippingLinkerFlags;                            // 0x0058(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     RemoteServerName;                                         // 0x0068(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	bool                                               bUseRSync;                                                // 0x0078(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0079(0x0007) MISSED OFFSET
	struct FString                                     RSyncUsername;                                            // 0x0080(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FIOSBuildResourceDirectory                  DeltaCopyInstallPath;                                     // 0x0090(0x0010) (Edit, Config, GlobalConfig)
	struct FString                                     SSHPrivateKeyLocation;                                    // 0x00A0(0x0010) (Edit, ZeroConstructor, EditConst)
	struct FIOSBuildResourceFilePath                   SSHPrivateKeyOverridePath;                                // 0x00B0(0x0010) (Edit, Config, GlobalConfig)
	bool                                               bTreatRemoteAsSeparateController;                         // 0x00C0(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bAllowRemoteRotation;                                     // 0x00C1(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bUseRemoteAsVirtualJoystick;                              // 0x00C2(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	bool                                               bUseRemoteAbsoluteDpadValues;                             // 0x00C3(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      bSupportsPortraitOrientation : 1;                         // 0x00C4(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bSupportsUpsideDownOrientation : 1;                       // 0x00C4(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bSupportsLandscapeLeftOrientation : 1;                    // 0x00C4(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bSupportsLandscapeRightOrientation : 1;                   // 0x00C4(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      UnknownData03[0x3];                                       // 0x00C5(0x0003) MISSED OFFSET
	struct FString                                     BundleDisplayName;                                        // 0x00C8(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     BundleName;                                               // 0x00D8(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     BundleIdentifier;                                         // 0x00E8(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     VersionInfo;                                              // 0x00F8(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	EPowerUsageFrameRateLock                           FrameRateLock;                                            // 0x0108(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	EIOSVersion                                        MinimumiOSVersion;                                        // 0x0109(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData04[0x2];                                       // 0x010A(0x0002) MISSED OFFSET
	unsigned char                                      bSupportsIPad : 1;                                        // 0x010C(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      bSupportsIPhone : 1;                                      // 0x010C(0x0001) (Edit, Config, GlobalConfig)
	unsigned char                                      UnknownData05[0x3];                                       // 0x010D(0x0003) MISSED OFFSET
	struct FString                                     AdditionalPlistData;                                      // 0x0110(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	bool                                               bEnableFacebookSupport;                                   // 0x0120(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData06[0x7];                                       // 0x0121(0x0007) MISSED OFFSET
	struct FString                                     FacebookAppID;                                            // 0x0128(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     MobileProvision;                                          // 0x0138(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	struct FString                                     SigningCertificate;                                       // 0x0148(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	bool                                               bAutomaticSigning;                                        // 0x0158(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData07[0x7];                                       // 0x0159(0x0007) MISSED OFFSET
	struct FString                                     IOSTeamID;                                                // 0x0160(0x0010) (Edit, ZeroConstructor, Config, GlobalConfig)
	bool                                               bDisableHTTPS;                                            // 0x0170(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      MaxShaderLanguageVersion;                                 // 0x0171(0x0001) (Edit, ZeroConstructor, Config, IsPlainOldData)
	bool                                               UseFastIntrinsics;                                        // 0x0172(0x0001) (Edit, ZeroConstructor, Config, IsPlainOldData)
	bool                                               EnableMathOptimisations;                                  // 0x0173(0x0001) (Edit, ZeroConstructor, Config, IsPlainOldData)
	bool                                               bUseIntegratedKeyboard;                                   // 0x0174(0x0001) (Edit, ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData08[0x3];                                       // 0x0175(0x0003) MISSED OFFSET
	int                                                AudioSampleRate;                                          // 0x0178(0x0004) (Edit, ZeroConstructor, Config, IsPlainOldData)
	int                                                AudioCallbackBufferFrameSize;                             // 0x017C(0x0004) (Edit, ZeroConstructor, Config, IsPlainOldData)
	int                                                AudioNumBuffersToEnqueue;                                 // 0x0180(0x0004) (Edit, ZeroConstructor, Config, IsPlainOldData)
	int                                                AudioMaxChannels;                                         // 0x0184(0x0004) (Edit, ZeroConstructor, Config, IsPlainOldData)
	int                                                AudioNumSourceWorkers;                                    // 0x0188(0x0004) (Edit, ZeroConstructor, Config, IsPlainOldData)
	unsigned char                                      UnknownData09[0x4];                                       // 0x018C(0x0004) MISSED OFFSET
	struct FString                                     SpatializationPlugin;                                     // 0x0190(0x0010) (Edit, ZeroConstructor, Config)
	struct FString                                     ReverbPlugin;                                             // 0x01A0(0x0010) (Edit, ZeroConstructor, Config)
	struct FString                                     OcclusionPlugin;                                          // 0x01B0(0x0010) (Edit, ZeroConstructor, Config)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IOSRuntimeSettings.IOSRuntimeSettings");
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
