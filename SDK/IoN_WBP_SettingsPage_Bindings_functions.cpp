// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.RefreshInputMappings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_SettingsPage_Bindings_C::RefreshInputMappings()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.RefreshInputMappings");

	UWBP_SettingsPage_Bindings_C_RefreshInputMappings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SettingsPage_Bindings_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.Construct");

	UWBP_SettingsPage_Bindings_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsPage_Bindings_C::BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature");

	UWBP_SettingsPage_Bindings_C_BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.ExecuteUbergraph_WBP_SettingsPage_Bindings
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Bindings_C::ExecuteUbergraph_WBP_SettingsPage_Bindings(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.ExecuteUbergraph_WBP_SettingsPage_Bindings");

	UWBP_SettingsPage_Bindings_C_ExecuteUbergraph_WBP_SettingsPage_Bindings_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
