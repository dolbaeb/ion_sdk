#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SAudioUpdateStruct.SAudioUpdateStruct
// 0x0010
struct FSAudioUpdateStruct
{
	class UAudioComponent*                             AudioEmittor_5_EC37367D413769EBC8347085400A4A09;          // 0x0000(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TEnumAsByte<EAudioType>                            AudioType_4_3CDAA07D4BB9A7FC4028B7A4994A497E;             // 0x0008(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0009(0x0003) MISSED OFFSET
	float                                              OriginalVolumeMultiplier_11_352BE51B46F8B38B523630B64B21A772;// 0x000C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
