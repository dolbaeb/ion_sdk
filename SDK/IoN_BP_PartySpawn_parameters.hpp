#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_PartySpawn.BP_PartySpawn_C.GetCharacter
struct ABP_PartySpawn_C_GetCharacter_Params
{
	class AIONCharacter*                               PlayerCharacter;                                          // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PartySpawn.BP_PartySpawn_C.SetPlayerDetails
struct ABP_PartySpawn_C_SetPlayerDetails_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PartySpawn.BP_PartySpawn_C.FillSlot
struct ABP_PartySpawn_C_FillSlot_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PartySpawn.BP_PartySpawn_C.ClearSlot
struct ABP_PartySpawn_C_ClearSlot_Params
{
};

// Function BP_PartySpawn.BP_PartySpawn_C.IsSlotAvailable
struct ABP_PartySpawn_C_IsSlotAvailable_Params
{
	bool                                               IsEmpty;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PartySpawn.BP_PartySpawn_C.UserConstructionScript
struct ABP_PartySpawn_C_UserConstructionScript_Params
{
};

// Function BP_PartySpawn.BP_PartySpawn_C.CreatePlayerPawn
struct ABP_PartySpawn_C_CreatePlayerPawn_Params
{
	struct FMatchmakingPlayer*                         Player;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PartySpawn.BP_PartySpawn_C.DestroyPlayerPawn
struct ABP_PartySpawn_C_DestroyPlayerPawn_Params
{
};

// Function BP_PartySpawn.BP_PartySpawn_C.PlayerUpdated
struct ABP_PartySpawn_C_PlayerUpdated_Params
{
	struct FMatchmakingPlayer*                         Player;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PartySpawn.BP_PartySpawn_C.BPEvent_ArmorUpdated
struct ABP_PartySpawn_C_BPEvent_ArmorUpdated_Params
{
	struct FString*                                    NewArmor;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function BP_PartySpawn.BP_PartySpawn_C.ExecuteUbergraph_BP_PartySpawn
struct ABP_PartySpawn_C_ExecuteUbergraph_BP_PartySpawn_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
