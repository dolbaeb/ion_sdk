#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_ActionManager.BP_ActionManager_C.Any Key Press
struct UBP_ActionManager_C_Any_Key_Press_Params
{
	struct FKey                                        Any_Key;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_ActionManager.BP_ActionManager_C.Input Analog Update
struct UBP_ActionManager_C_Input_Analog_Update_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.Listens New Mouse Bindings
struct UBP_ActionManager_C_Listens_New_Mouse_Bindings_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.Register New Binding
struct UBP_ActionManager_C_Register_New_Binding_Params
{
	struct FSKeyInput                                  New_Keybinding;                                           // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_ActionManager.BP_ActionManager_C.ReceiveBeginPlay
struct UBP_ActionManager_C_ReceiveBeginPlay_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.Tick Action Manager
struct UBP_ActionManager_C_Tick_Action_Manager_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.ReceiveTick
struct UBP_ActionManager_C_ReceiveTick_Params
{
	float*                                             DeltaSeconds;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ActionManager.BP_ActionManager_C.ExecuteUbergraph_BP_ActionManager
struct UBP_ActionManager_C_ExecuteUbergraph_BP_ActionManager_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ActionManager.BP_ActionManager_C.MenuPressed__DelegateSignature
struct UBP_ActionManager_C_MenuPressed__DelegateSignature_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.RunReleased__DelegateSignature
struct UBP_ActionManager_C_RunReleased__DelegateSignature_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.RunPressed__DelegateSignature
struct UBP_ActionManager_C_RunPressed__DelegateSignature_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.JumpPressed__DelegateSignature
struct UBP_ActionManager_C_JumpPressed__DelegateSignature_Params
{
};

// Function BP_ActionManager.BP_ActionManager_C.LookRight__DelegateSignature
struct UBP_ActionManager_C_LookRight__DelegateSignature_Params
{
	float                                              Axis;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ActionManager.BP_ActionManager_C.LookUp__DelegateSignature
struct UBP_ActionManager_C_LookUp__DelegateSignature_Params
{
	float                                              Axis;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ActionManager.BP_ActionManager_C.MoveRight__DelegateSignature
struct UBP_ActionManager_C_MoveRight__DelegateSignature_Params
{
	float                                              Axis;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ActionManager.BP_ActionManager_C.MoveForward__DelegateSignature
struct UBP_ActionManager_C_MoveForward__DelegateSignature_Params
{
	float                                              Axis;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
