#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_RedDot.BP_RedDot_C.UserConstructionScript
struct ABP_RedDot_C_UserConstructionScript_Params
{
};

// Function BP_RedDot.BP_RedDot_C.UpdateMaterialAimState
struct ABP_RedDot_C_UpdateMaterialAimState_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float*                                             AimRatio;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_RedDot.BP_RedDot_C.DetachFromFirearmBlueprint
struct ABP_RedDot_C_DetachFromFirearmBlueprint_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_RedDot.BP_RedDot_C.AttachToFirearmBlueprint
struct ABP_RedDot_C_AttachToFirearmBlueprint_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_RedDot.BP_RedDot_C.ExecuteUbergraph_BP_RedDot
struct ABP_RedDot_C_ExecuteUbergraph_BP_RedDot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
