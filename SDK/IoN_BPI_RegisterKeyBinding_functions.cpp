// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BPI_RegisterKeyBinding.BPI_RegisterKeyBinding_C.Key Pressed
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyInput              New_Keybinding                 (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           _                              (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBPI_RegisterKeyBinding_C::Key_Pressed(const struct FSKeyInput& New_Keybinding, bool* _)
{
	static auto fn = UObject::FindObject<UFunction>("Function BPI_RegisterKeyBinding.BPI_RegisterKeyBinding_C.Key Pressed");

	UBPI_RegisterKeyBinding_C_Key_Pressed_Params params;
	params.New_Keybinding = New_Keybinding;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (_ != nullptr)
		*_ = params._;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
