// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_UMP45.ABP_UMP45_C.ExecuteUbergraph_ABP_UMP45
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_UMP45_C::ExecuteUbergraph_ABP_UMP45(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_UMP45.ABP_UMP45_C.ExecuteUbergraph_ABP_UMP45");

	UABP_UMP45_C_ExecuteUbergraph_ABP_UMP45_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
