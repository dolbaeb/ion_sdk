#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnAppear
struct UWBP_LevelScoreBar_C_OnAppear_Params
{
};

// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.Set Current XP
struct UWBP_LevelScoreBar_C_Set_Current_XP_Params
{
	float                                              NewXP;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bAnimate;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.ExecuteUbergraph_WBP_LevelScoreBar
struct UWBP_LevelScoreBar_C_ExecuteUbergraph_WBP_LevelScoreBar_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnLevelUp__DelegateSignature
struct UWBP_LevelScoreBar_C_OnLevelUp__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
