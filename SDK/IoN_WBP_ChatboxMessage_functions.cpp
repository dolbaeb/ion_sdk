// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ChatboxMessage_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.Construct");

	UWBP_ChatboxMessage_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.ExecuteUbergraph_WBP_ChatboxMessage
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatboxMessage_C::ExecuteUbergraph_WBP_ChatboxMessage(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.ExecuteUbergraph_WBP_ChatboxMessage");

	UWBP_ChatboxMessage_C_ExecuteUbergraph_WBP_ChatboxMessage_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
