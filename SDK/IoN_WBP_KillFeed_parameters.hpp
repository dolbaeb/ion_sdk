#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_KillFeed.WBP_KillFeed_C.Construct
struct UWBP_KillFeed_C_Construct_Params
{
};

// Function WBP_KillFeed.WBP_KillFeed_C.RemoveEntry
struct UWBP_KillFeed_C_RemoveEntry_Params
{
	class UWBP_KillFeedEntry_C*                        Entry;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDied
struct UWBP_KillFeed_C_OnPlayerDied_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDowned
struct UWBP_KillFeed_C_OnPlayerDowned_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_KillFeed.WBP_KillFeed_C.ExecuteUbergraph_WBP_KillFeed
struct UWBP_KillFeed_C_ExecuteUbergraph_WBP_KillFeed_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
