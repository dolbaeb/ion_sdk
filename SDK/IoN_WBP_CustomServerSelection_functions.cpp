// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CustomServerSelection_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Construct");

	UWBP_CustomServerSelection_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_CustomServerSelection_C::BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature");

	UWBP_CustomServerSelection_C_BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Update Selected
// (BlueprintCallable, BlueprintEvent)

void UWBP_CustomServerSelection_C::Update_Selected()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Update Selected");

	UWBP_CustomServerSelection_C_Update_Selected_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CustomServerSelection_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.PreConstruct");

	UWBP_CustomServerSelection_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CustomServerSelection_C::BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature");

	UWBP_CustomServerSelection_C_BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.ExecuteUbergraph_WBP_CustomServerSelection
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CustomServerSelection_C::ExecuteUbergraph_WBP_CustomServerSelection(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.ExecuteUbergraph_WBP_CustomServerSelection");

	UWBP_CustomServerSelection_C_ExecuteUbergraph_WBP_CustomServerSelection_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
