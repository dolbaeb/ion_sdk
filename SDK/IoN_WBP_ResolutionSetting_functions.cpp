// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.KeyToWindowMode
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// struct FString                 Key                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<EWindowMode>       ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

TEnumAsByte<EWindowMode> UWBP_ResolutionSetting_C::KeyToWindowMode(const struct FString& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.KeyToWindowMode");

	UWBP_ResolutionSetting_C_KeyToWindowMode_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxScreenMode_GenerateWidget_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_ResolutionSetting_C::On_ComboBoxScreenMode_GenerateWidget_1(const struct FString& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxScreenMode_GenerateWidget_1");

	UWBP_ResolutionSetting_C_On_ComboBoxScreenMode_GenerateWidget_1_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxResolutions_GenerateWidget_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_ResolutionSetting_C::On_ComboBoxResolutions_GenerateWidget_1(const struct FString& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxResolutions_GenerateWidget_1");

	UWBP_ResolutionSetting_C_On_ComboBoxResolutions_GenerateWidget_1_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ResolutionToText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// int                            X                              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Y                              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ResolutionSetting_C::ResolutionToText(int X, int Y)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ResolutionToText");

	UWBP_ResolutionSetting_C_ResolutionToText_Params params;
	params.X = X;
	params.Y = Y;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.RefreshResolution
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_ResolutionSetting_C::RefreshResolution()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.RefreshResolution");

	UWBP_ResolutionSetting_C_RefreshResolution_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ResolutionSetting_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Construct");

	UWBP_ResolutionSetting_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ResolutionSetting_C::BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature");

	UWBP_ResolutionSetting_C_BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ResolutionSetting_C::BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature");

	UWBP_ResolutionSetting_C_BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ResolutionSetting_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Tick");

	UWBP_ResolutionSetting_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ResolutionSetting_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature");

	UWBP_ResolutionSetting_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ExecuteUbergraph_WBP_ResolutionSetting
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ResolutionSetting_C::ExecuteUbergraph_WBP_ResolutionSetting(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ExecuteUbergraph_WBP_ResolutionSetting");

	UWBP_ResolutionSetting_C_ExecuteUbergraph_WBP_ResolutionSetting_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
