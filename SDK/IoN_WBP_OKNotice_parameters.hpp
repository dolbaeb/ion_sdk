#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_OKNotice.WBP_OKNotice_C.SetMessage
struct UWBP_OKNotice_C_SetMessage_Params
{
	struct FString                                     Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_OKNotice.WBP_OKNotice_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature
struct UWBP_OKNotice_C_BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_OKNotice.WBP_OKNotice_C.ExecuteUbergraph_WBP_OKNotice
struct UWBP_OKNotice_C_ExecuteUbergraph_WBP_OKNotice_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_OKNotice.WBP_OKNotice_C.AcceptBtnClicked__DelegateSignature
struct UWBP_OKNotice_C_AcceptBtnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
