// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_KeyCombination.BP_KeyCombination_C.Get Key Combination Display Name
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Separator                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 No_Key_Display                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<EKeyCombinationDisplay> Display_Type                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Display_Name                   (Parm, OutParm, ZeroConstructor)

void UBP_KeyCombination_C::Get_Key_Combination_Display_Name(const struct FString& Separator, const struct FString& No_Key_Display, TEnumAsByte<EKeyCombinationDisplay> Display_Type, struct FString* Display_Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Get Key Combination Display Name");

	UBP_KeyCombination_C_Get_Key_Combination_Display_Name_Params params;
	params.Separator = Separator;
	params.No_Key_Display = No_Key_Display;
	params.Display_Type = Display_Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Display_Name != nullptr)
		*Display_Name = params.Display_Name;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Add Key Input
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyInput              InputPin                       (BlueprintVisible, BlueprintReadOnly, Parm)
// class UBP_KeyInput_C*          Input                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Add_Key_Input(const struct FSKeyInput& InputPin, class UBP_KeyInput_C** Input)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Add Key Input");

	UBP_KeyCombination_C_Add_Key_Input_Params params;
	params.InputPin = InputPin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Input != nullptr)
		*Input = params.Input;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Equal All Keys
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FSKeyInput>      Combination                    (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
// bool                           Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Equal_All_Keys(TArray<struct FSKeyInput>* Combination, bool* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Equal All Keys");

	UBP_KeyCombination_C_Equal_All_Keys_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Combination != nullptr)
		*Combination = params.Combination;
	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Equal All Conflicts
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FSKeyConflict>   Conflicts                      (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
// bool                           Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Equal_All_Conflicts(TArray<struct FSKeyConflict>* Conflicts, bool* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Equal All Conflicts");

	UBP_KeyCombination_C_Equal_All_Conflicts_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Conflicts != nullptr)
		*Conflicts = params.Conflicts;
	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Evaluate Blocking Combinations
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController*       Player_Controller              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Is_Active                      (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Evaluate_Blocking_Combinations(class APlayerController* Player_Controller, bool* Is_Active, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Evaluate Blocking Combinations");

	UBP_KeyCombination_C_Evaluate_Blocking_Combinations_Params params;
	params.Player_Controller = Player_Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Is_Active != nullptr)
		*Is_Active = params.Is_Active;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Clear Conflicting Combinations
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_KeyCombination_C::Clear_Conflicting_Combinations()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Clear Conflicting Combinations");

	UBP_KeyCombination_C_Clear_Conflicting_Combinations_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Add Conflicting Combination
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_KeyCombination_C*    Conflicted_Combination         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// TEnumAsByte<EKeyConflict>      Conflicted_                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Add_Conflicting_Combination(class UBP_KeyCombination_C* Conflicted_Combination, TEnumAsByte<EKeyConflict> Conflicted_)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Add Conflicting Combination");

	UBP_KeyCombination_C_Add_Conflicting_Combination_Params params;
	params.Conflicted_Combination = Conflicted_Combination;
	params.Conflicted_ = Conflicted_;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Detect Conflict
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_KeyCombination_C*    Input_Combination              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// TEnumAsByte<EKeyConflict>      Conflict_Type                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Detect_Conflict(class UBP_KeyCombination_C* Input_Combination, TEnumAsByte<EKeyConflict>* Conflict_Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Detect Conflict");

	UBP_KeyCombination_C_Detect_Conflict_Params params;
	params.Input_Combination = Input_Combination;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Conflict_Type != nullptr)
		*Conflict_Type = params.Conflict_Type;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Replace Key Combination
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FSKeyInput>      Key_Combination                (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UBP_KeyCombination_C::Replace_Key_Combination(TArray<struct FSKeyInput>* Key_Combination)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Replace Key Combination");

	UBP_KeyCombination_C_Replace_Key_Combination_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Key_Combination != nullptr)
		*Key_Combination = params.Key_Combination;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Load Key Combination
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Action_Name                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 Category                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 Name                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           Primary                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Load_Key_Combination(class UBP_GameSettings_C* Game_Settings, const struct FString& Action_Name, const struct FString& Category, const struct FString& Name, bool Primary)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Load Key Combination");

	UBP_KeyCombination_C_Load_Key_Combination_Params params;
	params.Game_Settings = Game_Settings;
	params.Action_Name = Action_Name;
	params.Category = Category;
	params.Name = Name;
	params.Primary = Primary;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Save Key Combination
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FSKeyActionSave         KeySave                        (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_KeyCombination_C::Save_Key_Combination(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Save Key Combination");

	UBP_KeyCombination_C_Save_Key_Combination_Params params;
	params.Game_Settings = Game_Settings;
	params.KeySave = KeySave;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Key Combination Current State
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController*       Player_Controller              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Ignore_Conflicts               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Axis_Value                     (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Is_Active                      (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Key_Combination_Current_State(class APlayerController* Player_Controller, bool Ignore_Conflicts, float* Axis_Value, bool* Is_Active, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Key Combination Current State");

	UBP_KeyCombination_C_Key_Combination_Current_State_Params params;
	params.Player_Controller = Player_Controller;
	params.Ignore_Conflicts = Ignore_Conflicts;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Axis_Value != nullptr)
		*Axis_Value = params.Axis_Value;
	if (Is_Active != nullptr)
		*Is_Active = params.Is_Active;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Init Key Combination
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Name                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           Can_t_Be_None                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// TArray<struct FSKeyInput>      Key_Combination                (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
// class UBP_KeyCombination_C*    Combination                    (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Init_Key_Combination(const struct FString& Name, bool Can_t_Be_None, TArray<struct FSKeyInput>* Key_Combination, class UBP_KeyCombination_C** Combination)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Init Key Combination");

	UBP_KeyCombination_C_Init_Key_Combination_Params params;
	params.Name = Name;
	params.Can_t_Be_None = Can_t_Be_None;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Key_Combination != nullptr)
		*Key_Combination = params.Key_Combination;
	if (Combination != nullptr)
		*Combination = params.Combination;
}


// Function BP_KeyCombination.BP_KeyCombination_C.Combination Updated__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_KeyCombination_C*    Combination                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyCombination_C::Combination_Updated__DelegateSignature(class UBP_KeyCombination_C* Combination)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyCombination.BP_KeyCombination_C.Combination Updated__DelegateSignature");

	UBP_KeyCombination_C_Combination_Updated__DelegateSignature_Params params;
	params.Combination = Combination;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
