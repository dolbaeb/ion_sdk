#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_IconHover.WBP_IconHover_C.GetSteamItem
struct UWBP_IconHover_C_GetSteamItem_Params
{
	struct FIONSteamInventoryItem                      SteamItem;                                                // (Parm, OutParm)
};

// Function WBP_IconHover.WBP_IconHover_C.SetSteamItem
struct UWBP_IconHover_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      SteamItem;                                                // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_IconHover.WBP_IconHover_C.SetItemRarity
struct UWBP_IconHover_C_SetItemRarity_Params
{
	struct FString                                     NewRarity;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_IconHover.WBP_IconHover_C.SetIconTexture
struct UWBP_IconHover_C_SetIconTexture_Params
{
	class UTexture2D*                                  NewTexture;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_IconHover.WBP_IconHover_C.SetIconName
struct UWBP_IconHover_C_SetIconName_Params
{
	struct FString                                     Name;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_IconHover.WBP_IconHover_C.OnMouseButtonDown
struct UWBP_IconHover_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_IconHover.WBP_IconHover_C.ItemSelected__DelegateSignature
struct UWBP_IconHover_C_ItemSelected__DelegateSignature_Params
{
	class UWBP_Icon_C*                                 BtnRef;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
