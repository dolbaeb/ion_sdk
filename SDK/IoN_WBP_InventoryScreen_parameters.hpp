#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BPEvent_RefreshInventory
struct UWBP_InventoryScreen_C_BPEvent_RefreshInventory_Params
{
};

// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_InventoryScreen_C_BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_InventoryScreen_C_BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryScreen.WBP_InventoryScreen_C.ExecuteUbergraph_WBP_InventoryScreen
struct UWBP_InventoryScreen_C_ExecuteUbergraph_WBP_InventoryScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
