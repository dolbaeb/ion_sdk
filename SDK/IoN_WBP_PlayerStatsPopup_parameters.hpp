#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnMouseButtonDown_1
struct UWBP_PlayerStatsPopup_C_OnMouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnResponse_4EA7138F45BE4EC38F18A393EDE067EB
struct UWBP_PlayerStatsPopup_C_OnResponse_4EA7138F45BE4EC38F18A393EDE067EB_Params
{
	struct FPlayerStats                                PlayerStats;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               bHasErrors;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.Construct
struct UWBP_PlayerStatsPopup_C_Construct_Params
{
};

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.ExecuteUbergraph_WBP_PlayerStatsPopup
struct UWBP_PlayerStatsPopup_C_ExecuteUbergraph_WBP_PlayerStatsPopup_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnClicked__DelegateSignature
struct UWBP_PlayerStatsPopup_C_OnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
