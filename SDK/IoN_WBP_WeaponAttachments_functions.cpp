// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_WeaponAttachments.WBP_WeaponAttachments_C.SetFirearm
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONFirearm*             Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponAttachments_C::SetFirearm(class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachments.WBP_WeaponAttachments_C.SetFirearm");

	UWBP_WeaponAttachments_C_SetFirearm_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
