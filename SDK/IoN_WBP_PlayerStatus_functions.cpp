// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Overlay_ShieldBar_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_PlayerStatus_C::Get_Overlay_ShieldBar_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Overlay_ShieldBar_Visibility_1");

	UWBP_PlayerStatus_C_Get_Overlay_ShieldBar_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Image_TeamColor_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_PlayerStatus_C::Get_Image_TeamColor_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Image_TeamColor_ColorAndOpacity_1");

	UWBP_PlayerStatus_C_Get_Image_TeamColor_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_ProgressBar_Health_Percent_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_PlayerStatus_C::Get_ProgressBar_Health_Percent_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_ProgressBar_Health_Percent_1");

	UWBP_PlayerStatus_C_Get_ProgressBar_Health_Percent_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_PlayerStatus_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.GetText_1");

	UWBP_PlayerStatus_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PlayerStatus_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.Construct");

	UWBP_PlayerStatus_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatus_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.Tick");

	UWBP_PlayerStatus_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatus.WBP_PlayerStatus_C.ExecuteUbergraph_WBP_PlayerStatus
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatus_C::ExecuteUbergraph_WBP_PlayerStatus(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus.WBP_PlayerStatus_C.ExecuteUbergraph_WBP_PlayerStatus");

	UWBP_PlayerStatus_C_ExecuteUbergraph_WBP_PlayerStatus_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
