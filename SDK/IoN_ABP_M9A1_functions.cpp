// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_M9A1.ABP_M9A1_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26
// (BlueprintEvent)

void UABP_M9A1_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M9A1.ABP_M9A1_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26");

	UABP_M9A1_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_M9A1.ABP_M9A1_C.BlueprintUpdateAnimation
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaTimeX                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_M9A1_C::BlueprintUpdateAnimation(float* DeltaTimeX)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M9A1.ABP_M9A1_C.BlueprintUpdateAnimation");

	UABP_M9A1_C_BlueprintUpdateAnimation_Params params;
	params.DeltaTimeX = DeltaTimeX;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_M9A1.ABP_M9A1_C.ExecuteUbergraph_ABP_M9A1
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_M9A1_C::ExecuteUbergraph_ABP_M9A1(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M9A1.ABP_M9A1_C.ExecuteUbergraph_ABP_M9A1");

	UABP_M9A1_C_ExecuteUbergraph_ABP_M9A1_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
