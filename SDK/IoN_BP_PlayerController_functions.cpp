// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_PlayerController.BP_PlayerController_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_PlayerController_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.UserConstructionScript");

	ABP_PlayerController_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteHUD
// (Exec, Event, Public, BlueprintEvent)

void ABP_PlayerController_C::IONDebugDeleteHUD()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteHUD");

	ABP_PlayerController_C_IONDebugDeleteHUD_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugPawnTick
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           bEnable                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugPawnTick(int* bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugPawnTick");

	ABP_PlayerController_C_IONDebugPawnTick_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugAnimation
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           bEnable                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugAnimation(int* bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugAnimation");

	ABP_PlayerController_C_IONDebugAnimation_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteAllItems
// (Exec, Event, Public, BlueprintEvent)

void ABP_PlayerController_C::IONDebugDeleteAllItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteAllItems");

	ABP_PlayerController_C_IONDebugDeleteAllItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugItemTick
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           bEnable                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugItemTick(int* bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugItemTick");

	ABP_PlayerController_C_IONDebugItemTick_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugSetLocation
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           Idx                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugSetLocation(int* Idx)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugSetLocation");

	ABP_PlayerController_C_IONDebugSetLocation_Params params;
	params.Idx = Idx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugSetCameraControl
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           Idx                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugSetCameraControl(int* Idx)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugSetCameraControl");

	ABP_PlayerController_C_IONDebugSetCameraControl_Params params;
	params.Idx = Idx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugSpawnChar
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           Count                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugSpawnChar(int* Count)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugSpawnChar");

	ABP_PlayerController_C_IONDebugSpawnChar_Params params;
	params.Count = Count;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugItemsMat
// (Exec, Event, Public, BlueprintEvent)

void ABP_PlayerController_C::IONDebugItemsMat()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugItemsMat");

	ABP_PlayerController_C_IONDebugItemsMat_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.IONDebugDFShadowsMode
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int*                           Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::IONDebugDFShadowsMode(int* Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.IONDebugDFShadowsMode");

	ABP_PlayerController_C_IONDebugDFShadowsMode_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.PrepareLandingAudioEvent
// (BlueprintCallable, BlueprintEvent)

void ABP_PlayerController_C::PrepareLandingAudioEvent()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.PrepareLandingAudioEvent");

	ABP_PlayerController_C_PrepareLandingAudioEvent_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.UpdateVoiceUI
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bProximity                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool*                          bActivated                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::UpdateVoiceUI(bool* bProximity, bool* bActivated)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.UpdateVoiceUI");

	ABP_PlayerController_C_UpdateVoiceUI_Params params;
	params.bProximity = bProximity;
	params.bActivated = bActivated;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.OnMatchResultsReceived
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FPlayerMatchHistory*    MatchResult                    (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PlayerController_C::OnMatchResultsReceived(struct FPlayerMatchHistory* MatchResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.OnMatchResultsReceived");

	ABP_PlayerController_C_OnMatchResultsReceived_Params params;
	params.MatchResult = MatchResult;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FGSAchievementEarnedMessage AchievementEarnedMessage       (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PlayerController_C::BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature(const struct FGSAchievementEarnedMessage& AchievementEarnedMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature");

	ABP_PlayerController_C_BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature_Params params;
	params.AchievementEarnedMessage = AchievementEarnedMessage;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerController.BP_PlayerController_C.ExecuteUbergraph_BP_PlayerController
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerController_C::ExecuteUbergraph_BP_PlayerController(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerController.BP_PlayerController_C.ExecuteUbergraph_BP_PlayerController");

	ABP_PlayerController_C_ExecuteUbergraph_BP_PlayerController_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
