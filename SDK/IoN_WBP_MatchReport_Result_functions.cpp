// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_TotalKills_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Result_C::Get_TextBlock_TotalKills_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_TotalKills_Text_1");

	UWBP_MatchReport_Result_C_Get_TextBlock_TotalKills_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_SquadType_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Result_C::Get_TextBlock_SquadType_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_SquadType_Text_1");

	UWBP_MatchReport_Result_C_Get_TextBlock_SquadType_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_1_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Result_C::Get_TextBlock_1_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_1_Text_1");

	UWBP_MatchReport_Result_C_Get_TextBlock_1_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_Participants_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Result_C::Get_TextBlock_Participants_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_Participants_Text_1");

	UWBP_MatchReport_Result_C_Get_TextBlock_Participants_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_Text_Placement_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Result_C::Get_Text_Placement_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_Text_Placement_Text_1");

	UWBP_MatchReport_Result_C_Get_Text_Placement_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MatchReport_Result_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Construct");

	UWBP_MatchReport_Result_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MatchReport_Result_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnAnimationFinished");

	UWBP_MatchReport_Result_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnReportClicked
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::OnReportClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnReportClicked");

	UWBP_MatchReport_Result_C_OnReportClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnMinimize
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::OnMinimize()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnMinimize");

	UWBP_MatchReport_Result_C_OnMinimize_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Appear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::Appear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Appear");

	UWBP_MatchReport_Result_C_Appear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Disappear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::Disappear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Disappear");

	UWBP_MatchReport_Result_C_Disappear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Begin Animation
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::Begin_Animation()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Begin Animation");

	UWBP_MatchReport_Result_C_Begin_Animation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.ExecuteUbergraph_WBP_MatchReport_Result
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_Result_C::ExecuteUbergraph_WBP_MatchReport_Result(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.ExecuteUbergraph_WBP_MatchReport_Result");

	UWBP_MatchReport_Result_C_ExecuteUbergraph_WBP_MatchReport_Result_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.AnimationFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Result_C::AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.AnimationFinished__DelegateSignature");

	UWBP_MatchReport_Result_C_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
