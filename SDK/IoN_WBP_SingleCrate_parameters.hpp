#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SingleCrate.WBP_SingleCrate_C.SetCrate
struct UWBP_SingleCrate_C_SetCrate_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature
struct UWBP_SingleCrate_C_BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature_Params
{
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.ExecuteUbergraph_WBP_SingleCrate
struct UWBP_SingleCrate_C_ExecuteUbergraph_WBP_SingleCrate_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.ViewDetailsCrate__DelegateSignature
struct UWBP_SingleCrate_C_ViewDetailsCrate__DelegateSignature_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SingleCrate.WBP_SingleCrate_C.PurchaseCrate__DelegateSignature
struct UWBP_SingleCrate_C_PurchaseCrate__DelegateSignature_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
