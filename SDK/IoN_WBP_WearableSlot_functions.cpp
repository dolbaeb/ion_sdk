// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_WearableSlot.WBP_WearableSlot_C.GetColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateColor             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateColor UWBP_WearableSlot_C::GetColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.GetColorAndOpacity_1");

	UWBP_WearableSlot_C_GetColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WearableSlot_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.GetText_1");

	UWBP_WearableSlot_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.GetVisibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_WearableSlot_C::GetVisibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.GetVisibility_1");

	UWBP_WearableSlot_C_GetVisibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.Get_BG_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_WearableSlot_C::Get_BG_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.Get_BG_Visibility_1");

	UWBP_WearableSlot_C_Get_BG_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WearableSlot_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.OnDrop");

	UWBP_WearableSlot_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_WearableSlot_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.GetCurrentItem");

	UWBP_WearableSlot_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WearableSlot_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.AcceptsDropForItem");

	UWBP_WearableSlot_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WearableSlot_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.PreConstruct");

	UWBP_WearableSlot_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_WearableSlot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.Construct");

	UWBP_WearableSlot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WearableSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.Tick");

	UWBP_WearableSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_WearableSlot_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.OnItemChanged");

	UWBP_WearableSlot_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WearableSlot.WBP_WearableSlot_C.ExecuteUbergraph_WBP_WearableSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WearableSlot_C::ExecuteUbergraph_WBP_WearableSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WearableSlot.WBP_WearableSlot_C.ExecuteUbergraph_WBP_WearableSlot");

	UWBP_WearableSlot_C_ExecuteUbergraph_WBP_WearableSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
