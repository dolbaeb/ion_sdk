// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_MainCharacter.BP_MainCharacter_C.ShowMapToolTip
// (Public, BlueprintCallable, BlueprintEvent)

void ABP_MainCharacter_C::ShowMapToolTip()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.ShowMapToolTip");

	ABP_MainCharacter_C_ShowMapToolTip_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.GetBoneForHitSim
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, Const)
// Parameters:
// struct FName                   BoneName                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   OutBone                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::GetBoneForHitSim(const struct FName& BoneName, struct FName* OutBone)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.GetBoneForHitSim");

	ABP_MainCharacter_C_GetBoneForHitSim_Params params;
	params.BoneName = BoneName;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutBone != nullptr)
		*OutBone = params.OutBone;
}


// Function BP_MainCharacter.BP_MainCharacter_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_MainCharacter_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.UserConstructionScript");

	ABP_MainCharacter_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__FinishedFunc
// (BlueprintEvent)

void ABP_MainCharacter_C::Timeline_0__FinishedFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__FinishedFunc");

	ABP_MainCharacter_C_Timeline_0__FinishedFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__UpdateFunc
// (BlueprintEvent)

void ABP_MainCharacter_C::Timeline_0__UpdateFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__UpdateFunc");

	ABP_MainCharacter_C_Timeline_0__UpdateFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__FinishedFunc
// (BlueprintEvent)

void ABP_MainCharacter_C::Timeline_1__FinishedFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__FinishedFunc");

	ABP_MainCharacter_C_Timeline_1__FinishedFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__UpdateFunc
// (BlueprintEvent)

void ABP_MainCharacter_C::Timeline_1__UpdateFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__UpdateFunc");

	ABP_MainCharacter_C_Timeline_1__UpdateFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.BeginPlayDebug
// (BlueprintCallable, BlueprintEvent)

void ABP_MainCharacter_C::BeginPlayDebug()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.BeginPlayDebug");

	ABP_MainCharacter_C_BeginPlayDebug_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.SetDropsuitMaterial
// (Net, NetReliable, NetMulticast, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UMaterialInterface*      Mat                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::SetDropsuitMaterial(class UMaterialInterface* Mat)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.SetDropsuitMaterial");

	ABP_MainCharacter_C_SetDropsuitMaterial_Params params;
	params.Mat = Mat;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.CamBlur
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Strength                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::CamBlur(float Strength)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.CamBlur");

	ABP_MainCharacter_C_CamBlur_Params params;
	params.Strength = Strength;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.EventBP_DropInModeStarted
// (Event, Public, BlueprintEvent)

void ABP_MainCharacter_C::EventBP_DropInModeStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.EventBP_DropInModeStarted");

	ABP_MainCharacter_C_EventBP_DropInModeStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.LandingFX
// (BlueprintCallable, BlueprintEvent)

void ABP_MainCharacter_C::LandingFX()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.LandingFX");

	ABP_MainCharacter_C_LandingFX_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.EventBP_LandingTrigger
// (Event, Public, BlueprintEvent)

void ABP_MainCharacter_C::EventBP_LandingTrigger()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.EventBP_LandingTrigger");

	ABP_MainCharacter_C_EventBP_LandingTrigger_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.Broadcast_LandingTrigger
// (Net, NetReliable, NetMulticast, BlueprintCallable, BlueprintEvent)

void ABP_MainCharacter_C::Broadcast_LandingTrigger()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.Broadcast_LandingTrigger");

	ABP_MainCharacter_C_Broadcast_LandingTrigger_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFromSweep                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              SweepResult                    (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm, IsPlainOldData)

void ABP_MainCharacter_C::BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature");

	ABP_MainCharacter_C_BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;
	params.bFromSweep = bFromSweep;
	params.SweepResult = SweepResult;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature");

	ABP_MainCharacter_C_BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.UpdateSuppressionEffects
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         SuppressionRatio               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::UpdateSuppressionEffects(float* SuppressionRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.UpdateSuppressionEffects");

	ABP_MainCharacter_C_UpdateSuppressionEffects_Params params;
	params.SuppressionRatio = SuppressionRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.UpdateHealthEffects
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         HealthRatio                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         OldHealthRatio                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::UpdateHealthEffects(float* HealthRatio, float* OldHealthRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.UpdateHealthEffects");

	ABP_MainCharacter_C_UpdateHealthEffects_Params params;
	params.HealthRatio = HealthRatio;
	params.OldHealthRatio = OldHealthRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_MainCharacter_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.ReceiveBeginPlay");

	ABP_MainCharacter_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.BP_SimulatedHit
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FName*                  BoneName                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         DamageImpulse                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector*                ShotFromDirection              (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)

void ABP_MainCharacter_C::BP_SimulatedHit(struct FName* BoneName, float* DamageImpulse, struct FVector* ShotFromDirection)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.BP_SimulatedHit");

	ABP_MainCharacter_C_BP_SimulatedHit_Params params;
	params.BoneName = BoneName;
	params.DamageImpulse = DamageImpulse;
	params.ShotFromDirection = ShotFromDirection;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.OnBulletSuppression
// (Event, Public, BlueprintEvent)
// Parameters:
// class AMainProjectile**        Projectile                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::OnBulletSuppression(class AMainProjectile** Projectile)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.OnBulletSuppression");

	ABP_MainCharacter_C_OnBulletSuppression_Params params;
	params.Projectile = Projectile;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.OnAimingStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         NewAimRatio                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool*                          bForce                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class AIONWeapon**             Weapon                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::OnAimingStateChanged(float* NewAimRatio, bool* bForce, class AIONWeapon** Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.OnAimingStateChanged");

	ABP_MainCharacter_C_OnAimingStateChanged_Params params;
	params.NewAimRatio = NewAimRatio;
	params.bForce = bForce;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.InventoryStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bOpen                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::InventoryStateChanged(bool* bOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.InventoryStateChanged");

	ABP_MainCharacter_C_InventoryStateChanged_Params params;
	params.bOpen = bOpen;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.BP_ProceduralFireAnimation
// (Event, Public, BlueprintEvent)

void ABP_MainCharacter_C::BP_ProceduralFireAnimation()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.BP_ProceduralFireAnimation");

	ABP_MainCharacter_C_BP_ProceduralFireAnimation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.OnHardLanding
// (Event, Public, BlueprintEvent)

void ABP_MainCharacter_C::OnHardLanding()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.OnHardLanding");

	ABP_MainCharacter_C_OnHardLanding_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MainCharacter.BP_MainCharacter_C.ExecuteUbergraph_BP_MainCharacter
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MainCharacter_C::ExecuteUbergraph_BP_MainCharacter(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MainCharacter.BP_MainCharacter_C.ExecuteUbergraph_BP_MainCharacter");

	ABP_MainCharacter_C_ExecuteUbergraph_BP_MainCharacter_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
