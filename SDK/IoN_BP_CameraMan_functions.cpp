// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_CameraMan.BP_CameraMan_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_CameraMan_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.UserConstructionScript");

	ABP_CameraMan_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.InpActEvt_OBS_Map_K2Node_InputActionEvent_1
// (BlueprintEvent)
// Parameters:
// struct FKey                    Key                            (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_CameraMan_C::InpActEvt_OBS_Map_K2Node_InputActionEvent_1(const struct FKey& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.InpActEvt_OBS_Map_K2Node_InputActionEvent_1");

	ABP_CameraMan_C_InpActEvt_OBS_Map_K2Node_InputActionEvent_1_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_CameraMan_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.ReceiveBeginPlay");

	ABP_CameraMan_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.Add Options Menu
// (BlueprintCallable, BlueprintEvent)

void ABP_CameraMan_C::Add_Options_Menu()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.Add Options Menu");

	ABP_CameraMan_C_Add_Options_Menu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.OnOptionChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// int*                           OptionIndex                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_CameraMan_C::OnOptionChanged(int* OptionIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.OnOptionChanged");

	ABP_CameraMan_C_OnOptionChanged_Params params;
	params.OptionIndex = OptionIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.ReceiveDestroyed
// (Event, Public, BlueprintEvent)

void ABP_CameraMan_C::ReceiveDestroyed()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.ReceiveDestroyed");

	ABP_CameraMan_C_ReceiveDestroyed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.ReceivePossessed
// (Event, Public, BlueprintEvent)
// Parameters:
// class AController**            NewController                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_CameraMan_C::ReceivePossessed(class AController** NewController)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.ReceivePossessed");

	ABP_CameraMan_C_ReceivePossessed_Params params;
	params.NewController = NewController;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_CameraMan.BP_CameraMan_C.ExecuteUbergraph_BP_CameraMan
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_CameraMan_C::ExecuteUbergraph_BP_CameraMan(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_CameraMan.BP_CameraMan_C.ExecuteUbergraph_BP_CameraMan");

	ABP_CameraMan_C_ExecuteUbergraph_BP_CameraMan_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
