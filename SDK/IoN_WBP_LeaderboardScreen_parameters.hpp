#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BuildCurrentLeaderboardShortCode
struct UWBP_LeaderboardScreen_C_BuildCurrentLeaderboardShortCode_Params
{
	struct FString                                     LeaderboardShortCode;                                     // (Parm, OutParm, ZeroConstructor)
	struct FString                                     GameMode;                                                 // (Parm, OutParm, ZeroConstructor)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.StringToLeaderboardCategory
struct UWBP_LeaderboardScreen_C_StringToLeaderboardCategory_Params
{
	struct FString                                     inString;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ELeaderboardCategories>                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetVisibility_Throbber
struct UWBP_LeaderboardScreen_C_GetVisibility_Throbber_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedFailure
struct UWBP_LeaderboardScreen_C_OnReceivedFailure_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedLeaderboard
struct UWBP_LeaderboardScreen_C_OnReceivedLeaderboard_Params
{
	TArray<struct FLeaderboardDataEntry>               LeaderboardData;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetMaxLeaderboardEntries
struct UWBP_LeaderboardScreen_C_GetMaxLeaderboardEntries_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetCurrentStatistic
struct UWBP_LeaderboardScreen_C_GetCurrentStatistic_Params
{
	struct FString                                     RetrunValue;                                              // (Parm, OutParm, ZeroConstructor)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.UpdateLeaderboard
struct UWBP_LeaderboardScreen_C_UpdateLeaderboard_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_LeaderboardScreen_C_BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_LeaderboardScreen_C_BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboardAroundPlayer
struct UWBP_LeaderboardScreen_C_GetLeaderboardAroundPlayer_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboard
struct UWBP_LeaderboardScreen_C_GetFriendLeaderboard_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboardAroundPlayer
struct UWBP_LeaderboardScreen_C_GetFriendLeaderboardAroundPlayer_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.DoUpdateLeaderboard
struct UWBP_LeaderboardScreen_C_DoUpdateLeaderboard_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Construct
struct UWBP_LeaderboardScreen_C_Construct_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboard
struct UWBP_LeaderboardScreen_C_GetLeaderboard_Params
{
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardSuccess
struct UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardSuccess_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFailed
struct UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFailed_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardSuccess
struct UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardSuccess_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFailed
struct UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFailed_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsSuccess
struct UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFriendsSuccess_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsFailed
struct UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFriendsFailed_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsSuccess
struct UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFriendsSuccess_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsFailed
struct UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFriendsFailed_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature
struct UWBP_LeaderboardScreen_C_BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature_Params
{
	TEnumAsByte<ELeaderboardTeamTypes>                 Team_Type;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature
struct UWBP_LeaderboardScreen_C_BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature_Params
{
	TEnumAsByte<ELeaderboardCategories>                Category;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Connected
struct UWBP_LeaderboardScreen_C_Connected_Params
{
	struct FString*                                    UserId;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString*                                    AuthToken;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.ExecuteUbergraph_WBP_LeaderboardScreen
struct UWBP_LeaderboardScreen_C_ExecuteUbergraph_WBP_LeaderboardScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
