// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SettingsPage_Audio_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Construct");

	UWBP_SettingsPage_Audio_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature");

	UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature");

	UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature");

	UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Refresh Input
// (BlueprintCallable, BlueprintEvent)

void UWBP_SettingsPage_Audio_C::Refresh_Input()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Refresh Input");

	UWBP_SettingsPage_Audio_C_Refresh_Input_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Slide_Value                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature(float Slide_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature");

	UWBP_SettingsPage_Audio_C_BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature_Params params;
	params.Slide_Value = Slide_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Slide_Value                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature(float Slide_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature");

	UWBP_SettingsPage_Audio_C_BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature_Params params;
	params.Slide_Value = Slide_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.ExecuteUbergraph_WBP_SettingsPage_Audio
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Audio_C::ExecuteUbergraph_WBP_SettingsPage_Audio(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.ExecuteUbergraph_WBP_SettingsPage_Audio");

	UWBP_SettingsPage_Audio_C_ExecuteUbergraph_WBP_SettingsPage_Audio_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
