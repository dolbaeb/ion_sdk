// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_Sako85.BP_Sako85_C.GetPickupLocation
// (Event, Protected, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, Const)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector ABP_Sako85_C::GetPickupLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85.BP_Sako85_C.GetPickupLocation");

	ABP_Sako85_C_GetPickupLocation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function BP_Sako85.BP_Sako85_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_Sako85_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85.BP_Sako85_C.UserConstructionScript");

	ABP_Sako85_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Sako85.BP_Sako85_C.ReceiveTick
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaSeconds                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Sako85_C::ReceiveTick(float* DeltaSeconds)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85.BP_Sako85_C.ReceiveTick");

	ABP_Sako85_C_ReceiveTick_Params params;
	params.DeltaSeconds = DeltaSeconds;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Sako85.BP_Sako85_C.ExecuteUbergraph_BP_Sako85
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Sako85_C::ExecuteUbergraph_BP_Sako85(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85.BP_Sako85_C.ExecuteUbergraph_BP_Sako85");

	ABP_Sako85_C_ExecuteUbergraph_BP_Sako85_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
