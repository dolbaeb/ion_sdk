// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragFinished
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnDragFinished()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragFinished");

	UWBP_InventorySlotBase_C_OnDragFinished_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.CancelDropHover
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::CancelDropHover()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.CancelDropHover");

	UWBP_InventorySlotBase_C_CancelDropHover_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemRightClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnItemRightClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemRightClicked");

	UWBP_InventorySlotBase_C_OnItemRightClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ItemIsEquipped
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventorySlotBase_C::ItemIsEquipped()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ItemIsEquipped");

	UWBP_InventorySlotBase_C_ItemIsEquipped_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.EquipItem
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Slot                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::EquipItem(int Slot)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.EquipItem");

	UWBP_InventorySlotBase_C_EquipItem_Params params;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetAnimations
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidgetAnimation*        HoverAnimation                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UWidgetAnimation*        DropHoverAnimation             (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UWidgetAnimation*        AcceptDropAnimation            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UWidgetAnimation*        ActiveAnimation                (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_InventorySlotBase_C::SetAnimations(class UWidgetAnimation* HoverAnimation, class UWidgetAnimation* DropHoverAnimation, class UWidgetAnimation* AcceptDropAnimation, class UWidgetAnimation* ActiveAnimation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetAnimations");

	UWBP_InventorySlotBase_C_SetAnimations_Params params;
	params.HoverAnimation = HoverAnimation;
	params.DropHoverAnimation = DropHoverAnimation;
	params.AcceptDropAnimation = AcceptDropAnimation;
	params.ActiveAnimation = ActiveAnimation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetActive
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Active                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::SetActive(bool Active)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetActive");

	UWBP_InventorySlotBase_C_SetActive_Params params;
	params.Active = Active;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem*                Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventorySlotBase_C::AcceptsDropForItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.AcceptsDropForItem");

	UWBP_InventorySlotBase_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnUnhovered
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnUnhovered()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnUnhovered");

	UWBP_InventorySlotBase_C_OnUnhovered_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnHovered
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnHovered()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnHovered");

	UWBP_InventorySlotBase_C_OnHovered_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.HasValidItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventorySlotBase_C::HasValidItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.HasValidItem");

	UWBP_InventorySlotBase_C_HasValidItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetDragItemVisual
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_InventorySlotBase_C::GetDragItemVisual()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetDragItemVisual");

	UWBP_InventorySlotBase_C_GetDragItemVisual_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnItemClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemClicked");

	UWBP_InventorySlotBase_C_OnItemClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonUp
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_InventorySlotBase_C::OnMouseButtonUp(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonUp");

	UWBP_InventorySlotBase_C_OnMouseButtonUp_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_InventorySlotBase_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonDown");

	UWBP_InventorySlotBase_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemChanged");

	UWBP_InventorySlotBase_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_InventorySlotBase_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetCurrentItem");

	UWBP_InventorySlotBase_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragDetected
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// class UDragDropOperation*      Operation                      (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::OnDragDetected(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragDetected");

	UWBP_InventorySlotBase_C_OnDragDetected_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Operation != nullptr)
		*Operation = params.Operation;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnFinishedItemDrag
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventorySlotBase_C::OnFinishedItemDrag()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnFinishedItemDrag");

	UWBP_InventorySlotBase_C_OnFinishedItemDrag_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnBeginItemDrag
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONItem*                Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::OnBeginItemDrag(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnBeginItemDrag");

	UWBP_InventorySlotBase_C_OnBeginItemDrag_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.Tick");

	UWBP_InventorySlotBase_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_InventorySlotBase_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseLeave");

	UWBP_InventorySlotBase_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseEnter
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_InventorySlotBase_C::OnMouseEnter(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseEnter");

	UWBP_InventorySlotBase_C_OnMouseEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragEnter
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::OnDragEnter(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragEnter");

	UWBP_InventorySlotBase_C_OnDragEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragLeave
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::OnDragLeave(struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragLeave");

	UWBP_InventorySlotBase_C_OnDragLeave_Params params;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragCancelled
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          PointerEvent                   (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::OnDragCancelled(struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragCancelled");

	UWBP_InventorySlotBase_C_OnDragCancelled_Params params;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ExecuteUbergraph_WBP_InventorySlotBase
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventorySlotBase_C::ExecuteUbergraph_WBP_InventorySlotBase(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ExecuteUbergraph_WBP_InventorySlotBase");

	UWBP_InventorySlotBase_C_ExecuteUbergraph_WBP_InventorySlotBase_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
