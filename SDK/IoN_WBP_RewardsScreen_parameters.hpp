#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowMainMenu
struct UWBP_RewardsScreen_C_ShowMainMenu_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.UpdatePrices
struct UWBP_RewardsScreen_C_UpdatePrices_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowItemScreen
struct UWBP_RewardsScreen_C_ShowItemScreen_Params
{
	bool                                               bShow;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowCrateContentWidget
struct UWBP_RewardsScreen_C_ShowCrateContentWidget_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.CrateContentSelected
struct UWBP_RewardsScreen_C_CrateContentSelected_Params
{
	class UWBP_Icon_C*                                 ItemBtnSelected;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.TryConfirmPurchase
struct UWBP_RewardsScreen_C_TryConfirmPurchase_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ViewCrateContents
struct UWBP_RewardsScreen_C_ViewCrateContents_Params
{
	class UIONSteamCrate*                              CrateToShow;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ToogleConfirmDialouge
struct UWBP_RewardsScreen_C_ToogleConfirmDialouge_Params
{
	bool                                               bShow;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.Construct
struct UWBP_RewardsScreen_C_Construct_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.PurchaseConfirmed
struct UWBP_RewardsScreen_C_PurchaseConfirmed_Params
{
	int                                                ItemDefID;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.CratePurchaseFailed
struct UWBP_RewardsScreen_C_CratePurchaseFailed_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature
struct UWBP_RewardsScreen_C_BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ExecuteUbergraph_WBP_RewardsScreen
struct UWBP_RewardsScreen_C_ExecuteUbergraph_WBP_RewardsScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
