// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_ACOG.BP_ACOG_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_ACOG_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ACOG.BP_ACOG_C.UserConstructionScript");

	ABP_ACOG_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ACOG.BP_ACOG_C.UpdateMaterialAimState
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         AimRatio                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_ACOG_C::UpdateMaterialAimState(class AIONFirearm** Firearm, float* AimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ACOG.BP_ACOG_C.UpdateMaterialAimState");

	ABP_ACOG_C_UpdateMaterialAimState_Params params;
	params.Firearm = Firearm;
	params.AimRatio = AimRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ACOG.BP_ACOG_C.DetachFromFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_ACOG_C::DetachFromFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ACOG.BP_ACOG_C.DetachFromFirearmBlueprint");

	ABP_ACOG_C_DetachFromFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ACOG.BP_ACOG_C.AttachToFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_ACOG_C::AttachToFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ACOG.BP_ACOG_C.AttachToFirearmBlueprint");

	ABP_ACOG_C_AttachToFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ACOG.BP_ACOG_C.ExecuteUbergraph_BP_ACOG
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_ACOG_C::ExecuteUbergraph_BP_ACOG(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ACOG.BP_ACOG_C.ExecuteUbergraph_BP_ACOG");

	ABP_ACOG_C_ExecuteUbergraph_BP_ACOG_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
