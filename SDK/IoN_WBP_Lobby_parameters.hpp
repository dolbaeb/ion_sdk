#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Lobby.WBP_Lobby_C.RemovePartyMember
struct UWBP_Lobby_C_RemovePartyMember_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_Lobby.WBP_Lobby_C.AddPartyMember
struct UWBP_Lobby_C_AddPartyMember_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_Lobby.WBP_Lobby_C.AddBtnClicked
struct UWBP_Lobby_C_AddBtnClicked_Params
{
};

// Function WBP_Lobby.WBP_Lobby_C.RefreshMembers
struct UWBP_Lobby_C_RefreshMembers_Params
{
};

// Function WBP_Lobby.WBP_Lobby_C.Tick
struct UWBP_Lobby_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Lobby.WBP_Lobby_C.ExecuteUbergraph_WBP_Lobby
struct UWBP_Lobby_C_ExecuteUbergraph_WBP_Lobby_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Lobby.WBP_Lobby_C.OpenFriendsListEvent__DelegateSignature
struct UWBP_Lobby_C_OpenFriendsListEvent__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
