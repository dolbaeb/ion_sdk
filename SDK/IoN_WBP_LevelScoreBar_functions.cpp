// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnAppear
// (BlueprintCallable, BlueprintEvent)

void UWBP_LevelScoreBar_C::OnAppear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnAppear");

	UWBP_LevelScoreBar_C_OnAppear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.Set Current XP
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          NewXP                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           bAnimate                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LevelScoreBar_C::Set_Current_XP(float NewXP, bool bAnimate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.Set Current XP");

	UWBP_LevelScoreBar_C_Set_Current_XP_Params params;
	params.NewXP = NewXP;
	params.bAnimate = bAnimate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.ExecuteUbergraph_WBP_LevelScoreBar
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LevelScoreBar_C::ExecuteUbergraph_WBP_LevelScoreBar(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.ExecuteUbergraph_WBP_LevelScoreBar");

	UWBP_LevelScoreBar_C_ExecuteUbergraph_WBP_LevelScoreBar_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnLevelUp__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_LevelScoreBar_C::OnLevelUp__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LevelScoreBar.WBP_LevelScoreBar_C.OnLevelUp__DelegateSignature");

	UWBP_LevelScoreBar_C_OnLevelUp__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
