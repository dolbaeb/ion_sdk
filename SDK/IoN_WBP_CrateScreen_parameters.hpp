#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CrateScreen.WBP_CrateScreen_C.SpawnAllIconWidgets
struct UWBP_CrateScreen_C_SpawnAllIconWidgets_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.CloseCrateScreen
struct UWBP_CrateScreen_C_CloseCrateScreen_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenFailed
struct UWBP_CrateScreen_C_CrateOpenFailed_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.TickAnimSpeed
struct UWBP_CrateScreen_C_TickAnimSpeed_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.PrepareItemsInCrateArray
struct UWBP_CrateScreen_C_PrepareItemsInCrateArray_Params
{
	class UIONSteamItem*                               GrantedItem;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.AnimateIcons
struct UWBP_CrateScreen_C_AnimateIcons_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.SetSteamItem
struct UWBP_CrateScreen_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.TickSingleIconAnim
struct UWBP_CrateScreen_C_TickSingleIconAnim_Params
{
	class UWidget*                                     Icon;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                Idx;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.Construct
struct UWBP_CrateScreen_C_Construct_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
struct UWBP_CrateScreen_C_BndEvt__Open_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature
struct UWBP_CrateScreen_C_BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.CrateOpenConfirmed
struct UWBP_CrateScreen_C_CrateOpenConfirmed_Params
{
	int                                                Result;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.Tick
struct UWBP_CrateScreen_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.TransitionOutEvent
struct UWBP_CrateScreen_C_TransitionOutEvent_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature
struct UWBP_CrateScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_47_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature
struct UWBP_CrateScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_137_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.ExecuteUbergraph_WBP_CrateScreen
struct UWBP_CrateScreen_C_ExecuteUbergraph_WBP_CrateScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.BackToInventory__DelegateSignature
struct UWBP_CrateScreen_C_BackToInventory__DelegateSignature_Params
{
};

// Function WBP_CrateScreen.WBP_CrateScreen_C.ClearActionsMenu__DelegateSignature
struct UWBP_CrateScreen_C_ClearActionsMenu__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
