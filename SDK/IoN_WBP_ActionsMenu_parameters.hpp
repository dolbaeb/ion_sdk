#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetEquipped
struct UWBP_ActionsMenu_C_SetEquipped_Params
{
	bool                                               Equipped;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetSteamItem
struct UWBP_ActionsMenu_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_ActionsMenu_C_BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature
struct UWBP_ActionsMenu_C_BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
struct UWBP_ActionsMenu_C_BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature
struct UWBP_ActionsMenu_C_BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ExecuteUbergraph_WBP_ActionsMenu
struct UWBP_ActionsMenu_C_ExecuteUbergraph_WBP_ActionsMenu_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ScrapBtnClicked__DelegateSignature
struct UWBP_ActionsMenu_C_ScrapBtnClicked__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ResetBtnClicked__DelegateSignature
struct UWBP_ActionsMenu_C_ResetBtnClicked__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.InspectBtnClicked__DelegateSignature
struct UWBP_ActionsMenu_C_InspectBtnClicked__DelegateSignature_Params
{
};

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.EquipBtnClicked__DelegateSignature
struct UWBP_ActionsMenu_C_EquipBtnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
