#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EMatchReportState.EMatchReportState
enum class EMatchReportState : uint8_t
{
	EMatchReportState__NewEnumerator0 = 0,
	EMatchReportState__NewEnumerator1 = 1,
	EMatchReportState__NewEnumerator4 = 2,
	EMatchReportState__NewEnumerator2 = 3,
	EMatchReportState__NewEnumerator3 = 4,
	EMatchReportState__EMatchReportState_MAX = 5
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
