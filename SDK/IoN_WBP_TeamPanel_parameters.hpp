#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_TeamPanel.WBP_TeamPanel_C.Construct
struct UWBP_TeamPanel_C_Construct_Params
{
};

// Function WBP_TeamPanel.WBP_TeamPanel_C.Tick
struct UWBP_TeamPanel_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TeamPanel.WBP_TeamPanel_C.OnPlayerLeft_Event_1
struct UWBP_TeamPanel_C_OnPlayerLeft_Event_1_Params
{
	class UWBP_TeamMateInfo_C*                         Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_TeamPanel.WBP_TeamPanel_C.ExecuteUbergraph_WBP_TeamPanel
struct UWBP_TeamPanel_C_ExecuteUbergraph_WBP_TeamPanel_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
