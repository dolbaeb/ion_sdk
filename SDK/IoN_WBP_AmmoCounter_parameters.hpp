#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnMouseButtonDown
struct UWBP_AmmoCounter_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.Construct
struct UWBP_AmmoCounter_C_Construct_Params
{
};

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.On Ammo Changed
struct UWBP_AmmoCounter_C_On_Ammo_Changed_Params
{
};

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnWearablesChanged
struct UWBP_AmmoCounter_C_OnWearablesChanged_Params
{
};

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.PreConstruct
struct UWBP_AmmoCounter_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.ExecuteUbergraph_WBP_AmmoCounter
struct UWBP_AmmoCounter_C_ExecuteUbergraph_WBP_AmmoCounter_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
