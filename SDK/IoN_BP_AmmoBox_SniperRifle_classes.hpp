#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_AmmoBox_SniperRifle.BP_AmmoBox_SniperRifle_C
// 0x0000 (0x03E8 - 0x03E8)
class ABP_AmmoBox_SniperRifle_C : public AIONAmmoBox
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_AmmoBox_SniperRifle.BP_AmmoBox_SniperRifle_C");
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
