#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EAudioType.EAudioType
enum class EAudioType : uint8_t
{
	EAudioType__NewEnumerator4     = 0,
	EAudioType__NewEnumerator0     = 1,
	EAudioType__NewEnumerator1     = 2,
	EAudioType__NewEnumerator2     = 3,
	EAudioType__NewEnumerator3     = 4,
	EAudioType__NewEnumerator5     = 5,
	EAudioType__EAudioType_MAX     = 6
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
