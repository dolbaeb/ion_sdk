// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_SpectatorInfoWidget_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.UserConstructionScript");

	ABP_SpectatorInfoWidget_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.SetupWidget
// (Event, Public, BlueprintEvent)

void ABP_SpectatorInfoWidget_C::SetupWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.SetupWidget");

	ABP_SpectatorInfoWidget_C_SetupWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.OnViewModeChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bMinimal                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_SpectatorInfoWidget_C::OnViewModeChanged(bool* bMinimal)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.OnViewModeChanged");

	ABP_SpectatorInfoWidget_C_OnViewModeChanged_Params params;
	params.bMinimal = bMinimal;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.ExecuteUbergraph_BP_SpectatorInfoWidget
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_SpectatorInfoWidget_C::ExecuteUbergraph_BP_SpectatorInfoWidget(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.ExecuteUbergraph_BP_SpectatorInfoWidget");

	ABP_SpectatorInfoWidget_C_ExecuteUbergraph_BP_SpectatorInfoWidget_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
