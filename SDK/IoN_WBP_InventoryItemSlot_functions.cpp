// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Label_Text_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_InventoryItemSlot_C::Get_Label_Text_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Label_Text_Visibility_1");

	UWBP_InventoryItemSlot_C_Get_Label_Text_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_DragReceive_Background_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_InventoryItemSlot_C::Get_DragReceive_Background_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_DragReceive_Background_1");

	UWBP_InventoryItemSlot_C_Get_DragReceive_Background_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Highlight_Background_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_InventoryItemSlot_C::Get_Highlight_Background_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Highlight_Background_1");

	UWBP_InventoryItemSlot_C_Get_Highlight_Background_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Frame_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_InventoryItemSlot_C::Get_Frame_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Frame_Brush_1");

	UWBP_InventoryItemSlot_C_Get_Frame_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.AcceptsDropForItem
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventoryItemSlot_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.AcceptsDropForItem");

	UWBP_InventoryItemSlot_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventoryItemSlot_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnDrop");

	UWBP_InventoryItemSlot_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_InventoryItemSlot_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetCurrentItem");

	UWBP_InventoryItemSlot_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_InventoryItemSlot_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetText_1");

	UWBP_InventoryItemSlot_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.SetAssignFullVisibility
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bVisible                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItemSlot_C::SetAssignFullVisibility(bool bVisible)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.SetAssignFullVisibility");

	UWBP_InventoryItemSlot_C_SetAssignFullVisibility_Params params;
	params.bVisible = bVisible;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_UnloadAmmoButton_bIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_InventoryItemSlot_C::Get_UnloadAmmoButton_bIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_UnloadAmmoButton_bIsEnabled_1");

	UWBP_InventoryItemSlot_C_Get_UnloadAmmoButton_bIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmountText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_InventoryItemSlot_C::Get_AmountText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmountText_Text_1");

	UWBP_InventoryItemSlot_C_Get_AmountText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmmoText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_InventoryItemSlot_C::Get_AmmoText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmmoText_Text_1");

	UWBP_InventoryItemSlot_C_Get_AmmoText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItemSlot_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemChanged");

	UWBP_InventoryItemSlot_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItemSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Tick");

	UWBP_InventoryItemSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_InventoryItemSlot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Construct");

	UWBP_InventoryItemSlot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItemSlot_C::OnItemClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemClicked");

	UWBP_InventoryItemSlot_C_OnItemClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnHovered
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItemSlot_C::OnHovered()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnHovered");

	UWBP_InventoryItemSlot_C_OnHovered_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnUnhovered
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItemSlot_C::OnUnhovered()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnUnhovered");

	UWBP_InventoryItemSlot_C_OnUnhovered_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.ExecuteUbergraph_WBP_InventoryItemSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItemSlot_C::ExecuteUbergraph_WBP_InventoryItemSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.ExecuteUbergraph_WBP_InventoryItemSlot");

	UWBP_InventoryItemSlot_C_ExecuteUbergraph_WBP_InventoryItemSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
