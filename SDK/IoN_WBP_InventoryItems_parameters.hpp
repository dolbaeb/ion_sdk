#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InventoryItems.WBP_InventoryItems_C.DeselectItem
struct UWBP_InventoryItems_C_DeselectItem_Params
{
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.SteamUpdatedItems
struct UWBP_InventoryItems_C_SteamUpdatedItems_Params
{
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.SetSteamTypeFilter
struct UWBP_InventoryItems_C_SetSteamTypeFilter_Params
{
	class UClass*                                      NewFilter;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.SetItemFilter
struct UWBP_InventoryItems_C_SetItemFilter_Params
{
	class UClass*                                      ItemFilter;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.NewItemSelected
struct UWBP_InventoryItems_C_NewItemSelected_Params
{
	class UWBP_Icon_C*                                 SelectedBtn;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.FillInventoryWithItems
struct UWBP_InventoryItems_C_FillInventoryWithItems_Params
{
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.RefreshItems
struct UWBP_InventoryItems_C_RefreshItems_Params
{
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.Construct
struct UWBP_InventoryItems_C_Construct_Params
{
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.ExecuteUbergraph_WBP_InventoryItems
struct UWBP_InventoryItems_C_ExecuteUbergraph_WBP_InventoryItems_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryItems.WBP_InventoryItems_C.ItemSelected__DelegateSignature
struct UWBP_InventoryItems_C_ItemSelected__DelegateSignature_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
