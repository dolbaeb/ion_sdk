// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_IonArmsArmor.BP_IonArmsArmor_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonArmsArmor_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonArmsArmor.BP_IonArmsArmor_C.UserConstructionScript");

	ABP_IonArmsArmor_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_AttachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonArmsArmor_C::BPEvent_AttachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_AttachWearable");

	ABP_IonArmsArmor_C_BPEvent_AttachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_DetachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonArmsArmor_C::BPEvent_DetachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_DetachWearable");

	ABP_IonArmsArmor_C_BPEvent_DetachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonArmsArmor.BP_IonArmsArmor_C.ExecuteUbergraph_BP_IonArmsArmor
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IonArmsArmor_C::ExecuteUbergraph_BP_IonArmsArmor(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonArmsArmor.BP_IonArmsArmor_C.ExecuteUbergraph_BP_IonArmsArmor");

	ABP_IonArmsArmor_C_ExecuteUbergraph_BP_IonArmsArmor_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
