#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_IONKnife.BP_IONKnife_C
// 0x0008 (0x0948 - 0x0940)
class ABP_IONKnife_C : public AIONKnife
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0940(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_IONKnife.BP_IONKnife_C");
		return ptr;
	}


	void UserConstructionScript();
	void EventBP_HitSurfaceType(TEnumAsByte<EPhysicalSurface>* SurfaceTypeHit);
	void ResetBloodmask();
	void ReceiveBeginPlay();
	void ExecuteUbergraph_BP_IONKnife(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
