// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956
// (BlueprintEvent)

void UABP_M1014_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956");

	UABP_M1014_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C
// (BlueprintEvent)

void UABP_M1014_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C");

	UABP_M1014_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_M1014.ABP_M1014_C.BlueprintUpdateAnimation
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaTimeX                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_M1014_C::BlueprintUpdateAnimation(float* DeltaTimeX)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M1014.ABP_M1014_C.BlueprintUpdateAnimation");

	UABP_M1014_C_BlueprintUpdateAnimation_Params params;
	params.DeltaTimeX = DeltaTimeX;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_M1014.ABP_M1014_C.ExecuteUbergraph_ABP_M1014
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_M1014_C::ExecuteUbergraph_ABP_M1014(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_M1014.ABP_M1014_C.ExecuteUbergraph_ABP_M1014");

	UABP_M1014_C_ExecuteUbergraph_ABP_M1014_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
