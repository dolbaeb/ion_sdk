// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapFailed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_ScrapDialouge_C::ScrapFailed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapFailed");

	UWBP_ScrapDialouge_C_ScrapFailed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapSuccess
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            CreditsReceived                (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScrapDialouge_C::ScrapSuccess(int CreditsReceived)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapSuccess");

	UWBP_ScrapDialouge_C_ScrapSuccess_Params params;
	params.CreditsReceived = CreditsReceived;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.SetItem
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  ItemToScrap                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_ScrapDialouge_C::SetItem(const struct FIONSteamInventoryItem& ItemToScrap)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.SetItem");

	UWBP_ScrapDialouge_C_SetItem_Params params;
	params.ItemToScrap = ItemToScrap;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_ScrapDialouge_C::BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature");

	UWBP_ScrapDialouge_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_ScrapDialouge_C::BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature");

	UWBP_ScrapDialouge_C_BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_ScrapDialouge_C::BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature");

	UWBP_ScrapDialouge_C_BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ExecuteUbergraph_WBP_ScrapDialouge
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScrapDialouge_C::ExecuteUbergraph_WBP_ScrapDialouge(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ExecuteUbergraph_WBP_ScrapDialouge");

	UWBP_ScrapDialouge_C_ExecuteUbergraph_WBP_ScrapDialouge_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
