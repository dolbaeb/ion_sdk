#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C
// 0x0018 (0x0390 - 0x0378)
class ABP_WearableMannequinAdvanced_C : public AIONWearableMannequinAdvanced
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0378(0x0008) (Transient, DuplicateTransient)
	float                                              CharacterSpin_NewTrack_0_725D74C8411CD3FB326D1390ED1B5AD7;// 0x0380(0x0004) (ZeroConstructor, IsPlainOldData)
	TEnumAsByte<ETimelineDirection>                    CharacterSpin__Direction_725D74C8411CD3FB326D1390ED1B5AD7;// 0x0384(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0385(0x0003) MISSED OFFSET
	class UTimelineComponent*                          CharacterSpin;                                            // 0x0388(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C");
		return ptr;
	}


	void UserConstructionScript();
	void CharacterSpin__FinishedFunc();
	void CharacterSpin__UpdateFunc();
	void ExecuteUbergraph_BP_WearableMannequinAdvanced(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
