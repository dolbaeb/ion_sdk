#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.PreConstruct
struct UWBP_MainMenuSettingsButton_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature
struct UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.ExecuteUbergraph_WBP_MainMenuSettingsButton
struct UWBP_MainMenuSettingsButton_C_ExecuteUbergraph_WBP_MainMenuSettingsButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.OnClicked__DelegateSignature
struct UWBP_MainMenuSettingsButton_C_OnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
