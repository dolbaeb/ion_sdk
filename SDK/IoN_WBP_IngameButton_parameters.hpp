#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
struct UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
struct UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_IngameButton.WBP_IngameButton_C.BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature
struct UWBP_IngameButton_C_BndEvt__Button_385_K2Node_ComponentBoundEvent_142_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_IngameButton.WBP_IngameButton_C.PreConstruct
struct UWBP_IngameButton_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_IngameButton.WBP_IngameButton_C.ExecuteUbergraph_WBP_IngameButton
struct UWBP_IngameButton_C_ExecuteUbergraph_WBP_IngameButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_IngameButton.WBP_IngameButton_C.OnClicked__DelegateSignature
struct UWBP_IngameButton_C_OnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
