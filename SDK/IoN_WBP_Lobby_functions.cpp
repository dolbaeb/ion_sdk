// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Lobby.WBP_Lobby_C.RemovePartyMember
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_Lobby_C::RemovePartyMember(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.RemovePartyMember");

	UWBP_Lobby_C_RemovePartyMember_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.AddPartyMember
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_Lobby_C::AddPartyMember(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.AddPartyMember");

	UWBP_Lobby_C_AddPartyMember_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.AddBtnClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Lobby_C::AddBtnClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.AddBtnClicked");

	UWBP_Lobby_C_AddBtnClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.RefreshMembers
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_Lobby_C::RefreshMembers()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.RefreshMembers");

	UWBP_Lobby_C_RefreshMembers_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Lobby_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.Tick");

	UWBP_Lobby_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.ExecuteUbergraph_WBP_Lobby
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Lobby_C::ExecuteUbergraph_WBP_Lobby(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.ExecuteUbergraph_WBP_Lobby");

	UWBP_Lobby_C_ExecuteUbergraph_WBP_Lobby_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Lobby.WBP_Lobby_C.OpenFriendsListEvent__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_Lobby_C::OpenFriendsListEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Lobby.WBP_Lobby_C.OpenFriendsListEvent__DelegateSignature");

	UWBP_Lobby_C_OpenFriendsListEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
