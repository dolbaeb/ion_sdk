// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function Mlv_MainMenu.Mlv_MainMenu_C.GetFilledPartySlot
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class ABP_PartySpawn_C*        FilledSlot                     (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void AMlv_MainMenu_C::GetFilledPartySlot(class ABP_PartySpawn_C** FilledSlot)
{
	static auto fn = UObject::FindObject<UFunction>("Function Mlv_MainMenu.Mlv_MainMenu_C.GetFilledPartySlot");

	AMlv_MainMenu_C_GetFilledPartySlot_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (FilledSlot != nullptr)
		*FilledSlot = params.FilledSlot;
}


// Function Mlv_MainMenu.Mlv_MainMenu_C.ReOrderPartyMembers
// (Public, BlueprintCallable, BlueprintEvent)

void AMlv_MainMenu_C::ReOrderPartyMembers()
{
	static auto fn = UObject::FindObject<UFunction>("Function Mlv_MainMenu.Mlv_MainMenu_C.ReOrderPartyMembers");

	AMlv_MainMenu_C_ReOrderPartyMembers_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberLeave
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void AMlv_MainMenu_C::OnPartyMemberLeave(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberLeave");

	AMlv_MainMenu_C_OnPartyMemberLeave_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberJoined
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer      PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void AMlv_MainMenu_C::OnPartyMemberJoined(const struct FMatchmakingPlayer& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberJoined");

	AMlv_MainMenu_C_OnPartyMemberJoined_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
