#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function Mlv_MainMenu.Mlv_MainMenu_C.GetFilledPartySlot
struct AMlv_MainMenu_C_GetFilledPartySlot_Params
{
	class ABP_PartySpawn_C*                            FilledSlot;                                               // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function Mlv_MainMenu.Mlv_MainMenu_C.ReOrderPartyMembers
struct AMlv_MainMenu_C_ReOrderPartyMembers_Params
{
};

// Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberLeave
struct AMlv_MainMenu_C_OnPartyMemberLeave_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function Mlv_MainMenu.Mlv_MainMenu_C.OnPartyMemberJoined
struct AMlv_MainMenu_C_OnPartyMemberJoined_Params
{
	struct FMatchmakingPlayer                          PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
