// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_BulletShake_AK47_3.BP_BulletShake_AK47_2_C.NewFunction_1
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_BulletShake_AK47_2_C::NewFunction_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_BulletShake_AK47_3.BP_BulletShake_AK47_2_C.NewFunction_1");

	UBP_BulletShake_AK47_2_C_NewFunction_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
