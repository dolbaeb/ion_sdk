#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_BulletShake_Sako85_3.BP_BulletShake_Sako85_2_C
// 0x0000 (0x0170 - 0x0170)
class UBP_BulletShake_Sako85_2_C : public UCameraShake
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_BulletShake_Sako85_3.BP_BulletShake_Sako85_2_C");
		return ptr;
	}


	void NewFunction_1();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
