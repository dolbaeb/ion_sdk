#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_AK47.BP_AK47_C.UserConstructionScript
struct ABP_AK47_C_UserConstructionScript_Params
{
};

// Function BP_AK47.BP_AK47_C.BPEvent_OnPickedUp
struct ABP_AK47_C_BPEvent_OnPickedUp_Params
{
	class AIONCharacter**                              Character;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_AK47.BP_AK47_C.BPEvent_ApplyWeaponSkinMaterial
struct ABP_AK47_C_BPEvent_ApplyWeaponSkinMaterial_Params
{
};

// Function BP_AK47.BP_AK47_C.ExecuteUbergraph_BP_AK47
struct ABP_AK47_C_ExecuteUbergraph_BP_AK47_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
