// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_LastMatchBox_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseButtonDown");

	UWBP_LastMatchBox_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_Highlight_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_LastMatchBox_C::Get_Highlight_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_Highlight_Visibility_1");

	UWBP_LastMatchBox_C_Get_Highlight_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Mode_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Mode_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Mode_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Mode_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Score_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Score_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Score_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Score_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_LastMatchBox_C::Get_TextBlock_KilledBy_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Visibility_1");

	UWBP_LastMatchBox_C_Get_TextBlock_KilledBy_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledByWeapon_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_KilledByWeapon_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledByWeapon_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_KilledByWeapon_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Date_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Date_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Date_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Date_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Time_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Time_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Time_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Time_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_MapName_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_MapName_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_MapName_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_MapName_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_KilledBy_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_KilledBy_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Duration_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Duration_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Duration_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Duration_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_PlacementSuffix_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_PlacementSuffix_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_PlacementSuffix_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_PlacementSuffix_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Placement_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_Placement_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Placement_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_Placement_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_NumKills_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_LastMatchBox_C::Get_TextBlock_NumKills_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_NumKills_Text_1");

	UWBP_LastMatchBox_C_Get_TextBlock_NumKills_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnResponse_451DE5964F9B8049D003B7840A2EFAB3
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FPlayerMatchHistory     PlayerMatchHistory             (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           bHasErrors                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LastMatchBox_C::OnResponse_451DE5964F9B8049D003B7840A2EFAB3(const struct FPlayerMatchHistory& PlayerMatchHistory, bool bHasErrors)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnResponse_451DE5964F9B8049D003B7840A2EFAB3");

	UWBP_LastMatchBox_C_OnResponse_451DE5964F9B8049D003B7840A2EFAB3_Params params;
	params.PlayerMatchHistory = PlayerMatchHistory;
	params.bHasErrors = bHasErrors;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseEnter
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_LastMatchBox_C::OnMouseEnter(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseEnter");

	UWBP_LastMatchBox_C_OnMouseEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_LastMatchBox_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseLeave");

	UWBP_LastMatchBox_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_LastMatchBox_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Construct");

	UWBP_LastMatchBox_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Connected
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                UserId                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString*                AuthToken                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_LastMatchBox_C::Connected(struct FString* UserId, struct FString* AuthToken)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Connected");

	UWBP_LastMatchBox_C_Connected_Params params;
	params.UserId = UserId;
	params.AuthToken = AuthToken;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_LastMatchBox_C::BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature");

	UWBP_LastMatchBox_C_BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Reconnect
// (BlueprintCallable, BlueprintEvent)

void UWBP_LastMatchBox_C::Reconnect()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.Reconnect");

	UWBP_LastMatchBox_C_Reconnect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LastMatchBox.WBP_LastMatchBox_C.ExecuteUbergraph_WBP_LastMatchBox
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LastMatchBox_C::ExecuteUbergraph_WBP_LastMatchBox(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LastMatchBox.WBP_LastMatchBox_C.ExecuteUbergraph_WBP_LastMatchBox");

	UWBP_LastMatchBox_C_ExecuteUbergraph_WBP_LastMatchBox_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
