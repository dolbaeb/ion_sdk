#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_IonArmsArmor.BP_IonArmsArmor_C.UserConstructionScript
struct ABP_IonArmsArmor_C_UserConstructionScript_Params
{
};

// Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_AttachWearable
struct ABP_IonArmsArmor_C_BPEvent_AttachWearable_Params
{
};

// Function BP_IonArmsArmor.BP_IonArmsArmor_C.BPEvent_DetachWearable
struct ABP_IonArmsArmor_C_BPEvent_DetachWearable_Params
{
};

// Function BP_IonArmsArmor.BP_IonArmsArmor_C.ExecuteUbergraph_BP_IonArmsArmor
struct ABP_IonArmsArmor_C_ExecuteUbergraph_BP_IonArmsArmor_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
