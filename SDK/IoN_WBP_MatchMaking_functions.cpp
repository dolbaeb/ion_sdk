// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchMaking.WBP_MatchMaking_C.GetVisibility_Throbber
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_MatchMaking_C::GetVisibility_Throbber()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchMaking.WBP_MatchMaking_C.GetVisibility_Throbber");

	UWBP_MatchMaking_C_GetVisibility_Throbber_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchMaking.WBP_MatchMaking_C.UpdatePlayButtonImage
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MatchMaking_C::UpdatePlayButtonImage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchMaking.WBP_MatchMaking_C.UpdatePlayButtonImage");

	UWBP_MatchMaking_C_UpdatePlayButtonImage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseEnter
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_MatchMaking_C::OnMouseEnter(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseEnter");

	UWBP_MatchMaking_C_OnMouseEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_MatchMaking_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseLeave");

	UWBP_MatchMaking_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchMaking.WBP_MatchMaking_C.ExecuteUbergraph_WBP_MatchMaking
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchMaking_C::ExecuteUbergraph_WBP_MatchMaking(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchMaking.WBP_MatchMaking_C.ExecuteUbergraph_WBP_MatchMaking");

	UWBP_MatchMaking_C_ExecuteUbergraph_WBP_MatchMaking_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
