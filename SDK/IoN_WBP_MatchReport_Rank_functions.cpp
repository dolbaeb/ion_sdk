// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Get_BadgeText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Rank_C::Get_BadgeText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Get_BadgeText_Text_1");

	UWBP_MatchReport_Rank_C_Get_BadgeText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Begin Animation
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Begin_Animation()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Begin Animation");

	UWBP_MatchReport_Rank_C_Begin_Animation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MatchReport_Rank_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnAnimationFinished");

	UWBP_MatchReport_Rank_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Appear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Appear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Appear");

	UWBP_MatchReport_Rank_C_Appear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Disappear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Disappear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Disappear");

	UWBP_MatchReport_Rank_C_Disappear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Construct");

	UWBP_MatchReport_Rank_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnSetMatchHistory
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FPlayerMatchHistory     MatchHistory                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_MatchReport_Rank_C::OnSetMatchHistory(const struct FPlayerMatchHistory& MatchHistory)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnSetMatchHistory");

	UWBP_MatchReport_Rank_C_OnSetMatchHistory_Params params;
	params.MatchHistory = MatchHistory;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnReportClicked
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::OnReportClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnReportClicked");

	UWBP_MatchReport_Rank_C_OnReportClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Set Final Rank State
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Set_Final_Rank_State()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Set Final Rank State");

	UWBP_MatchReport_Rank_C_Set_Final_Rank_State_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Fast Forward
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::Fast_Forward()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Fast Forward");

	UWBP_MatchReport_Rank_C_Fast_Forward_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_Rank_C::BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature");

	UWBP_MatchReport_Rank_C_BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.CheckRankChange
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::CheckRankChange()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.CheckRankChange");

	UWBP_MatchReport_Rank_C_CheckRankChange_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.ExecuteUbergraph_WBP_MatchReport_Rank
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_Rank_C::ExecuteUbergraph_WBP_MatchReport_Rank(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.ExecuteUbergraph_WBP_MatchReport_Rank");

	UWBP_MatchReport_Rank_C_ExecuteUbergraph_WBP_MatchReport_Rank_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.AnimationFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Rank_C::AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.AnimationFinished__DelegateSignature");

	UWBP_MatchReport_Rank_C_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
