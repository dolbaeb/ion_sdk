#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_KeyCombination.BP_KeyCombination_C
// 0x0070 (0x00A0 - 0x0030)
class UBP_KeyCombination_C : public UObject
{
public:
	struct FString                                     Name;                                                     // 0x0030(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	TArray<class UBP_KeyInput_C*>                      Key_Inputs;                                               // 0x0040(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FSKeyInput>                          Default_Combination;                                      // 0x0050(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	bool                                               Can_t_Be_None;                                            // 0x0060(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0061(0x0007) MISSED OFFSET
	TArray<class UBP_KeyConflict_C*>                   Conflicting_Mappings_Blocked;                             // 0x0068(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	TArray<class UBP_KeyConflict_C*>                   Conflicting_Mappings_Info;                                // 0x0078(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	struct FScriptMulticastDelegate                    Combination_Updated;                                      // 0x0088(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	class UBP_KeyMapping_C*                            Parent_Mapping;                                           // 0x0098(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_KeyCombination.BP_KeyCombination_C");
		return ptr;
	}


	void Get_Key_Combination_Display_Name(const struct FString& Separator, const struct FString& No_Key_Display, TEnumAsByte<EKeyCombinationDisplay> Display_Type, struct FString* Display_Name);
	void Add_Key_Input(const struct FSKeyInput& InputPin, class UBP_KeyInput_C** Input);
	void Equal_All_Keys(TArray<struct FSKeyInput>* Combination, bool* Result);
	void Equal_All_Conflicts(TArray<struct FSKeyConflict>* Conflicts, bool* Result);
	void Evaluate_Blocking_Combinations(class APlayerController* Player_Controller, bool* Is_Active, bool* Just_Pressed, bool* Just_Released);
	void Clear_Conflicting_Combinations();
	void Add_Conflicting_Combination(class UBP_KeyCombination_C* Conflicted_Combination, TEnumAsByte<EKeyConflict> Conflicted_);
	void Detect_Conflict(class UBP_KeyCombination_C* Input_Combination, TEnumAsByte<EKeyConflict>* Conflict_Type);
	void Replace_Key_Combination(TArray<struct FSKeyInput>* Key_Combination);
	void Load_Key_Combination(class UBP_GameSettings_C* Game_Settings, const struct FString& Action_Name, const struct FString& Category, const struct FString& Name, bool Primary);
	void Save_Key_Combination(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave);
	void Key_Combination_Current_State(class APlayerController* Player_Controller, bool Ignore_Conflicts, float* Axis_Value, bool* Is_Active, bool* Just_Pressed, bool* Just_Released);
	void Init_Key_Combination(const struct FString& Name, bool Can_t_Be_None, TArray<struct FSKeyInput>* Key_Combination, class UBP_KeyCombination_C** Combination);
	void Combination_Updated__DelegateSignature(class UBP_KeyCombination_C* Combination);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
