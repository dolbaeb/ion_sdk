#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_ColorAndOpacity_1
struct UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Icon_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_Brush_1
struct UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Icon_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Backfill_ColorAndOpacity_1
struct UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Backfill_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Tick
struct UWBP_PlayerStatus_EquipmentSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot
struct UWBP_PlayerStatus_EquipmentSlot_C_ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
