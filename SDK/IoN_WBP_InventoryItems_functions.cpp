// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InventoryItems.WBP_InventoryItems_C.DeselectItem
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItems_C::DeselectItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.DeselectItem");

	UWBP_InventoryItems_C_DeselectItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.SteamUpdatedItems
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItems_C::SteamUpdatedItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.SteamUpdatedItems");

	UWBP_InventoryItems_C_SteamUpdatedItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.SetSteamTypeFilter
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UClass*                  NewFilter                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItems_C::SetSteamTypeFilter(class UClass* NewFilter)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.SetSteamTypeFilter");

	UWBP_InventoryItems_C_SetSteamTypeFilter_Params params;
	params.NewFilter = NewFilter;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.SetItemFilter
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UClass*                  ItemFilter                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItems_C::SetItemFilter(class UClass* ItemFilter)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.SetItemFilter");

	UWBP_InventoryItems_C_SetItemFilter_Params params;
	params.ItemFilter = ItemFilter;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.NewItemSelected
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_Icon_C*             SelectedBtn                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_InventoryItems_C::NewItemSelected(class UWBP_Icon_C* SelectedBtn)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.NewItemSelected");

	UWBP_InventoryItems_C_NewItemSelected_Params params;
	params.SelectedBtn = SelectedBtn;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.FillInventoryWithItems
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItems_C::FillInventoryWithItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.FillInventoryWithItems");

	UWBP_InventoryItems_C_FillInventoryWithItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.RefreshItems
// (BlueprintCallable, BlueprintEvent)

void UWBP_InventoryItems_C::RefreshItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.RefreshItems");

	UWBP_InventoryItems_C_RefreshItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_InventoryItems_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.Construct");

	UWBP_InventoryItems_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.ExecuteUbergraph_WBP_InventoryItems
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryItems_C::ExecuteUbergraph_WBP_InventoryItems(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.ExecuteUbergraph_WBP_InventoryItems");

	UWBP_InventoryItems_C_ExecuteUbergraph_WBP_InventoryItems_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryItems.WBP_InventoryItems_C.ItemSelected__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_InventoryItems_C::ItemSelected__DelegateSignature(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryItems.WBP_InventoryItems_C.ItemSelected__DelegateSignature");

	UWBP_InventoryItems_C_ItemSelected__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
