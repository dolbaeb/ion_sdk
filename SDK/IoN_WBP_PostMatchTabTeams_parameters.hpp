#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Kills
struct UWBP_PostMatchTabTeams_C_Sort_by_Kills_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Place
struct UWBP_PostMatchTabTeams_C_Sort_by_Place_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Team
struct UWBP_PostMatchTabTeams_C_Sort_by_Team_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Damage
struct UWBP_PostMatchTabTeams_C_Sort_by_Damage_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Accuracy
struct UWBP_PostMatchTabTeams_C_Sort_by_Accuracy_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Time Alive
struct UWBP_PostMatchTabTeams_C_Sort_by_Time_Alive_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Sort by Headshot Accuracy
struct UWBP_PostMatchTabTeams_C_Sort_by_Headshot_Accuracy_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Minimize All Tabs
struct UWBP_PostMatchTabTeams_C_Minimize_All_Tabs_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Team Widgets
struct UWBP_PostMatchTabTeams_C_Create_Team_Widgets_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Construct
struct UWBP_PostMatchTabTeams_C_Construct_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Change Mode
struct UWBP_PostMatchTabTeams_C_Change_Mode_Params
{
	bool                                               Show_Teams;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Create Solo Players
struct UWBP_PostMatchTabTeams_C_Create_Solo_Players_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.Repopulate Fields
struct UWBP_PostMatchTabTeams_C_Repopulate_Fields_Params
{
};

// Function WBP_PostMatchTabTeams.WBP_PostMatchTabTeams_C.ExecuteUbergraph_WBP_PostMatchTabTeams
struct UWBP_PostMatchTabTeams_C_ExecuteUbergraph_WBP_PostMatchTabTeams_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
