// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_KeyInput_Digital.BP_KeyInput_Digital_C.Key Input Current State
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController**      Controller                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Axis_Value                     (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Down                           (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyInput_Digital_C::Key_Input_Current_State(class APlayerController** Controller, float* Axis_Value, bool* Down, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput_Digital.BP_KeyInput_Digital_C.Key Input Current State");

	UBP_KeyInput_Digital_C_Key_Input_Current_State_Params params;
	params.Controller = Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Axis_Value != nullptr)
		*Axis_Value = params.Axis_Value;
	if (Down != nullptr)
		*Down = params.Down;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
