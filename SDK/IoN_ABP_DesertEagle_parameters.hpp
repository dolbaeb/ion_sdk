#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_DesertEagle.ABP_DesertEagle_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4
struct UABP_DesertEagle_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4_Params
{
};

// Function ABP_DesertEagle.ABP_DesertEagle_C.BlueprintUpdateAnimation
struct UABP_DesertEagle_C_BlueprintUpdateAnimation_Params
{
	float*                                             DeltaTimeX;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function ABP_DesertEagle.ABP_DesertEagle_C.ExecuteUbergraph_ABP_DesertEagle
struct UABP_DesertEagle_C_ExecuteUbergraph_ABP_DesertEagle_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
