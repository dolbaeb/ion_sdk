#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragFinished
struct UWBP_InventorySlotBase_C_OnDragFinished_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.CancelDropHover
struct UWBP_InventorySlotBase_C_CancelDropHover_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemRightClicked
struct UWBP_InventorySlotBase_C_OnItemRightClicked_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ItemIsEquipped
struct UWBP_InventorySlotBase_C_ItemIsEquipped_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.EquipItem
struct UWBP_InventorySlotBase_C_EquipItem_Params
{
	int                                                Slot;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetAnimations
struct UWBP_InventorySlotBase_C_SetAnimations_Params
{
	class UWidgetAnimation*                            HoverAnimation;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UWidgetAnimation*                            DropHoverAnimation;                                       // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UWidgetAnimation*                            AcceptDropAnimation;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UWidgetAnimation*                            ActiveAnimation;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.SetActive
struct UWBP_InventorySlotBase_C_SetActive_Params
{
	bool                                               Active;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.AcceptsDropForItem
struct UWBP_InventorySlotBase_C_AcceptsDropForItem_Params
{
	class AIONItem*                                    Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnUnhovered
struct UWBP_InventorySlotBase_C_OnUnhovered_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnHovered
struct UWBP_InventorySlotBase_C_OnHovered_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.HasValidItem
struct UWBP_InventorySlotBase_C_HasValidItem_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetDragItemVisual
struct UWBP_InventorySlotBase_C_GetDragItemVisual_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemClicked
struct UWBP_InventorySlotBase_C_OnItemClicked_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonUp
struct UWBP_InventorySlotBase_C_OnMouseButtonUp_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseButtonDown
struct UWBP_InventorySlotBase_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnItemChanged
struct UWBP_InventorySlotBase_C_OnItemChanged_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.GetCurrentItem
struct UWBP_InventorySlotBase_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragDetected
struct UWBP_InventorySlotBase_C_OnDragDetected_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	class UDragDropOperation*                          Operation;                                                // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnFinishedItemDrag
struct UWBP_InventorySlotBase_C_OnFinishedItemDrag_Params
{
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnBeginItemDrag
struct UWBP_InventorySlotBase_C_OnBeginItemDrag_Params
{
	class AIONItem*                                    Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.Tick
struct UWBP_InventorySlotBase_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseLeave
struct UWBP_InventorySlotBase_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnMouseEnter
struct UWBP_InventorySlotBase_C_OnMouseEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragEnter
struct UWBP_InventorySlotBase_C_OnDragEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragLeave
struct UWBP_InventorySlotBase_C_OnDragLeave_Params
{
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.OnDragCancelled
struct UWBP_InventorySlotBase_C_OnDragCancelled_Params
{
	struct FPointerEvent*                              PointerEvent;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventorySlotBase.WBP_InventorySlotBase_C.ExecuteUbergraph_WBP_InventorySlotBase
struct UWBP_InventorySlotBase_C_ExecuteUbergraph_WBP_InventorySlotBase_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
