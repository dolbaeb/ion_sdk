#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_MainCharacter.BP_MainCharacter_C
// 0x0070 (0x0D10 - 0x0CA0)
class ABP_MainCharacter_C : public AIONCharacter
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0CA0(0x0008) (Transient, DuplicateTransient)
	float                                              Timeline_1_NewTrack_0_5E89164944CFCE9296583DBECDE738CF;   // 0x0CA8(0x0004) (ZeroConstructor, IsPlainOldData)
	TEnumAsByte<ETimelineDirection>                    Timeline_1__Direction_5E89164944CFCE9296583DBECDE738CF;   // 0x0CAC(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0CAD(0x0003) MISSED OFFSET
	class UTimelineComponent*                          Timeline_2;                                               // 0x0CB0(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              Timeline_0_PhysicsWeight_CE55B56A4F99F14BB35ABD9690058BCD;// 0x0CB8(0x0004) (ZeroConstructor, IsPlainOldData)
	TEnumAsByte<ETimelineDirection>                    Timeline_0__Direction_CE55B56A4F99F14BB35ABD9690058BCD;   // 0x0CBC(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0CBD(0x0003) MISSED OFFSET
	class UTimelineComponent*                          Timeline_1;                                               // 0x0CC0(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              CosmeticDamage;                                           // 0x0CC8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x0CCC(0x0004) MISSED OFFSET
	struct FName                                       HitBone;                                                  // 0x0CD0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     ShotImpulse;                                              // 0x0CD8(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x0CE4(0x0004) MISSED OFFSET
	class UMaterialInstanceDynamic*                    BlurPPMaterialInstance;                                   // 0x0CE8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ImpactSoundDelay;                                         // 0x0CF0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Blur_Multiplier;                                          // 0x0CF4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UMaterialInstanceDynamic*                    HorzPass;                                                 // 0x0CF8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UMaterialInstanceDynamic*                    VertPass;                                                 // 0x0D00(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              PlayRateHeroSlamAnimation;                                // 0x0D08(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DelayBeforeInputReset;                                    // 0x0D0C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_MainCharacter.BP_MainCharacter_C");
		return ptr;
	}


	void ShowMapToolTip();
	void GetBoneForHitSim(const struct FName& BoneName, struct FName* OutBone);
	void UserConstructionScript();
	void Timeline_0__FinishedFunc();
	void Timeline_0__UpdateFunc();
	void Timeline_1__FinishedFunc();
	void Timeline_1__UpdateFunc();
	void BeginPlayDebug();
	void SetDropsuitMaterial(class UMaterialInterface* Mat);
	void CamBlur(float Strength);
	void EventBP_DropInModeStarted();
	void LandingFX();
	void EventBP_LandingTrigger();
	void Broadcast_LandingTrigger();
	void BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult);
	void BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex);
	void UpdateSuppressionEffects(float* SuppressionRatio);
	void UpdateHealthEffects(float* HealthRatio, float* OldHealthRatio);
	void ReceiveBeginPlay();
	void BP_SimulatedHit(struct FName* BoneName, float* DamageImpulse, struct FVector* ShotFromDirection);
	void OnBulletSuppression(class AMainProjectile** Projectile);
	void OnAimingStateChanged(float* NewAimRatio, bool* bForce, class AIONWeapon** Weapon);
	void InventoryStateChanged(bool* bOpen);
	void BP_ProceduralFireAnimation();
	void OnHardLanding();
	void ExecuteUbergraph_BP_MainCharacter(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
