#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_DragItemOperation.BP_DragItemOperation_C.Drop
struct UBP_DragItemOperation_C_Drop_Params
{
	struct FPointerEvent*                              PointerEvent;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_DragItemOperation.BP_DragItemOperation_C.DragCancelled
struct UBP_DragItemOperation_C_DragCancelled_Params
{
	struct FPointerEvent*                              PointerEvent;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_DragItemOperation.BP_DragItemOperation_C.ExecuteUbergraph_BP_DragItemOperation
struct UBP_DragItemOperation_C_ExecuteUbergraph_BP_DragItemOperation_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_DragItemOperation.BP_DragItemOperation_C.DragFinished__DelegateSignature
struct UBP_DragItemOperation_C_DragFinished__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
