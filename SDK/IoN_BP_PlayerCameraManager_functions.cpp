// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.PhotographyCameraModify
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FVector*                NewCameraLocation              (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FVector*                PreviousCameraLocation         (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FVector*                OriginalCameraLocation         (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FVector                 ResultCameraLocation           (Parm, OutParm, IsPlainOldData)

void ABP_PlayerCameraManager_C::PhotographyCameraModify(struct FVector* NewCameraLocation, struct FVector* PreviousCameraLocation, struct FVector* OriginalCameraLocation, struct FVector* ResultCameraLocation)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.PhotographyCameraModify");

	ABP_PlayerCameraManager_C_PhotographyCameraModify_Params params;
	params.NewCameraLocation = NewCameraLocation;
	params.PreviousCameraLocation = PreviousCameraLocation;
	params.OriginalCameraLocation = OriginalCameraLocation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (ResultCameraLocation != nullptr)
		*ResultCameraLocation = params.ResultCameraLocation;
}


// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_PlayerCameraManager_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.UserConstructionScript");

	ABP_PlayerCameraManager_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionStart
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void ABP_PlayerCameraManager_C::OnPhotographySessionStart()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionStart");

	ABP_PlayerCameraManager_C_OnPhotographySessionStart_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionEnd
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void ABP_PlayerCameraManager_C::OnPhotographySessionEnd()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionEnd");

	ABP_PlayerCameraManager_C_OnPhotographySessionEnd_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.ExecuteUbergraph_BP_PlayerCameraManager
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PlayerCameraManager_C::ExecuteUbergraph_BP_PlayerCameraManager(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.ExecuteUbergraph_BP_PlayerCameraManager");

	ABP_PlayerCameraManager_C_ExecuteUbergraph_BP_PlayerCameraManager_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
