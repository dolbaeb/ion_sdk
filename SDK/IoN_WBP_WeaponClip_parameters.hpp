#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Brush_1
struct UWBP_WeaponClip_C_Get_Image_FallbackWeapon_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Visibility_1
struct UWBP_WeaponClip_C_Get_Image_FallbackWeapon_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetVisibility_Firearm
struct UWBP_WeaponClip_C_GetVisibility_Firearm_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Firemode
struct UWBP_WeaponClip_C_GetBrush_Firemode_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Firemode
struct UWBP_WeaponClip_C_GetText_Firemode_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Weapon
struct UWBP_WeaponClip_C_GetBrush_Weapon_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_ExtraAmmo
struct UWBP_WeaponClip_C_GetText_ExtraAmmo_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Clip
struct UWBP_WeaponClip_C_GetText_Clip_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.Tick
struct UWBP_WeaponClip_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.Reset
struct UWBP_WeaponClip_C_Reset_Params
{
};

// Function WBP_WeaponClip.WBP_WeaponClip_C.ExecuteUbergraph_WBP_WeaponClip
struct UWBP_WeaponClip_C_ExecuteUbergraph_WBP_WeaponClip_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
