#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_Visibility
struct UWBP_SettingsSectionButton_C_Get_TextBlock_Label_Visibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_Button_Visibility
struct UWBP_SettingsSectionButton_C_Get_Button_Visibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_ColorAndOpacity_1
struct UWBP_SettingsSectionButton_C_Get_TextBlock_Label_ColorAndOpacity_1_Params
{
	struct FSlateColor                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.GetbIsEnabled_1
struct UWBP_SettingsSectionButton_C_GetbIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature
struct UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature
struct UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.PreConstruct
struct UWBP_SettingsSectionButton_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.ExecuteUbergraph_WBP_SettingsSectionButton
struct UWBP_SettingsSectionButton_C_ExecuteUbergraph_WBP_SettingsSectionButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.OnClicked__DelegateSignature
struct UWBP_SettingsSectionButton_C_OnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
