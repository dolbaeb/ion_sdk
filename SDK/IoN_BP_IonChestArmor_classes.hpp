#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_IonChestArmor.BP_IonChestArmor_C
// 0x0008 (0x0488 - 0x0480)
class ABP_IonChestArmor_C : public ABP_ArmorChest_C
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0480(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_IonChestArmor.BP_IonChestArmor_C");
		return ptr;
	}


	void UserConstructionScript();
	void BPEvent_AttachWearable();
	void BPEvent_DetachWearable();
	void ExecuteUbergraph_BP_IonChestArmor(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
