#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_TacticalCompass.WBP_TacticalCompass_C.GetMarkerForPlayer
struct UWBP_TacticalCompass_C_GetMarkerForPlayer_Params
{
	class AIONPlayerState*                             Player;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class UWBP_TacticalCompassMarker_C*                Array_Element;                                            // (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_TacticalCompass.WBP_TacticalCompass_C.Construct
struct UWBP_TacticalCompass_C_Construct_Params
{
};

// Function WBP_TacticalCompass.WBP_TacticalCompass_C.Tick
struct UWBP_TacticalCompass_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TacticalCompass.WBP_TacticalCompass_C.ExecuteUbergraph_WBP_TacticalCompass
struct UWBP_TacticalCompass_C_ExecuteUbergraph_WBP_TacticalCompass_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
