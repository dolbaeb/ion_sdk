// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_MenuCtrl.BP_MenuCtrl_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_MenuCtrl_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.UserConstructionScript");

	ABP_MenuCtrl_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.BPEvent_ShowOKDialog
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void ABP_MenuCtrl_C::BPEvent_ShowOKDialog(struct FString* Message)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.BPEvent_ShowOKDialog");

	ABP_MenuCtrl_C_BPEvent_ShowOKDialog_Params params;
	params.Message = Message;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.ExecuteUbergraph_BP_MenuCtrl
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuCtrl_C::ExecuteUbergraph_BP_MenuCtrl(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.ExecuteUbergraph_BP_MenuCtrl");

	ABP_MenuCtrl_C_ExecuteUbergraph_BP_MenuCtrl_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPopulateCustomServers__DelegateSignature
// (Public, Delegate, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FIONServerInfo>  servers                        (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void ABP_MenuCtrl_C::OnPopulateCustomServers__DelegateSignature(TArray<struct FIONServerInfo>* servers)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.OnPopulateCustomServers__DelegateSignature");

	ABP_MenuCtrl_C_OnPopulateCustomServers__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (servers != nullptr)
		*servers = params.servers;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPlayersReadyStatusesChanged__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void ABP_MenuCtrl_C::OnPlayersReadyStatusesChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.OnPlayersReadyStatusesChanged__DelegateSignature");

	ABP_MenuCtrl_C_OnPlayersReadyStatusesChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemberLeave__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuCtrl_C::OnPartyMemberLeave__DelegateSignature(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemberLeave__DelegateSignature");

	ABP_MenuCtrl_C_OnPartyMemberLeave__DelegateSignature_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemeberJoined__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuCtrl_C::OnPartyMemeberJoined__DelegateSignature(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemeberJoined__DelegateSignature");

	ABP_MenuCtrl_C_OnPartyMemeberJoined__DelegateSignature_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
