#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Get_DistanceText_Text_1
struct UWBP_TacticalCompassMarker_C_Get_DistanceText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.PreConstruct
struct UWBP_TacticalCompassMarker_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Construct
struct UWBP_TacticalCompassMarker_C_Construct_Params
{
};

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Tick
struct UWBP_TacticalCompassMarker_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.ExecuteUbergraph_WBP_TacticalCompassMarker
struct UWBP_TacticalCompassMarker_C_ExecuteUbergraph_WBP_TacticalCompassMarker_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
