// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Invite.WBP_Invite_C.GetbIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_Invite_C::GetbIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.GetbIsEnabled_1");

	UWBP_Invite_C_GetbIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Invite.WBP_Invite_C.OnGetMenuContent_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_Invite_C::OnGetMenuContent_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.OnGetMenuContent_1");

	UWBP_Invite_C_OnGetMenuContent_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Invite.WBP_Invite_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Invite_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.GetText_1");

	UWBP_Invite_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Invite.WBP_Invite_C.Get_Button_Invite_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_Invite_C::Get_Button_Invite_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.Get_Button_Invite_Visibility_1");

	UWBP_Invite_C_Get_Button_Invite_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Invite.WBP_Invite_C.GetColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateColor             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateColor UWBP_Invite_C::GetColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.GetColorAndOpacity_1");

	UWBP_Invite_C_GetColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Invite.WBP_Invite_C.AddToPartyBtnPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FBlueprintOnlineFriend  OnlineFriend                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_Invite_C::AddToPartyBtnPressed__DelegateSignature(const struct FBlueprintOnlineFriend& OnlineFriend)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Invite.WBP_Invite_C.AddToPartyBtnPressed__DelegateSignature");

	UWBP_Invite_C_AddToPartyBtnPressed__DelegateSignature_Params params;
	params.OnlineFriend = OnlineFriend;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
