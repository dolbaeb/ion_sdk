// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_KeyMapping.BP_KeyMapping_C.Revert To Default KeyMapping
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_KeyMapping_C::Revert_To_Default_KeyMapping()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyMapping.BP_KeyMapping_C.Revert To Default KeyMapping");

	UBP_KeyMapping_C_Revert_To_Default_KeyMapping_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyMapping.BP_KeyMapping_C.Load Key Mapping
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Action_Name                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 Category                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UBP_KeyMapping_C::Load_Key_Mapping(class UBP_GameSettings_C* Game_Settings, const struct FString& Action_Name, const struct FString& Category)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyMapping.BP_KeyMapping_C.Load Key Mapping");

	UBP_KeyMapping_C_Load_Key_Mapping_Params params;
	params.Game_Settings = Game_Settings;
	params.Action_Name = Action_Name;
	params.Category = Category;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyMapping.BP_KeyMapping_C.Save Key Mapping
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FSKeyActionSave         KeySave                        (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_KeyMapping_C::Save_Key_Mapping(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyMapping.BP_KeyMapping_C.Save Key Mapping");

	UBP_KeyMapping_C_Save_Key_Mapping_Params params;
	params.Game_Settings = Game_Settings;
	params.KeySave = KeySave;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyMapping.BP_KeyMapping_C.Key Mapping Current State
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController*       Player_Controller              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Mapping_Value                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Is_Active                      (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyMapping_C::Key_Mapping_Current_State(class APlayerController* Player_Controller, float* Mapping_Value, bool* Is_Active, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyMapping.BP_KeyMapping_C.Key Mapping Current State");

	UBP_KeyMapping_C_Key_Mapping_Current_State_Params params;
	params.Player_Controller = Player_Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Mapping_Value != nullptr)
		*Mapping_Value = params.Mapping_Value;
	if (Is_Active != nullptr)
		*Is_Active = params.Is_Active;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


// Function BP_KeyMapping.BP_KeyMapping_C.Init Key Mapping
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyMapping            Key_Mapping                    (BlueprintVisible, BlueprintReadOnly, Parm)
// class UBP_KeyMapping_C*        Mapping                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyMapping_C::Init_Key_Mapping(const struct FSKeyMapping& Key_Mapping, class UBP_KeyMapping_C** Mapping)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyMapping.BP_KeyMapping_C.Init Key Mapping");

	UBP_KeyMapping_C_Init_Key_Mapping_Params params;
	params.Key_Mapping = Key_Mapping;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Mapping != nullptr)
		*Mapping = params.Mapping;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
