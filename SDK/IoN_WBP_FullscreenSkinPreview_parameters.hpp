#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CheckRarityFX
struct UWBP_FullscreenSkinPreview_C_CheckRarityFX_Params
{
};

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.SetSteamItem
struct UWBP_FullscreenSkinPreview_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature
struct UWBP_FullscreenSkinPreview_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.ExecuteUbergraph_WBP_FullscreenSkinPreview
struct UWBP_FullscreenSkinPreview_C_ExecuteUbergraph_WBP_FullscreenSkinPreview_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CloseBtnPressed__DelegateSignature
struct UWBP_FullscreenSkinPreview_C_CloseBtnPressed__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
