// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Icon.WBP_Icon_C.ShowMainMenu
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::ShowMainMenu()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ShowMainMenu");

	UWBP_Icon_C_ShowMainMenu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ScrapBtnPressed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::ScrapBtnPressed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ScrapBtnPressed");

	UWBP_Icon_C_ScrapBtnPressed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ResetBtnPressed
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::ResetBtnPressed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ResetBtnPressed");

	UWBP_Icon_C_ResetBtnPressed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.InpsectBtnPressed
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::InpsectBtnPressed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.InpsectBtnPressed");

	UWBP_Icon_C_InpsectBtnPressed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.EquipBtnPressed
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::EquipBtnPressed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.EquipBtnPressed");

	UWBP_Icon_C_EquipBtnPressed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.GetToolTipWidget_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_Icon_C::GetToolTipWidget_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.GetToolTipWidget_1");

	UWBP_Icon_C_GetToolTipWidget_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Icon.WBP_Icon_C.GetSteamItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// struct FIONSteamInventoryItem  SteamItem                      (Parm, OutParm)

void UWBP_Icon_C::GetSteamItem(struct FIONSteamInventoryItem* SteamItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.GetSteamItem");

	UWBP_Icon_C_GetSteamItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (SteamItem != nullptr)
		*SteamItem = params.SteamItem;
}


// Function WBP_Icon.WBP_Icon_C.SetSteamItem
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  SteamItem                      (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_Icon_C::SetSteamItem(const struct FIONSteamInventoryItem& SteamItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.SetSteamItem");

	UWBP_Icon_C_SetSteamItem_Params params;
	params.SteamItem = SteamItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.SetIconTexture
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTexture2D*              NewTexture                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Icon_C::SetIconTexture(class UTexture2D* NewTexture)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.SetIconTexture");

	UWBP_Icon_C_SetIconTexture_Params params;
	params.NewTexture = NewTexture;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.SetIconName
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Name                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_Icon_C::SetIconName(const struct FString& Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.SetIconName");

	UWBP_Icon_C_SetIconName_Params params;
	params.Name = Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_Icon_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.OnMouseButtonDown");

	UWBP_Icon_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Icon.WBP_Icon_C.BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_Icon_C::BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature");

	UWBP_Icon_C_BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ChangeSelectionState
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bSelected                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Icon_C::ChangeSelectionState(bool bSelected)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ChangeSelectionState");

	UWBP_Icon_C_ChangeSelectionState_Params params;
	params.bSelected = bSelected;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ChangeEquipState
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bEquip                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Icon_C::ChangeEquipState(bool bEquip)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ChangeEquipState");

	UWBP_Icon_C_ChangeEquipState_Params params;
	params.bEquip = bEquip;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.BPEvent_EquipChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool*                          bEquipped                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Icon_C::BPEvent_EquipChanged(bool* bEquipped)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.BPEvent_EquipChanged");

	UWBP_Icon_C_BPEvent_EquipChanged_Params params;
	params.bEquipped = bEquipped;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ExecuteUbergraph_WBP_Icon
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Icon_C::ExecuteUbergraph_WBP_Icon(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ExecuteUbergraph_WBP_Icon");

	UWBP_Icon_C_ExecuteUbergraph_WBP_Icon_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ItemUnEquipped__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::ItemUnEquipped__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ItemUnEquipped__DelegateSignature");

	UWBP_Icon_C_ItemUnEquipped__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ItemEquipped__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_Icon_C::ItemEquipped__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ItemEquipped__DelegateSignature");

	UWBP_Icon_C_ItemEquipped__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Icon.WBP_Icon_C.ItemSelected__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_Icon_C*             BtnRef                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_Icon_C::ItemSelected__DelegateSignature(class UWBP_Icon_C* BtnRef)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Icon.WBP_Icon_C.ItemSelected__DelegateSignature");

	UWBP_Icon_C_ItemSelected__DelegateSignature_Params params;
	params.BtnRef = BtnRef;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
