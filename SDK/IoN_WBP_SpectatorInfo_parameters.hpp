#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetCardVisibility
struct UWBP_SpectatorInfo_C_GetCardVisibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponAmmo
struct UWBP_SpectatorInfo_C_GetWeaponAmmo_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponIconBrush
struct UWBP_SpectatorInfo_C_GetWeaponIconBrush_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetPlayerHealthPercentage
struct UWBP_SpectatorInfo_C_GetPlayerHealthPercentage_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Setup Widget
struct UWBP_SpectatorInfo_C_Setup_Widget_Params
{
	class AIONCharacter*                               Character;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Create Background MID
struct UWBP_SpectatorInfo_C_Create_Background_MID_Params
{
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Tick
struct UWBP_SpectatorInfo_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Reset Armour Icons
struct UWBP_SpectatorInfo_C_Reset_Armour_Icons_Params
{
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Update Armour Icons
struct UWBP_SpectatorInfo_C_Update_Armour_Icons_Params
{
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Change View Mode
struct UWBP_SpectatorInfo_C_Change_View_Mode_Params
{
	bool                                               Minimal_Mode_;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.ExecuteUbergraph_WBP_SpectatorInfo
struct UWBP_SpectatorInfo_C_ExecuteUbergraph_WBP_SpectatorInfo_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
