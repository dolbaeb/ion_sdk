#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.Construct
struct UWBP_ChatboxMessage_C_Construct_Params
{
};

// Function WBP_ChatboxMessage.WBP_ChatboxMessage_C.ExecuteUbergraph_WBP_ChatboxMessage
struct UWBP_ChatboxMessage_C_ExecuteUbergraph_WBP_ChatboxMessage_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
