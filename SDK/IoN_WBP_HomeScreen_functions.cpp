// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_HomeScreen.WBP_HomeScreen_C.OnFail_5A0F3C1F41374D769909778BE8D3E051
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTexture2DDynamic*       Texture                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_HomeScreen_C::OnFail_5A0F3C1F41374D769909778BE8D3E051(class UTexture2DDynamic* Texture)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.OnFail_5A0F3C1F41374D769909778BE8D3E051");

	UWBP_HomeScreen_C_OnFail_5A0F3C1F41374D769909778BE8D3E051_Params params;
	params.Texture = Texture;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.OnSuccess_5A0F3C1F41374D769909778BE8D3E051
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTexture2DDynamic*       Texture                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_HomeScreen_C::OnSuccess_5A0F3C1F41374D769909778BE8D3E051(class UTexture2DDynamic* Texture)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.OnSuccess_5A0F3C1F41374D769909778BE8D3E051");

	UWBP_HomeScreen_C_OnSuccess_5A0F3C1F41374D769909778BE8D3E051_Params params;
	params.Texture = Texture;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           bHasErrors                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_HomeScreen_C::OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00(const struct FString& Message, bool bHasErrors)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00");

	UWBP_HomeScreen_C_OnResponse_84A5EBF14671C3A10DAF28ADA7A05E00_Params params;
	params.Message = Message;
	params.bHasErrors = bHasErrors;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_A0F42E9749E8C110926964B48F717ABB
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 ImageURL                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 LinkURL                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           bHasErrors                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_HomeScreen_C::OnResponse_A0F42E9749E8C110926964B48F717ABB(const struct FString& ImageURL, const struct FString& LinkURL, bool bHasErrors)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.OnResponse_A0F42E9749E8C110926964B48F717ABB");

	UWBP_HomeScreen_C_OnResponse_A0F42E9749E8C110926964B48F717ABB_Params params;
	params.ImageURL = ImageURL;
	params.LinkURL = LinkURL;
	params.bHasErrors = bHasErrors;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.OnShow
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_HomeScreen_C::OnShow()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.OnShow");

	UWBP_HomeScreen_C_OnShow_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.On Profile Updated
// (BlueprintCallable, BlueprintEvent)

void UWBP_HomeScreen_C::On_Profile_Updated()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.On Profile Updated");

	UWBP_HomeScreen_C_On_Profile_Updated_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_HomeScreen_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature");

	UWBP_HomeScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.LoadToasterMessage
// (BlueprintCallable, BlueprintEvent)

void UWBP_HomeScreen_C::LoadToasterMessage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.LoadToasterMessage");

	UWBP_HomeScreen_C_LoadToasterMessage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.LoadMenuNews
// (BlueprintCallable, BlueprintEvent)

void UWBP_HomeScreen_C::LoadMenuNews()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.LoadMenuNews");

	UWBP_HomeScreen_C_LoadMenuNews_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.Connected
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                UserId                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString*                AuthToken                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_HomeScreen_C::Connected(struct FString* UserId, struct FString* AuthToken)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.Connected");

	UWBP_HomeScreen_C_Connected_Params params;
	params.UserId = UserId;
	params.AuthToken = AuthToken;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_HomeScreen.WBP_HomeScreen_C.ExecuteUbergraph_WBP_HomeScreen
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_HomeScreen_C::ExecuteUbergraph_WBP_HomeScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_HomeScreen.WBP_HomeScreen_C.ExecuteUbergraph_WBP_HomeScreen");

	UWBP_HomeScreen_C_ExecuteUbergraph_WBP_HomeScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
