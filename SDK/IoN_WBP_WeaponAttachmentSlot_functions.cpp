// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Get_AttachmentFrame_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_WeaponAttachmentSlot_C::Get_AttachmentFrame_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Get_AttachmentFrame_ColorAndOpacity_1");

	UWBP_WeaponAttachmentSlot_C_Get_AttachmentFrame_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WeaponAttachmentSlot_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnDrop");

	UWBP_WeaponAttachmentSlot_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_WeaponAttachmentSlot_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.GetCurrentItem");

	UWBP_WeaponAttachmentSlot_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WeaponAttachmentSlot_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.AcceptsDropForItem");

	UWBP_WeaponAttachmentSlot_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.InitAttachmentSlot
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   AttachmentSlot                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class AIONFirearm*             Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponAttachmentSlot_C::InitAttachmentSlot(TEnumAsByte<EAttachmentSlot> AttachmentSlot, class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.InitAttachmentSlot");

	UWBP_WeaponAttachmentSlot_C_InitAttachmentSlot_Params params;
	params.AttachmentSlot = AttachmentSlot;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_WeaponAttachmentSlot_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnItemChanged");

	UWBP_WeaponAttachmentSlot_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponAttachmentSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Tick");

	UWBP_WeaponAttachmentSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_WeaponAttachmentSlot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Construct");

	UWBP_WeaponAttachmentSlot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.ExecuteUbergraph_WBP_WeaponAttachmentSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponAttachmentSlot_C::ExecuteUbergraph_WBP_WeaponAttachmentSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.ExecuteUbergraph_WBP_WeaponAttachmentSlot");

	UWBP_WeaponAttachmentSlot_C_ExecuteUbergraph_WBP_WeaponAttachmentSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
