// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_EndGameFocus.BP_EndGameFocus_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_EndGameFocus_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_EndGameFocus.BP_EndGameFocus_C.UserConstructionScript");

	ABP_EndGameFocus_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_EndGameFocus.BP_EndGameFocus_C.ReceiveTick
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaSeconds                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_EndGameFocus_C::ReceiveTick(float* DeltaSeconds)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_EndGameFocus.BP_EndGameFocus_C.ReceiveTick");

	ABP_EndGameFocus_C_ReceiveTick_Params params;
	params.DeltaSeconds = DeltaSeconds;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_EndGameFocus.BP_EndGameFocus_C.ExecuteUbergraph_BP_EndGameFocus
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_EndGameFocus_C::ExecuteUbergraph_BP_EndGameFocus(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_EndGameFocus.BP_EndGameFocus_C.ExecuteUbergraph_BP_EndGameFocus");

	ABP_EndGameFocus_C_ExecuteUbergraph_BP_EndGameFocus_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
