// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_Visibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_SettingsSectionButton_C::Get_TextBlock_Label_Visibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_Visibility");

	UWBP_SettingsSectionButton_C_Get_TextBlock_Label_Visibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_Button_Visibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_SettingsSectionButton_C::Get_Button_Visibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_Button_Visibility");

	UWBP_SettingsSectionButton_C_Get_Button_Visibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateColor             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateColor UWBP_SettingsSectionButton_C::Get_TextBlock_Label_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.Get_TextBlock_Label_ColorAndOpacity_1");

	UWBP_SettingsSectionButton_C_Get_TextBlock_Label_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.GetbIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_SettingsSectionButton_C::GetbIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.GetbIsEnabled_1");

	UWBP_SettingsSectionButton_C_GetbIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsSectionButton_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature");

	UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_3_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsSectionButton_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature");

	UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_17_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsSectionButton_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature");

	UWBP_SettingsSectionButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_186_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsSectionButton_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.PreConstruct");

	UWBP_SettingsSectionButton_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.ExecuteUbergraph_WBP_SettingsSectionButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsSectionButton_C::ExecuteUbergraph_WBP_SettingsSectionButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.ExecuteUbergraph_WBP_SettingsSectionButton");

	UWBP_SettingsSectionButton_C_ExecuteUbergraph_WBP_SettingsSectionButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.OnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_SettingsSectionButton_C::OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsSectionButton.WBP_SettingsSectionButton_C.OnClicked__DelegateSignature");

	UWBP_SettingsSectionButton_C_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
