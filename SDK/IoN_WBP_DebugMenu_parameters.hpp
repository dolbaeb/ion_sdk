#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_DebugMenu.WBP_DebugMenu_C.Construct
struct UWBP_DebugMenu_C_Construct_Params
{
};

// Function WBP_DebugMenu.WBP_DebugMenu_C.Button Clicked
struct UWBP_DebugMenu_C_Button_Clicked_Params
{
	class UWBP_DebugMenuButton_C*                      Object_To_Ignore;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_DebugMenu.WBP_DebugMenu_C.Toggle Visibility
struct UWBP_DebugMenu_C_Toggle_Visibility_Params
{
};

// Function WBP_DebugMenu.WBP_DebugMenu_C.ExecuteUbergraph_WBP_DebugMenu
struct UWBP_DebugMenu_C_ExecuteUbergraph_WBP_DebugMenu_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
