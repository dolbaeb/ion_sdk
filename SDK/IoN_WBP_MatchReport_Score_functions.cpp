// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Score_Total_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_Score_Total_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Score_Total_Text_1");

	UWBP_MatchReport_Score_C_Get_Score_Total_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Kill_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_Sub_Kill_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Kill_Text_1");

	UWBP_MatchReport_Score_C_Get_Sub_Kill_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Placement_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_Sub_Placement_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Placement_Text_1");

	UWBP_MatchReport_Score_C_Get_Sub_Placement_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Damage_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_Sub_Damage_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Damage_Text_1");

	UWBP_MatchReport_Score_C_Get_Sub_Damage_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_NextLevel_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_TextBlock_NextLevel_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_NextLevel_Text_1");

	UWBP_MatchReport_Score_C_Get_TextBlock_NextLevel_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_CurrentLevel_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_MatchReport_Score_C::Get_TextBlock_CurrentLevel_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_CurrentLevel_Text_1");

	UWBP_MatchReport_Score_C_Get_TextBlock_CurrentLevel_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ScoreBarAppear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::ScoreBarAppear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ScoreBarAppear");

	UWBP_MatchReport_Score_C_ScoreBarAppear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnMinimize
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::OnMinimize()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnMinimize");

	UWBP_MatchReport_Score_C_OnMinimize_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MatchReport_Score_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnAnimationFinished");

	UWBP_MatchReport_Score_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Appear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::Appear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Appear");

	UWBP_MatchReport_Score_C_Appear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Disappear
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::Disappear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Disappear");

	UWBP_MatchReport_Score_C_Disappear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnSetMatchHistory
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FPlayerMatchHistory     MatchHistory                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_MatchReport_Score_C::OnSetMatchHistory(const struct FPlayerMatchHistory& MatchHistory)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnSetMatchHistory");

	UWBP_MatchReport_Score_C_OnSetMatchHistory_Params params;
	params.MatchHistory = MatchHistory;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_Score_C::BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature");

	UWBP_MatchReport_Score_C_BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_Score_C::BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature");

	UWBP_MatchReport_Score_C_BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_Score_C::BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature");

	UWBP_MatchReport_Score_C_BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_Score_C::BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature");

	UWBP_MatchReport_Score_C_BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Begin Animation
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::Begin_Animation()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Begin Animation");

	UWBP_MatchReport_Score_C_Begin_Animation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ShowRewards
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::ShowRewards()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ShowRewards");

	UWBP_MatchReport_Score_C_ShowRewards_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnReportClicked
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::OnReportClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnReportClicked");

	UWBP_MatchReport_Score_C_OnReportClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Fast Forward
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::Fast_Forward()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Fast Forward");

	UWBP_MatchReport_Score_C_Fast_Forward_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountPlacementScore
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::CountPlacementScore()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountPlacementScore");

	UWBP_MatchReport_Score_C_CountPlacementScore_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountDamageScore
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::CountDamageScore()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountDamageScore");

	UWBP_MatchReport_Score_C_CountDamageScore_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountKillScore
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::CountKillScore()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountKillScore");

	UWBP_MatchReport_Score_C_CountKillScore_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MatchReport_Score_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Construct");

	UWBP_MatchReport_Score_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ExecuteUbergraph_WBP_MatchReport_Score
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_Score_C::ExecuteUbergraph_WBP_MatchReport_Score(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ExecuteUbergraph_WBP_MatchReport_Score");

	UWBP_MatchReport_Score_C_ExecuteUbergraph_WBP_MatchReport_Score_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.AnimationFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_Score_C::AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.AnimationFinished__DelegateSignature");

	UWBP_MatchReport_Score_C_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
