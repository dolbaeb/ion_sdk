#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Update Audio Channel
struct UBPI_GameSettingsInterface_C_Update_Audio_Channel_Params
{
	TEnumAsByte<EAudioType>                            Audio_Channel;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Volume;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               _;                                                        // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Run Console Command
struct UBPI_GameSettingsInterface_C_Run_Console_Command_Params
{
	struct FString                                     Console_Command;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               _;                                                        // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Get Settings Instance
struct UBPI_GameSettingsInterface_C_Get_Settings_Instance_Params
{
	class UBP_GameSettingsWrapper_C*                   SettingsWrapper;                                          // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
