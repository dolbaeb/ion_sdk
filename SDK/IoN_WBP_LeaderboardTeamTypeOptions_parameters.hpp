#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Select Option
struct UWBP_LeaderboardTeamTypeOptions_C_Select_Option_Params
{
	class UTextBlock*                                  Text;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      Image;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Construct
struct UWBP_LeaderboardTeamTypeOptions_C_Construct_Params
{
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions
struct UWBP_LeaderboardTeamTypeOptions_C_ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.OnTeamTypeChanged__DelegateSignature
struct UWBP_LeaderboardTeamTypeOptions_C_OnTeamTypeChanged__DelegateSignature_Params
{
	TEnumAsByte<ELeaderboardTeamTypes>                 Team_Type;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
