#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass Mlv_MainMenu.Mlv_MainMenu_C
// 0x0038 (0x0408 - 0x03D0)
class AMlv_MainMenu_C : public AIONMenuLevelScriptActor
{
public:
	class UWBP_MainMenu_C*                             MainMenuWidget;                                           // 0x03D0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	TArray<class ABP_PartySpawn_C*>                    PartySpawns_1;                                            // 0x03D8(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance)
	class ABP_PartySpawn_C*                            BP_PartySpawn4_723_EdGraph_3_RefProperty;                 // 0x03E8(0x0008) (ZeroConstructor, IsPlainOldData)
	class ABP_PartySpawn_C*                            BP_PartySpawn3_576_EdGraph_3_RefProperty;                 // 0x03F0(0x0008) (ZeroConstructor, IsPlainOldData)
	class ABP_PartySpawn_C*                            BP_PartySpawn2_431_EdGraph_3_RefProperty;                 // 0x03F8(0x0008) (ZeroConstructor, IsPlainOldData)
	class ABP_PartySpawn_C*                            BP_PartySpawn_242_EdGraph_3_RefProperty;                  // 0x0400(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass Mlv_MainMenu.Mlv_MainMenu_C");
		return ptr;
	}


	void GetFilledPartySlot(class ABP_PartySpawn_C** FilledSlot);
	void ReOrderPartyMembers();
	void OnPartyMemberLeave(const struct FMainMenuPartyMember& PartyMember);
	void OnPartyMemberJoined(const struct FMatchmakingPlayer& PartyMember);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
