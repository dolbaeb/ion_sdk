#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Visibility_1
struct UWBP_ProximityItemSlot_C_Get_Image_Status_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Brush_1
struct UWBP_ProximityItemSlot_C_Get_Image_Status_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.RemoveItemFromStack
struct UWBP_ProximityItemSlot_C_RemoveItemFromStack_Params
{
	class AIONItem*                                    ItemToRemove;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	bool                                               bWasRemoved;                                              // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AddItemToStack
struct UWBP_ProximityItemSlot_C_AddItemToStack_Params
{
	class AIONItem*                                    NewItem;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AcceptsDropForItem
struct UWBP_ProximityItemSlot_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnDrop
struct UWBP_ProximityItemSlot_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.GetCurrentItem
struct UWBP_ProximityItemSlot_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmountText_Text_1
struct UWBP_ProximityItemSlot_C_Get_AmountText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmmoText_Text_1
struct UWBP_ProximityItemSlot_C_Get_AmmoText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemChanged
struct UWBP_ProximityItemSlot_C_OnItemChanged_Params
{
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Tick
struct UWBP_ProximityItemSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Construct
struct UWBP_ProximityItemSlot_C_Construct_Params
{
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemClicked
struct UWBP_ProximityItemSlot_C_OnItemClicked_Params
{
};

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.ExecuteUbergraph_WBP_ProximityItemSlot
struct UWBP_ProximityItemSlot_C_ExecuteUbergraph_WBP_ProximityItemSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
