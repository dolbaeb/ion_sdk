#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SKeyAction.SKeyAction
// 0x0020
struct FSKeyAction
{
	struct FString                                     Category_51_02F7E3B1472CCCD9B03C9AAA3760BFCF;             // 0x0000(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	TArray<struct FSKeyMapping>                        KeyMappings_53_06DB6A864A4AB13D9C9761A1848B4A2A;          // 0x0010(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
