#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_EndGameFocus.BP_EndGameFocus_C.UserConstructionScript
struct ABP_EndGameFocus_C_UserConstructionScript_Params
{
};

// Function BP_EndGameFocus.BP_EndGameFocus_C.ReceiveTick
struct ABP_EndGameFocus_C_ReceiveTick_Params
{
	float*                                             DeltaSeconds;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_EndGameFocus.BP_EndGameFocus_C.ExecuteUbergraph_BP_EndGameFocus
struct ABP_EndGameFocus_C_ExecuteUbergraph_BP_EndGameFocus_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
