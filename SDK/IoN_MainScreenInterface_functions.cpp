// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function MainScreenInterface.MainScreenInterface_C.OnShow
// (Public, BlueprintCallable, BlueprintEvent)

void UMainScreenInterface_C::OnShow()
{
	static auto fn = UObject::FindObject<UFunction>("Function MainScreenInterface.MainScreenInterface_C.OnShow");

	UMainScreenInterface_C_OnShow_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
