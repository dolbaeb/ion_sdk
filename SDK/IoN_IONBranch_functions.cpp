// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function IONBranch.AnnouncerAudioComponent.PlayAnnouncerSound
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UAkAudioEvent*           EventToPlay                    (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHighPriority                  (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UAnnouncerAudioComponent::PlayAnnouncerSound(class UAkAudioEvent* EventToPlay, bool bHighPriority)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.PlayAnnouncerSound");

	UAnnouncerAudioComponent_PlayAnnouncerSound_Params params;
	params.EventToPlay = EventToPlay;
	params.bHighPriority = bHighPriority;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.AnnouncerAudioComponent.OnSafeZoneMarked
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnSafeZoneMarked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnSafeZoneMarked");

	UAnnouncerAudioComponent_OnSafeZoneMarked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStopped
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnPlasmaConvergenceStopped()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStopped");

	UAnnouncerAudioComponent_OnPlasmaConvergenceStopped_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStarted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnPlasmaConvergenceStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStarted");

	UAnnouncerAudioComponent_OnPlasmaConvergenceStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnNextRoundCountdownStarted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnNextRoundCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnNextRoundCountdownStarted");

	UAnnouncerAudioComponent_OnNextRoundCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnMatchEnded
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnMatchEnded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnMatchEnded");

	UAnnouncerAudioComponent_OnMatchEnded_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerWon
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnLocalPlayerWon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerWon");

	UAnnouncerAudioComponent_OnLocalPlayerWon_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerDied
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnLocalPlayerDied()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerDied");

	UAnnouncerAudioComponent_OnLocalPlayerDied_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnLastStageCompleted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnLastStageCompleted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnLastStageCompleted");

	UAnnouncerAudioComponent_OnLastStageCompleted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnFinalConvergenceStarted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnFinalConvergenceStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnFinalConvergenceStarted");

	UAnnouncerAudioComponent_OnFinalConvergenceStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleStarted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleStarted");

	UAnnouncerAudioComponent_OnBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleCountdownStarted
// (Event, Public, BlueprintEvent)

void UAnnouncerAudioComponent::OnBattleRoyaleCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleCountdownStarted");

	UAnnouncerAudioComponent_OnBattleRoyaleCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.PlayArenaSound
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UAkAudioEvent*           EventToPlay                    (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHighPriority                  (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UArenaAudioComponent::PlayArenaSound(class UAkAudioEvent* EventToPlay, bool bHighPriority)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.PlayArenaSound");

	UArenaAudioComponent_PlayArenaSound_Params params;
	params.EventToPlay = EventToPlay;
	params.bHighPriority = bHighPriority;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.ArenaAudioComponent.OnSafeZoneMarked
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnSafeZoneMarked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnSafeZoneMarked");

	UArenaAudioComponent_OnSafeZoneMarked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStopped
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnPlasmaConvergenceStopped()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStopped");

	UArenaAudioComponent_OnPlasmaConvergenceStopped_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStarted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnPlasmaConvergenceStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStarted");

	UArenaAudioComponent_OnPlasmaConvergenceStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnNextRoundCountdownStarted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnNextRoundCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnNextRoundCountdownStarted");

	UArenaAudioComponent_OnNextRoundCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnMatchEnded
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnMatchEnded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnMatchEnded");

	UArenaAudioComponent_OnMatchEnded_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnLocalPlayerWon
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnLocalPlayerWon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnLocalPlayerWon");

	UArenaAudioComponent_OnLocalPlayerWon_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnLocalPlayerDied
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnLocalPlayerDied()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnLocalPlayerDied");

	UArenaAudioComponent_OnLocalPlayerDied_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnLastStageCompleted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnLastStageCompleted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnLastStageCompleted");

	UArenaAudioComponent_OnLastStageCompleted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnFinalConvergenceStarted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnFinalConvergenceStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnFinalConvergenceStarted");

	UArenaAudioComponent_OnFinalConvergenceStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnBattleRoyaleStarted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnBattleRoyaleStarted");

	UArenaAudioComponent_OnBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ArenaAudioComponent.OnBattleRoyaleCountdownStarted
// (Event, Public, BlueprintEvent)

void UArenaAudioComponent::OnBattleRoyaleCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ArenaAudioComponent.OnBattleRoyaleCountdownStarted");

	UArenaAudioComponent_OnBattleRoyaleCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameState.GetNumPlayersAlive
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONGameState::GetNumPlayersAlive()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameState.GetNumPlayersAlive");

	AIONGameState_GetNumPlayersAlive_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameState.GetAnnouncerAudioComponent
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UAnnouncerAudioComponent* ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UAnnouncerAudioComponent* AIONGameState::GetAnnouncerAudioComponent()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameState.GetAnnouncerAudioComponent");

	AIONGameState_GetAnnouncerAudioComponent_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameState.BP_OnPlayerDied
// (Event, Public, BlueprintEvent)
// Parameters:
// class APlayerState*            Victim                         (Parm, ZeroConstructor, IsPlainOldData)
// class APlayerState*            DamageInstigator               (Parm, ZeroConstructor, IsPlainOldData)
// class AActor*                  DamageCauser                   (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHeadshot                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bKnockdown                     (Parm, ZeroConstructor, IsPlainOldData)
// int                            Range                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameState::BP_OnPlayerDied(class APlayerState* Victim, class APlayerState* DamageInstigator, class AActor* DamageCauser, bool bHeadshot, bool bKnockdown, int Range)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameState.BP_OnPlayerDied");

	AIONGameState_BP_OnPlayerDied_Params params;
	params.Victim = Victim;
	params.DamageInstigator = DamageInstigator;
	params.DamageCauser = DamageCauser;
	params.bHeadshot = bHeadshot;
	params.bKnockdown = bKnockdown;
	params.Range = Range;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.OnRep_SpawnRandomSeed
// (Final, Native, Public)

void ABattleRoyaleGameState::OnRep_SpawnRandomSeed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.OnRep_SpawnRandomSeed");

	ABattleRoyaleGameState_OnRep_SpawnRandomSeed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.OnRep_PlasmaStages
// (Final, Native, Public)

void ABattleRoyaleGameState::OnRep_PlasmaStages()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.OnRep_PlasmaStages");

	ABattleRoyaleGameState_OnRep_PlasmaStages_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaRadius
// (Final, Native, Public)

void ABattleRoyaleGameState::OnRep_InitialPlasmaRadius()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaRadius");

	ABattleRoyaleGameState_OnRep_InitialPlasmaRadius_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaLocation
// (Final, Native, Private)

void ABattleRoyaleGameState::OnRep_InitialPlasmaLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaLocation");

	ABattleRoyaleGameState_OnRep_InitialPlasmaLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.OnRep_BattleRoyaleStartTime
// (Final, Native, Private)

void ABattleRoyaleGameState::OnRep_BattleRoyaleStartTime()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.OnRep_BattleRoyaleStartTime");

	ABattleRoyaleGameState_OnRep_BattleRoyaleStartTime_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BattleRoyaleGameState.HasBattleRoyaleStarted
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool ABattleRoyaleGameState::HasBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.HasBattleRoyaleStarted");

	ABattleRoyaleGameState_HasBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.BattleRoyaleGameState.GetTimeSinceMatchStart
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float ABattleRoyaleGameState::GetTimeSinceMatchStart()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.GetTimeSinceMatchStart");

	ABattleRoyaleGameState_GetTimeSinceMatchStart_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.BattleRoyaleGameState.GetMatchStartTime
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float ABattleRoyaleGameState::GetMatchStartTime()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.GetMatchStartTime");

	ABattleRoyaleGameState_GetMatchStartTime_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.BattleRoyaleGameState.GetInitialPlasmaLocation
// (Final, Native, Public, HasDefaults, BlueprintCallable)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector ABattleRoyaleGameState::GetInitialPlasmaLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BattleRoyaleGameState.GetInitialPlasmaLocation");

	ABattleRoyaleGameState_GetInitialPlasmaLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.UpdateSteamInventoryItems
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::UpdateSteamInventoryItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.UpdateSteamInventoryItems");

	AIONBasePlayerController_UpdateSteamInventoryItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseSuccessfull__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// int                            NewItem                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::SteamPurchaseSuccessfull__DelegateSignature(int NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseSuccessfull__DelegateSignature");

	AIONBasePlayerController_SteamPurchaseSuccessfull__DelegateSignature_Params params;
	params.NewItem = NewItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseFailed__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::SteamPurchaseFailed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseFailed__DelegateSignature");

	AIONBasePlayerController_SteamPurchaseFailed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.SteamInventoryUpdateDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::SteamInventoryUpdateDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.SteamInventoryUpdateDelegate__DelegateSignature");

	AIONBasePlayerController_SteamInventoryUpdateDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.ShowHighlights
// (Final, Exec, Native, Public)

void AIONBasePlayerController::ShowHighlights()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.ShowHighlights");

	AIONBasePlayerController_ShowHighlights_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SetAudioOutputDevicesVolume
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            Volume                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::SetAudioOutputDevicesVolume(int Volume)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SetAudioOutputDevicesVolume");

	AIONBasePlayerController_SetAudioOutputDevicesVolume_Params params;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SetAudioOutputDevice
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 Name                           (Parm, ZeroConstructor)

void AIONBasePlayerController::SetAudioOutputDevice(const struct FString& Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SetAudioOutputDevice");

	AIONBasePlayerController_SetAudioOutputDevice_Params params;
	params.Name = Name;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SetAudioInputDevicesVolume
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            Volume                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::SetAudioInputDevicesVolume(int Volume)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SetAudioInputDevicesVolume");

	AIONBasePlayerController_SetAudioInputDevicesVolume_Params params;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SetAudioInputDevice
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 Name                           (Parm, ZeroConstructor)

void AIONBasePlayerController::SetAudioInputDevice(const struct FString& Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SetAudioInputDevice");

	AIONBasePlayerController_SetAudioInputDevice_Params params;
	params.Name = Name;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SerializeLoadout
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::SerializeLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SerializeLoadout");

	AIONBasePlayerController_SerializeLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.SaveLoadout
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::SaveLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.SaveLoadout");

	AIONBasePlayerController_SaveLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.RemoveSkinFromLoadout
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  ItemToRemove                   (Parm)

void AIONBasePlayerController::RemoveSkinFromLoadout(const struct FIONSteamInventoryItem& ItemToRemove)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.RemoveSkinFromLoadout");

	AIONBasePlayerController_RemoveSkinFromLoadout_Params params;
	params.ItemToRemove = ItemToRemove;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.RecordTestHighlight
// (Final, Exec, Native, Public)

void AIONBasePlayerController::RecordTestHighlight()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.RecordTestHighlight");

	AIONBasePlayerController_RecordTestHighlight_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.ProcessVirtualGoodFromSteam
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 OrderId                        (Parm, ZeroConstructor)

void AIONBasePlayerController::ProcessVirtualGoodFromSteam(const struct FString& OrderId)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.ProcessVirtualGoodFromSteam");

	AIONBasePlayerController_ProcessVirtualGoodFromSteam_Params params;
	params.OrderId = OrderId;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OpenHighlightsGroup
// (Final, Exec, Native, Public)

void AIONBasePlayerController::OpenHighlightsGroup()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OpenHighlightsGroup");

	AIONBasePlayerController_OpenHighlightsGroup_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnTeamChatMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSTeamChatMessage      TeamChatMessage                (Parm)

void AIONBasePlayerController::OnTeamChatMessageReceived(const struct FGSTeamChatMessage& TeamChatMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnTeamChatMessageReceived");

	AIONBasePlayerController_OnTeamChatMessageReceived_Params params;
	params.TeamChatMessage = TeamChatMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnScriptMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSScriptMessage        ScriptMessage                  (Parm)

void AIONBasePlayerController::OnScriptMessageReceived(const struct FGSScriptMessage& ScriptMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnScriptMessageReceived");

	AIONBasePlayerController_OnScriptMessageReceived_Params params;
	params.ScriptMessage = ScriptMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnRep_PlayerState
// (Native, Public)

void AIONBasePlayerController::OnRep_PlayerState()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnRep_PlayerState");

	AIONBasePlayerController_OnRep_PlayerState_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnNewTeamScoreMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSNewTeamScoreMessage  NewTeamScoreMessage            (Parm)

void AIONBasePlayerController::OnNewTeamScoreMessageReceived(const struct FGSNewTeamScoreMessage& NewTeamScoreMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnNewTeamScoreMessageReceived");

	AIONBasePlayerController_OnNewTeamScoreMessageReceived_Params params;
	params.NewTeamScoreMessage = NewTeamScoreMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnNewHighScoreMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSNewHighScoreMessage  NewHighScoreMessage            (Parm)

void AIONBasePlayerController::OnNewHighScoreMessageReceived(const struct FGSNewHighScoreMessage& NewHighScoreMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnNewHighScoreMessageReceived");

	AIONBasePlayerController_OnNewHighScoreMessageReceived_Params params;
	params.NewHighScoreMessage = NewHighScoreMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnGlobalRankChangedMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSGlobalRankChangedMessage GlobalRankChangedMessage       (Parm)

void AIONBasePlayerController::OnGlobalRankChangedMessageReceived(const struct FGSGlobalRankChangedMessage& GlobalRankChangedMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnGlobalRankChangedMessageReceived");

	AIONBasePlayerController_OnGlobalRankChangedMessageReceived_Params params;
	params.GlobalRankChangedMessage = GlobalRankChangedMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.OnAchievementEarnedMessageReceived
// (Final, Native, Public)
// Parameters:
// struct FGSAchievementEarnedMessage AchievementEarnedMessage       (Parm)

void AIONBasePlayerController::OnAchievementEarnedMessageReceived(const struct FGSAchievementEarnedMessage& AchievementEarnedMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.OnAchievementEarnedMessageReceived");

	AIONBasePlayerController_OnAchievementEarnedMessageReceived_Params params;
	params.AchievementEarnedMessage = AchievementEarnedMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.LoadGlobalLeaderboardData
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 LeaderboardShortCode           (Parm, ZeroConstructor)
// struct FString                 GameMode                       (Parm, ZeroConstructor)
// int                            NumEntries                     (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bIsSocial                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FScriptDelegate         OnSuccess                      (Parm, ZeroConstructor)
// struct FScriptDelegate         OnFailed                       (Parm, ZeroConstructor)

void AIONBasePlayerController::LoadGlobalLeaderboardData(const struct FString& LeaderboardShortCode, const struct FString& GameMode, int NumEntries, bool bIsSocial, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.LoadGlobalLeaderboardData");

	AIONBasePlayerController_LoadGlobalLeaderboardData_Params params;
	params.LeaderboardShortCode = LeaderboardShortCode;
	params.GameMode = GameMode;
	params.NumEntries = NumEntries;
	params.bIsSocial = bIsSocial;
	params.OnSuccess = OnSuccess;
	params.OnFailed = OnFailed;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.LoadAroundMeLeaderboardData
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 LeaderboardShortCode           (Parm, ZeroConstructor)
// struct FString                 GameMode                       (Parm, ZeroConstructor)
// int                            NumEntries                     (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bIsSocial                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FScriptDelegate         OnSuccess                      (Parm, ZeroConstructor)
// struct FScriptDelegate         OnFailed                       (Parm, ZeroConstructor)

void AIONBasePlayerController::LoadAroundMeLeaderboardData(const struct FString& LeaderboardShortCode, const struct FString& GameMode, int NumEntries, bool bIsSocial, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.LoadAroundMeLeaderboardData");

	AIONBasePlayerController_LoadAroundMeLeaderboardData_Params params;
	params.LeaderboardShortCode = LeaderboardShortCode;
	params.GameMode = GameMode;
	params.NumEntries = NumEntries;
	params.bIsSocial = bIsSocial;
	params.OnSuccess = OnSuccess;
	params.OnFailed = OnFailed;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.IsSameSquadAs
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class APlayerState*            OtherPlayer                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONBasePlayerController::IsSameSquadAs(class APlayerState* OtherPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.IsSameSquadAs");

	AIONBasePlayerController_IsSameSquadAs_Params params;
	params.OtherPlayer = OtherPlayer;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.IsItemInLoadout
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (Parm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONBasePlayerController::IsItemInLoadout(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.IsItemInLoadout");

	AIONBasePlayerController_IsItemInLoadout_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GS_UpdateCredits
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::GS_UpdateCredits()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_UpdateCredits");

	AIONBasePlayerController_GS_UpdateCredits_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_ScrapItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  ScrapItem                      (Parm)

void AIONBasePlayerController::GS_ScrapItem(const struct FIONSteamInventoryItem& ScrapItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_ScrapItem");

	AIONBasePlayerController_GS_ScrapItem_Params params;
	params.ScrapItem = ScrapItem;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_PurchaseCrate
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONSteamCrate*          CrateToPurchase                (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::GS_PurchaseCrate(class UIONSteamCrate* CrateToPurchase)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_PurchaseCrate");

	AIONBasePlayerController_GS_PurchaseCrate_Params params;
	params.CrateToPurchase = CrateToPurchase;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_OpenCrate
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  CrateToOpen                    (Parm)

void AIONBasePlayerController::GS_OpenCrate(const struct FIONSteamInventoryItem& CrateToOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_OpenCrate");

	AIONBasePlayerController_GS_OpenCrate_Params params;
	params.CrateToOpen = CrateToOpen;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_LoadStoreItems
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::GS_LoadStoreItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_LoadStoreItems");

	AIONBasePlayerController_GS_LoadStoreItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_LoadLoadout_Server
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::GS_LoadLoadout_Server()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_LoadLoadout_Server");

	AIONBasePlayerController_GS_LoadLoadout_Server_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GS_LoadLoadout
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::GS_LoadLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GS_LoadLoadout");

	AIONBasePlayerController_GS_LoadLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GetSteamSessionTicket
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONBasePlayerController::GetSteamSessionTicket()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetSteamSessionTicket");

	AIONBasePlayerController_GetSteamSessionTicket_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetSquadMates
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TArray<class APlayerState*>    ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class APlayerState*> AIONBasePlayerController::GetSquadMates()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetSquadMates");

	AIONBasePlayerController_GetSquadMates_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetSquadID
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONBasePlayerController::GetSquadID()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetSquadID");

	AIONBasePlayerController_GetSquadID_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetLogEventData
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 EventKey                       (Parm, ZeroConstructor)

void AIONBasePlayerController::GetLogEventData(const struct FString& EventKey)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetLogEventData");

	AIONBasePlayerController_GetLogEventData_Params params;
	params.EventKey = EventKey;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GetListOfDifferentLeaderboards
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::GetListOfDifferentLeaderboards()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetListOfDifferentLeaderboards");

	AIONBasePlayerController_GetListOfDifferentLeaderboards_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GetKeysForAction
// (Final, Native, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FName                   ActionName                     (Parm, ZeroConstructor, IsPlainOldData)
// TArray<struct FInputActionKeyMapping> Bindings                       (Parm, OutParm, ZeroConstructor)

void AIONBasePlayerController::GetKeysForAction(const struct FName& ActionName, TArray<struct FInputActionKeyMapping>* Bindings)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetKeysForAction");

	AIONBasePlayerController_GetKeysForAction_Params params;
	params.ActionName = ActionName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Bindings != nullptr)
		*Bindings = params.Bindings;
}


// Function IONBranch.IONBasePlayerController.GetItemBySteamDefIDFast
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            ID                             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class UIONSteamItem*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONSteamItem* AIONBasePlayerController::GetItemBySteamDefIDFast(int ID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetItemBySteamDefIDFast");

	AIONBasePlayerController_GetItemBySteamDefIDFast_Params params;
	params.ID = ID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetItemBySteamDefID
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            ID                             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class UIONSteamItem*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONSteamItem* AIONBasePlayerController::GetItemBySteamDefID(int ID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetItemBySteamDefID");

	AIONBasePlayerController_GetItemBySteamDefID_Params params;
	params.ID = ID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetGameSparksIDFromSteamID
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// struct FScriptDelegate         OnSuccess                      (Parm, ZeroConstructor)
// struct FScriptDelegate         OnFailed                       (Parm, ZeroConstructor)

void AIONBasePlayerController::GetGameSparksIDFromSteamID(const struct FString& SteamID, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetGameSparksIDFromSteamID");

	AIONBasePlayerController_GetGameSparksIDFromSteamID_Params params;
	params.SteamID = SteamID;
	params.OnSuccess = OnSuccess;
	params.OnFailed = OnFailed;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.GetCurrentAudioOutputDevice
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONBasePlayerController::GetCurrentAudioOutputDevice()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetCurrentAudioOutputDevice");

	AIONBasePlayerController_GetCurrentAudioOutputDevice_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetCurrentAudioInputDevice
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONBasePlayerController::GetCurrentAudioInputDevice()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetCurrentAudioInputDevice");

	AIONBasePlayerController_GetCurrentAudioInputDevice_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetAudioOutputDevices
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<struct FString>         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FString> AIONBasePlayerController::GetAudioOutputDevices()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetAudioOutputDevices");

	AIONBasePlayerController_GetAudioOutputDevices_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetAudioInputDevices
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<struct FString>         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FString> AIONBasePlayerController::GetAudioInputDevices()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetAudioInputDevices");

	AIONBasePlayerController_GetAudioInputDevices_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONBasePlayerController.GetAppliedSkinByWeapon
// (Native, Public, BlueprintCallable)
// Parameters:
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)
// class UIONWeaponSkin*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONWeaponSkin* AIONBasePlayerController::GetAppliedSkinByWeapon(class AIONWeapon* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.GetAppliedSkinByWeapon");

	AIONBasePlayerController_GetAppliedSkinByWeapon_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapItemDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// int                            Result                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::GameSparksScrapItemDelegate__DelegateSignature(int Result)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapItemDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksScrapItemDelegate__DelegateSignature_Params params;
	params.Result = Result;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapFailedDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksScrapFailedDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapFailedDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksScrapFailedDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateFailedDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksPurchaseCrateFailedDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateFailedDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksPurchaseCrateFailedDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// int                            ItemDefID                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::GameSparksPurchaseCrateDelegate__DelegateSignature(int ItemDefID)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksPurchaseCrateDelegate__DelegateSignature_Params params;
	params.ItemDefID = ItemDefID;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksOpenCrateDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// int                            Result                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::GameSparksOpenCrateDelegate__DelegateSignature(int Result)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksOpenCrateDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksOpenCrateDelegate__DelegateSignature_Params params;
	params.Result = Result;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutFailedDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksLoadoutFailedDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutFailedDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksLoadoutFailedDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksLoadoutDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksLoadoutDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksFaileDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksFaileDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksFaileDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksFaileDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksCreditsUpdatedDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksCreditsUpdatedDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksCreditsUpdatedDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksCreditsUpdatedDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.GameSparksAuthenticatedDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONBasePlayerController::GameSparksAuthenticatedDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.GameSparksAuthenticatedDelegate__DelegateSignature");

	AIONBasePlayerController_GameSparksAuthenticatedDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.FindAllWeaponSkins
// (Final, Native, Public, BlueprintCallable)

void AIONBasePlayerController::FindAllWeaponSkins()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.FindAllWeaponSkins");

	AIONBasePlayerController_FindAllWeaponSkins_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetLeaderboard__DelegateSignature
// (Public, Delegate, HasOutParms)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void AIONBasePlayerController::DelegateOnSuccessGetLeaderboard__DelegateSignature(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetLeaderboard__DelegateSignature");

	AIONBasePlayerController_DelegateOnSuccessGetLeaderboard__DelegateSignature_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetGameSparksID__DelegateSignature
// (Public, Delegate)
// Parameters:
// class UGameSparksScriptData*   ScriptData                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::DelegateOnSuccessGetGameSparksID__DelegateSignature(class UGameSparksScriptData* ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetGameSparksID__DelegateSignature");

	AIONBasePlayerController_DelegateOnSuccessGetGameSparksID__DelegateSignature_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetLeaderboard__DelegateSignature
// (Public, Delegate, HasOutParms)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void AIONBasePlayerController::DelegateOnFailedGetLeaderboard__DelegateSignature(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetLeaderboard__DelegateSignature");

	AIONBasePlayerController_DelegateOnFailedGetLeaderboard__DelegateSignature_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetGameSparksID__DelegateSignature
// (Public, Delegate)
// Parameters:
// class UGameSparksScriptData*   ScriptData                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONBasePlayerController::DelegateOnFailedGetGameSparksID__DelegateSignature(class UGameSparksScriptData* ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetGameSparksID__DelegateSignature");

	AIONBasePlayerController_DelegateOnFailedGetGameSparksID__DelegateSignature_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.CloseHighlightsGroup
// (Final, Exec, Native, Public)

void AIONBasePlayerController::CloseHighlightsGroup()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.CloseHighlightsGroup");

	AIONBasePlayerController_CloseHighlightsGroup_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.ClientSetKickedMessage
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FText                   KickedReason                   (ConstParm, Parm, ReferenceParm)

void AIONBasePlayerController::ClientSetKickedMessage(const struct FText& KickedReason)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.ClientSetKickedMessage");

	AIONBasePlayerController_ClientSetKickedMessage_Params params;
	params.KickedReason = KickedReason;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBasePlayerController.ApplyNewSkinToLoadout
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  NewItem                        (Parm)

void AIONBasePlayerController::ApplyNewSkinToLoadout(const struct FIONSteamInventoryItem& NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBasePlayerController.ApplyNewSkinToLoadout");

	AIONBasePlayerController_ApplyNewSkinToLoadout_Params params;
	params.NewItem = NewItem;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.VoiceModeChanged
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// int                            NewVoiceMode                   (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::VoiceModeChanged(int NewVoiceMode)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.VoiceModeChanged");

	AMainPlayerController_VoiceModeChanged_Params params;
	params.NewVoiceMode = NewVoiceMode;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.UpdateVoiceUI
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bProximity                     (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bActivated                     (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::UpdateVoiceUI(bool bProximity, bool bActivated)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.UpdateVoiceUI");

	AMainPlayerController_UpdateVoiceUI_Params params;
	params.bProximity = bProximity;
	params.bActivated = bActivated;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ToggleScreenshotMode
// (Final, Exec, Native, Public)

void AMainPlayerController::ToggleScreenshotMode()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleScreenshotMode");

	AMainPlayerController_ToggleScreenshotMode_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.TogglePing
// (Final, Exec, Native, Public)

void AMainPlayerController::TogglePing()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.TogglePing");

	AMainPlayerController_TogglePing_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ToggleMutePlayerByPlayerState
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class APlayerState*            OtherPlayerState               (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::ToggleMutePlayerByPlayerState(class APlayerState* OtherPlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleMutePlayerByPlayerState");

	AMainPlayerController_ToggleMutePlayerByPlayerState_Params params;
	params.OtherPlayerState = OtherPlayerState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.ToggleHUD
// (Final, Exec, Native, Public)

void AMainPlayerController::ToggleHUD()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleHUD");

	AMainPlayerController_ToggleHUD_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ToggleHighlights
// (Final, Exec, Native, Public)

void AMainPlayerController::ToggleHighlights()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleHighlights");

	AMainPlayerController_ToggleHighlights_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ToggleFreeCam
// (Final, Exec, Native, Public)

void AMainPlayerController::ToggleFreeCam()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleFreeCam");

	AMainPlayerController_ToggleFreeCam_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ToggleDebugText
// (Final, Exec, Native, Public)

void AMainPlayerController::ToggleDebugText()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ToggleDebugText");

	AMainPlayerController_ToggleDebugText_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.Suicide
// (Final, Exec, Native, Public)

void AMainPlayerController::Suicide()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.Suicide");

	AMainPlayerController_Suicide_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.StopClientBot
// (Final, Exec, Native, Public)

void AMainPlayerController::StopClientBot()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.StopClientBot");

	AMainPlayerController_StopClientBot_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.StartSpectating
// (Final, Exec, Native, Public, BlueprintCallable)

void AMainPlayerController::StartSpectating()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.StartSpectating");

	AMainPlayerController_StartSpectating_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.StartPlaying
// (Final, Exec, Native, Public, BlueprintCallable)

void AMainPlayerController::StartPlaying()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.StartPlaying");

	AMainPlayerController_StartPlaying_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.StartClientBot
// (Final, Exec, Native, Public)

void AMainPlayerController::StartClientBot()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.StartClientBot");

	AMainPlayerController_StartClientBot_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SpectatePreviousPlayer
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SpectatePreviousPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SpectatePreviousPlayer");

	AMainPlayerController_SpectatePreviousPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SpectateNextPlayer
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SpectateNextPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SpectateNextPlayer");

	AMainPlayerController_SpectateNextPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SpawnIn
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SpawnIn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SpawnIn");

	AMainPlayerController_SpawnIn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByTimeAlive
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByTimeAlive()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByTimeAlive");

	AMainPlayerController_SortResultsByTimeAlive_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByTeamIndex
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByTeamIndex()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByTeamIndex");

	AMainPlayerController_SortResultsByTeamIndex_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByPlace
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByPlace()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByPlace");

	AMainPlayerController_SortResultsByPlace_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByKills
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByKills()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByKills");

	AMainPlayerController_SortResultsByKills_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByHeadshotAccuracy
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByHeadshotAccuracy()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByHeadshotAccuracy");

	AMainPlayerController_SortResultsByHeadshotAccuracy_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByDamageAmount
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByDamageAmount()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByDamageAmount");

	AMainPlayerController_SortResultsByDamageAmount_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SortResultsByAccuracy
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::SortResultsByAccuracy()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SortResultsByAccuracy");

	AMainPlayerController_SortResultsByAccuracy_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerToggleFreeCam
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerToggleFreeCam()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerToggleFreeCam");

	AMainPlayerController_ServerToggleFreeCam_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerToggleCameraMan
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerToggleCameraMan()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerToggleCameraMan");

	AMainPlayerController_ServerToggleCameraMan_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerSuicide
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerSuicide()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerSuicide");

	AMainPlayerController_ServerSuicide_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerStartSpectating
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerStartSpectating()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerStartSpectating");

	AMainPlayerController_ServerStartSpectating_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerStartPlaying
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerStartPlaying()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerStartPlaying");

	AMainPlayerController_ServerStartPlaying_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerSpawnIn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerSpawnIn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerSpawnIn");

	AMainPlayerController_ServerSpawnIn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerSetDoorOpen
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class ADoor*                   Door                           (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bNewOpen                       (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::ServerSetDoorOpen(class ADoor* Door, bool bNewOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerSetDoorOpen");

	AMainPlayerController_ServerSetDoorOpen_Params params;
	params.Door = Door;
	params.bNewOpen = bNewOpen;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerSendTeamMessage
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// struct FString                 S                              (Parm, ZeroConstructor)
// struct FName                   Type                           (ConstParm, Parm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void AMainPlayerController::ServerSendTeamMessage(const struct FString& S, const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerSendTeamMessage");

	AMainPlayerController_ServerSendTeamMessage_Params params;
	params.S = S;
	params.Type = Type;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerSendBEPacket
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// TArray<unsigned char>          Packet                         (ConstParm, Parm, ZeroConstructor, ReferenceParm)

void AMainPlayerController::ServerSendBEPacket(TArray<unsigned char> Packet)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerSendBEPacket");

	AMainPlayerController_ServerSendBEPacket_Params params;
	params.Packet = Packet;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ServerRespawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AMainPlayerController::ServerRespawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ServerRespawn");

	AMainPlayerController_ServerRespawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SendTeamMessage
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 S                              (Parm, ZeroConstructor)
// struct FName                   Type                           (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void AMainPlayerController::SendTeamMessage(const struct FString& S, const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SendTeamMessage");

	AMainPlayerController_SendTeamMessage_Params params;
	params.S = S;
	params.Type = Type;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SelfUnmuteAll
// (Final, Native, Protected, BlueprintCallable)

void AMainPlayerController::SelfUnmuteAll()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SelfUnmuteAll");

	AMainPlayerController_SelfUnmuteAll_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.SelfMuteAll
// (Final, Native, Protected, BlueprintCallable)

void AMainPlayerController::SelfMuteAll()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.SelfMuteAll");

	AMainPlayerController_SelfMuteAll_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.Respawn
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::Respawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.Respawn");

	AMainPlayerController_Respawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ResetInputMode
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::ResetInputMode()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ResetInputMode");

	AMainPlayerController_ResetInputMode_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.RemoveBots
// (Final, Exec, Native, Public)

void AMainPlayerController::RemoveBots()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.RemoveBots");

	AMainPlayerController_RemoveBots_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.OpenMenu
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::OpenMenu()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.OpenMenu");

	AMainPlayerController_OpenMenu_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.OnMatchResultsReceived
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FPlayerMatchHistory     MatchResult                    (Parm)

void AMainPlayerController::OnMatchResultsReceived(const struct FPlayerMatchHistory& MatchResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.OnMatchResultsReceived");

	AMainPlayerController_OnMatchResultsReceived_Params params;
	params.MatchResult = MatchResult;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.OnAudioLevelLoaded
// (Final, Native, Protected)

void AMainPlayerController::OnAudioLevelLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.OnAudioLevelLoaded");

	AMainPlayerController_OnAudioLevelLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.MoveCharacterToTransform
// (Final, Native, Public, HasDefaults, BlueprintCallable)
// Parameters:
// class AIONCharacter*           CCharacter                     (Parm, ZeroConstructor, IsPlainOldData)
// struct FTransform              NewTransform                   (Parm, IsPlainOldData)

void AMainPlayerController::MoveCharacterToTransform(class AIONCharacter* CCharacter, const struct FTransform& NewTransform)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.MoveCharacterToTransform");

	AMainPlayerController_MoveCharacterToTransform_Params params;
	params.CCharacter = CCharacter;
	params.NewTransform = NewTransform;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.LogoutOfVivoxServer
// (Final, BlueprintCosmetic, Native, Protected, BlueprintCallable)

void AMainPlayerController::LogoutOfVivoxServer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.LogoutOfVivoxServer");

	AMainPlayerController_LogoutOfVivoxServer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.LoginToVivoxServer
// (Final, BlueprintCosmetic, Native, Protected, BlueprintCallable)

void AMainPlayerController::LoginToVivoxServer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.LoginToVivoxServer");

	AMainPlayerController_LoginToVivoxServer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ListPrePassPolys
// (Final, Exec, Native, Public)

void AMainPlayerController::ListPrePassPolys()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ListPrePassPolys");

	AMainPlayerController_ListPrePassPolys_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ListPawns
// (Final, Exec, Native, Public)

void AMainPlayerController::ListPawns()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ListPawns");

	AMainPlayerController_ListPawns_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ListAsyncLoads
// (Final, Exec, Native, Public)
// Parameters:
// float                          MinLoadTime                    (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::ListAsyncLoads(float MinLoadTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ListAsyncLoads");

	AMainPlayerController_ListAsyncLoads_Params params;
	params.MinLoadTime = MinLoadTime;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.IsStreamer
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::IsStreamer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.IsStreamer");

	AMainPlayerController_IsStreamer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.IsQA
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::IsQA()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.IsQA");

	AMainPlayerController_IsQA_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.IsDeveloper
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::IsDeveloper()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.IsDeveloper");

	AMainPlayerController_IsDeveloper_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.IsAdmin
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::IsAdmin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.IsAdmin");

	AMainPlayerController_IsAdmin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.HasSpectatableTeammate
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::HasSpectatableTeammate()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.HasSpectatableTeammate");

	AMainPlayerController_HasSpectatableTeammate_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.HasAccessLevel
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EIONAdminAccessLevels          RequestedLevel                 (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::HasAccessLevel(EIONAdminAccessLevels RequestedLevel)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.HasAccessLevel");

	AMainPlayerController_HasAccessLevel_Params params;
	params.RequestedLevel = RequestedLevel;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.HandleReturnToMainMenu
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::HandleReturnToMainMenu()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.HandleReturnToMainMenu");

	AMainPlayerController_HandleReturnToMainMenu_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.GotoBot
// (Final, Exec, Native, Public)
// Parameters:
// int                            BotIndex                       (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::GotoBot(int BotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GotoBot");

	AMainPlayerController_GotoBot_Params params;
	params.BotIndex = BotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.GetTimeUntilRespawn
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AMainPlayerController::GetTimeUntilRespawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetTimeUntilRespawn");

	AMainPlayerController_GetTimeUntilRespawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetSquadChannelName
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AMainPlayerController::GetSquadChannelName()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetSquadChannelName");

	AMainPlayerController_GetSquadChannelName_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetServerTickRate
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AMainPlayerController::GetServerTickRate()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetServerTickRate");

	AMainPlayerController_GetServerTickRate_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetProximityServerName
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AMainPlayerController::GetProximityServerName()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetProximityServerName");

	AMainPlayerController_GetProximityServerName_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetGameOverlay
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UIONGameOverlayWidget*   ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UIONGameOverlayWidget* AMainPlayerController::GetGameOverlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetGameOverlay");

	AMainPlayerController_GetGameOverlay_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetCharacter
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* AMainPlayerController::GetCharacter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetCharacter");

	AMainPlayerController_GetCharacter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.GetAudioEnergy
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AMainPlayerController::GetAudioEnergy()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.GetAudioEnergy");

	AMainPlayerController_GetAudioEnergy_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.EnableStaticMeshCollision
// (Final, Exec, Native, Public)

void AMainPlayerController::EnableStaticMeshCollision()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.EnableStaticMeshCollision");

	AMainPlayerController_EnableStaticMeshCollision_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.DebugMenu
// (Exec, Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bShow                          (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::DebugMenu(bool bShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.DebugMenu");

	AMainPlayerController_DebugMenu_Params params;
	params.bShow = bShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ConnectToVivoxVoiceChannels
// (Final, BlueprintCosmetic, Native, Protected, BlueprintCallable)

void AMainPlayerController::ConnectToVivoxVoiceChannels()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ConnectToVivoxVoiceChannels");

	AMainPlayerController_ConnectToVivoxVoiceChannels_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ConnectToVivoxSquadChannel
// (Final, BlueprintCosmetic, Native, Protected, BlueprintCallable)

void AMainPlayerController::ConnectToVivoxSquadChannel()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ConnectToVivoxSquadChannel");

	AMainPlayerController_ConnectToVivoxSquadChannel_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ConnectToVivoxProximityChannel
// (Final, BlueprintCosmetic, Native, Protected, BlueprintCallable)

void AMainPlayerController::ConnectToVivoxProximityChannel()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ConnectToVivoxProximityChannel");

	AMainPlayerController_ConnectToVivoxProximityChannel_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.CloseMenu
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::CloseMenu()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CloseMenu");

	AMainPlayerController_CloseMenu_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.CloseInventory
// (Final, Native, Public, BlueprintCallable)

void AMainPlayerController::CloseInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CloseInventory");

	AMainPlayerController_CloseInventory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientUnregisterPlayer
// (Net, NetReliable, Native, Event, Public, NetClient, NetValidate)

void AMainPlayerController::ClientUnregisterPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientUnregisterPlayer");

	AMainPlayerController_ClientUnregisterPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientShowMatchResult
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FPlayerMatchHistory     MatchResult                    (Parm)

void AMainPlayerController::ClientShowMatchResult(const struct FPlayerMatchHistory& MatchResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientShowMatchResult");

	AMainPlayerController_ClientShowMatchResult_Params params;
	params.MatchResult = MatchResult;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientShowEliminationScreen
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FPlayerMatchResult      Result                         (Parm)

void AMainPlayerController::ClientShowEliminationScreen(const struct FPlayerMatchResult& Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientShowEliminationScreen");

	AMainPlayerController_ClientShowEliminationScreen_Params params;
	params.Result = Result;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientSendMatchResults
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FMatchResults           Results                        (Parm)

void AMainPlayerController::ClientSendMatchResults(const struct FMatchResults& Results)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientSendMatchResults");

	AMainPlayerController_ClientSendMatchResults_Params params;
	params.Results = Results;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientSendBEPacket
// (Net, NetReliable, Native, Event, Public, NetClient, NetValidate)
// Parameters:
// TArray<unsigned char>          Packet                         (ConstParm, Parm, ZeroConstructor, ReferenceParm)

void AMainPlayerController::ClientSendBEPacket(TArray<unsigned char> Packet)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientSendBEPacket");

	AMainPlayerController_ClientSendBEPacket_Params params;
	params.Packet = Packet;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientRegisterPlayer
// (Net, NetReliable, Native, Event, Public, NetClient, NetValidate)

void AMainPlayerController::ClientRegisterPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientRegisterPlayer");

	AMainPlayerController_ClientRegisterPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientReceiveScoreEvent
// (Net, Native, Event, Public, NetClient)
// Parameters:
// EScoreEvent                    ScoreType                      (Parm, ZeroConstructor, IsPlainOldData)
// int                            Points                         (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::ClientReceiveScoreEvent(EScoreEvent ScoreType, int Points)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientReceiveScoreEvent");

	AMainPlayerController_ClientReceiveScoreEvent_Params params;
	params.ScoreType = ScoreType;
	params.Points = Points;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientPushCmd
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FString                 Cmd                            (Parm, ZeroConstructor)

void AMainPlayerController::ClientPushCmd(const struct FString& Cmd)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientPushCmd");

	AMainPlayerController_ClientPushCmd_Params params;
	params.Cmd = Cmd;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientPostLogin
// (Net, NetReliable, Native, Event, Public, NetClient)

void AMainPlayerController::ClientPostLogin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientPostLogin");

	AMainPlayerController_ClientPostLogin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientOnPlayerKilled
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void AMainPlayerController::ClientOnPlayerKilled(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientOnPlayerKilled");

	AMainPlayerController_ClientOnPlayerKilled_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientOnPlayerDowned
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void AMainPlayerController::ClientOnPlayerDowned(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientOnPlayerDowned");

	AMainPlayerController_ClientOnPlayerDowned_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClientConfirmHit
// (Net, NetReliable, Native, Event, Public, HasDefaults, NetClient)
// Parameters:
// class AActor*                  HitActor                       (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     HitComponent                   (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// struct FVector                 HitRelativeLocation            (ConstParm, Parm, ReferenceParm, IsPlainOldData)
// bool                           bHeadshot                      (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::ClientConfirmHit(class AActor* HitActor, class UPrimitiveComponent* HitComponent, const struct FVector& HitRelativeLocation, bool bHeadshot)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClientConfirmHit");

	AMainPlayerController_ClientConfirmHit_Params params;
	params.HitActor = HitActor;
	params.HitComponent = HitComponent;
	params.HitRelativeLocation = HitRelativeLocation;
	params.bHeadshot = bHeadshot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.ClearAsyncLoads
// (Final, Exec, Native, Public)

void AMainPlayerController::ClearAsyncLoads()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.ClearAsyncLoads");

	AMainPlayerController_ClearAsyncLoads_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainPlayerController.CanUseCameraMan
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::CanUseCameraMan()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CanUseCameraMan");

	AMainPlayerController_CanUseCameraMan_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.CanSpectate
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::CanSpectate()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CanSpectate");

	AMainPlayerController_CanSpectate_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.CanRespawn
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::CanRespawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CanRespawn");

	AMainPlayerController_CanRespawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.CanEverRespawn
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMainPlayerController::CanEverRespawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.CanEverRespawn");

	AMainPlayerController_CanEverRespawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainPlayerController.AddBots
// (Final, Exec, Native, Public)
// Parameters:
// int                            NumBots                        (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bSpawnNearMe                   (Parm, ZeroConstructor, IsPlainOldData)

void AMainPlayerController::AddBots(int NumBots, bool bSpawnNearMe)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainPlayerController.AddBots");

	AMainPlayerController_AddBots_Params params;
	params.NumBots = NumBots;
	params.bSpawnNearMe = bSpawnNearMe;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.ServerOnPlayerMapFullyLoaded
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void ABRPlayerController::ServerOnPlayerMapFullyLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.ServerOnPlayerMapFullyLoaded");

	ABRPlayerController_ServerOnPlayerMapFullyLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.OnPrematchLevelLoaded
// (Final, Native, Protected)

void ABRPlayerController::OnPrematchLevelLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.OnPrematchLevelLoaded");

	ABRPlayerController_OnPrematchLevelLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.OnMapLevelLoaded
// (Final, Native, Protected)

void ABRPlayerController::OnMapLevelLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.OnMapLevelLoaded");

	ABRPlayerController_OnMapLevelLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugSpawnChar
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            Count                          (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugSpawnChar(int Count)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugSpawnChar");

	ABRPlayerController_IONDebugSpawnChar_Params params;
	params.Count = Count;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugSetLocation
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            Idx                            (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugSetLocation(int Idx)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugSetLocation");

	ABRPlayerController_IONDebugSetLocation_Params params;
	params.Idx = Idx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugSetCameraControl
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            Idx                            (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugSetCameraControl(int Idx)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugSetCameraControl");

	ABRPlayerController_IONDebugSetCameraControl_Params params;
	params.Idx = Idx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugPawnTick
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            bEnable                        (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugPawnTick(int bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugPawnTick");

	ABRPlayerController_IONDebugPawnTick_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugItemTick
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            bEnable                        (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugItemTick(int bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugItemTick");

	ABRPlayerController_IONDebugItemTick_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugItemsMat
// (Exec, Event, Public, BlueprintEvent)

void ABRPlayerController::IONDebugItemsMat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugItemsMat");

	ABRPlayerController_IONDebugItemsMat_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugDFShadowsMode
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            Mode                           (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugDFShadowsMode(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugDFShadowsMode");

	ABRPlayerController_IONDebugDFShadowsMode_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugDeleteHUD
// (Exec, Event, Public, BlueprintEvent)

void ABRPlayerController::IONDebugDeleteHUD()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugDeleteHUD");

	ABRPlayerController_IONDebugDeleteHUD_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugDeleteAllItems
// (Exec, Event, Public, BlueprintEvent)

void ABRPlayerController::IONDebugDeleteAllItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugDeleteAllItems");

	ABRPlayerController_IONDebugDeleteAllItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.IONDebugAnimation
// (Exec, Event, Public, BlueprintEvent)
// Parameters:
// int                            bEnable                        (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::IONDebugAnimation(int bEnable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.IONDebugAnimation");

	ABRPlayerController_IONDebugAnimation_Params params;
	params.bEnable = bEnable;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.ForceOnPrematchLevelLoaded
// (Final, Native, Protected)

void ABRPlayerController::ForceOnPrematchLevelLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.ForceOnPrematchLevelLoaded");

	ABRPlayerController_ForceOnPrematchLevelLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.ForceOnMapLevelLoaded
// (Final, Native, Protected)

void ABRPlayerController::ForceOnMapLevelLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.ForceOnMapLevelLoaded");

	ABRPlayerController_ForceOnMapLevelLoaded_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.ClientSetAndHoldCameraFade
// (Net, NetReliable, Native, Event, Public, HasDefaults, NetClient)
// Parameters:
// bool                           bEnableFading                  (Parm, ZeroConstructor, IsPlainOldData)
// struct FColor                  FadeColor                      (Parm, IsPlainOldData)
// struct FVector2D               FadeAlpha                      (Parm, IsPlainOldData)
// float                          FadeTime                       (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFadeAudio                     (Parm, ZeroConstructor, IsPlainOldData)

void ABRPlayerController::ClientSetAndHoldCameraFade(bool bEnableFading, const struct FColor& FadeColor, const struct FVector2D& FadeAlpha, float FadeTime, bool bFadeAudio)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.ClientSetAndHoldCameraFade");

	ABRPlayerController_ClientSetAndHoldCameraFade_Params params;
	params.bEnableFading = bEnableFading;
	params.FadeColor = FadeColor;
	params.FadeAlpha = FadeAlpha;
	params.FadeTime = FadeTime;
	params.bFadeAudio = bFadeAudio;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.BRPlayerController.ClientOnDropInCountdownStarted
// (Net, NetReliable, Native, Event, Public, NetClient)

void ABRPlayerController::ClientOnDropInCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.BRPlayerController.ClientOnDropInCountdownStarted");

	ABRPlayerController_ClientOnDropInCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.DaytimeActor.OnPlayerLeftVolume
// (Event, Public, BlueprintEvent)
// Parameters:
// class ADaytimeVolume*          Volume                         (Parm, ZeroConstructor, IsPlainOldData)

void ADaytimeActor::OnPlayerLeftVolume(class ADaytimeVolume* Volume)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.DaytimeActor.OnPlayerLeftVolume");

	ADaytimeActor_OnPlayerLeftVolume_Params params;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.DaytimeActor.OnPlayerEnteredVolume
// (Event, Public, BlueprintEvent)
// Parameters:
// class ADaytimeVolume*          Volume                         (Parm, ZeroConstructor, IsPlainOldData)

void ADaytimeActor::OnPlayerEnteredVolume(class ADaytimeVolume* Volume)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.DaytimeActor.OnPlayerEnteredVolume");

	ADaytimeActor_OnPlayerEnteredVolume_Params params;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.DaytimeVolume.OnBrushEndOverlap
// (Final, Native, Private)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)

void ADaytimeVolume::OnBrushEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.DaytimeVolume.OnBrushEndOverlap");

	ADaytimeVolume_OnBrushEndOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.DaytimeVolume.OnBrushBeginOverlap
// (Final, Native, Private, HasOutParms)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFromSweep                     (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              SweepResult                    (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)

void ADaytimeVolume::OnBrushBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.DaytimeVolume.OnBrushBeginOverlap");

	ADaytimeVolume_OnBrushBeginOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;
	params.bFromSweep = bFromSweep;
	params.SweepResult = SweepResult;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.Door.OpenDoor
// (Final, Native, Public)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void ADoor::OpenDoor(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.Door.OpenDoor");

	ADoor_OpenDoor_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.Door.OnRep_bIsOpen
// (Final, Native, Private)

void ADoor::OnRep_bIsOpen()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.Door.OnRep_bIsOpen");

	ADoor_OnRep_bIsOpen_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.Door.OnDoorOpened
// (Event, Public, BlueprintEvent)

void ADoor::OnDoorOpened()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.Door.OnDoorOpened");

	ADoor_OnDoorOpened_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.Door.OnDoorClosed
// (Event, Public, BlueprintEvent)

void ADoor::OnDoorClosed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.Door.OnDoorClosed");

	ADoor_OnDoorClosed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.Door.CloseDoor
// (Final, Native, Public)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void ADoor::CloseDoor(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.Door.CloseDoor");

	ADoor_CloseDoor_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.FriendsFunctionLibrary.IsFriendPlayingThisGame
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UFriendsFunctionLibrary::STATIC_IsFriendPlayingThisGame(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.IsFriendPlayingThisGame");

	UFriendsFunctionLibrary_IsFriendPlayingThisGame_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.IsFriendPlaying
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UFriendsFunctionLibrary::STATIC_IsFriendPlaying(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.IsFriendPlaying");

	UFriendsFunctionLibrary_IsFriendPlaying_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.IsFriendOnline
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UFriendsFunctionLibrary::STATIC_IsFriendOnline(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.IsFriendOnline");

	UFriendsFunctionLibrary_IsFriendOnline_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.IsFriendJoinable
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UFriendsFunctionLibrary::STATIC_IsFriendJoinable(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.IsFriendJoinable");

	UFriendsFunctionLibrary_IsFriendJoinable_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.IsFriendInvitable
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UFriendsFunctionLibrary::STATIC_IsFriendInvitable(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.IsFriendInvitable");

	UFriendsFunctionLibrary_IsFriendInvitable_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.GetFriendName
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UFriendsFunctionLibrary::STATIC_GetFriendName(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.GetFriendName");

	UFriendsFunctionLibrary_GetFriendName_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.GetFriendInviteStatus
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// EBlueprintInviteStatus         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EBlueprintInviteStatus UFriendsFunctionLibrary::STATIC_GetFriendInviteStatus(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.GetFriendInviteStatus");

	UFriendsFunctionLibrary_GetFriendInviteStatus_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.GetFriendId
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UFriendsFunctionLibrary::STATIC_GetFriendId(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.GetFriendId");

	UFriendsFunctionLibrary_GetFriendId_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.FriendsFunctionLibrary.GetFriendAvatar
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 Outer                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)
// ESteamAvatarSize               Size                           (Parm, ZeroConstructor, IsPlainOldData)
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UFriendsFunctionLibrary::STATIC_GetFriendAvatar(class UObject* Outer, const struct FBlueprintOnlineFriend& Friend, ESteamAvatarSize Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.FriendsFunctionLibrary.GetFriendAvatar");

	UFriendsFunctionLibrary_GetFriendAvatar_Params params;
	params.Outer = Outer;
	params.Friend = Friend;
	params.Size = Size;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IndoorVolume.OnComponentEndOverlap
// (Final, Native, Private)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)

void AIndoorVolume::OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IndoorVolume.OnComponentEndOverlap");

	AIndoorVolume_OnComponentEndOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IndoorVolume.OnComponentBeginOverlap
// (Final, Native, Private, HasOutParms)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFromSweep                     (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              SweepResult                    (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)

void AIndoorVolume::OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IndoorVolume.OnComponentBeginOverlap");

	AIndoorVolume_OnComponentBeginOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;
	params.bFromSweep = bFromSweep;
	params.SweepResult = SweepResult;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerTogglePlasma
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void UIONAdminComponent::ServerTogglePlasma()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerTogglePlasma");

	UIONAdminComponent_ServerTogglePlasma_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerStartMatch
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void UIONAdminComponent::ServerStartMatch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerStartMatch");

	UIONAdminComponent_ServerStartMatch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerSay
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// struct FString                 Command                        (Parm, ZeroConstructor)

void UIONAdminComponent::ServerSay(const struct FString& Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerSay");

	UIONAdminComponent_ServerSay_Params params;
	params.Command = Command;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerPausePrequeue
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void UIONAdminComponent::ServerPausePrequeue()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerPausePrequeue");

	UIONAdminComponent_ServerPausePrequeue_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerLogAdminCommand
// (Net, NetReliable, Native, Event, Protected, NetServer, NetValidate)
// Parameters:
// struct FString                 Command                        (Parm, ZeroConstructor)

void UIONAdminComponent::ServerLogAdminCommand(const struct FString& Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerLogAdminCommand");

	UIONAdminComponent_ServerLogAdminCommand_Params params;
	params.Command = Command;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerGiveSkin
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// int                            SteamDefID                     (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void UIONAdminComponent::ServerGiveSkin(int SteamDefID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerGiveSkin");

	UIONAdminComponent_ServerGiveSkin_Params params;
	params.SteamDefID = SteamDefID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.ServerExec
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// struct FString                 Command                        (Parm, ZeroConstructor)

void UIONAdminComponent::ServerExec(const struct FString& Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.ServerExec");

	UIONAdminComponent_ServerExec_Params params;
	params.Command = Command;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.DebugGiveSkin
// (Final, Exec, Native, Public)
// Parameters:
// int                            SteamDefID                     (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void UIONAdminComponent::DebugGiveSkin(int SteamDefID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.DebugGiveSkin");

	UIONAdminComponent_DebugGiveSkin_Params params;
	params.SteamDefID = SteamDefID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.AdminTogglePlasma
// (Final, Exec, Native, Public)

void UIONAdminComponent::AdminTogglePlasma()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.AdminTogglePlasma");

	UIONAdminComponent_AdminTogglePlasma_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.AdminStartMatch
// (Final, Exec, Native, Public)

void UIONAdminComponent::AdminStartMatch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.AdminStartMatch");

	UIONAdminComponent_AdminStartMatch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.AdminSay
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 Command                        (Parm, ZeroConstructor)

void UIONAdminComponent::AdminSay(const struct FString& Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.AdminSay");

	UIONAdminComponent_AdminSay_Params params;
	params.Command = Command;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.AdminPausePrequeue
// (Final, Exec, Native, Public)

void UIONAdminComponent::AdminPausePrequeue()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.AdminPausePrequeue");

	UIONAdminComponent_AdminPausePrequeue_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAdminComponent.AdminExec
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 Command                        (Parm, ZeroConstructor)

void UIONAdminComponent::AdminExec(const struct FString& Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAdminComponent.AdminExec");

	UIONAdminComponent_AdminExec_Params params;
	params.Command = Command;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.StopOutline
// (BlueprintCosmetic, Native, Public, BlueprintCallable)

void AIONItem::StopOutline()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.StopOutline");

	AIONItem_StopOutline_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.StartOutline
// (BlueprintCosmetic, Native, Public, BlueprintCallable)

void AIONItem::StartOutline()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.StartOutline");

	AIONItem_StartOutline_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.RemoveFromInventory
// (BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONItem::RemoveFromInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.RemoveFromInventory");

	AIONItem_RemoveFromInventory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.PlaceOnGround
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)

void AIONItem::PlaceOnGround()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.PlaceOnGround");

	AIONItem_PlaceOnGround_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.IsInInventory
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONItem::IsInInventory(class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.IsInInventory");

	AIONItem_IsInInventory_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.IsEquivalentTo
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONItem*                Other                          (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONItem::IsEquivalentTo(class AIONItem* Other)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.IsEquivalentTo");

	AIONItem_IsEquivalentTo_Params params;
	params.Other = Other;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.Internal_PickUp
// (Final, Native, Private)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONItem::Internal_PickUp(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.Internal_PickUp");

	AIONItem_Internal_PickUp_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.GetPickupLocation
// (Native, Event, Protected, HasDefaults, BlueprintEvent, Const)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector AIONItem::GetPickupLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetPickupLocation");

	AIONItem_GetPickupLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.GetItemTypeText
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText AIONItem::GetItemTypeText()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetItemTypeText");

	AIONItem_GetItemTypeText_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.GetItemPickupEvent
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UAkAudioEvent*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UAkAudioEvent* AIONItem::GetItemPickupEvent()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetItemPickupEvent");

	AIONItem_GetItemPickupEvent_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.GetItemDescription
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FItemDescription        ReturnValue                    (Parm, OutParm, ReturnParm)

struct FItemDescription AIONItem::GetItemDescription()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetItemDescription");

	AIONItem_GetItemDescription_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.GetCharacterChecked
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* AIONItem::GetCharacterChecked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetCharacterChecked");

	AIONItem_GetCharacterChecked_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.GetCharacter
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* AIONItem::GetCharacter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.GetCharacter");

	AIONItem_GetCharacter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.DestroyItemSafely
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// float                          LifeSpan                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONItem::DestroyItemSafely(float LifeSpan)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.DestroyItemSafely");

	AIONItem_DestroyItemSafely_Params params;
	params.LifeSpan = LifeSpan;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItem.CanBePickedUp
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONItem::CanBePickedUp()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.CanBePickedUp");

	AIONItem_CanBePickedUp_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItem.CanBeDropped
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONItem::CanBeDropped()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItem.CanBeDropped");

	AIONItem_CanBeDropped_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStackableItem.SetNumberOfItems
// (BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// int                            Value                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONStackableItem::SetNumberOfItems(int Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.SetNumberOfItems");

	AIONStackableItem_SetNumberOfItems_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStackableItem.RemoveItems
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONStackableItem::RemoveItems(int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.RemoveItems");

	AIONStackableItem_RemoveItems_Params params;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStackableItem.RemoveAllItems
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)

void AIONStackableItem::RemoveAllItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.RemoveAllItems");

	AIONStackableItem_RemoveAllItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStackableItem.IsStackable
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONStackableItem::IsStackable()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.IsStackable");

	AIONStackableItem_IsStackable_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStackableItem.GetNumberOfItems
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONStackableItem::GetNumberOfItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.GetNumberOfItems");

	AIONStackableItem_GetNumberOfItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStackableItem.GetMaxItems
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONStackableItem::GetMaxItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.GetMaxItems");

	AIONStackableItem_GetMaxItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStackableItem.CanBeMergedWith
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONStackableItem*       OtherItem                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONStackableItem::CanBeMergedWith(class AIONStackableItem* OtherItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.CanBeMergedWith");

	AIONStackableItem_CanBeMergedWith_Params params;
	params.OtherItem = OtherItem;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStackableItem.AddItems
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONStackableItem::AddItems(int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStackableItem.AddItems");

	AIONStackableItem_AddItems_Params params;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationBaseWidget.SetArmorVisibility
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bNewVisibility                 (Parm, ZeroConstructor, IsPlainOldData)
// class UClass*                  WearableToHide                 (Parm, ZeroConstructor, IsPlainOldData)

void UIONCustomizationBaseWidget::SetArmorVisibility(bool bNewVisibility, class UClass* WearableToHide)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationBaseWidget.SetArmorVisibility");

	UIONCustomizationBaseWidget_SetArmorVisibility_Params params;
	params.bNewVisibility = bNewVisibility;
	params.WearableToHide = WearableToHide;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationBaseWidget.GetIONPC
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONBasePlayerController* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONBasePlayerController* UIONCustomizationBaseWidget::GetIONPC()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationBaseWidget.GetIONPC");

	UIONCustomizationBaseWidget_GetIONPC_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCustomizationBaseWidget.GetDisplayChar
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* UIONCustomizationBaseWidget::GetDisplayChar()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationBaseWidget.GetDisplayChar");

	UIONCustomizationBaseWidget_GetDisplayChar_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCustomizationBaseWidget.GetCustomizationPawn
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCustomizationPawn*   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCustomizationPawn* UIONCustomizationBaseWidget::GetCustomizationPawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationBaseWidget.GetCustomizationPawn");

	UIONCustomizationBaseWidget_GetCustomizationPawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAppearanceScreen.UpdateSkinColorLoadoutIdx
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// unsigned char                  NewIdx                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONAppearanceScreen::UpdateSkinColorLoadoutIdx(unsigned char NewIdx)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.UpdateSkinColorLoadoutIdx");

	UIONAppearanceScreen_UpdateSkinColorLoadoutIdx_Params params;
	params.NewIdx = NewIdx;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAppearanceScreen.UpdateHairLoadoutIdx
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// unsigned char                  NewIdx                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONAppearanceScreen::UpdateHairLoadoutIdx(unsigned char NewIdx)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.UpdateHairLoadoutIdx");

	UIONAppearanceScreen_UpdateHairLoadoutIdx_Params params;
	params.NewIdx = NewIdx;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAppearanceScreen.UpdateFaceLoadoutIdx
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// unsigned char                  NewIdx                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONAppearanceScreen::UpdateFaceLoadoutIdx(unsigned char NewIdx)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.UpdateFaceLoadoutIdx");

	UIONAppearanceScreen_UpdateFaceLoadoutIdx_Params params;
	params.NewIdx = NewIdx;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAppearanceScreen.GetSkinColorLoadoutIdx
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// unsigned char                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

unsigned char UIONAppearanceScreen::GetSkinColorLoadoutIdx()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.GetSkinColorLoadoutIdx");

	UIONAppearanceScreen_GetSkinColorLoadoutIdx_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAppearanceScreen.GetHairLoadoutIdx
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// unsigned char                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

unsigned char UIONAppearanceScreen::GetHairLoadoutIdx()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.GetHairLoadoutIdx");

	UIONAppearanceScreen_GetHairLoadoutIdx_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIdx
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// unsigned char                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

unsigned char UIONAppearanceScreen::GetFaceLoadoutIdx()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIdx");

	UIONAppearanceScreen_GetFaceLoadoutIdx_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIcon
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONAppearanceScreen::GetFaceLoadoutIcon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIcon");

	UIONAppearanceScreen_GetFaceLoadoutIcon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWearable.SetMatParamOnBaseChar
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   ParamName                      (Parm, ZeroConstructor, IsPlainOldData)
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONWearable::SetMatParamOnBaseChar(const struct FName& ParamName, float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearable.SetMatParamOnBaseChar");

	AIONWearable_SetMatParamOnBaseChar_Params params;
	params.ParamName = ParamName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWearable.GetHealthRatio
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONWearable::GetHealthRatio()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearable.GetHealthRatio");

	AIONWearable_GetHealthRatio_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWearable.GetCharacterOwner
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* AIONWearable::GetCharacterOwner()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearable.GetCharacterOwner");

	AIONWearable_GetCharacterOwner_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWearable.BPEvent_DetachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void AIONWearable::BPEvent_DetachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearable.BPEvent_DetachWearable");

	AIONWearable_BPEvent_DetachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWearable.BPEvent_AttachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void AIONWearable::BPEvent_AttachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearable.BPEvent_AttachWearable");

	AIONWearable_BPEvent_AttachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONArmor.UpdateArmorSkin
// (Native, Protected)

void AIONArmor::UpdateArmorSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONArmor.UpdateArmorSkin");

	AIONArmor_UpdateArmorSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONArmor.RestoreDefaultSkin
// (Native, Public, BlueprintCallable)

void AIONArmor::RestoreDefaultSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONArmor.RestoreDefaultSkin");

	AIONArmor_RestoreDefaultSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONArmor.BPEvent_ApplyArmorMesh
// (Event, Public, BlueprintEvent)

void AIONArmor::BPEvent_ApplyArmorMesh()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONArmor.BPEvent_ApplyArmorMesh");

	AIONArmor_BPEvent_ApplyArmorMesh_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONArmor.ApplyArmorSkin
// (Native, Public, BlueprintCallable)
// Parameters:
// class UIONArmorSkin*           NewArmorSkin                   (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bForce                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONArmor::ApplyArmorSkin(class UIONArmorSkin* NewArmorSkin, bool bForce)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONArmor.ApplyArmorSkin");

	AIONArmor_ApplyArmorSkin_Params params;
	params.NewArmorSkin = NewArmorSkin;
	params.bForce = bForce;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSteamItem.GetRarityColor
// (Final, Native, Public, HasDefaults, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FColor                  ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FColor UIONSteamItem::GetRarityColor()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamItem.GetRarityColor");

	UIONSteamItem_GetRarityColor_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAttachment.IsCompatibleWith
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONAttachment::IsCompatibleWith(class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAttachment.IsCompatibleWith");

	AIONAttachment_IsCompatibleWith_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAttachment.GetAttachmentSlot
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

TEnumAsByte<EAttachmentSlot> AIONAttachment::GetAttachmentSlot()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAttachment.GetAttachmentSlot");

	AIONAttachment_GetAttachmentSlot_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONAttachment.DetachFromFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONAttachment::DetachFromFirearmBlueprint(class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAttachment.DetachFromFirearmBlueprint");

	AIONAttachment_DetachFromFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAttachment.AttachToFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONAttachment::AttachToFirearmBlueprint(class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAttachment.AttachToFirearmBlueprint");

	AIONAttachment_AttachToFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAudioVolume.OnComponentEndOverlap
// (Final, Native, Private)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)

void AIONAudioVolume::OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAudioVolume.OnComponentEndOverlap");

	AIONAudioVolume_OnComponentEndOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONAudioVolume.OnComponentBeginOverlap
// (Final, Native, Private, HasOutParms)
// Parameters:
// class UPrimitiveComponent*     OverlappedComponent            (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class AActor*                  OtherActor                     (Parm, ZeroConstructor, IsPlainOldData)
// class UPrimitiveComponent*     OtherComp                      (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            OtherBodyIndex                 (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFromSweep                     (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              SweepResult                    (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)

void AIONAudioVolume::OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONAudioVolume.OnComponentBeginOverlap");

	AIONAudioVolume_OnComponentBeginOverlap_Params params;
	params.OverlappedComponent = OverlappedComponent;
	params.OtherActor = OtherActor;
	params.OtherComp = OtherComp;
	params.OtherBodyIndex = OtherBodyIndex;
	params.bFromSweep = bFromSweep;
	params.SweepResult = SweepResult;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.SquadEliminated
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONSquadState*          Squad                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::SquadEliminated(class AIONSquadState* Squad)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.SquadEliminated");

	AIONGameRuleSet_SquadEliminated_Params params;
	params.Squad = Squad;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.SpawnPlayer
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class ABRPlayerController*     NewPlayer                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::SpawnPlayer(class ABRPlayerController* NewPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.SpawnPlayer");

	AIONGameRuleSet_SpawnPlayer_Params params;
	params.NewPlayer = NewPlayer;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.PostPlayerLogin
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class APlayerController*       Controller                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::PostPlayerLogin(class APlayerController* Controller)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.PostPlayerLogin");

	AIONGameRuleSet_PostPlayerLogin_Params params;
	params.Controller = Controller;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.PlayerSpawned
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class ABRPlayerController*     NewPlayer                      (Parm, ZeroConstructor, IsPlainOldData)
// class APawn*                   Pawn                           (Parm, ZeroConstructor, IsPlainOldData)
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::PlayerSpawned(class ABRPlayerController* NewPlayer, class APawn* Pawn, class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.PlayerSpawned");

	AIONGameRuleSet_PlayerSpawned_Params params;
	params.NewPlayer = NewPlayer;
	params.Pawn = Pawn;
	params.Character = Character;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.PlayerLogout
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AController*             Controller                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::PlayerLogout(class AController* Controller)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.PlayerLogout");

	AIONGameRuleSet_PlayerLogout_Params params;
	params.Controller = Controller;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.PlayerJoined
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AMainPlayerController*   NewPlayer                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::PlayerJoined(class AMainPlayerController* NewPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.PlayerJoined");

	AIONGameRuleSet_PlayerJoined_Params params;
	params.NewPlayer = NewPlayer;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.MatchStarted
// (Native, Event, Public, BlueprintEvent)

void AIONGameRuleSet::MatchStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.MatchStarted");

	AIONGameRuleSet_MatchStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.MatchLeft
// (Native, Event, Public, BlueprintEvent)

void AIONGameRuleSet::MatchLeft()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.MatchLeft");

	AIONGameRuleSet_MatchLeft_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.MatchJoined
// (Native, Event, Public, BlueprintEvent)

void AIONGameRuleSet::MatchJoined()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.MatchJoined");

	AIONGameRuleSet_MatchJoined_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.HasBattleRoyaleStarted
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONGameRuleSet::HasBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.HasBattleRoyaleStarted");

	AIONGameRuleSet_HasBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameRuleSet.GetGameMode
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONGameMode*            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONGameMode* AIONGameRuleSet::GetGameMode()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.GetGameMode");

	AIONGameRuleSet_GetGameMode_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameRuleSet.GetDisplayName
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONGameRuleSet::GetDisplayName()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.GetDisplayName");

	AIONGameRuleSet_GetDisplayName_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameRuleSet.CheckMatchOver
// (Native, Event, Public, BlueprintEvent)

void AIONGameRuleSet::CheckMatchOver()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.CheckMatchOver");

	AIONGameRuleSet_CheckMatchOver_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.CharacterTookDamage
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitInfo                HitInfo                        (Parm)
// float                          DamageAmount                   (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::CharacterTookDamage(class AIONCharacter* Character, const struct FHitInfo& HitInfo, float DamageAmount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.CharacterTookDamage");

	AIONGameRuleSet_CharacterTookDamage_Params params;
	params.Character = Character;
	params.HitInfo = HitInfo;
	params.DamageAmount = DamageAmount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.CharacterRevived
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter*           DownedCharacter                (Parm, ZeroConstructor, IsPlainOldData)
// class AIONCharacter*           ReviverCharacters              (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameRuleSet::CharacterRevived(class AIONCharacter* DownedCharacter, class AIONCharacter* ReviverCharacters)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.CharacterRevived");

	AIONGameRuleSet_CharacterRevived_Params params;
	params.DownedCharacter = DownedCharacter;
	params.ReviverCharacters = ReviverCharacters;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.CharacterDowned
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitInfo                LastHitInfo                    (Parm)

void AIONGameRuleSet::CharacterDowned(class AIONCharacter* Character, const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.CharacterDowned");

	AIONGameRuleSet_CharacterDowned_Params params;
	params.Character = Character;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameRuleSet.CharacterDied
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitInfo                LastHitInfo                    (Parm)

void AIONGameRuleSet::CharacterDied(class AIONCharacter* Character, const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameRuleSet.CharacterDied");

	AIONGameRuleSet_CharacterDied_Params params;
	params.Character = Character;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONBehaviorButton.HandleClick
// (Final, Native, Private)

void UIONBehaviorButton::HandleClick()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONBehaviorButton.HandleClick");

	UIONBehaviorButton_HandleClick_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONButton.OnReleased
// (Native, Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONButton::OnReleased(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONButton.OnReleased");

	AIONButton_OnReleased_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONButton.OnPressed
// (Native, Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONButton::OnPressed(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONButton.OnPressed");

	AIONButton_OnPressed_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.SpectatePlayer
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCameraMan::SpectatePlayer(class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.SpectatePlayer");

	AIONCameraMan_SpectatePlayer_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.SpectateModeToString
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONCameraMan::SpectateModeToString()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.SpectateModeToString");

	AIONCameraMan_SpectateModeToString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCameraMan.ServerSetFlySpeedTarget
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// float                          InFlySpeedTarget               (Parm, ZeroConstructor, IsPlainOldData)

void AIONCameraMan::ServerSetFlySpeedTarget(float InFlySpeedTarget)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.ServerSetFlySpeedTarget");

	AIONCameraMan_ServerSetFlySpeedTarget_Params params;
	params.InFlySpeedTarget = InFlySpeedTarget;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.ServerDetachFromPlayer
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AIONCameraMan::ServerDetachFromPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.ServerDetachFromPlayer");

	AIONCameraMan_ServerDetachFromPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.ServerAttachToPlayer
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bFirstPerson                   (Parm, ZeroConstructor, IsPlainOldData)

void AIONCameraMan::ServerAttachToPlayer(class AIONCharacter* Character, bool bFirstPerson)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.ServerAttachToPlayer");

	AIONCameraMan_ServerAttachToPlayer_Params params;
	params.Character = Character;
	params.bFirstPerson = bFirstPerson;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.PlayercardViewModeToString
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONCameraMan::PlayercardViewModeToString()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.PlayercardViewModeToString");

	AIONCameraMan_PlayercardViewModeToString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCameraMan.OnOptionChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// int                            OptionIndex                    (Parm, ZeroConstructor, IsPlainOldData)

void AIONCameraMan::OnOptionChanged(int OptionIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.OnOptionChanged");

	AIONCameraMan_OnOptionChanged_Params params;
	params.OptionIndex = OptionIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCameraMan.HUDViewToString
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONCameraMan::HUDViewToString()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.HUDViewToString");

	AIONCameraMan_HUDViewToString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCameraMan.DrawHUD
// (Native, Event, Public, BlueprintEvent)
// Parameters:
// class AIONObserverCamHUD*      PlayerHUD                      (Parm, ZeroConstructor, IsPlainOldData)
// class UCanvas*                 Canvas                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCameraMan::DrawHUD(class AIONObserverCamHUD* PlayerHUD, class UCanvas* Canvas)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCameraMan.DrawHUD");

	AIONCameraMan_DrawHUD_Params params;
	params.PlayerHUD = PlayerHUD;
	params.Canvas = Canvas;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.WantsToSprint
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::WantsToSprint()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.WantsToSprint");

	AIONCharacter_WantsToSprint_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.WantsToProne
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::WantsToProne()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.WantsToProne");

	AIONCharacter_WantsToProne_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.WantsToFire
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::WantsToFire()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.WantsToFire");

	AIONCharacter_WantsToFire_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.UpdateSuppressionEffects
// (Event, Public, BlueprintEvent)
// Parameters:
// float                          SuppressionRatio               (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::UpdateSuppressionEffects(float SuppressionRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateSuppressionEffects");

	AIONCharacter_UpdateSuppressionEffects_Params params;
	params.SuppressionRatio = SuppressionRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateShieldEffects
// (Final, Native, Public, BlueprintCallable)

void AIONCharacter::UpdateShieldEffects()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateShieldEffects");

	AIONCharacter_UpdateShieldEffects_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateHealthEffects
// (Event, Public, BlueprintEvent)
// Parameters:
// float                          HealthRatio                    (Parm, ZeroConstructor, IsPlainOldData)
// float                          OldHealthRatio                 (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::UpdateHealthEffects(float HealthRatio, float OldHealthRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateHealthEffects");

	AIONCharacter_UpdateHealthEffects_Params params;
	params.HealthRatio = HealthRatio;
	params.OldHealthRatio = OldHealthRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateFirstPersonVisibility
// (Final, Native, Public, BlueprintCallable)

void AIONCharacter::UpdateFirstPersonVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateFirstPersonVisibility");

	AIONCharacter_UpdateFirstPersonVisibility_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateEquipementSound_Wearable
// (Final, Native, Public)

void AIONCharacter::UpdateEquipementSound_Wearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateEquipementSound_Wearable");

	AIONCharacter_UpdateEquipementSound_Wearable_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateEquipementSound_Firearms
// (Final, Native, Public)

void AIONCharacter::UpdateEquipementSound_Firearms()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateEquipementSound_Firearms");

	AIONCharacter_UpdateEquipementSound_Firearms_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateEquipementSound_BoosterOrNade
// (Final, Native, Public)

void AIONCharacter::UpdateEquipementSound_BoosterOrNade()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateEquipementSound_BoosterOrNade");

	AIONCharacter_UpdateEquipementSound_BoosterOrNade_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UpdateDropsuitSkin
// (Final, Native, Public)

void AIONCharacter::UpdateDropsuitSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UpdateDropsuitSkin");

	AIONCharacter_UpdateDropsuitSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.UnloadAmmo
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONFirearm*             Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::UnloadAmmo(class AIONFirearm* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.UnloadAmmo");

	AIONCharacter_UnloadAmmo_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.TimeLeftTillImpact
// (Final, Native, Public, HasOutParms)
// Parameters:
// float                          OutTime                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              OutHit                         (Parm, OutParm, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::TimeLeftTillImpact(float* OutTime, struct FHitResult* OutHit)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.TimeLeftTillImpact");

	AIONCharacter_TimeLeftTillImpact_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutTime != nullptr)
		*OutTime = params.OutTime;
	if (OutHit != nullptr)
		*OutHit = params.OutHit;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.StopFiring
// (Final, Native, Public, BlueprintCallable)

void AIONCharacter::StopFiring()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.StopFiring");

	AIONCharacter_StopFiring_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.StopAiming
// (Final, Native, Private, BlueprintCallable)

void AIONCharacter::StopAiming()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.StopAiming");

	AIONCharacter_StopAiming_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.SlotIsEnabled
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::SlotIsEnabled(int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SlotIsEnabled");

	AIONCharacter_SlotIsEnabled_Params params;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.SlotAcceptsItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)
// class AIONItem*                Item                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::SlotAcceptsItem(int SlotIndex, class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SlotAcceptsItem");

	AIONCharacter_SlotAcceptsItem_Params params;
	params.SlotIndex = SlotIndex;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.ShouldPlayAnim
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::ShouldPlayAnim()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ShouldPlayAnim");

	AIONCharacter_ShouldPlayAnim_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.SetWorldOrigin
// (Final, Exec, Native, Public)
// Parameters:
// int                            X                              (Parm, ZeroConstructor, IsPlainOldData)
// int                            Y                              (Parm, ZeroConstructor, IsPlainOldData)
// int                            Z                              (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::SetWorldOrigin(int X, int Y, int Z)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SetWorldOrigin");

	AIONCharacter_SetWorldOrigin_Params params;
	params.X = X;
	params.Y = Y;
	params.Z = Z;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.SetSprinting
// (Native, Public, BlueprintCallable)
// Parameters:
// bool                           NewSprinting                   (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::SetSprinting(bool NewSprinting)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SetSprinting");

	AIONCharacter_SetSprinting_Params params;
	params.NewSprinting = NewSprinting;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.SetProne
// (Native, Public, BlueprintCallable)
// Parameters:
// bool                           NewProne                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::SetProne(bool NewProne)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SetProne");

	AIONCharacter_SetProne_Params params;
	params.NewProne = NewProne;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.SetCrouching
// (Native, Public, BlueprintCallable)
// Parameters:
// bool                           NewCrouch                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::SetCrouching(bool NewCrouch)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SetCrouching");

	AIONCharacter_SetCrouching_Params params;
	params.NewCrouch = NewCrouch;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.SetCharacterMesh
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 MeshName                       (Parm, ZeroConstructor)

void AIONCharacter::SetCharacterMesh(const struct FString& MeshName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.SetCharacterMesh");

	AIONCharacter_SetCharacterMesh_Params params;
	params.MeshName = MeshName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateReviving
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// bool                           NewReviving                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateReviving(bool NewReviving)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateReviving");

	AIONCharacter_ServerUpdateReviving_Params params;
	params.NewReviving = NewReviving;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateMapOpen
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// bool                           NewMapOpen                     (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateMapOpen(bool NewMapOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateMapOpen");

	AIONCharacter_ServerUpdateMapOpen_Params params;
	params.NewMapOpen = NewMapOpen;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateInteracting
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// bool                           NewInteracting                 (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateInteracting(bool NewInteracting)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateInteracting");

	AIONCharacter_ServerUpdateInteracting_Params params;
	params.NewInteracting = NewInteracting;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateInspectingWeapon
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// bool                           NewInspectingWeapon            (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateInspectingWeapon(bool NewInspectingWeapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateInspectingWeapon");

	AIONCharacter_ServerUpdateInspectingWeapon_Params params;
	params.NewInspectingWeapon = NewInspectingWeapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateFiring
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// bool                           NewFiring                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateFiring(bool NewFiring)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateFiring");

	AIONCharacter_ServerUpdateFiring_Params params;
	params.NewFiring = NewFiring;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateFireRandomSeed
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// unsigned char                  NewFireRandomSeed              (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateFireRandomSeed(unsigned char NewFireRandomSeed)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateFireRandomSeed");

	AIONCharacter_ServerUpdateFireRandomSeed_Params params;
	params.NewFireRandomSeed = NewFireRandomSeed;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateAttachmentActive
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// bool                           NewAttachmentActive            (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateAttachmentActive(bool NewAttachmentActive)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateAttachmentActive");

	AIONCharacter_ServerUpdateAttachmentActive_Params params;
	params.NewAttachmentActive = NewAttachmentActive;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUpdateAiming
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// bool                           NewAiming                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// float                          NewAimRatio                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUpdateAiming(bool NewAiming, float NewAimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUpdateAiming");

	AIONCharacter_ServerUpdateAiming_Params params;
	params.NewAiming = NewAiming;
	params.NewAimRatio = NewAimRatio;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerUnloadAmmo
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONFirearm*             Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerUnloadAmmo(class AIONFirearm* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerUnloadAmmo");

	AIONCharacter_ServerUnloadAmmo_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerStopRevive
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)
// class AIONCharacter*           Other                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerStopRevive(class AMainPlayerController* PC, class AIONCharacter* Other)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerStopRevive");

	AIONCharacter_ServerStopRevive_Params params;
	params.PC = PC;
	params.Other = Other;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerStartRevive
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)
// class AIONCharacter*           Other                          (Parm, ZeroConstructor, IsPlainOldData)
// float                          NewReviveTime                  (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerStartRevive(class AMainPlayerController* PC, class AIONCharacter* Other, float NewReviveTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerStartRevive");

	AIONCharacter_ServerStartRevive_Params params;
	params.PC = PC;
	params.Other = Other;
	params.NewReviveTime = NewReviveTime;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerSetNewCachedInteractObject
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class UObject*                 NewCachedInteractObject        (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerSetNewCachedInteractObject(class UObject* NewCachedInteractObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerSetNewCachedInteractObject");

	AIONCharacter_ServerSetNewCachedInteractObject_Params params;
	params.NewCachedInteractObject = NewCachedInteractObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerSetIsSlowingBloodLoss
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// bool                           NewIsSlowing                   (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerSetIsSlowingBloodLoss(bool NewIsSlowing)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerSetIsSlowingBloodLoss");

	AIONCharacter_ServerSetIsSlowingBloodLoss_Params params;
	params.NewIsSlowing = NewIsSlowing;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerSetInspectingSkin
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// bool                           bNewInspectingSkin             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerSetInspectingSkin(bool bNewInspectingSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerSetInspectingSkin");

	AIONCharacter_ServerSetInspectingSkin_Params params;
	params.bNewInspectingSkin = bNewInspectingSkin;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerSetFreeLook
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// bool                           NewFreeLook                    (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerSetFreeLook(bool NewFreeLook)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerSetFreeLook");

	AIONCharacter_ServerSetFreeLook_Params params;
	params.NewFreeLook = NewFreeLook;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerPickUpSpawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerPickUpSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerPickUpSpawn");

	AIONCharacter_ServerPickUpSpawn_Params params;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerPickUp
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerPickUp(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerPickUp");

	AIONCharacter_ServerPickUp_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerMoveMagazineToSlot
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerMoveMagazineToSlot(class AIONFirearm* Firearm, int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerMoveMagazineToSlot");

	AIONCharacter_ServerMoveMagazineToSlot_Params params;
	params.Firearm = Firearm;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerMoveItemToSlotSpawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerMoveItemToSlotSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerMoveItemToSlotSpawn");

	AIONCharacter_ServerMoveItemToSlotSpawn_Params params;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerMoveItemToSlot
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerMoveItemToSlot(class AIONItem* Item, int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerMoveItemToSlot");

	AIONCharacter_ServerMoveItemToSlot_Params params;
	params.Item = Item;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerMergeItemsSpawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)
// class AIONStackableItem*       To                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerMergeItemsSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, class AIONStackableItem* To)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerMergeItemsSpawn");

	AIONCharacter_ServerMergeItemsSpawn_Params params;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;
	params.To = To;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerMergeItems
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONStackableItem*       Item                           (Parm, ZeroConstructor, IsPlainOldData)
// class AIONStackableItem*       To                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerMergeItems(class AIONStackableItem* Item, class AIONStackableItem* To)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerMergeItems");

	AIONCharacter_ServerMergeItems_Params params;
	params.Item = Item;
	params.To = To;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerExitVehicle
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AIONCharacter::ServerExitVehicle()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerExitVehicle");

	AIONCharacter_ServerExitVehicle_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerEquipWearableSpawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerEquipWearableSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerEquipWearableSpawn");

	AIONCharacter_ServerEquipWearableSpawn_Params params;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerEquipWearable
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONWearable*            Wearable                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerEquipWearable(class AIONWearable* Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerEquipWearable");

	AIONCharacter_ServerEquipWearable_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerEquipWeapon
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerEquipWeapon(class AIONWeapon* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerEquipWeapon");

	AIONCharacter_ServerEquipWeapon_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerEnterVehicle
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AVehicleBase*            Vehicle                        (Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   SeatName                       (ConstParm, Parm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void AIONCharacter::ServerEnterVehicle(class AVehicleBase* Vehicle, const struct FName& SeatName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerEnterVehicle");

	AIONCharacter_ServerEnterVehicle_Params params;
	params.Vehicle = Vehicle;
	params.SeatName = SeatName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerDropStackable
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONStackableItem*       Item                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerDropStackable(class AIONStackableItem* Item, int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerDropStackable");

	AIONCharacter_ServerDropStackable_Params params;
	params.Item = Item;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerDropItem
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bShuffle                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerDropItem(class AIONItem* Item, bool bShuffle)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerDropItem");

	AIONCharacter_ServerDropItem_Params params;
	params.Item = Item;
	params.bShuffle = bShuffle;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerDropAmmoPack
// (Final, Net, NetReliable, Native, Event, Private, NetServer, NetValidate)
// Parameters:
// EFirearmType                   AmmoType                       (Parm, ZeroConstructor, IsPlainOldData)
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerDropAmmoPack(EFirearmType AmmoType, int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerDropAmmoPack");

	AIONCharacter_ServerDropAmmoPack_Params params;
	params.AmmoType = AmmoType;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearmSpawn
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerAttachAttachmentToFirearmSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearmSpawn");

	AIONCharacter_ServerAttachAttachmentToFirearmSpawn_Params params;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearm
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONAttachment*          Attachment                     (Parm, ZeroConstructor, IsPlainOldData)
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ServerAttachAttachmentToFirearm(class AIONAttachment* Attachment, class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearm");

	AIONCharacter_ServerAttachAttachmentToFirearm_Params params;
	params.Attachment = Attachment;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ReviveReleased
// (Final, Native, Public)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ReviveReleased(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ReviveReleased");

	AIONCharacter_ReviveReleased_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.RevivePressed
// (Final, Native, Public)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::RevivePressed(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.RevivePressed");

	AIONCharacter_RevivePressed_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ResetBulletPaths
// (Final, Exec, Native, Public)

void AIONCharacter::ResetBulletPaths()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ResetBulletPaths");

	AIONCharacter_ResetBulletPaths_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.PickUp
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::PickUp(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.PickUp");

	AIONCharacter_PickUp_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnStepped
// (Final, Native, Public, BlueprintCallable)

void AIONCharacter::OnStepped()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnStepped");

	AIONCharacter_OnStepped_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONCharacter.OnRevivingStopped__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONCharacter::OnRevivingStopped__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONCharacter.OnRevivingStopped__DelegateSignature");

	AIONCharacter_OnRevivingStopped__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONCharacter.OnRevivingStarted__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONCharacter::OnRevivingStarted__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONCharacter.OnRevivingStarted__DelegateSignature");

	AIONCharacter_OnRevivingStarted__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONCharacter.OnRevived__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONCharacter::OnRevived__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONCharacter.OnRevived__DelegateSignature");

	AIONCharacter_OnRevived__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_VehicleSeatName
// (Final, Native, Public)

void AIONCharacter::OnRep_VehicleSeatName()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_VehicleSeatName");

	AIONCharacter_OnRep_VehicleSeatName_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_Reviving
// (Native, Public)

void AIONCharacter::OnRep_Reviving()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_Reviving");

	AIONCharacter_OnRep_Reviving_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_QueuedWeapon
// (Final, Native, Private)
// Parameters:
// class AIONWeapon*              OldWeapon                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::OnRep_QueuedWeapon(class AIONWeapon* OldWeapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_QueuedWeapon");

	AIONCharacter_OnRep_QueuedWeapon_Params params;
	params.OldWeapon = OldWeapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_IsSprinting
// (Native, Public)

void AIONCharacter::OnRep_IsSprinting()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_IsSprinting");

	AIONCharacter_OnRep_IsSprinting_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_IsProne
// (Native, Public)

void AIONCharacter::OnRep_IsProne()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_IsProne");

	AIONCharacter_OnRep_IsProne_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_IsDowned
// (Native, Public)

void AIONCharacter::OnRep_IsDowned()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_IsDowned");

	AIONCharacter_OnRep_IsDowned_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_DamageInfoArray
// (Final, Native, Private)

void AIONCharacter::OnRep_DamageInfoArray()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_DamageInfoArray");

	AIONCharacter_OnRep_DamageInfoArray_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnRep_ChangeDropIn
// (Final, Native, Public)
// Parameters:
// bool                           OldIsInDrop                    (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::OnRep_ChangeDropIn(bool OldIsInDrop)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnRep_ChangeDropIn");

	AIONCharacter_OnRep_ChangeDropIn_Params params;
	params.OldIsInDrop = OldIsInDrop;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnInventoryChanged
// (Final, Native, Private)

void AIONCharacter::OnInventoryChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnInventoryChanged");

	AIONCharacter_OnInventoryChanged_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnHardLanding
// (Event, Public, BlueprintEvent)

void AIONCharacter::OnHardLanding()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnHardLanding");

	AIONCharacter_OnHardLanding_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONCharacter.OnDowned__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONCharacter::OnDowned__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONCharacter.OnDowned__DelegateSignature");

	AIONCharacter_OnDowned__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONCharacter.OnDied__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void AIONCharacter::OnDied__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONCharacter.OnDied__DelegateSignature");

	AIONCharacter_OnDied__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnBulletSuppression
// (Event, Public, BlueprintEvent)
// Parameters:
// class AMainProjectile*         Projectile                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::OnBulletSuppression(class AMainProjectile* Projectile)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnBulletSuppression");

	AIONCharacter_OnBulletSuppression_Params params;
	params.Projectile = Projectile;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.OnAimingStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// float                          NewAimRatio                    (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bForce                         (Parm, ZeroConstructor, IsPlainOldData)
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::OnAimingStateChanged(float NewAimRatio, bool bForce, class AIONWeapon* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.OnAimingStateChanged");

	AIONCharacter_OnAimingStateChanged_Params params;
	params.NewAimRatio = NewAimRatio;
	params.bForce = bForce;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.MulticastWeaponTask
// (Final, Net, Native, Event, NetMulticast, Private)
// Parameters:
// EWeaponTask                    TaskID                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::MulticastWeaponTask(EWeaponTask TaskID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.MulticastWeaponTask");

	AIONCharacter_MulticastWeaponTask_Params params;
	params.TaskID = TaskID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.MulticastLaunchPadJump
// (Net, NetReliable, Native, Event, NetMulticast, Public, HasDefaults)
// Parameters:
// struct FVector                 LaunchPadLocation              (Parm, IsPlainOldData)
// struct FRotator                LaunchPadRotator               (Parm, IsPlainOldData)

void AIONCharacter::MulticastLaunchPadJump(const struct FVector& LaunchPadLocation, const struct FRotator& LaunchPadRotator)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.MulticastLaunchPadJump");

	AIONCharacter_MulticastLaunchPadJump_Params params;
	params.LaunchPadLocation = LaunchPadLocation;
	params.LaunchPadRotator = LaunchPadRotator;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.MoveMagazineToSlot
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::MoveMagazineToSlot(class AIONFirearm* Firearm, int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.MoveMagazineToSlot");

	AIONCharacter_MoveMagazineToSlot_Params params;
	params.Firearm = Firearm;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.MoveItemToSlot
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            SlotIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::MoveItemToSlot(class AIONItem* Item, int SlotIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.MoveItemToSlot");

	AIONCharacter_MoveItemToSlot_Params params;
	params.Item = Item;
	params.SlotIndex = SlotIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.MergeItems
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONStackableItem*       From                           (Parm, ZeroConstructor, IsPlainOldData)
// class AIONStackableItem*       To                             (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::MergeItems(class AIONStackableItem* From, class AIONStackableItem* To)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.MergeItems");

	AIONCharacter_MergeItems_Params params;
	params.From = From;
	params.To = To;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.LaunchPadJump
// (Final, Native, Public, HasDefaults, BlueprintCallable)
// Parameters:
// struct FVector                 LaunchPadLocation              (Parm, IsPlainOldData)
// struct FRotator                LaunchPadRotator               (Parm, IsPlainOldData)

void AIONCharacter::LaunchPadJump(const struct FVector& LaunchPadLocation, const struct FRotator& LaunchPadRotator)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.LaunchPadJump");

	AIONCharacter_LaunchPadJump_Params params;
	params.LaunchPadLocation = LaunchPadLocation;
	params.LaunchPadRotator = LaunchPadRotator;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.Kill
// (Final, Exec, Native, Public)

void AIONCharacter::Kill()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.Kill");

	AIONCharacter_Kill_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.IsSprinting
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsSprinting()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsSprinting");

	AIONCharacter_IsSprinting_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsSameSquadAs
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class APlayerState*            OtherPlayer                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsSameSquadAs(class APlayerState* OtherPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsSameSquadAs");

	AIONCharacter_IsSameSquadAs_Params params;
	params.OtherPlayer = OtherPlayer;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsProne
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsProne()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsProne");

	AIONCharacter_IsProne_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsMapOpen
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsMapOpen()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsMapOpen");

	AIONCharacter_IsMapOpen_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsInWater
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsInWater()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsInWater");

	AIONCharacter_IsInWater_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsInspectingWeapon
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsInspectingWeapon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsInspectingWeapon");

	AIONCharacter_IsInspectingWeapon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsInspectingSkin
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsInspectingSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsInspectingSkin");

	AIONCharacter_IsInspectingSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsInInvincibilityVolume
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsInInvincibilityVolume()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsInInvincibilityVolume");

	AIONCharacter_IsInInvincibilityVolume_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsInFirstPersonView
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsInFirstPersonView()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsInFirstPersonView");

	AIONCharacter_IsInFirstPersonView_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsHoldingMovementKeys
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsHoldingMovementKeys()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsHoldingMovementKeys");

	AIONCharacter_IsHoldingMovementKeys_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsFreeLooking
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsFreeLooking()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsFreeLooking");

	AIONCharacter_IsFreeLooking_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsFiring
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsFiring()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsFiring");

	AIONCharacter_IsFiring_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsDowned
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsDowned()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsDowned");

	AIONCharacter_IsDowned_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.IsDead
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::IsDead()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.IsDead");

	AIONCharacter_IsDead_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.InventoryStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bOpen                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::InventoryStateChanged(bool bOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.InventoryStateChanged");

	AIONCharacter_InventoryStateChanged_Params params;
	params.bOpen = bOpen;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.HasItem
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UClass*                  ItemClass                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           OutReturn                      (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* AIONCharacter::HasItem(class UClass* ItemClass, bool* OutReturn)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.HasItem");

	AIONCharacter_HasItem_Params params;
	params.ItemClass = ItemClass;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutReturn != nullptr)
		*OutReturn = params.OutReturn;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.HasBattleRoyaleStarted
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::HasBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.HasBattleRoyaleStarted");

	AIONCharacter_HasBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GiveItem
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 ClassName                      (Parm, ZeroConstructor)

void AIONCharacter::GiveItem(const struct FString& ClassName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GiveItem");

	AIONCharacter_GiveItem_Params params;
	params.ClassName = ClassName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.GetWeaponTask
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EWeaponTask                    ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EWeaponTask AIONCharacter::GetWeaponTask()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetWeaponTask");

	AIONCharacter_GetWeaponTask_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetViewYaw
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetViewYaw()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetViewYaw");

	AIONCharacter_GetViewYaw_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetViewPitch
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetViewPitch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetViewPitch");

	AIONCharacter_GetViewPitch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetStatusIndicatorForItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONItem*                Item                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// EItemStatusIndicator           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EItemStatusIndicator AIONCharacter::GetStatusIndicatorForItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetStatusIndicatorForItem");

	AIONCharacter_GetStatusIndicatorForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetStance
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// ECharacterStance               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ECharacterStance AIONCharacter::GetStance()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetStance");

	AIONCharacter_GetStance_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetProximityItems
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          MaxRange                       (Parm, ZeroConstructor, IsPlainOldData)
// TArray<class AIONItem*>        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class AIONItem*> AIONCharacter::GetProximityItems(float MaxRange)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetProximityItems");

	AIONCharacter_GetProximityItems_Params params;
	params.MaxRange = MaxRange;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetKillerPlayer
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class APlayerState*            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class APlayerState* AIONCharacter::GetKillerPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetKillerPlayer");

	AIONCharacter_GetKillerPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetInteractionItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* AIONCharacter::GetInteractionItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetInteractionItem");

	AIONCharacter_GetInteractionItem_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetHealth
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetHealth()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetHealth");

	AIONCharacter_GetHealth_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetFreeLookAmmount
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetFreeLookAmmount()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetFreeLookAmmount");

	AIONCharacter_GetFreeLookAmmount_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetDamageDealthByPlayer
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class APlayerState*            Player                         (Parm, ZeroConstructor, IsPlainOldData)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetDamageDealthByPlayer(class APlayerState* Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetDamageDealthByPlayer");

	AIONCharacter_GetDamageDealthByPlayer_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetCurrentWeapon
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONWeapon*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONWeapon* AIONCharacter::GetCurrentWeapon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetCurrentWeapon");

	AIONCharacter_GetCurrentWeapon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetCameraManager
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class APlayerCameraManager*    ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class APlayerCameraManager* AIONCharacter::GetCameraManager()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetCameraManager");

	AIONCharacter_GetCameraManager_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetAimRatio
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONCharacter::GetAimRatio()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetAimRatio");

	AIONCharacter_GetAimRatio_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.GetAiming
// (Final, Native, Private, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::GetAiming()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.GetAiming");

	AIONCharacter_GetAiming_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.FindEmptySlotForItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONCharacter::FindEmptySlotForItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.FindEmptySlotForItem");

	AIONCharacter_FindEmptySlotForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.EventBP_LandingTrigger
// (Event, Public, BlueprintEvent)

void AIONCharacter::EventBP_LandingTrigger()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EventBP_LandingTrigger");

	AIONCharacter_EventBP_LandingTrigger_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EventBP_LandedOnTree
// (Event, Public, HasDefaults, BlueprintEvent)
// Parameters:
// struct FTransform              TreeTransform                  (Parm, IsPlainOldData)

void AIONCharacter::EventBP_LandedOnTree(const struct FTransform& TreeTransform)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EventBP_LandedOnTree");

	AIONCharacter_EventBP_LandedOnTree_Params params;
	params.TreeTransform = TreeTransform;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EventBP_DropInModeStarted
// (Event, Public, BlueprintEvent)

void AIONCharacter::EventBP_DropInModeStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EventBP_DropInModeStarted");

	AIONCharacter_EventBP_DropInModeStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EventBP_DropInModeEnded
// (Event, Public, BlueprintEvent)

void AIONCharacter::EventBP_DropInModeEnded()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EventBP_DropInModeEnded");

	AIONCharacter_EventBP_DropInModeEnded_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EquipWearable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONWearable*            Wearable                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::EquipWearable(class AIONWearable* Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipWearable");

	AIONCharacter_EquipWearable_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EquipWeapon
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bForce                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::EquipWeapon(class AIONWeapon* Weapon, bool bForce)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipWeapon");

	AIONCharacter_EquipWeapon_Params params;
	params.Weapon = Weapon;
	params.bForce = bForce;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EquipSlot
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// unsigned char                  Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// EInventoryType                 InventoryType                  (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::EquipSlot(unsigned char Slot, EInventoryType InventoryType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipSlot");

	AIONCharacter_EquipSlot_Params params;
	params.Slot = Slot;
	params.InventoryType = InventoryType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.EquipPreviousWeapon
// (Final, Native, Private, BlueprintCallable)

void AIONCharacter::EquipPreviousWeapon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipPreviousWeapon");

	AIONCharacter_EquipPreviousWeapon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EquipKnife
// (Final, Native, Public, BlueprintCallable)

void AIONCharacter::EquipKnife()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipKnife");

	AIONCharacter_EquipKnife_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.EquipCosmeticItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::EquipCosmeticItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.EquipCosmeticItem");

	AIONCharacter_EquipCosmeticItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.DropStackable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONStackableItem*       Item                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::DropStackable(class AIONStackableItem* Item, int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.DropStackable");

	AIONCharacter_DropStackable_Params params;
	params.Item = Item;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.DropItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bShuffle                       (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bForce                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::DropItem(class AIONItem* Item, bool bShuffle, bool bForce)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.DropItem");

	AIONCharacter_DropItem_Params params;
	params.Item = Item;
	params.bShuffle = bShuffle;
	params.bForce = bForce;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.DropAmmoPack
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EFirearmType                   AmmoType                       (Parm, ZeroConstructor, IsPlainOldData)
// int                            Amount                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::DropAmmoPack(EFirearmType AmmoType, int Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.DropAmmoPack");

	AIONCharacter_DropAmmoPack_Params params;
	params.AmmoType = AmmoType;
	params.Amount = Amount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ChangeShieldEffects
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bNewActive                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ChangeShieldEffects(bool bNewActive)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ChangeShieldEffects");

	AIONCharacter_ChangeShieldEffects_Params params;
	params.bNewActive = bNewActive;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ChangeDropInMode
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bEnter                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ChangeDropInMode(bool bEnter)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ChangeDropInMode");

	AIONCharacter_ChangeDropInMode_Params params;
	params.bEnter = bEnter;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.CanFreeLook
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONCharacter::CanFreeLook()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.CanFreeLook");

	AIONCharacter_CanFreeLook_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacter.BP_SimulatedHit
// (Event, Public, HasDefaults, BlueprintEvent)
// Parameters:
// struct FName                   BoneName                       (Parm, ZeroConstructor, IsPlainOldData)
// float                          DamageImpulse                  (Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 ShotFromDirection              (Parm, IsPlainOldData)

void AIONCharacter::BP_SimulatedHit(const struct FName& BoneName, float DamageImpulse, const struct FVector& ShotFromDirection)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.BP_SimulatedHit");

	AIONCharacter_BP_SimulatedHit_Params params;
	params.BoneName = BoneName;
	params.DamageImpulse = DamageImpulse;
	params.ShotFromDirection = ShotFromDirection;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.BP_ProceduralFireAnimation
// (Event, Public, BlueprintEvent)

void AIONCharacter::BP_ProceduralFireAnimation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.BP_ProceduralFireAnimation");

	AIONCharacter_BP_ProceduralFireAnimation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.AttachAttachmentToFirearm
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONAttachment*          Attachment                     (Parm, ZeroConstructor, IsPlainOldData)
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::AttachAttachmentToFirearm(class AIONAttachment* Attachment, class AIONFirearm* Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.AttachAttachmentToFirearm");

	AIONCharacter_AttachAttachmentToFirearm_Params params;
	params.Attachment = Attachment;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ApplyDropsuitSkin
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONDropSkin*            NewDropsuitSkin                (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ApplyDropsuitSkin(class UIONDropSkin* NewDropsuitSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ApplyDropsuitSkin");

	AIONCharacter_ApplyDropsuitSkin_Params params;
	params.NewDropsuitSkin = NewDropsuitSkin;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacter.ApplyArmorSkin
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONArmorSkin*           NewArmorSkin                   (Parm, ZeroConstructor, IsPlainOldData)

void AIONCharacter::ApplyArmorSkin(class UIONArmorSkin* NewArmorSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacter.ApplyArmorSkin");

	AIONCharacter_ApplyArmorSkin_Params params;
	params.NewArmorSkin = NewArmorSkin;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeedCrouched
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONCharacterAnimInstance::GetMaxWalkSpeedCrouched()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeedCrouched");

	UIONCharacterAnimInstance_GetMaxWalkSpeedCrouched_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeed
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONCharacterAnimInstance::GetMaxWalkSpeed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeed");

	UIONCharacterAnimInstance_GetMaxWalkSpeed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacterAnimInstance.GetMaxSwimSpeed
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONCharacterAnimInstance::GetMaxSwimSpeed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterAnimInstance.GetMaxSwimSpeed");

	UIONCharacterAnimInstance_GetMaxSwimSpeed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacterMovementComponent.IsSprinting
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONCharacterMovementComponent::IsSprinting()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterMovementComponent.IsSprinting");

	UIONCharacterMovementComponent_IsSprinting_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacterMovementComponent.IsProne
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONCharacterMovementComponent::IsProne()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterMovementComponent.IsProne");

	UIONCharacterMovementComponent_IsProne_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCharacterMovementComponent.GetSprintRatio
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONCharacterMovementComponent::GetSprintRatio()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCharacterMovementComponent.GetSprintRatio");

	UIONCharacterMovementComponent_GetSprintRatio_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONConnectionOverlayUserWidget.GetOverlayOpacity
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONConnectionOverlayUserWidget::GetOverlayOpacity()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONConnectionOverlayUserWidget.GetOverlayOpacity");

	UIONConnectionOverlayUserWidget_GetOverlayOpacity_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCrateScreenWidget.ScrapItemForCredits
// (Final, Native, Protected, BlueprintCallable)

void UIONCrateScreenWidget::ScrapItemForCredits()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateScreenWidget.ScrapItemForCredits");

	UIONCrateScreenWidget_ScrapItemForCredits_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCrateScreenWidget.OpenCrate
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           CrateToOpen                    (Parm, ZeroConstructor, IsPlainOldData)
// class UIONSteamItem*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONSteamItem* UIONCrateScreenWidget::OpenCrate(class UIONSteamItem* CrateToOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateScreenWidget.OpenCrate");

	UIONCrateScreenWidget_OpenCrate_Params params;
	params.CrateToOpen = CrateToOpen;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCrateScreenWidget.GetCrateContent
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Crate                          (Parm, ZeroConstructor, IsPlainOldData)
// TArray<class UIONSteamItem*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONSteamItem*> UIONCrateScreenWidget::GetCrateContent(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateScreenWidget.GetCrateContent");

	UIONCrateScreenWidget_GetCrateContent_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCrateScreenWidget.BPEvent_OpenCrate
// (Event, Protected, BlueprintEvent)
// Parameters:
// class UIONSteamItem*           GainedItem                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONCrateScreenWidget::BPEvent_OpenCrate(class UIONSteamItem* GainedItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateScreenWidget.BPEvent_OpenCrate");

	UIONCrateScreenWidget_BPEvent_OpenCrate_Params params;
	params.GainedItem = GainedItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCrateScreenWidget.AddItemToInventory
// (Final, Native, Protected, BlueprintCallable)

void UIONCrateScreenWidget::AddItemToInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateScreenWidget.AddItemToInventory");

	UIONCrateScreenWidget_AddItemToInventory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCrateWidget.GetCrateContent
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// TArray<class UIONSteamItem*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONSteamItem*> UIONCrateWidget::GetCrateContent()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCrateWidget.GetCrateContent");

	UIONCrateWidget_GetCrateContent_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONCustomizationInventoryWidget.RefreshInventory
// (Final, Native, Public, BlueprintCallable)

void UIONCustomizationInventoryWidget::RefreshInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationInventoryWidget.RefreshInventory");

	UIONCustomizationInventoryWidget_RefreshInventory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationInventoryWidget.BPEvent_RefreshInventory
// (Event, Public, BlueprintEvent)

void UIONCustomizationInventoryWidget::BPEvent_RefreshInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationInventoryWidget.BPEvent_RefreshInventory");

	UIONCustomizationInventoryWidget_BPEvent_RefreshInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.SkinsDebug
// (Final, Exec, Native, Protected)
// Parameters:
// int                            Mode                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::SkinsDebug(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.SkinsDebug");

	AIONCustomizationPawn_SkinsDebug_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.SetupDisplayCharacter
// (Final, Native, Public, BlueprintCallable)

void AIONCustomizationPawn::SetupDisplayCharacter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.SetupDisplayCharacter");

	AIONCustomizationPawn_SetupDisplayCharacter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.SetArmorVisiblity
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bNewVisibility                 (Parm, ZeroConstructor, IsPlainOldData)
// class UClass*                  WearableToHide                 (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::SetArmorVisiblity(bool bNewVisibility, class UClass* WearableToHide)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.SetArmorVisiblity");

	AIONCustomizationPawn_SetArmorVisiblity_Params params;
	params.bNewVisibility = bNewVisibility;
	params.WearableToHide = WearableToHide;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.PreviewSkinOnChar
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           NewItem                        (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::PreviewSkinOnChar(class UIONSteamItem* NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.PreviewSkinOnChar");

	AIONCustomizationPawn_PreviewSkinOnChar_Params params;
	params.NewItem = NewItem;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.InspectWearable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  Wearable                       (Parm)

void AIONCustomizationPawn::InspectWearable(const struct FIONSteamInventoryItem& Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.InspectWearable");

	AIONCustomizationPawn_InspectWearable_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.InspectWeaponSkin
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Item                           (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::InspectWeaponSkin(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.InspectWeaponSkin");

	AIONCustomizationPawn_InspectWeaponSkin_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.InspectCrate
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FIONSteamInventoryItem  Crate                          (Parm)

void AIONCustomizationPawn::InspectCrate(const struct FIONSteamInventoryItem& Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.InspectCrate");

	AIONCustomizationPawn_InspectCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.DeleteInspectWeapon
// (Final, Native, Protected, BlueprintCallable)

void AIONCustomizationPawn::DeleteInspectWeapon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.DeleteInspectWeapon");

	AIONCustomizationPawn_DeleteInspectWeapon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_SkinsDebug
// (Event, Public, BlueprintEvent)
// Parameters:
// int                            Mode                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::BPEvent_SkinsDebug(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_SkinsDebug");

	AIONCustomizationPawn_BPEvent_SkinsDebug_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_PreviewWeaponSkin
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::BPEvent_PreviewWeaponSkin(class AIONWeapon* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_PreviewWeaponSkin");

	AIONCustomizationPawn_BPEvent_PreviewWeaponSkin_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_InspectWeaponSkin
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONWeaponSkin*          WeaponSkin                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::BPEvent_InspectWeaponSkin(class UIONWeaponSkin* WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_InspectWeaponSkin");

	AIONCustomizationPawn_BPEvent_InspectWeaponSkin_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_InspectDropsuit
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONDropSkin*            DropSkin                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::BPEvent_InspectDropsuit(class UIONDropSkin* DropSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_InspectDropsuit");

	AIONCustomizationPawn_BPEvent_InspectDropsuit_Params params;
	params.DropSkin = DropSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_InspectCrate
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Crate                          (Parm)

void AIONCustomizationPawn::BPEvent_InspectCrate(const struct FIONSteamInventoryItem& Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_InspectCrate");

	AIONCustomizationPawn_BPEvent_InspectCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_InspectArmor
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONArmorSkin*           ArmorSkin                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONCustomizationPawn::BPEvent_InspectArmor(class UIONArmorSkin* ArmorSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_InspectArmor");

	AIONCustomizationPawn_BPEvent_InspectArmor_Params params;
	params.ArmorSkin = ArmorSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.BPEvent_ApplyCurrentLoadout
// (Event, Public, BlueprintEvent)

void AIONCustomizationPawn::BPEvent_ApplyCurrentLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.BPEvent_ApplyCurrentLoadout");

	AIONCustomizationPawn_BPEvent_ApplyCurrentLoadout_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONCustomizationPawn.ApplyCurrentLoadout
// (Final, Native, Public, BlueprintCallable)

void AIONCustomizationPawn::ApplyCurrentLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONCustomizationPawn.ApplyCurrentLoadout");

	AIONCustomizationPawn_ApplyCurrentLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONDataSingleton.GetRankForScore
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            Score                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FRank                   ReturnValue                    (ConstParm, Parm, OutParm, ReturnParm)

struct FRank UIONDataSingleton::GetRankForScore(int Score)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDataSingleton.GetRankForScore");

	UIONDataSingleton_GetRankForScore_Params params;
	params.Score = Score;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDataSingleton.GetNextRankForScore
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            Score                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FRank                   ReturnValue                    (ConstParm, Parm, OutParm, ReturnParm)

struct FRank UIONDataSingleton::GetNextRankForScore(int Score)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDataSingleton.GetNextRankForScore");

	UIONDataSingleton_GetNextRankForScore_Params params;
	params.Score = Score;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDataSingleton.GetConfigForScoreEvent
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EScoreEvent                    ScoreEvent                     (Parm, ZeroConstructor, IsPlainOldData)
// struct FScoreEventConfig       ReturnValue                    (ConstParm, Parm, OutParm, ReturnParm)

struct FScoreEventConfig UIONDataSingleton::GetConfigForScoreEvent(EScoreEvent ScoreEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDataSingleton.GetConfigForScoreEvent");

	UIONDataSingleton_GetConfigForScoreEvent_Params params;
	params.ScoreEvent = ScoreEvent;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEvent.RemoveFromActiveEvents
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONEvent::RemoveFromActiveEvents()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEvent.RemoveFromActiveEvents");

	AIONEvent_RemoveFromActiveEvents_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEvent.GetEventManager
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class AIONEventManager*        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONEventManager* AIONEvent::GetEventManager()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEvent.GetEventManager");

	AIONEvent_GetEventManager_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDrone.SpawnItemFromLootTable
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// TArray<struct FLootTableEntry> LootTable                      (Parm, OutParm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONDrone::SpawnItemFromLootTable(TArray<struct FLootTableEntry>* LootTable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.SpawnItemFromLootTable");

	AIONDrone_SpawnItemFromLootTable_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (LootTable != nullptr)
		*LootTable = params.LootTable;

	return params.ReturnValue;
}


// Function IONBranch.IONDrone.OnRep_Health
// (Final, Native, Public)

void AIONDrone::OnRep_Health()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.OnRep_Health");

	AIONDrone_OnRep_Health_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONDrone.OnRep_Crashed
// (Final, Native, Public)

void AIONDrone::OnRep_Crashed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.OnRep_Crashed");

	AIONDrone_OnRep_Crashed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONDrone.IsDead
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONDrone::IsDead()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.IsDead");

	AIONDrone_IsDead_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDrone.GetMaxHealth
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONDrone::GetMaxHealth()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.GetMaxHealth");

	AIONDrone_GetMaxHealth_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDrone.FindNewTargetTime
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONDrone::FindNewTargetTime()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.FindNewTargetTime");

	AIONDrone_FindNewTargetTime_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDrone.FindNewTargetLocation
// (Final, Native, Public, HasDefaults, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector AIONDrone::FindNewTargetLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDrone.FindNewTargetLocation");

	AIONDrone_FindNewTargetLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEventSpawnBehavior.SpawnEvent
// (Native, Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONEvent*               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONEvent* UIONEventSpawnBehavior::SpawnEvent()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventSpawnBehavior.SpawnEvent");

	UIONEventSpawnBehavior_SpawnEvent_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEventSpawnBehavior.GetWorld
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UWorld*                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UWorld* UIONEventSpawnBehavior::GetWorld()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventSpawnBehavior.GetWorld");

	UIONEventSpawnBehavior_GetWorld_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEventSpawnBehavior.GetEventManager
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONEventManager*        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONEventManager* UIONEventSpawnBehavior::GetEventManager()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventSpawnBehavior.GetEventManager");

	UIONEventSpawnBehavior_GetEventManager_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEventSpawnBehavior.CanSpawn
// (Native, Event, Public, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONEventSpawnBehavior::CanSpawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventSpawnBehavior.CanSpawn");

	UIONEventSpawnBehavior_CanSpawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDroneSpawnBehavior.SpawnEvent
// (Native, Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONEvent*               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONEvent* UIONDroneSpawnBehavior::SpawnEvent()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDroneSpawnBehavior.SpawnEvent");

	UIONDroneSpawnBehavior_SpawnEvent_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONDroneSpawnBehavior.CanSpawn
// (Native, Event, Public, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONDroneSpawnBehavior::CanSpawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONDroneSpawnBehavior.CanSpawn");

	UIONDroneSpawnBehavior_CanSpawn_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONEventManager.Update
// (Final, Native, Public, BlueprintCallable)

void AIONEventManager::Update()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventManager.Update");

	AIONEventManager_Update_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONEventManager.GetActiveEventsOfClass
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UClass*                  Class                          (Parm, ZeroConstructor, IsPlainOldData)
// TArray<class AIONEvent*>       OutEvents                      (Parm, OutParm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONEventManager::GetActiveEventsOfClass(class UClass* Class, TArray<class AIONEvent*>* OutEvents)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONEventManager.GetActiveEventsOfClass");

	AIONEventManager_GetActiveEventsOfClass_Params params;
	params.Class = Class;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutEvents != nullptr)
		*OutEvents = params.OutEvents;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.UpdateWeaponSkinMeshes
// (Native, Public)
// Parameters:
// TArray<class UMeshComponent*>  MeshList                       (Parm, ZeroConstructor)

void AIONWeapon::UpdateWeaponSkinMeshes(TArray<class UMeshComponent*> MeshList)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.UpdateWeaponSkinMeshes");

	AIONWeapon_UpdateWeaponSkinMeshes_Params params;
	params.MeshList = MeshList;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.UpdateWeaponSkin
// (Native, Public)

void AIONWeapon::UpdateWeaponSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.UpdateWeaponSkin");

	AIONWeapon_UpdateWeaponSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.ServerStartTask
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// EWeaponTask                    TaskID                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::ServerStartTask(EWeaponTask TaskID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.ServerStartTask");

	AIONWeapon_ServerStartTask_Params params;
	params.TaskID = TaskID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.RestoreDefaultSkin
// (Native, Public, BlueprintCallable)

void AIONWeapon::RestoreDefaultSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.RestoreDefaultSkin");

	AIONWeapon_RestoreDefaultSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONWeapon.OnWeaponSkinChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// class UIONWeaponSkin*          WeaponSkin                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::OnWeaponSkinChanged__DelegateSignature(class UIONWeaponSkin* WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONWeapon.OnWeaponSkinChanged__DelegateSignature");

	AIONWeapon_OnWeaponSkinChanged__DelegateSignature_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.MulticastConfirmTask
// (Net, NetReliable, Native, Event, NetMulticast, Public)
// Parameters:
// EWeaponTask                    TaskID                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::MulticastConfirmTask(EWeaponTask TaskID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.MulticastConfirmTask");

	AIONWeapon_MulticastConfirmTask_Params params;
	params.TaskID = TaskID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.IsEquipped
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONWeapon::IsEquipped()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.IsEquipped");

	AIONWeapon_IsEquipped_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetViewedMesh
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class USkeletalMeshComponent*  ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class USkeletalMeshComponent* AIONWeapon::GetViewedMesh()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetViewedMesh");

	AIONWeapon_GetViewedMesh_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetPickupLocation
// (Native, Event, Protected, HasDefaults, BlueprintEvent, Const)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector AIONWeapon::GetPickupLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetPickupLocation");

	AIONWeapon_GetPickupLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetLeftHandSocketLocation
// (Final, Native, Public, HasDefaults, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FVector                 ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector AIONWeapon::GetLeftHandSocketLocation()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetLeftHandSocketLocation");

	AIONWeapon_GetLeftHandSocketLocation_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetFPMesh
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class USkeletalMeshComponent*  ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class USkeletalMeshComponent* AIONWeapon::GetFPMesh()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetFPMesh");

	AIONWeapon_GetFPMesh_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetCurrentWeaponSkin
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UIONWeaponSkin*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONWeaponSkin* AIONWeapon::GetCurrentWeaponSkin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetCurrentWeaponSkin");

	AIONWeapon_GetCurrentWeaponSkin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.GetAlternateIdle
// (Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONWeapon::GetAlternateIdle()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.GetAlternateIdle");

	AIONWeapon_GetAlternateIdle_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.Get3PMesh
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class USkeletalMeshComponent*  ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class USkeletalMeshComponent* AIONWeapon::Get3PMesh()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.Get3PMesh");

	AIONWeapon_Get3PMesh_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWeapon.ClientStartTask
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// EWeaponTask                    TaskID                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::ClientStartTask(EWeaponTask TaskID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.ClientStartTask");

	AIONWeapon_ClientStartTask_Params params;
	params.TaskID = TaskID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.ClientConfirmTask
// (Net, NetReliable, Native, Event, Public, NetClient)
// Parameters:
// EWeaponTask                    TaskID                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::ClientConfirmTask(EWeaponTask TaskID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.ClientConfirmTask");

	AIONWeapon_ClientConfirmTask_Params params;
	params.TaskID = TaskID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.BPEvent_OnPickedUp
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::BPEvent_OnPickedUp(class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.BPEvent_OnPickedUp");

	AIONWeapon_BPEvent_OnPickedUp_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.BPEvent_ApplyWeaponSkinMaterial
// (Event, Public, BlueprintEvent)

void AIONWeapon::BPEvent_ApplyWeaponSkinMaterial()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.BPEvent_ApplyWeaponSkinMaterial");

	AIONWeapon_BPEvent_ApplyWeaponSkinMaterial_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWeapon.ApplyWeaponSkin
// (Native, Public, BlueprintCallable)
// Parameters:
// class UIONWeaponSkin*          NewWeaponSkin                  (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bForce                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWeapon::ApplyWeaponSkin(class UIONWeaponSkin* NewWeaponSkin, bool bForce)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWeapon.ApplyWeaponSkin");

	AIONWeapon_ApplyWeaponSkin_Params params;
	params.NewWeaponSkin = NewWeaponSkin;
	params.bForce = bForce;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.ShouldUseSightAdapter
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONFirearm::ShouldUseSightAdapter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.ShouldUseSightAdapter");

	AIONFirearm_ShouldUseSightAdapter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.ServerNotifyHit
// (Final, Net, NetReliable, Native, Event, Private, NetServer, HasDefaults, NetValidate)
// Parameters:
// struct FHitResult              Hit                            (ConstParm, Parm, IsPlainOldData)
// struct FVector                 WorldLocation                  (ConstParm, Parm, IsPlainOldData)

void AIONFirearm::ServerNotifyHit(const struct FHitResult& Hit, const struct FVector& WorldLocation)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.ServerNotifyHit");

	AIONFirearm_ServerNotifyHit_Params params;
	params.Hit = Hit;
	params.WorldLocation = WorldLocation;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.OnRep_UnderBarrelRail
// (Final, Native, Private)
// Parameters:
// class AIONAttachment*          OldAttachment                  (Parm, ZeroConstructor, IsPlainOldData)

void AIONFirearm::OnRep_UnderBarrelRail(class AIONAttachment* OldAttachment)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.OnRep_UnderBarrelRail");

	AIONFirearm_OnRep_UnderBarrelRail_Params params;
	params.OldAttachment = OldAttachment;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.OnRep_Sight
// (Final, Native, Private)
// Parameters:
// class AIONSight*               OldSight                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONFirearm::OnRep_Sight(class AIONSight* OldSight)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.OnRep_Sight");

	AIONFirearm_OnRep_Sight_Params params;
	params.OldSight = OldSight;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.OnRep_Siderail
// (Final, Native, Private)
// Parameters:
// class AIONAttachment*          OldAttachment                  (Parm, ZeroConstructor, IsPlainOldData)

void AIONFirearm::OnRep_Siderail(class AIONAttachment* OldAttachment)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.OnRep_Siderail");

	AIONFirearm_OnRep_Siderail_Params params;
	params.OldAttachment = OldAttachment;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.OnRep_ExtendedMagazine
// (Final, Native, Private)
// Parameters:
// class AIONExtendedMagazine*    OldMag                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONFirearm::OnRep_ExtendedMagazine(class AIONExtendedMagazine* OldMag)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.OnRep_ExtendedMagazine");

	AIONFirearm_OnRep_ExtendedMagazine_Params params;
	params.OldMag = OldMag;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.OnRep_Barrel
// (Final, Native, Private)
// Parameters:
// class AIONBarrel*              OldBarrel                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONFirearm::OnRep_Barrel(class AIONBarrel* OldBarrel)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.OnRep_Barrel");

	AIONFirearm_OnRep_Barrel_Params params;
	params.OldBarrel = OldBarrel;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.MulticastImpactNotify
// (Final, Net, NetReliable, Native, Event, NetMulticast, Private, HasDefaults, NetValidate)
// Parameters:
// struct FHitResult              Hit                            (ConstParm, Parm, IsPlainOldData)
// struct FVector                 WorldLocation                  (ConstParm, Parm, IsPlainOldData)

void AIONFirearm::MulticastImpactNotify(const struct FHitResult& Hit, const struct FVector& WorldLocation)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.MulticastImpactNotify");

	AIONFirearm_MulticastImpactNotify_Params params;
	params.Hit = Hit;
	params.WorldLocation = WorldLocation;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONFirearm.HasAttachmentSlot
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONFirearm::HasAttachmentSlot(TEnumAsByte<EAttachmentSlot> Slot)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.HasAttachmentSlot");

	AIONFirearm_HasAttachmentSlot_Params params;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetRoundIcon
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* AIONFirearm::GetRoundIcon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetRoundIcon");

	AIONFirearm_GetRoundIcon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetRoundChambered
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONFirearm::GetRoundChambered()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetRoundChambered");

	AIONFirearm_GetRoundChambered_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetReloadTime
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetReloadTime()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetReloadTime");

	AIONFirearm_GetReloadTime_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetRangeStat
// (Final, Native, Public, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetRangeStat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetRangeStat");

	AIONFirearm_GetRangeStat_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetMagazineCapacity
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONFirearm::GetMagazineCapacity()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetMagazineCapacity");

	AIONFirearm_GetMagazineCapacity_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetMagazine
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONFirearm::GetMagazine()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetMagazine");

	AIONFirearm_GetMagazine_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetFireRateStat
// (Final, Native, Public, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetFireRateStat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetFireRateStat");

	AIONFirearm_GetFireRateStat_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetDamageStat
// (Final, Native, Public, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetDamageStat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetDamageStat");

	AIONFirearm_GetDamageStat_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAttachmentSocketName
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

struct FName AIONFirearm::GetAttachmentSocketName(TEnumAsByte<EAttachmentSlot> Slot)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAttachmentSocketName");

	AIONFirearm_GetAttachmentSocketName_Params params;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAttachments
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TArray<class AIONAttachment*>  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class AIONAttachment*> AIONFirearm::GetAttachments()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAttachments");

	AIONFirearm_GetAttachments_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAttachmentComponent
// (Final, Native, Public, HasOutParms, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// class UStaticMeshComponent*    OutMesh                        (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UStaticMeshComponent*    OutMeshFP                      (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)

void AIONFirearm::GetAttachmentComponent(TEnumAsByte<EAttachmentSlot> Slot, class UStaticMeshComponent** OutMesh, class UStaticMeshComponent** OutMeshFP)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAttachmentComponent");

	AIONFirearm_GetAttachmentComponent_Params params;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutMesh != nullptr)
		*OutMesh = params.OutMesh;
	if (OutMeshFP != nullptr)
		*OutMeshFP = params.OutMeshFP;
}


// Function IONBranch.IONFirearm.GetAttachment
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TEnumAsByte<EAttachmentSlot>   Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// class AIONAttachment*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONAttachment* AIONFirearm::GetAttachment(TEnumAsByte<EAttachmentSlot> Slot)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAttachment");

	AIONFirearm_GetAttachment_Params params;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAmmoTypeText
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText AIONFirearm::GetAmmoTypeText()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAmmoTypeText");

	AIONFirearm_GetAmmoTypeText_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAmmoBoxIcon
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* AIONFirearm::GetAmmoBoxIcon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAmmoBoxIcon");

	AIONFirearm_GetAmmoBoxIcon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAmmoBoxClassPath
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString AIONFirearm::GetAmmoBoxClassPath()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAmmoBoxClassPath");

	AIONFirearm_GetAmmoBoxClassPath_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAmmoBoxClass
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UClass*                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UClass* AIONFirearm::GetAmmoBoxClass()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAmmoBoxClass");

	AIONFirearm_GetAmmoBoxClass_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAccuracyStat
// (Final, Native, Public, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetAccuracyStat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAccuracyStat");

	AIONFirearm_GetAccuracyStat_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFirearm.GetAccuracy
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONFirearm::GetAccuracy()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFirearm.GetAccuracy");

	AIONFirearm_GetAccuracy_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONFriendListUserWidget.OnUpdateFriendList
// (Event, Public, BlueprintEvent)

void UIONFriendListUserWidget::OnUpdateFriendList()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONFriendListUserWidget.OnUpdateFriendList");

	UIONFriendListUserWidget_OnUpdateFriendList_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.UpdateSteamInventoryItems
// (Final, Native, Public, BlueprintCallable)

void UIONGameInstance::UpdateSteamInventoryItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.UpdateSteamInventoryItems");

	UIONGameInstance_UpdateSteamInventoryItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.UpdatePlayerStats
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)
// class AIONPlayerState*         Winner                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameInstance::UpdatePlayerStats(class AIONPlayerState* PlayerState, class AIONPlayerState* Winner)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.UpdatePlayerStats");

	UIONGameInstance_UpdatePlayerStats_Params params;
	params.PlayerState = PlayerState;
	params.Winner = Winner;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.UpdateLoadingScreen
// (Final, Native, Public)
// Parameters:
// struct FString                 NewMessage                     (Parm, ZeroConstructor)

void UIONGameInstance::UpdateLoadingScreen(const struct FString& NewMessage)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.UpdateLoadingScreen");

	UIONGameInstance_UpdateLoadingScreen_Params params;
	params.NewMessage = NewMessage;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.OnLevelActorRemoved
// (Final, Native, Private)
// Parameters:
// class AActor*                  InActor                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameInstance::OnLevelActorRemoved(class AActor* InActor)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.OnLevelActorRemoved");

	UIONGameInstance_OnLevelActorRemoved_Params params;
	params.InActor = InActor;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.OnGameSparksAvailable
// (Final, Native, Public)
// Parameters:
// bool                           bAvailable                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameInstance::OnGameSparksAvailable(bool bAvailable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.OnGameSparksAvailable");

	UIONGameInstance_OnGameSparksAvailable_Params params;
	params.bAvailable = bAvailable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.GetMatchmaking
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONMatchmaking*         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONMatchmaking* UIONGameInstance::GetMatchmaking()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.GetMatchmaking");

	UIONGameInstance_GetMatchmaking_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameInstance.GetItemBySteamDefID
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            ID                             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class UIONSteamItem*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONSteamItem* UIONGameInstance::GetItemBySteamDefID(int ID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.GetItemBySteamDefID");

	UIONGameInstance_GetItemBySteamDefID_Params params;
	params.ID = ID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameInstance.EndLoadingScreen
// (Final, Native, Public, BlueprintCallable)

void UIONGameInstance::EndLoadingScreen()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.EndLoadingScreen");

	UIONGameInstance_EndLoadingScreen_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.DownloadPlayerStats
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameInstance::DownloadPlayerStats(class AIONPlayerState* PlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.DownloadPlayerStats");

	UIONGameInstance_DownloadPlayerStats_Params params;
	params.PlayerState = PlayerState;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameInstance.BeginLoadingScreen
// (Final, Native, Public)
// Parameters:
// struct FString                 MapName                        (Parm, ZeroConstructor)

void UIONGameInstance::BeginLoadingScreen(const struct FString& MapName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameInstance.BeginLoadingScreen");

	UIONGameInstance_BeginLoadingScreen_Params params;
	params.MapName = MapName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.UnbanPlayer
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerId                       (Parm, ZeroConstructor)

void AIONGameMode::UnbanPlayer(const struct FString& PlayerId)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.UnbanPlayer");

	AIONGameMode_UnbanPlayer_Params params;
	params.PlayerId = PlayerId;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.ToggleQueueTimer
// (Final, Exec, Native, Public)

void AIONGameMode::ToggleQueueTimer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.ToggleQueueTimer");

	AIONGameMode_ToggleQueueTimer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.TogglePlasmaField
// (Final, Exec, Native, Public)

void AIONGameMode::TogglePlasmaField()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.TogglePlasmaField");

	AIONGameMode_TogglePlasmaField_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.StartBattleRoyale
// (Final, Exec, Native, Public)

void AIONGameMode::StartBattleRoyale()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.StartBattleRoyale");

	AIONGameMode_StartBattleRoyale_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.SpawnBeamDrop
// (Final, Exec, Native, Public)

void AIONGameMode::SpawnBeamDrop()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.SpawnBeamDrop");

	AIONGameMode_SpawnBeamDrop_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.SetMaxPlayers
// (Final, Exec, Native, Public)
// Parameters:
// int                            MaxPlayers                     (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameMode::SetMaxPlayers(int MaxPlayers)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.SetMaxPlayers");

	AIONGameMode_SetMaxPlayers_Params params;
	params.MaxPlayers = MaxPlayers;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.SetDamageFactor
// (Final, Exec, Native, Public)
// Parameters:
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameMode::SetDamageFactor(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.SetDamageFactor");

	AIONGameMode_SetDamageFactor_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.SendStatsToCameramen
// (Final, Native, Public)

void AIONGameMode::SendStatsToCameramen()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.SendStatsToCameramen");

	AIONGameMode_SendStatsToCameramen_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.RestartMatch
// (Final, Exec, Native, Public)

void AIONGameMode::RestartMatch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.RestartMatch");

	AIONGameMode_RestartMatch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.RemoveAdmin
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerId                       (Parm, ZeroConstructor)

void AIONGameMode::RemoveAdmin(const struct FString& PlayerId)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.RemoveAdmin");

	AIONGameMode_RemoveAdmin_Params params;
	params.PlayerId = PlayerId;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.KillPlayer
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerName                     (Parm, ZeroConstructor)

void AIONGameMode::KillPlayer(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.KillPlayer");

	AIONGameMode_KillPlayer_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.KickPlayer
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerName                     (Parm, ZeroConstructor)

void AIONGameMode::KickPlayer(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.KickPlayer");

	AIONGameMode_KickPlayer_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.HasBattleRoyaleStarted
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONGameMode::HasBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.HasBattleRoyaleStarted");

	AIONGameMode_HasBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameMode.GivePoints
// (Final, Exec, Native, Public)
// Parameters:
// int                            Num                            (Parm, ZeroConstructor, IsPlainOldData)

void AIONGameMode::GivePoints(int Num)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.GivePoints");

	AIONGameMode_GivePoints_Params params;
	params.Num = Num;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.ForceSkinDrops
// (Final, Exec, Native, Public)

void AIONGameMode::ForceSkinDrops()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.ForceSkinDrops");

	AIONGameMode_ForceSkinDrops_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.ForceRestartMatch
// (Final, Exec, Native, Public)

void AIONGameMode::ForceRestartMatch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.ForceRestartMatch");

	AIONGameMode_ForceRestartMatch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.BanPlayer
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerName                     (Parm, ZeroConstructor)

void AIONGameMode::BanPlayer(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.BanPlayer");

	AIONGameMode_BanPlayer_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameMode.AddAdmin
// (Final, Exec, Native, Public)
// Parameters:
// struct FString                 PlayerId                       (Parm, ZeroConstructor)

void AIONGameMode::AddAdmin(const struct FString& PlayerId)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameMode.AddAdmin");

	AIONGameMode_AddAdmin_Params params;
	params.PlayerId = PlayerId;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.StartMessage
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FName                   Type                           (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UIONGameOverlayWidget::StartMessage(const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.StartMessage");

	UIONGameOverlayWidget_StartMessage_Params params;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.SetMapTooltipVisibility
// (Event, Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bInVisible                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameOverlayWidget::SetMapTooltipVisibility(bool bInVisible)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.SetMapTooltipVisibility");

	UIONGameOverlayWidget_SetMapTooltipVisibility_Params params;
	params.bInVisible = bInVisible;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.SetChatVisibility
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bInVisible                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameOverlayWidget::SetChatVisibility(bool bInVisible)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.SetChatVisibility");

	UIONGameOverlayWidget_SetChatVisibility_Params params;
	params.bInVisible = bInVisible;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnScoreEventReceived
// (Event, Public, BlueprintEvent)
// Parameters:
// EScoreEvent                    Type                           (Parm, ZeroConstructor, IsPlainOldData)
// int                            Points                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameOverlayWidget::OnScoreEventReceived(EScoreEvent Type, int Points)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnScoreEventReceived");

	UIONGameOverlayWidget_OnScoreEventReceived_Params params;
	params.Type = Type;
	params.Points = Points;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnReceivedChatMessage
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         From                           (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Message                        (Parm, ZeroConstructor)
// struct FName                   Type                           (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UIONGameOverlayWidget::OnReceivedChatMessage(class AIONPlayerState* From, const struct FString& Message, const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnReceivedChatMessage");

	UIONGameOverlayWidget_OnReceivedChatMessage_Params params;
	params.From = From;
	params.Message = Message;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnPlayerDowned
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void UIONGameOverlayWidget::OnPlayerDowned(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnPlayerDowned");

	UIONGameOverlayWidget_OnPlayerDowned_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnPlayerDied
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void UIONGameOverlayWidget::OnPlayerDied(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnPlayerDied");

	UIONGameOverlayWidget_OnPlayerDied_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnMicrophoneStateChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bEnabled                       (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameOverlayWidget::OnMicrophoneStateChanged(bool bEnabled)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnMicrophoneStateChanged");

	UIONGameOverlayWidget_OnMicrophoneStateChanged_Params params;
	params.bEnabled = bEnabled;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnKillConfirmed
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void UIONGameOverlayWidget::OnKillConfirmed(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnKillConfirmed");

	UIONGameOverlayWidget_OnKillConfirmed_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.OnDownConfirmed
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (Parm)

void UIONGameOverlayWidget::OnDownConfirmed(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.OnDownConfirmed");

	UIONGameOverlayWidget_OnDownConfirmed_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameOverlayWidget.DisplaySubtitle
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   SubtitleText                   (ConstParm, Parm, OutParm, ReferenceParm)

void UIONGameOverlayWidget::DisplaySubtitle(const struct FText& SubtitleText)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameOverlayWidget.DisplaySubtitle");

	UIONGameOverlayWidget_DisplaySubtitle_Params params;
	params.SubtitleText = SubtitleText;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.GSGetLatestMatchRequest.GetLatestMatch
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UGSGetLatestMatchRequest* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetLatestMatchRequest* UGSGetLatestMatchRequest::STATIC_GetLatestMatch()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetLatestMatchRequest.GetLatestMatch");

	UGSGetLatestMatchRequest_GetLatestMatch_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.GSGetGameConfigRequest.GetGameConfig
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UGSGetGameConfigRequest* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetGameConfigRequest* UGSGetGameConfigRequest::STATIC_GetGameConfig()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetGameConfigRequest.GetGameConfig");

	UGSGetGameConfigRequest_GetGameConfig_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.GSGetPlayerProfileRequest.GetPlayerProfile
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 PlayerId                       (Parm, ZeroConstructor)
// class UGSGetPlayerProfileRequest* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetPlayerProfileRequest* UGSGetPlayerProfileRequest::STATIC_GetPlayerProfile(const struct FString& PlayerId)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetPlayerProfileRequest.GetPlayerProfile");

	UGSGetPlayerProfileRequest_GetPlayerProfile_Params params;
	params.PlayerId = PlayerId;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.GSGetPlayerStatsRequest.GetPlayerStats
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 PlayerId                       (Parm, ZeroConstructor)
// struct FString                 GameMode                       (Parm, ZeroConstructor)
// class UGSGetPlayerStatsRequest* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetPlayerStatsRequest* UGSGetPlayerStatsRequest::STATIC_GetPlayerStats(const struct FString& PlayerId, const struct FString& GameMode)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetPlayerStatsRequest.GetPlayerStats");

	UGSGetPlayerStatsRequest_GetPlayerStats_Params params;
	params.PlayerId = PlayerId;
	params.GameMode = GameMode;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.GSGetToasterRequest.GetToaster
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UGSGetToasterRequest*    ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetToasterRequest* UGSGetToasterRequest::STATIC_GetToaster()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetToasterRequest.GetToaster");

	UGSGetToasterRequest_GetToaster_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.GSGetMenuNewsRequest.GetMenuNews
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UGSGetMenuNewsRequest*   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UGSGetMenuNewsRequest* UGSGetMenuNewsRequest::STATIC_GetMenuNews()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.GSGetMenuNewsRequest.GetMenuNews");

	UGSGetMenuNewsRequest_GetMenuNews_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameService.DebugGetRandomMatchHistory
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FPlayerMatchHistory     ReturnValue                    (Parm, OutParm, ReturnParm)

struct FPlayerMatchHistory UIONGameService::STATIC_DebugGetRandomMatchHistory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameService.DebugGetRandomMatchHistory");

	UIONGameService_DebugGetRandomMatchHistory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameUserSettings.SetStringConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Value                          (Parm, ZeroConstructor)

void UIONGameUserSettings::SetStringConfigVariable(const struct FName& VariableName, const struct FString& Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.SetStringConfigVariable");

	UIONGameUserSettings_SetStringConfigVariable_Params params;
	params.VariableName = VariableName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameUserSettings.SetIntConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// int                            Value                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameUserSettings::SetIntConfigVariable(const struct FName& VariableName, int Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.SetIntConfigVariable");

	UIONGameUserSettings_SetIntConfigVariable_Params params;
	params.VariableName = VariableName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameUserSettings.SetFloatConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONGameUserSettings::SetFloatConfigVariable(const struct FName& VariableName, float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.SetFloatConfigVariable");

	UIONGameUserSettings_SetFloatConfigVariable_Params params;
	params.VariableName = VariableName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONGameUserSettings.GetStringConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONGameUserSettings::GetStringConfigVariable(const struct FName& VariableName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.GetStringConfigVariable");

	UIONGameUserSettings_GetStringConfigVariable_Params params;
	params.VariableName = VariableName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameUserSettings.GetIntConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONGameUserSettings::GetIntConfigVariable(const struct FName& VariableName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.GetIntConfigVariable");

	UIONGameUserSettings_GetIntConfigVariable_Params params;
	params.VariableName = VariableName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONGameUserSettings.GetFloatConfigVariable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   VariableName                   (Parm, ZeroConstructor, IsPlainOldData)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONGameUserSettings::GetFloatConfigVariable(const struct FName& VariableName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONGameUserSettings.GetFloatConfigVariable");

	UIONGameUserSettings_GetFloatConfigVariable_Params params;
	params.VariableName = VariableName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONOnlineUserWidget.Connected
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FString                 UserId                         (Parm, ZeroConstructor)
// struct FString                 AuthToken                      (Parm, ZeroConstructor)

void UIONOnlineUserWidget::Connected(const struct FString& UserId, const struct FString& AuthToken)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONOnlineUserWidget.Connected");

	UIONOnlineUserWidget_Connected_Params params;
	params.UserId = UserId;
	params.AuthToken = AuthToken;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONHomescreenUserWidget.HighlightsButtonPressed
// (Final, Native, Private)

void UIONHomescreenUserWidget::HighlightsButtonPressed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONHomescreenUserWidget.HighlightsButtonPressed");

	UIONHomescreenUserWidget_HighlightsButtonPressed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONHomescreenUserWidget.CheckIfHighlightsShouldBeVisible
// (Final, Native, Private, BlueprintCallable)

void UIONHomescreenUserWidget::CheckIfHighlightsShouldBeVisible()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONHomescreenUserWidget.CheckIfHighlightsShouldBeVisible");

	UIONHomescreenUserWidget_CheckIfHighlightsShouldBeVisible_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInputFunctionLibrary.SetInvertAxis
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FName                   AxisName                       (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// bool                           bInvert                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONInputFunctionLibrary::STATIC_SetInvertAxis(const struct FName& AxisName, bool bInvert)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.SetInvertAxis");

	UIONInputFunctionLibrary_SetInvertAxis_Params params;
	params.AxisName = AxisName;
	params.bInvert = bInvert;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInputFunctionLibrary.SaveMappings
// (Final, Native, Static, Public, BlueprintCallable)

void UIONInputFunctionLibrary::STATIC_SaveMappings()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.SaveMappings");

	UIONInputFunctionLibrary_SaveMappings_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInputFunctionLibrary.RemoveMappings
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FName                   Interaction                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FKey                    Key                            (ConstParm, Parm)
// bool                           bSave                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONInputFunctionLibrary::STATIC_RemoveMappings(const struct FName& Interaction, const struct FKey& Key, bool bSave)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.RemoveMappings");

	UIONInputFunctionLibrary_RemoveMappings_Params params;
	params.Interaction = Interaction;
	params.Key = Key;
	params.bSave = bSave;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInputFunctionLibrary.RebindKey
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FInputMapping           InputMapping                   (ConstParm, Parm, OutParm, ReferenceParm)
// struct FKey                    Key                            (ConstParm, Parm, OutParm, ReferenceParm)
// EInputKeyType                  KeyType                        (Parm, ZeroConstructor, IsPlainOldData)
// TArray<struct FName>           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FName> UIONInputFunctionLibrary::STATIC_RebindKey(const struct FInputMapping& InputMapping, const struct FKey& Key, EInputKeyType KeyType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.RebindKey");

	UIONInputFunctionLibrary_RebindKey_Params params;
	params.InputMapping = InputMapping;
	params.Key = Key;
	params.KeyType = KeyType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.IsKeyEventActionPressed
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FKeyEvent               KeyEvent                       (ConstParm, Parm, OutParm, ReferenceParm)
// struct FName                   ActionName                     (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInputFunctionLibrary::STATIC_IsKeyEventActionPressed(const struct FKeyEvent& KeyEvent, const struct FName& ActionName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.IsKeyEventActionPressed");

	UIONInputFunctionLibrary_IsKeyEventActionPressed_Params params;
	params.KeyEvent = KeyEvent;
	params.ActionName = ActionName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.IsInputMappingInConflict
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FInputMapping           InputMapping                   (ConstParm, Parm, OutParm, ReferenceParm)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInputFunctionLibrary::STATIC_IsInputMappingInConflict(const struct FInputMapping& InputMapping)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.IsInputMappingInConflict");

	UIONInputFunctionLibrary_IsInputMappingInConflict_Params params;
	params.InputMapping = InputMapping;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.GetInvertAxis
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FName                   AxisName                       (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInputFunctionLibrary::STATIC_GetInvertAxis(const struct FName& AxisName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.GetInvertAxis");

	UIONInputFunctionLibrary_GetInvertAxis_Params params;
	params.AxisName = AxisName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.GetInputMappings
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// TArray<struct FInputCategory>  OutCategories                  (Parm, OutParm, ZeroConstructor)

void UIONInputFunctionLibrary::STATIC_GetInputMappings(TArray<struct FInputCategory>* OutCategories)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.GetInputMappings");

	UIONInputFunctionLibrary_GetInputMappings_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutCategories != nullptr)
		*OutCategories = params.OutCategories;
}


// Function IONBranch.IONInputFunctionLibrary.GetInputMappingDisplayName
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FInputMapping           InputMapping                   (ConstParm, Parm, OutParm, ReferenceParm)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UIONInputFunctionLibrary::STATIC_GetInputMappingDisplayName(const struct FInputMapping& InputMapping)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.GetInputMappingDisplayName");

	UIONInputFunctionLibrary_GetInputMappingDisplayName_Params params;
	params.InputMapping = InputMapping;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.GetAxisMapping
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FName                   AxisName                       (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// struct FName                   ActionName                     (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// bool                           bNegativeAxis                  (Parm, ZeroConstructor, IsPlainOldData)
// struct FInputMapping           ReturnValue                    (Parm, OutParm, ReturnParm)

struct FInputMapping UIONInputFunctionLibrary::STATIC_GetAxisMapping(const struct FName& AxisName, const struct FName& ActionName, bool bNegativeAxis)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.GetAxisMapping");

	UIONInputFunctionLibrary_GetAxisMapping_Params params;
	params.AxisName = AxisName;
	params.ActionName = ActionName;
	params.bNegativeAxis = bNegativeAxis;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.GetActionMapping
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FName                   MappingName                    (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// struct FName                   ActionName                     (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// struct FInputMapping           ReturnValue                    (Parm, OutParm, ReturnParm)

struct FInputMapping UIONInputFunctionLibrary::STATIC_GetActionMapping(const struct FName& MappingName, const struct FName& ActionName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.GetActionMapping");

	UIONInputFunctionLibrary_GetActionMapping_Params params;
	params.MappingName = MappingName;
	params.ActionName = ActionName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInputFunctionLibrary.ClearKeyBinding
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FInputMapping           InputMapping                   (ConstParm, Parm, OutParm, ReferenceParm)
// EInputKeyType                  KeyType                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONInputFunctionLibrary::STATIC_ClearKeyBinding(const struct FInputMapping& InputMapping, EInputKeyType KeyType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.ClearKeyBinding");

	UIONInputFunctionLibrary_ClearKeyBinding_Params params;
	params.InputMapping = InputMapping;
	params.KeyType = KeyType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInputFunctionLibrary.AddMapping
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FName                   Interaction                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           bAxis                          (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bNegative                      (Parm, ZeroConstructor, IsPlainOldData)
// struct FKey                    Key                            (Parm)
// bool                           bSave                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONInputFunctionLibrary::STATIC_AddMapping(const struct FName& Interaction, bool bAxis, bool bNegative, const struct FKey& Key, bool bSave)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInputFunctionLibrary.AddMapping");

	UIONInputFunctionLibrary_AddMapping_Params params;
	params.Interaction = Interaction;
	params.bAxis = bAxis;
	params.bNegative = bNegative;
	params.Key = Key;
	params.bSave = bSave;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.ServerMoveItemToSlot
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// class AIONItem*                NewItem                        (Parm, ZeroConstructor, IsPlainOldData)
// int                            Slot                           (Parm, ZeroConstructor, IsPlainOldData)
// class AItemSpawn*              ItemSpawn                      (Parm, ZeroConstructor, IsPlainOldData)
// unsigned char                  ItemIndex                      (Parm, ZeroConstructor, IsPlainOldData)

void UIONInventoryComponent::ServerMoveItemToSlot(class AIONItem* NewItem, int Slot, class AItemSpawn* ItemSpawn, unsigned char ItemIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.ServerMoveItemToSlot");

	UIONInventoryComponent_ServerMoveItemToSlot_Params params;
	params.NewItem = NewItem;
	params.Slot = Slot;
	params.ItemSpawn = ItemSpawn;
	params.ItemIndex = ItemIndex;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.RemoveItem
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                ItemToRemove                   (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bCallOnRep                     (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryComponent::RemoveItem(class AIONItem* ItemToRemove, bool bCallOnRep)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.RemoveItem");

	UIONInventoryComponent_RemoveItem_Params params;
	params.ItemToRemove = ItemToRemove;
	params.bCallOnRep = bCallOnRep;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.RemoveAmmo
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EFirearmType                   AmmoType                       (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// int                            Ammount                        (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryComponent::RemoveAmmo(EFirearmType AmmoType, int Ammount)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.RemoveAmmo");

	UIONInventoryComponent_RemoveAmmo_Params params;
	params.AmmoType = AmmoType;
	params.Ammount = Ammount;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnWearablesChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnWearablesChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnWearablesChanged__DelegateSignature");

	UIONInventoryComponent_OnWearablesChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Wearables
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Wearables()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Wearables");

	UIONInventoryComponent_OnRep_Wearables_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Knife
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Knife()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Knife");

	UIONInventoryComponent_OnRep_Knife_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Grenades
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Grenades()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Grenades");

	UIONInventoryComponent_OnRep_Grenades_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Firearms
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Firearms()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Firearms");

	UIONInventoryComponent_OnRep_Firearms_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Boosters
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Boosters()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Boosters");

	UIONInventoryComponent_OnRep_Boosters_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Attachments
// (Final, Native, Protected)

void UIONInventoryComponent::OnRep_Attachments()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Attachments");

	UIONInventoryComponent_OnRep_Attachments_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.OnRep_Ammo
// (Final, Native, Protected)
// Parameters:
// TArray<int>                    OldAmmo                        (Parm, ZeroConstructor)

void UIONInventoryComponent::OnRep_Ammo(TArray<int> OldAmmo)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.OnRep_Ammo");

	UIONInventoryComponent_OnRep_Ammo_Params params;
	params.OldAmmo = OldAmmo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnInventoryChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnInventoryChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnInventoryChanged__DelegateSignature");

	UIONInventoryComponent_OnInventoryChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnGrenadesChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnGrenadesChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnGrenadesChanged__DelegateSignature");

	UIONInventoryComponent_OnGrenadesChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnFirearmsChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnFirearmsChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnFirearmsChanged__DelegateSignature");

	UIONInventoryComponent_OnFirearmsChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnBoostersChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnBoostersChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnBoostersChanged__DelegateSignature");

	UIONInventoryComponent_OnBoostersChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnAttachmentsChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnAttachmentsChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnAttachmentsChanged__DelegateSignature");

	UIONInventoryComponent_OnAttachmentsChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONInventoryComponent.OnAmmoChanged__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONInventoryComponent::OnAmmoChanged__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONInventoryComponent.OnAmmoChanged__DelegateSignature");

	UIONInventoryComponent_OnAmmoChanged__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.MoveItemToSlot
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                NewItem                        (Parm, ZeroConstructor, IsPlainOldData)
// int                            Slot                           (Parm, ZeroConstructor, IsPlainOldData)

void UIONInventoryComponent::MoveItemToSlot(class AIONItem* NewItem, int Slot)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.MoveItemToSlot");

	UIONInventoryComponent_MoveItemToSlot_Params params;
	params.NewItem = NewItem;
	params.Slot = Slot;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryComponent.HasItemOfClass
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UClass*                  ItemClass                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryComponent::HasItemOfClass(class UClass* ItemClass)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.HasItemOfClass");

	UIONInventoryComponent_HasItemOfClass_Params params;
	params.ItemClass = ItemClass;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetKnife
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONWeapon*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONWeapon* UIONInventoryComponent::GetKnife()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetKnife");

	UIONInventoryComponent_GetKnife_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetInventoryAttributes
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FInventoryAttributes    ReturnValue                    (Parm, OutParm, ReturnParm)

struct FInventoryAttributes UIONInventoryComponent::GetInventoryAttributes()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetInventoryAttributes");

	UIONInventoryComponent_GetInventoryAttributes_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetIndexForNewItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// EInventoryType                 ArrayType                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class AIONItem*                Item                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONInventoryComponent::GetIndexForNewItem(EInventoryType ArrayType, class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetIndexForNewItem");

	UIONInventoryComponent_GetIndexForNewItem_Params params;
	params.ArrayType = ArrayType;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetCharacterChecked
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* UIONInventoryComponent::GetCharacterChecked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetCharacterChecked");

	UIONInventoryComponent_GetCharacterChecked_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetCharacter
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* UIONInventoryComponent::GetCharacter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetCharacter");

	UIONInventoryComponent_GetCharacter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetArrayFromType
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// EInventoryType                 ArrayType                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// TArray<class AIONItem*>        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class AIONItem*> UIONInventoryComponent::GetArrayFromType(EInventoryType ArrayType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetArrayFromType");

	UIONInventoryComponent_GetArrayFromType_Params params;
	params.ArrayType = ArrayType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetAmmoOfType
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EFirearmType                   FirearmType                    (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONInventoryComponent::GetAmmoOfType(EFirearmType FirearmType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetAmmoOfType");

	UIONInventoryComponent_GetAmmoOfType_Params params;
	params.FirearmType = FirearmType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.GetAllItems
// (Final, Native, Public, HasOutParms, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TArray<class AIONItem*>        OutItems                       (Parm, OutParm, ZeroConstructor)

void UIONInventoryComponent::GetAllItems(TArray<class AIONItem*>* OutItems)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.GetAllItems");

	UIONInventoryComponent_GetAllItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutItems != nullptr)
		*OutItems = params.OutItems;
}


// Function IONBranch.IONInventoryComponent.FindItemOfClass
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UClass*                  ItemClass                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UIONInventoryComponent::FindItemOfClass(class UClass* ItemClass)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.FindItemOfClass");

	UIONInventoryComponent_FindItemOfClass_Params params;
	params.ItemClass = ItemClass;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.CanAddItem
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class AIONItem*                Item                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryComponent::CanAddItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.CanAddItem");

	UIONInventoryComponent_CanAddItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryComponent.AddItem
// (Final, BlueprintAuthorityOnly, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                NewItem                        (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryComponent::AddItem(class AIONItem* NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryComponent.AddItem");

	UIONInventoryComponent_AddItem_Params params;
	params.NewItem = NewItem;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryWidget.ShowInventoryBar
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bInventoryFull                 (Parm, ZeroConstructor, IsPlainOldData)

void UIONInventoryWidget::ShowInventoryBar(bool bInventoryFull)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.ShowInventoryBar");

	UIONInventoryWidget_ShowInventoryBar_Params params;
	params.bInventoryFull = bInventoryFull;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryWidget.ShowInventory
// (Event, Public, BlueprintEvent)

void UIONInventoryWidget::ShowInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.ShowInventory");

	UIONInventoryWidget_ShowInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryWidget.ShouldAssignItemToSlot
// (Event, Public, BlueprintEvent, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONInventoryWidget::ShouldAssignItemToSlot()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.ShouldAssignItemToSlot");

	UIONInventoryWidget_ShouldAssignItemToSlot_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONInventoryWidget.SetDetailsItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)

void UIONInventoryWidget::SetDetailsItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.SetDetailsItem");

	UIONInventoryWidget_SetDetailsItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryWidget.OnUpdateWeaponStatsRender
// (Final, Native, Private)
// Parameters:
// class UCanvas*                 Canvas                         (Parm, ZeroConstructor, IsPlainOldData)
// int                            SizeX                          (Parm, ZeroConstructor, IsPlainOldData)
// int                            SizeY                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONInventoryWidget::OnUpdateWeaponStatsRender(class UCanvas* Canvas, int SizeX, int SizeY)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.OnUpdateWeaponStatsRender");

	UIONInventoryWidget_OnUpdateWeaponStatsRender_Params params;
	params.Canvas = Canvas;
	params.SizeX = SizeX;
	params.SizeY = SizeY;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInventoryWidget.HideInventory
// (Event, Public, BlueprintEvent)

void UIONInventoryWidget::HideInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInventoryWidget.HideInventory");

	UIONInventoryWidget_HideInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInviteReceivedUserWidget.OpenFriendsListButtonClicked
// (Final, Native, Private)

void UIONInviteReceivedUserWidget::OpenFriendsListButtonClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInviteReceivedUserWidget.OpenFriendsListButtonClicked");

	UIONInviteReceivedUserWidget_OpenFriendsListButtonClicked_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONInviteReceivedUserWidget.Dismiss
// (Final, Native, Private)

void UIONInviteReceivedUserWidget::Dismiss()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONInviteReceivedUserWidget.Dismiss");

	UIONInviteReceivedUserWidget_Dismiss_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItemIcon.SetInteractive
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bNewInteractive                (Parm, ZeroConstructor, IsPlainOldData)

void UIONItemIcon::SetInteractive(bool bNewInteractive)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemIcon.SetInteractive");

	UIONItemIcon_SetInteractive_Params params;
	params.bNewInteractive = bNewInteractive;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItemIcon.SetEquipped
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bNewEquipped                   (Parm, ZeroConstructor, IsPlainOldData)

void UIONItemIcon::SetEquipped(bool bNewEquipped)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemIcon.SetEquipped");

	UIONItemIcon_SetEquipped_Params params;
	params.bNewEquipped = bNewEquipped;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItemIcon.IsInLoadout
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONItemIcon::IsInLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemIcon.IsInLoadout");

	UIONItemIcon_IsInLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONItemIcon.BPEvent_EquipChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bEquipped                      (Parm, ZeroConstructor, IsPlainOldData)

void UIONItemIcon::BPEvent_EquipChanged(bool bEquipped)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemIcon.BPEvent_EquipChanged");

	UIONItemIcon_BPEvent_EquipChanged_Params params;
	params.bEquipped = bEquipped;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItemWidget.UpdateWeaponSkin
// (Final, Native, Protected)
// Parameters:
// class UIONWeaponSkin*          WeaponSkin                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONItemWidget::UpdateWeaponSkin(class UIONWeaponSkin* WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemWidget.UpdateWeaponSkin");

	UIONItemWidget_UpdateWeaponSkin_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONItemWidget.UpdateItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONItem*                Item                           (Parm, ZeroConstructor, IsPlainOldData)

void UIONItemWidget::UpdateItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONItemWidget.UpdateItem");

	UIONItemWidget_UpdateItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONKnife.IsRecharging
// (Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONKnife::IsRecharging()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONKnife.IsRecharging");

	AIONKnife_IsRecharging_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONKnife.EventBP_HitSurfaceType
// (Event, Public, BlueprintEvent)
// Parameters:
// TEnumAsByte<EPhysicalSurface>  SurfaceTypeHit                 (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void AIONKnife::EventBP_HitSurfaceType(TEnumAsByte<EPhysicalSurface> SurfaceTypeHit)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONKnife.EventBP_HitSurfaceType");

	AIONKnife_EventBP_HitSurfaceType_Params params;
	params.SurfaceTypeHit = SurfaceTypeHit;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONLoadoutWidget.GetWearableLoadoutIcon
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UClass*                  Wearable                       (Parm, ZeroConstructor, IsPlainOldData)
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONLoadoutWidget::GetWearableLoadoutIcon(class UClass* Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLoadoutWidget.GetWearableLoadoutIcon");

	UIONLoadoutWidget_GetWearableLoadoutIcon_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONLoadoutWidget.GetWearableLoadout
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<class UIONArmorSkin*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONArmorSkin*> UIONLoadoutWidget::GetWearableLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLoadoutWidget.GetWearableLoadout");

	UIONLoadoutWidget_GetWearableLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONLoadoutWidget.GetWeaponLoadout
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<class UIONWeaponSkin*>  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONWeaponSkin*> UIONLoadoutWidget::GetWeaponLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLoadoutWidget.GetWeaponLoadout");

	UIONLoadoutWidget_GetWeaponLoadout_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONLoadoutWidget.GetLoadoutForWeapon
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UClass*                  Weapon                         (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FIONSteamInventoryItem  ReturnValue                    (Parm, OutParm, ReturnParm)

struct FIONSteamInventoryItem UIONLoadoutWidget::GetLoadoutForWeapon(class UClass* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLoadoutWidget.GetLoadoutForWeapon");

	UIONLoadoutWidget_GetLoadoutForWeapon_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONLoadoutWidget.GetDropsuitLoadoutIcon
// (Final, Native, Protected, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONLoadoutWidget::GetDropsuitLoadoutIcon()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLoadoutWidget.GetDropsuitLoadoutIcon");

	UIONLoadoutWidget_GetDropsuitLoadoutIcon_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONLobbyMemberUserWidget.OnMemberButtonClicked
// (Final, Native, Private)

void UIONLobbyMemberUserWidget::OnMemberButtonClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLobbyMemberUserWidget.OnMemberButtonClicked");

	UIONLobbyMemberUserWidget_OnMemberButtonClicked_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONLobbyMemberUserWidget.LeaveParty
// (Final, Native, Public)

void UIONLobbyMemberUserWidget::LeaveParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLobbyMemberUserWidget.LeaveParty");

	UIONLobbyMemberUserWidget_LeaveParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONLobbyMemberUserWidget.KickPlayer
// (Final, Native, Public)

void UIONLobbyMemberUserWidget::KickPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLobbyMemberUserWidget.KickPlayer");

	UIONLobbyMemberUserWidget_KickPlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONLobbyUserWidget.PartyPlayerLeave
// (Final, Native, Public)
// Parameters:
// struct FMatchmakingPlayer      Player                         (Parm)

void UIONLobbyUserWidget::PartyPlayerLeave(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLobbyUserWidget.PartyPlayerLeave");

	UIONLobbyUserWidget_PartyPlayerLeave_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONLobbyUserWidget.PartyPlayerJoin
// (Final, Native, Public)
// Parameters:
// struct FMatchmakingPlayer      Player                         (Parm)

void UIONLobbyUserWidget::PartyPlayerJoin(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONLobbyUserWidget.PartyPlayerJoin");

	UIONLobbyUserWidget_PartyPlayerJoin_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMainMenuUserWidget.GetIONPC
// (Final, Native, Private, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONBasePlayerController* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONBasePlayerController* UIONMainMenuUserWidget::GetIONPC()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMainMenuUserWidget.GetIONPC");

	UIONMainMenuUserWidget_GetIONPC_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.UpdatePartyMembersArmor
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// struct FString                 ArmorData                      (Parm, ZeroConstructor)

void UIONMatchmaking::UpdatePartyMembersArmor(const struct FString& SteamID, const struct FString& ArmorData)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.UpdatePartyMembersArmor");

	UIONMatchmaking_UpdatePartyMembersArmor_Params params;
	params.SteamID = SteamID;
	params.ArmorData = ArmorData;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmaking.SetRegion
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EIONPartyRegion                region                         (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::SetRegion(EIONPartyRegion region)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.SetRegion");

	UIONMatchmaking_SetRegion_Params params;
	params.region = region;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.SetPartyType
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EIONPartyTypes                 Type                           (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::SetPartyType(EIONPartyTypes Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.SetPartyType");

	UIONMatchmaking_SetPartyType_Params params;
	params.Type = Type;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.SetAutomatch
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           bAutomatch                     (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void UIONMatchmaking::SetAutomatch(bool bAutomatch)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.SetAutomatch");

	UIONMatchmaking_SetAutomatch_Params params;
	params.bAutomatch = bAutomatch;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyUpdated__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// struct FMatchmakingParty       Party                          (Parm)

void UIONMatchmaking::MatchmakingPartyUpdated__DelegateSignature(const struct FMatchmakingParty& Party)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyUpdated__DelegateSignature");

	UIONMatchmaking_MatchmakingPartyUpdated__DelegateSignature_Params params;
	params.Party = Party;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyMemberArmorUpdated__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// struct FString                 ArmorData                      (Parm, ZeroConstructor)

void UIONMatchmaking::MatchmakingPartyMemberArmorUpdated__DelegateSignature(const struct FString& SteamID, const struct FString& ArmorData)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyMemberArmorUpdated__DelegateSignature");

	UIONMatchmaking_MatchmakingPartyMemberArmorUpdated__DelegateSignature_Params params;
	params.SteamID = SteamID;
	params.ArmorData = ArmorData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmaking.IsRegionAllowed
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EIONPartyRegion                region                         (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::IsRegionAllowed(EIONPartyRegion region)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.IsRegionAllowed");

	UIONMatchmaking_IsRegionAllowed_Params params;
	params.region = region;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.IsPartyTypeAllowed
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// EIONPartyTypes                 PartyType                      (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::IsPartyTypeAllowed(EIONPartyTypes PartyType)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.IsPartyTypeAllowed");

	UIONMatchmaking_IsPartyTypeAllowed_Params params;
	params.PartyType = PartyType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.IsPartyLeader
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::IsPartyLeader()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.IsPartyLeader");

	UIONMatchmaking_IsPartyLeader_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.IsLeader
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::IsLeader()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.IsLeader");

	UIONMatchmaking_IsLeader_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.IsAutomatchingAllowed
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONMatchmaking::IsAutomatchingAllowed()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.IsAutomatchingAllowed");

	UIONMatchmaking_IsAutomatchingAllowed_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.Invite
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)

void UIONMatchmaking::Invite(const struct FString& SteamID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.Invite");

	UIONMatchmaking_Invite_Params params;
	params.SteamID = SteamID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmaking.GetPartySize
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONMatchmaking::GetPartySize()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.GetPartySize");

	UIONMatchmaking_GetPartySize_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.GetMaxPlayersForCurrentMode
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONMatchmaking::GetMaxPlayersForCurrentMode()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.GetMaxPlayersForCurrentMode");

	UIONMatchmaking_GetMaxPlayersForCurrentMode_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMatchmaking.DisableRegion
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EIONPartyRegion                region                         (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void UIONMatchmaking::DisableRegion(EIONPartyRegion region)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmaking.DisableRegion");

	UIONMatchmaking_DisableRegion_Params params;
	params.region = region;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestGameInstance.InitializeUI
// (Final, Native, Public, BlueprintCallable)

void UIONMatchmakingTestGameInstance::InitializeUI()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestGameInstance.InitializeUI");

	UIONMatchmakingTestGameInstance_InitializeUI_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ToggleAutomatch
// (Final, Native, Private)
// Parameters:
// bool                           Checked                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONMatchmakingTestMenuWidget::ToggleAutomatch(bool Checked)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ToggleAutomatch");

	UIONMatchmakingTestMenuWidget_ToggleAutomatch_Params params;
	params.Checked = Checked;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingPartyUpdated
// (Final, Native, Private)
// Parameters:
// struct FMatchmakingParty       Party                          (Parm)

void UIONMatchmakingTestMenuWidget::OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingPartyUpdated");

	UIONMatchmakingTestMenuWidget_OnMatchmakingPartyUpdated_Params params;
	params.Party = Party;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingInviteReceived
// (Final, Native, Private)
// Parameters:
// TArray<struct FMatchmakingInvite> Invites                        (Parm, ZeroConstructor)

void UIONMatchmakingTestMenuWidget::OnMatchmakingInviteReceived(TArray<struct FMatchmakingInvite> Invites)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingInviteReceived");

	UIONMatchmakingTestMenuWidget_OnMatchmakingInviteReceived_Params params;
	params.Invites = Invites;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.LeaveParty
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::LeaveParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.LeaveParty");

	UIONMatchmakingTestMenuWidget_LeaveParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.InvitePlayer
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::InvitePlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.InvitePlayer");

	UIONMatchmakingTestMenuWidget_InvitePlayer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSquadParty
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToSquadParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSquadParty");

	UIONMatchmakingTestMenuWidget_ChangeToSquadParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSoloParty
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToSoloParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSoloParty");

	UIONMatchmakingTestMenuWidget_ChangeToSoloParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSEARegion
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToSEARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSEARegion");

	UIONMatchmakingTestMenuWidget_ChangeToSEARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSARegion
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToSARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSARegion");

	UIONMatchmakingTestMenuWidget_ChangeToSARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToOCERegion
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToOCERegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToOCERegion");

	UIONMatchmakingTestMenuWidget_ChangeToOCERegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToNARegion
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToNARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToNARegion");

	UIONMatchmakingTestMenuWidget_ChangeToNARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToEURegion
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToEURegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToEURegion");

	UIONMatchmakingTestMenuWidget_ChangeToEURegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToDuoParty
// (Final, Native, Private)

void UIONMatchmakingTestMenuWidget::ChangeToDuoParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToDuoParty");

	UIONMatchmakingTestMenuWidget_ChangeToDuoParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingUserWidget.UpdateReadyImage
// (Final, Native, Private)
// Parameters:
// struct FMatchmakingParty       Party                          (Parm)

void UIONMatchmakingUserWidget::UpdateReadyImage(const struct FMatchmakingParty& Party)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingUserWidget.UpdateReadyImage");

	UIONMatchmakingUserWidget_UpdateReadyImage_Params params;
	params.Party = Party;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingUserWidget.ToggleReadiness
// (Final, Native, Private)

void UIONMatchmakingUserWidget::ToggleReadiness()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingUserWidget.ToggleReadiness");

	UIONMatchmakingUserWidget_ToggleReadiness_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingUserWidget.ShowServerSelection
// (Final, Native, Private, BlueprintCallable)

void UIONMatchmakingUserWidget::ShowServerSelection()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingUserWidget.ShowServerSelection");

	UIONMatchmakingUserWidget_ShowServerSelection_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchmakingUserWidget.HideServerSelection
// (Final, Native, Private, BlueprintCallable)

void UIONMatchmakingUserWidget::HideServerSelection()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchmakingUserWidget.HideServerSelection");

	UIONMatchmakingUserWidget_HideServerSelection_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.StartLevelProgress
// (Final, Native, Public, BlueprintCallable)

void UIONMatchReportWidget::StartLevelProgress()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.StartLevelProgress");

	UIONMatchReportWidget_StartLevelProgress_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.SetMatchHistory
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FPlayerMatchHistory     InMatchHistory                 (Parm)

void UIONMatchReportWidget::SetMatchHistory(const struct FPlayerMatchHistory& InMatchHistory)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.SetMatchHistory");

	UIONMatchReportWidget_SetMatchHistory_Params params;
	params.InMatchHistory = InMatchHistory;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.OnScoreBucketFilled
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FScoreEvent             ScoreEvent                     (Parm)

void UIONMatchReportWidget::OnScoreBucketFilled(const struct FScoreEvent& ScoreEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.OnScoreBucketFilled");

	UIONMatchReportWidget_OnScoreBucketFilled_Params params;
	params.ScoreEvent = ScoreEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.OnMatchHistorySet
// (Event, Public, BlueprintEvent)

void UIONMatchReportWidget::OnMatchHistorySet()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.OnMatchHistorySet");

	UIONMatchReportWidget_OnMatchHistorySet_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.OnLevelIncreased
// (Event, Public, BlueprintEvent)

void UIONMatchReportWidget::OnLevelIncreased()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.OnLevelIncreased");

	UIONMatchReportWidget_OnLevelIncreased_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMatchReportWidget.GetMatchHistory
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FPlayerMatchHistory     ReturnValue                    (Parm, OutParm, ReturnParm)

struct FPlayerMatchHistory UIONMatchReportWidget::GetMatchHistory()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMatchReportWidget.GetMatchHistory");

	UIONMatchReportWidget_GetMatchHistory_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONMenuLevelScriptActor.OnPlayerArmorUpdated
// (Final, Native, Private)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// struct FString                 UpdatedArmor                   (Parm, ZeroConstructor)

void AIONMenuLevelScriptActor::OnPlayerArmorUpdated(const struct FString& SteamID, const struct FString& UpdatedArmor)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMenuLevelScriptActor.OnPlayerArmorUpdated");

	AIONMenuLevelScriptActor_OnPlayerArmorUpdated_Params params;
	params.SteamID = SteamID;
	params.UpdatedArmor = UpdatedArmor;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMenuLevelScriptActor.OnMatchmakingPartyUpdated
// (Final, Native, Private)
// Parameters:
// struct FMatchmakingParty       Party                          (Parm)

void AIONMenuLevelScriptActor::OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMenuLevelScriptActor.OnMatchmakingPartyUpdated");

	AIONMenuLevelScriptActor_OnMatchmakingPartyUpdated_Params params;
	params.Party = Party;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONMTPlayerUserWidget.ToggleReadiness
// (Final, Native, Private)
// Parameters:
// bool                           Checked                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONMTPlayerUserWidget::ToggleReadiness(bool Checked)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONMTPlayerUserWidget.ToggleReadiness");

	UIONMTPlayerUserWidget_ToggleReadiness_Params params;
	params.Checked = Checked;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPartySpawnActor.PlayerUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer      Player                         (Parm)

void AIONPartySpawnActor::PlayerUpdated(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPartySpawnActor.PlayerUpdated");

	AIONPartySpawnActor_PlayerUpdated_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPartySpawnActor.DestroyPlayerPawn
// (Event, Public, BlueprintEvent)

void AIONPartySpawnActor::DestroyPlayerPawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPartySpawnActor.DestroyPlayerPawn");

	AIONPartySpawnActor_DestroyPlayerPawn_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPartySpawnActor.CreatePlayerPawn
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer      Player                         (Parm)

void AIONPartySpawnActor::CreatePlayerPawn(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPartySpawnActor.CreatePlayerPawn");

	AIONPartySpawnActor_CreatePlayerPawn_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPartySpawnActor.BPEvent_ArmorUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString                 NewArmor                       (Parm, ZeroConstructor)

void AIONPartySpawnActor::BPEvent_ArmorUpdated(const struct FString& NewArmor)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPartySpawnActor.BPEvent_ArmorUpdated");

	AIONPartySpawnActor_BPEvent_ArmorUpdated_Params params;
	params.NewArmor = NewArmor;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerState.SetVoiceActivity
// (Net, NetReliable, Native, Event, Public, NetServer, BlueprintCallable, NetValidate)
// Parameters:
// EIONVoiceActivity              NewVoiceActivity               (Parm, ZeroConstructor, IsPlainOldData)

void AIONPlayerState::SetVoiceActivity(EIONVoiceActivity NewVoiceActivity)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.SetVoiceActivity");

	AIONPlayerState_SetVoiceActivity_Params params;
	params.NewVoiceActivity = NewVoiceActivity;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerState.SetSquadState
// (Native, Public)
// Parameters:
// class AIONSquadState*          NewSquadState                  (Parm, ZeroConstructor, IsPlainOldData)

void AIONPlayerState::SetSquadState(class AIONSquadState* NewSquadState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.SetSquadState");

	AIONPlayerState_SetSquadState_Params params;
	params.NewSquadState = NewSquadState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerState.ServerSetGameSparksID
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)
// Parameters:
// struct FString                 InGameSparksID                 (Parm, ZeroConstructor)

void AIONPlayerState::ServerSetGameSparksID(const struct FString& InGameSparksID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.ServerSetGameSparksID");

	AIONPlayerState_ServerSetGameSparksID_Params params;
	params.InGameSparksID = InGameSparksID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerState.OnRep_SquadState
// (Native, Public)
// Parameters:
// class AIONSquadState*          OldSquadState                  (Parm, ZeroConstructor, IsPlainOldData)

void AIONPlayerState::OnRep_SquadState(class AIONSquadState* OldSquadState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.OnRep_SquadState");

	AIONPlayerState_OnRep_SquadState_Params params;
	params.OldSquadState = OldSquadState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerState.IsStreamer
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONPlayerState::IsStreamer()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.IsStreamer");

	AIONPlayerState_IsStreamer_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.IsSameSquadAs
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class APlayerState*            Other                          (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONPlayerState::IsSameSquadAs(class APlayerState* Other)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.IsSameSquadAs");

	AIONPlayerState_IsSameSquadAs_Params params;
	params.Other = Other;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.IsQA
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONPlayerState::IsQA()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.IsQA");

	AIONPlayerState_IsQA_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.IsDeveloper
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONPlayerState::IsDeveloper()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.IsDeveloper");

	AIONPlayerState_IsDeveloper_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.IsAdmin
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONPlayerState::IsAdmin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.IsAdmin");

	AIONPlayerState_IsAdmin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.GetTeamIndex
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONPlayerState::GetTeamIndex()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.GetTeamIndex");

	AIONPlayerState_GetTeamIndex_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.GetTeamColor
// (Final, Native, Public, HasDefaults, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor AIONPlayerState::GetTeamColor()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.GetTeamColor");

	AIONPlayerState_GetTeamColor_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerState.GetPlayerColor
// (Final, Native, Public, HasDefaults, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor AIONPlayerState::GetPlayerColor()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerState.GetPlayerColor");

	AIONPlayerState_GetPlayerColor_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONPlayerWidget.UpdateInteractionWidgetWithText
// (Native, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   InteractionText                (ConstParm, Parm, OutParm, ReferenceParm)
// float                          DeltaTime                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHideInteractButton            (Parm, ZeroConstructor, IsPlainOldData)

void UIONPlayerWidget::UpdateInteractionWidgetWithText(const struct FText& InteractionText, float DeltaTime, bool bHideInteractButton)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerWidget.UpdateInteractionWidgetWithText");

	UIONPlayerWidget_UpdateInteractionWidgetWithText_Params params;
	params.InteractionText = InteractionText;
	params.DeltaTime = DeltaTime;
	params.bHideInteractButton = bHideInteractButton;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerWidget.UpdateInteractionWidget
// (Native, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FInteractionOption      InteractionOption              (ConstParm, Parm, OutParm, ReferenceParm)
// float                          DeltaTime                      (Parm, ZeroConstructor, IsPlainOldData)

void UIONPlayerWidget::UpdateInteractionWidget(const struct FInteractionOption& InteractionOption, float DeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerWidget.UpdateInteractionWidget");

	UIONPlayerWidget_UpdateInteractionWidget_Params params;
	params.InteractionOption = InteractionOption;
	params.DeltaTime = DeltaTime;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerWidget.HideInteractionWidget
// (Event, Public, BlueprintEvent)

void UIONPlayerWidget::HideInteractionWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerWidget.HideInteractionWidget");

	UIONPlayerWidget_HideInteractionWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONPlayerWidget.GetInteractionWidget
// (Event, Public, BlueprintEvent, Const)
// Parameters:
// class UWidget*                 ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UIONPlayerWidget::GetInteractionWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONPlayerWidget.GetInteractionWidget");

	UIONPlayerWidget_GetInteractionWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONRenderPawn.ItCurrentSkins
// (Final, Native, Public, BlueprintCallable)

void AIONRenderPawn::ItCurrentSkins()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRenderPawn.ItCurrentSkins");

	AIONRenderPawn_ItCurrentSkins_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONRewardsScreen.PurchaseItem
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Item                           (Parm, ZeroConstructor, IsPlainOldData)

void UIONRewardsScreen::PurchaseItem(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.PurchaseItem");

	UIONRewardsScreen_PurchaseItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONRewardsScreen.PurchaseCrate
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Crate                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONRewardsScreen::PurchaseCrate(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.PurchaseCrate");

	UIONRewardsScreen_PurchaseCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONRewardsScreen.GetPriceForItem
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Item                           (Parm, ZeroConstructor, IsPlainOldData)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONRewardsScreen::GetPriceForItem(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.GetPriceForItem");

	UIONRewardsScreen_GetPriceForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONRewardsScreen.GetCratePrice
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Crate                          (Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UIONRewardsScreen::GetCratePrice(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.GetCratePrice");

	UIONRewardsScreen_GetCratePrice_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONRewardsScreen.GetCrateContent
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// class UIONSteamItem*           Crate                          (Parm, ZeroConstructor, IsPlainOldData)
// TArray<class UIONSteamItem*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONSteamItem*> UIONRewardsScreen::GetCrateContent(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.GetCrateContent");

	UIONRewardsScreen_GetCrateContent_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONRewardsScreen.GetAllCrates
// (Final, Native, Protected, BlueprintCallable)
// Parameters:
// TArray<class UIONSteamCrate*>  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class UIONSteamCrate*> UIONRewardsScreen::GetAllCrates()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONRewardsScreen.GetAllCrates");

	UIONRewardsScreen_GetAllCrates_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONScoreEventWidget.OnScoreStringUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString                 NewString                      (Parm, ZeroConstructor)

void UIONScoreEventWidget::OnScoreStringUpdated(const struct FString& NewString)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONScoreEventWidget.OnScoreStringUpdated");

	UIONScoreEventWidget_OnScoreStringUpdated_Params params;
	params.NewString = NewString;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONScoreEventWidget.OnAccumulatedScoreUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// int                            NewScore                       (ConstParm, Parm, ZeroConstructor, IsPlainOldData)

void UIONScoreEventWidget::OnAccumulatedScoreUpdated(int NewScore)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONScoreEventWidget.OnAccumulatedScoreUpdated");

	UIONScoreEventWidget_OnAccumulatedScoreUpdated_Params params;
	params.NewScore = NewScore;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONScoreEventWidget.AddScoreEvent
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// EScoreEvent                    EventType                      (Parm, ZeroConstructor, IsPlainOldData)
// int                            Points                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONScoreEventWidget::AddScoreEvent(EScoreEvent EventType, int Points)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONScoreEventWidget.AddScoreEvent");

	UIONScoreEventWidget_AddScoreEvent_Params params;
	params.EventType = EventType;
	params.Points = Points;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerAuthWidget.SetupAuth
// (Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 ServerName                     (Parm, ZeroConstructor)
// struct FScriptDelegate         NewAcceptedDelegate            (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
// struct FScriptDelegate         NewCancelledDelegate           (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UIONServerAuthWidget::SetupAuth(const struct FString& ServerName, const struct FScriptDelegate& NewAcceptedDelegate, const struct FScriptDelegate& NewCancelledDelegate)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerAuthWidget.SetupAuth");

	UIONServerAuthWidget_SetupAuth_Params params;
	params.ServerName = ServerName;
	params.NewAcceptedDelegate = NewAcceptedDelegate;
	params.NewCancelledDelegate = NewCancelledDelegate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ToggleAutomatch
// (Final, Native, Private)
// Parameters:
// bool                           Checked                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONServerSelectionUserWidget::ToggleAutomatch(bool Checked)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ToggleAutomatch");

	UIONServerSelectionUserWidget_ToggleAutomatch_Params params;
	params.Checked = Checked;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.OnMatchmakingPartyUpdated
// (Final, Native, Private)
// Parameters:
// struct FMatchmakingParty       Party                          (Parm)

void UIONServerSelectionUserWidget::OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.OnMatchmakingPartyUpdated");

	UIONServerSelectionUserWidget_OnMatchmakingPartyUpdated_Params params;
	params.Party = Party;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToSquadParty
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToSquadParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToSquadParty");

	UIONServerSelectionUserWidget_ChangeToSquadParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToSoloParty
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToSoloParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToSoloParty");

	UIONServerSelectionUserWidget_ChangeToSoloParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToSEARegion
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToSEARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToSEARegion");

	UIONServerSelectionUserWidget_ChangeToSEARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToSARegion
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToSARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToSARegion");

	UIONServerSelectionUserWidget_ChangeToSARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToOCERegion
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToOCERegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToOCERegion");

	UIONServerSelectionUserWidget_ChangeToOCERegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToNARegion
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToNARegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToNARegion");

	UIONServerSelectionUserWidget_ChangeToNARegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToEURegion
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToEURegion()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToEURegion");

	UIONServerSelectionUserWidget_ChangeToEURegion_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONServerSelectionUserWidget.ChangeToDuoParty
// (Final, Native, Private)

void UIONServerSelectionUserWidget::ChangeToDuoParty()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONServerSelectionUserWidget.ChangeToDuoParty");

	UIONServerSelectionUserWidget_ChangeToDuoParty_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSight.UpdateMaterialAimState
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm*             Firearm                        (Parm, ZeroConstructor, IsPlainOldData)
// float                          AimRatio                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONSight::UpdateMaterialAimState(class AIONFirearm* Firearm, float AimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSight.UpdateMaterialAimState");

	AIONSight_UpdateMaterialAimState_Params params;
	params.Firearm = Firearm;
	params.AimRatio = AimRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSight.GetMagnificationStat
// (Final, Native, Public, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float AIONSight::GetMagnificationStat()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSight.GetMagnificationStat");

	AIONSight_GetMagnificationStat_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSliderWidget.SliderValueChanged
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// float                          InValue                        (Parm, ZeroConstructor, IsPlainOldData)

void UIONSliderWidget::SliderValueChanged(float InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSliderWidget.SliderValueChanged");

	UIONSliderWidget_SliderValueChanged_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSpectatorInfo.SetupWidget
// (Event, Public, BlueprintEvent)

void AIONSpectatorInfo::SetupWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSpectatorInfo.SetupWidget");

	AIONSpectatorInfo_SetupWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSpectatorInfo.SetTrackedCharacter
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONCharacter*           Character                      (Parm, ZeroConstructor, IsPlainOldData)

void AIONSpectatorInfo::SetTrackedCharacter(class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSpectatorInfo.SetTrackedCharacter");

	AIONSpectatorInfo_SetTrackedCharacter_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSpectatorInfo.OnViewModeChanged
// (Event, Public, BlueprintEvent)
// Parameters:
// bool                           bMinimal                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONSpectatorInfo::OnViewModeChanged(bool bMinimal)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSpectatorInfo.OnViewModeChanged");

	AIONSpectatorInfo_OnViewModeChanged_Params params;
	params.bMinimal = bMinimal;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSpectatorInfo.GetTrackedCharacter
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// class AIONCharacter*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONCharacter* AIONSpectatorInfo::GetTrackedCharacter()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSpectatorInfo.GetTrackedCharacter");

	AIONSpectatorInfo_GetTrackedCharacter_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSquadState.RemovePlayerState
// (Native, Public)
// Parameters:
// class AIONPlayerState*         PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)
// bool                           bKeepGameServiceID             (Parm, ZeroConstructor, IsPlainOldData)

void AIONSquadState::RemovePlayerState(class AIONPlayerState* PlayerState, bool bKeepGameServiceID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSquadState.RemovePlayerState");

	AIONSquadState_RemovePlayerState_Params params;
	params.PlayerState = PlayerState;
	params.bKeepGameServiceID = bKeepGameServiceID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSquadState.IsSquadStanding
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONSquadState::IsSquadStanding()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSquadState.IsSquadStanding");

	AIONSquadState_IsSquadStanding_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSquadState.IsSquadAlive
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AIONSquadState::IsSquadAlive()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSquadState.IsSquadAlive");

	AIONSquadState_IsSquadAlive_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSquadState.GetSquadAliveCount
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int AIONSquadState::GetSquadAliveCount()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSquadState.GetSquadAliveCount");

	AIONSquadState_GetSquadAliveCount_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSquadState.AddPlayerState
// (Native, Public)
// Parameters:
// class AIONPlayerState*         PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)

void AIONSquadState::AddPlayerState(class AIONPlayerState* PlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSquadState.AddPlayerState");

	AIONSquadState_AddPlayerState_Params params;
	params.PlayerState = PlayerState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.UpdateAudioEnvironment
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UAkComponent*            AudioComponent                 (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UIONStatics::STATIC_UpdateAudioEnvironment(class UAkComponent* AudioComponent)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.UpdateAudioEnvironment");

	UIONStatics_UpdateAudioEnvironment_Params params;
	params.AudioComponent = AudioComponent;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.SetSoundClassVolume
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class USoundClass*             SoundClass                     (Parm, ZeroConstructor, IsPlainOldData)
// float                          Volume                         (Parm, ZeroConstructor, IsPlainOldData)

void UIONStatics::STATIC_SetSoundClassVolume(class USoundClass* SoundClass, float Volume)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.SetSoundClassVolume");

	UIONStatics_SetSoundClassVolume_Params params;
	params.SoundClass = SoundClass;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.SetSoundClassPitch
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class USoundClass*             SoundClass                     (Parm, ZeroConstructor, IsPlainOldData)
// float                          Pitch                          (Parm, ZeroConstructor, IsPlainOldData)

void UIONStatics::STATIC_SetSoundClassPitch(class USoundClass* SoundClass, float Pitch)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.SetSoundClassPitch");

	UIONStatics_SetSoundClassPitch_Params params;
	params.SoundClass = SoundClass;
	params.Pitch = Pitch;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.SetAntiCheatRunning
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// bool                           bIsRunning                     (Parm, ZeroConstructor, IsPlainOldData)

void UIONStatics::STATIC_SetAntiCheatRunning(bool bIsRunning)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.SetAntiCheatRunning");

	UIONStatics_SetAntiCheatRunning_Params params;
	params.bIsRunning = bIsRunning;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.SecondsToText
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// float                          Seconds                        (Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UIONStatics::STATIC_SecondsToText(float Seconds)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.SecondsToText");

	UIONStatics_SecondsToText_Params params;
	params.Seconds = Seconds;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.RotationToDirectionText
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FRotator                Rotation                       (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UIONStatics::STATIC_RotationToDirectionText(const struct FRotator& Rotation)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.RotationToDirectionText");

	UIONStatics_RotationToDirectionText_Params params;
	params.Rotation = Rotation;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.RebaseHitToLocalOrigin
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FHitResult              Hit                            (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FHitResult              ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FHitResult UIONStatics::STATIC_RebaseHitToLocalOrigin(class UObject* WorldContextObject, const struct FHitResult& Hit)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.RebaseHitToLocalOrigin");

	UIONStatics_RebaseHitToLocalOrigin_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Hit = Hit;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.OpenWebURL
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 URL                            (Parm, ZeroConstructor)

void UIONStatics::STATIC_OpenWebURL(const struct FString& URL)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.OpenWebURL");

	UIONStatics_OpenWebURL_Params params;
	params.URL = URL;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.OpenShootingRange
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)

void UIONStatics::STATIC_OpenShootingRange(class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.OpenShootingRange");

	UIONStatics_OpenShootingRange_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.OpenMainMenu
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)

void UIONStatics::STATIC_OpenMainMenu(class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.OpenMainMenu");

	UIONStatics_OpenMainMenu_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.IsNoSteam
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONStatics::STATIC_IsNoSteam()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.IsNoSteam");

	UIONStatics_IsNoSteam_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.IsAntiCheatRunning
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONStatics::STATIC_IsAntiCheatRunning()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.IsAntiCheatRunning");

	UIONStatics_IsAntiCheatRunning_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.IsAntiCheatEnabled
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONStatics::STATIC_IsAntiCheatEnabled()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.IsAntiCheatEnabled");

	UIONStatics_IsAntiCheatEnabled_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetWorldSettings
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// class AMainWorldSettings*      ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AMainWorldSettings* UIONStatics::STATIC_GetWorldSettings(class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetWorldSettings");

	UIONStatics_GetWorldSettings_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetWidgetChildGeometry
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UWidget*                 ParentWidget                   (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UWidget*                 ChildWidget                    (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// struct FGeometry               MyGeometry                     (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FGeometry               ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FGeometry UIONStatics::STATIC_GetWidgetChildGeometry(class UWidget* ParentWidget, class UWidget* ChildWidget, const struct FGeometry& MyGeometry)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetWidgetChildGeometry");

	UIONStatics_GetWidgetChildGeometry_Params params;
	params.ParentWidget = ParentWidget;
	params.ChildWidget = ChildWidget;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetRelativeMapLocationForPosition
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 Position                       (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FVector2D               ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FVector2D UIONStatics::STATIC_GetRelativeMapLocationForPosition(class UObject* WorldContextObject, const struct FVector& Position)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetRelativeMapLocationForPosition");

	UIONStatics_GetRelativeMapLocationForPosition_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Position = Position;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetPlayerStateInfo
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class APlayerState*            PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONStatics::STATIC_GetPlayerStateInfo(class APlayerState* PlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetPlayerStateInfo");

	UIONStatics_GetPlayerStateInfo_Params params;
	params.PlayerState = PlayerState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetPlayerPawn
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class APlayerState*            Player                         (Parm, ZeroConstructor, IsPlainOldData)
// class APawn*                   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class APawn* UIONStatics::STATIC_GetPlayerPawn(class APlayerState* Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetPlayerPawn");

	UIONStatics_GetPlayerPawn_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetPlayerControllerInfo
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class APlayerController*       PlayerController               (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONStatics::STATIC_GetPlayerControllerInfo(class APlayerController* PlayerController)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetPlayerControllerInfo");

	UIONStatics_GetPlayerControllerInfo_Params params;
	params.PlayerController = PlayerController;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetPlasmaStage
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 OutPlasmaLocation              (Parm, OutParm, IsPlainOldData)
// float                          OutPlasmaRadius                (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FVector                 OutSafeZoneLocation            (Parm, OutParm, IsPlainOldData)
// float                          OutSafeZoneRadius              (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// EPlasmaState                   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EPlasmaState UIONStatics::STATIC_GetPlasmaStage(class UObject* WorldContextObject, struct FVector* OutPlasmaLocation, float* OutPlasmaRadius, struct FVector* OutSafeZoneLocation, float* OutSafeZoneRadius)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetPlasmaStage");

	UIONStatics_GetPlasmaStage_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutPlasmaLocation != nullptr)
		*OutPlasmaLocation = params.OutPlasmaLocation;
	if (OutPlasmaRadius != nullptr)
		*OutPlasmaRadius = params.OutPlasmaRadius;
	if (OutSafeZoneLocation != nullptr)
		*OutSafeZoneLocation = params.OutSafeZoneLocation;
	if (OutSafeZoneRadius != nullptr)
		*OutSafeZoneRadius = params.OutSafeZoneRadius;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetPlasmaLocation
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 OutPlasmaLocation              (Parm, OutParm, IsPlainOldData)
// float                          OutPlasmaRadius                (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONStatics::STATIC_GetPlasmaLocation(class UObject* WorldContextObject, struct FVector* OutPlasmaLocation, float* OutPlasmaRadius)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetPlasmaLocation");

	UIONStatics_GetPlasmaLocation_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutPlasmaLocation != nullptr)
		*OutPlasmaLocation = params.OutPlasmaLocation;
	if (OutPlasmaRadius != nullptr)
		*OutPlasmaRadius = params.OutPlasmaRadius;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetOrdinalWithPlace
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// unsigned char                  Place                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONStatics::STATIC_GetOrdinalWithPlace(unsigned char Place)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetOrdinalWithPlace");

	UIONStatics_GetOrdinalWithPlace_Params params;
	params.Place = Place;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetOrdinalForPlace
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// unsigned char                  Place                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONStatics::STATIC_GetOrdinalForPlace(unsigned char Place)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetOrdinalForPlace");

	UIONStatics_GetOrdinalForPlace_Params params;
	params.Place = Place;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetOnlinePlatformType
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// EOnlinePlatformType            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EOnlinePlatformType UIONStatics::STATIC_GetOnlinePlatformType()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetOnlinePlatformType");

	UIONStatics_GetOnlinePlatformType_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetInventoryTypeFromClass
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UClass*                  ItemClass                      (Parm, ZeroConstructor, IsPlainOldData)
// EInventoryType                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

EInventoryType UIONStatics::STATIC_GetInventoryTypeFromClass(class UClass* ItemClass)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetInventoryTypeFromClass");

	UIONStatics_GetInventoryTypeFromClass_Params params;
	params.ItemClass = ItemClass;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetGridLocationText
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FIntPoint               Coordinates                    (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UIONStatics::STATIC_GetGridLocationText(class UObject* WorldContextObject, const struct FIntPoint& Coordinates)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetGridLocationText");

	UIONStatics_GetGridLocationText_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Coordinates = Coordinates;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetGridLocation
// (Final, Native, Static, Public, HasOutParms, HasDefaults, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 Location                       (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
// struct FIntPoint               ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FIntPoint UIONStatics::STATIC_GetGridLocation(class UObject* WorldContextObject, const struct FVector& Location)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetGridLocation");

	UIONStatics_GetGridLocation_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Location = Location;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetGameNewsURL
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONStatics::STATIC_GetGameNewsURL()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetGameNewsURL");

	UIONStatics_GetGameNewsURL_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetDataSingleton
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UIONDataSingleton*       ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONDataSingleton* UIONStatics::STATIC_GetDataSingleton()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetDataSingleton");

	UIONStatics_GetDataSingleton_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetCurrentDPIScale
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UIONStatics::STATIC_GetCurrentDPIScale()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetCurrentDPIScale");

	UIONStatics_GetCurrentDPIScale_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONStatics.GetBattleRoyaleStage
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   OutTimeText                    (Parm, OutParm)
// struct FText                   OutStageText                   (Parm, OutParm)

void UIONStatics::STATIC_GetBattleRoyaleStage(class UObject* WorldContextObject, struct FText* OutTimeText, struct FText* OutStageText)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.GetBattleRoyaleStage");

	UIONStatics_GetBattleRoyaleStage_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutTimeText != nullptr)
		*OutTimeText = params.OutTimeText;
	if (OutStageText != nullptr)
		*OutStageText = params.OutStageText;
}


// Function IONBranch.IONStatics.DisplaySubtitle
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   SubtitleText                   (ConstParm, Parm)

void UIONStatics::STATIC_DisplaySubtitle(class UObject* WorldContextObject, const struct FText& SubtitleText)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.DisplaySubtitle");

	UIONStatics_DisplaySubtitle_Params params;
	params.WorldContextObject = WorldContextObject;
	params.SubtitleText = SubtitleText;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONStatics.CreateWidgetSlow
// (Final, BlueprintCosmetic, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// class UClass*                  WidgetType                     (Parm, ZeroConstructor, IsPlainOldData)
// class APlayerController*       OwningPlayer                   (Parm, ZeroConstructor, IsPlainOldData)
// class UUserWidget*             ReturnValue                    (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UUserWidget* UIONStatics::STATIC_CreateWidgetSlow(class UObject* WorldContextObject, class UClass* WidgetType, class APlayerController* OwningPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONStatics.CreateWidgetSlow");

	UIONStatics_CreateWidgetSlow_Params params;
	params.WorldContextObject = WorldContextObject;
	params.WidgetType = WidgetType;
	params.OwningPlayer = OwningPlayer;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSteamInventory.TriggerItemDrop
// (Final, Native, Public, BlueprintCallable)

void UIONSteamInventory::TriggerItemDrop()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamInventory.TriggerItemDrop");

	UIONSteamInventory_TriggerItemDrop_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONSteamInventory.SteamInventoryUpdateDelegate__DelegateSignature
// (MulticastDelegate, Public, Delegate)

void UIONSteamInventory::SteamInventoryUpdateDelegate__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONSteamInventory.SteamInventoryUpdateDelegate__DelegateSignature");

	UIONSteamInventory_SteamInventoryUpdateDelegate__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSteamInventory.PlayerOwnsItem
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            ItemToCheck                    (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONSteamInventory::PlayerOwnsItem(int ItemToCheck)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamInventory.PlayerOwnsItem");

	UIONSteamInventory_PlayerOwnsItem_Params params;
	params.ItemToCheck = ItemToCheck;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// DelegateFunction IONBranch.IONSteamInventory.OnSteamSellItem__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// struct FString                 ItemDefString                  (Parm, ZeroConstructor)

void UIONSteamInventory::OnSteamSellItem__DelegateSignature(const struct FString& ItemDefString)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONSteamInventory.OnSteamSellItem__DelegateSignature");

	UIONSteamInventory_OnSteamSellItem__DelegateSignature_Params params;
	params.ItemDefString = ItemDefString;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// DelegateFunction IONBranch.IONSteamInventory.OnSteamBuyItem__DelegateSignature
// (MulticastDelegate, Public, Delegate)
// Parameters:
// struct FString                 ItemDefString                  (Parm, ZeroConstructor)

void UIONSteamInventory::OnSteamBuyItem__DelegateSignature(const struct FString& ItemDefString)
{
	static auto fn = UObject::FindObject<UFunction>("DelegateFunction IONBranch.IONSteamInventory.OnSteamBuyItem__DelegateSignature");

	UIONSteamInventory_OnSteamBuyItem__DelegateSignature_Params params;
	params.ItemDefString = ItemDefString;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONSteamInventory.IsAvailable
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UIONSteamInventory::IsAvailable()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamInventory.IsAvailable");

	UIONSteamInventory_IsAvailable_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSteamInventory.GetItemsBySteamItemDef
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<int>                    ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<int> UIONSteamInventory::GetItemsBySteamItemDef()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamInventory.GetItemsBySteamItemDef");

	UIONSteamInventory_GetItemsBySteamItemDef_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONSteamInventory.GenerateDevItems
// (Final, Native, Public, BlueprintCallable)

void UIONSteamInventory::GenerateDevItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONSteamInventory.GenerateDevItems");

	UIONSteamInventory_GenerateDevItems_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONUserFunctionLibrary.GetSteamOwnName
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONUserFunctionLibrary::STATIC_GetSteamOwnName()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetSteamOwnName");

	UIONUserFunctionLibrary_GetSteamOwnName_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetSteamID
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONUserFunctionLibrary::STATIC_GetSteamID()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetSteamID");

	UIONUserFunctionLibrary_GetSteamID_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetSteamFriendPersonaName
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONUserFunctionLibrary::STATIC_GetSteamFriendPersonaName(const struct FString& SteamID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetSteamFriendPersonaName");

	UIONUserFunctionLibrary_GetSteamFriendPersonaName_Params params;
	params.SteamID = SteamID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromSteamId
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// class AIONPlayerState*         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONPlayerState* UIONUserFunctionLibrary::STATIC_GetPlayerStateFromSteamId(class UObject* WorldContextObject, const struct FString& SteamID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromSteamId");

	UIONUserFunctionLibrary_GetPlayerStateFromSteamId_Params params;
	params.WorldContextObject = WorldContextObject;
	params.SteamID = SteamID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromGameSparksID
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 GameSparksID                   (Parm, ZeroConstructor)
// class AIONPlayerState*         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONPlayerState* UIONUserFunctionLibrary::STATIC_GetPlayerStateFromGameSparksID(class UObject* WorldContextObject, const struct FString& GameSparksID)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromGameSparksID");

	UIONUserFunctionLibrary_GetPlayerStateFromGameSparksID_Params params;
	params.WorldContextObject = WorldContextObject;
	params.GameSparksID = GameSparksID;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetPlayerId
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class APlayerState*            PlayerState                    (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONUserFunctionLibrary::STATIC_GetPlayerId(class APlayerState* PlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetPlayerId");

	UIONUserFunctionLibrary_GetPlayerId_Params params;
	params.PlayerState = PlayerState;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatarFromSteamID
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 Outer                          (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 SteamID                        (Parm, ZeroConstructor)
// ESteamAvatarSize               Size                           (Parm, ZeroConstructor, IsPlainOldData)
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONUserFunctionLibrary::STATIC_GetPlayerAvatarFromSteamID(class UObject* Outer, const struct FString& SteamID, ESteamAvatarSize Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatarFromSteamID");

	UIONUserFunctionLibrary_GetPlayerAvatarFromSteamID_Params params;
	params.Outer = Outer;
	params.SteamID = SteamID;
	params.Size = Size;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatar
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 Outer                          (Parm, ZeroConstructor, IsPlainOldData)
// class APlayerState*            Player                         (Parm, ZeroConstructor, IsPlainOldData)
// ESteamAvatarSize               Size                           (Parm, ZeroConstructor, IsPlainOldData)
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONUserFunctionLibrary::STATIC_GetPlayerAvatar(class UObject* Outer, class APlayerState* Player, ESteamAvatarSize Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatar");

	UIONUserFunctionLibrary_GetPlayerAvatar_Params params;
	params.Outer = Outer;
	params.Player = Player;
	params.Size = Size;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetOwnAvatar
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 Outer                          (Parm, ZeroConstructor, IsPlainOldData)
// class APlayerController*       PC                             (Parm, ZeroConstructor, IsPlainOldData)
// ESteamAvatarSize               Size                           (Parm, ZeroConstructor, IsPlainOldData)
// class UTexture2D*              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UTexture2D* UIONUserFunctionLibrary::STATIC_GetOwnAvatar(class UObject* Outer, class APlayerController* PC, ESteamAvatarSize Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetOwnAvatar");

	UIONUserFunctionLibrary_GetOwnAvatar_Params params;
	params.Outer = Outer;
	params.PC = PC;
	params.Size = Size;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONUserFunctionLibrary.GetAuthToken
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UIONUserFunctionLibrary::STATIC_GetAuthToken()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONUserFunctionLibrary.GetAuthToken");

	UIONUserFunctionLibrary_GetAuthToken_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.IONWearableMannequin.OnWearableUnequipped
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONWearable*            Wearable                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONWearableMannequin::OnWearableUnequipped(class AIONWearable* Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearableMannequin.OnWearableUnequipped");

	AIONWearableMannequin_OnWearableUnequipped_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWearableMannequin.OnWearableEquipped
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONWearable*            Wearable                       (Parm, ZeroConstructor, IsPlainOldData)

void AIONWearableMannequin::OnWearableEquipped(class AIONWearable* Wearable)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearableMannequin.OnWearableEquipped");

	AIONWearableMannequin_OnWearableEquipped_Params params;
	params.Wearable = Wearable;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWearableMannequin.OnWeaponChanged
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class AIONWeapon*              Weapon                         (Parm, ZeroConstructor, IsPlainOldData)

void AIONWearableMannequin::OnWeaponChanged(class AIONWeapon* Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearableMannequin.OnWeaponChanged");

	AIONWearableMannequin_OnWeaponChanged_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.IONWearableMannequinAdvanced.UpdateMannequin
// (Final, Native, Protected)

void AIONWearableMannequinAdvanced::UpdateMannequin()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.IONWearableMannequinAdvanced.UpdateMannequin");

	AIONWearableMannequinAdvanced_UpdateMannequin_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.LobbyFunctionLibrary.JoinFriendLobby
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class APlayerController*       PC                             (Parm, ZeroConstructor, IsPlainOldData)
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)

void ULobbyFunctionLibrary::STATIC_JoinFriendLobby(class APlayerController* PC, const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.LobbyFunctionLibrary.JoinFriendLobby");

	ULobbyFunctionLibrary_JoinFriendLobby_Params params;
	params.PC = PC;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.LobbyFunctionLibrary.InviteFriendToLobby
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class APlayerController*       PC                             (Parm, ZeroConstructor, IsPlainOldData)
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, Parm, OutParm, ReferenceParm)

void ULobbyFunctionLibrary::STATIC_InviteFriendToLobby(class APlayerController* PC, const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.LobbyFunctionLibrary.InviteFriendToLobby");

	ULobbyFunctionLibrary_InviteFriendToLobby_Params params;
	params.PC = PC;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.LobbyFunctionLibrary.CreateLobby
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class APlayerController*       PC                             (Parm, ZeroConstructor, IsPlainOldData)

void ULobbyFunctionLibrary::STATIC_CreateLobby(class APlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.LobbyFunctionLibrary.CreateLobby");

	ULobbyFunctionLibrary_CreateLobby_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.LobbyFunctionLibrary.CancelMatchmaking
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool ULobbyFunctionLibrary::STATIC_CancelMatchmaking()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.LobbyFunctionLibrary.CancelMatchmaking");

	ULobbyFunctionLibrary_CancelMatchmaking_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainLocalPlayer.SetPartySettings
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FMainMenuPartySettings  PartySettings                  (Parm)

void UMainLocalPlayer::SetPartySettings(const struct FMainMenuPartySettings& PartySettings)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainLocalPlayer.SetPartySettings");

	UMainLocalPlayer_SetPartySettings_Params params;
	params.PartySettings = PartySettings;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MainLocalPlayer.K2_GetNickname
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString UMainLocalPlayer::K2_GetNickname()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainLocalPlayer.K2_GetNickname");

	UMainLocalPlayer_K2_GetNickname_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainLocalPlayer.IsPlayerBanned
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UMainLocalPlayer::IsPlayerBanned()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainLocalPlayer.IsPlayerBanned");

	UMainLocalPlayer_IsPlayerBanned_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainLocalPlayer.IsLoggedIntoGameSparks
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UMainLocalPlayer::IsLoggedIntoGameSparks()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainLocalPlayer.IsLoggedIntoGameSparks");

	UMainLocalPlayer_IsLoggedIntoGameSparks_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MainLocalPlayer.GetPartySettings
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FMainMenuPartySettings  ReturnValue                    (Parm, OutParm, ReturnParm)

struct FMainMenuPartySettings UMainLocalPlayer::GetPartySettings()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MainLocalPlayer.GetPartySettings");

	UMainLocalPlayer_GetPartySettings_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MatchHistoryFunctionLibrary.GetMatchDeathCauserInfo
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 DeathCauserContext             (Parm, ZeroConstructor)
// struct FText                   OutCauserText                  (Parm, OutParm)
// class UTexture2D*              OutCauserIcon                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UMatchHistoryFunctionLibrary::STATIC_GetMatchDeathCauserInfo(const struct FString& DeathCauserContext, struct FText* OutCauserText, class UTexture2D** OutCauserIcon)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MatchHistoryFunctionLibrary.GetMatchDeathCauserInfo");

	UMatchHistoryFunctionLibrary_GetMatchDeathCauserInfo_Params params;
	params.DeathCauserContext = DeathCauserContext;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutCauserText != nullptr)
		*OutCauserText = params.OutCauserText;
	if (OutCauserIcon != nullptr)
		*OutCauserIcon = params.OutCauserIcon;
}


// Function IONBranch.MenuPlayerController.ShowDesktopNotification
// (Final, Exec, Native, Public)

void AMenuPlayerController::ShowDesktopNotification()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.ShowDesktopNotification");

	AMenuPlayerController_ShowDesktopNotification_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.JoinSession
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FMatchmakingServerFoundResult Result                         (Parm)

void AMenuPlayerController::JoinSession(const struct FMatchmakingServerFoundResult& Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.JoinSession");

	AMenuPlayerController_JoinSession_Params params;
	params.Result = Result;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.IsQA
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMenuPlayerController::IsQA()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.IsQA");

	AMenuPlayerController_IsQA_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MenuPlayerController.IsDeveloper
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool AMenuPlayerController::IsDeveloper()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.IsDeveloper");

	AMenuPlayerController_IsDeveloper_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MenuPlayerController.GetMatchmaking
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class UIONMatchmaking*         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UIONMatchmaking* AMenuPlayerController::GetMatchmaking()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.GetMatchmaking");

	AMenuPlayerController_GetMatchmaking_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.MenuPlayerController.GenerateFriendsList
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<struct FBlueprintOnlineFriend> FriendsList                    (Parm, ZeroConstructor)

void AMenuPlayerController::GenerateFriendsList(TArray<struct FBlueprintOnlineFriend> FriendsList)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.GenerateFriendsList");

	AMenuPlayerController_GenerateFriendsList_Params params;
	params.FriendsList = FriendsList;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.FindCustomServers
// (Final, Native, Public, BlueprintCallable)

void AMenuPlayerController::FindCustomServers()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.FindCustomServers");

	AMenuPlayerController_FindCustomServers_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_ShowOKDialog
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString                 Message                        (Parm, ZeroConstructor)

void AMenuPlayerController::BPEvent_ShowOKDialog(const struct FString& Message)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_ShowOKDialog");

	AMenuPlayerController_BPEvent_ShowOKDialog_Params params;
	params.Message = Message;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PopulateCustomServers
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// TArray<struct FIONServerInfo>  CustomServers                  (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void AMenuPlayerController::BPEvent_PopulateCustomServers(TArray<struct FIONServerInfo> CustomServers)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PopulateCustomServers");

	AMenuPlayerController_BPEvent_PopulateCustomServers_Params params;
	params.CustomServers = CustomServers;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PlayersReadyStatusesChanged
// (Event, Public, BlueprintEvent)

void AMenuPlayerController::BPEvent_PlayersReadyStatusesChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PlayersReadyStatusesChanged");

	AMenuPlayerController_BPEvent_PlayersReadyStatusesChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PartySettingsUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMainMenuPartySettings  PartySettings                  (Parm)

void AMenuPlayerController::BPEvent_PartySettingsUpdated(const struct FMainMenuPartySettings& PartySettings)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PartySettingsUpdated");

	AMenuPlayerController_BPEvent_PartySettingsUpdated_Params params;
	params.PartySettings = PartySettings;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PartyMemberLeave
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (Parm)

void AMenuPlayerController::BPEvent_PartyMemberLeave(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PartyMemberLeave");

	AMenuPlayerController_BPEvent_PartyMemberLeave_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PartyMemberJoined
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMainMenuPartyMember    PartyMember                    (Parm)

void AMenuPlayerController::BPEvent_PartyMemberJoined(const struct FMainMenuPartyMember& PartyMember)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PartyMemberJoined");

	AMenuPlayerController_BPEvent_PartyMemberJoined_Params params;
	params.PartyMember = PartyMember;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.MenuPlayerController.BPEvent_PartyInviteReceived
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString                 PlayerName                     (Parm, ZeroConstructor)

void AMenuPlayerController::BPEvent_PartyInviteReceived(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.MenuPlayerController.BPEvent_PartyInviteReceived");

	AMenuPlayerController_BPEvent_PartyInviteReceived_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SeatComponent.EnterVehicle
// (Final, Native, Public)
// Parameters:
// class AMainPlayerController*   PC                             (Parm, ZeroConstructor, IsPlainOldData)

void USeatComponent::EnterVehicle(class AMainPlayerController* PC)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SeatComponent.EnterVehicle");

	USeatComponent_EnterVehicle_Params params;
	params.PC = PC;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.ReadFriendsListCallbackProxy.ReadFriendsList
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// class APlayerController*       PlayerController               (Parm, ZeroConstructor, IsPlainOldData)
// class UReadFriendsListCallbackProxy* ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class UReadFriendsListCallbackProxy* UReadFriendsListCallbackProxy::STATIC_ReadFriendsList(class UObject* WorldContextObject, class APlayerController* PlayerController)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.ReadFriendsListCallbackProxy.ReadFriendsList");

	UReadFriendsListCallbackProxy_ReadFriendsList_Params params;
	params.WorldContextObject = WorldContextObject;
	params.PlayerController = PlayerController;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.SetFloatPropertyByName
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UObject*                 Object                         (Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   PropertyName                   (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetFloatPropertyByName(class UObject* Object, const struct FName& PropertyName, float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetFloatPropertyByName");

	USettingsFunctionLibrary_SetFloatPropertyByName_Params params;
	params.Object = Object;
	params.PropertyName = PropertyName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetCVarString
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// struct FString                 Value                          (Parm, ZeroConstructor)

void USettingsFunctionLibrary::STATIC_SetCVarString(const struct FString& CVarName, const struct FString& Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetCVarString");

	USettingsFunctionLibrary_SetCVarString_Params params;
	params.CVarName = CVarName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetCVarInt
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// int                            Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetCVarInt(const struct FString& CVarName, int Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetCVarInt");

	USettingsFunctionLibrary_SetCVarInt_Params params;
	params.CVarName = CVarName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetCVarFloat
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetCVarFloat(const struct FString& CVarName, float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetCVarFloat");

	USettingsFunctionLibrary_SetCVarFloat_Params params;
	params.CVarName = CVarName;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetCVarBool
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// bool                           bValue                         (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetCVarBool(const struct FString& CVarName, bool bValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetCVarBool");

	USettingsFunctionLibrary_SetCVarBool_Params params;
	params.CVarName = CVarName;
	params.bValue = bValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetConfigString
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// struct FString                 Value                          (Parm, ZeroConstructor)

void USettingsFunctionLibrary::STATIC_SetConfigString(const struct FString& Section, const struct FString& Key, const struct FString& Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetConfigString");

	USettingsFunctionLibrary_SetConfigString_Params params;
	params.Section = Section;
	params.Key = Key;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetConfigInt
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// int                            Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetConfigInt(const struct FString& Section, const struct FString& Key, int Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetConfigInt");

	USettingsFunctionLibrary_SetConfigInt_Params params;
	params.Section = Section;
	params.Key = Key;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetConfigFloat
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// float                          Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetConfigFloat(const struct FString& Section, const struct FString& Key, float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetConfigFloat");

	USettingsFunctionLibrary_SetConfigFloat_Params params;
	params.Section = Section;
	params.Key = Key;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SetConfigBool
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// bool                           Value                          (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SetConfigBool(const struct FString& Section, const struct FString& Key, bool Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SetConfigBool");

	USettingsFunctionLibrary_SetConfigBool_Params params;
	params.Section = Section;
	params.Key = Key;
	params.Value = Value;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.SaveObjectConfig
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 Object                         (Parm, ZeroConstructor, IsPlainOldData)

void USettingsFunctionLibrary::STATIC_SaveObjectConfig(class UObject* Object)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.SaveObjectConfig");

	USettingsFunctionLibrary_SaveObjectConfig_Params params;
	params.Object = Object;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.SettingsFunctionLibrary.GetFloatPropertyByName
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 Object                         (Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   PropertyName                   (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USettingsFunctionLibrary::STATIC_GetFloatPropertyByName(class UObject* Object, const struct FName& PropertyName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetFloatPropertyByName");

	USettingsFunctionLibrary_GetFloatPropertyByName_Params params;
	params.Object = Object;
	params.PropertyName = PropertyName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetCVarString
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USettingsFunctionLibrary::STATIC_GetCVarString(const struct FString& CVarName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetCVarString");

	USettingsFunctionLibrary_GetCVarString_Params params;
	params.CVarName = CVarName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetCVarInt
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int USettingsFunctionLibrary::STATIC_GetCVarInt(const struct FString& CVarName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetCVarInt");

	USettingsFunctionLibrary_GetCVarInt_Params params;
	params.CVarName = CVarName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetCVarFloat
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USettingsFunctionLibrary::STATIC_GetCVarFloat(const struct FString& CVarName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetCVarFloat");

	USettingsFunctionLibrary_GetCVarFloat_Params params;
	params.CVarName = CVarName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetCVarBool
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 CVarName                       (Parm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USettingsFunctionLibrary::STATIC_GetCVarBool(const struct FString& CVarName)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetCVarBool");

	USettingsFunctionLibrary_GetCVarBool_Params params;
	params.CVarName = CVarName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetConfigString
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USettingsFunctionLibrary::STATIC_GetConfigString(const struct FString& Section, const struct FString& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetConfigString");

	USettingsFunctionLibrary_GetConfigString_Params params;
	params.Section = Section;
	params.Key = Key;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetConfigInt
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int USettingsFunctionLibrary::STATIC_GetConfigInt(const struct FString& Section, const struct FString& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetConfigInt");

	USettingsFunctionLibrary_GetConfigInt_Params params;
	params.Section = Section;
	params.Key = Key;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetConfigFloat
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USettingsFunctionLibrary::STATIC_GetConfigFloat(const struct FString& Section, const struct FString& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetConfigFloat");

	USettingsFunctionLibrary_GetConfigFloat_Params params;
	params.Section = Section;
	params.Key = Key;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SettingsFunctionLibrary.GetConfigBool
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 Section                        (Parm, ZeroConstructor)
// struct FString                 Key                            (Parm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USettingsFunctionLibrary::STATIC_GetConfigBool(const struct FString& Section, const struct FString& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SettingsFunctionLibrary.GetConfigBool");

	USettingsFunctionLibrary_GetConfigBool_Params params;
	params.Section = Section;
	params.Key = Key;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function IONBranch.SpawnCapsule.InitClientCapsule
// (Event, Public, BlueprintEvent)

void ASpawnCapsule::InitClientCapsule()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.SpawnCapsule.InitClientCapsule");

	ASpawnCapsule_InitClientCapsule_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function IONBranch.VehicleBase.ServerExitVehicle
// (Net, NetReliable, Native, Event, Public, NetServer, NetValidate)

void AVehicleBase::ServerExitVehicle()
{
	static auto fn = UObject::FindObject<UFunction>("Function IONBranch.VehicleBase.ServerExitVehicle");

	AVehicleBase_ServerExitVehicle_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
