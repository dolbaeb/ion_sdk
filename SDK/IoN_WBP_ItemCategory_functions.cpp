// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ItemCategory.WBP_ItemCategory_C.SetSelectionState
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bSelected                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ItemCategory_C::SetSelectionState(bool bSelected)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.SetSelectionState");

	UWBP_ItemCategory_C_SetSelectionState_Params params;
	params.bSelected = bSelected;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemCategory.WBP_ItemCategory_C.BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ItemCategory_C::BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature");

	UWBP_ItemCategory_C_BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemCategory.WBP_ItemCategory_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ItemCategory_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.Construct");

	UWBP_ItemCategory_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemCategory.WBP_ItemCategory_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ItemCategory_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.PreConstruct");

	UWBP_ItemCategory_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemCategory.WBP_ItemCategory_C.ExecuteUbergraph_WBP_ItemCategory
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ItemCategory_C::ExecuteUbergraph_WBP_ItemCategory(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.ExecuteUbergraph_WBP_ItemCategory");

	UWBP_ItemCategory_C_ExecuteUbergraph_WBP_ItemCategory_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemCategory.WBP_ItemCategory_C.BtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ItemCategory_C::BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemCategory.WBP_ItemCategory_C.BtnClicked__DelegateSignature");

	UWBP_ItemCategory_C_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
