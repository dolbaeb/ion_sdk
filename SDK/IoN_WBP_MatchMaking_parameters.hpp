#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchMaking.WBP_MatchMaking_C.GetVisibility_Throbber
struct UWBP_MatchMaking_C_GetVisibility_Throbber_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_MatchMaking.WBP_MatchMaking_C.UpdatePlayButtonImage
struct UWBP_MatchMaking_C_UpdatePlayButtonImage_Params
{
};

// Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseEnter
struct UWBP_MatchMaking_C_OnMouseEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_MatchMaking.WBP_MatchMaking_C.OnMouseLeave
struct UWBP_MatchMaking_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_MatchMaking.WBP_MatchMaking_C.ExecuteUbergraph_WBP_MatchMaking
struct UWBP_MatchMaking_C_ExecuteUbergraph_WBP_MatchMaking_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
