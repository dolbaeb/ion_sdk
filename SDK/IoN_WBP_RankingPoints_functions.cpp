// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_NextRank_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_RankingPoints_C::Get_TextBlock_NextRank_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_NextRank_Text_1");

	UWBP_RankingPoints_C_Get_TextBlock_NextRank_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_PreviousRank_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_RankingPoints_C::Get_TextBlock_PreviousRank_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_PreviousRank_Visibility_1");

	UWBP_RankingPoints_C_Get_TextBlock_PreviousRank_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.GetText_PreviousRank
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_RankingPoints_C::GetText_PreviousRank()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.GetText_PreviousRank");

	UWBP_RankingPoints_C_GetText_PreviousRank_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.GetVisibility_NextRank
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_RankingPoints_C::GetVisibility_NextRank()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.GetVisibility_NextRank");

	UWBP_RankingPoints_C_GetVisibility_NextRank_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.GetPercent_Progress
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_RankingPoints_C::GetPercent_Progress()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.GetPercent_Progress");

	UWBP_RankingPoints_C_GetPercent_Progress_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_RankingPoints
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_RankingPoints_C::Get_TextBlock_RankingPoints()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_RankingPoints");

	UWBP_RankingPoints_C_Get_TextBlock_RankingPoints_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateColor             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateColor UWBP_RankingPoints_C::Get_SkillGainText_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_ColorAndOpacity_1");

	UWBP_RankingPoints_C_Get_SkillGainText_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_RankingPoints_C::Get_SkillGainText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_Text_1");

	UWBP_RankingPoints_C_Get_SkillGainText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.SetRankState
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSRankDisplayState      RankState                      (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_RankingPoints_C::SetRankState(const struct FSRankDisplayState& RankState)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.SetRankState");

	UWBP_RankingPoints_C_SetRankState_Params params;
	params.RankState = RankState;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Appear
// (BlueprintCallable, BlueprintEvent)

void UWBP_RankingPoints_C::Appear()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Appear");

	UWBP_RankingPoints_C_Appear_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_RankingPoints_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.Construct");

	UWBP_RankingPoints_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.StartAnimatingPoints
// (BlueprintCallable, BlueprintEvent)

void UWBP_RankingPoints_C::StartAnimatingPoints()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.StartAnimatingPoints");

	UWBP_RankingPoints_C_StartAnimatingPoints_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.AnimatePoints
// (BlueprintCallable, BlueprintEvent)

void UWBP_RankingPoints_C::AnimatePoints()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.AnimatePoints");

	UWBP_RankingPoints_C_AnimatePoints_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.ExecuteUbergraph_WBP_RankingPoints
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RankingPoints_C::ExecuteUbergraph_WBP_RankingPoints(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.ExecuteUbergraph_WBP_RankingPoints");

	UWBP_RankingPoints_C_ExecuteUbergraph_WBP_RankingPoints_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RankingPoints.WBP_RankingPoints_C.ProgressFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_RankingPoints_C::ProgressFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RankingPoints.WBP_RankingPoints_C.ProgressFinished__DelegateSignature");

	UWBP_RankingPoints_C_ProgressFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
