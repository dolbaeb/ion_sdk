// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function Slv_Loadout_02.Slv_Loadout_02_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ASlv_Loadout_02_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function Slv_Loadout_02.Slv_Loadout_02_C.ReceiveBeginPlay");

	ASlv_Loadout_02_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function Slv_Loadout_02.Slv_Loadout_02_C.ExecuteUbergraph_Slv_Loadout_02
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ASlv_Loadout_02_C::ExecuteUbergraph_Slv_Loadout_02(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function Slv_Loadout_02.Slv_Loadout_02_C.ExecuteUbergraph_Slv_Loadout_02");

	ASlv_Loadout_02_C_ExecuteUbergraph_Slv_Loadout_02_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
