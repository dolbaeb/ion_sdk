// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWB_ScalabilitySetting_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.Construct");

	UWB_ScalabilitySetting_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ScalabilitySetting_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.PreConstruct");

	UWB_ScalabilitySetting_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.ExecuteUbergraph_WB_ScalabilitySetting
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ScalabilitySetting_C::ExecuteUbergraph_WB_ScalabilitySetting(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.ExecuteUbergraph_WB_ScalabilitySetting");

	UWB_ScalabilitySetting_C_ExecuteUbergraph_WB_ScalabilitySetting_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
