#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.UserConstructionScript
struct ABP_Sako85_Bullet_Elite_C_UserConstructionScript_Params
{
};

// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveBeginPlay
struct ABP_Sako85_Bullet_Elite_C_ReceiveBeginPlay_Params
{
};

// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveEndPlay
struct ABP_Sako85_Bullet_Elite_C_ReceiveEndPlay_Params
{
	TEnumAsByte<EEndPlayReason>*                       EndPlayReason;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ExecuteUbergraph_BP_Sako85_Bullet_Elite
struct ABP_Sako85_Bullet_Elite_C_ExecuteUbergraph_BP_Sako85_Bullet_Elite_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
