// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_IonLegArmor.BP_IonLegArmor_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonLegArmor_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonLegArmor.BP_IonLegArmor_C.UserConstructionScript");

	ABP_IonLegArmor_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_AttachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonLegArmor_C::BPEvent_AttachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_AttachWearable");

	ABP_IonLegArmor_C_BPEvent_AttachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_DetachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonLegArmor_C::BPEvent_DetachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_DetachWearable");

	ABP_IonLegArmor_C_BPEvent_DetachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonLegArmor.BP_IonLegArmor_C.ExecuteUbergraph_BP_IonLegArmor
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IonLegArmor_C::ExecuteUbergraph_BP_IonLegArmor(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonLegArmor.BP_IonLegArmor_C.ExecuteUbergraph_BP_IonLegArmor");

	ABP_IonLegArmor_C_ExecuteUbergraph_BP_IonLegArmor_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
