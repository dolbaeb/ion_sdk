#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.PreConstruct
struct UWBP_DebugMenuButton_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Construct
struct UWBP_DebugMenuButton_C_Construct_Params
{
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
struct UWBP_DebugMenuButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Reset
struct UWBP_DebugMenuButton_C_Reset_Params
{
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Select
struct UWBP_DebugMenuButton_C_Select_Params
{
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.ExecuteUbergraph_WBP_DebugMenuButton
struct UWBP_DebugMenuButton_C_ExecuteUbergraph_WBP_DebugMenuButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.On Clicked__DelegateSignature
struct UWBP_DebugMenuButton_C_On_Clicked__DelegateSignature_Params
{
	class UWBP_DebugMenuButton_C*                      Object_To_Ignore;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
