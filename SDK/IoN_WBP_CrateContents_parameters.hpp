#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CrateContents.WBP_CrateContents_C.SetCrate
struct UWBP_CrateContents_C_SetCrate_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateContents.WBP_CrateContents_C.Construct
struct UWBP_CrateContents_C_Construct_Params
{
};

// Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature
struct UWBP_CrateContents_C_BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature_Params
{
};

// Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature
struct UWBP_CrateContents_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_CrateContents.WBP_CrateContents_C.ExecuteUbergraph_WBP_CrateContents
struct UWBP_CrateContents_C_ExecuteUbergraph_WBP_CrateContents_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrateContents.WBP_CrateContents_C.RedeemCrate__DelegateSignature
struct UWBP_CrateContents_C_RedeemCrate__DelegateSignature_Params
{
	class UIONSteamCrate*                              Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
