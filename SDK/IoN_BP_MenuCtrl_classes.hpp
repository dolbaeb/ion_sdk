#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_MenuCtrl.BP_MenuCtrl_C
// 0x0058 (0x0A50 - 0x09F8)
class ABP_MenuCtrl_C : public AMenuPlayerController
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x09F8(0x0008) (Transient, DuplicateTransient)
	class UGameSparksScriptData*                       PlayerLoadOutData_1;                                      // 0x0A00(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FScriptMulticastDelegate                    OnPartyMemeberJoined;                                     // 0x0A08(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnPartyMemberLeave;                                       // 0x0A18(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnPlayersReadyStatusesChanged;                            // 0x0A28(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnPopulateCustomServers;                                  // 0x0A38(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	class UGameSparksScriptData*                       NewVar_1;                                                 // 0x0A48(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_MenuCtrl.BP_MenuCtrl_C");
		return ptr;
	}


	void UserConstructionScript();
	void BPEvent_ShowOKDialog(struct FString* Message);
	void ExecuteUbergraph_BP_MenuCtrl(int EntryPoint);
	void OnPopulateCustomServers__DelegateSignature(TArray<struct FIONServerInfo>* servers);
	void OnPlayersReadyStatusesChanged__DelegateSignature();
	void OnPartyMemberLeave__DelegateSignature(const struct FMainMenuPartyMember& PartyMember);
	void OnPartyMemeberJoined__DelegateSignature(const struct FMainMenuPartyMember& PartyMember);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
