#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956
struct UABP_M1014_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956_Params
{
};

// Function ABP_M1014.ABP_M1014_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C
struct UABP_M1014_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C_Params
{
};

// Function ABP_M1014.ABP_M1014_C.BlueprintUpdateAnimation
struct UABP_M1014_C_BlueprintUpdateAnimation_Params
{
	float*                                             DeltaTimeX;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function ABP_M1014.ABP_M1014_C.ExecuteUbergraph_ABP_M1014
struct UABP_M1014_C_ExecuteUbergraph_ABP_M1014_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
