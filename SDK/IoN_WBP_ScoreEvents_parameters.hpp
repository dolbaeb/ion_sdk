#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.GetVisibility_Developer
struct UWBP_ScoreEvents_C_GetVisibility_Developer_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.Get_VictimName_Text_1
struct UWBP_ScoreEvents_C_Get_VictimName_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKillConfirmed
struct UWBP_ScoreEvents_C_OnKillConfirmed_Params
{
	class AIONPlayerState*                             Victim;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHeadshot;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKnockout
struct UWBP_ScoreEvents_C_OnKnockout_Params
{
	class AIONPlayerState*                             Victim;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHeadshot;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnScoreStringUpdated
struct UWBP_ScoreEvents_C_OnScoreStringUpdated_Params
{
	struct FString*                                    NewString;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.Construct
struct UWBP_ScoreEvents_C_Construct_Params
{
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnAccumulatedScoreUpdated
struct UWBP_ScoreEvents_C_OnAccumulatedScoreUpdated_Params
{
	int*                                               NewScore;                                                 // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.ExecuteUbergraph_WBP_ScoreEvents
struct UWBP_ScoreEvents_C_ExecuteUbergraph_WBP_ScoreEvents_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
