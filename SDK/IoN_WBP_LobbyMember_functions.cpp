// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LobbyMember.WBP_LobbyMember_C.On_AvatarImage_MouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_LobbyMember_C::On_AvatarImage_MouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.On_AvatarImage_MouseButtonDown_1");

	UWBP_LobbyMember_C_On_AvatarImage_MouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_LobbyMember_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.Construct");

	UWBP_LobbyMember_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LobbyMember_C::BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature");

	UWBP_LobbyMember_C_BndEvt__MemberButton_K2Node_ComponentBoundEvent_19_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LobbyMember_C::BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature");

	UWBP_LobbyMember_C_BndEvt__MemberButton_K2Node_ComponentBoundEvent_32_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.ExecuteUbergraph_WBP_LobbyMember
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LobbyMember_C::ExecuteUbergraph_WBP_LobbyMember(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.ExecuteUbergraph_WBP_LobbyMember");

	UWBP_LobbyMember_C_ExecuteUbergraph_WBP_LobbyMember_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.PartyAddBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_LobbyMember_C::PartyAddBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.PartyAddBtnClicked__DelegateSignature");

	UWBP_LobbyMember_C_PartyAddBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LobbyMember.WBP_LobbyMember_C.PartyMemberClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerState*            MemberPlayerState              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LobbyMember_C::PartyMemberClicked__DelegateSignature(class APlayerState* MemberPlayerState)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LobbyMember.WBP_LobbyMember_C.PartyMemberClicked__DelegateSignature");

	UWBP_LobbyMember_C_PartyMemberClicked__DelegateSignature_Params params;
	params.MemberPlayerState = MemberPlayerState;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
