// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CrateContents.WBP_CrateContents_C.SetCrate
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateContents_C::SetCrate(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.SetCrate");

	UWBP_CrateContents_C_SetCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateContents.WBP_CrateContents_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CrateContents_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.Construct");

	UWBP_CrateContents_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateContents_C::BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature");

	UWBP_CrateContents_C_BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_191_RedeemCratePressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_CrateContents_C::BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature");

	UWBP_CrateContents_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateContents.WBP_CrateContents_C.ExecuteUbergraph_WBP_CrateContents
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateContents_C::ExecuteUbergraph_WBP_CrateContents(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.ExecuteUbergraph_WBP_CrateContents");

	UWBP_CrateContents_C_ExecuteUbergraph_WBP_CrateContents_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrateContents.WBP_CrateContents_C.RedeemCrate__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrateContents_C::RedeemCrate__DelegateSignature(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrateContents.WBP_CrateContents_C.RedeemCrate__DelegateSignature");

	UWBP_CrateContents_C_RedeemCrate__DelegateSignature_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
