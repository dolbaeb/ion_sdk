#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ClampMapWithinBounds
struct UWBP_MapGrid_Spectator_C_ClampMapWithinBounds_Params
{
	struct FVector2D                                   New_Location;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FVector2D                                   Clamped_Location;                                         // (Parm, OutParm, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.UpdateFieldMaterial
struct UWBP_MapGrid_Spectator_C_UpdateFieldMaterial_Params
{
	class UMaterialInstanceDynamic*                    Mid;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       ParamName;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     Location;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float                                              Radius;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Construct
struct UWBP_MapGrid_Spectator_C_Construct_Params
{
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Tick
struct UWBP_MapGrid_Spectator_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.On Player Left
struct UWBP_MapGrid_Spectator_C_On_Player_Left_Params
{
	class UWBP_MapArrow_Spectator_C*                   Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_MapGrid_Spectator_C_BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Zoom Map
struct UWBP_MapGrid_Spectator_C_Zoom_Map_Params
{
	float                                              Zoom_Amount;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Drag Map
struct UWBP_MapGrid_Spectator_C_Drag_Map_Params
{
	struct FVector2D                                   Drag_Delta;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Update Zoom Scale Text
struct UWBP_MapGrid_Spectator_C_Update_Zoom_Scale_Text_Params
{
	struct FVector2D                                   Scale;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
};

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ExecuteUbergraph_WBP_MapGrid_Spectator
struct UWBP_MapGrid_Spectator_C_ExecuteUbergraph_WBP_MapGrid_Spectator_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
