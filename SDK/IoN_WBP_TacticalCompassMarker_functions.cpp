// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Get_DistanceText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_TacticalCompassMarker_C::Get_DistanceText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Get_DistanceText_Text_1");

	UWBP_TacticalCompassMarker_C_Get_DistanceText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TacticalCompassMarker_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.PreConstruct");

	UWBP_TacticalCompassMarker_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_TacticalCompassMarker_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Construct");

	UWBP_TacticalCompassMarker_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TacticalCompassMarker_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.Tick");

	UWBP_TacticalCompassMarker_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.ExecuteUbergraph_WBP_TacticalCompassMarker
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TacticalCompassMarker_C::ExecuteUbergraph_WBP_TacticalCompassMarker(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompassMarker.WBP_TacticalCompassMarker_C.ExecuteUbergraph_WBP_TacticalCompassMarker");

	UWBP_TacticalCompassMarker_C_ExecuteUbergraph_WBP_TacticalCompassMarker_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
