#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_MMChar.ABP_MMChar_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA
struct UABP_MMChar_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA_Params
{
};

// Function ABP_MMChar.ABP_MMChar_C.ExecuteUbergraph_ABP_MMChar
struct UABP_MMChar_C_ExecuteUbergraph_ABP_MMChar_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
