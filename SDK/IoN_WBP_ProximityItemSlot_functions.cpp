// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_ProximityItemSlot_C::Get_Image_Status_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Visibility_1");

	UWBP_ProximityItemSlot_C_Get_Image_Status_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_ProximityItemSlot_C::Get_Image_Status_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_Image_Status_Brush_1");

	UWBP_ProximityItemSlot_C_Get_Image_Status_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.RemoveItemFromStack
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONItem*                ItemToRemove                   (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
// bool                           bWasRemoved                    (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityItemSlot_C::RemoveItemFromStack(class AIONItem** ItemToRemove, bool* bWasRemoved)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.RemoveItemFromStack");

	UWBP_ProximityItemSlot_C_RemoveItemFromStack_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (ItemToRemove != nullptr)
		*ItemToRemove = params.ItemToRemove;
	if (bWasRemoved != nullptr)
		*bWasRemoved = params.bWasRemoved;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AddItemToStack
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONItem*                NewItem                        (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UWBP_ProximityItemSlot_C::AddItemToStack(class AIONItem** NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AddItemToStack");

	UWBP_ProximityItemSlot_C_AddItemToStack_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (NewItem != nullptr)
		*NewItem = params.NewItem;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_ProximityItemSlot_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.AcceptsDropForItem");

	UWBP_ProximityItemSlot_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_ProximityItemSlot_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnDrop");

	UWBP_ProximityItemSlot_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_ProximityItemSlot_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.GetCurrentItem");

	UWBP_ProximityItemSlot_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmountText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ProximityItemSlot_C::Get_AmountText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmountText_Text_1");

	UWBP_ProximityItemSlot_C_Get_AmountText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmmoText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ProximityItemSlot_C::Get_AmmoText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Get_AmmoText_Text_1");

	UWBP_ProximityItemSlot_C_Get_AmmoText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_ProximityItemSlot_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemChanged");

	UWBP_ProximityItemSlot_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityItemSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Tick");

	UWBP_ProximityItemSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ProximityItemSlot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.Construct");

	UWBP_ProximityItemSlot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_ProximityItemSlot_C::OnItemClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.OnItemClicked");

	UWBP_ProximityItemSlot_C_OnItemClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.ExecuteUbergraph_WBP_ProximityItemSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityItemSlot_C::ExecuteUbergraph_WBP_ProximityItemSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityItemSlot.WBP_ProximityItemSlot_C.ExecuteUbergraph_WBP_ProximityItemSlot");

	UWBP_ProximityItemSlot_C_ExecuteUbergraph_WBP_ProximityItemSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
