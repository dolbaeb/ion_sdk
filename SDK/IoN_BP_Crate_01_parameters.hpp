#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_Crate_01.BP_Crate_01_C.SetCrate
struct ABP_Crate_01_C_SetCrate_Params
{
	struct FIONSteamInventoryItem                      Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_Crate_01.BP_Crate_01_C.UserConstructionScript
struct ABP_Crate_01_C_UserConstructionScript_Params
{
};

// Function BP_Crate_01.BP_Crate_01_C.PlayOpen
struct ABP_Crate_01_C_PlayOpen_Params
{
};

// Function BP_Crate_01.BP_Crate_01_C.ExecuteUbergraph_BP_Crate_01
struct ABP_Crate_01_C_ExecuteUbergraph_BP_Crate_01_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
