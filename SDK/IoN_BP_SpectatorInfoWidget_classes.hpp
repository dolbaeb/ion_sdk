#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C
// 0x0008 (0x0340 - 0x0338)
class ABP_SpectatorInfoWidget_C : public AIONSpectatorInfo
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0338(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C");
		return ptr;
	}


	void UserConstructionScript();
	void SetupWidget();
	void OnViewModeChanged(bool* bMinimal);
	void ExecuteUbergraph_BP_SpectatorInfoWidget(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
