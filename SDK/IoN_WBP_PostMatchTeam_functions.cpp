// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchTeam_C::BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchTeam_C_BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Set Minimized
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Minimize                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchTeam_C::Set_Minimized(bool Minimize)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Set Minimized");

	UWBP_PostMatchTeam_C_Set_Minimized_Params params;
	params.Minimize = Minimize;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PostMatchTeam_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Construct");

	UWBP_PostMatchTeam_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Fill With Players
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FPlayerMatchResult> Players                        (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_PostMatchTeam_C::Fill_With_Players(TArray<struct FPlayerMatchResult> Players)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Fill With Players");

	UWBP_PostMatchTeam_C_Fill_With_Players_Params params;
	params.Players = Players;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Calculate Team Totals
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTeam_C::Calculate_Team_Totals()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Calculate Team Totals");

	UWBP_PostMatchTeam_C_Calculate_Team_Totals_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Update Team Values
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchTeam_C::Update_Team_Values()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Update Team Values");

	UWBP_PostMatchTeam_C_Update_Team_Values_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.ExecuteUbergraph_WBP_PostMatchTeam
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchTeam_C::ExecuteUbergraph_WBP_PostMatchTeam(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.ExecuteUbergraph_WBP_PostMatchTeam");

	UWBP_PostMatchTeam_C_ExecuteUbergraph_WBP_PostMatchTeam_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
