#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_M1014.ABP_M1014_C
// 0x05F2 (0x09C2 - 0x03D0)
class UABP_M1014_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_4EF6C77F4109B4F81656798535F2963C;      // 0x03D8(0x0048)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_66D23FE44885B94277AC849E721D9727;      // 0x0420(0x0068)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C;// 0x0488(0x00D0)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_A4A2E9014AC8F4B47403A3A6018BACE6;// 0x0558(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_F67178284906D1D921DF7BA5F5812B90;// 0x05A0(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_9653464B4D517C38F17FCD8A43F0F47A;// 0x0658(0x00B8)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_9DE6F0124673C2C436A6088D2C09A64E;// 0x0710(0x0048)
	struct FAnimNode_TwoWayBlend                       AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956;// 0x0758(0x0078)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_5FDF890E41D0C0C36285F8A8DC553C27;// 0x07D0(0x0038)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_E50C54FF40FB9DF8A5FB1EA4DAF9A3F6;// 0x0808(0x0070)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_1ABCEC72494D03137B526DBE398059AC;// 0x0878(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_00DBC00943299398F4DC67B9FBAF5BA3;// 0x0920(0x0050)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_19893E154A1291BD369D13A371286C12;// 0x0970(0x0050)
	bool                                               Is_Montage_Playing;                                       // 0x09C0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bRoundChambered;                                          // 0x09C1(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_M1014.ABP_M1014_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_TwoWayBlend_3B540E8E418EBFEFC1C46EAB6BA12956();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M1014_AnimGraphNode_BlendListByBool_7B7231384BFF9B9B7087D48F71E4272C();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void ExecuteUbergraph_ABP_M1014(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
