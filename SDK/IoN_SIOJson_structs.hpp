#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// Enum SIOJson.ESIOJson
enum class ESIOJson : uint8_t
{
	ESIOJson__None                 = 0,
	ESIOJson__Null                 = 1,
	ESIOJson__String               = 2,
	ESIOJson__Number               = 3,
	ESIOJson__Boolean              = 4,
	ESIOJson__Array                = 5,
	ESIOJson__Object               = 6,
	ESIOJson__Binary               = 7,
	ESIOJson__ESIOJson_MAX         = 8
};


// Enum SIOJson.ESIORequestStatus
enum class ESIORequestStatus : uint8_t
{
	ESIORequestStatus__NotStarted  = 0,
	ESIORequestStatus__Processing  = 1,
	ESIORequestStatus__Failed      = 2,
	ESIORequestStatus__Failed_ConnectionError = 3,
	ESIORequestStatus__Succeeded   = 4,
	ESIORequestStatus__ESIORequestStatus_MAX = 5
};


// Enum SIOJson.ESIORequestContentType
enum class ESIORequestContentType : uint8_t
{
	ESIORequestContentType__x_www_form_urlencoded_url = 0,
	ESIORequestContentType__x_www_form_urlencoded_body = 1,
	ESIORequestContentType__json   = 2,
	ESIORequestContentType__binary = 3,
	ESIORequestContentType__ESIORequestContentType_MAX = 4
};


// Enum SIOJson.ESIORequestVerb
enum class ESIORequestVerb : uint8_t
{
	ESIORequestVerb__GET           = 0,
	ESIORequestVerb__POST          = 1,
	ESIORequestVerb__PUT           = 2,
	ESIORequestVerb__DEL           = 3,
	ESIORequestVerb__CUSTOM        = 4,
	ESIORequestVerb__ESIORequestVerb_MAX = 5
};



//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// ScriptStruct SIOJson.SIOJCallResponse
// 0x0030
struct FSIOJCallResponse
{
	class USIOJRequestJSON*                            Request;                                                  // 0x0000(0x0008) (ZeroConstructor, IsPlainOldData)
	class UObject*                                     WorldContextObject;                                       // 0x0008(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FScriptDelegate                             Callback;                                                 // 0x0010(0x0014) (ZeroConstructor, InstancedReference)
	unsigned char                                      UnknownData00[0x10];                                      // 0x0020(0x0010) MISSED OFFSET
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
