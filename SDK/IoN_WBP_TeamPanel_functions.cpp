// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_TeamPanel.WBP_TeamPanel_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_TeamPanel_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamPanel.WBP_TeamPanel_C.Construct");

	UWBP_TeamPanel_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamPanel.WBP_TeamPanel_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TeamPanel_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamPanel.WBP_TeamPanel_C.Tick");

	UWBP_TeamPanel_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamPanel.WBP_TeamPanel_C.OnPlayerLeft_Event_1
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_TeamMateInfo_C*     Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_TeamPanel_C::OnPlayerLeft_Event_1(class UWBP_TeamMateInfo_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamPanel.WBP_TeamPanel_C.OnPlayerLeft_Event_1");

	UWBP_TeamPanel_C_OnPlayerLeft_Event_1_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamPanel.WBP_TeamPanel_C.ExecuteUbergraph_WBP_TeamPanel
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TeamPanel_C::ExecuteUbergraph_WBP_TeamPanel(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamPanel.WBP_TeamPanel_C.ExecuteUbergraph_WBP_TeamPanel");

	UWBP_TeamPanel_C_ExecuteUbergraph_WBP_TeamPanel_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
