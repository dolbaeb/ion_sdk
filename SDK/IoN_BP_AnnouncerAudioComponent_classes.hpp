#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C
// 0x0009 (0x04F9 - 0x04F0)
class UBP_AnnouncerAudioComponent_C : public UAnnouncerAudioComponent
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x04F0(0x0008) (Transient, DuplicateTransient)
	bool                                               bMatchEnded;                                              // 0x04F8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C");
		return ptr;
	}


	void OnBattleRoyaleStarted();
	void OnBattleRoyaleCountdownStarted();
	void OnMatchEnded();
	void OnFinalConvergenceStarted();
	void ExecuteUbergraph_BP_AnnouncerAudioComponent(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
