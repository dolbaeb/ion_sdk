#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.On_BackgroundImage_MouseButtonDown_1
struct UWBP_EliminationScreen_C_On_BackgroundImage_MouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Construct
struct UWBP_EliminationScreen_C_Construct_Params
{
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature
struct UWBP_EliminationScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature
struct UWBP_EliminationScreen_C_BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Battle Royale
struct UWBP_EliminationScreen_C_Setup_Battle_Royale_Params
{
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.OnAnimationFinished
struct UWBP_EliminationScreen_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Gun Game
struct UWBP_EliminationScreen_C_Setup_Gun_Game_Params
{
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Tick
struct UWBP_EliminationScreen_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.ExecuteUbergraph_WBP_EliminationScreen
struct UWBP_EliminationScreen_C_ExecuteUbergraph_WBP_EliminationScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
