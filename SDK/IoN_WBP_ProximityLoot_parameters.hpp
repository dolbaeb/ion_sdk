#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.AddItemToSlots
struct UWBP_ProximityLoot_C_AddItemToSlots_Params
{
	class AIONItem*                                    Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetListForItem
struct UWBP_ProximityLoot_C_GetListForItem_Params
{
	class AIONItem*                                    Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class UVerticalBox*                                ListBox;                                                  // (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetCurrentItems
struct UWBP_ProximityLoot_C_GetCurrentItems_Params
{
	TArray<class AIONItem*>                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.SetProximityItems
struct UWBP_ProximityLoot_C_SetProximityItems_Params
{
	TArray<class AIONItem*>                            ProximityItems;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.Construct
struct UWBP_ProximityLoot_C_Construct_Params
{
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.Tick
struct UWBP_ProximityLoot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.ExecuteUbergraph_WBP_ProximityLoot
struct UWBP_ProximityLoot_C_ExecuteUbergraph_WBP_ProximityLoot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
