#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct STeamColors.STeamColors
// 0x0020
struct FSTeamColors
{
	struct FLinearColor                                PrimaryColor_5_1EF9EEF4410A876C6BD3548C7C2F837E;          // 0x0000(0x0010) (Edit, BlueprintVisible, IsPlainOldData)
	struct FLinearColor                                SecondaryColor_6_69E4D10E4672B58ADF8070B9AA495720;        // 0x0010(0x0010) (Edit, BlueprintVisible, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
