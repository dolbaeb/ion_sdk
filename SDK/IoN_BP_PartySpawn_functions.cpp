// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_PartySpawn.BP_PartySpawn_C.GetCharacter
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONCharacter*           PlayerCharacter                (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void ABP_PartySpawn_C::GetCharacter(class AIONCharacter** PlayerCharacter)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.GetCharacter");

	ABP_PartySpawn_C_GetCharacter_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (PlayerCharacter != nullptr)
		*PlayerCharacter = params.PlayerCharacter;
}


// Function BP_PartySpawn.BP_PartySpawn_C.SetPlayerDetails
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer      Player                         (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PartySpawn_C::SetPlayerDetails(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.SetPlayerDetails");

	ABP_PartySpawn_C_SetPlayerDetails_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.FillSlot
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer      Player                         (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PartySpawn_C::FillSlot(const struct FMatchmakingPlayer& Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.FillSlot");

	ABP_PartySpawn_C_FillSlot_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.ClearSlot
// (Public, BlueprintCallable, BlueprintEvent)

void ABP_PartySpawn_C::ClearSlot()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.ClearSlot");

	ABP_PartySpawn_C_ClearSlot_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.IsSlotAvailable
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           IsEmpty                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void ABP_PartySpawn_C::IsSlotAvailable(bool* IsEmpty)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.IsSlotAvailable");

	ABP_PartySpawn_C_IsSlotAvailable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (IsEmpty != nullptr)
		*IsEmpty = params.IsEmpty;
}


// Function BP_PartySpawn.BP_PartySpawn_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_PartySpawn_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.UserConstructionScript");

	ABP_PartySpawn_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.CreatePlayerPawn
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer*     Player                         (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PartySpawn_C::CreatePlayerPawn(struct FMatchmakingPlayer* Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.CreatePlayerPawn");

	ABP_PartySpawn_C_CreatePlayerPawn_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.DestroyPlayerPawn
// (Event, Public, BlueprintEvent)

void ABP_PartySpawn_C::DestroyPlayerPawn()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.DestroyPlayerPawn");

	ABP_PartySpawn_C_DestroyPlayerPawn_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.PlayerUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FMatchmakingPlayer*     Player                         (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_PartySpawn_C::PlayerUpdated(struct FMatchmakingPlayer* Player)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.PlayerUpdated");

	ABP_PartySpawn_C_PlayerUpdated_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.BPEvent_ArmorUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                NewArmor                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void ABP_PartySpawn_C::BPEvent_ArmorUpdated(struct FString* NewArmor)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.BPEvent_ArmorUpdated");

	ABP_PartySpawn_C_BPEvent_ArmorUpdated_Params params;
	params.NewArmor = NewArmor;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_PartySpawn.BP_PartySpawn_C.ExecuteUbergraph_BP_PartySpawn
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_PartySpawn_C::ExecuteUbergraph_BP_PartySpawn(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_PartySpawn.BP_PartySpawn_C.ExecuteUbergraph_BP_PartySpawn");

	ABP_PartySpawn_C_ExecuteUbergraph_BP_PartySpawn_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
