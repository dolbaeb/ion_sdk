#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Highlight Text
struct UWBP_PostMatchStats_C_Highlight_Text_Params
{
	class UTextBlock*                                  Text;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Unhighlight All Text
struct UWBP_PostMatchStats_C_Unhighlight_All_Text_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchStats_C_BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Update Result Text
struct UWBP_PostMatchStats_C_Update_Result_Text_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Change Teams View
struct UWBP_PostMatchStats_C_Change_Teams_View_Params
{
	bool                                               View_Teams;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Construct
struct UWBP_PostMatchStats_C_Construct_Params
{
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Tick
struct UWBP_PostMatchStats_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.ExecuteUbergraph_WBP_PostMatchStats
struct UWBP_PostMatchStats_C_ExecuteUbergraph_WBP_PostMatchStats_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
