// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenuSettingsButton_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.PreConstruct");

	UWBP_MainMenuSettingsButton_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuSettingsButton_C::BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature");

	UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_59_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuSettingsButton_C::BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_0_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuSettingsButton_C::BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenuSettingsButton_C_BndEvt__SettingsBtn_K2Node_ComponentBoundEvent_1_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.ExecuteUbergraph_WBP_MainMenuSettingsButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenuSettingsButton_C::ExecuteUbergraph_WBP_MainMenuSettingsButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.ExecuteUbergraph_WBP_MainMenuSettingsButton");

	UWBP_MainMenuSettingsButton_C_ExecuteUbergraph_WBP_MainMenuSettingsButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.OnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenuSettingsButton_C::OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuSettingsButton.WBP_MainMenuSettingsButton_C.OnClicked__DelegateSignature");

	UWBP_MainMenuSettingsButton_C_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
