// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Crosshair.WBP_Crosshair_C.Get Line Color
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_Crosshair_C::Get_Line_Color()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Get Line Color");

	UWBP_Crosshair_C_Get_Line_Color_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Get Dot Color
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_Crosshair_C::Get_Dot_Color()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Get Dot Color");

	UWBP_Crosshair_C_Get_Dot_Color_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Crosshair.WBP_Crosshair_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.PreConstruct");

	UWBP_Crosshair_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_Crosshair_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Construct");

	UWBP_Crosshair_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Tick");

	UWBP_Crosshair_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Update Reticle
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Spread                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::Update_Reticle(float Spread)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Update Reticle");

	UWBP_Crosshair_C_Update_Reticle_Params params;
	params.Spread = Spread;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Set Dot Opacity
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Opacity                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::Set_Dot_Opacity(float Opacity)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Set Dot Opacity");

	UWBP_Crosshair_C_Set_Dot_Opacity_Params params;
	params.Opacity = Opacity;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.Set Lines Opacity
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Opacity                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::Set_Lines_Opacity(float Opacity)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.Set Lines Opacity");

	UWBP_Crosshair_C_Set_Lines_Opacity_Params params;
	params.Opacity = Opacity;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Crosshair.WBP_Crosshair_C.ExecuteUbergraph_WBP_Crosshair
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Crosshair_C::ExecuteUbergraph_WBP_Crosshair(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Crosshair.WBP_Crosshair_C.ExecuteUbergraph_WBP_Crosshair");

	UWBP_Crosshair_C_ExecuteUbergraph_WBP_Crosshair_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
