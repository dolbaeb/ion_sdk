// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_MenuPawn.BP_MenuPawn_C.StartCrateAnim
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_CrateScreen_C*      CurrentCrateScreen             (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// float                          TotalDistance                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::StartCrateAnim(class UWBP_CrateScreen_C* CurrentCrateScreen, float TotalDistance)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.StartCrateAnim");

	ABP_MenuPawn_C_StartCrateAnim_Params params;
	params.CurrentCrateScreen = CurrentCrateScreen;
	params.TotalDistance = TotalDistance;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.CleanupWearableItem
// (Public, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::CleanupWearableItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.CleanupWearableItem");

	ABP_MenuPawn_C_CleanupWearableItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.DoWeaponRotation
// (Public, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::DoWeaponRotation()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.DoWeaponRotation");

	ABP_MenuPawn_C_DoWeaponRotation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.CleanupCrateItem
// (Public, BlueprintCallable, BlueprintEvent, Const)

void ABP_MenuPawn_C::CleanupCrateItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.CleanupCrateItem");

	ABP_MenuPawn_C_CleanupCrateItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.CleanupDisplayItems
// (Public, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::CleanupDisplayItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.CleanupDisplayItems");

	ABP_MenuPawn_C_CleanupDisplayItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.SwitchToSkinCam
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bSwitch                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::SwitchToSkinCam(bool bSwitch)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.SwitchToSkinCam");

	ABP_MenuPawn_C_SwitchToSkinCam_Params params;
	params.bSwitch = bSwitch;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.HideEquippedItems
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           HideItems                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::HideEquippedItems(bool HideItems)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.HideEquippedItems");

	ABP_MenuPawn_C_HideEquippedItems_Params params;
	params.HideItems = HideItems;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ApplyWeaponSkin
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONWeaponSkin*          NewSkin                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::ApplyWeaponSkin(class UIONWeaponSkin* NewSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ApplyWeaponSkin");

	ABP_MenuPawn_C_ApplyWeaponSkin_Params params;
	params.NewSkin = NewSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.TickObjectTransforms
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::TickObjectTransforms()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.TickObjectTransforms");

	ABP_MenuPawn_C_TickObjectTransforms_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ChangeZoom
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            ZoomLevel                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::ChangeZoom(int ZoomLevel)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ChangeZoom");

	ABP_MenuPawn_C_ChangeZoom_Params params;
	params.ZoomLevel = ZoomLevel;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ZoomIn
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           In                             (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::ZoomIn(bool In)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ZoomIn");

	ABP_MenuPawn_C_ZoomIn_Params params;
	params.In = In;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.TickCameraPosition
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::TickCameraPosition()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.TickCameraPosition");

	ABP_MenuPawn_C_TickCameraPosition_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.UserConstructionScript");

	ABP_MenuPawn_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_Ctrl+Shift_K_K2Node_InputKeyEvent_3
// (BlueprintEvent)
// Parameters:
// struct FKey                    Key                            (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuPawn_C::InpActEvt_Ctrl_Shift_K_K2Node_InputKeyEvent_3(const struct FKey& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_Ctrl+Shift_K_K2Node_InputKeyEvent_3");

	ABP_MenuPawn_C_InpActEvt_Ctrl_Shift_K_K2Node_InputKeyEvent_3_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2
// (BlueprintEvent)
// Parameters:
// struct FKey                    Key                            (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuPawn_C::InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2(const struct FKey& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2");

	ABP_MenuPawn_C_InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1
// (BlueprintEvent)
// Parameters:
// struct FKey                    Key                            (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuPawn_C::InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1(const struct FKey& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1");

	ABP_MenuPawn_C_InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_SkinsDebug
// (Event, Public, BlueprintEvent)
// Parameters:
// int*                           Mode                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::BPEvent_SkinsDebug(int* Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_SkinsDebug");

	ABP_MenuPawn_C_BPEvent_SkinsDebug_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1
// (BlueprintEvent)
// Parameters:
// float                          AxisValue                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1(float AxisValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1");

	ABP_MenuPawn_C_InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1_Params params;
	params.AxisValue = AxisValue;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4
// (BlueprintEvent)
// Parameters:
// float                          AxisValue                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4(float AxisValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4");

	ABP_MenuPawn_C_InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4_Params params;
	params.AxisValue = AxisValue;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_MenuPawn_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ReceiveBeginPlay");

	ABP_MenuPawn_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ReceiveTick
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaSeconds                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::ReceiveTick(float* DeltaSeconds)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ReceiveTick");

	ABP_MenuPawn_C_ReceiveTick_Params params;
	params.DeltaSeconds = DeltaSeconds;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.OpenCrateAnimation
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::OpenCrateAnimation()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.OpenCrateAnimation");

	ABP_MenuPawn_C_OpenCrateAnimation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectWeaponSkin
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONWeaponSkin**         WeaponSkin                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::BPEvent_InspectWeaponSkin(class UIONWeaponSkin** WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectWeaponSkin");

	ABP_MenuPawn_C_BPEvent_InspectWeaponSkin_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.LoadoutLoaded
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::LoadoutLoaded()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.LoadoutLoaded");

	ABP_MenuPawn_C_LoadoutLoaded_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ShowDefaultChar
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::ShowDefaultChar()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ShowDefaultChar");

	ABP_MenuPawn_C_ShowDefaultChar_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectCrate
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem* Crate                          (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_MenuPawn_C::BPEvent_InspectCrate(struct FIONSteamInventoryItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectCrate");

	ABP_MenuPawn_C_BPEvent_InspectCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.PlayGoldEffect
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::PlayGoldEffect()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.PlayGoldEffect");

	ABP_MenuPawn_C_PlayGoldEffect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.StopGoldEffect
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::StopGoldEffect()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.StopGoldEffect");

	ABP_MenuPawn_C_StopGoldEffect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectArmor
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONArmorSkin**          ArmorSkin                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::BPEvent_InspectArmor(class UIONArmorSkin** ArmorSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectArmor");

	ABP_MenuPawn_C_BPEvent_InspectArmor_Params params;
	params.ArmorSkin = ArmorSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectDropsuit
// (Event, Public, BlueprintEvent)
// Parameters:
// class UIONDropSkin**           DropSkin                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::BPEvent_InspectDropsuit(class UIONDropSkin** DropSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectDropsuit");

	ABP_MenuPawn_C_BPEvent_InspectDropsuit_Params params;
	params.DropSkin = DropSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_PreviewWeaponSkin
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONWeapon**             Weapon                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::BPEvent_PreviewWeaponSkin(class AIONWeapon** Weapon)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_PreviewWeaponSkin");

	ABP_MenuPawn_C_BPEvent_PreviewWeaponSkin_Params params;
	params.Weapon = Weapon;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.CrateTransitionToOpening
// (BlueprintCallable, BlueprintEvent)

void ABP_MenuPawn_C::CrateTransitionToOpening()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.CrateTransitionToOpening");

	ABP_MenuPawn_C_CrateTransitionToOpening_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_MenuPawn.BP_MenuPawn_C.ExecuteUbergraph_BP_MenuPawn
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_MenuPawn_C::ExecuteUbergraph_BP_MenuPawn(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_MenuPawn.BP_MenuPawn_C.ExecuteUbergraph_BP_MenuPawn");

	ABP_MenuPawn_C_ExecuteUbergraph_BP_MenuPawn_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
