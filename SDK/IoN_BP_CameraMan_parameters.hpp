#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_CameraMan.BP_CameraMan_C.UserConstructionScript
struct ABP_CameraMan_C_UserConstructionScript_Params
{
};

// Function BP_CameraMan.BP_CameraMan_C.InpActEvt_OBS_Map_K2Node_InputActionEvent_1
struct ABP_CameraMan_C_InpActEvt_OBS_Map_K2Node_InputActionEvent_1_Params
{
	struct FKey                                        Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_CameraMan.BP_CameraMan_C.ReceiveBeginPlay
struct ABP_CameraMan_C_ReceiveBeginPlay_Params
{
};

// Function BP_CameraMan.BP_CameraMan_C.Add Options Menu
struct ABP_CameraMan_C_Add_Options_Menu_Params
{
};

// Function BP_CameraMan.BP_CameraMan_C.OnOptionChanged
struct ABP_CameraMan_C_OnOptionChanged_Params
{
	int*                                               OptionIndex;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_CameraMan.BP_CameraMan_C.ReceiveDestroyed
struct ABP_CameraMan_C_ReceiveDestroyed_Params
{
};

// Function BP_CameraMan.BP_CameraMan_C.ReceivePossessed
struct ABP_CameraMan_C_ReceivePossessed_Params
{
	class AController**                                NewController;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_CameraMan.BP_CameraMan_C.ExecuteUbergraph_BP_CameraMan
struct ABP_CameraMan_C_ExecuteUbergraph_BP_CameraMan_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
