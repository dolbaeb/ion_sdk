// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsPage_Controls_C::BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature");

	UWBP_SettingsPage_Controls_C_BndEvt__Advanced_Btn_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.ExecuteUbergraph_WBP_SettingsPage_Controls
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Controls_C::ExecuteUbergraph_WBP_SettingsPage_Controls(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Controls.WBP_SettingsPage_Controls_C.ExecuteUbergraph_WBP_SettingsPage_Controls");

	UWBP_SettingsPage_Controls_C_ExecuteUbergraph_WBP_SettingsPage_Controls_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
