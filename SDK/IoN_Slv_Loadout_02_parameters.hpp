#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function Slv_Loadout_02.Slv_Loadout_02_C.ReceiveBeginPlay
struct ASlv_Loadout_02_C_ReceiveBeginPlay_Params
{
};

// Function Slv_Loadout_02.Slv_Loadout_02_C.ExecuteUbergraph_Slv_Loadout_02
struct ASlv_Loadout_02_C_ExecuteUbergraph_Slv_Loadout_02_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
