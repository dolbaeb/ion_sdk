// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_DesertEagle.ABP_DesertEagle_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4
// (BlueprintEvent)

void UABP_DesertEagle_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_DesertEagle.ABP_DesertEagle_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4");

	UABP_DesertEagle_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_DesertEagle.ABP_DesertEagle_C.BlueprintUpdateAnimation
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaTimeX                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_DesertEagle_C::BlueprintUpdateAnimation(float* DeltaTimeX)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_DesertEagle.ABP_DesertEagle_C.BlueprintUpdateAnimation");

	UABP_DesertEagle_C_BlueprintUpdateAnimation_Params params;
	params.DeltaTimeX = DeltaTimeX;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_DesertEagle.ABP_DesertEagle_C.ExecuteUbergraph_ABP_DesertEagle
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_DesertEagle_C::ExecuteUbergraph_ABP_DesertEagle(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_DesertEagle.ABP_DesertEagle_C.ExecuteUbergraph_ABP_DesertEagle");

	UABP_DesertEagle_C_ExecuteUbergraph_ABP_DesertEagle_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
