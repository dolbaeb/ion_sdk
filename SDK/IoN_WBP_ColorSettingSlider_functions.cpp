// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.Get_TextBox_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ColorSettingSlider_C::Get_TextBox_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.Get_TextBox_Text_1");

	UWBP_ColorSettingSlider_C_Get_TextBox_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature");

	UWBP_ColorSettingSlider_C_BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature");

	UWBP_ColorSettingSlider_C_BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.PreConstruct");

	UWBP_ColorSettingSlider_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.SetCurrentValue
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::SetCurrentValue(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.SetCurrentValue");

	UWBP_ColorSettingSlider_C_SetCurrentValue_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ColorSettingSlider_C::BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature");

	UWBP_ColorSettingSlider_C_BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.ExecuteUbergraph_WBP_ColorSettingSlider
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::ExecuteUbergraph_WBP_ColorSettingSlider(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.ExecuteUbergraph_WBP_ColorSettingSlider");

	UWBP_ColorSettingSlider_C_ExecuteUbergraph_WBP_ColorSettingSlider_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueCommitted__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueCommitted__DelegateSignature");

	UWBP_ColorSettingSlider_C_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueChanged__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ColorSettingSlider_C::OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueChanged__DelegateSignature");

	UWBP_ColorSettingSlider_C_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
