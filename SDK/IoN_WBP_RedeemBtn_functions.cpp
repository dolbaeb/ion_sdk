// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.SetPrice
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            NewPrice                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RedeemBtn_C::SetPrice(int NewPrice)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.SetPrice");

	UWBP_RedeemBtn_C_SetPrice_Params params;
	params.NewPrice = NewPrice;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RedeemBtn_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature");

	UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RedeemBtn_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature");

	UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RedeemBtn_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature");

	UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_RedeemBtn_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.Construct");

	UWBP_RedeemBtn_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.ExecuteUbergraph_WBP_RedeemBtn
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RedeemBtn_C::ExecuteUbergraph_WBP_RedeemBtn(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.ExecuteUbergraph_WBP_RedeemBtn");

	UWBP_RedeemBtn_C_ExecuteUbergraph_WBP_RedeemBtn_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RedeemBtn.WBP_RedeemBtn_C.RedeemCratePressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_RedeemBtn_C::RedeemCratePressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RedeemBtn.WBP_RedeemBtn_C.RedeemCratePressed__DelegateSignature");

	UWBP_RedeemBtn_C_RedeemCratePressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
