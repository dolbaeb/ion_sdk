// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_Holo.BP_Holo_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_Holo_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Holo.BP_Holo_C.UserConstructionScript");

	ABP_Holo_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Holo.BP_Holo_C.UpdateMaterialAimState
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         AimRatio                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Holo_C::UpdateMaterialAimState(class AIONFirearm** Firearm, float* AimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Holo.BP_Holo_C.UpdateMaterialAimState");

	ABP_Holo_C_UpdateMaterialAimState_Params params;
	params.Firearm = Firearm;
	params.AimRatio = AimRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Holo.BP_Holo_C.DetachFromFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Holo_C::DetachFromFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Holo.BP_Holo_C.DetachFromFirearmBlueprint");

	ABP_Holo_C_DetachFromFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Holo.BP_Holo_C.AttachToFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Holo_C::AttachToFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Holo.BP_Holo_C.AttachToFirearmBlueprint");

	ABP_Holo_C_AttachToFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Holo.BP_Holo_C.ExecuteUbergraph_BP_Holo
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Holo_C::ExecuteUbergraph_BP_Holo(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Holo.BP_Holo_C.ExecuteUbergraph_BP_Holo");

	ABP_Holo_C_ExecuteUbergraph_BP_Holo_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
