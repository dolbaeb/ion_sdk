// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MapArrow.WBP_MapArrow_C.Get_Image_Base_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_MapArrow_C::Get_Image_Base_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow.WBP_MapArrow_C.Get_Image_Base_ColorAndOpacity_1");

	UWBP_MapArrow_C_Get_Image_Base_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MapArrow.WBP_MapArrow_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MapArrow_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow.WBP_MapArrow_C.Construct");

	UWBP_MapArrow_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow.WBP_MapArrow_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapArrow_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow.WBP_MapArrow_C.Tick");

	UWBP_MapArrow_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow.WBP_MapArrow_C.ExecuteUbergraph_WBP_MapArrow
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapArrow_C::ExecuteUbergraph_WBP_MapArrow(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow.WBP_MapArrow_C.ExecuteUbergraph_WBP_MapArrow");

	UWBP_MapArrow_C_ExecuteUbergraph_WBP_MapArrow_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapArrow.WBP_MapArrow_C.OnPlayerLeft__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_MapArrow_C*         Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MapArrow_C::OnPlayerLeft__DelegateSignature(class UWBP_MapArrow_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapArrow.WBP_MapArrow_C.OnPlayerLeft__DelegateSignature");

	UWBP_MapArrow_C_OnPlayerLeft__DelegateSignature_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
