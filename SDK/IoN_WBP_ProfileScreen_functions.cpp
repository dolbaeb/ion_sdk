// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ProfileScreen.WBP_ProfileScreen_C.OnShow
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_ProfileScreen_C::OnShow()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProfileScreen.WBP_ProfileScreen_C.OnShow");

	UWBP_ProfileScreen_C_OnShow_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProfileScreen.WBP_ProfileScreen_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ProfileScreen_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProfileScreen.WBP_ProfileScreen_C.Construct");

	UWBP_ProfileScreen_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProfileScreen.WBP_ProfileScreen_C.ExecuteUbergraph_WBP_ProfileScreen
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProfileScreen_C::ExecuteUbergraph_WBP_ProfileScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProfileScreen.WBP_ProfileScreen_C.ExecuteUbergraph_WBP_ProfileScreen");

	UWBP_ProfileScreen_C_ExecuteUbergraph_WBP_ProfileScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
