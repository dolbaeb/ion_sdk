#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_GameOverlay.WBP_GameOverlay_C.DisplayAchievementUnlock
struct UWBP_GameOverlay_C_DisplayAchievementUnlock_Params
{
	struct FText                                       Achievement;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_Disclaimer_Visibility_1
struct UWBP_GameOverlay_C_Get_TextBlock_Disclaimer_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_KillsBox_Visibility_1
struct UWBP_GameOverlay_C_Get_KillsBox_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Kills
struct UWBP_GameOverlay_C_GetText_Kills_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetCreditBoxVisibility
struct UWBP_GameOverlay_C_GetCreditBoxVisibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Score
struct UWBP_GameOverlay_C_GetText_Score_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetSurvivorsBoxVisibility
struct UWBP_GameOverlay_C_GetSurvivorsBoxVisibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_SurvivorCount
struct UWBP_GameOverlay_C_GetText_SurvivorCount_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetServerInfoVisibility
struct UWBP_GameOverlay_C_GetServerInfoVisibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Tooltip
struct UWBP_GameOverlay_C_GetText_Tooltip_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetVisibility_1
struct UWBP_GameOverlay_C_GetVisibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_TextBlock_PlayerCount
struct UWBP_GameOverlay_C_Get_TextBlock_PlayerCount_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_MatchTimer
struct UWBP_GameOverlay_C_GetText_MatchTimer_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Get_QueueInfo_Visibility_1
struct UWBP_GameOverlay_C_Get_QueueInfo_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.GetText_Debug
struct UWBP_GameOverlay_C_GetText_Debug_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.StartMessage
struct UWBP_GameOverlay_C_StartMessage_Params
{
	struct FName*                                      Type;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnReceivedChatMessage
struct UWBP_GameOverlay_C_OnReceivedChatMessage_Params
{
	class AIONPlayerState**                            From;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString*                                    Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FName*                                      Type;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Construct
struct UWBP_GameOverlay_C_Construct_Params
{
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.SecondTimer
struct UWBP_GameOverlay_C_SecondTimer_Params
{
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.SetChatVisibility
struct UWBP_GameOverlay_C_SetChatVisibility_Params
{
	bool*                                              bInVisible;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnMicrophoneStateChanged
struct UWBP_GameOverlay_C_OnMicrophoneStateChanged_Params
{
	bool*                                              bEnabled;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.SetMapTooltipVisibility
struct UWBP_GameOverlay_C_SetMapTooltipVisibility_Params
{
	bool*                                              bInVisible;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Update Voice Icon
struct UWBP_GameOverlay_C_Update_Voice_Icon_Params
{
	bool                                               Proximity;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Activated;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.Tick
struct UWBP_GameOverlay_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnScoreEventReceived
struct UWBP_GameOverlay_C_OnScoreEventReceived_Params
{
	EScoreEvent*                                       Type;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int*                                               Points;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnKillConfirmed
struct UWBP_GameOverlay_C_OnKillConfirmed_Params
{
	struct FHitInfo*                                   LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnDownConfirmed
struct UWBP_GameOverlay_C_OnDownConfirmed_Params
{
	struct FHitInfo*                                   LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDied
struct UWBP_GameOverlay_C_OnPlayerDied_Params
{
	struct FHitInfo*                                   LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.OnPlayerDowned
struct UWBP_GameOverlay_C_OnPlayerDowned_Params
{
	struct FHitInfo*                                   LastHitInfo;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.DisplaySubtitle
struct UWBP_GameOverlay_C_DisplaySubtitle_Params
{
	struct FText*                                      SubtitleText;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_GameOverlay.WBP_GameOverlay_C.ExecuteUbergraph_WBP_GameOverlay
struct UWBP_GameOverlay_C_ExecuteUbergraph_WBP_GameOverlay_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
