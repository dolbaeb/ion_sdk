// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetCardVisibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_SpectatorInfo_C::GetCardVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetCardVisibility");

	UWBP_SpectatorInfo_C_GetCardVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponAmmo
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorInfo_C::GetWeaponAmmo()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponAmmo");

	UWBP_SpectatorInfo_C_GetWeaponAmmo_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponIconBrush
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_SpectatorInfo_C::GetWeaponIconBrush()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetWeaponIconBrush");

	UWBP_SpectatorInfo_C_GetWeaponIconBrush_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetPlayerHealthPercentage
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_SpectatorInfo_C::GetPlayerHealthPercentage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.GetPlayerHealthPercentage");

	UWBP_SpectatorInfo_C_GetPlayerHealthPercentage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Setup Widget
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONCharacter*           Character                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorInfo_C::Setup_Widget(class AIONCharacter* Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Setup Widget");

	UWBP_SpectatorInfo_C_Setup_Widget_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Create Background MID
// (BlueprintCallable, BlueprintEvent)

void UWBP_SpectatorInfo_C::Create_Background_MID()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Create Background MID");

	UWBP_SpectatorInfo_C_Create_Background_MID_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorInfo_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Tick");

	UWBP_SpectatorInfo_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Reset Armour Icons
// (BlueprintCallable, BlueprintEvent)

void UWBP_SpectatorInfo_C::Reset_Armour_Icons()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Reset Armour Icons");

	UWBP_SpectatorInfo_C_Reset_Armour_Icons_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Update Armour Icons
// (BlueprintCallable, BlueprintEvent)

void UWBP_SpectatorInfo_C::Update_Armour_Icons()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Update Armour Icons");

	UWBP_SpectatorInfo_C_Update_Armour_Icons_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Change View Mode
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Minimal_Mode_                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorInfo_C::Change_View_Mode(bool Minimal_Mode_)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.Change View Mode");

	UWBP_SpectatorInfo_C_Change_View_Mode_Params params;
	params.Minimal_Mode_ = Minimal_Mode_;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.ExecuteUbergraph_WBP_SpectatorInfo
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorInfo_C::ExecuteUbergraph_WBP_SpectatorInfo(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorInfo.WBP_SpectatorInfo_C.ExecuteUbergraph_WBP_SpectatorInfo");

	UWBP_SpectatorInfo_C_ExecuteUbergraph_WBP_SpectatorInfo_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
