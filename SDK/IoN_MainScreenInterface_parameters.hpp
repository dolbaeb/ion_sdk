#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function MainScreenInterface.MainScreenInterface_C.OnShow
struct UMainScreenInterface_C_OnShow_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
