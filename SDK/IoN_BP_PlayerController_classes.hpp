#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_PlayerController.BP_PlayerController_C
// 0x0038 (0x0D30 - 0x0CF8)
class ABP_PlayerController_C : public ABRPlayerController
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0CF8(0x0008) (Transient, DuplicateTransient)
	class UGSMessageListeners*                         GSMessageListeners;                                       // 0x0D00(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	bool                                               PawnHidden;                                               // 0x0D08(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0D09(0x0007) MISSED OFFSET
	TArray<struct FVector>                             DebugLocations;                                           // 0x0D10(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	bool                                               bSavedDirLight;                                           // 0x0D20(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0D21(0x0007) MISSED OFFSET
	class ADirectionalLight*                           DirLight;                                                 // 0x0D28(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_PlayerController.BP_PlayerController_C");
		return ptr;
	}


	void UserConstructionScript();
	void IONDebugDeleteHUD();
	void IONDebugPawnTick(int* bEnable);
	void IONDebugAnimation(int* bEnable);
	void IONDebugDeleteAllItems();
	void IONDebugItemTick(int* bEnable);
	void IONDebugSetLocation(int* Idx);
	void IONDebugSetCameraControl(int* Idx);
	void IONDebugSpawnChar(int* Count);
	void IONDebugItemsMat();
	void IONDebugDFShadowsMode(int* Mode);
	void PrepareLandingAudioEvent();
	void UpdateVoiceUI(bool* bProximity, bool* bActivated);
	void OnMatchResultsReceived(struct FPlayerMatchHistory* MatchResult);
	void BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature(const struct FGSAchievementEarnedMessage& AchievementEarnedMessage);
	void ExecuteUbergraph_BP_PlayerController(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
