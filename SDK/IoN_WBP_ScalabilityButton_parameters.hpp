#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.EvaluateIndex
struct UWBP_ScalabilityButton_C_EvaluateIndex_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.GetCheckedState_1
struct UWBP_ScalabilityButton_C_GetCheckedState_1_Params
{
	ECheckBoxState                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_ScalabilityButton_C_BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.ExecuteUbergraph_WBP_ScalabilityButton
struct UWBP_ScalabilityButton_C_ExecuteUbergraph_WBP_ScalabilityButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
