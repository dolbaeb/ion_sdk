#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetPlayercardModeText
struct UWBP_SpectatorOptions_C_GetPlayercardModeText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonUp
struct UWBP_SpectatorOptions_C_OnMouseButtonUp_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonDown
struct UWBP_SpectatorOptions_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseWheel
struct UWBP_SpectatorOptions_C_OnMouseWheel_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Get Spectator Background Color
struct UWBP_SpectatorOptions_C_Get_Spectator_Background_Color_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ShowMinimapOptionText
struct UWBP_SpectatorOptions_C_ShowMinimapOptionText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Set Map Open
struct UWBP_SpectatorOptions_C_Set_Map_Open_Params
{
	bool                                               Open_Map_;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnKeyDown
struct UWBP_SpectatorOptions_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.SetMinimapOpen
struct UWBP_SpectatorOptions_C_SetMinimapOpen_Params
{
	bool                                               Maximize;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerState
struct UWBP_SpectatorOptions_C_GetSpectatedPlayerState_Params
{
	class APlayerState*                                State;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateTextVisibility
struct UWBP_SpectatorOptions_C_GetSpectateTextVisibility_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateModeText
struct UWBP_SpectatorOptions_C_GetSpectateModeText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetMapVisibilityText
struct UWBP_SpectatorOptions_C_GetMapVisibilityText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerName
struct UWBP_SpectatorOptions_C_GetSpectatedPlayerName_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetBlurText
struct UWBP_SpectatorOptions_C_GetBlurText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFocalDistanceText
struct UWBP_SpectatorOptions_C_GetFocalDistanceText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFOVText
struct UWBP_SpectatorOptions_C_GetFOVText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFlySpeedText
struct UWBP_SpectatorOptions_C_GetFlySpeedText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Option Changed
struct UWBP_SpectatorOptions_C_On_Option_Changed_Params
{
	int                                                OptionIndex;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Select Option
struct UWBP_SpectatorOptions_C_Select_Option_Params
{
	class UTextBlock*                                  Text;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Construct
struct UWBP_SpectatorOptions_C_Construct_Params
{
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Scrolled
struct UWBP_SpectatorOptions_C_On_Scrolled_Params
{
	float                                              Scroll_Delta;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Tick
struct UWBP_SpectatorOptions_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ExecuteUbergraph_WBP_SpectatorOptions
struct UWBP_SpectatorOptions_C_ExecuteUbergraph_WBP_SpectatorOptions_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
