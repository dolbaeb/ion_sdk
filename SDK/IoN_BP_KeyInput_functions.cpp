// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_KeyInput.BP_KeyInput_C.Generate Display Name
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_KeyInput_C::Generate_Display_Name()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput.BP_KeyInput_C.Generate Display Name");

	UBP_KeyInput_C_Generate_Display_Name_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyInput.BP_KeyInput_C.Save Key Input
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FSKeyActionSave         KeySave                        (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_KeyInput_C::Save_Key_Input(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput.BP_KeyInput_C.Save Key Input");

	UBP_KeyInput_C_Save_Key_Input_Params params;
	params.Game_Settings = Game_Settings;
	params.KeySave = KeySave;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyInput.BP_KeyInput_C.Update Analog Axis Value
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          World_Delta_Seconds            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class APlayerController*       Player_Controller              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyInput_C::Update_Analog_Axis_Value(float World_Delta_Seconds, class APlayerController* Player_Controller)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput.BP_KeyInput_C.Update Analog Axis Value");

	UBP_KeyInput_C_Update_Analog_Axis_Value_Params params;
	params.World_Delta_Seconds = World_Delta_Seconds;
	params.Player_Controller = Player_Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyInput.BP_KeyInput_C.Key Input Current State
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController*       Controller                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Axis_Value                     (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Down                           (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyInput_C::Key_Input_Current_State(class APlayerController* Controller, float* Axis_Value, bool* Down, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput.BP_KeyInput_C.Key Input Current State");

	UBP_KeyInput_C_Key_Input_Current_State_Params params;
	params.Controller = Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Axis_Value != nullptr)
		*Axis_Value = params.Axis_Value;
	if (Down != nullptr)
		*Down = params.Down;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


// Function BP_KeyInput.BP_KeyInput_C.Init Key Input
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyInput              Key_Input                      (BlueprintVisible, BlueprintReadOnly, Parm)
// class UBP_KeyInput_C*          Input                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyInput_C::Init_Key_Input(const struct FSKeyInput& Key_Input, class UBP_KeyInput_C** Input)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyInput.BP_KeyInput_C.Init Key Input");

	UBP_KeyInput_C_Init_Key_Input_Params params;
	params.Key_Input = Key_Input;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Input != nullptr)
		*Input = params.Input;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
