#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_ExtraCapacityUnit.BP_ExtraCapacityUnit_C
// 0x0000 (0x0460 - 0x0460)
class ABP_ExtraCapacityUnit_C : public AIONExtraCapacityUnit
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_ExtraCapacityUnit.BP_ExtraCapacityUnit_C");
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
