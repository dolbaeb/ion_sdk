#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapFailed
struct UWBP_ScrapDialouge_C_ScrapFailed_Params
{
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ScrapSuccess
struct UWBP_ScrapDialouge_C_ScrapSuccess_Params
{
	int                                                CreditsReceived;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.SetItem
struct UWBP_ScrapDialouge_C_SetItem_Params
{
	struct FIONSteamInventoryItem                      ItemToScrap;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature
struct UWBP_ScrapDialouge_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_10_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature
struct UWBP_ScrapDialouge_C_BndEvt__ScrapBtn_K2Node_ComponentBoundEvent_12_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature
struct UWBP_ScrapDialouge_C_BndEvt__WBP_MMBtn_C_2_K2Node_ComponentBoundEvent_134_BtnPressed__DelegateSignature_Params
{
};

// Function WBP_ScrapDialouge.WBP_ScrapDialouge_C.ExecuteUbergraph_WBP_ScrapDialouge
struct UWBP_ScrapDialouge_C_ExecuteUbergraph_WBP_ScrapDialouge_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
