// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_Sako85_Bullet_Elite_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.UserConstructionScript");

	ABP_Sako85_Bullet_Elite_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_Sako85_Bullet_Elite_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveBeginPlay");

	ABP_Sako85_Bullet_Elite_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveEndPlay
// (Event, Public, BlueprintEvent)
// Parameters:
// TEnumAsByte<EEndPlayReason>*   EndPlayReason                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Sako85_Bullet_Elite_C::ReceiveEndPlay(TEnumAsByte<EEndPlayReason>* EndPlayReason)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ReceiveEndPlay");

	ABP_Sako85_Bullet_Elite_C_ReceiveEndPlay_Params params;
	params.EndPlayReason = EndPlayReason;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ExecuteUbergraph_BP_Sako85_Bullet_Elite
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Sako85_Bullet_Elite_C::ExecuteUbergraph_BP_Sako85_Bullet_Elite(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Sako85_Bullet_Elite.BP_Sako85_Bullet_Elite_C.ExecuteUbergraph_BP_Sako85_Bullet_Elite");

	ABP_Sako85_Bullet_Elite_C_ExecuteUbergraph_BP_Sako85_Bullet_Elite_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
