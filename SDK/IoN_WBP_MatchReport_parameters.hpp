#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchReport.WBP_MatchReport_C.OnMouseButtonDown
struct UWBP_MatchReport_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.Show Close Button
struct UWBP_MatchReport_C_Show_Close_Button_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.OnMatchHistorySet
struct UWBP_MatchReport_C_OnMatchHistorySet_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
struct UWBP_MatchReport_C_BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MatchReport.WBP_MatchReport_C.ExecuteUbergraph_WBP_MatchReport
struct UWBP_MatchReport_C_ExecuteUbergraph_WBP_MatchReport_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
