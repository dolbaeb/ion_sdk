// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function Storm_Skeleton_AnimBlueprint.Storm_Skeleton_AnimBlueprint_C.ExecuteUbergraph_Storm_Skeleton_AnimBlueprint
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UStorm_Skeleton_AnimBlueprint_C::ExecuteUbergraph_Storm_Skeleton_AnimBlueprint(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function Storm_Skeleton_AnimBlueprint.Storm_Skeleton_AnimBlueprint_C.ExecuteUbergraph_Storm_Skeleton_AnimBlueprint");

	UStorm_Skeleton_AnimBlueprint_C_ExecuteUbergraph_Storm_Skeleton_AnimBlueprint_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
