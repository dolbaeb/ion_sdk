// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.SetFocus
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           PrimaryKey                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ClassicMappingItem_C::SetFocus(bool PrimaryKey)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.SetFocus");

	UWB_ClassicMappingItem_C_SetFocus_Params params;
	params.PrimaryKey = PrimaryKey;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseWheel
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnMouseWheel(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseWheel");

	UWB_ClassicMappingItem_C_OnMouseWheel_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.RefreshInputMappings
// (Public, BlueprintCallable, BlueprintEvent)

void UWB_ClassicMappingItem_C::RefreshInputMappings()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.RefreshInputMappings");

	UWB_ClassicMappingItem_C_RefreshInputMappings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_GamepadKeyButton_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWB_ClassicMappingItem_C::Get_GamepadKeyButton_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_GamepadKeyButton_Visibility_1");

	UWB_ClassicMappingItem_C_Get_GamepadKeyButton_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyText_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWB_ClassicMappingItem_C::Get_KeyText_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyText_Visibility_1");

	UWB_ClassicMappingItem_C_Get_KeyText_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseMove
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnMouseMove(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseMove");

	UWB_ClassicMappingItem_C_OnMouseMove_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnAnalogValueChanged
// (Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FAnalogInputEvent*      InAnalogInputEvent             (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnAnalogValueChanged(struct FGeometry* MyGeometry, struct FAnalogInputEvent* InAnalogInputEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnAnalogValueChanged");

	UWB_ClassicMappingItem_C_OnAnalogValueChanged_Params params;
	params.MyGeometry = MyGeometry;
	params.InAnalogInputEvent = InAnalogInputEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnInputKeyDown
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FKey                    Key                            (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnInputKeyDown(const struct FKey& Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnInputKeyDown");

	UWB_ClassicMappingItem_C_OnInputKeyDown_Params params;
	params.Key = Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseButtonDown");

	UWB_ClassicMappingItem_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_SecondaryButton_bIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWB_ClassicMappingItem_C::Get_SecondaryButton_bIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_SecondaryButton_bIsEnabled_1");

	UWB_ClassicMappingItem_C_Get_SecondaryButton_bIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyButton_bIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWB_ClassicMappingItem_C::Get_KeyButton_bIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyButton_bIsEnabled_1");

	UWB_ClassicMappingItem_C_Get_KeyButton_bIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnKeyDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWB_ClassicMappingItem_C::OnKeyDown(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnKeyDown");

	UWB_ClassicMappingItem_C_OnKeyDown_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWB_ClassicMappingItem_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Construct");

	UWB_ClassicMappingItem_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWB_ClassicMappingItem_C::BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature");

	UWB_ClassicMappingItem_C_BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWB_ClassicMappingItem_C::BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature");

	UWB_ClassicMappingItem_C_BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnFocusLost
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FFocusEvent*            InFocusEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWB_ClassicMappingItem_C::OnFocusLost(struct FFocusEvent* InFocusEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnFocusLost");

	UWB_ClassicMappingItem_C_OnFocusLost_Params params;
	params.InFocusEvent = InFocusEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWB_ClassicMappingItem_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseLeave");

	UWB_ClassicMappingItem_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.CancelRebind
// (BlueprintCallable, BlueprintEvent)

void UWB_ClassicMappingItem_C::CancelRebind()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.CancelRebind");

	UWB_ClassicMappingItem_C_CancelRebind_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ClassicMappingItem_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.PreConstruct");

	UWB_ClassicMappingItem_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.ExecuteUbergraph_WB_ClassicMappingItem
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ClassicMappingItem_C::ExecuteUbergraph_WB_ClassicMappingItem(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.ExecuteUbergraph_WB_ClassicMappingItem");

	UWB_ClassicMappingItem_C_ExecuteUbergraph_WB_ClassicMappingItem_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
