// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_FriendPopup.WBP_FriendPopup_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_FriendPopup_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendPopup.WBP_FriendPopup_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature");

	UWBP_FriendPopup_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendPopup.WBP_FriendPopup_C.ExecuteUbergraph_WBP_FriendPopup
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_FriendPopup_C::ExecuteUbergraph_WBP_FriendPopup(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendPopup.WBP_FriendPopup_C.ExecuteUbergraph_WBP_FriendPopup");

	UWBP_FriendPopup_C_ExecuteUbergraph_WBP_FriendPopup_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
