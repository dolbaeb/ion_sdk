// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Update Audio Channel
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EAudioType>        Audio_Channel                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Volume                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           _                              (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBPI_GameSettingsInterface_C::Update_Audio_Channel(TEnumAsByte<EAudioType> Audio_Channel, float Volume, bool* _)
{
	static auto fn = UObject::FindObject<UFunction>("Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Update Audio Channel");

	UBPI_GameSettingsInterface_C_Update_Audio_Channel_Params params;
	params.Audio_Channel = Audio_Channel;
	params.Volume = Volume;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (_ != nullptr)
		*_ = params._;
}


// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Run Console Command
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Console_Command                (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           _                              (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBPI_GameSettingsInterface_C::Run_Console_Command(const struct FString& Console_Command, bool* _)
{
	static auto fn = UObject::FindObject<UFunction>("Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Run Console Command");

	UBPI_GameSettingsInterface_C_Run_Console_Command_Params params;
	params.Console_Command = Console_Command;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (_ != nullptr)
		*_ = params._;
}


// Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Get Settings Instance
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettingsWrapper_C* SettingsWrapper                (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBPI_GameSettingsInterface_C::Get_Settings_Instance(class UBP_GameSettingsWrapper_C** SettingsWrapper)
{
	static auto fn = UObject::FindObject<UFunction>("Function BPI_GameSettingsInterface.BPI_GameSettingsInterface_C.Get Settings Instance");

	UBPI_GameSettingsInterface_C_Get_Settings_Instance_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (SettingsWrapper != nullptr)
		*SettingsWrapper = params.SettingsWrapper;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
