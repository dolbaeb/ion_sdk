#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MapArrow.WBP_MapArrow_C.Get_Image_Base_ColorAndOpacity_1
struct UWBP_MapArrow_C_Get_Image_Base_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_MapArrow.WBP_MapArrow_C.Construct
struct UWBP_MapArrow_C_Construct_Params
{
};

// Function WBP_MapArrow.WBP_MapArrow_C.Tick
struct UWBP_MapArrow_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapArrow.WBP_MapArrow_C.ExecuteUbergraph_WBP_MapArrow
struct UWBP_MapArrow_C_ExecuteUbergraph_WBP_MapArrow_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapArrow.WBP_MapArrow_C.OnPlayerLeft__DelegateSignature
struct UWBP_MapArrow_C_OnPlayerLeft__DelegateSignature_Params
{
	class UWBP_MapArrow_C*                             Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
