// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetToolTipWidget_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_WeaponSlot_C::GetToolTipWidget_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetToolTipWidget_1");

	UWBP_WeaponSlot_C_GetToolTipWidget_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.UpdateWeaponSkin
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamItem*           WeaponSkin                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponSlot_C::UpdateWeaponSkin(class UIONSteamItem* WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.UpdateWeaponSkin");

	UWBP_WeaponSlot_C_UpdateWeaponSkin_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_DragReceive_Background_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponSlot_C::Get_DragReceive_Background_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_DragReceive_Background_1");

	UWBP_WeaponSlot_C_Get_DragReceive_Background_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Highlight_Background_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponSlot_C::Get_Highlight_Background_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Highlight_Background_1");

	UWBP_WeaponSlot_C_Get_Highlight_Background_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetLabelText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponSlot_C::GetLabelText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetLabelText");

	UWBP_WeaponSlot_C_GetLabelText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Frame_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponSlot_C::Get_Frame_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Frame_Brush_1");

	UWBP_WeaponSlot_C_Get_Frame_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_WeaponSlot_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetCurrentItem");

	UWBP_WeaponSlot_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponSlot_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetText_1");

	UWBP_WeaponSlot_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WeaponSlot_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.AcceptsDropForItem");

	UWBP_WeaponSlot_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmountText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponSlot_C::Get_AmountText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmountText_Text_1");

	UWBP_WeaponSlot_C_Get_AmountText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmmoText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponSlot_C::Get_AmmoText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmmoText_Text_1");

	UWBP_WeaponSlot_C_Get_AmmoText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_WeaponSlot_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnDrop");

	UWBP_WeaponSlot_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_WeaponSlot_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemChanged");

	UWBP_WeaponSlot_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemClicked
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_WeaponSlot_C::OnItemClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemClicked");

	UWBP_WeaponSlot_C_OnItemClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_WeaponSlot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Construct");

	UWBP_WeaponSlot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.Tick");

	UWBP_WeaponSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnWeaponSkinChanged
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONWeaponSkin*          WeaponSkin                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponSlot_C::OnWeaponSkinChanged(class UIONWeaponSkin* WeaponSkin)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnWeaponSkinChanged");

	UWBP_WeaponSlot_C_OnWeaponSkinChanged_Params params;
	params.WeaponSkin = WeaponSkin;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponSlot.WBP_WeaponSlot_C.ExecuteUbergraph_WBP_WeaponSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponSlot_C::ExecuteUbergraph_WBP_WeaponSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponSlot.WBP_WeaponSlot_C.ExecuteUbergraph_WBP_WeaponSlot");

	UWBP_WeaponSlot_C_ExecuteUbergraph_WBP_WeaponSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
