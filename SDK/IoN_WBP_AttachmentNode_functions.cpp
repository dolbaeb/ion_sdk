// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.AcceptsDropForItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem**               Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_AttachmentNode_C::AcceptsDropForItem(class AIONItem** Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.AcceptsDropForItem");

	UWBP_AttachmentNode_C_AcceptsDropForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Get_EmptyBG_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_AttachmentNode_C::Get_EmptyBG_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.Get_EmptyBG_Visibility_1");

	UWBP_AttachmentNode_C_Get_EmptyBG_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnDrop
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          PointerEvent                   (BlueprintVisible, BlueprintReadOnly, Parm)
// class UDragDropOperation**     Operation                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_AttachmentNode_C::OnDrop(struct FGeometry* MyGeometry, struct FPointerEvent* PointerEvent, class UDragDropOperation** Operation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnDrop");

	UWBP_AttachmentNode_C_OnDrop_Params params;
	params.MyGeometry = MyGeometry;
	params.PointerEvent = PointerEvent;
	params.Operation = Operation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_2
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_AttachmentNode_C::OnMouseButtonDown_2(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_2");

	UWBP_AttachmentNode_C_OnMouseButtonDown_2_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_AttachmentNode_C::OnMouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_1");

	UWBP_AttachmentNode_C_OnMouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_AttachmentNode_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetText_1");

	UWBP_AttachmentNode_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetCurrentItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class AIONItem*                ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class AIONItem* UWBP_AttachmentNode_C::GetCurrentItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetCurrentItem");

	UWBP_AttachmentNode_C_GetCurrentItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnItemChanged
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_AttachmentNode_C::OnItemChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnItemChanged");

	UWBP_AttachmentNode_C_OnItemChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AttachmentNode_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.PreConstruct");

	UWBP_AttachmentNode_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AttachmentNode_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.Tick");

	UWBP_AttachmentNode_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_AttachmentNode_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.Construct");

	UWBP_AttachmentNode_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AttachmentNode.WBP_AttachmentNode_C.ExecuteUbergraph_WBP_AttachmentNode
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AttachmentNode_C::ExecuteUbergraph_WBP_AttachmentNode(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AttachmentNode.WBP_AttachmentNode_C.ExecuteUbergraph_WBP_AttachmentNode");

	UWBP_AttachmentNode_C_ExecuteUbergraph_WBP_AttachmentNode_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
