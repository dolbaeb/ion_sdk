#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Icon.WBP_Icon_C.ShowMainMenu
struct UWBP_Icon_C_ShowMainMenu_Params
{
};

// Function WBP_Icon.WBP_Icon_C.ScrapBtnPressed
struct UWBP_Icon_C_ScrapBtnPressed_Params
{
};

// Function WBP_Icon.WBP_Icon_C.ResetBtnPressed
struct UWBP_Icon_C_ResetBtnPressed_Params
{
};

// Function WBP_Icon.WBP_Icon_C.InpsectBtnPressed
struct UWBP_Icon_C_InpsectBtnPressed_Params
{
};

// Function WBP_Icon.WBP_Icon_C.EquipBtnPressed
struct UWBP_Icon_C_EquipBtnPressed_Params
{
};

// Function WBP_Icon.WBP_Icon_C.GetToolTipWidget_1
struct UWBP_Icon_C_GetToolTipWidget_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.GetSteamItem
struct UWBP_Icon_C_GetSteamItem_Params
{
	struct FIONSteamInventoryItem                      SteamItem;                                                // (Parm, OutParm)
};

// Function WBP_Icon.WBP_Icon_C.SetSteamItem
struct UWBP_Icon_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      SteamItem;                                                // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_Icon.WBP_Icon_C.SetIconTexture
struct UWBP_Icon_C_SetIconTexture_Params
{
	class UTexture2D*                                  NewTexture;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.SetIconName
struct UWBP_Icon_C_SetIconName_Params
{
	struct FString                                     Name;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_Icon.WBP_Icon_C.OnMouseButtonDown
struct UWBP_Icon_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Icon.WBP_Icon_C.BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature
struct UWBP_Icon_C_BndEvt__ItemButton_K2Node_ComponentBoundEvent_0_OnButtonPressedEvent__DelegateSignature_Params
{
};

// Function WBP_Icon.WBP_Icon_C.ChangeSelectionState
struct UWBP_Icon_C_ChangeSelectionState_Params
{
	bool                                               bSelected;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.ChangeEquipState
struct UWBP_Icon_C_ChangeEquipState_Params
{
	bool                                               bEquip;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.BPEvent_EquipChanged
struct UWBP_Icon_C_BPEvent_EquipChanged_Params
{
	bool*                                              bEquipped;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.ExecuteUbergraph_WBP_Icon
struct UWBP_Icon_C_ExecuteUbergraph_WBP_Icon_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Icon.WBP_Icon_C.ItemUnEquipped__DelegateSignature
struct UWBP_Icon_C_ItemUnEquipped__DelegateSignature_Params
{
};

// Function WBP_Icon.WBP_Icon_C.ItemEquipped__DelegateSignature
struct UWBP_Icon_C_ItemEquipped__DelegateSignature_Params
{
};

// Function WBP_Icon.WBP_Icon_C.ItemSelected__DelegateSignature
struct UWBP_Icon_C_ItemSelected__DelegateSignature_Params
{
	class UWBP_Icon_C*                                 BtnRef;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
