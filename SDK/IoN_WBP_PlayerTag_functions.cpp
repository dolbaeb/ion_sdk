// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerTag.WBP_PlayerTag_C.Show
// (BlueprintCallable, BlueprintEvent)

void UWBP_PlayerTag_C::Show()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.Show");

	UWBP_PlayerTag_C_Show_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerTag.WBP_PlayerTag_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PlayerTag_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.Construct");

	UWBP_PlayerTag_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerTag.WBP_PlayerTag_C.SetPawn
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class APawn*                   Pawn                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerTag_C::SetPawn(class APawn* Pawn)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.SetPawn");

	UWBP_PlayerTag_C_SetPawn_Params params;
	params.Pawn = Pawn;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerTag.WBP_PlayerTag_C.Retry PlayerState
// (BlueprintCallable, BlueprintEvent)

void UWBP_PlayerTag_C::Retry_PlayerState()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.Retry PlayerState");

	UWBP_PlayerTag_C_Retry_PlayerState_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerTag.WBP_PlayerTag_C.ShowMainMenu
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FText                   PlayerName                     (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_PlayerTag_C::ShowMainMenu(const struct FText& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.ShowMainMenu");

	UWBP_PlayerTag_C_ShowMainMenu_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerTag.WBP_PlayerTag_C.ExecuteUbergraph_WBP_PlayerTag
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerTag_C::ExecuteUbergraph_WBP_PlayerTag(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerTag.WBP_PlayerTag_C.ExecuteUbergraph_WBP_PlayerTag");

	UWBP_PlayerTag_C_ExecuteUbergraph_WBP_PlayerTag_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
