// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Player.WBP_Player_C.GetVisibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_Player_C::GetVisibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.GetVisibility_1");

	UWBP_Player_C_GetVisibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Player.WBP_Player_C.MakeDisplayTextForItem
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONItem*                Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Player_C::MakeDisplayTextForItem(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.MakeDisplayTextForItem");

	UWBP_Player_C_MakeDisplayTextForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Player.WBP_Player_C.GetText_2
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Player_C::GetText_2()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.GetText_2");

	UWBP_Player_C_GetText_2_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Player.WBP_Player_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Player_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.GetText_1");

	UWBP_Player_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Player.WBP_Player_C.GetInteractionWidget
// (Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent, Const)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_Player_C::GetInteractionWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.GetInteractionWidget");

	UWBP_Player_C_GetInteractionWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Player.WBP_Player_C.UpdateInteractionWidget
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FInteractionOption*     InteractionOption              (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// float*                         DeltaTime                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Player_C::UpdateInteractionWidget(struct FInteractionOption* InteractionOption, float* DeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.UpdateInteractionWidget");

	UWBP_Player_C_UpdateInteractionWidget_Params params;
	params.InteractionOption = InteractionOption;
	params.DeltaTime = DeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.HideInteractionWidget
// (Event, Public, BlueprintEvent)

void UWBP_Player_C::HideInteractionWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.HideInteractionWidget");

	UWBP_Player_C_HideInteractionWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Player_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.Tick");

	UWBP_Player_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.On Helmet Equipped
// (BlueprintCallable, BlueprintEvent)

void UWBP_Player_C::On_Helmet_Equipped()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.On Helmet Equipped");

	UWBP_Player_C_On_Helmet_Equipped_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.On Helmet Removed
// (BlueprintCallable, BlueprintEvent)

void UWBP_Player_C::On_Helmet_Removed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.On Helmet Removed");

	UWBP_Player_C_On_Helmet_Removed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.UpdateInteractionWidgetWithText
// (Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FText*                  InteractionText                (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// float*                         DeltaTime                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool*                          bHideInteractButton            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Player_C::UpdateInteractionWidgetWithText(struct FText* InteractionText, float* DeltaTime, bool* bHideInteractButton)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.UpdateInteractionWidgetWithText");

	UWBP_Player_C_UpdateInteractionWidgetWithText_Params params;
	params.InteractionText = InteractionText;
	params.DeltaTime = DeltaTime;
	params.bHideInteractButton = bHideInteractButton;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_Player_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.Construct");

	UWBP_Player_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.On Downed Out State Changed
// (BlueprintCallable, BlueprintEvent)

void UWBP_Player_C::On_Downed_Out_State_Changed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.On Downed Out State Changed");

	UWBP_Player_C_On_Downed_Out_State_Changed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.On Reviving State Changed
// (BlueprintCallable, BlueprintEvent)

void UWBP_Player_C::On_Reviving_State_Changed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.On Reviving State Changed");

	UWBP_Player_C_On_Reviving_State_Changed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.OnWearablesChanged
// (BlueprintCallable, BlueprintEvent)

void UWBP_Player_C::OnWearablesChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.OnWearablesChanged");

	UWBP_Player_C_OnWearablesChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Player.WBP_Player_C.ExecuteUbergraph_WBP_Player
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Player_C::ExecuteUbergraph_WBP_Player(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Player.WBP_Player_C.ExecuteUbergraph_WBP_Player");

	UWBP_Player_C_ExecuteUbergraph_WBP_Player_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
