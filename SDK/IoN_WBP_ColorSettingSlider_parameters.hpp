#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.Get_TextBox_Text_1
struct UWBP_ColorSettingSlider_C_Get_TextBox_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature
struct UWBP_ColorSettingSlider_C_BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature
struct UWBP_ColorSettingSlider_C_BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.PreConstruct
struct UWBP_ColorSettingSlider_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.SetCurrentValue
struct UWBP_ColorSettingSlider_C_SetCurrentValue_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature
struct UWBP_ColorSettingSlider_C_BndEvt__Slider_K2Node_ComponentBoundEvent_132_OnMouseCaptureEndEvent__DelegateSignature_Params
{
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.ExecuteUbergraph_WBP_ColorSettingSlider
struct UWBP_ColorSettingSlider_C_ExecuteUbergraph_WBP_ColorSettingSlider_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueCommitted__DelegateSignature
struct UWBP_ColorSettingSlider_C_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ColorSettingSlider.WBP_ColorSettingSlider_C.OnValueChanged__DelegateSignature
struct UWBP_ColorSettingSlider_C_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
