#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_BulletShake_Sako85_3.BP_BulletShake_Sako85_2_C.NewFunction_1
struct UBP_BulletShake_Sako85_2_C_NewFunction_1_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
