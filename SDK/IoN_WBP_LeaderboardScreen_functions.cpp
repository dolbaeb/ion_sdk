// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BuildCurrentLeaderboardShortCode
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FString                 LeaderboardShortCode           (Parm, OutParm, ZeroConstructor)
// struct FString                 GameMode                       (Parm, OutParm, ZeroConstructor)

void UWBP_LeaderboardScreen_C::BuildCurrentLeaderboardShortCode(struct FString* LeaderboardShortCode, struct FString* GameMode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BuildCurrentLeaderboardShortCode");

	UWBP_LeaderboardScreen_C_BuildCurrentLeaderboardShortCode_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (LeaderboardShortCode != nullptr)
		*LeaderboardShortCode = params.LeaderboardShortCode;
	if (GameMode != nullptr)
		*GameMode = params.GameMode;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.StringToLeaderboardCategory
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FString                 inString                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ELeaderboardCategories> ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

TEnumAsByte<ELeaderboardCategories> UWBP_LeaderboardScreen_C::StringToLeaderboardCategory(const struct FString& inString)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.StringToLeaderboardCategory");

	UWBP_LeaderboardScreen_C_StringToLeaderboardCategory_Params params;
	params.inString = inString;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetVisibility_Throbber
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_LeaderboardScreen_C::GetVisibility_Throbber()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetVisibility_Throbber");

	UWBP_LeaderboardScreen_C_GetVisibility_Throbber_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedFailure
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::OnReceivedFailure()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedFailure");

	UWBP_LeaderboardScreen_C_OnReceivedFailure_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedLeaderboard
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> LeaderboardData                (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnReceivedLeaderboard(TArray<struct FLeaderboardDataEntry>* LeaderboardData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnReceivedLeaderboard");

	UWBP_LeaderboardScreen_C_OnReceivedLeaderboard_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (LeaderboardData != nullptr)
		*LeaderboardData = params.LeaderboardData;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetMaxLeaderboardEntries
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UWBP_LeaderboardScreen_C::GetMaxLeaderboardEntries()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetMaxLeaderboardEntries");

	UWBP_LeaderboardScreen_C_GetMaxLeaderboardEntries_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetCurrentStatistic
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, Const)
// Parameters:
// struct FString                 RetrunValue                    (Parm, OutParm, ZeroConstructor)

void UWBP_LeaderboardScreen_C::GetCurrentStatistic(struct FString* RetrunValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetCurrentStatistic");

	UWBP_LeaderboardScreen_C_GetCurrentStatistic_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (RetrunValue != nullptr)
		*RetrunValue = params.RetrunValue;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.UpdateLeaderboard
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::UpdateLeaderboard()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.UpdateLeaderboard");

	UWBP_LeaderboardScreen_C_UpdateLeaderboard_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardScreen_C::BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_LeaderboardScreen_C_BndEvt__GlobalCheckBox_K2Node_ComponentBoundEvent_9_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardScreen_C::BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_LeaderboardScreen_C_BndEvt__FriendsOnlyCheckBox_K2Node_ComponentBoundEvent_11_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboardAroundPlayer
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::GetLeaderboardAroundPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboardAroundPlayer");

	UWBP_LeaderboardScreen_C_GetLeaderboardAroundPlayer_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboard
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::GetFriendLeaderboard()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboard");

	UWBP_LeaderboardScreen_C_GetFriendLeaderboard_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboardAroundPlayer
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::GetFriendLeaderboardAroundPlayer()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetFriendLeaderboardAroundPlayer");

	UWBP_LeaderboardScreen_C_GetFriendLeaderboardAroundPlayer_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.DoUpdateLeaderboard
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::DoUpdateLeaderboard()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.DoUpdateLeaderboard");

	UWBP_LeaderboardScreen_C_DoUpdateLeaderboard_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_LeaderboardScreen_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Construct");

	UWBP_LeaderboardScreen_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboard
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardScreen_C::GetLeaderboard()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.GetLeaderboard");

	UWBP_LeaderboardScreen_C_GetLeaderboard_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardSuccess
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetGlobalLeaderboardSuccess(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardSuccess");

	UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardSuccess_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFailed
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetGlobalLeaderboardFailed(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFailed");

	UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFailed_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardSuccess
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetAroundMeLeaderboardSuccess(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardSuccess");

	UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardSuccess_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFailed
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetAroundMeLeaderboardFailed(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFailed");

	UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFailed_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsSuccess
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetGlobalLeaderboardFriendsSuccess(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsSuccess");

	UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFriendsSuccess_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsFailed
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetGlobalLeaderboardFriendsFailed(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetGlobalLeaderboardFriendsFailed");

	UWBP_LeaderboardScreen_C_OnGetGlobalLeaderboardFriendsFailed_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsSuccess
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetAroundMeLeaderboardFriendsSuccess(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsSuccess");

	UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFriendsSuccess_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsFailed
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FLeaderboardDataEntry> ScriptData                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_LeaderboardScreen_C::OnGetAroundMeLeaderboardFriendsFailed(TArray<struct FLeaderboardDataEntry> ScriptData)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.OnGetAroundMeLeaderboardFriendsFailed");

	UWBP_LeaderboardScreen_C_OnGetAroundMeLeaderboardFriendsFailed_Params params;
	params.ScriptData = ScriptData;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// TEnumAsByte<ELeaderboardTeamTypes> Team_Type                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardScreen_C::BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature(TEnumAsByte<ELeaderboardTeamTypes> Team_Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature");

	UWBP_LeaderboardScreen_C_BndEvt__WBP_LeaderboardTeamTypeOptions_K2Node_ComponentBoundEvent_574_OnTeamTypeChanged__DelegateSignature_Params params;
	params.Team_Type = Team_Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// TEnumAsByte<ELeaderboardCategories> Category                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardScreen_C::BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature(TEnumAsByte<ELeaderboardCategories> Category)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature");

	UWBP_LeaderboardScreen_C_BndEvt__WBP_LeaderboardOptionsPanel_K2Node_ComponentBoundEvent_602_OnCategoryChanged__DelegateSignature_Params params;
	params.Category = Category;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Connected
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                UserId                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString*                AuthToken                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_LeaderboardScreen_C::Connected(struct FString* UserId, struct FString* AuthToken)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.Connected");

	UWBP_LeaderboardScreen_C_Connected_Params params;
	params.UserId = UserId;
	params.AuthToken = AuthToken;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.ExecuteUbergraph_WBP_LeaderboardScreen
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardScreen_C::ExecuteUbergraph_WBP_LeaderboardScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardScreen.WBP_LeaderboardScreen_C.ExecuteUbergraph_WBP_LeaderboardScreen");

	UWBP_LeaderboardScreen_C_ExecuteUbergraph_WBP_LeaderboardScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
