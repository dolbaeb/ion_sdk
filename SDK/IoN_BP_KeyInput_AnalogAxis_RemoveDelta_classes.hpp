#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_KeyInput_AnalogAxis_RemoveDelta.BP_KeyInput_AnalogAxis_RemoveDelta_C
// 0x0000 (0x0070 - 0x0070)
class UBP_KeyInput_AnalogAxis_RemoveDelta_C : public UBP_KeyInput_AnalogAxis_C
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_KeyInput_AnalogAxis_RemoveDelta.BP_KeyInput_AnalogAxis_RemoveDelta_C");
		return ptr;
	}


	void Key_Input_Current_State(class APlayerController** Controller, float* Axis_Value, bool* Down, bool* Just_Pressed, bool* Just_Released);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
