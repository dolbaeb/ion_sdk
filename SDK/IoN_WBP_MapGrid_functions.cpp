// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MapGrid.WBP_MapGrid_C.UpdateFieldMaterial
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UMaterialInstanceDynamic* Mid                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   ParamName                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 Location                       (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float                          Radius                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_C::UpdateFieldMaterial(class UMaterialInstanceDynamic* Mid, const struct FName& ParamName, const struct FVector& Location, float Radius)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid.WBP_MapGrid_C.UpdateFieldMaterial");

	UWBP_MapGrid_C_UpdateFieldMaterial_Params params;
	params.Mid = Mid;
	params.ParamName = ParamName;
	params.Location = Location;
	params.Radius = Radius;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid.WBP_MapGrid_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MapGrid_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid.WBP_MapGrid_C.Construct");

	UWBP_MapGrid_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid.WBP_MapGrid_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid.WBP_MapGrid_C.Tick");

	UWBP_MapGrid_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid.WBP_MapGrid_C.OnPlayerLeft_Event_1
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_MapArrow_C*         Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MapGrid_C::OnPlayerLeft_Event_1(class UWBP_MapArrow_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid.WBP_MapGrid_C.OnPlayerLeft_Event_1");

	UWBP_MapGrid_C_OnPlayerLeft_Event_1_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid.WBP_MapGrid_C.ExecuteUbergraph_WBP_MapGrid
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_C::ExecuteUbergraph_WBP_MapGrid(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid.WBP_MapGrid_C.ExecuteUbergraph_WBP_MapGrid");

	UWBP_MapGrid_C_ExecuteUbergraph_WBP_MapGrid_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
