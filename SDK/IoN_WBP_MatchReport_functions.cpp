// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MatchReport.WBP_MatchReport_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_MatchReport_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.OnMouseButtonDown");

	UWBP_MatchReport_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Score_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Score_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Rank_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Rank_K2Node_ComponentBoundEvent_1_Clicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.Show Close Button
// (BlueprintCallable, BlueprintEvent)

void UWBP_MatchReport_C::Show_Close_Button()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.Show Close Button");

	UWBP_MatchReport_C_Show_Close_Button_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Tab_Result_K2Node_ComponentBoundEvent_0_Clicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__WBP_MatchReport_Result_K2Node_ComponentBoundEvent_0_AnimationFinished__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.OnMatchHistorySet
// (Event, Public, BlueprintEvent)

void UWBP_MatchReport_C::OnMatchHistorySet()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.OnMatchHistorySet");

	UWBP_MatchReport_C_OnMatchHistorySet_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MatchReport_C::BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature");

	UWBP_MatchReport_C_BndEvt__Button_396_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MatchReport.WBP_MatchReport_C.ExecuteUbergraph_WBP_MatchReport
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MatchReport_C::ExecuteUbergraph_WBP_MatchReport(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MatchReport.WBP_MatchReport_C.ExecuteUbergraph_WBP_MatchReport");

	UWBP_MatchReport_C_ExecuteUbergraph_WBP_MatchReport_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
