// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_IONKnife.BP_IONKnife_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IONKnife_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IONKnife.BP_IONKnife_C.UserConstructionScript");

	ABP_IONKnife_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IONKnife.BP_IONKnife_C.EventBP_HitSurfaceType
// (Event, Public, BlueprintEvent)
// Parameters:
// TEnumAsByte<EPhysicalSurface>* SurfaceTypeHit                 (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IONKnife_C::EventBP_HitSurfaceType(TEnumAsByte<EPhysicalSurface>* SurfaceTypeHit)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IONKnife.BP_IONKnife_C.EventBP_HitSurfaceType");

	ABP_IONKnife_C_EventBP_HitSurfaceType_Params params;
	params.SurfaceTypeHit = SurfaceTypeHit;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IONKnife.BP_IONKnife_C.ResetBloodmask
// (BlueprintCallable, BlueprintEvent)

void ABP_IONKnife_C::ResetBloodmask()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IONKnife.BP_IONKnife_C.ResetBloodmask");

	ABP_IONKnife_C_ResetBloodmask_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IONKnife.BP_IONKnife_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_IONKnife_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IONKnife.BP_IONKnife_C.ReceiveBeginPlay");

	ABP_IONKnife_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IONKnife.BP_IONKnife_C.ExecuteUbergraph_BP_IONKnife
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IONKnife_C::ExecuteUbergraph_BP_IONKnife(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IONKnife.BP_IONKnife_C.ExecuteUbergraph_BP_IONKnife");

	ABP_IONKnife_C_ExecuteUbergraph_BP_IONKnife_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
