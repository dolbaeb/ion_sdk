#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// WidgetBlueprintGeneratedClass WBP_MainMenuBtn.WBP_MainMenuBtn_C
// 0x014C (0x035C - 0x0210)
class UWBP_MainMenuBtn_C : public UUserWidget
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0210(0x0008) (Transient, DuplicateTransient)
	class UBorder*                                     BGHighlight;                                              // 0x0218(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData, RepSkip, RepNotify, Interp, NonTransactional, EditorOnly, NoDestructor, AutoWeak, ContainsInstancedReference, AssetRegistrySearchable, SimpleDisplay, AdvancedDisplay, Protected, BlueprintCallable, BlueprintAuthorityOnly, TextExportTransient, NonPIEDuplicateTransient, ExposeOnSpawn, PersistentInstance, UObjectWrapper, HasGetValueTypeHash, NativeAccessSpecifierPublic, NativeAccessSpecifierProtected, NativeAccessSpecifierPrivate)
	class UButton*                                     Btn;                                                      // 0x0220(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData, RepSkip, RepNotify, Interp, NonTransactional, EditorOnly, NoDestructor, AutoWeak, ContainsInstancedReference, AssetRegistrySearchable, SimpleDisplay, AdvancedDisplay, Protected, BlueprintCallable, BlueprintAuthorityOnly, TextExportTransient, NonPIEDuplicateTransient, ExposeOnSpawn, PersistentInstance, UObjectWrapper, HasGetValueTypeHash, NativeAccessSpecifierPublic, NativeAccessSpecifierProtected, NativeAccessSpecifierPrivate)
	class UTextBlock*                                  Text;                                                     // 0x0228(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData, RepSkip, RepNotify, Interp, NonTransactional, EditorOnly, NoDestructor, AutoWeak, ContainsInstancedReference, AssetRegistrySearchable, SimpleDisplay, AdvancedDisplay, Protected, BlueprintCallable, BlueprintAuthorityOnly, TextExportTransient, NonPIEDuplicateTransient, ExposeOnSpawn, PersistentInstance, UObjectWrapper, HasGetValueTypeHash, NativeAccessSpecifierPublic, NativeAccessSpecifierProtected, NativeAccessSpecifierPrivate)
	struct FText                                       BtnText;                                                  // 0x0230(0x0018) (Edit, BlueprintVisible)
	struct FScriptMulticastDelegate                    BtnClicked;                                               // 0x0248(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	struct FSlateFontInfo                              Normal_FontInfo;                                          // 0x0258(0x0058) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FSlateColor                                 Normal_Color;                                             // 0x02B0(0x0028) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FSlateFontInfo                              Hovered_FontInfo;                                         // 0x02D8(0x0058) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FSlateColor                                 Hovered_Color;                                            // 0x0330(0x0028) (Edit, BlueprintVisible, DisableEditOnInstance)
	int                                                FontSize;                                                 // 0x0358(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("WidgetBlueprintGeneratedClass WBP_MainMenuBtn.WBP_MainMenuBtn_C");
		return ptr;
	}


	void PreConstruct(bool* IsDesignTime);
	void BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature();
	void BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature();
	void BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature();
	void ExecuteUbergraph_WBP_MainMenuBtn(int EntryPoint);
	void BtnClicked__DelegateSignature();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
