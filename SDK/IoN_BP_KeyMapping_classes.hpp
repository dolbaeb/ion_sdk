#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_KeyMapping.BP_KeyMapping_C
// 0x0030 (0x0060 - 0x0030)
class UBP_KeyMapping_C : public UObject
{
public:
	struct FString                                     Name;                                                     // 0x0030(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	float                                              Scale;                                                    // 0x0040(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0044(0x0004) MISSED OFFSET
	class UBP_KeyCombination_C*                        Primary_Combination;                                      // 0x0048(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyCombination_C*                        Secondary_Combination;                                    // 0x0050(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBP_KeyAction_C*                             Parent_Action;                                            // 0x0058(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_KeyMapping.BP_KeyMapping_C");
		return ptr;
	}


	void Revert_To_Default_KeyMapping();
	void Load_Key_Mapping(class UBP_GameSettings_C* Game_Settings, const struct FString& Action_Name, const struct FString& Category);
	void Save_Key_Mapping(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave);
	void Key_Mapping_Current_State(class APlayerController* Player_Controller, float* Mapping_Value, bool* Is_Active, bool* Just_Pressed, bool* Just_Released);
	void Init_Key_Mapping(const struct FSKeyMapping& Key_Mapping, class UBP_KeyMapping_C** Mapping);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
