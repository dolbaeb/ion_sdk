#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.Get_Label_Text_1
struct UWBP_ScoreCounter_C_Get_Label_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.Tick
struct UWBP_ScoreCounter_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.BeginAnimation
struct UWBP_ScoreCounter_C_BeginAnimation_Params
{
};

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.ExecuteUbergraph_WBP_ScoreCounter
struct UWBP_ScoreCounter_C_ExecuteUbergraph_WBP_ScoreCounter_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ScoreCounter.WBP_ScoreCounter_C.OnValueUpdated__DelegateSignature
struct UWBP_ScoreCounter_C_OnValueUpdated__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
