#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SChannelMessage.SChannelMessage
// 0x0049
struct FSChannelMessage
{
	struct FDateTime                                   MessageTime_5_CDFA17B9413C7E838492E38FECF6BE64;           // 0x0000(0x0008) (Edit, BlueprintVisible)
	struct FText                                       PlayerName_10_600E41CD4927B6968704DBB132241588;           // 0x0008(0x0018) (Edit, BlueprintVisible)
	struct FText                                       Message_8_863BA3504F0960659BD080965D900EE7;               // 0x0020(0x0018) (Edit, BlueprintVisible)
	struct FGuid                                       MessageID_2_A749122444ABCFCFBF761FA02EF6CC1B;             // 0x0038(0x0010) (Edit, BlueprintVisible, IsPlainOldData)
	TEnumAsByte<EChatChannel>                          Channel_14_31D8E03F4EABE5CD33ECCD828723AF4E;              // 0x0048(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
