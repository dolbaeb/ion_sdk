// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerBox.WBP_PlayerBox_C.OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FPlayerProfile          PlayerProfile                  (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           bHasErrors                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBox_C::OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1(const struct FPlayerProfile& PlayerProfile, bool bHasErrors)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1");

	UWBP_PlayerBox_C_OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1_Params params;
	params.PlayerProfile = PlayerProfile;
	params.bHasErrors = bHasErrors;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.Connected
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                UserId                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString*                AuthToken                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_PlayerBox_C::Connected(struct FString* UserId, struct FString* AuthToken)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.Connected");

	UWBP_PlayerBox_C_Connected_Params params;
	params.UserId = UserId;
	params.AuthToken = AuthToken;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PlayerBox_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.Construct");

	UWBP_PlayerBox_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.UpdateModeInfo
// (BlueprintCallable, BlueprintEvent)

void UWBP_PlayerBox_C::UpdateModeInfo()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.UpdateModeInfo");

	UWBP_PlayerBox_C_UpdateModeInfo_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature
// (BlueprintEvent)
// Parameters:
// int                            Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBox_C::BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature");

	UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature
// (BlueprintEvent)
// Parameters:
// int                            Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBox_C::BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature");

	UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature
// (BlueprintEvent)
// Parameters:
// int                            Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBox_C::BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature");

	UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_PlayerBox_C::BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature");

	UWBP_PlayerBox_C_BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.Reconnect
// (BlueprintCallable, BlueprintEvent)

void UWBP_PlayerBox_C::Reconnect()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.Reconnect");

	UWBP_PlayerBox_C_Reconnect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBox.WBP_PlayerBox_C.ExecuteUbergraph_WBP_PlayerBox
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBox_C::ExecuteUbergraph_WBP_PlayerBox(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBox.WBP_PlayerBox_C.ExecuteUbergraph_WBP_PlayerBox");

	UWBP_PlayerBox_C_ExecuteUbergraph_WBP_PlayerBox_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
