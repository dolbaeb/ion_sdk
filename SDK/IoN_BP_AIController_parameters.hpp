#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_AIController.BP_AIController_C.UserConstructionScript
struct ABP_AIController_C_UserConstructionScript_Params
{
};

// Function BP_AIController.BP_AIController_C.ReceiveBeginPlay
struct ABP_AIController_C_ReceiveBeginPlay_Params
{
};

// Function BP_AIController.BP_AIController_C.Move to random spot
struct ABP_AIController_C_Move_to_random_spot_Params
{
};

// Function BP_AIController.BP_AIController_C.ExecuteUbergraph_BP_AIController
struct ABP_AIController_C_ExecuteUbergraph_BP_AIController_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
