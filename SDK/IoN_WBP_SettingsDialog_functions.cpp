// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnMouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SettingsDialog_C::OnMouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnMouseButtonDown_1");

	UWBP_SettingsDialog_C_OnMouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnKeyDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SettingsDialog_C::OnKeyDown(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnKeyDown");

	UWBP_SettingsDialog_C_OnKeyDown_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Switch Tab
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_SettingsSectionButton_C* Selected_Button                (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            Active_Widget_Index            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsDialog_C::Switch_Tab(class UWBP_SettingsSectionButton_C* Selected_Button, int Active_Widget_Index)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Switch Tab");

	UWBP_SettingsDialog_C_Switch_Tab_Params params;
	params.Selected_Button = Selected_Button;
	params.Active_Widget_Index = Active_Widget_Index;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Cancel Save Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__Cancel_Save_Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Cancel Save Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__Cancel_Save_Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Open Menu
// (BlueprintCallable, BlueprintEvent)

void UWBP_SettingsDialog_C::Open_Menu()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Open Menu");

	UWBP_SettingsDialog_C_Open_Menu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Close Menu
// (BlueprintCallable, BlueprintEvent)

void UWBP_SettingsDialog_C::Close_Menu()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Close Menu");

	UWBP_SettingsDialog_C_Close_Menu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Apply And Close
// (BlueprintCallable, BlueprintEvent)

void UWBP_SettingsDialog_C::Apply_And_Close()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Apply And Close");

	UWBP_SettingsDialog_C_Apply_And_Close_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Begin Tab Switch
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_SettingsSectionButton_C* SelectedButton                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// int                            ActiveWidgetIndex              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsDialog_C::Begin_Tab_Switch(class UWBP_SettingsSectionButton_C* SelectedButton, int ActiveWidgetIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Begin Tab Switch");

	UWBP_SettingsDialog_C_Begin_Tab_Switch_Params params;
	params.SelectedButton = SelectedButton;
	params.ActiveWidgetIndex = ActiveWidgetIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_SettingsDialog_C::BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature");

	UWBP_SettingsDialog_C_BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.ExecuteUbergraph_WBP_SettingsDialog
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsDialog_C::ExecuteUbergraph_WBP_SettingsDialog(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.ExecuteUbergraph_WBP_SettingsDialog");

	UWBP_SettingsDialog_C_ExecuteUbergraph_WBP_SettingsDialog_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Menu Closed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_SettingsDialog_C::Menu_Closed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsDialog.WBP_SettingsDialog_C.Menu Closed__DelegateSignature");

	UWBP_SettingsDialog_C_Menu_Closed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
