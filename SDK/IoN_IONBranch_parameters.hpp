#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function IONBranch.AnnouncerAudioComponent.PlayAnnouncerSound
struct UAnnouncerAudioComponent_PlayAnnouncerSound_Params
{
	class UAkAudioEvent*                               EventToPlay;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHighPriority;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.AnnouncerAudioComponent.OnSafeZoneMarked
struct UAnnouncerAudioComponent_OnSafeZoneMarked_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStopped
struct UAnnouncerAudioComponent_OnPlasmaConvergenceStopped_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnPlasmaConvergenceStarted
struct UAnnouncerAudioComponent_OnPlasmaConvergenceStarted_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnNextRoundCountdownStarted
struct UAnnouncerAudioComponent_OnNextRoundCountdownStarted_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnMatchEnded
struct UAnnouncerAudioComponent_OnMatchEnded_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerWon
struct UAnnouncerAudioComponent_OnLocalPlayerWon_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnLocalPlayerDied
struct UAnnouncerAudioComponent_OnLocalPlayerDied_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnLastStageCompleted
struct UAnnouncerAudioComponent_OnLastStageCompleted_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnFinalConvergenceStarted
struct UAnnouncerAudioComponent_OnFinalConvergenceStarted_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleStarted
struct UAnnouncerAudioComponent_OnBattleRoyaleStarted_Params
{
};

// Function IONBranch.AnnouncerAudioComponent.OnBattleRoyaleCountdownStarted
struct UAnnouncerAudioComponent_OnBattleRoyaleCountdownStarted_Params
{
};

// Function IONBranch.ArenaAudioComponent.PlayArenaSound
struct UArenaAudioComponent_PlayArenaSound_Params
{
	class UAkAudioEvent*                               EventToPlay;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHighPriority;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.ArenaAudioComponent.OnSafeZoneMarked
struct UArenaAudioComponent_OnSafeZoneMarked_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStopped
struct UArenaAudioComponent_OnPlasmaConvergenceStopped_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnPlasmaConvergenceStarted
struct UArenaAudioComponent_OnPlasmaConvergenceStarted_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnNextRoundCountdownStarted
struct UArenaAudioComponent_OnNextRoundCountdownStarted_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnMatchEnded
struct UArenaAudioComponent_OnMatchEnded_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnLocalPlayerWon
struct UArenaAudioComponent_OnLocalPlayerWon_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnLocalPlayerDied
struct UArenaAudioComponent_OnLocalPlayerDied_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnLastStageCompleted
struct UArenaAudioComponent_OnLastStageCompleted_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnFinalConvergenceStarted
struct UArenaAudioComponent_OnFinalConvergenceStarted_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnBattleRoyaleStarted
struct UArenaAudioComponent_OnBattleRoyaleStarted_Params
{
};

// Function IONBranch.ArenaAudioComponent.OnBattleRoyaleCountdownStarted
struct UArenaAudioComponent_OnBattleRoyaleCountdownStarted_Params
{
};

// Function IONBranch.IONGameState.GetNumPlayersAlive
struct AIONGameState_GetNumPlayersAlive_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameState.GetAnnouncerAudioComponent
struct AIONGameState_GetAnnouncerAudioComponent_Params
{
	class UAnnouncerAudioComponent*                    ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONGameState.BP_OnPlayerDied
struct AIONGameState_BP_OnPlayerDied_Params
{
	class APlayerState*                                Victim;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class APlayerState*                                DamageInstigator;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	class AActor*                                      DamageCauser;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHeadshot;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bKnockdown;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Range;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BattleRoyaleGameState.OnRep_SpawnRandomSeed
struct ABattleRoyaleGameState_OnRep_SpawnRandomSeed_Params
{
};

// Function IONBranch.BattleRoyaleGameState.OnRep_PlasmaStages
struct ABattleRoyaleGameState_OnRep_PlasmaStages_Params
{
};

// Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaRadius
struct ABattleRoyaleGameState_OnRep_InitialPlasmaRadius_Params
{
};

// Function IONBranch.BattleRoyaleGameState.OnRep_InitialPlasmaLocation
struct ABattleRoyaleGameState_OnRep_InitialPlasmaLocation_Params
{
};

// Function IONBranch.BattleRoyaleGameState.OnRep_BattleRoyaleStartTime
struct ABattleRoyaleGameState_OnRep_BattleRoyaleStartTime_Params
{
};

// Function IONBranch.BattleRoyaleGameState.HasBattleRoyaleStarted
struct ABattleRoyaleGameState_HasBattleRoyaleStarted_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.BattleRoyaleGameState.GetTimeSinceMatchStart
struct ABattleRoyaleGameState_GetTimeSinceMatchStart_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.BattleRoyaleGameState.GetMatchStartTime
struct ABattleRoyaleGameState_GetMatchStartTime_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.BattleRoyaleGameState.GetInitialPlasmaLocation
struct ABattleRoyaleGameState_GetInitialPlasmaLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.UpdateSteamInventoryItems
struct AIONBasePlayerController_UpdateSteamInventoryItems_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseSuccessfull__DelegateSignature
struct AIONBasePlayerController_SteamPurchaseSuccessfull__DelegateSignature_Params
{
	int                                                NewItem;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.SteamPurchaseFailed__DelegateSignature
struct AIONBasePlayerController_SteamPurchaseFailed__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.SteamInventoryUpdateDelegate__DelegateSignature
struct AIONBasePlayerController_SteamInventoryUpdateDelegate__DelegateSignature_Params
{
};

// Function IONBranch.IONBasePlayerController.ShowHighlights
struct AIONBasePlayerController_ShowHighlights_Params
{
};

// Function IONBranch.IONBasePlayerController.SetAudioOutputDevicesVolume
struct AIONBasePlayerController_SetAudioOutputDevicesVolume_Params
{
	int                                                Volume;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.SetAudioOutputDevice
struct AIONBasePlayerController_SetAudioOutputDevice_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.SetAudioInputDevicesVolume
struct AIONBasePlayerController_SetAudioInputDevicesVolume_Params
{
	int                                                Volume;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.SetAudioInputDevice
struct AIONBasePlayerController_SetAudioInputDevice_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.SerializeLoadout
struct AIONBasePlayerController_SerializeLoadout_Params
{
};

// Function IONBranch.IONBasePlayerController.SaveLoadout
struct AIONBasePlayerController_SaveLoadout_Params
{
};

// Function IONBranch.IONBasePlayerController.RemoveSkinFromLoadout
struct AIONBasePlayerController_RemoveSkinFromLoadout_Params
{
	struct FIONSteamInventoryItem                      ItemToRemove;                                             // (Parm)
};

// Function IONBranch.IONBasePlayerController.RecordTestHighlight
struct AIONBasePlayerController_RecordTestHighlight_Params
{
};

// Function IONBranch.IONBasePlayerController.ProcessVirtualGoodFromSteam
struct AIONBasePlayerController_ProcessVirtualGoodFromSteam_Params
{
	struct FString                                     OrderId;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.OpenHighlightsGroup
struct AIONBasePlayerController_OpenHighlightsGroup_Params
{
};

// Function IONBranch.IONBasePlayerController.OnTeamChatMessageReceived
struct AIONBasePlayerController_OnTeamChatMessageReceived_Params
{
	struct FGSTeamChatMessage                          TeamChatMessage;                                          // (Parm)
};

// Function IONBranch.IONBasePlayerController.OnScriptMessageReceived
struct AIONBasePlayerController_OnScriptMessageReceived_Params
{
	struct FGSScriptMessage                            ScriptMessage;                                            // (Parm)
};

// Function IONBranch.IONBasePlayerController.OnRep_PlayerState
struct AIONBasePlayerController_OnRep_PlayerState_Params
{
};

// Function IONBranch.IONBasePlayerController.OnNewTeamScoreMessageReceived
struct AIONBasePlayerController_OnNewTeamScoreMessageReceived_Params
{
	struct FGSNewTeamScoreMessage                      NewTeamScoreMessage;                                      // (Parm)
};

// Function IONBranch.IONBasePlayerController.OnNewHighScoreMessageReceived
struct AIONBasePlayerController_OnNewHighScoreMessageReceived_Params
{
	struct FGSNewHighScoreMessage                      NewHighScoreMessage;                                      // (Parm)
};

// Function IONBranch.IONBasePlayerController.OnGlobalRankChangedMessageReceived
struct AIONBasePlayerController_OnGlobalRankChangedMessageReceived_Params
{
	struct FGSGlobalRankChangedMessage                 GlobalRankChangedMessage;                                 // (Parm)
};

// Function IONBranch.IONBasePlayerController.OnAchievementEarnedMessageReceived
struct AIONBasePlayerController_OnAchievementEarnedMessageReceived_Params
{
	struct FGSAchievementEarnedMessage                 AchievementEarnedMessage;                                 // (Parm)
};

// Function IONBranch.IONBasePlayerController.LoadGlobalLeaderboardData
struct AIONBasePlayerController_LoadGlobalLeaderboardData_Params
{
	struct FString                                     LeaderboardShortCode;                                     // (Parm, ZeroConstructor)
	struct FString                                     GameMode;                                                 // (Parm, ZeroConstructor)
	int                                                NumEntries;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bIsSocial;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FScriptDelegate                             OnSuccess;                                                // (Parm, ZeroConstructor)
	struct FScriptDelegate                             OnFailed;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.LoadAroundMeLeaderboardData
struct AIONBasePlayerController_LoadAroundMeLeaderboardData_Params
{
	struct FString                                     LeaderboardShortCode;                                     // (Parm, ZeroConstructor)
	struct FString                                     GameMode;                                                 // (Parm, ZeroConstructor)
	int                                                NumEntries;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bIsSocial;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FScriptDelegate                             OnSuccess;                                                // (Parm, ZeroConstructor)
	struct FScriptDelegate                             OnFailed;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.IsSameSquadAs
struct AIONBasePlayerController_IsSameSquadAs_Params
{
	class APlayerState*                                OtherPlayer;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.IsItemInLoadout
struct AIONBasePlayerController_IsItemInLoadout_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (Parm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.GS_UpdateCredits
struct AIONBasePlayerController_GS_UpdateCredits_Params
{
};

// Function IONBranch.IONBasePlayerController.GS_ScrapItem
struct AIONBasePlayerController_GS_ScrapItem_Params
{
	struct FIONSteamInventoryItem                      ScrapItem;                                                // (Parm)
};

// Function IONBranch.IONBasePlayerController.GS_PurchaseCrate
struct AIONBasePlayerController_GS_PurchaseCrate_Params
{
	class UIONSteamCrate*                              CrateToPurchase;                                          // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.GS_OpenCrate
struct AIONBasePlayerController_GS_OpenCrate_Params
{
	struct FIONSteamInventoryItem                      CrateToOpen;                                              // (Parm)
};

// Function IONBranch.IONBasePlayerController.GS_LoadStoreItems
struct AIONBasePlayerController_GS_LoadStoreItems_Params
{
};

// Function IONBranch.IONBasePlayerController.GS_LoadLoadout_Server
struct AIONBasePlayerController_GS_LoadLoadout_Server_Params
{
};

// Function IONBranch.IONBasePlayerController.GS_LoadLoadout
struct AIONBasePlayerController_GS_LoadLoadout_Params
{
};

// Function IONBranch.IONBasePlayerController.GetSteamSessionTicket
struct AIONBasePlayerController_GetSteamSessionTicket_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetSquadMates
struct AIONBasePlayerController_GetSquadMates_Params
{
	TArray<class APlayerState*>                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetSquadID
struct AIONBasePlayerController_GetSquadID_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetLogEventData
struct AIONBasePlayerController_GetLogEventData_Params
{
	struct FString                                     EventKey;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.GetListOfDifferentLeaderboards
struct AIONBasePlayerController_GetListOfDifferentLeaderboards_Params
{
};

// Function IONBranch.IONBasePlayerController.GetKeysForAction
struct AIONBasePlayerController_GetKeysForAction_Params
{
	struct FName                                       ActionName;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<struct FInputActionKeyMapping>              Bindings;                                                 // (Parm, OutParm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.GetItemBySteamDefIDFast
struct AIONBasePlayerController_GetItemBySteamDefIDFast_Params
{
	int                                                ID;                                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UIONSteamItem*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.GetItemBySteamDefID
struct AIONBasePlayerController_GetItemBySteamDefID_Params
{
	int                                                ID;                                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UIONSteamItem*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.GetGameSparksIDFromSteamID
struct AIONBasePlayerController_GetGameSparksIDFromSteamID_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	struct FScriptDelegate                             OnSuccess;                                                // (Parm, ZeroConstructor)
	struct FScriptDelegate                             OnFailed;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONBasePlayerController.GetCurrentAudioOutputDevice
struct AIONBasePlayerController_GetCurrentAudioOutputDevice_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetCurrentAudioInputDevice
struct AIONBasePlayerController_GetCurrentAudioInputDevice_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetAudioOutputDevices
struct AIONBasePlayerController_GetAudioOutputDevices_Params
{
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetAudioInputDevices
struct AIONBasePlayerController_GetAudioInputDevices_Params
{
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONBasePlayerController.GetAppliedSkinByWeapon
struct AIONBasePlayerController_GetAppliedSkinByWeapon_Params
{
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UIONWeaponSkin*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapItemDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksScrapItemDelegate__DelegateSignature_Params
{
	int                                                Result;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksScrapFailedDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksScrapFailedDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateFailedDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksPurchaseCrateFailedDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksPurchaseCrateDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksPurchaseCrateDelegate__DelegateSignature_Params
{
	int                                                ItemDefID;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksOpenCrateDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksOpenCrateDelegate__DelegateSignature_Params
{
	int                                                Result;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutFailedDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksLoadoutFailedDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksLoadoutDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksLoadoutDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksFaileDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksFaileDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksCreditsUpdatedDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksCreditsUpdatedDelegate__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.GameSparksAuthenticatedDelegate__DelegateSignature
struct AIONBasePlayerController_GameSparksAuthenticatedDelegate__DelegateSignature_Params
{
};

// Function IONBranch.IONBasePlayerController.FindAllWeaponSkins
struct AIONBasePlayerController_FindAllWeaponSkins_Params
{
};

// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetLeaderboard__DelegateSignature
struct AIONBasePlayerController_DelegateOnSuccessGetLeaderboard__DelegateSignature_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnSuccessGetGameSparksID__DelegateSignature
struct AIONBasePlayerController_DelegateOnSuccessGetGameSparksID__DelegateSignature_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetLeaderboard__DelegateSignature
struct AIONBasePlayerController_DelegateOnFailedGetLeaderboard__DelegateSignature_Params
{
	TArray<struct FLeaderboardDataEntry>               ScriptData;                                               // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// DelegateFunction IONBranch.IONBasePlayerController.DelegateOnFailedGetGameSparksID__DelegateSignature
struct AIONBasePlayerController_DelegateOnFailedGetGameSparksID__DelegateSignature_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONBasePlayerController.CloseHighlightsGroup
struct AIONBasePlayerController_CloseHighlightsGroup_Params
{
};

// Function IONBranch.IONBasePlayerController.ClientSetKickedMessage
struct AIONBasePlayerController_ClientSetKickedMessage_Params
{
	struct FText                                       KickedReason;                                             // (ConstParm, Parm, ReferenceParm)
};

// Function IONBranch.IONBasePlayerController.ApplyNewSkinToLoadout
struct AIONBasePlayerController_ApplyNewSkinToLoadout_Params
{
	struct FIONSteamInventoryItem                      NewItem;                                                  // (Parm)
};

// Function IONBranch.MainPlayerController.VoiceModeChanged
struct AMainPlayerController_VoiceModeChanged_Params
{
	int                                                NewVoiceMode;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.UpdateVoiceUI
struct AMainPlayerController_UpdateVoiceUI_Params
{
	bool                                               bProximity;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bActivated;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ToggleScreenshotMode
struct AMainPlayerController_ToggleScreenshotMode_Params
{
};

// Function IONBranch.MainPlayerController.TogglePing
struct AMainPlayerController_TogglePing_Params
{
};

// Function IONBranch.MainPlayerController.ToggleMutePlayerByPlayerState
struct AMainPlayerController_ToggleMutePlayerByPlayerState_Params
{
	class APlayerState*                                OtherPlayerState;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ToggleHUD
struct AMainPlayerController_ToggleHUD_Params
{
};

// Function IONBranch.MainPlayerController.ToggleHighlights
struct AMainPlayerController_ToggleHighlights_Params
{
};

// Function IONBranch.MainPlayerController.ToggleFreeCam
struct AMainPlayerController_ToggleFreeCam_Params
{
};

// Function IONBranch.MainPlayerController.ToggleDebugText
struct AMainPlayerController_ToggleDebugText_Params
{
};

// Function IONBranch.MainPlayerController.Suicide
struct AMainPlayerController_Suicide_Params
{
};

// Function IONBranch.MainPlayerController.StopClientBot
struct AMainPlayerController_StopClientBot_Params
{
};

// Function IONBranch.MainPlayerController.StartSpectating
struct AMainPlayerController_StartSpectating_Params
{
};

// Function IONBranch.MainPlayerController.StartPlaying
struct AMainPlayerController_StartPlaying_Params
{
};

// Function IONBranch.MainPlayerController.StartClientBot
struct AMainPlayerController_StartClientBot_Params
{
};

// Function IONBranch.MainPlayerController.SpectatePreviousPlayer
struct AMainPlayerController_SpectatePreviousPlayer_Params
{
};

// Function IONBranch.MainPlayerController.SpectateNextPlayer
struct AMainPlayerController_SpectateNextPlayer_Params
{
};

// Function IONBranch.MainPlayerController.SpawnIn
struct AMainPlayerController_SpawnIn_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByTimeAlive
struct AMainPlayerController_SortResultsByTimeAlive_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByTeamIndex
struct AMainPlayerController_SortResultsByTeamIndex_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByPlace
struct AMainPlayerController_SortResultsByPlace_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByKills
struct AMainPlayerController_SortResultsByKills_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByHeadshotAccuracy
struct AMainPlayerController_SortResultsByHeadshotAccuracy_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByDamageAmount
struct AMainPlayerController_SortResultsByDamageAmount_Params
{
};

// Function IONBranch.MainPlayerController.SortResultsByAccuracy
struct AMainPlayerController_SortResultsByAccuracy_Params
{
};

// Function IONBranch.MainPlayerController.ServerToggleFreeCam
struct AMainPlayerController_ServerToggleFreeCam_Params
{
};

// Function IONBranch.MainPlayerController.ServerToggleCameraMan
struct AMainPlayerController_ServerToggleCameraMan_Params
{
};

// Function IONBranch.MainPlayerController.ServerSuicide
struct AMainPlayerController_ServerSuicide_Params
{
};

// Function IONBranch.MainPlayerController.ServerStartSpectating
struct AMainPlayerController_ServerStartSpectating_Params
{
};

// Function IONBranch.MainPlayerController.ServerStartPlaying
struct AMainPlayerController_ServerStartPlaying_Params
{
};

// Function IONBranch.MainPlayerController.ServerSpawnIn
struct AMainPlayerController_ServerSpawnIn_Params
{
};

// Function IONBranch.MainPlayerController.ServerSetDoorOpen
struct AMainPlayerController_ServerSetDoorOpen_Params
{
	class ADoor*                                       Door;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bNewOpen;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ServerSendTeamMessage
struct AMainPlayerController_ServerSendTeamMessage_Params
{
	struct FString                                     S;                                                        // (Parm, ZeroConstructor)
	struct FName                                       Type;                                                     // (ConstParm, Parm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ServerSendBEPacket
struct AMainPlayerController_ServerSendBEPacket_Params
{
	TArray<unsigned char>                              Packet;                                                   // (ConstParm, Parm, ZeroConstructor, ReferenceParm)
};

// Function IONBranch.MainPlayerController.ServerRespawn
struct AMainPlayerController_ServerRespawn_Params
{
};

// Function IONBranch.MainPlayerController.SendTeamMessage
struct AMainPlayerController_SendTeamMessage_Params
{
	struct FString                                     S;                                                        // (Parm, ZeroConstructor)
	struct FName                                       Type;                                                     // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.SelfUnmuteAll
struct AMainPlayerController_SelfUnmuteAll_Params
{
};

// Function IONBranch.MainPlayerController.SelfMuteAll
struct AMainPlayerController_SelfMuteAll_Params
{
};

// Function IONBranch.MainPlayerController.Respawn
struct AMainPlayerController_Respawn_Params
{
};

// Function IONBranch.MainPlayerController.ResetInputMode
struct AMainPlayerController_ResetInputMode_Params
{
};

// Function IONBranch.MainPlayerController.RemoveBots
struct AMainPlayerController_RemoveBots_Params
{
};

// Function IONBranch.MainPlayerController.OpenMenu
struct AMainPlayerController_OpenMenu_Params
{
};

// Function IONBranch.MainPlayerController.OnMatchResultsReceived
struct AMainPlayerController_OnMatchResultsReceived_Params
{
	struct FPlayerMatchHistory                         MatchResult;                                              // (Parm)
};

// Function IONBranch.MainPlayerController.OnAudioLevelLoaded
struct AMainPlayerController_OnAudioLevelLoaded_Params
{
};

// Function IONBranch.MainPlayerController.MoveCharacterToTransform
struct AMainPlayerController_MoveCharacterToTransform_Params
{
	class AIONCharacter*                               CCharacter;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FTransform                                  NewTransform;                                             // (Parm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.LogoutOfVivoxServer
struct AMainPlayerController_LogoutOfVivoxServer_Params
{
};

// Function IONBranch.MainPlayerController.LoginToVivoxServer
struct AMainPlayerController_LoginToVivoxServer_Params
{
};

// Function IONBranch.MainPlayerController.ListPrePassPolys
struct AMainPlayerController_ListPrePassPolys_Params
{
};

// Function IONBranch.MainPlayerController.ListPawns
struct AMainPlayerController_ListPawns_Params
{
};

// Function IONBranch.MainPlayerController.ListAsyncLoads
struct AMainPlayerController_ListAsyncLoads_Params
{
	float                                              MinLoadTime;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.IsStreamer
struct AMainPlayerController_IsStreamer_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.IsQA
struct AMainPlayerController_IsQA_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.IsDeveloper
struct AMainPlayerController_IsDeveloper_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.IsAdmin
struct AMainPlayerController_IsAdmin_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.HasSpectatableTeammate
struct AMainPlayerController_HasSpectatableTeammate_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.HasAccessLevel
struct AMainPlayerController_HasAccessLevel_Params
{
	EIONAdminAccessLevels                              RequestedLevel;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.HandleReturnToMainMenu
struct AMainPlayerController_HandleReturnToMainMenu_Params
{
};

// Function IONBranch.MainPlayerController.GotoBot
struct AMainPlayerController_GotoBot_Params
{
	int                                                BotIndex;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.GetTimeUntilRespawn
struct AMainPlayerController_GetTimeUntilRespawn_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.GetSquadChannelName
struct AMainPlayerController_GetSquadChannelName_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.MainPlayerController.GetServerTickRate
struct AMainPlayerController_GetServerTickRate_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.GetProximityServerName
struct AMainPlayerController_GetProximityServerName_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.MainPlayerController.GetGameOverlay
struct AMainPlayerController_GetGameOverlay_Params
{
	class UIONGameOverlayWidget*                       ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.GetCharacter
struct AMainPlayerController_GetCharacter_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.GetAudioEnergy
struct AMainPlayerController_GetAudioEnergy_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.EnableStaticMeshCollision
struct AMainPlayerController_EnableStaticMeshCollision_Params
{
};

// Function IONBranch.MainPlayerController.DebugMenu
struct AMainPlayerController_DebugMenu_Params
{
	bool                                               bShow;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ConnectToVivoxVoiceChannels
struct AMainPlayerController_ConnectToVivoxVoiceChannels_Params
{
};

// Function IONBranch.MainPlayerController.ConnectToVivoxSquadChannel
struct AMainPlayerController_ConnectToVivoxSquadChannel_Params
{
};

// Function IONBranch.MainPlayerController.ConnectToVivoxProximityChannel
struct AMainPlayerController_ConnectToVivoxProximityChannel_Params
{
};

// Function IONBranch.MainPlayerController.CloseMenu
struct AMainPlayerController_CloseMenu_Params
{
};

// Function IONBranch.MainPlayerController.CloseInventory
struct AMainPlayerController_CloseInventory_Params
{
};

// Function IONBranch.MainPlayerController.ClientUnregisterPlayer
struct AMainPlayerController_ClientUnregisterPlayer_Params
{
};

// Function IONBranch.MainPlayerController.ClientShowMatchResult
struct AMainPlayerController_ClientShowMatchResult_Params
{
	struct FPlayerMatchHistory                         MatchResult;                                              // (Parm)
};

// Function IONBranch.MainPlayerController.ClientShowEliminationScreen
struct AMainPlayerController_ClientShowEliminationScreen_Params
{
	struct FPlayerMatchResult                          Result;                                                   // (Parm)
};

// Function IONBranch.MainPlayerController.ClientSendMatchResults
struct AMainPlayerController_ClientSendMatchResults_Params
{
	struct FMatchResults                               Results;                                                  // (Parm)
};

// Function IONBranch.MainPlayerController.ClientSendBEPacket
struct AMainPlayerController_ClientSendBEPacket_Params
{
	TArray<unsigned char>                              Packet;                                                   // (ConstParm, Parm, ZeroConstructor, ReferenceParm)
};

// Function IONBranch.MainPlayerController.ClientRegisterPlayer
struct AMainPlayerController_ClientRegisterPlayer_Params
{
};

// Function IONBranch.MainPlayerController.ClientReceiveScoreEvent
struct AMainPlayerController_ClientReceiveScoreEvent_Params
{
	EScoreEvent                                        ScoreType;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Points;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ClientPushCmd
struct AMainPlayerController_ClientPushCmd_Params
{
	struct FString                                     Cmd;                                                      // (Parm, ZeroConstructor)
};

// Function IONBranch.MainPlayerController.ClientPostLogin
struct AMainPlayerController_ClientPostLogin_Params
{
};

// Function IONBranch.MainPlayerController.ClientOnPlayerKilled
struct AMainPlayerController_ClientOnPlayerKilled_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.MainPlayerController.ClientOnPlayerDowned
struct AMainPlayerController_ClientOnPlayerDowned_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.MainPlayerController.ClientConfirmHit
struct AMainPlayerController_ClientConfirmHit_Params
{
	class AActor*                                      HitActor;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         HitComponent;                                             // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	struct FVector                                     HitRelativeLocation;                                      // (ConstParm, Parm, ReferenceParm, IsPlainOldData)
	bool                                               bHeadshot;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.ClearAsyncLoads
struct AMainPlayerController_ClearAsyncLoads_Params
{
};

// Function IONBranch.MainPlayerController.CanUseCameraMan
struct AMainPlayerController_CanUseCameraMan_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.CanSpectate
struct AMainPlayerController_CanSpectate_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.CanRespawn
struct AMainPlayerController_CanRespawn_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.CanEverRespawn
struct AMainPlayerController_CanEverRespawn_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainPlayerController.AddBots
struct AMainPlayerController_AddBots_Params
{
	int                                                NumBots;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bSpawnNearMe;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.ServerOnPlayerMapFullyLoaded
struct ABRPlayerController_ServerOnPlayerMapFullyLoaded_Params
{
};

// Function IONBranch.BRPlayerController.OnPrematchLevelLoaded
struct ABRPlayerController_OnPrematchLevelLoaded_Params
{
};

// Function IONBranch.BRPlayerController.OnMapLevelLoaded
struct ABRPlayerController_OnMapLevelLoaded_Params
{
};

// Function IONBranch.BRPlayerController.IONDebugSpawnChar
struct ABRPlayerController_IONDebugSpawnChar_Params
{
	int                                                Count;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugSetLocation
struct ABRPlayerController_IONDebugSetLocation_Params
{
	int                                                Idx;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugSetCameraControl
struct ABRPlayerController_IONDebugSetCameraControl_Params
{
	int                                                Idx;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugPawnTick
struct ABRPlayerController_IONDebugPawnTick_Params
{
	int                                                bEnable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugItemTick
struct ABRPlayerController_IONDebugItemTick_Params
{
	int                                                bEnable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugItemsMat
struct ABRPlayerController_IONDebugItemsMat_Params
{
};

// Function IONBranch.BRPlayerController.IONDebugDFShadowsMode
struct ABRPlayerController_IONDebugDFShadowsMode_Params
{
	int                                                Mode;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.IONDebugDeleteHUD
struct ABRPlayerController_IONDebugDeleteHUD_Params
{
};

// Function IONBranch.BRPlayerController.IONDebugDeleteAllItems
struct ABRPlayerController_IONDebugDeleteAllItems_Params
{
};

// Function IONBranch.BRPlayerController.IONDebugAnimation
struct ABRPlayerController_IONDebugAnimation_Params
{
	int                                                bEnable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.ForceOnPrematchLevelLoaded
struct ABRPlayerController_ForceOnPrematchLevelLoaded_Params
{
};

// Function IONBranch.BRPlayerController.ForceOnMapLevelLoaded
struct ABRPlayerController_ForceOnMapLevelLoaded_Params
{
};

// Function IONBranch.BRPlayerController.ClientSetAndHoldCameraFade
struct ABRPlayerController_ClientSetAndHoldCameraFade_Params
{
	bool                                               bEnableFading;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FColor                                      FadeColor;                                                // (Parm, IsPlainOldData)
	struct FVector2D                                   FadeAlpha;                                                // (Parm, IsPlainOldData)
	float                                              FadeTime;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFadeAudio;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.BRPlayerController.ClientOnDropInCountdownStarted
struct ABRPlayerController_ClientOnDropInCountdownStarted_Params
{
};

// Function IONBranch.DaytimeActor.OnPlayerLeftVolume
struct ADaytimeActor_OnPlayerLeftVolume_Params
{
	class ADaytimeVolume*                              Volume;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.DaytimeActor.OnPlayerEnteredVolume
struct ADaytimeActor_OnPlayerEnteredVolume_Params
{
	class ADaytimeVolume*                              Volume;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.DaytimeVolume.OnBrushEndOverlap
struct ADaytimeVolume_OnBrushEndOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.DaytimeVolume.OnBrushBeginOverlap
struct ADaytimeVolume_OnBrushBeginOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFromSweep;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  SweepResult;                                              // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.Door.OpenDoor
struct ADoor_OpenDoor_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.Door.OnRep_bIsOpen
struct ADoor_OnRep_bIsOpen_Params
{
};

// Function IONBranch.Door.OnDoorOpened
struct ADoor_OnDoorOpened_Params
{
};

// Function IONBranch.Door.OnDoorClosed
struct ADoor_OnDoorClosed_Params
{
};

// Function IONBranch.Door.CloseDoor
struct ADoor_CloseDoor_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.IsFriendPlayingThisGame
struct UFriendsFunctionLibrary_IsFriendPlayingThisGame_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.IsFriendPlaying
struct UFriendsFunctionLibrary_IsFriendPlaying_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.IsFriendOnline
struct UFriendsFunctionLibrary_IsFriendOnline_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.IsFriendJoinable
struct UFriendsFunctionLibrary_IsFriendJoinable_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.IsFriendInvitable
struct UFriendsFunctionLibrary_IsFriendInvitable_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.GetFriendName
struct UFriendsFunctionLibrary_GetFriendName_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.FriendsFunctionLibrary.GetFriendInviteStatus
struct UFriendsFunctionLibrary_GetFriendInviteStatus_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	EBlueprintInviteStatus                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.FriendsFunctionLibrary.GetFriendId
struct UFriendsFunctionLibrary_GetFriendId_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.FriendsFunctionLibrary.GetFriendAvatar
struct UFriendsFunctionLibrary_GetFriendAvatar_Params
{
	class UObject*                                     Outer;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
	ESteamAvatarSize                                   Size;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IndoorVolume.OnComponentEndOverlap
struct AIndoorVolume_OnComponentEndOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IndoorVolume.OnComponentBeginOverlap
struct AIndoorVolume_OnComponentBeginOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFromSweep;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  SweepResult;                                              // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.IONAdminComponent.ServerTogglePlasma
struct UIONAdminComponent_ServerTogglePlasma_Params
{
};

// Function IONBranch.IONAdminComponent.ServerStartMatch
struct UIONAdminComponent_ServerStartMatch_Params
{
};

// Function IONBranch.IONAdminComponent.ServerSay
struct UIONAdminComponent_ServerSay_Params
{
	struct FString                                     Command;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONAdminComponent.ServerPausePrequeue
struct UIONAdminComponent_ServerPausePrequeue_Params
{
};

// Function IONBranch.IONAdminComponent.ServerLogAdminCommand
struct UIONAdminComponent_ServerLogAdminCommand_Params
{
	struct FString                                     Command;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONAdminComponent.ServerGiveSkin
struct UIONAdminComponent_ServerGiveSkin_Params
{
	int                                                SteamDefID;                                               // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAdminComponent.ServerExec
struct UIONAdminComponent_ServerExec_Params
{
	struct FString                                     Command;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONAdminComponent.DebugGiveSkin
struct UIONAdminComponent_DebugGiveSkin_Params
{
	int                                                SteamDefID;                                               // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAdminComponent.AdminTogglePlasma
struct UIONAdminComponent_AdminTogglePlasma_Params
{
};

// Function IONBranch.IONAdminComponent.AdminStartMatch
struct UIONAdminComponent_AdminStartMatch_Params
{
};

// Function IONBranch.IONAdminComponent.AdminSay
struct UIONAdminComponent_AdminSay_Params
{
	struct FString                                     Command;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONAdminComponent.AdminPausePrequeue
struct UIONAdminComponent_AdminPausePrequeue_Params
{
};

// Function IONBranch.IONAdminComponent.AdminExec
struct UIONAdminComponent_AdminExec_Params
{
	struct FString                                     Command;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONItem.StopOutline
struct AIONItem_StopOutline_Params
{
};

// Function IONBranch.IONItem.StartOutline
struct AIONItem_StartOutline_Params
{
};

// Function IONBranch.IONItem.RemoveFromInventory
struct AIONItem_RemoveFromInventory_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.PlaceOnGround
struct AIONItem_PlaceOnGround_Params
{
};

// Function IONBranch.IONItem.IsInInventory
struct AIONItem_IsInInventory_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.IsEquivalentTo
struct AIONItem_IsEquivalentTo_Params
{
	class AIONItem*                                    Other;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.Internal_PickUp
struct AIONItem_Internal_PickUp_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItem.GetPickupLocation
struct AIONItem_GetPickupLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.GetItemTypeText
struct AIONItem_GetItemTypeText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONItem.GetItemPickupEvent
struct AIONItem_GetItemPickupEvent_Params
{
	class UAkAudioEvent*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.GetItemDescription
struct AIONItem_GetItemDescription_Params
{
	struct FItemDescription                            ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONItem.GetCharacterChecked
struct AIONItem_GetCharacterChecked_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.GetCharacter
struct AIONItem_GetCharacter_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.DestroyItemSafely
struct AIONItem_DestroyItemSafely_Params
{
	float                                              LifeSpan;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItem.CanBePickedUp
struct AIONItem_CanBePickedUp_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItem.CanBeDropped
struct AIONItem_CanBeDropped_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.SetNumberOfItems
struct AIONStackableItem_SetNumberOfItems_Params
{
	int                                                Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.RemoveItems
struct AIONStackableItem_RemoveItems_Params
{
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.RemoveAllItems
struct AIONStackableItem_RemoveAllItems_Params
{
};

// Function IONBranch.IONStackableItem.IsStackable
struct AIONStackableItem_IsStackable_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.GetNumberOfItems
struct AIONStackableItem_GetNumberOfItems_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.GetMaxItems
struct AIONStackableItem_GetMaxItems_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.CanBeMergedWith
struct AIONStackableItem_CanBeMergedWith_Params
{
	class AIONStackableItem*                           OtherItem;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStackableItem.AddItems
struct AIONStackableItem_AddItems_Params
{
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationBaseWidget.SetArmorVisibility
struct UIONCustomizationBaseWidget_SetArmorVisibility_Params
{
	bool                                               bNewVisibility;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	class UClass*                                      WearableToHide;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationBaseWidget.GetIONPC
struct UIONCustomizationBaseWidget_GetIONPC_Params
{
	class AIONBasePlayerController*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCustomizationBaseWidget.GetDisplayChar
struct UIONCustomizationBaseWidget_GetDisplayChar_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCustomizationBaseWidget.GetCustomizationPawn
struct UIONCustomizationBaseWidget_GetCustomizationPawn_Params
{
	class AIONCustomizationPawn*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.UpdateSkinColorLoadoutIdx
struct UIONAppearanceScreen_UpdateSkinColorLoadoutIdx_Params
{
	unsigned char                                      NewIdx;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.UpdateHairLoadoutIdx
struct UIONAppearanceScreen_UpdateHairLoadoutIdx_Params
{
	unsigned char                                      NewIdx;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.UpdateFaceLoadoutIdx
struct UIONAppearanceScreen_UpdateFaceLoadoutIdx_Params
{
	unsigned char                                      NewIdx;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.GetSkinColorLoadoutIdx
struct UIONAppearanceScreen_GetSkinColorLoadoutIdx_Params
{
	unsigned char                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.GetHairLoadoutIdx
struct UIONAppearanceScreen_GetHairLoadoutIdx_Params
{
	unsigned char                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIdx
struct UIONAppearanceScreen_GetFaceLoadoutIdx_Params
{
	unsigned char                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAppearanceScreen.GetFaceLoadoutIcon
struct UIONAppearanceScreen_GetFaceLoadoutIcon_Params
{
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWearable.SetMatParamOnBaseChar
struct AIONWearable_SetMatParamOnBaseChar_Params
{
	struct FName                                       ParamName;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWearable.GetHealthRatio
struct AIONWearable_GetHealthRatio_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWearable.GetCharacterOwner
struct AIONWearable_GetCharacterOwner_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWearable.BPEvent_DetachWearable
struct AIONWearable_BPEvent_DetachWearable_Params
{
};

// Function IONBranch.IONWearable.BPEvent_AttachWearable
struct AIONWearable_BPEvent_AttachWearable_Params
{
};

// Function IONBranch.IONArmor.UpdateArmorSkin
struct AIONArmor_UpdateArmorSkin_Params
{
};

// Function IONBranch.IONArmor.RestoreDefaultSkin
struct AIONArmor_RestoreDefaultSkin_Params
{
};

// Function IONBranch.IONArmor.BPEvent_ApplyArmorMesh
struct AIONArmor_BPEvent_ApplyArmorMesh_Params
{
};

// Function IONBranch.IONArmor.ApplyArmorSkin
struct AIONArmor_ApplyArmorSkin_Params
{
	class UIONArmorSkin*                               NewArmorSkin;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bForce;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSteamItem.GetRarityColor
struct UIONSteamItem_GetRarityColor_Params
{
	struct FColor                                      ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAttachment.IsCompatibleWith
struct AIONAttachment_IsCompatibleWith_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAttachment.GetAttachmentSlot
struct AIONAttachment_GetAttachmentSlot_Params
{
	TEnumAsByte<EAttachmentSlot>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONAttachment.DetachFromFirearmBlueprint
struct AIONAttachment_DetachFromFirearmBlueprint_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAttachment.AttachToFirearmBlueprint
struct AIONAttachment_AttachToFirearmBlueprint_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAudioVolume.OnComponentEndOverlap
struct AIONAudioVolume_OnComponentEndOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONAudioVolume.OnComponentBeginOverlap
struct AIONAudioVolume_OnComponentBeginOverlap_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFromSweep;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  SweepResult;                                              // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.SquadEliminated
struct AIONGameRuleSet_SquadEliminated_Params
{
	class AIONSquadState*                              Squad;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.SpawnPlayer
struct AIONGameRuleSet_SpawnPlayer_Params
{
	class ABRPlayerController*                         NewPlayer;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.PostPlayerLogin
struct AIONGameRuleSet_PostPlayerLogin_Params
{
	class APlayerController*                           Controller;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.PlayerSpawned
struct AIONGameRuleSet_PlayerSpawned_Params
{
	class ABRPlayerController*                         NewPlayer;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class APawn*                                       Pawn;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.PlayerLogout
struct AIONGameRuleSet_PlayerLogout_Params
{
	class AController*                                 Controller;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.PlayerJoined
struct AIONGameRuleSet_PlayerJoined_Params
{
	class AMainPlayerController*                       NewPlayer;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.MatchStarted
struct AIONGameRuleSet_MatchStarted_Params
{
};

// Function IONBranch.IONGameRuleSet.MatchLeft
struct AIONGameRuleSet_MatchLeft_Params
{
};

// Function IONBranch.IONGameRuleSet.MatchJoined
struct AIONGameRuleSet_MatchJoined_Params
{
};

// Function IONBranch.IONGameRuleSet.HasBattleRoyaleStarted
struct AIONGameRuleSet_HasBattleRoyaleStarted_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.GetGameMode
struct AIONGameRuleSet_GetGameMode_Params
{
	class AIONGameMode*                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.GetDisplayName
struct AIONGameRuleSet_GetDisplayName_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONGameRuleSet.CheckMatchOver
struct AIONGameRuleSet_CheckMatchOver_Params
{
};

// Function IONBranch.IONGameRuleSet.CharacterTookDamage
struct AIONGameRuleSet_CharacterTookDamage_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitInfo                                    HitInfo;                                                  // (Parm)
	float                                              DamageAmount;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.CharacterRevived
struct AIONGameRuleSet_CharacterRevived_Params
{
	class AIONCharacter*                               DownedCharacter;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONCharacter*                               ReviverCharacters;                                        // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameRuleSet.CharacterDowned
struct AIONGameRuleSet_CharacterDowned_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONGameRuleSet.CharacterDied
struct AIONGameRuleSet_CharacterDied_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONBehaviorButton.HandleClick
struct UIONBehaviorButton_HandleClick_Params
{
};

// Function IONBranch.IONButton.OnReleased
struct AIONButton_OnReleased_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONButton.OnPressed
struct AIONButton_OnPressed_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCameraMan.SpectatePlayer
struct AIONCameraMan_SpectatePlayer_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCameraMan.SpectateModeToString
struct AIONCameraMan_SpectateModeToString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCameraMan.ServerSetFlySpeedTarget
struct AIONCameraMan_ServerSetFlySpeedTarget_Params
{
	float                                              InFlySpeedTarget;                                         // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCameraMan.ServerDetachFromPlayer
struct AIONCameraMan_ServerDetachFromPlayer_Params
{
};

// Function IONBranch.IONCameraMan.ServerAttachToPlayer
struct AIONCameraMan_ServerAttachToPlayer_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFirstPerson;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCameraMan.PlayercardViewModeToString
struct AIONCameraMan_PlayercardViewModeToString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCameraMan.OnOptionChanged
struct AIONCameraMan_OnOptionChanged_Params
{
	int                                                OptionIndex;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCameraMan.HUDViewToString
struct AIONCameraMan_HUDViewToString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCameraMan.DrawHUD
struct AIONCameraMan_DrawHUD_Params
{
	class AIONObserverCamHUD*                          PlayerHUD;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UCanvas*                                     Canvas;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.WantsToSprint
struct AIONCharacter_WantsToSprint_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.WantsToProne
struct AIONCharacter_WantsToProne_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.WantsToFire
struct AIONCharacter_WantsToFire_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.UpdateSuppressionEffects
struct AIONCharacter_UpdateSuppressionEffects_Params
{
	float                                              SuppressionRatio;                                         // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.UpdateShieldEffects
struct AIONCharacter_UpdateShieldEffects_Params
{
};

// Function IONBranch.IONCharacter.UpdateHealthEffects
struct AIONCharacter_UpdateHealthEffects_Params
{
	float                                              HealthRatio;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              OldHealthRatio;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.UpdateFirstPersonVisibility
struct AIONCharacter_UpdateFirstPersonVisibility_Params
{
};

// Function IONBranch.IONCharacter.UpdateEquipementSound_Wearable
struct AIONCharacter_UpdateEquipementSound_Wearable_Params
{
};

// Function IONBranch.IONCharacter.UpdateEquipementSound_Firearms
struct AIONCharacter_UpdateEquipementSound_Firearms_Params
{
};

// Function IONBranch.IONCharacter.UpdateEquipementSound_BoosterOrNade
struct AIONCharacter_UpdateEquipementSound_BoosterOrNade_Params
{
};

// Function IONBranch.IONCharacter.UpdateDropsuitSkin
struct AIONCharacter_UpdateDropsuitSkin_Params
{
};

// Function IONBranch.IONCharacter.UnloadAmmo
struct AIONCharacter_UnloadAmmo_Params
{
	class AIONFirearm*                                 Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.TimeLeftTillImpact
struct AIONCharacter_TimeLeftTillImpact_Params
{
	float                                              OutTime;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  OutHit;                                                   // (Parm, OutParm, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.StopFiring
struct AIONCharacter_StopFiring_Params
{
};

// Function IONBranch.IONCharacter.StopAiming
struct AIONCharacter_StopAiming_Params
{
};

// Function IONBranch.IONCharacter.SlotIsEnabled
struct AIONCharacter_SlotIsEnabled_Params
{
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SlotAcceptsItem
struct AIONCharacter_SlotAcceptsItem_Params
{
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONItem*                                    Item;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ShouldPlayAnim
struct AIONCharacter_ShouldPlayAnim_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SetWorldOrigin
struct AIONCharacter_SetWorldOrigin_Params
{
	int                                                X;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Y;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Z;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SetSprinting
struct AIONCharacter_SetSprinting_Params
{
	bool                                               NewSprinting;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SetProne
struct AIONCharacter_SetProne_Params
{
	bool                                               NewProne;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SetCrouching
struct AIONCharacter_SetCrouching_Params
{
	bool                                               NewCrouch;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.SetCharacterMesh
struct AIONCharacter_SetCharacterMesh_Params
{
	struct FString                                     MeshName;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONCharacter.ServerUpdateReviving
struct AIONCharacter_ServerUpdateReviving_Params
{
	bool                                               NewReviving;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateMapOpen
struct AIONCharacter_ServerUpdateMapOpen_Params
{
	bool                                               NewMapOpen;                                               // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateInteracting
struct AIONCharacter_ServerUpdateInteracting_Params
{
	bool                                               NewInteracting;                                           // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateInspectingWeapon
struct AIONCharacter_ServerUpdateInspectingWeapon_Params
{
	bool                                               NewInspectingWeapon;                                      // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateFiring
struct AIONCharacter_ServerUpdateFiring_Params
{
	bool                                               NewFiring;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateFireRandomSeed
struct AIONCharacter_ServerUpdateFireRandomSeed_Params
{
	unsigned char                                      NewFireRandomSeed;                                        // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateAttachmentActive
struct AIONCharacter_ServerUpdateAttachmentActive_Params
{
	bool                                               NewAttachmentActive;                                      // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUpdateAiming
struct AIONCharacter_ServerUpdateAiming_Params
{
	bool                                               NewAiming;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	float                                              NewAimRatio;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerUnloadAmmo
struct AIONCharacter_ServerUnloadAmmo_Params
{
	class AIONFirearm*                                 Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerStopRevive
struct AIONCharacter_ServerStopRevive_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONCharacter*                               Other;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerStartRevive
struct AIONCharacter_ServerStartRevive_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONCharacter*                               Other;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              NewReviveTime;                                            // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerSetNewCachedInteractObject
struct AIONCharacter_ServerSetNewCachedInteractObject_Params
{
	class UObject*                                     NewCachedInteractObject;                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerSetIsSlowingBloodLoss
struct AIONCharacter_ServerSetIsSlowingBloodLoss_Params
{
	bool                                               NewIsSlowing;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerSetInspectingSkin
struct AIONCharacter_ServerSetInspectingSkin_Params
{
	bool                                               bNewInspectingSkin;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerSetFreeLook
struct AIONCharacter_ServerSetFreeLook_Params
{
	bool                                               NewFreeLook;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerPickUpSpawn
struct AIONCharacter_ServerPickUpSpawn_Params
{
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerPickUp
struct AIONCharacter_ServerPickUp_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerMoveMagazineToSlot
struct AIONCharacter_ServerMoveMagazineToSlot_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerMoveItemToSlotSpawn
struct AIONCharacter_ServerMoveItemToSlotSpawn_Params
{
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerMoveItemToSlot
struct AIONCharacter_ServerMoveItemToSlot_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerMergeItemsSpawn
struct AIONCharacter_ServerMergeItemsSpawn_Params
{
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONStackableItem*                           To;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerMergeItems
struct AIONCharacter_ServerMergeItems_Params
{
	class AIONStackableItem*                           Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONStackableItem*                           To;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerExitVehicle
struct AIONCharacter_ServerExitVehicle_Params
{
};

// Function IONBranch.IONCharacter.ServerEquipWearableSpawn
struct AIONCharacter_ServerEquipWearableSpawn_Params
{
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerEquipWearable
struct AIONCharacter_ServerEquipWearable_Params
{
	class AIONWearable*                                Wearable;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerEquipWeapon
struct AIONCharacter_ServerEquipWeapon_Params
{
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerEnterVehicle
struct AIONCharacter_ServerEnterVehicle_Params
{
	class AVehicleBase*                                Vehicle;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       SeatName;                                                 // (ConstParm, Parm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerDropStackable
struct AIONCharacter_ServerDropStackable_Params
{
	class AIONStackableItem*                           Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerDropItem
struct AIONCharacter_ServerDropItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bShuffle;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerDropAmmoPack
struct AIONCharacter_ServerDropAmmoPack_Params
{
	EFirearmType                                       AmmoType;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearmSpawn
struct AIONCharacter_ServerAttachAttachmentToFirearmSpawn_Params
{
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ServerAttachAttachmentToFirearm
struct AIONCharacter_ServerAttachAttachmentToFirearm_Params
{
	class AIONAttachment*                              Attachment;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ReviveReleased
struct AIONCharacter_ReviveReleased_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.RevivePressed
struct AIONCharacter_RevivePressed_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ResetBulletPaths
struct AIONCharacter_ResetBulletPaths_Params
{
};

// Function IONBranch.IONCharacter.PickUp
struct AIONCharacter_PickUp_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.OnStepped
struct AIONCharacter_OnStepped_Params
{
};

// DelegateFunction IONBranch.IONCharacter.OnRevivingStopped__DelegateSignature
struct AIONCharacter_OnRevivingStopped__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONCharacter.OnRevivingStarted__DelegateSignature
struct AIONCharacter_OnRevivingStarted__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONCharacter.OnRevived__DelegateSignature
struct AIONCharacter_OnRevived__DelegateSignature_Params
{
};

// Function IONBranch.IONCharacter.OnRep_VehicleSeatName
struct AIONCharacter_OnRep_VehicleSeatName_Params
{
};

// Function IONBranch.IONCharacter.OnRep_Reviving
struct AIONCharacter_OnRep_Reviving_Params
{
};

// Function IONBranch.IONCharacter.OnRep_QueuedWeapon
struct AIONCharacter_OnRep_QueuedWeapon_Params
{
	class AIONWeapon*                                  OldWeapon;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.OnRep_IsSprinting
struct AIONCharacter_OnRep_IsSprinting_Params
{
};

// Function IONBranch.IONCharacter.OnRep_IsProne
struct AIONCharacter_OnRep_IsProne_Params
{
};

// Function IONBranch.IONCharacter.OnRep_IsDowned
struct AIONCharacter_OnRep_IsDowned_Params
{
};

// Function IONBranch.IONCharacter.OnRep_DamageInfoArray
struct AIONCharacter_OnRep_DamageInfoArray_Params
{
};

// Function IONBranch.IONCharacter.OnRep_ChangeDropIn
struct AIONCharacter_OnRep_ChangeDropIn_Params
{
	bool                                               OldIsInDrop;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.OnInventoryChanged
struct AIONCharacter_OnInventoryChanged_Params
{
};

// Function IONBranch.IONCharacter.OnHardLanding
struct AIONCharacter_OnHardLanding_Params
{
};

// DelegateFunction IONBranch.IONCharacter.OnDowned__DelegateSignature
struct AIONCharacter_OnDowned__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONCharacter.OnDied__DelegateSignature
struct AIONCharacter_OnDied__DelegateSignature_Params
{
};

// Function IONBranch.IONCharacter.OnBulletSuppression
struct AIONCharacter_OnBulletSuppression_Params
{
	class AMainProjectile*                             Projectile;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.OnAimingStateChanged
struct AIONCharacter_OnAimingStateChanged_Params
{
	float                                              NewAimRatio;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bForce;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.MulticastWeaponTask
struct AIONCharacter_MulticastWeaponTask_Params
{
	EWeaponTask                                        TaskID;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.MulticastLaunchPadJump
struct AIONCharacter_MulticastLaunchPadJump_Params
{
	struct FVector                                     LaunchPadLocation;                                        // (Parm, IsPlainOldData)
	struct FRotator                                    LaunchPadRotator;                                         // (Parm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.MoveMagazineToSlot
struct AIONCharacter_MoveMagazineToSlot_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.MoveItemToSlot
struct AIONCharacter_MoveItemToSlot_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SlotIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.MergeItems
struct AIONCharacter_MergeItems_Params
{
	class AIONStackableItem*                           From;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONStackableItem*                           To;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.LaunchPadJump
struct AIONCharacter_LaunchPadJump_Params
{
	struct FVector                                     LaunchPadLocation;                                        // (Parm, IsPlainOldData)
	struct FRotator                                    LaunchPadRotator;                                         // (Parm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.Kill
struct AIONCharacter_Kill_Params
{
};

// Function IONBranch.IONCharacter.IsSprinting
struct AIONCharacter_IsSprinting_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsSameSquadAs
struct AIONCharacter_IsSameSquadAs_Params
{
	class APlayerState*                                OtherPlayer;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsProne
struct AIONCharacter_IsProne_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsMapOpen
struct AIONCharacter_IsMapOpen_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsInWater
struct AIONCharacter_IsInWater_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsInspectingWeapon
struct AIONCharacter_IsInspectingWeapon_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsInspectingSkin
struct AIONCharacter_IsInspectingSkin_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsInInvincibilityVolume
struct AIONCharacter_IsInInvincibilityVolume_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsInFirstPersonView
struct AIONCharacter_IsInFirstPersonView_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsHoldingMovementKeys
struct AIONCharacter_IsHoldingMovementKeys_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsFreeLooking
struct AIONCharacter_IsFreeLooking_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsFiring
struct AIONCharacter_IsFiring_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsDowned
struct AIONCharacter_IsDowned_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.IsDead
struct AIONCharacter_IsDead_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.InventoryStateChanged
struct AIONCharacter_InventoryStateChanged_Params
{
	bool                                               bOpen;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.HasItem
struct AIONCharacter_HasItem_Params
{
	class UClass*                                      ItemClass;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               OutReturn;                                                // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.HasBattleRoyaleStarted
struct AIONCharacter_HasBattleRoyaleStarted_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GiveItem
struct AIONCharacter_GiveItem_Params
{
	struct FString                                     ClassName;                                                // (Parm, ZeroConstructor)
};

// Function IONBranch.IONCharacter.GetWeaponTask
struct AIONCharacter_GetWeaponTask_Params
{
	EWeaponTask                                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetViewYaw
struct AIONCharacter_GetViewYaw_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetViewPitch
struct AIONCharacter_GetViewPitch_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetStatusIndicatorForItem
struct AIONCharacter_GetStatusIndicatorForItem_Params
{
	class AIONItem*                                    Item;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	EItemStatusIndicator                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetStance
struct AIONCharacter_GetStance_Params
{
	ECharacterStance                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetProximityItems
struct AIONCharacter_GetProximityItems_Params
{
	float                                              MaxRange;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<class AIONItem*>                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCharacter.GetKillerPlayer
struct AIONCharacter_GetKillerPlayer_Params
{
	class APlayerState*                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetInteractionItem
struct AIONCharacter_GetInteractionItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetHealth
struct AIONCharacter_GetHealth_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetFreeLookAmmount
struct AIONCharacter_GetFreeLookAmmount_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetDamageDealthByPlayer
struct AIONCharacter_GetDamageDealthByPlayer_Params
{
	class APlayerState*                                Player;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetCurrentWeapon
struct AIONCharacter_GetCurrentWeapon_Params
{
	class AIONWeapon*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetCameraManager
struct AIONCharacter_GetCameraManager_Params
{
	class APlayerCameraManager*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetAimRatio
struct AIONCharacter_GetAimRatio_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.GetAiming
struct AIONCharacter_GetAiming_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.FindEmptySlotForItem
struct AIONCharacter_FindEmptySlotForItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.EventBP_LandingTrigger
struct AIONCharacter_EventBP_LandingTrigger_Params
{
};

// Function IONBranch.IONCharacter.EventBP_LandedOnTree
struct AIONCharacter_EventBP_LandedOnTree_Params
{
	struct FTransform                                  TreeTransform;                                            // (Parm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.EventBP_DropInModeStarted
struct AIONCharacter_EventBP_DropInModeStarted_Params
{
};

// Function IONBranch.IONCharacter.EventBP_DropInModeEnded
struct AIONCharacter_EventBP_DropInModeEnded_Params
{
};

// Function IONBranch.IONCharacter.EquipWearable
struct AIONCharacter_EquipWearable_Params
{
	class AIONWearable*                                Wearable;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.EquipWeapon
struct AIONCharacter_EquipWeapon_Params
{
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bForce;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.EquipSlot
struct AIONCharacter_EquipSlot_Params
{
	unsigned char                                      Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	EInventoryType                                     InventoryType;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.EquipPreviousWeapon
struct AIONCharacter_EquipPreviousWeapon_Params
{
};

// Function IONBranch.IONCharacter.EquipKnife
struct AIONCharacter_EquipKnife_Params
{
};

// Function IONBranch.IONCharacter.EquipCosmeticItem
struct AIONCharacter_EquipCosmeticItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.DropStackable
struct AIONCharacter_DropStackable_Params
{
	class AIONStackableItem*                           Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.DropItem
struct AIONCharacter_DropItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bShuffle;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bForce;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.DropAmmoPack
struct AIONCharacter_DropAmmoPack_Params
{
	EFirearmType                                       AmmoType;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Amount;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ChangeShieldEffects
struct AIONCharacter_ChangeShieldEffects_Params
{
	bool                                               bNewActive;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ChangeDropInMode
struct AIONCharacter_ChangeDropInMode_Params
{
	bool                                               bEnter;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.CanFreeLook
struct AIONCharacter_CanFreeLook_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.BP_SimulatedHit
struct AIONCharacter_BP_SimulatedHit_Params
{
	struct FName                                       BoneName;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              DamageImpulse;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     ShotFromDirection;                                        // (Parm, IsPlainOldData)
};

// Function IONBranch.IONCharacter.BP_ProceduralFireAnimation
struct AIONCharacter_BP_ProceduralFireAnimation_Params
{
};

// Function IONBranch.IONCharacter.AttachAttachmentToFirearm
struct AIONCharacter_AttachAttachmentToFirearm_Params
{
	class AIONAttachment*                              Attachment;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ApplyDropsuitSkin
struct AIONCharacter_ApplyDropsuitSkin_Params
{
	class UIONDropSkin*                                NewDropsuitSkin;                                          // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacter.ApplyArmorSkin
struct AIONCharacter_ApplyArmorSkin_Params
{
	class UIONArmorSkin*                               NewArmorSkin;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeedCrouched
struct UIONCharacterAnimInstance_GetMaxWalkSpeedCrouched_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacterAnimInstance.GetMaxWalkSpeed
struct UIONCharacterAnimInstance_GetMaxWalkSpeed_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacterAnimInstance.GetMaxSwimSpeed
struct UIONCharacterAnimInstance_GetMaxSwimSpeed_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacterMovementComponent.IsSprinting
struct UIONCharacterMovementComponent_IsSprinting_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacterMovementComponent.IsProne
struct UIONCharacterMovementComponent_IsProne_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCharacterMovementComponent.GetSprintRatio
struct UIONCharacterMovementComponent_GetSprintRatio_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONConnectionOverlayUserWidget.GetOverlayOpacity
struct UIONConnectionOverlayUserWidget_GetOverlayOpacity_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCrateScreenWidget.ScrapItemForCredits
struct UIONCrateScreenWidget_ScrapItemForCredits_Params
{
};

// Function IONBranch.IONCrateScreenWidget.OpenCrate
struct UIONCrateScreenWidget_OpenCrate_Params
{
	class UIONSteamItem*                               CrateToOpen;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	class UIONSteamItem*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONCrateScreenWidget.GetCrateContent
struct UIONCrateScreenWidget_GetCrateContent_Params
{
	class UIONSteamItem*                               Crate;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<class UIONSteamItem*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCrateScreenWidget.BPEvent_OpenCrate
struct UIONCrateScreenWidget_BPEvent_OpenCrate_Params
{
	class UIONSteamItem*                               GainedItem;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCrateScreenWidget.AddItemToInventory
struct UIONCrateScreenWidget_AddItemToInventory_Params
{
};

// Function IONBranch.IONCrateWidget.GetCrateContent
struct UIONCrateWidget_GetCrateContent_Params
{
	TArray<class UIONSteamItem*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONCustomizationInventoryWidget.RefreshInventory
struct UIONCustomizationInventoryWidget_RefreshInventory_Params
{
};

// Function IONBranch.IONCustomizationInventoryWidget.BPEvent_RefreshInventory
struct UIONCustomizationInventoryWidget_BPEvent_RefreshInventory_Params
{
};

// Function IONBranch.IONCustomizationPawn.SkinsDebug
struct AIONCustomizationPawn_SkinsDebug_Params
{
	int                                                Mode;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.SetupDisplayCharacter
struct AIONCustomizationPawn_SetupDisplayCharacter_Params
{
};

// Function IONBranch.IONCustomizationPawn.SetArmorVisiblity
struct AIONCustomizationPawn_SetArmorVisiblity_Params
{
	bool                                               bNewVisibility;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	class UClass*                                      WearableToHide;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.PreviewSkinOnChar
struct AIONCustomizationPawn_PreviewSkinOnChar_Params
{
	class UIONSteamItem*                               NewItem;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.InspectWearable
struct AIONCustomizationPawn_InspectWearable_Params
{
	struct FIONSteamInventoryItem                      Wearable;                                                 // (Parm)
};

// Function IONBranch.IONCustomizationPawn.InspectWeaponSkin
struct AIONCustomizationPawn_InspectWeaponSkin_Params
{
	class UIONSteamItem*                               Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.InspectCrate
struct AIONCustomizationPawn_InspectCrate_Params
{
	struct FIONSteamInventoryItem                      Crate;                                                    // (Parm)
};

// Function IONBranch.IONCustomizationPawn.DeleteInspectWeapon
struct AIONCustomizationPawn_DeleteInspectWeapon_Params
{
};

// Function IONBranch.IONCustomizationPawn.BPEvent_SkinsDebug
struct AIONCustomizationPawn_BPEvent_SkinsDebug_Params
{
	int                                                Mode;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_PreviewWeaponSkin
struct AIONCustomizationPawn_BPEvent_PreviewWeaponSkin_Params
{
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_InspectWeaponSkin
struct AIONCustomizationPawn_BPEvent_InspectWeaponSkin_Params
{
	class UIONWeaponSkin*                              WeaponSkin;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_InspectDropsuit
struct AIONCustomizationPawn_BPEvent_InspectDropsuit_Params
{
	class UIONDropSkin*                                DropSkin;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_InspectCrate
struct AIONCustomizationPawn_BPEvent_InspectCrate_Params
{
	struct FIONSteamInventoryItem                      Crate;                                                    // (Parm)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_InspectArmor
struct AIONCustomizationPawn_BPEvent_InspectArmor_Params
{
	class UIONArmorSkin*                               ArmorSkin;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONCustomizationPawn.BPEvent_ApplyCurrentLoadout
struct AIONCustomizationPawn_BPEvent_ApplyCurrentLoadout_Params
{
};

// Function IONBranch.IONCustomizationPawn.ApplyCurrentLoadout
struct AIONCustomizationPawn_ApplyCurrentLoadout_Params
{
};

// Function IONBranch.IONDataSingleton.GetRankForScore
struct UIONDataSingleton_GetRankForScore_Params
{
	int                                                Score;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FRank                                       ReturnValue;                                              // (ConstParm, Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONDataSingleton.GetNextRankForScore
struct UIONDataSingleton_GetNextRankForScore_Params
{
	int                                                Score;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FRank                                       ReturnValue;                                              // (ConstParm, Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONDataSingleton.GetConfigForScoreEvent
struct UIONDataSingleton_GetConfigForScoreEvent_Params
{
	EScoreEvent                                        ScoreEvent;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FScoreEventConfig                           ReturnValue;                                              // (ConstParm, Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONEvent.RemoveFromActiveEvents
struct AIONEvent_RemoveFromActiveEvents_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEvent.GetEventManager
struct AIONEvent_GetEventManager_Params
{
	class AIONEventManager*                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDrone.SpawnItemFromLootTable
struct AIONDrone_SpawnItemFromLootTable_Params
{
	TArray<struct FLootTableEntry>                     LootTable;                                                // (Parm, OutParm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDrone.OnRep_Health
struct AIONDrone_OnRep_Health_Params
{
};

// Function IONBranch.IONDrone.OnRep_Crashed
struct AIONDrone_OnRep_Crashed_Params
{
};

// Function IONBranch.IONDrone.IsDead
struct AIONDrone_IsDead_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDrone.GetMaxHealth
struct AIONDrone_GetMaxHealth_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDrone.FindNewTargetTime
struct AIONDrone_FindNewTargetTime_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDrone.FindNewTargetLocation
struct AIONDrone_FindNewTargetLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEventSpawnBehavior.SpawnEvent
struct UIONEventSpawnBehavior_SpawnEvent_Params
{
	class AIONEvent*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEventSpawnBehavior.GetWorld
struct UIONEventSpawnBehavior_GetWorld_Params
{
	class UWorld*                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEventSpawnBehavior.GetEventManager
struct UIONEventSpawnBehavior_GetEventManager_Params
{
	class AIONEventManager*                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEventSpawnBehavior.CanSpawn
struct UIONEventSpawnBehavior_CanSpawn_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDroneSpawnBehavior.SpawnEvent
struct UIONDroneSpawnBehavior_SpawnEvent_Params
{
	class AIONEvent*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONDroneSpawnBehavior.CanSpawn
struct UIONDroneSpawnBehavior_CanSpawn_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONEventManager.Update
struct AIONEventManager_Update_Params
{
};

// Function IONBranch.IONEventManager.GetActiveEventsOfClass
struct AIONEventManager_GetActiveEventsOfClass_Params
{
	class UClass*                                      Class;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<class AIONEvent*>                           OutEvents;                                                // (Parm, OutParm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.UpdateWeaponSkinMeshes
struct AIONWeapon_UpdateWeaponSkinMeshes_Params
{
	TArray<class UMeshComponent*>                      MeshList;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONWeapon.UpdateWeaponSkin
struct AIONWeapon_UpdateWeaponSkin_Params
{
};

// Function IONBranch.IONWeapon.ServerStartTask
struct AIONWeapon_ServerStartTask_Params
{
	EWeaponTask                                        TaskID;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.RestoreDefaultSkin
struct AIONWeapon_RestoreDefaultSkin_Params
{
};

// DelegateFunction IONBranch.IONWeapon.OnWeaponSkinChanged__DelegateSignature
struct AIONWeapon_OnWeaponSkinChanged__DelegateSignature_Params
{
	class UIONWeaponSkin*                              WeaponSkin;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.MulticastConfirmTask
struct AIONWeapon_MulticastConfirmTask_Params
{
	EWeaponTask                                        TaskID;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.IsEquipped
struct AIONWeapon_IsEquipped_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetViewedMesh
struct AIONWeapon_GetViewedMesh_Params
{
	class USkeletalMeshComponent*                      ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetPickupLocation
struct AIONWeapon_GetPickupLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetLeftHandSocketLocation
struct AIONWeapon_GetLeftHandSocketLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetFPMesh
struct AIONWeapon_GetFPMesh_Params
{
	class USkeletalMeshComponent*                      ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetCurrentWeaponSkin
struct AIONWeapon_GetCurrentWeaponSkin_Params
{
	class UIONWeaponSkin*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.GetAlternateIdle
struct AIONWeapon_GetAlternateIdle_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONWeapon.Get3PMesh
struct AIONWeapon_Get3PMesh_Params
{
	class USkeletalMeshComponent*                      ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONWeapon.ClientStartTask
struct AIONWeapon_ClientStartTask_Params
{
	EWeaponTask                                        TaskID;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.ClientConfirmTask
struct AIONWeapon_ClientConfirmTask_Params
{
	EWeaponTask                                        TaskID;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.BPEvent_OnPickedUp
struct AIONWeapon_BPEvent_OnPickedUp_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWeapon.BPEvent_ApplyWeaponSkinMaterial
struct AIONWeapon_BPEvent_ApplyWeaponSkinMaterial_Params
{
};

// Function IONBranch.IONWeapon.ApplyWeaponSkin
struct AIONWeapon_ApplyWeaponSkin_Params
{
	class UIONWeaponSkin*                              NewWeaponSkin;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bForce;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.ShouldUseSightAdapter
struct AIONFirearm_ShouldUseSightAdapter_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.ServerNotifyHit
struct AIONFirearm_ServerNotifyHit_Params
{
	struct FHitResult                                  Hit;                                                      // (ConstParm, Parm, IsPlainOldData)
	struct FVector                                     WorldLocation;                                            // (ConstParm, Parm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.OnRep_UnderBarrelRail
struct AIONFirearm_OnRep_UnderBarrelRail_Params
{
	class AIONAttachment*                              OldAttachment;                                            // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.OnRep_Sight
struct AIONFirearm_OnRep_Sight_Params
{
	class AIONSight*                                   OldSight;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.OnRep_Siderail
struct AIONFirearm_OnRep_Siderail_Params
{
	class AIONAttachment*                              OldAttachment;                                            // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.OnRep_ExtendedMagazine
struct AIONFirearm_OnRep_ExtendedMagazine_Params
{
	class AIONExtendedMagazine*                        OldMag;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.OnRep_Barrel
struct AIONFirearm_OnRep_Barrel_Params
{
	class AIONBarrel*                                  OldBarrel;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONFirearm.MulticastImpactNotify
struct AIONFirearm_MulticastImpactNotify_Params
{
	struct FHitResult                                  Hit;                                                      // (ConstParm, Parm, IsPlainOldData)
	struct FVector                                     WorldLocation;                                            // (ConstParm, Parm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.HasAttachmentSlot
struct AIONFirearm_HasAttachmentSlot_Params
{
	TEnumAsByte<EAttachmentSlot>                       Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetRoundIcon
struct AIONFirearm_GetRoundIcon_Params
{
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetRoundChambered
struct AIONFirearm_GetRoundChambered_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetReloadTime
struct AIONFirearm_GetReloadTime_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetRangeStat
struct AIONFirearm_GetRangeStat_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetMagazineCapacity
struct AIONFirearm_GetMagazineCapacity_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetMagazine
struct AIONFirearm_GetMagazine_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetFireRateStat
struct AIONFirearm_GetFireRateStat_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetDamageStat
struct AIONFirearm_GetDamageStat_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAttachmentSocketName
struct AIONFirearm_GetAttachmentSocketName_Params
{
	TEnumAsByte<EAttachmentSlot>                       Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAttachments
struct AIONFirearm_GetAttachments_Params
{
	TArray<class AIONAttachment*>                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONFirearm.GetAttachmentComponent
struct AIONFirearm_GetAttachmentComponent_Params
{
	TEnumAsByte<EAttachmentSlot>                       Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UStaticMeshComponent*                        OutMesh;                                                  // (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        OutMeshFP;                                                // (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAttachment
struct AIONFirearm_GetAttachment_Params
{
	TEnumAsByte<EAttachmentSlot>                       Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONAttachment*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAmmoTypeText
struct AIONFirearm_GetAmmoTypeText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONFirearm.GetAmmoBoxIcon
struct AIONFirearm_GetAmmoBoxIcon_Params
{
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAmmoBoxClassPath
struct AIONFirearm_GetAmmoBoxClassPath_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONFirearm.GetAmmoBoxClass
struct AIONFirearm_GetAmmoBoxClass_Params
{
	class UClass*                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAccuracyStat
struct AIONFirearm_GetAccuracyStat_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFirearm.GetAccuracy
struct AIONFirearm_GetAccuracy_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONFriendListUserWidget.OnUpdateFriendList
struct UIONFriendListUserWidget_OnUpdateFriendList_Params
{
};

// Function IONBranch.IONGameInstance.UpdateSteamInventoryItems
struct UIONGameInstance_UpdateSteamInventoryItems_Params
{
};

// Function IONBranch.IONGameInstance.UpdatePlayerStats
struct UIONGameInstance_UpdatePlayerStats_Params
{
	class AIONPlayerState*                             PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	class AIONPlayerState*                             Winner;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.UpdateLoadingScreen
struct UIONGameInstance_UpdateLoadingScreen_Params
{
	struct FString                                     NewMessage;                                               // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameInstance.OnLevelActorRemoved
struct UIONGameInstance_OnLevelActorRemoved_Params
{
	class AActor*                                      InActor;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.OnGameSparksAvailable
struct UIONGameInstance_OnGameSparksAvailable_Params
{
	bool                                               bAvailable;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.GetMatchmaking
struct UIONGameInstance_GetMatchmaking_Params
{
	class UIONMatchmaking*                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.GetItemBySteamDefID
struct UIONGameInstance_GetItemBySteamDefID_Params
{
	int                                                ID;                                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UIONSteamItem*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.EndLoadingScreen
struct UIONGameInstance_EndLoadingScreen_Params
{
};

// Function IONBranch.IONGameInstance.DownloadPlayerStats
struct UIONGameInstance_DownloadPlayerStats_Params
{
	class AIONPlayerState*                             PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameInstance.BeginLoadingScreen
struct UIONGameInstance_BeginLoadingScreen_Params
{
	struct FString                                     MapName;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.UnbanPlayer
struct AIONGameMode_UnbanPlayer_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.ToggleQueueTimer
struct AIONGameMode_ToggleQueueTimer_Params
{
};

// Function IONBranch.IONGameMode.TogglePlasmaField
struct AIONGameMode_TogglePlasmaField_Params
{
};

// Function IONBranch.IONGameMode.StartBattleRoyale
struct AIONGameMode_StartBattleRoyale_Params
{
};

// Function IONBranch.IONGameMode.SpawnBeamDrop
struct AIONGameMode_SpawnBeamDrop_Params
{
};

// Function IONBranch.IONGameMode.SetMaxPlayers
struct AIONGameMode_SetMaxPlayers_Params
{
	int                                                MaxPlayers;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameMode.SetDamageFactor
struct AIONGameMode_SetDamageFactor_Params
{
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameMode.SendStatsToCameramen
struct AIONGameMode_SendStatsToCameramen_Params
{
};

// Function IONBranch.IONGameMode.RestartMatch
struct AIONGameMode_RestartMatch_Params
{
};

// Function IONBranch.IONGameMode.RemoveAdmin
struct AIONGameMode_RemoveAdmin_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.KillPlayer
struct AIONGameMode_KillPlayer_Params
{
	struct FString                                     PlayerName;                                               // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.KickPlayer
struct AIONGameMode_KickPlayer_Params
{
	struct FString                                     PlayerName;                                               // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.HasBattleRoyaleStarted
struct AIONGameMode_HasBattleRoyaleStarted_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameMode.GivePoints
struct AIONGameMode_GivePoints_Params
{
	int                                                Num;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameMode.ForceSkinDrops
struct AIONGameMode_ForceSkinDrops_Params
{
};

// Function IONBranch.IONGameMode.ForceRestartMatch
struct AIONGameMode_ForceRestartMatch_Params
{
};

// Function IONBranch.IONGameMode.BanPlayer
struct AIONGameMode_BanPlayer_Params
{
	struct FString                                     PlayerName;                                               // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameMode.AddAdmin
struct AIONGameMode_AddAdmin_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameOverlayWidget.StartMessage
struct UIONGameOverlayWidget_StartMessage_Params
{
	struct FName                                       Type;                                                     // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.SetMapTooltipVisibility
struct UIONGameOverlayWidget_SetMapTooltipVisibility_Params
{
	bool                                               bInVisible;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.SetChatVisibility
struct UIONGameOverlayWidget_SetChatVisibility_Params
{
	bool                                               bInVisible;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.OnScoreEventReceived
struct UIONGameOverlayWidget_OnScoreEventReceived_Params
{
	EScoreEvent                                        Type;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Points;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.OnReceivedChatMessage
struct UIONGameOverlayWidget_OnReceivedChatMessage_Params
{
	class AIONPlayerState*                             From;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	struct FName                                       Type;                                                     // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.OnPlayerDowned
struct UIONGameOverlayWidget_OnPlayerDowned_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONGameOverlayWidget.OnPlayerDied
struct UIONGameOverlayWidget_OnPlayerDied_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONGameOverlayWidget.OnMicrophoneStateChanged
struct UIONGameOverlayWidget_OnMicrophoneStateChanged_Params
{
	bool                                               bEnabled;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameOverlayWidget.OnKillConfirmed
struct UIONGameOverlayWidget_OnKillConfirmed_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONGameOverlayWidget.OnDownConfirmed
struct UIONGameOverlayWidget_OnDownConfirmed_Params
{
	struct FHitInfo                                    LastHitInfo;                                              // (Parm)
};

// Function IONBranch.IONGameOverlayWidget.DisplaySubtitle
struct UIONGameOverlayWidget_DisplaySubtitle_Params
{
	struct FText                                       SubtitleText;                                             // (ConstParm, Parm, OutParm, ReferenceParm)
};

// Function IONBranch.GSGetLatestMatchRequest.GetLatestMatch
struct UGSGetLatestMatchRequest_GetLatestMatch_Params
{
	class UGSGetLatestMatchRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.GSGetGameConfigRequest.GetGameConfig
struct UGSGetGameConfigRequest_GetGameConfig_Params
{
	class UGSGetGameConfigRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.GSGetPlayerProfileRequest.GetPlayerProfile
struct UGSGetPlayerProfileRequest_GetPlayerProfile_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
	class UGSGetPlayerProfileRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.GSGetPlayerStatsRequest.GetPlayerStats
struct UGSGetPlayerStatsRequest_GetPlayerStats_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
	struct FString                                     GameMode;                                                 // (Parm, ZeroConstructor)
	class UGSGetPlayerStatsRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.GSGetToasterRequest.GetToaster
struct UGSGetToasterRequest_GetToaster_Params
{
	class UGSGetToasterRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.GSGetMenuNewsRequest.GetMenuNews
struct UGSGetMenuNewsRequest_GetMenuNews_Params
{
	class UGSGetMenuNewsRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameService.DebugGetRandomMatchHistory
struct UIONGameService_DebugGetRandomMatchHistory_Params
{
	struct FPlayerMatchHistory                         ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONGameUserSettings.SetStringConfigVariable
struct UIONGameUserSettings_SetStringConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
};

// Function IONBranch.IONGameUserSettings.SetIntConfigVariable
struct UIONGameUserSettings_SetIntConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameUserSettings.SetFloatConfigVariable
struct UIONGameUserSettings_SetFloatConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONGameUserSettings.GetStringConfigVariable
struct UIONGameUserSettings_GetStringConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONGameUserSettings.GetIntConfigVariable
struct UIONGameUserSettings_GetIntConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONGameUserSettings.GetFloatConfigVariable
struct UIONGameUserSettings_GetFloatConfigVariable_Params
{
	struct FName                                       VariableName;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONOnlineUserWidget.Connected
struct UIONOnlineUserWidget_Connected_Params
{
	struct FString                                     UserId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     AuthToken;                                                // (Parm, ZeroConstructor)
};

// Function IONBranch.IONHomescreenUserWidget.HighlightsButtonPressed
struct UIONHomescreenUserWidget_HighlightsButtonPressed_Params
{
};

// Function IONBranch.IONHomescreenUserWidget.CheckIfHighlightsShouldBeVisible
struct UIONHomescreenUserWidget_CheckIfHighlightsShouldBeVisible_Params
{
};

// Function IONBranch.IONInputFunctionLibrary.SetInvertAxis
struct UIONInputFunctionLibrary_SetInvertAxis_Params
{
	struct FName                                       AxisName;                                                 // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	bool                                               bInvert;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.SaveMappings
struct UIONInputFunctionLibrary_SaveMappings_Params
{
};

// Function IONBranch.IONInputFunctionLibrary.RemoveMappings
struct UIONInputFunctionLibrary_RemoveMappings_Params
{
	struct FName                                       Interaction;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FKey                                        Key;                                                      // (ConstParm, Parm)
	bool                                               bSave;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.RebindKey
struct UIONInputFunctionLibrary_RebindKey_Params
{
	struct FInputMapping                               InputMapping;                                             // (ConstParm, Parm, OutParm, ReferenceParm)
	struct FKey                                        Key;                                                      // (ConstParm, Parm, OutParm, ReferenceParm)
	EInputKeyType                                      KeyType;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<struct FName>                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONInputFunctionLibrary.IsKeyEventActionPressed
struct UIONInputFunctionLibrary_IsKeyEventActionPressed_Params
{
	struct FKeyEvent                                   KeyEvent;                                                 // (ConstParm, Parm, OutParm, ReferenceParm)
	struct FName                                       ActionName;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.IsInputMappingInConflict
struct UIONInputFunctionLibrary_IsInputMappingInConflict_Params
{
	struct FInputMapping                               InputMapping;                                             // (ConstParm, Parm, OutParm, ReferenceParm)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.GetInvertAxis
struct UIONInputFunctionLibrary_GetInvertAxis_Params
{
	struct FName                                       AxisName;                                                 // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.GetInputMappings
struct UIONInputFunctionLibrary_GetInputMappings_Params
{
	TArray<struct FInputCategory>                      OutCategories;                                            // (Parm, OutParm, ZeroConstructor)
};

// Function IONBranch.IONInputFunctionLibrary.GetInputMappingDisplayName
struct UIONInputFunctionLibrary_GetInputMappingDisplayName_Params
{
	struct FInputMapping                               InputMapping;                                             // (ConstParm, Parm, OutParm, ReferenceParm)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONInputFunctionLibrary.GetAxisMapping
struct UIONInputFunctionLibrary_GetAxisMapping_Params
{
	struct FName                                       AxisName;                                                 // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	struct FName                                       ActionName;                                               // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	bool                                               bNegativeAxis;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FInputMapping                               ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONInputFunctionLibrary.GetActionMapping
struct UIONInputFunctionLibrary_GetActionMapping_Params
{
	struct FName                                       MappingName;                                              // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	struct FName                                       ActionName;                                               // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	struct FInputMapping                               ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONInputFunctionLibrary.ClearKeyBinding
struct UIONInputFunctionLibrary_ClearKeyBinding_Params
{
	struct FInputMapping                               InputMapping;                                             // (ConstParm, Parm, OutParm, ReferenceParm)
	EInputKeyType                                      KeyType;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInputFunctionLibrary.AddMapping
struct UIONInputFunctionLibrary_AddMapping_Params
{
	struct FName                                       Interaction;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bAxis;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bNegative;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FKey                                        Key;                                                      // (Parm)
	bool                                               bSave;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.ServerMoveItemToSlot
struct UIONInventoryComponent_ServerMoveItemToSlot_Params
{
	class AIONItem*                                    NewItem;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class AItemSpawn*                                  ItemSpawn;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	unsigned char                                      ItemIndex;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.RemoveItem
struct UIONInventoryComponent_RemoveItem_Params
{
	class AIONItem*                                    ItemToRemove;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bCallOnRep;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.RemoveAmmo
struct UIONInventoryComponent_RemoveAmmo_Params
{
	EFirearmType                                       AmmoType;                                                 // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Ammount;                                                  // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// DelegateFunction IONBranch.IONInventoryComponent.OnWearablesChanged__DelegateSignature
struct UIONInventoryComponent_OnWearablesChanged__DelegateSignature_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Wearables
struct UIONInventoryComponent_OnRep_Wearables_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Knife
struct UIONInventoryComponent_OnRep_Knife_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Grenades
struct UIONInventoryComponent_OnRep_Grenades_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Firearms
struct UIONInventoryComponent_OnRep_Firearms_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Boosters
struct UIONInventoryComponent_OnRep_Boosters_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Attachments
struct UIONInventoryComponent_OnRep_Attachments_Params
{
};

// Function IONBranch.IONInventoryComponent.OnRep_Ammo
struct UIONInventoryComponent_OnRep_Ammo_Params
{
	TArray<int>                                        OldAmmo;                                                  // (Parm, ZeroConstructor)
};

// DelegateFunction IONBranch.IONInventoryComponent.OnInventoryChanged__DelegateSignature
struct UIONInventoryComponent_OnInventoryChanged__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONInventoryComponent.OnGrenadesChanged__DelegateSignature
struct UIONInventoryComponent_OnGrenadesChanged__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONInventoryComponent.OnFirearmsChanged__DelegateSignature
struct UIONInventoryComponent_OnFirearmsChanged__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONInventoryComponent.OnBoostersChanged__DelegateSignature
struct UIONInventoryComponent_OnBoostersChanged__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONInventoryComponent.OnAttachmentsChanged__DelegateSignature
struct UIONInventoryComponent_OnAttachmentsChanged__DelegateSignature_Params
{
};

// DelegateFunction IONBranch.IONInventoryComponent.OnAmmoChanged__DelegateSignature
struct UIONInventoryComponent_OnAmmoChanged__DelegateSignature_Params
{
};

// Function IONBranch.IONInventoryComponent.MoveItemToSlot
struct UIONInventoryComponent_MoveItemToSlot_Params
{
	class AIONItem*                                    NewItem;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Slot;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.HasItemOfClass
struct UIONInventoryComponent_HasItemOfClass_Params
{
	class UClass*                                      ItemClass;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetKnife
struct UIONInventoryComponent_GetKnife_Params
{
	class AIONWeapon*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetInventoryAttributes
struct UIONInventoryComponent_GetInventoryAttributes_Params
{
	struct FInventoryAttributes                        ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONInventoryComponent.GetIndexForNewItem
struct UIONInventoryComponent_GetIndexForNewItem_Params
{
	EInventoryType                                     ArrayType;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class AIONItem*                                    Item;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetCharacterChecked
struct UIONInventoryComponent_GetCharacterChecked_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetCharacter
struct UIONInventoryComponent_GetCharacter_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetArrayFromType
struct UIONInventoryComponent_GetArrayFromType_Params
{
	EInventoryType                                     ArrayType;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	TArray<class AIONItem*>                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONInventoryComponent.GetAmmoOfType
struct UIONInventoryComponent_GetAmmoOfType_Params
{
	EFirearmType                                       FirearmType;                                              // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.GetAllItems
struct UIONInventoryComponent_GetAllItems_Params
{
	TArray<class AIONItem*>                            OutItems;                                                 // (Parm, OutParm, ZeroConstructor)
};

// Function IONBranch.IONInventoryComponent.FindItemOfClass
struct UIONInventoryComponent_FindItemOfClass_Params
{
	class UClass*                                      ItemClass;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.CanAddItem
struct UIONInventoryComponent_CanAddItem_Params
{
	class AIONItem*                                    Item;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryComponent.AddItem
struct UIONInventoryComponent_AddItem_Params
{
	class AIONItem*                                    NewItem;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryWidget.ShowInventoryBar
struct UIONInventoryWidget_ShowInventoryBar_Params
{
	bool                                               bInventoryFull;                                           // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryWidget.ShowInventory
struct UIONInventoryWidget_ShowInventory_Params
{
};

// Function IONBranch.IONInventoryWidget.ShouldAssignItemToSlot
struct UIONInventoryWidget_ShouldAssignItemToSlot_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONInventoryWidget.SetDetailsItem
struct UIONInventoryWidget_SetDetailsItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryWidget.OnUpdateWeaponStatsRender
struct UIONInventoryWidget_OnUpdateWeaponStatsRender_Params
{
	class UCanvas*                                     Canvas;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SizeX;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                SizeY;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONInventoryWidget.HideInventory
struct UIONInventoryWidget_HideInventory_Params
{
};

// Function IONBranch.IONInviteReceivedUserWidget.OpenFriendsListButtonClicked
struct UIONInviteReceivedUserWidget_OpenFriendsListButtonClicked_Params
{
};

// Function IONBranch.IONInviteReceivedUserWidget.Dismiss
struct UIONInviteReceivedUserWidget_Dismiss_Params
{
};

// Function IONBranch.IONItemIcon.SetInteractive
struct UIONItemIcon_SetInteractive_Params
{
	bool                                               bNewInteractive;                                          // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItemIcon.SetEquipped
struct UIONItemIcon_SetEquipped_Params
{
	bool                                               bNewEquipped;                                             // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItemIcon.IsInLoadout
struct UIONItemIcon_IsInLoadout_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONItemIcon.BPEvent_EquipChanged
struct UIONItemIcon_BPEvent_EquipChanged_Params
{
	bool                                               bEquipped;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItemWidget.UpdateWeaponSkin
struct UIONItemWidget_UpdateWeaponSkin_Params
{
	class UIONWeaponSkin*                              WeaponSkin;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONItemWidget.UpdateItem
struct UIONItemWidget_UpdateItem_Params
{
	class AIONItem*                                    Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONKnife.IsRecharging
struct AIONKnife_IsRecharging_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONKnife.EventBP_HitSurfaceType
struct AIONKnife_EventBP_HitSurfaceType_Params
{
	TEnumAsByte<EPhysicalSurface>                      SurfaceTypeHit;                                           // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONLoadoutWidget.GetWearableLoadoutIcon
struct UIONLoadoutWidget_GetWearableLoadoutIcon_Params
{
	class UClass*                                      Wearable;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONLoadoutWidget.GetWearableLoadout
struct UIONLoadoutWidget_GetWearableLoadout_Params
{
	TArray<class UIONArmorSkin*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONLoadoutWidget.GetWeaponLoadout
struct UIONLoadoutWidget_GetWeaponLoadout_Params
{
	TArray<class UIONWeaponSkin*>                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONLoadoutWidget.GetLoadoutForWeapon
struct UIONLoadoutWidget_GetLoadoutForWeapon_Params
{
	class UClass*                                      Weapon;                                                   // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FIONSteamInventoryItem                      ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONLoadoutWidget.GetDropsuitLoadoutIcon
struct UIONLoadoutWidget_GetDropsuitLoadoutIcon_Params
{
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONLobbyMemberUserWidget.OnMemberButtonClicked
struct UIONLobbyMemberUserWidget_OnMemberButtonClicked_Params
{
};

// Function IONBranch.IONLobbyMemberUserWidget.LeaveParty
struct UIONLobbyMemberUserWidget_LeaveParty_Params
{
};

// Function IONBranch.IONLobbyMemberUserWidget.KickPlayer
struct UIONLobbyMemberUserWidget_KickPlayer_Params
{
};

// Function IONBranch.IONLobbyUserWidget.PartyPlayerLeave
struct UIONLobbyUserWidget_PartyPlayerLeave_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (Parm)
};

// Function IONBranch.IONLobbyUserWidget.PartyPlayerJoin
struct UIONLobbyUserWidget_PartyPlayerJoin_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (Parm)
};

// Function IONBranch.IONMainMenuUserWidget.GetIONPC
struct UIONMainMenuUserWidget_GetIONPC_Params
{
	class AIONBasePlayerController*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.UpdatePartyMembersArmor
struct UIONMatchmaking_UpdatePartyMembersArmor_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	struct FString                                     ArmorData;                                                // (Parm, ZeroConstructor)
};

// Function IONBranch.IONMatchmaking.SetRegion
struct UIONMatchmaking_SetRegion_Params
{
	EIONPartyRegion                                    region;                                                   // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.SetPartyType
struct UIONMatchmaking_SetPartyType_Params
{
	EIONPartyTypes                                     Type;                                                     // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.SetAutomatch
struct UIONMatchmaking_SetAutomatch_Params
{
	bool                                               bAutomatch;                                               // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyUpdated__DelegateSignature
struct UIONMatchmaking_MatchmakingPartyUpdated__DelegateSignature_Params
{
	struct FMatchmakingParty                           Party;                                                    // (Parm)
};

// DelegateFunction IONBranch.IONMatchmaking.MatchmakingPartyMemberArmorUpdated__DelegateSignature
struct UIONMatchmaking_MatchmakingPartyMemberArmorUpdated__DelegateSignature_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	struct FString                                     ArmorData;                                                // (Parm, ZeroConstructor)
};

// Function IONBranch.IONMatchmaking.IsRegionAllowed
struct UIONMatchmaking_IsRegionAllowed_Params
{
	EIONPartyRegion                                    region;                                                   // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.IsPartyTypeAllowed
struct UIONMatchmaking_IsPartyTypeAllowed_Params
{
	EIONPartyTypes                                     PartyType;                                                // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.IsPartyLeader
struct UIONMatchmaking_IsPartyLeader_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.IsLeader
struct UIONMatchmaking_IsLeader_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.IsAutomatchingAllowed
struct UIONMatchmaking_IsAutomatchingAllowed_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.Invite
struct UIONMatchmaking_Invite_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONMatchmaking.GetPartySize
struct UIONMatchmaking_GetPartySize_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.GetMaxPlayersForCurrentMode
struct UIONMatchmaking_GetMaxPlayersForCurrentMode_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONMatchmaking.DisableRegion
struct UIONMatchmaking_DisableRegion_Params
{
	EIONPartyRegion                                    region;                                                   // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONMatchmakingTestGameInstance.InitializeUI
struct UIONMatchmakingTestGameInstance_InitializeUI_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ToggleAutomatch
struct UIONMatchmakingTestMenuWidget_ToggleAutomatch_Params
{
	bool                                               Checked;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingPartyUpdated
struct UIONMatchmakingTestMenuWidget_OnMatchmakingPartyUpdated_Params
{
	struct FMatchmakingParty                           Party;                                                    // (Parm)
};

// Function IONBranch.IONMatchmakingTestMenuWidget.OnMatchmakingInviteReceived
struct UIONMatchmakingTestMenuWidget_OnMatchmakingInviteReceived_Params
{
	TArray<struct FMatchmakingInvite>                  Invites;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.IONMatchmakingTestMenuWidget.LeaveParty
struct UIONMatchmakingTestMenuWidget_LeaveParty_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.InvitePlayer
struct UIONMatchmakingTestMenuWidget_InvitePlayer_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSquadParty
struct UIONMatchmakingTestMenuWidget_ChangeToSquadParty_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSoloParty
struct UIONMatchmakingTestMenuWidget_ChangeToSoloParty_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSEARegion
struct UIONMatchmakingTestMenuWidget_ChangeToSEARegion_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToSARegion
struct UIONMatchmakingTestMenuWidget_ChangeToSARegion_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToOCERegion
struct UIONMatchmakingTestMenuWidget_ChangeToOCERegion_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToNARegion
struct UIONMatchmakingTestMenuWidget_ChangeToNARegion_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToEURegion
struct UIONMatchmakingTestMenuWidget_ChangeToEURegion_Params
{
};

// Function IONBranch.IONMatchmakingTestMenuWidget.ChangeToDuoParty
struct UIONMatchmakingTestMenuWidget_ChangeToDuoParty_Params
{
};

// Function IONBranch.IONMatchmakingUserWidget.UpdateReadyImage
struct UIONMatchmakingUserWidget_UpdateReadyImage_Params
{
	struct FMatchmakingParty                           Party;                                                    // (Parm)
};

// Function IONBranch.IONMatchmakingUserWidget.ToggleReadiness
struct UIONMatchmakingUserWidget_ToggleReadiness_Params
{
};

// Function IONBranch.IONMatchmakingUserWidget.ShowServerSelection
struct UIONMatchmakingUserWidget_ShowServerSelection_Params
{
};

// Function IONBranch.IONMatchmakingUserWidget.HideServerSelection
struct UIONMatchmakingUserWidget_HideServerSelection_Params
{
};

// Function IONBranch.IONMatchReportWidget.StartLevelProgress
struct UIONMatchReportWidget_StartLevelProgress_Params
{
};

// Function IONBranch.IONMatchReportWidget.SetMatchHistory
struct UIONMatchReportWidget_SetMatchHistory_Params
{
	struct FPlayerMatchHistory                         InMatchHistory;                                           // (Parm)
};

// Function IONBranch.IONMatchReportWidget.OnScoreBucketFilled
struct UIONMatchReportWidget_OnScoreBucketFilled_Params
{
	struct FScoreEvent                                 ScoreEvent;                                               // (Parm)
};

// Function IONBranch.IONMatchReportWidget.OnMatchHistorySet
struct UIONMatchReportWidget_OnMatchHistorySet_Params
{
};

// Function IONBranch.IONMatchReportWidget.OnLevelIncreased
struct UIONMatchReportWidget_OnLevelIncreased_Params
{
};

// Function IONBranch.IONMatchReportWidget.GetMatchHistory
struct UIONMatchReportWidget_GetMatchHistory_Params
{
	struct FPlayerMatchHistory                         ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONMenuLevelScriptActor.OnPlayerArmorUpdated
struct AIONMenuLevelScriptActor_OnPlayerArmorUpdated_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	struct FString                                     UpdatedArmor;                                             // (Parm, ZeroConstructor)
};

// Function IONBranch.IONMenuLevelScriptActor.OnMatchmakingPartyUpdated
struct AIONMenuLevelScriptActor_OnMatchmakingPartyUpdated_Params
{
	struct FMatchmakingParty                           Party;                                                    // (Parm)
};

// Function IONBranch.IONMTPlayerUserWidget.ToggleReadiness
struct UIONMTPlayerUserWidget_ToggleReadiness_Params
{
	bool                                               Checked;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPartySpawnActor.PlayerUpdated
struct AIONPartySpawnActor_PlayerUpdated_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (Parm)
};

// Function IONBranch.IONPartySpawnActor.DestroyPlayerPawn
struct AIONPartySpawnActor_DestroyPlayerPawn_Params
{
};

// Function IONBranch.IONPartySpawnActor.CreatePlayerPawn
struct AIONPartySpawnActor_CreatePlayerPawn_Params
{
	struct FMatchmakingPlayer                          Player;                                                   // (Parm)
};

// Function IONBranch.IONPartySpawnActor.BPEvent_ArmorUpdated
struct AIONPartySpawnActor_BPEvent_ArmorUpdated_Params
{
	struct FString                                     NewArmor;                                                 // (Parm, ZeroConstructor)
};

// Function IONBranch.IONPlayerState.SetVoiceActivity
struct AIONPlayerState_SetVoiceActivity_Params
{
	EIONVoiceActivity                                  NewVoiceActivity;                                         // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.SetSquadState
struct AIONPlayerState_SetSquadState_Params
{
	class AIONSquadState*                              NewSquadState;                                            // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.ServerSetGameSparksID
struct AIONPlayerState_ServerSetGameSparksID_Params
{
	struct FString                                     InGameSparksID;                                           // (Parm, ZeroConstructor)
};

// Function IONBranch.IONPlayerState.OnRep_SquadState
struct AIONPlayerState_OnRep_SquadState_Params
{
	class AIONSquadState*                              OldSquadState;                                            // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.IsStreamer
struct AIONPlayerState_IsStreamer_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.IsSameSquadAs
struct AIONPlayerState_IsSameSquadAs_Params
{
	class APlayerState*                                Other;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.IsQA
struct AIONPlayerState_IsQA_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.IsDeveloper
struct AIONPlayerState_IsDeveloper_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.IsAdmin
struct AIONPlayerState_IsAdmin_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.GetTeamIndex
struct AIONPlayerState_GetTeamIndex_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.GetTeamColor
struct AIONPlayerState_GetTeamColor_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerState.GetPlayerColor
struct AIONPlayerState_GetPlayerColor_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONPlayerWidget.UpdateInteractionWidgetWithText
struct UIONPlayerWidget_UpdateInteractionWidgetWithText_Params
{
	struct FText                                       InteractionText;                                          // (ConstParm, Parm, OutParm, ReferenceParm)
	float                                              DeltaTime;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bHideInteractButton;                                      // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPlayerWidget.UpdateInteractionWidget
struct UIONPlayerWidget_UpdateInteractionWidget_Params
{
	struct FInteractionOption                          InteractionOption;                                        // (ConstParm, Parm, OutParm, ReferenceParm)
	float                                              DeltaTime;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONPlayerWidget.HideInteractionWidget
struct UIONPlayerWidget_HideInteractionWidget_Params
{
};

// Function IONBranch.IONPlayerWidget.GetInteractionWidget
struct UIONPlayerWidget_GetInteractionWidget_Params
{
	class UWidget*                                     ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONRenderPawn.ItCurrentSkins
struct AIONRenderPawn_ItCurrentSkins_Params
{
};

// Function IONBranch.IONRewardsScreen.PurchaseItem
struct UIONRewardsScreen_PurchaseItem_Params
{
	class UIONSteamItem*                               Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONRewardsScreen.PurchaseCrate
struct UIONRewardsScreen_PurchaseCrate_Params
{
	class UIONSteamItem*                               Crate;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONRewardsScreen.GetPriceForItem
struct UIONRewardsScreen_GetPriceForItem_Params
{
	class UIONSteamItem*                               Item;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONRewardsScreen.GetCratePrice
struct UIONRewardsScreen_GetCratePrice_Params
{
	class UIONSteamItem*                               Crate;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONRewardsScreen.GetCrateContent
struct UIONRewardsScreen_GetCrateContent_Params
{
	class UIONSteamItem*                               Crate;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<class UIONSteamItem*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONRewardsScreen.GetAllCrates
struct UIONRewardsScreen_GetAllCrates_Params
{
	TArray<class UIONSteamCrate*>                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONScoreEventWidget.OnScoreStringUpdated
struct UIONScoreEventWidget_OnScoreStringUpdated_Params
{
	struct FString                                     NewString;                                                // (Parm, ZeroConstructor)
};

// Function IONBranch.IONScoreEventWidget.OnAccumulatedScoreUpdated
struct UIONScoreEventWidget_OnAccumulatedScoreUpdated_Params
{
	int                                                NewScore;                                                 // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONScoreEventWidget.AddScoreEvent
struct UIONScoreEventWidget_AddScoreEvent_Params
{
	EScoreEvent                                        EventType;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Points;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONServerAuthWidget.SetupAuth
struct UIONServerAuthWidget_SetupAuth_Params
{
	struct FString                                     ServerName;                                               // (Parm, ZeroConstructor)
	struct FScriptDelegate                             NewAcceptedDelegate;                                      // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
	struct FScriptDelegate                             NewCancelledDelegate;                                     // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function IONBranch.IONServerSelectionUserWidget.ToggleAutomatch
struct UIONServerSelectionUserWidget_ToggleAutomatch_Params
{
	bool                                               Checked;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONServerSelectionUserWidget.OnMatchmakingPartyUpdated
struct UIONServerSelectionUserWidget_OnMatchmakingPartyUpdated_Params
{
	struct FMatchmakingParty                           Party;                                                    // (Parm)
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToSquadParty
struct UIONServerSelectionUserWidget_ChangeToSquadParty_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToSoloParty
struct UIONServerSelectionUserWidget_ChangeToSoloParty_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToSEARegion
struct UIONServerSelectionUserWidget_ChangeToSEARegion_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToSARegion
struct UIONServerSelectionUserWidget_ChangeToSARegion_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToOCERegion
struct UIONServerSelectionUserWidget_ChangeToOCERegion_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToNARegion
struct UIONServerSelectionUserWidget_ChangeToNARegion_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToEURegion
struct UIONServerSelectionUserWidget_ChangeToEURegion_Params
{
};

// Function IONBranch.IONServerSelectionUserWidget.ChangeToDuoParty
struct UIONServerSelectionUserWidget_ChangeToDuoParty_Params
{
};

// Function IONBranch.IONSight.UpdateMaterialAimState
struct AIONSight_UpdateMaterialAimState_Params
{
	class AIONFirearm*                                 Firearm;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              AimRatio;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSight.GetMagnificationStat
struct AIONSight_GetMagnificationStat_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSliderWidget.SliderValueChanged
struct UIONSliderWidget_SliderValueChanged_Params
{
	float                                              InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSpectatorInfo.SetupWidget
struct AIONSpectatorInfo_SetupWidget_Params
{
};

// Function IONBranch.IONSpectatorInfo.SetTrackedCharacter
struct AIONSpectatorInfo_SetTrackedCharacter_Params
{
	class AIONCharacter*                               Character;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSpectatorInfo.OnViewModeChanged
struct AIONSpectatorInfo_OnViewModeChanged_Params
{
	bool                                               bMinimal;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSpectatorInfo.GetTrackedCharacter
struct AIONSpectatorInfo_GetTrackedCharacter_Params
{
	class AIONCharacter*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSquadState.RemovePlayerState
struct AIONSquadState_RemovePlayerState_Params
{
	class AIONPlayerState*                             PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bKeepGameServiceID;                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONSquadState.IsSquadStanding
struct AIONSquadState_IsSquadStanding_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSquadState.IsSquadAlive
struct AIONSquadState_IsSquadAlive_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSquadState.GetSquadAliveCount
struct AIONSquadState_GetSquadAliveCount_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSquadState.AddPlayerState
struct AIONSquadState_AddPlayerState_Params
{
	class AIONPlayerState*                             PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.UpdateAudioEnvironment
struct UIONStatics_UpdateAudioEnvironment_Params
{
	class UAkComponent*                                AudioComponent;                                           // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONStatics.SetSoundClassVolume
struct UIONStatics_SetSoundClassVolume_Params
{
	class USoundClass*                                 SoundClass;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Volume;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.SetSoundClassPitch
struct UIONStatics_SetSoundClassPitch_Params
{
	class USoundClass*                                 SoundClass;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Pitch;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.SetAntiCheatRunning
struct UIONStatics_SetAntiCheatRunning_Params
{
	bool                                               bIsRunning;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.SecondsToText
struct UIONStatics_SecondsToText_Params
{
	float                                              Seconds;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONStatics.RotationToDirectionText
struct UIONStatics_RotationToDirectionText_Params
{
	struct FRotator                                    Rotation;                                                 // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONStatics.RebaseHitToLocalOrigin
struct UIONStatics_RebaseHitToLocalOrigin_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  Hit;                                                      // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FHitResult                                  ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.OpenWebURL
struct UIONStatics_OpenWebURL_Params
{
	struct FString                                     URL;                                                      // (Parm, ZeroConstructor)
};

// Function IONBranch.IONStatics.OpenShootingRange
struct UIONStatics_OpenShootingRange_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.OpenMainMenu
struct UIONStatics_OpenMainMenu_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONStatics.IsNoSteam
struct UIONStatics_IsNoSteam_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.IsAntiCheatRunning
struct UIONStatics_IsAntiCheatRunning_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.IsAntiCheatEnabled
struct UIONStatics_IsAntiCheatEnabled_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetWorldSettings
struct UIONStatics_GetWorldSettings_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class AMainWorldSettings*                          ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetWidgetChildGeometry
struct UIONStatics_GetWidgetChildGeometry_Params
{
	class UWidget*                                     ParentWidget;                                             // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UWidget*                                     ChildWidget;                                              // (Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	struct FGeometry                                   MyGeometry;                                               // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FGeometry                                   ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetRelativeMapLocationForPosition
struct UIONStatics_GetRelativeMapLocationForPosition_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     Position;                                                 // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FVector2D                                   ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetPlayerStateInfo
struct UIONStatics_GetPlayerStateInfo_Params
{
	class APlayerState*                                PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONStatics.GetPlayerPawn
struct UIONStatics_GetPlayerPawn_Params
{
	class APlayerState*                                Player;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class APawn*                                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetPlayerControllerInfo
struct UIONStatics_GetPlayerControllerInfo_Params
{
	class APlayerController*                           PlayerController;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONStatics.GetPlasmaStage
struct UIONStatics_GetPlasmaStage_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     OutPlasmaLocation;                                        // (Parm, OutParm, IsPlainOldData)
	float                                              OutPlasmaRadius;                                          // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     OutSafeZoneLocation;                                      // (Parm, OutParm, IsPlainOldData)
	float                                              OutSafeZoneRadius;                                        // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	EPlasmaState                                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetPlasmaLocation
struct UIONStatics_GetPlasmaLocation_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     OutPlasmaLocation;                                        // (Parm, OutParm, IsPlainOldData)
	float                                              OutPlasmaRadius;                                          // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetOrdinalWithPlace
struct UIONStatics_GetOrdinalWithPlace_Params
{
	unsigned char                                      Place;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONStatics.GetOrdinalForPlace
struct UIONStatics_GetOrdinalForPlace_Params
{
	unsigned char                                      Place;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONStatics.GetOnlinePlatformType
struct UIONStatics_GetOnlinePlatformType_Params
{
	EOnlinePlatformType                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetInventoryTypeFromClass
struct UIONStatics_GetInventoryTypeFromClass_Params
{
	class UClass*                                      ItemClass;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	EInventoryType                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetGridLocationText
struct UIONStatics_GetGridLocationText_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FIntPoint                                   Coordinates;                                              // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.IONStatics.GetGridLocation
struct UIONStatics_GetGridLocation_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     Location;                                                 // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	struct FIntPoint                                   ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetGameNewsURL
struct UIONStatics_GetGameNewsURL_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONStatics.GetDataSingleton
struct UIONStatics_GetDataSingleton_Params
{
	class UIONDataSingleton*                           ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetCurrentDPIScale
struct UIONStatics_GetCurrentDPIScale_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONStatics.GetBattleRoyaleStage
struct UIONStatics_GetBattleRoyaleStage_Params
{
	class UObject*                                     WorldContextObject;                                       // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       OutTimeText;                                              // (Parm, OutParm)
	struct FText                                       OutStageText;                                             // (Parm, OutParm)
};

// Function IONBranch.IONStatics.DisplaySubtitle
struct UIONStatics_DisplaySubtitle_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       SubtitleText;                                             // (ConstParm, Parm)
};

// Function IONBranch.IONStatics.CreateWidgetSlow
struct UIONStatics_CreateWidgetSlow_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UClass*                                      WidgetType;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class APlayerController*                           OwningPlayer;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	class UUserWidget*                                 ReturnValue;                                              // (ExportObject, Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function IONBranch.IONSteamInventory.TriggerItemDrop
struct UIONSteamInventory_TriggerItemDrop_Params
{
};

// DelegateFunction IONBranch.IONSteamInventory.SteamInventoryUpdateDelegate__DelegateSignature
struct UIONSteamInventory_SteamInventoryUpdateDelegate__DelegateSignature_Params
{
};

// Function IONBranch.IONSteamInventory.PlayerOwnsItem
struct UIONSteamInventory_PlayerOwnsItem_Params
{
	int                                                ItemToCheck;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// DelegateFunction IONBranch.IONSteamInventory.OnSteamSellItem__DelegateSignature
struct UIONSteamInventory_OnSteamSellItem__DelegateSignature_Params
{
	struct FString                                     ItemDefString;                                            // (Parm, ZeroConstructor)
};

// DelegateFunction IONBranch.IONSteamInventory.OnSteamBuyItem__DelegateSignature
struct UIONSteamInventory_OnSteamBuyItem__DelegateSignature_Params
{
	struct FString                                     ItemDefString;                                            // (Parm, ZeroConstructor)
};

// Function IONBranch.IONSteamInventory.IsAvailable
struct UIONSteamInventory_IsAvailable_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONSteamInventory.GetItemsBySteamItemDef
struct UIONSteamInventory_GetItemsBySteamItemDef_Params
{
	TArray<int>                                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONSteamInventory.GenerateDevItems
struct UIONSteamInventory_GenerateDevItems_Params
{
};

// Function IONBranch.IONUserFunctionLibrary.GetSteamOwnName
struct UIONUserFunctionLibrary_GetSteamOwnName_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONUserFunctionLibrary.GetSteamID
struct UIONUserFunctionLibrary_GetSteamID_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONUserFunctionLibrary.GetSteamFriendPersonaName
struct UIONUserFunctionLibrary_GetSteamFriendPersonaName_Params
{
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromSteamId
struct UIONUserFunctionLibrary_GetPlayerStateFromSteamId_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	class AIONPlayerState*                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONUserFunctionLibrary.GetPlayerStateFromGameSparksID
struct UIONUserFunctionLibrary_GetPlayerStateFromGameSparksID_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     GameSparksID;                                             // (Parm, ZeroConstructor)
	class AIONPlayerState*                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONUserFunctionLibrary.GetPlayerId
struct UIONUserFunctionLibrary_GetPlayerId_Params
{
	class APlayerState*                                PlayerState;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatarFromSteamID
struct UIONUserFunctionLibrary_GetPlayerAvatarFromSteamID_Params
{
	class UObject*                                     Outer;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     SteamID;                                                  // (Parm, ZeroConstructor)
	ESteamAvatarSize                                   Size;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONUserFunctionLibrary.GetPlayerAvatar
struct UIONUserFunctionLibrary_GetPlayerAvatar_Params
{
	class UObject*                                     Outer;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class APlayerState*                                Player;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	ESteamAvatarSize                                   Size;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONUserFunctionLibrary.GetOwnAvatar
struct UIONUserFunctionLibrary_GetOwnAvatar_Params
{
	class UObject*                                     Outer;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class APlayerController*                           PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
	ESteamAvatarSize                                   Size;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UTexture2D*                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.IONUserFunctionLibrary.GetAuthToken
struct UIONUserFunctionLibrary_GetAuthToken_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.IONWearableMannequin.OnWearableUnequipped
struct AIONWearableMannequin_OnWearableUnequipped_Params
{
	class AIONWearable*                                Wearable;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWearableMannequin.OnWearableEquipped
struct AIONWearableMannequin_OnWearableEquipped_Params
{
	class AIONWearable*                                Wearable;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWearableMannequin.OnWeaponChanged
struct AIONWearableMannequin_OnWeaponChanged_Params
{
	class AIONWeapon*                                  Weapon;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.IONWearableMannequinAdvanced.UpdateMannequin
struct AIONWearableMannequinAdvanced_UpdateMannequin_Params
{
};

// Function IONBranch.LobbyFunctionLibrary.JoinFriendLobby
struct ULobbyFunctionLibrary_JoinFriendLobby_Params
{
	class APlayerController*                           PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
};

// Function IONBranch.LobbyFunctionLibrary.InviteFriendToLobby
struct ULobbyFunctionLibrary_InviteFriendToLobby_Params
{
	class APlayerController*                           PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, Parm, OutParm, ReferenceParm)
};

// Function IONBranch.LobbyFunctionLibrary.CreateLobby
struct ULobbyFunctionLibrary_CreateLobby_Params
{
	class APlayerController*                           PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.LobbyFunctionLibrary.CancelMatchmaking
struct ULobbyFunctionLibrary_CancelMatchmaking_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainLocalPlayer.SetPartySettings
struct UMainLocalPlayer_SetPartySettings_Params
{
	struct FMainMenuPartySettings                      PartySettings;                                            // (Parm)
};

// Function IONBranch.MainLocalPlayer.K2_GetNickname
struct UMainLocalPlayer_K2_GetNickname_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.MainLocalPlayer.IsPlayerBanned
struct UMainLocalPlayer_IsPlayerBanned_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainLocalPlayer.IsLoggedIntoGameSparks
struct UMainLocalPlayer_IsLoggedIntoGameSparks_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MainLocalPlayer.GetPartySettings
struct UMainLocalPlayer_GetPartySettings_Params
{
	struct FMainMenuPartySettings                      ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function IONBranch.MatchHistoryFunctionLibrary.GetMatchDeathCauserInfo
struct UMatchHistoryFunctionLibrary_GetMatchDeathCauserInfo_Params
{
	struct FString                                     DeathCauserContext;                                       // (Parm, ZeroConstructor)
	struct FText                                       OutCauserText;                                            // (Parm, OutParm)
	class UTexture2D*                                  OutCauserIcon;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.MenuPlayerController.ShowDesktopNotification
struct AMenuPlayerController_ShowDesktopNotification_Params
{
};

// Function IONBranch.MenuPlayerController.JoinSession
struct AMenuPlayerController_JoinSession_Params
{
	struct FMatchmakingServerFoundResult               Result;                                                   // (Parm)
};

// Function IONBranch.MenuPlayerController.IsQA
struct AMenuPlayerController_IsQA_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MenuPlayerController.IsDeveloper
struct AMenuPlayerController_IsDeveloper_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MenuPlayerController.GetMatchmaking
struct AMenuPlayerController_GetMatchmaking_Params
{
	class UIONMatchmaking*                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.MenuPlayerController.GenerateFriendsList
struct AMenuPlayerController_GenerateFriendsList_Params
{
	TArray<struct FBlueprintOnlineFriend>              FriendsList;                                              // (Parm, ZeroConstructor)
};

// Function IONBranch.MenuPlayerController.FindCustomServers
struct AMenuPlayerController_FindCustomServers_Params
{
};

// Function IONBranch.MenuPlayerController.BPEvent_ShowOKDialog
struct AMenuPlayerController_BPEvent_ShowOKDialog_Params
{
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
};

// Function IONBranch.MenuPlayerController.BPEvent_PopulateCustomServers
struct AMenuPlayerController_BPEvent_PopulateCustomServers_Params
{
	TArray<struct FIONServerInfo>                      CustomServers;                                            // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function IONBranch.MenuPlayerController.BPEvent_PlayersReadyStatusesChanged
struct AMenuPlayerController_BPEvent_PlayersReadyStatusesChanged_Params
{
};

// Function IONBranch.MenuPlayerController.BPEvent_PartySettingsUpdated
struct AMenuPlayerController_BPEvent_PartySettingsUpdated_Params
{
	struct FMainMenuPartySettings                      PartySettings;                                            // (Parm)
};

// Function IONBranch.MenuPlayerController.BPEvent_PartyMemberLeave
struct AMenuPlayerController_BPEvent_PartyMemberLeave_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (Parm)
};

// Function IONBranch.MenuPlayerController.BPEvent_PartyMemberJoined
struct AMenuPlayerController_BPEvent_PartyMemberJoined_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (Parm)
};

// Function IONBranch.MenuPlayerController.BPEvent_PartyInviteReceived
struct AMenuPlayerController_BPEvent_PartyInviteReceived_Params
{
	struct FString                                     PlayerName;                                               // (Parm, ZeroConstructor)
};

// Function IONBranch.SeatComponent.EnterVehicle
struct USeatComponent_EnterVehicle_Params
{
	class AMainPlayerController*                       PC;                                                       // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.ReadFriendsListCallbackProxy.ReadFriendsList
struct UReadFriendsListCallbackProxy_ReadFriendsList_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class APlayerController*                           PlayerController;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	class UReadFriendsListCallbackProxy*               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetFloatPropertyByName
struct USettingsFunctionLibrary_SetFloatPropertyByName_Params
{
	class UObject*                                     Object;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       PropertyName;                                             // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetCVarString
struct USettingsFunctionLibrary_SetCVarString_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
};

// Function IONBranch.SettingsFunctionLibrary.SetCVarInt
struct USettingsFunctionLibrary_SetCVarInt_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	int                                                Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetCVarFloat
struct USettingsFunctionLibrary_SetCVarFloat_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetCVarBool
struct USettingsFunctionLibrary_SetCVarBool_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	bool                                               bValue;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetConfigString
struct USettingsFunctionLibrary_SetConfigString_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
};

// Function IONBranch.SettingsFunctionLibrary.SetConfigInt
struct USettingsFunctionLibrary_SetConfigInt_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	int                                                Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetConfigFloat
struct USettingsFunctionLibrary_SetConfigFloat_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SetConfigBool
struct USettingsFunctionLibrary_SetConfigBool_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	bool                                               Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.SaveObjectConfig
struct USettingsFunctionLibrary_SaveObjectConfig_Params
{
	class UObject*                                     Object;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetFloatPropertyByName
struct USettingsFunctionLibrary_GetFloatPropertyByName_Params
{
	class UObject*                                     Object;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       PropertyName;                                             // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetCVarString
struct USettingsFunctionLibrary_GetCVarString_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.SettingsFunctionLibrary.GetCVarInt
struct USettingsFunctionLibrary_GetCVarInt_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetCVarFloat
struct USettingsFunctionLibrary_GetCVarFloat_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetCVarBool
struct USettingsFunctionLibrary_GetCVarBool_Params
{
	struct FString                                     CVarName;                                                 // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetConfigString
struct USettingsFunctionLibrary_GetConfigString_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function IONBranch.SettingsFunctionLibrary.GetConfigInt
struct USettingsFunctionLibrary_GetConfigInt_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetConfigFloat
struct USettingsFunctionLibrary_GetConfigFloat_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SettingsFunctionLibrary.GetConfigBool
struct USettingsFunctionLibrary_GetConfigBool_Params
{
	struct FString                                     Section;                                                  // (Parm, ZeroConstructor)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function IONBranch.SpawnCapsule.InitClientCapsule
struct ASpawnCapsule_InitClientCapsule_Params
{
};

// Function IONBranch.VehicleBase.ServerExitVehicle
struct AVehicleBase_ServerExitVehicle_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
