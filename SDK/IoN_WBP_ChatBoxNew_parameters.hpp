#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.MakeMessageID
struct UWBP_ChatBoxNew_C_MakeMessageID_Params
{
	struct FGuid                                       MessageId;                                                // (Parm, OutParm, IsPlainOldData)
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.Construct
struct UWBP_ChatBoxNew_C_Construct_Params
{
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature
struct UWBP_ChatBoxNew_C_BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearEnteredMessage
struct UWBP_ChatBoxNew_C_ClearEnteredMessage_Params
{
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.SetChannelMessages
struct UWBP_ChatBoxNew_C_SetChannelMessages_Params
{
	TEnumAsByte<EChatChannel>                          Channel;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	TArray<struct FSChannelMessage>                    Messages;                                                 // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearChannelMessages
struct UWBP_ChatBoxNew_C_ClearChannelMessages_Params
{
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ExecuteUbergraph_WBP_ChatBoxNew
struct UWBP_ChatBoxNew_C_ExecuteUbergraph_WBP_ChatBoxNew_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.OnChannelChanged__DelegateSignature
struct UWBP_ChatBoxNew_C_OnChannelChanged__DelegateSignature_Params
{
	TEnumAsByte<EChatChannel>                          Channel;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
