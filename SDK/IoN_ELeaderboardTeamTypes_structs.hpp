#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum ELeaderboardTeamTypes.ELeaderboardTeamTypes
enum class ELeaderboardTeamTypes : uint8_t
{
	ELeaderboardTeamTypes__NewEnumerator0 = 0,
	ELeaderboardTeamTypes__NewEnumerator1 = 1,
	ELeaderboardTeamTypes__NewEnumerator2 = 2,
	ELeaderboardTeamTypes__NewEnumerator6 = 3,
	ELeaderboardTeamTypes__ELeaderboardTeamTypes_MAX = 4
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
