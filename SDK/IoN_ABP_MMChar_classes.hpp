#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_MMChar.ABP_MMChar_C
// 0x04D8 (0x08A8 - 0x03D0)
class UABP_MMChar_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_2998B37C45F7700F82A1C4869704216D;      // 0x03D8(0x0048)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA;// 0x0420(0x0070)
	struct FAnimNode_TwoBoneIK                         AnimGraphNode_TwoBoneIK_C673E723485A28EC8C2A0B839B033932; // 0x0490(0x01C0)
	struct FAnimNode_TwoBoneIK                         AnimGraphNode_TwoBoneIK_655E5C4749DB1D88A187F0BFC97C3DFE; // 0x0650(0x01C0)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_FCE263754DFD6EDAA14787B82E5AE562;// 0x0810(0x0048)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_BE627E5542B50208BE951F9C86E9013C;// 0x0858(0x0048)
	class UAnimSequence*                               CurrentAnim;                                              // 0x08A0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_MMChar.ABP_MMChar_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA();
	void ExecuteUbergraph_ABP_MMChar(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
