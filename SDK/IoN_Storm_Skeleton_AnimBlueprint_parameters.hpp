#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function Storm_Skeleton_AnimBlueprint.Storm_Skeleton_AnimBlueprint_C.ExecuteUbergraph_Storm_Skeleton_AnimBlueprint
struct UStorm_Skeleton_AnimBlueprint_C_ExecuteUbergraph_Storm_Skeleton_AnimBlueprint_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
