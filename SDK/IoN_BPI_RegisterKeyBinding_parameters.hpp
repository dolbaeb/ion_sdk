#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BPI_RegisterKeyBinding.BPI_RegisterKeyBinding_C.Key Pressed
struct UBPI_RegisterKeyBinding_C_Key_Pressed_Params
{
	struct FSKeyInput                                  New_Keybinding;                                           // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               _;                                                        // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
