#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass Storm_Skeleton_AnimBlueprint.Storm_Skeleton_AnimBlueprint_C
// 0x00F0 (0x04C0 - 0x03D0)
class UStorm_Skeleton_AnimBlueprint_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_CA80136D4836F3B8C76C36A70938E70A;      // 0x03D8(0x0048)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_F718A5AE47BB27B782D8B2A29C1976BB;// 0x0420(0x0038)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_3C4ECA7846B8BFE1B7CA09A992BA15FA;      // 0x0458(0x0068)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass Storm_Skeleton_AnimBlueprint.Storm_Skeleton_AnimBlueprint_C");
		return ptr;
	}


	void ExecuteUbergraph_Storm_Skeleton_AnimBlueprint(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
