#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerTag.WBP_PlayerTag_C.Show
struct UWBP_PlayerTag_C_Show_Params
{
};

// Function WBP_PlayerTag.WBP_PlayerTag_C.Construct
struct UWBP_PlayerTag_C_Construct_Params
{
};

// Function WBP_PlayerTag.WBP_PlayerTag_C.SetPawn
struct UWBP_PlayerTag_C_SetPawn_Params
{
	class APawn*                                       Pawn;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerTag.WBP_PlayerTag_C.Retry PlayerState
struct UWBP_PlayerTag_C_Retry_PlayerState_Params
{
};

// Function WBP_PlayerTag.WBP_PlayerTag_C.ShowMainMenu
struct UWBP_PlayerTag_C_ShowMainMenu_Params
{
	struct FText                                       PlayerName;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_PlayerTag.WBP_PlayerTag_C.ExecuteUbergraph_WBP_PlayerTag
struct UWBP_PlayerTag_C_ExecuteUbergraph_WBP_PlayerTag_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
