#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_FriendPopup.WBP_FriendPopup_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature
struct UWBP_FriendPopup_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_160_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_FriendPopup.WBP_FriendPopup_C.ExecuteUbergraph_WBP_FriendPopup
struct UWBP_FriendPopup_C_ExecuteUbergraph_WBP_FriendPopup_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
