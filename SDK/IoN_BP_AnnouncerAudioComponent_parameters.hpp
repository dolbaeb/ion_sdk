#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleStarted
struct UBP_AnnouncerAudioComponent_C_OnBattleRoyaleStarted_Params
{
};

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleCountdownStarted
struct UBP_AnnouncerAudioComponent_C_OnBattleRoyaleCountdownStarted_Params
{
};

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnMatchEnded
struct UBP_AnnouncerAudioComponent_C_OnMatchEnded_Params
{
};

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnFinalConvergenceStarted
struct UBP_AnnouncerAudioComponent_C_OnFinalConvergenceStarted_Params
{
};

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.ExecuteUbergraph_BP_AnnouncerAudioComponent
struct UBP_AnnouncerAudioComponent_C_ExecuteUbergraph_BP_AnnouncerAudioComponent_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
