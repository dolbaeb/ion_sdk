#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.Construct
struct UWB_ScalabilitySetting_C_Construct_Params
{
};

// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.PreConstruct
struct UWB_ScalabilitySetting_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WB_ScalabilitySetting.WB_ScalabilitySetting_C.ExecuteUbergraph_WB_ScalabilitySetting
struct UWB_ScalabilitySetting_C_ExecuteUbergraph_WB_ScalabilitySetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
