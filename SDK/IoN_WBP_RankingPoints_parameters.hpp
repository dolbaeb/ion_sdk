#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_NextRank_Text_1
struct UWBP_RankingPoints_C_Get_TextBlock_NextRank_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_PreviousRank_Visibility_1
struct UWBP_RankingPoints_C_Get_TextBlock_PreviousRank_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.GetText_PreviousRank
struct UWBP_RankingPoints_C_GetText_PreviousRank_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.GetVisibility_NextRank
struct UWBP_RankingPoints_C_GetVisibility_NextRank_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.GetPercent_Progress
struct UWBP_RankingPoints_C_GetPercent_Progress_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_TextBlock_RankingPoints
struct UWBP_RankingPoints_C_Get_TextBlock_RankingPoints_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_ColorAndOpacity_1
struct UWBP_RankingPoints_C_Get_SkillGainText_ColorAndOpacity_1_Params
{
	struct FSlateColor                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Get_SkillGainText_Text_1
struct UWBP_RankingPoints_C_Get_SkillGainText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.SetRankState
struct UWBP_RankingPoints_C_SetRankState_Params
{
	struct FSRankDisplayState                          RankState;                                                // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Appear
struct UWBP_RankingPoints_C_Appear_Params
{
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.Construct
struct UWBP_RankingPoints_C_Construct_Params
{
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.StartAnimatingPoints
struct UWBP_RankingPoints_C_StartAnimatingPoints_Params
{
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.AnimatePoints
struct UWBP_RankingPoints_C_AnimatePoints_Params
{
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.ExecuteUbergraph_WBP_RankingPoints
struct UWBP_RankingPoints_C_ExecuteUbergraph_WBP_RankingPoints_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RankingPoints.WBP_RankingPoints_C.ProgressFinished__DelegateSignature
struct UWBP_RankingPoints_C_ProgressFinished__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
