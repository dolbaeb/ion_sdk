// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_WearableMannequinAdvanced_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.UserConstructionScript");

	ABP_WearableMannequinAdvanced_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__FinishedFunc
// (BlueprintEvent)

void ABP_WearableMannequinAdvanced_C::CharacterSpin__FinishedFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__FinishedFunc");

	ABP_WearableMannequinAdvanced_C_CharacterSpin__FinishedFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__UpdateFunc
// (BlueprintEvent)

void ABP_WearableMannequinAdvanced_C::CharacterSpin__UpdateFunc()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__UpdateFunc");

	ABP_WearableMannequinAdvanced_C_CharacterSpin__UpdateFunc_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.ExecuteUbergraph_BP_WearableMannequinAdvanced
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_WearableMannequinAdvanced_C::ExecuteUbergraph_BP_WearableMannequinAdvanced(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.ExecuteUbergraph_BP_WearableMannequinAdvanced");

	ABP_WearableMannequinAdvanced_C_ExecuteUbergraph_BP_WearableMannequinAdvanced_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
