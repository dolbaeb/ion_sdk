#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Generate Combobox Widget Item
struct UWBP_SettingsPage_Dev_C_Generate_Combobox_Widget_Item_Params
{
	struct FString                                     Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Refresh Dev Settings
struct UWBP_SettingsPage_Dev_C_Refresh_Dev_Settings_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Refresh Slider Time Dilation
struct UWBP_SettingsPage_Dev_C_Refresh_Slider_Time_Dilation_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Refresh Pause Game
struct UWBP_SettingsPage_Dev_C_Refresh_Pause_Game_Params
{
	bool                                               InIsChecked;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__My Slider_K2Node_ComponentBoundEvent_1112_OnFloatValueChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__My_Slider_K2Node_ComponentBoundEvent_1112_OnFloatValueChangedEvent__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__My CheckBox_K2Node_ComponentBoundEvent_1130_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__My_CheckBox_K2Node_ComponentBoundEvent_1130_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__My Edit Value_K2Node_ComponentBoundEvent_165_OnEditableTextCommittedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__My_Edit_Value_K2Node_ComponentBoundEvent_165_OnEditableTextCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_248_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_248_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonMemory_K2Node_ComponentBoundEvent_395_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonMemory_K2Node_ComponentBoundEvent_395_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonNetwork_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonNetwork_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonParticles_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonParticles_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonStatTick_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonStatTick_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonSceneRendering_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonSceneRendering_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonInitViews_K2Node_ComponentBoundEvent_591_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonInitViews_K2Node_ComponentBoundEvent_591_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonEngine_K2Node_ComponentBoundEvent_4_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonEngine_K2Node_ComponentBoundEvent_4_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__MyComboBox_K2Node_ComponentBoundEvent_128_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__MyComboBox_K2Node_ComponentBoundEvent_128_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonStatSlow_K2Node_ComponentBoundEvent_712_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonStatSlow_K2Node_ComponentBoundEvent_712_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.BndEvt__ButtonClearStats_K2Node_ComponentBoundEvent_739_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Dev_C_BndEvt__ButtonClearStats_K2Node_ComponentBoundEvent_739_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.ExecuteUbergraph_WBP_SettingsPage_Dev
struct UWBP_SettingsPage_Dev_C_ExecuteUbergraph_WBP_SettingsPage_Dev_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Pause Game When Menu Open__DelegateSignature
struct UWBP_SettingsPage_Dev_C_Pause_Game_When_Menu_Open__DelegateSignature_Params
{
	bool                                               Pause_Game_On_Menu_Open;                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Dev.WBP_SettingsPage_Dev_C.Settings File Deleted__DelegateSignature
struct UWBP_SettingsPage_Dev_C_Settings_File_Deleted__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
