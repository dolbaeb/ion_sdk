// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function SIOJson.SIOJLibrary.StructToJsonObject
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UProperty*               AnyStruct                      (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJLibrary::STATIC_StructToJsonObject(class UProperty* AnyStruct)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.StructToJsonObject");

	USIOJLibrary_StructToJsonObject_Params params;
	params.AnyStruct = AnyStruct;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.StringToJsonValueArray
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 JSONString                     (Parm, ZeroConstructor)
// TArray<class USIOJsonValue*>   OutJsonValueArray              (Parm, OutParm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJLibrary::STATIC_StringToJsonValueArray(const struct FString& JSONString, TArray<class USIOJsonValue*>* OutJsonValueArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.StringToJsonValueArray");

	USIOJLibrary_StringToJsonValueArray_Params params;
	params.JSONString = JSONString;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (OutJsonValueArray != nullptr)
		*OutJsonValueArray = params.OutJsonValueArray;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.PercentEncode
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Source                         (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJLibrary::STATIC_PercentEncode(const struct FString& Source)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.PercentEncode");

	USIOJLibrary_PercentEncode_Params params;
	params.Source = Source;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.JsonObjectToStruct
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          JsonObject                     (Parm, ZeroConstructor, IsPlainOldData)
// class UProperty*               AnyStruct                      (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJLibrary::STATIC_JsonObjectToStruct(class USIOJsonObject* JsonObject, class UProperty* AnyStruct)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.JsonObjectToStruct");

	USIOJLibrary_JsonObjectToStruct_Params params;
	params.JsonObject = JsonObject;
	params.AnyStruct = AnyStruct;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_StringToJsonValue
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// struct FString                 inString                       (Parm, ZeroConstructor)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_StringToJsonValue(const struct FString& inString)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_StringToJsonValue");

	USIOJLibrary_Conv_StringToJsonValue_Params params;
	params.inString = inString;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonValueToJsonObject
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonValue*           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJLibrary::STATIC_Conv_JsonValueToJsonObject(class USIOJsonValue* InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonValueToJsonObject");

	USIOJLibrary_Conv_JsonValueToJsonObject_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonValueToInt
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonValue*           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int USIOJLibrary::STATIC_Conv_JsonValueToInt(class USIOJsonValue* InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonValueToInt");

	USIOJLibrary_Conv_JsonValueToInt_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonValueToFloat
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonValue*           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USIOJLibrary::STATIC_Conv_JsonValueToFloat(class USIOJsonValue* InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonValueToFloat");

	USIOJLibrary_Conv_JsonValueToFloat_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonValueToBytes
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonValue*           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// TArray<unsigned char>          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<unsigned char> USIOJLibrary::STATIC_Conv_JsonValueToBytes(class USIOJsonValue* InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonValueToBytes");

	USIOJLibrary_Conv_JsonValueToBytes_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonValueToBool
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonValue*           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJLibrary::STATIC_Conv_JsonValueToBool(class USIOJsonValue* InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonValueToBool");

	USIOJLibrary_Conv_JsonValueToBool_Params params;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonObjectToString
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonObject*          InObject                       (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJLibrary::STATIC_Conv_JsonObjectToString(class USIOJsonObject* InObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonObjectToString");

	USIOJLibrary_Conv_JsonObjectToString_Params params;
	params.InObject = InObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_JsonObjectToJsonValue
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonObject*          InObject                       (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_JsonObjectToJsonValue(class USIOJsonObject* InObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_JsonObjectToJsonValue");

	USIOJLibrary_Conv_JsonObjectToJsonValue_Params params;
	params.InObject = InObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_IntToJsonValue
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// int                            inInt                          (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_IntToJsonValue(int inInt)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_IntToJsonValue");

	USIOJLibrary_Conv_IntToJsonValue_Params params;
	params.inInt = inInt;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_FloatToJsonValue
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// float                          InFloat                        (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_FloatToJsonValue(float InFloat)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_FloatToJsonValue");

	USIOJLibrary_Conv_FloatToJsonValue_Params params;
	params.InFloat = InFloat;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_BytesToJsonValue
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// TArray<unsigned char>          InBytes                        (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_BytesToJsonValue(TArray<unsigned char> InBytes)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_BytesToJsonValue");

	USIOJLibrary_Conv_BytesToJsonValue_Params params;
	params.InBytes = InBytes;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_BoolToJsonValue
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// bool                           InBool                         (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_BoolToJsonValue(bool InBool)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_BoolToJsonValue");

	USIOJLibrary_Conv_BoolToJsonValue_Params params;
	params.InBool = InBool;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Conv_ArrayToJsonValue
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// TArray<class USIOJsonValue*>   inArray                        (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJLibrary::STATIC_Conv_ArrayToJsonValue(TArray<class USIOJsonValue*> inArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Conv_ArrayToJsonValue");

	USIOJLibrary_Conv_ArrayToJsonValue_Params params;
	params.inArray = inArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.CallURL
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 URL                            (Parm, ZeroConstructor)
// ESIORequestVerb                Verb                           (Parm, ZeroConstructor, IsPlainOldData)
// ESIORequestContentType         ContentType                    (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonObject*          SIOJJson                       (Parm, ZeroConstructor, IsPlainOldData)
// struct FScriptDelegate         Callback                       (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJLibrary::STATIC_CallURL(class UObject* WorldContextObject, const struct FString& URL, ESIORequestVerb Verb, ESIORequestContentType ContentType, class USIOJsonObject* SIOJJson, const struct FScriptDelegate& Callback)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.CallURL");

	USIOJLibrary_CallURL_Params params;
	params.WorldContextObject = WorldContextObject;
	params.URL = URL;
	params.Verb = Verb;
	params.ContentType = ContentType;
	params.SIOJJson = SIOJJson;
	params.Callback = Callback;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJLibrary.Base64Encode
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// struct FString                 Source                         (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJLibrary::STATIC_Base64Encode(const struct FString& Source)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Base64Encode");

	USIOJLibrary_Base64Encode_Params params;
	params.Source = Source;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJLibrary.Base64Decode
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 Source                         (Parm, ZeroConstructor)
// struct FString                 Dest                           (Parm, OutParm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJLibrary::STATIC_Base64Decode(const struct FString& Source, struct FString* Dest)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJLibrary.Base64Decode");

	USIOJLibrary_Base64Decode_Params params;
	params.Source = Source;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Dest != nullptr)
		*Dest = params.Dest;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.SetVerb
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// ESIORequestVerb                Verb                           (Parm, ZeroConstructor, IsPlainOldData)

void USIOJRequestJSON::SetVerb(ESIORequestVerb Verb)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetVerb");

	USIOJRequestJSON_SetVerb_Params params;
	params.Verb = Verb;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetResponseObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          JsonObject                     (Parm, ZeroConstructor, IsPlainOldData)

void USIOJRequestJSON::SetResponseObject(class USIOJsonObject* JsonObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetResponseObject");

	USIOJRequestJSON_SetResponseObject_Params params;
	params.JsonObject = JsonObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetRequestObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          JsonObject                     (Parm, ZeroConstructor, IsPlainOldData)

void USIOJRequestJSON::SetRequestObject(class USIOJsonObject* JsonObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetRequestObject");

	USIOJRequestJSON_SetRequestObject_Params params;
	params.JsonObject = JsonObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetHeader
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 HeaderName                     (Parm, ZeroConstructor)
// struct FString                 HeaderValue                    (Parm, ZeroConstructor)

void USIOJRequestJSON::SetHeader(const struct FString& HeaderName, const struct FString& HeaderValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetHeader");

	USIOJRequestJSON_SetHeader_Params params;
	params.HeaderName = HeaderName;
	params.HeaderValue = HeaderValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetCustomVerb
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 Verb                           (Parm, ZeroConstructor)

void USIOJRequestJSON::SetCustomVerb(const struct FString& Verb)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetCustomVerb");

	USIOJRequestJSON_SetCustomVerb_Params params;
	params.Verb = Verb;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetContentType
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// ESIORequestContentType         ContentType                    (Parm, ZeroConstructor, IsPlainOldData)

void USIOJRequestJSON::SetContentType(ESIORequestContentType ContentType)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetContentType");

	USIOJRequestJSON_SetContentType_Params params;
	params.ContentType = ContentType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetBinaryRequestContent
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// TArray<unsigned char>          Content                        (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJRequestJSON::SetBinaryRequestContent(TArray<unsigned char> Content)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetBinaryRequestContent");

	USIOJRequestJSON_SetBinaryRequestContent_Params params;
	params.Content = Content;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.SetBinaryContentType
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ContentType                    (Parm, ZeroConstructor)

void USIOJRequestJSON::SetBinaryContentType(const struct FString& ContentType)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.SetBinaryContentType");

	USIOJRequestJSON_SetBinaryContentType_Params params;
	params.ContentType = ContentType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.ResetResponseData
// (Final, Native, Public, BlueprintCallable)

void USIOJRequestJSON::ResetResponseData()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ResetResponseData");

	USIOJRequestJSON_ResetResponseData_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.ResetRequestData
// (Final, Native, Public, BlueprintCallable)

void USIOJRequestJSON::ResetRequestData()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ResetRequestData");

	USIOJRequestJSON_ResetRequestData_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.ResetData
// (Final, Native, Public, BlueprintCallable)

void USIOJRequestJSON::ResetData()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ResetData");

	USIOJRequestJSON_ResetData_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.RemoveTag
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   Tag                            (Parm, ZeroConstructor, IsPlainOldData)
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int USIOJRequestJSON::RemoveTag(const struct FName& Tag)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.RemoveTag");

	USIOJRequestJSON_RemoveTag_Params params;
	params.Tag = Tag;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.ProcessURL
// (Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 URL                            (Parm, ZeroConstructor)

void USIOJRequestJSON::ProcessURL(const struct FString& URL)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ProcessURL");

	USIOJRequestJSON_ProcessURL_Params params;
	params.URL = URL;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.HasTag
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FName                   Tag                            (Parm, ZeroConstructor, IsPlainOldData)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJRequestJSON::HasTag(const struct FName& Tag)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.HasTag");

	USIOJRequestJSON_HasTag_Params params;
	params.Tag = Tag;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetUrl
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJRequestJSON::GetUrl()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetUrl");

	USIOJRequestJSON_GetUrl_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetStatus
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// ESIORequestStatus              ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESIORequestStatus USIOJRequestJSON::GetStatus()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetStatus");

	USIOJRequestJSON_GetStatus_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetResponseObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJRequestJSON::GetResponseObject()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetResponseObject");

	USIOJRequestJSON_GetResponseObject_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetResponseHeader
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 HeaderName                     (ConstParm, Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJRequestJSON::GetResponseHeader(const struct FString& HeaderName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetResponseHeader");

	USIOJRequestJSON_GetResponseHeader_Params params;
	params.HeaderName = HeaderName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetResponseCode
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int USIOJRequestJSON::GetResponseCode()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetResponseCode");

	USIOJRequestJSON_GetResponseCode_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetRequestObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJRequestJSON::GetRequestObject()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetRequestObject");

	USIOJRequestJSON_GetRequestObject_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.GetAllResponseHeaders
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<struct FString>         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FString> USIOJRequestJSON::GetAllResponseHeaders()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.GetAllResponseHeaders");

	USIOJRequestJSON_GetAllResponseHeaders_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.ConstructRequestExt
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// ESIORequestVerb                Verb                           (Parm, ZeroConstructor, IsPlainOldData)
// ESIORequestContentType         ContentType                    (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJRequestJSON*        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJRequestJSON* USIOJRequestJSON::STATIC_ConstructRequestExt(class UObject* WorldContextObject, ESIORequestVerb Verb, ESIORequestContentType ContentType)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ConstructRequestExt");

	USIOJRequestJSON_ConstructRequestExt_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Verb = Verb;
	params.ContentType = ContentType;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.ConstructRequest
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJRequestJSON*        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJRequestJSON* USIOJRequestJSON::STATIC_ConstructRequest(class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ConstructRequest");

	USIOJRequestJSON_ConstructRequest_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJRequestJSON.Cancel
// (Final, Native, Public, BlueprintCallable)

void USIOJRequestJSON::Cancel()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.Cancel");

	USIOJRequestJSON_Cancel_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJRequestJSON.ApplyURL
// (Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 URL                            (Parm, ZeroConstructor)
// class USIOJsonObject*          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FLatentActionInfo       LatentInfo                     (Parm)

void USIOJRequestJSON::ApplyURL(const struct FString& URL, class UObject* WorldContextObject, const struct FLatentActionInfo& LatentInfo, class USIOJsonObject** Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.ApplyURL");

	USIOJRequestJSON_ApplyURL_Params params;
	params.URL = URL;
	params.WorldContextObject = WorldContextObject;
	params.LatentInfo = LatentInfo;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function SIOJson.SIOJRequestJSON.AddTag
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FName                   Tag                            (Parm, ZeroConstructor, IsPlainOldData)

void USIOJRequestJSON::AddTag(const struct FName& Tag)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJRequestJSON.AddTag");

	USIOJRequestJSON_AddTag_Params params;
	params.Tag = Tag;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetStringField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// struct FString                 StringValue                    (Parm, ZeroConstructor)

void USIOJsonObject::SetStringField(const struct FString& FieldName, const struct FString& StringValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetStringField");

	USIOJsonObject_SetStringField_Params params;
	params.FieldName = FieldName;
	params.StringValue = StringValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetStringArrayField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<struct FString>         StringArray                    (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetStringArrayField(const struct FString& FieldName, TArray<struct FString> StringArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetStringArrayField");

	USIOJsonObject_SetStringArrayField_Params params;
	params.FieldName = FieldName;
	params.StringArray = StringArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetObjectField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// class USIOJsonObject*          JsonObject                     (Parm, ZeroConstructor, IsPlainOldData)

void USIOJsonObject::SetObjectField(const struct FString& FieldName, class USIOJsonObject* JsonObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetObjectField");

	USIOJsonObject_SetObjectField_Params params;
	params.FieldName = FieldName;
	params.JsonObject = JsonObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetObjectArrayField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<class USIOJsonObject*>  ObjectArray                    (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetObjectArrayField(const struct FString& FieldName, TArray<class USIOJsonObject*> ObjectArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetObjectArrayField");

	USIOJsonObject_SetObjectArrayField_Params params;
	params.FieldName = FieldName;
	params.ObjectArray = ObjectArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetNumberField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// float                          Number                         (Parm, ZeroConstructor, IsPlainOldData)

void USIOJsonObject::SetNumberField(const struct FString& FieldName, float Number)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetNumberField");

	USIOJsonObject_SetNumberField_Params params;
	params.FieldName = FieldName;
	params.Number = Number;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetNumberArrayField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<float>                  NumberArray                    (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetNumberArrayField(const struct FString& FieldName, TArray<float> NumberArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetNumberArrayField");

	USIOJsonObject_SetNumberArrayField_Params params;
	params.FieldName = FieldName;
	params.NumberArray = NumberArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// class USIOJsonValue*           JsonValue                      (Parm, ZeroConstructor, IsPlainOldData)

void USIOJsonObject::SetField(const struct FString& FieldName, class USIOJsonValue* JsonValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetField");

	USIOJsonObject_SetField_Params params;
	params.FieldName = FieldName;
	params.JsonValue = JsonValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetBoolField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// bool                           InValue                        (Parm, ZeroConstructor, IsPlainOldData)

void USIOJsonObject::SetBoolField(const struct FString& FieldName, bool InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetBoolField");

	USIOJsonObject_SetBoolField_Params params;
	params.FieldName = FieldName;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetBoolArrayField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<bool>                   BoolArray                      (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetBoolArrayField(const struct FString& FieldName, TArray<bool> BoolArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetBoolArrayField");

	USIOJsonObject_SetBoolArrayField_Params params;
	params.FieldName = FieldName;
	params.BoolArray = BoolArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetBinaryField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<unsigned char>          Bytes                          (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetBinaryField(const struct FString& FieldName, TArray<unsigned char> Bytes)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetBinaryField");

	USIOJsonObject_SetBinaryField_Params params;
	params.FieldName = FieldName;
	params.Bytes = Bytes;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.SetArrayField
// (Final, Native, Public, HasOutParms, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<class USIOJsonValue*>   inArray                        (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)

void USIOJsonObject::SetArrayField(const struct FString& FieldName, TArray<class USIOJsonValue*> inArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.SetArrayField");

	USIOJsonObject_SetArrayField_Params params;
	params.FieldName = FieldName;
	params.inArray = inArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.Reset
// (Final, Native, Public, BlueprintCallable)

void USIOJsonObject::Reset()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.Reset");

	USIOJsonObject_Reset_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.RemoveField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)

void USIOJsonObject::RemoveField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.RemoveField");

	USIOJsonObject_RemoveField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.MergeJsonObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          InJsonObject                   (Parm, ZeroConstructor, IsPlainOldData)
// bool                           Overwrite                      (Parm, ZeroConstructor, IsPlainOldData)

void USIOJsonObject::MergeJsonObject(class USIOJsonObject* InJsonObject, bool Overwrite)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.MergeJsonObject");

	USIOJsonObject_MergeJsonObject_Params params;
	params.InJsonObject = InJsonObject;
	params.Overwrite = Overwrite;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function SIOJson.SIOJsonObject.HasField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJsonObject::HasField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.HasField");

	USIOJsonObject_HasField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetStringField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonObject::GetStringField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetStringField");

	USIOJsonObject_GetStringField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetStringArrayField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<struct FString>         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FString> USIOJsonObject::GetStringArrayField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetStringArrayField");

	USIOJsonObject_GetStringArrayField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetObjectField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJsonObject::GetObjectField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetObjectField");

	USIOJsonObject_GetObjectField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetObjectArrayField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<class USIOJsonObject*>  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class USIOJsonObject*> USIOJsonObject::GetObjectArrayField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetObjectArrayField");

	USIOJsonObject_GetObjectArrayField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetNumberField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USIOJsonObject::GetNumberField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetNumberField");

	USIOJsonObject_GetNumberField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetNumberArrayField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<float>                  ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<float> USIOJsonObject::GetNumberArrayField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetNumberArrayField");

	USIOJsonObject_GetNumberArrayField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetFieldNames
// (Final, Native, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// TArray<struct FString>         ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<struct FString> USIOJsonObject::GetFieldNames()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetFieldNames");

	USIOJsonObject_GetFieldNames_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonObject::GetField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetField");

	USIOJsonObject_GetField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetBoolField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJsonObject::GetBoolField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetBoolField");

	USIOJsonObject_GetBoolField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetBoolArrayField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<bool>                   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<bool> USIOJsonObject::GetBoolArrayField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetBoolArrayField");

	USIOJsonObject_GetBoolArrayField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetBinaryField
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<unsigned char>          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<unsigned char> USIOJsonObject::GetBinaryField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetBinaryField");

	USIOJsonObject_GetBinaryField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.GetArrayField
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 FieldName                      (Parm, ZeroConstructor)
// TArray<class USIOJsonValue*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class USIOJsonValue*> USIOJsonObject::GetArrayField(const struct FString& FieldName)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.GetArrayField");

	USIOJsonObject_GetArrayField_Params params;
	params.FieldName = FieldName;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.EncodeJsonToSingleString
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonObject::EncodeJsonToSingleString()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.EncodeJsonToSingleString");

	USIOJsonObject_EncodeJsonToSingleString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.EncodeJson
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonObject::EncodeJson()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.EncodeJson");

	USIOJsonObject_EncodeJson_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.DecodeJson
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// struct FString                 JSONString                     (Parm, ZeroConstructor)
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJsonObject::DecodeJson(const struct FString& JSONString)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.DecodeJson");

	USIOJsonObject_DecodeJson_Params params;
	params.JSONString = JSONString;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonObject.ConstructJsonObject
// (Final, Native, Static, Public, BlueprintCallable)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJsonObject::STATIC_ConstructJsonObject(class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonObject.ConstructJsonObject");

	USIOJsonObject_ConstructJsonObject_Params params;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ValueFromJsonString
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 StringValue                    (Parm, ZeroConstructor)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ValueFromJsonString(class UObject* WorldContextObject, const struct FString& StringValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ValueFromJsonString");

	USIOJsonValue_ValueFromJsonString_Params params;
	params.WorldContextObject = WorldContextObject;
	params.StringValue = StringValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.IsNull
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJsonValue::IsNull()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.IsNull");

	USIOJsonValue_IsNull_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.GetTypeString
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonValue::GetTypeString()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.GetTypeString");

	USIOJsonValue_GetTypeString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.GetType
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TEnumAsByte<ESIOJson>          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

TEnumAsByte<ESIOJson> USIOJsonValue::GetType()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.GetType");

	USIOJsonValue_GetType_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.EncodeJson
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonValue::EncodeJson()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.EncodeJson");

	USIOJsonValue_EncodeJson_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueString
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 StringValue                    (Parm, ZeroConstructor)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueString(class UObject* WorldContextObject, const struct FString& StringValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueString");

	USIOJsonValue_ConstructJsonValueString_Params params;
	params.WorldContextObject = WorldContextObject;
	params.StringValue = StringValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueObject
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class USIOJsonObject*          JsonObject                     (Parm, ZeroConstructor, IsPlainOldData)
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueObject(class USIOJsonObject* JsonObject, class UObject* WorldContextObject)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueObject");

	USIOJsonValue_ConstructJsonValueObject_Params params;
	params.JsonObject = JsonObject;
	params.WorldContextObject = WorldContextObject;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueNumber
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// float                          Number                         (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueNumber(class UObject* WorldContextObject, float Number)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueNumber");

	USIOJsonValue_ConstructJsonValueNumber_Params params;
	params.WorldContextObject = WorldContextObject;
	params.Number = Number;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueBool
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// bool                           InValue                        (Parm, ZeroConstructor, IsPlainOldData)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueBool(class UObject* WorldContextObject, bool InValue)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueBool");

	USIOJsonValue_ConstructJsonValueBool_Params params;
	params.WorldContextObject = WorldContextObject;
	params.InValue = InValue;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueBinary
// (Final, Native, Static, Public, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// TArray<unsigned char>          ByteArray                      (Parm, ZeroConstructor)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueBinary(class UObject* WorldContextObject, TArray<unsigned char> ByteArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueBinary");

	USIOJsonValue_ConstructJsonValueBinary_Params params;
	params.WorldContextObject = WorldContextObject;
	params.ByteArray = ByteArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.ConstructJsonValueArray
// (Final, Native, Static, Public, HasOutParms, BlueprintCallable, BlueprintPure)
// Parameters:
// class UObject*                 WorldContextObject             (Parm, ZeroConstructor, IsPlainOldData)
// TArray<class USIOJsonValue*>   inArray                        (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
// class USIOJsonValue*           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonValue* USIOJsonValue::STATIC_ConstructJsonValueArray(class UObject* WorldContextObject, TArray<class USIOJsonValue*> inArray)
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.ConstructJsonValueArray");

	USIOJsonValue_ConstructJsonValueArray_Params params;
	params.WorldContextObject = WorldContextObject;
	params.inArray = inArray;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsString
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// struct FString                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

struct FString USIOJsonValue::AsString()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsString");

	USIOJsonValue_AsString_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsObject
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// class USIOJsonObject*          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

class USIOJsonObject* USIOJsonValue::AsObject()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsObject");

	USIOJsonValue_AsObject_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsNumber
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float USIOJsonValue::AsNumber()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsNumber");

	USIOJsonValue_AsNumber_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsBool
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool USIOJsonValue::AsBool()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsBool");

	USIOJsonValue_AsBool_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsBinary
// (Final, Native, Public, BlueprintCallable)
// Parameters:
// TArray<unsigned char>          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<unsigned char> USIOJsonValue::AsBinary()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsBinary");

	USIOJsonValue_AsBinary_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function SIOJson.SIOJsonValue.AsArray
// (Final, Native, Public, BlueprintCallable, BlueprintPure, Const)
// Parameters:
// TArray<class USIOJsonValue*>   ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class USIOJsonValue*> USIOJsonValue::AsArray()
{
	static auto fn = UObject::FindObject<UFunction>("Function SIOJson.SIOJsonValue.AsArray");

	USIOJsonValue_AsArray_Params params;

	auto flags = fn->FunctionFlags;
	fn->FunctionFlags |= 0x400;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
