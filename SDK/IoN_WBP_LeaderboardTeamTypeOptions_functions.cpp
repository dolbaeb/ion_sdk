// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Select Option
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTextBlock*              Text                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UImage*                  Image                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_LeaderboardTeamTypeOptions_C::Select_Option(class UTextBlock* Text, class UImage* Image)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Select Option");

	UWBP_LeaderboardTeamTypeOptions_C_Select_Option_Params params;
	params.Text = Text;
	params.Image = Image;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_LeaderboardTeamTypeOptions_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.Construct");

	UWBP_LeaderboardTeamTypeOptions_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardTeamTypeOptions_C::BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonSolos_K2Node_ComponentBoundEvent_139_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardTeamTypeOptions_C::BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonDuos_K2Node_ComponentBoundEvent_156_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardTeamTypeOptions_C::BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonSquads_K2Node_ComponentBoundEvent_174_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardTeamTypeOptions_C::BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardTeamTypeOptions_C_BndEvt__ButtonAllTime_K2Node_ComponentBoundEvent_32_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardTeamTypeOptions_C::ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions");

	UWBP_LeaderboardTeamTypeOptions_C_ExecuteUbergraph_WBP_LeaderboardTeamTypeOptions_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.OnTeamTypeChanged__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<ELeaderboardTeamTypes> Team_Type                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardTeamTypeOptions_C::OnTeamTypeChanged__DelegateSignature(TEnumAsByte<ELeaderboardTeamTypes> Team_Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardTeamTypeOptions.WBP_LeaderboardTeamTypeOptions_C.OnTeamTypeChanged__DelegateSignature");

	UWBP_LeaderboardTeamTypeOptions_C_OnTeamTypeChanged__DelegateSignature_Params params;
	params.Team_Type = Team_Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
