#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_Sako85_Elite.BP_Sako85_Elite_C
// 0x0008 (0x12B8 - 0x12B0)
class ABP_Sako85_Elite_C : public AIONSniperRifle
{
public:
	class UMaterialInterface*                          Sako_Material_Override;                                   // 0x12B0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_Sako85_Elite.BP_Sako85_Elite_C");
		return ptr;
	}


	void UserConstructionScript();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
