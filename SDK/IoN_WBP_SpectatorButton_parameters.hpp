#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SpectatorButton.WBP_SpectatorButton_C.Construct
struct UWBP_SpectatorButton_C_Construct_Params
{
};

// Function WBP_SpectatorButton.WBP_SpectatorButton_C.BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
struct UWBP_SpectatorButton_C_BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SpectatorButton.WBP_SpectatorButton_C.ExecuteUbergraph_WBP_SpectatorButton
struct UWBP_SpectatorButton_C_ExecuteUbergraph_WBP_SpectatorButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
