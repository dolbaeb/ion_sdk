#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_M9A1.ABP_M9A1_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26
struct UABP_M9A1_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26_Params
{
};

// Function ABP_M9A1.ABP_M9A1_C.BlueprintUpdateAnimation
struct UABP_M9A1_C_BlueprintUpdateAnimation_Params
{
	float*                                             DeltaTimeX;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function ABP_M9A1.ABP_M9A1_C.ExecuteUbergraph_ABP_M9A1
struct UABP_M9A1_C_ExecuteUbergraph_ABP_M9A1_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
