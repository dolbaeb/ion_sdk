#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_UMP45.ABP_UMP45_C
// 0x00F0 (0x04C0 - 0x03D0)
class UABP_UMP45_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_08D61C394CBDE4A0804C80907CE677A3;      // 0x03D8(0x0048)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_7E71979E4723D226058411ADB189364A;      // 0x0420(0x0068)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_A43B33C54EE57979D0E40687F339DE4D;// 0x0488(0x0038)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_UMP45.ABP_UMP45_C");
		return ptr;
	}


	void ExecuteUbergraph_ABP_UMP45(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
