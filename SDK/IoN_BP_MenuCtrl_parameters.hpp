#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_MenuCtrl.BP_MenuCtrl_C.UserConstructionScript
struct ABP_MenuCtrl_C_UserConstructionScript_Params
{
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.BPEvent_ShowOKDialog
struct ABP_MenuCtrl_C_BPEvent_ShowOKDialog_Params
{
	struct FString*                                    Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.ExecuteUbergraph_BP_MenuCtrl
struct ABP_MenuCtrl_C_ExecuteUbergraph_BP_MenuCtrl_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPopulateCustomServers__DelegateSignature
struct ABP_MenuCtrl_C_OnPopulateCustomServers__DelegateSignature_Params
{
	TArray<struct FIONServerInfo>                      servers;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPlayersReadyStatusesChanged__DelegateSignature
struct ABP_MenuCtrl_C_OnPlayersReadyStatusesChanged__DelegateSignature_Params
{
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemberLeave__DelegateSignature
struct ABP_MenuCtrl_C_OnPartyMemberLeave__DelegateSignature_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_MenuCtrl.BP_MenuCtrl_C.OnPartyMemeberJoined__DelegateSignature
struct ABP_MenuCtrl_C_OnPartyMemeberJoined__DelegateSignature_Params
{
	struct FMainMenuPartyMember                        PartyMember;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
