#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ItemCategory.WBP_ItemCategory_C.SetSelectionState
struct UWBP_ItemCategory_C_SetSelectionState_Params
{
	bool                                               bSelected;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ItemCategory.WBP_ItemCategory_C.BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
struct UWBP_ItemCategory_C_BndEvt__AK47_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ItemCategory.WBP_ItemCategory_C.Construct
struct UWBP_ItemCategory_C_Construct_Params
{
};

// Function WBP_ItemCategory.WBP_ItemCategory_C.PreConstruct
struct UWBP_ItemCategory_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ItemCategory.WBP_ItemCategory_C.ExecuteUbergraph_WBP_ItemCategory
struct UWBP_ItemCategory_C_ExecuteUbergraph_WBP_ItemCategory_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ItemCategory.WBP_ItemCategory_C.BtnClicked__DelegateSignature
struct UWBP_ItemCategory_C_BtnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
