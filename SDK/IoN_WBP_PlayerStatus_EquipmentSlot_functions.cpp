// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_PlayerStatus_EquipmentSlot_C::Get_Image_Icon_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_ColorAndOpacity_1");

	UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Icon_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_PlayerStatus_EquipmentSlot_C::Get_Image_Icon_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Icon_Brush_1");

	UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Icon_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Backfill_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_PlayerStatus_EquipmentSlot_C::Get_Image_Backfill_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Get_Image_Backfill_ColorAndOpacity_1");

	UWBP_PlayerStatus_EquipmentSlot_C_Get_Image_Backfill_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatus_EquipmentSlot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.Tick");

	UWBP_PlayerStatus_EquipmentSlot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatus_EquipmentSlot_C::ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatus_EquipmentSlot.WBP_PlayerStatus_EquipmentSlot_C.ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot");

	UWBP_PlayerStatus_EquipmentSlot_C_ExecuteUbergraph_WBP_PlayerStatus_EquipmentSlot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
