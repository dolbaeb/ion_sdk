// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponClip_C::Get_Image_FallbackWeapon_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Brush_1");

	UWBP_WeaponClip_C_Get_Image_FallbackWeapon_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_WeaponClip_C::Get_Image_FallbackWeapon_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.Get_Image_FallbackWeapon_Visibility_1");

	UWBP_WeaponClip_C_Get_Image_FallbackWeapon_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetVisibility_Firearm
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_WeaponClip_C::GetVisibility_Firearm()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetVisibility_Firearm");

	UWBP_WeaponClip_C_GetVisibility_Firearm_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Firemode
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponClip_C::GetBrush_Firemode()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Firemode");

	UWBP_WeaponClip_C_GetBrush_Firemode_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Firemode
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponClip_C::GetText_Firemode()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Firemode");

	UWBP_WeaponClip_C_GetText_Firemode_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Weapon
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_WeaponClip_C::GetBrush_Weapon()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetBrush_Weapon");

	UWBP_WeaponClip_C_GetBrush_Weapon_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_ExtraAmmo
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponClip_C::GetText_ExtraAmmo()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_ExtraAmmo");

	UWBP_WeaponClip_C_GetText_ExtraAmmo_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Clip
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_WeaponClip_C::GetText_Clip()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.GetText_Clip");

	UWBP_WeaponClip_C_GetText_Clip_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponClip_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.Tick");

	UWBP_WeaponClip_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.Reset
// (BlueprintCallable, BlueprintEvent)

void UWBP_WeaponClip_C::Reset()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.Reset");

	UWBP_WeaponClip_C_Reset_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_WeaponClip.WBP_WeaponClip_C.ExecuteUbergraph_WBP_WeaponClip
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_WeaponClip_C::ExecuteUbergraph_WBP_WeaponClip(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_WeaponClip.WBP_WeaponClip_C.ExecuteUbergraph_WBP_WeaponClip");

	UWBP_WeaponClip_C_ExecuteUbergraph_WBP_WeaponClip_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
