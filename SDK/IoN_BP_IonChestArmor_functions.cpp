// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_IonChestArmor.BP_IonChestArmor_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonChestArmor_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonChestArmor.BP_IonChestArmor_C.UserConstructionScript");

	ABP_IonChestArmor_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonChestArmor.BP_IonChestArmor_C.BPEvent_AttachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonChestArmor_C::BPEvent_AttachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonChestArmor.BP_IonChestArmor_C.BPEvent_AttachWearable");

	ABP_IonChestArmor_C_BPEvent_AttachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonChestArmor.BP_IonChestArmor_C.BPEvent_DetachWearable
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonChestArmor_C::BPEvent_DetachWearable()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonChestArmor.BP_IonChestArmor_C.BPEvent_DetachWearable");

	ABP_IonChestArmor_C_BPEvent_DetachWearable_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonChestArmor.BP_IonChestArmor_C.ExecuteUbergraph_BP_IonChestArmor
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IonChestArmor_C::ExecuteUbergraph_BP_IonChestArmor(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonChestArmor.BP_IonChestArmor_C.ExecuteUbergraph_BP_IonChestArmor");

	ABP_IonChestArmor_C_ExecuteUbergraph_BP_IonChestArmor_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
