#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BPI_GameSettingsInterface.BPI_GameSettingsInterface_C
// 0x0000 (0x0030 - 0x0030)
class UBPI_GameSettingsInterface_C : public UInterface
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BPI_GameSettingsInterface.BPI_GameSettingsInterface_C");
		return ptr;
	}


	void Update_Audio_Channel(TEnumAsByte<EAudioType> Audio_Channel, float Volume, bool* _);
	void Run_Console_Command(const struct FString& Console_Command, bool* _);
	void Get_Settings_Instance(class UBP_GameSettingsWrapper_C** SettingsWrapper);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
