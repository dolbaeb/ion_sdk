#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Construct
struct UWBP_PostMatchPlayer_C_Construct_Params
{
};

// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Fill UI Values
struct UWBP_PostMatchPlayer_C_Fill_UI_Values_Params
{
};

// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.ExecuteUbergraph_WBP_PostMatchPlayer
struct UWBP_PostMatchPlayer_C_ExecuteUbergraph_WBP_PostMatchPlayer_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
