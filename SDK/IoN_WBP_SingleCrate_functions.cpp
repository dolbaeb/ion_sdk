// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SingleCrate.WBP_SingleCrate_C.SetCrate
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SingleCrate_C::SetCrate(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.SetCrate");

	UWBP_SingleCrate_C_SetCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature
// (BlueprintEvent)

void UWBP_SingleCrate_C::BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature");

	UWBP_SingleCrate_C_BndEvt__WBP_RedeemBtn_K2Node_ComponentBoundEvent_14_RedeemCratePressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.ExecuteUbergraph_WBP_SingleCrate
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SingleCrate_C::ExecuteUbergraph_WBP_SingleCrate(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.ExecuteUbergraph_WBP_SingleCrate");

	UWBP_SingleCrate_C_ExecuteUbergraph_WBP_SingleCrate_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.ViewDetailsCrate__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SingleCrate_C::ViewDetailsCrate__DelegateSignature(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.ViewDetailsCrate__DelegateSignature");

	UWBP_SingleCrate_C_ViewDetailsCrate__DelegateSignature_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SingleCrate.WBP_SingleCrate_C.PurchaseCrate__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SingleCrate_C::PurchaseCrate__DelegateSignature(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SingleCrate.WBP_SingleCrate_C.PurchaseCrate__DelegateSignature");

	UWBP_SingleCrate_C_PurchaseCrate__DelegateSignature_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
