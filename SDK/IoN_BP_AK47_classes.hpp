#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_AK47.BP_AK47_C
// 0x0008 (0x1178 - 0x1170)
class ABP_AK47_C : public AIONFirearm
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x1170(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_AK47.BP_AK47_C");
		return ptr;
	}


	void UserConstructionScript();
	void BPEvent_OnPickedUp(class AIONCharacter** Character);
	void BPEvent_ApplyWeaponSkinMaterial();
	void ExecuteUbergraph_BP_AK47(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
