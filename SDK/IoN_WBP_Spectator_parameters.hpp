#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Spectator.WBP_Spectator_C.GetViewTargetPlayer
struct UWBP_Spectator_C_GetViewTargetPlayer_Params
{
	class APlayerState*                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Spectator.WBP_Spectator_C.Get_PlayerNameText_Text_1
struct UWBP_Spectator_C_Get_PlayerNameText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Spectator.WBP_Spectator_C.OnKeyDown
struct UWBP_Spectator_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Spectator.WBP_Spectator_C.Construct
struct UWBP_Spectator_C_Construct_Params
{
};

// Function WBP_Spectator.WBP_Spectator_C.Tick
struct UWBP_Spectator_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Spectator.WBP_Spectator_C.ExecuteUbergraph_WBP_Spectator
struct UWBP_Spectator_C_ExecuteUbergraph_WBP_Spectator_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
