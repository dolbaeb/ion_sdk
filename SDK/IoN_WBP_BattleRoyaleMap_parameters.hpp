#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.RotateWidget
struct UWBP_BattleRoyaleMap_C_RotateWidget_Params
{
	class UWidget*                                     Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              DeltaTime;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              RotationRate;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Tick
struct UWBP_BattleRoyaleMap_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Construct
struct UWBP_BattleRoyaleMap_C_Construct_Params
{
};

// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.ExecuteUbergraph_WBP_BattleRoyaleMap
struct UWBP_BattleRoyaleMap_C_ExecuteUbergraph_WBP_BattleRoyaleMap_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
