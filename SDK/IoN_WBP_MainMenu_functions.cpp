// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MainMenu.WBP_MainMenu_C.ShowKickMessage
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::ShowKickMessage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ShowKickMessage");

	UWBP_MainMenu_C_ShowKickMessage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.HideCompletely
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bHide                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenu_C::HideCompletely(bool bHide)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.HideCompletely");

	UWBP_MainMenu_C_HideCompletely_Params params;
	params.bHide = bHide;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.CrateOpening
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::CrateOpening()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.CrateOpening");

	UWBP_MainMenu_C_CrateOpening_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.OnCreditsUpdated
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::OnCreditsUpdated()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.OnCreditsUpdated");

	UWBP_MainMenu_C_OnCreditsUpdated_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.TransitionToInventory
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::TransitionToInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.TransitionToInventory");

	UWBP_MainMenu_C_TransitionToInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.Set Play Btn Image
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::Set_Play_Btn_Image()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.Set Play Btn Image");

	UWBP_MainMenu_C_Set_Play_Btn_Image_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_MainMenu_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.OnMouseButtonDown");

	UWBP_MainMenu_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ChangeZoom
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            NewZoomLevel                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenu_C::ChangeZoom(int NewZoomLevel)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ChangeZoom");

	UWBP_MainMenu_C_ChangeZoom_Params params;
	params.NewZoomLevel = NewZoomLevel;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ShowSubmenu
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            SubmenuIdx                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenu_C::ShowSubmenu(int SubmenuIdx)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ShowSubmenu");

	UWBP_MainMenu_C_ShowSubmenu_Params params;
	params.SubmenuIdx = SubmenuIdx;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.GetVisibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_MainMenu_C::GetVisibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.GetVisibility_1");

	UWBP_MainMenu_C_GetVisibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_MainMenu.WBP_MainMenu_C.On_ShadeOverlay_MouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_MainMenu_C::On_ShadeOverlay_MouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.On_ShadeOverlay_MouseButtonDown_1");

	UWBP_MainMenu_C_On_ShadeOverlay_MouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_MainMenu.WBP_MainMenu_C.OnSettingsMenuClosed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::OnSettingsMenuClosed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.OnSettingsMenuClosed");

	UWBP_MainMenu_C_OnSettingsMenuClosed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__WBP_MainMenuBtn_K2Node_ComponentBoundEvent_877_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__LoadoutBtn_K2Node_ComponentBoundEvent_295_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__InventoryBtn_K2Node_ComponentBoundEvent_307_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__Button_Quit_K2Node_ComponentBoundEvent_1853_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ShowPartyInvite
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 PlayerName                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_MainMenu_C::ShowPartyInvite(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ShowPartyInvite");

	UWBP_MainMenu_C_ShowPartyInvite_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__ProfileBtn_K2Node_ComponentBoundEvent_34_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__Button_165_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__QuitBtn_K2Node_ComponentBoundEvent_83_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__QuitBtn_K2Node_ComponentBoundEvent_214_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__LeaderboardBtn_K2Node_ComponentBoundEvent_207_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__WBP_OKNotice_K2Node_ComponentBoundEvent_338_AcceptBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__TrainingBtn_K2Node_ComponentBoundEvent_380_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__Button_Playmatch_K2Node_ComponentBoundEvent_358_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ShowUIDebug
// (BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::ShowUIDebug()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ShowUIDebug");

	UWBP_MainMenu_C_ShowUIDebug_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MainMenu_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.Construct");

	UWBP_MainMenu_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__Appearance_K2Node_ComponentBoundEvent_52_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__RewardsBtn_K2Node_ComponentBoundEvent_80_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ShowOKNotice
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_MainMenu_C::ShowOKNotice(const struct FString& Message)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ShowOKNotice");

	UWBP_MainMenu_C_ShowOKNotice_Params params;
	params.Message = Message;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__ItemsButton_K2Node_ComponentBoundEvent_40_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.On Settings Menu Closed
// (BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::On_Settings_Menu_Closed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.On Settings Menu Closed");

	UWBP_MainMenu_C_On_Settings_Menu_Closed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__CustomizeBtn_K2Node_ComponentBoundEvent_21_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenu_C::BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature");

	UWBP_MainMenu_C_BndEvt__RewardsButton_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.ExecuteUbergraph_WBP_MainMenu
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenu_C::ExecuteUbergraph_WBP_MainMenu(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.ExecuteUbergraph_WBP_MainMenu");

	UWBP_MainMenu_C_ExecuteUbergraph_WBP_MainMenu_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenu.WBP_MainMenu_C.Profile Updated__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenu_C::Profile_Updated__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenu.WBP_MainMenu_C.Profile Updated__DelegateSignature");

	UWBP_MainMenu_C_Profile_Updated__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
