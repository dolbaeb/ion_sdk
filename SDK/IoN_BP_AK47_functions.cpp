// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_AK47.BP_AK47_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_AK47_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AK47.BP_AK47_C.UserConstructionScript");

	ABP_AK47_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AK47.BP_AK47_C.BPEvent_OnPickedUp
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONCharacter**          Character                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_AK47_C::BPEvent_OnPickedUp(class AIONCharacter** Character)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AK47.BP_AK47_C.BPEvent_OnPickedUp");

	ABP_AK47_C_BPEvent_OnPickedUp_Params params;
	params.Character = Character;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AK47.BP_AK47_C.BPEvent_ApplyWeaponSkinMaterial
// (Event, Public, BlueprintEvent)

void ABP_AK47_C::BPEvent_ApplyWeaponSkinMaterial()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AK47.BP_AK47_C.BPEvent_ApplyWeaponSkinMaterial");

	ABP_AK47_C_BPEvent_ApplyWeaponSkinMaterial_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AK47.BP_AK47_C.ExecuteUbergraph_BP_AK47
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_AK47_C::ExecuteUbergraph_BP_AK47(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AK47.BP_AK47_C.ExecuteUbergraph_BP_AK47");

	ABP_AK47_C_ExecuteUbergraph_BP_AK47_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
