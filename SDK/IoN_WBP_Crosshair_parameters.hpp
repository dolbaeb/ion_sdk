#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Crosshair.WBP_Crosshair_C.Get Line Color
struct UWBP_Crosshair_C_Get_Line_Color_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.Get Dot Color
struct UWBP_Crosshair_C_Get_Dot_Color_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.PreConstruct
struct UWBP_Crosshair_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.Construct
struct UWBP_Crosshair_C_Construct_Params
{
};

// Function WBP_Crosshair.WBP_Crosshair_C.Tick
struct UWBP_Crosshair_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.Update Reticle
struct UWBP_Crosshair_C_Update_Reticle_Params
{
	float                                              Spread;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.Set Dot Opacity
struct UWBP_Crosshair_C_Set_Dot_Opacity_Params
{
	float                                              Opacity;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.Set Lines Opacity
struct UWBP_Crosshair_C_Set_Lines_Opacity_Params
{
	float                                              Opacity;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Crosshair.WBP_Crosshair_C.ExecuteUbergraph_WBP_Crosshair
struct UWBP_Crosshair_C_ExecuteUbergraph_WBP_Crosshair_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
