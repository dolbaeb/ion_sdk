#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Player.WBP_Player_C.GetVisibility_1
struct UWBP_Player_C_GetVisibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Player.WBP_Player_C.MakeDisplayTextForItem
struct UWBP_Player_C_MakeDisplayTextForItem_Params
{
	class AIONItem*                                    Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Player.WBP_Player_C.GetText_2
struct UWBP_Player_C_GetText_2_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Player.WBP_Player_C.GetText_1
struct UWBP_Player_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Player.WBP_Player_C.GetInteractionWidget
struct UWBP_Player_C_GetInteractionWidget_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_Player.WBP_Player_C.UpdateInteractionWidget
struct UWBP_Player_C_UpdateInteractionWidget_Params
{
	struct FInteractionOption*                         InteractionOption;                                        // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	float*                                             DeltaTime;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Player.WBP_Player_C.HideInteractionWidget
struct UWBP_Player_C_HideInteractionWidget_Params
{
};

// Function WBP_Player.WBP_Player_C.Tick
struct UWBP_Player_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Player.WBP_Player_C.On Helmet Equipped
struct UWBP_Player_C_On_Helmet_Equipped_Params
{
};

// Function WBP_Player.WBP_Player_C.On Helmet Removed
struct UWBP_Player_C_On_Helmet_Removed_Params
{
};

// Function WBP_Player.WBP_Player_C.UpdateInteractionWidgetWithText
struct UWBP_Player_C_UpdateInteractionWidgetWithText_Params
{
	struct FText*                                      InteractionText;                                          // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	float*                                             DeltaTime;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool*                                              bHideInteractButton;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Player.WBP_Player_C.Construct
struct UWBP_Player_C_Construct_Params
{
};

// Function WBP_Player.WBP_Player_C.On Downed Out State Changed
struct UWBP_Player_C_On_Downed_Out_State_Changed_Params
{
};

// Function WBP_Player.WBP_Player_C.On Reviving State Changed
struct UWBP_Player_C_On_Reviving_State_Changed_Params
{
};

// Function WBP_Player.WBP_Player_C.OnWearablesChanged
struct UWBP_Player_C_OnWearablesChanged_Params
{
};

// Function WBP_Player.WBP_Player_C.ExecuteUbergraph_WBP_Player
struct UWBP_Player_C_ExecuteUbergraph_WBP_Player_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
