#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_AK47.ABP_AK47_C
// 0x00F0 (0x04C0 - 0x03D0)
class UABP_AK47_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_94DAC75D40F171177CF8DA8756FD3DCE;      // 0x03D8(0x0048)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_A3C9D13A40941EE146E70A9B1B543EFF;// 0x0420(0x0038)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_5FE1E4BE44BB8178BCFE8289D5CF3B81;      // 0x0458(0x0068)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_AK47.ABP_AK47_C");
		return ptr;
	}


	void ExecuteUbergraph_ABP_AK47(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
