// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_RedDot.BP_RedDot_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_RedDot_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_RedDot.BP_RedDot_C.UserConstructionScript");

	ABP_RedDot_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_RedDot.BP_RedDot_C.UpdateMaterialAimState
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         AimRatio                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_RedDot_C::UpdateMaterialAimState(class AIONFirearm** Firearm, float* AimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_RedDot.BP_RedDot_C.UpdateMaterialAimState");

	ABP_RedDot_C_UpdateMaterialAimState_Params params;
	params.Firearm = Firearm;
	params.AimRatio = AimRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_RedDot.BP_RedDot_C.DetachFromFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_RedDot_C::DetachFromFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_RedDot.BP_RedDot_C.DetachFromFirearmBlueprint");

	ABP_RedDot_C_DetachFromFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_RedDot.BP_RedDot_C.AttachToFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_RedDot_C::AttachToFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_RedDot.BP_RedDot_C.AttachToFirearmBlueprint");

	ABP_RedDot_C_AttachToFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_RedDot.BP_RedDot_C.ExecuteUbergraph_BP_RedDot
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_RedDot_C::ExecuteUbergraph_BP_RedDot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_RedDot.BP_RedDot_C.ExecuteUbergraph_BP_RedDot");

	ABP_RedDot_C_ExecuteUbergraph_BP_RedDot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
