#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_IonArmsArmor.BP_IonArmsArmor_C
// 0x0018 (0x0498 - 0x0480)
class ABP_IonArmsArmor_C : public ABP_ArmorArms_C
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0480(0x0008) (Transient, DuplicateTransient)
	class USkeletalMesh*                               ArmorFPMesh;                                              // 0x0488(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               DefaultFPMesh;                                            // 0x0490(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_IonArmsArmor.BP_IonArmsArmor_C");
		return ptr;
	}


	void UserConstructionScript();
	void BPEvent_AttachWearable();
	void BPEvent_DetachWearable();
	void ExecuteUbergraph_BP_IonArmsArmor(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
