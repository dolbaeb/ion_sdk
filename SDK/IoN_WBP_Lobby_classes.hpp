#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// WidgetBlueprintGeneratedClass WBP_Lobby.WBP_Lobby_C
// 0x0020 (0x0250 - 0x0230)
class UWBP_Lobby_C : public UIONLobbyUserWidget
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0230(0x0008) (Transient, DuplicateTransient)
	class UObject*                                     NewVar_1;                                                 // 0x0238(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FScriptMulticastDelegate                    OpenFriendsListEvent;                                     // 0x0240(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("WidgetBlueprintGeneratedClass WBP_Lobby.WBP_Lobby_C");
		return ptr;
	}


	void RemovePartyMember(const struct FMainMenuPartyMember& PartyMember);
	void AddPartyMember(const struct FMainMenuPartyMember& PartyMember);
	void AddBtnClicked();
	void RefreshMembers();
	void Tick(struct FGeometry* MyGeometry, float* InDeltaTime);
	void ExecuteUbergraph_WBP_Lobby(int EntryPoint);
	void OpenFriendsListEvent__DelegateSignature();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
