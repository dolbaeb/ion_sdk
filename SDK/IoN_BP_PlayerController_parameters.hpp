#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_PlayerController.BP_PlayerController_C.UserConstructionScript
struct ABP_PlayerController_C_UserConstructionScript_Params
{
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteHUD
struct ABP_PlayerController_C_IONDebugDeleteHUD_Params
{
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugPawnTick
struct ABP_PlayerController_C_IONDebugPawnTick_Params
{
	int*                                               bEnable;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugAnimation
struct ABP_PlayerController_C_IONDebugAnimation_Params
{
	int*                                               bEnable;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugDeleteAllItems
struct ABP_PlayerController_C_IONDebugDeleteAllItems_Params
{
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugItemTick
struct ABP_PlayerController_C_IONDebugItemTick_Params
{
	int*                                               bEnable;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugSetLocation
struct ABP_PlayerController_C_IONDebugSetLocation_Params
{
	int*                                               Idx;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugSetCameraControl
struct ABP_PlayerController_C_IONDebugSetCameraControl_Params
{
	int*                                               Idx;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugSpawnChar
struct ABP_PlayerController_C_IONDebugSpawnChar_Params
{
	int*                                               Count;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugItemsMat
struct ABP_PlayerController_C_IONDebugItemsMat_Params
{
};

// Function BP_PlayerController.BP_PlayerController_C.IONDebugDFShadowsMode
struct ABP_PlayerController_C_IONDebugDFShadowsMode_Params
{
	int*                                               Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.PrepareLandingAudioEvent
struct ABP_PlayerController_C_PrepareLandingAudioEvent_Params
{
};

// Function BP_PlayerController.BP_PlayerController_C.UpdateVoiceUI
struct ABP_PlayerController_C_UpdateVoiceUI_Params
{
	bool*                                              bProximity;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool*                                              bActivated;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_PlayerController.BP_PlayerController_C.OnMatchResultsReceived
struct ABP_PlayerController_C_OnMatchResultsReceived_Params
{
	struct FPlayerMatchHistory*                        MatchResult;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PlayerController.BP_PlayerController_C.BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature
struct ABP_PlayerController_C_BndEvt__GSMessageListeners_K2Node_ComponentBoundEvent_0_OnAchievementEarnedMessage__DelegateSignature_Params
{
	struct FGSAchievementEarnedMessage                 AchievementEarnedMessage;                                 // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_PlayerController.BP_PlayerController_C.ExecuteUbergraph_BP_PlayerController
struct ABP_PlayerController_C_ExecuteUbergraph_BP_PlayerController_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
