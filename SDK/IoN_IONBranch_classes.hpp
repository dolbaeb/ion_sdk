#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class IONBranch.AnnouncerAudioComponent
// 0x0010 (0x04F0 - 0x04E0)
class UAnnouncerAudioComponent : public UAkComponent
{
public:
	TArray<class UAkAudioEvent*>                       QueuedAnnouncerSounds;                                    // 0x04E0(0x0010) (ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.AnnouncerAudioComponent");
		return ptr;
	}


	bool PlayAnnouncerSound(class UAkAudioEvent* EventToPlay, bool bHighPriority);
	void OnSafeZoneMarked();
	void OnPlasmaConvergenceStopped();
	void OnPlasmaConvergenceStarted();
	void OnNextRoundCountdownStarted();
	void OnMatchEnded();
	void OnLocalPlayerWon();
	void OnLocalPlayerDied();
	void OnLastStageCompleted();
	void OnFinalConvergenceStarted();
	void OnBattleRoyaleStarted();
	void OnBattleRoyaleCountdownStarted();
};


// Class IONBranch.ArenaAudioComponent
// 0x0010 (0x04F0 - 0x04E0)
class UArenaAudioComponent : public UAkComponent
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x04E0(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.ArenaAudioComponent");
		return ptr;
	}


	bool PlayArenaSound(class UAkAudioEvent* EventToPlay, bool bHighPriority);
	void OnSafeZoneMarked();
	void OnPlasmaConvergenceStopped();
	void OnPlasmaConvergenceStarted();
	void OnNextRoundCountdownStarted();
	void OnMatchEnded();
	void OnLocalPlayerWon();
	void OnLocalPlayerDied();
	void OnLastStageCompleted();
	void OnFinalConvergenceStarted();
	void OnBattleRoyaleStarted();
	void OnBattleRoyaleCountdownStarted();
};


// Class IONBranch.AutomationSettings
// 0x0008 (0x0038 - 0x0030)
class UAutomationSettings : public UObject
{
public:
	int                                                MaxCharacterCount;                                        // 0x0030(0x0004) (Edit, ZeroConstructor, Config, NoClear, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0034(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.AutomationSettings");
		return ptr;
	}

};


// Class IONBranch.IONGameState
// 0x02C0 (0x0640 - 0x0380)
class AIONGameState : public AGameState
{
public:
	class UAnnouncerAudioComponent*                    AnnouncerAudioComponent;                                  // 0x0380(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	EIONPartyTypes                                     ServerType;                                               // 0x0388(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x2AF];                                     // 0x0389(0x02AF) MISSED OFFSET
	int                                                PlayersAlive;                                             // 0x0638(0x0004) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x063C(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameState");
		return ptr;
	}


	int GetNumPlayersAlive();
	class UAnnouncerAudioComponent* GetAnnouncerAudioComponent();
	void BP_OnPlayerDied(class APlayerState* Victim, class APlayerState* DamageInstigator, class AActor* DamageCauser, bool bHeadshot, bool bKnockdown, int Range);
};


// Class IONBranch.BattleRoyaleGameState
// 0x0420 (0x0A60 - 0x0640)
class ABattleRoyaleGameState : public AIONGameState
{
public:
	float                                              BattleRoyaleStartTime;                                    // 0x0640(0x0004) (Net, ZeroConstructor, IsPlainOldData)
	bool                                               bPlasmaFieldEnabled;                                      // 0x0644(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0645(0x0003) MISSED OFFSET
	float                                              ServerTickRate;                                           // 0x0648(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient, IsPlainOldData)
	bool                                               bAlwaysEnableInventory;                                   // 0x064C(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	EBattleRoyaleMatchState                            BattleState;                                              // 0x064D(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x2];                                       // 0x064E(0x0002) MISSED OFFSET
	float                                              TimeUntilForcedMatchStart;                                // 0x0650(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient, IsPlainOldData)
	int                                                SpawnRandomSeed;                                          // 0x0654(0x0004) (Net, ZeroConstructor, IsPlainOldData)
	struct FSpawnedItemArray                           SpawnedItems;                                             // 0x0658(0x00C8) (Net, Transient)
	TArray<struct FPlasmaStage>                        PlasmaStages;                                             // 0x0720(0x0010) (Net, ZeroConstructor)
	float                                              InitialPlasmaRadius;                                      // 0x0730(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	struct FVector                                     InitialPlasmaLocation;                                    // 0x0734(0x000C) (Net, IsPlainOldData)
	unsigned char                                      UnknownData02[0x320];                                     // 0x0740(0x0320) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.BattleRoyaleGameState");
		return ptr;
	}


	void OnRep_SpawnRandomSeed();
	void OnRep_PlasmaStages();
	void OnRep_InitialPlasmaRadius();
	void OnRep_InitialPlasmaLocation();
	void OnRep_BattleRoyaleStartTime();
	bool HasBattleRoyaleStarted();
	float GetTimeSinceMatchStart();
	float GetMatchStartTime();
	struct FVector GetInitialPlasmaLocation();
};


// Class IONBranch.BeamDrop
// 0x0070 (0x0390 - 0x0320)
class ABeamDrop : public AActor
{
public:
	class UParticleSystemComponent*                    BeamParticle;                                             // 0x0320(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UParticleSystemComponent*                    DropLocationParticle;                                     // 0x0328(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USceneComponent*                             DropLocation;                                             // 0x0330(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UAkAudioEvent*                               StartBeamDropEvent;                                       // 0x0338(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               EndBeamDropEvent;                                         // 0x0340(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               BeamHumEvent;                                             // 0x0348(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlasmaBallHumEvent;                                       // 0x0350(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlasmaBallVanishEvent;                                    // 0x0358(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UDecalComponent*                             DropLocationDecal;                                        // 0x0360(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UClass*                                      ItemCacheToSpawn;                                         // 0x0368(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              HeightSpawnPoint;                                         // 0x0370(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BeamLength;                                               // 0x0374(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BallTravelSpeed;                                          // 0x0378(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BeamDropTimeToEnd;                                        // 0x037C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x10];                                      // 0x0380(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.BeamDrop");
		return ptr;
	}

};


// Class IONBranch.IONBasePlayerController
// 0x02D0 (0x0940 - 0x0670)
class AIONBasePlayerController : public APlayerController
{
public:
	class AIONSquad*                                   AuthoritySquad;                                           // 0x0670(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class AIONSquadState*                              SquadState;                                               // 0x0678(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	bool                                               bIsMasterServerAuthed;                                    // 0x0680(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0681(0x0007) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnGameSparksFaileDelegate;                                // 0x0688(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	int                                                PlayerCredits;                                            // 0x0698(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x069C(0x0004) MISSED OFFSET
	struct FScriptMulticastDelegate                    GameSparksCreditsUpdatedDelegate;                         // 0x06A0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    GameSparksOpenCrateDelegate;                              // 0x06B0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    GameSparksPurchaseCrateDelegate;                          // 0x06C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGameSparksPurchaseCrateFailedDelegate;                  // 0x06D0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FIONSteamInventoryItem                      CachedOpenedCrate;                                        // 0x06E0(0x0010)
	struct FIONSteamInventoryItem                      CachedScrapItem;                                          // 0x06F0(0x0010)
	struct FScriptMulticastDelegate                    GameSparksScrapItemDelegate;                              // 0x0700(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGameSparksScrapFailedDelegate;                          // 0x0710(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    GameSparksLoadoutDelegate;                                // 0x0720(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    GameSparksLoadoutFailedDelegate;                          // 0x0730(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FLoadoutData                                PlayerLoadout;                                            // 0x0740(0x0160) (Edit, BlueprintVisible, EditConst)
	struct FScriptMulticastDelegate                    SteamInventoryUpdateDelegate;                             // 0x08A0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData02[0x8];                                       // 0x08B0(0x0008) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnSteamPurchaseSuccessfull;                               // 0x08B8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSteamPurchaseFailed;                                    // 0x08C8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	TArray<class UIONWeaponSkin*>                      WeaponSkinList;                                           // 0x08D8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FScriptMulticastDelegate                    GameSparksAuthenticatedDelegate;                          // 0x08E8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData03[0x48];                                      // 0x08F8(0x0048) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONBasePlayerController");
		return ptr;
	}


	void UpdateSteamInventoryItems();
	void SteamPurchaseSuccessfull__DelegateSignature(int NewItem);
	void SteamPurchaseFailed__DelegateSignature();
	void SteamInventoryUpdateDelegate__DelegateSignature();
	void ShowHighlights();
	void SetAudioOutputDevicesVolume(int Volume);
	void SetAudioOutputDevice(const struct FString& Name);
	void SetAudioInputDevicesVolume(int Volume);
	void SetAudioInputDevice(const struct FString& Name);
	void SerializeLoadout();
	void SaveLoadout();
	void RemoveSkinFromLoadout(const struct FIONSteamInventoryItem& ItemToRemove);
	void RecordTestHighlight();
	void ProcessVirtualGoodFromSteam(const struct FString& OrderId);
	void OpenHighlightsGroup();
	void OnTeamChatMessageReceived(const struct FGSTeamChatMessage& TeamChatMessage);
	void OnScriptMessageReceived(const struct FGSScriptMessage& ScriptMessage);
	void OnRep_PlayerState();
	void OnNewTeamScoreMessageReceived(const struct FGSNewTeamScoreMessage& NewTeamScoreMessage);
	void OnNewHighScoreMessageReceived(const struct FGSNewHighScoreMessage& NewHighScoreMessage);
	void OnGlobalRankChangedMessageReceived(const struct FGSGlobalRankChangedMessage& GlobalRankChangedMessage);
	void OnAchievementEarnedMessageReceived(const struct FGSAchievementEarnedMessage& AchievementEarnedMessage);
	void LoadGlobalLeaderboardData(const struct FString& LeaderboardShortCode, const struct FString& GameMode, int NumEntries, bool bIsSocial, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed);
	void LoadAroundMeLeaderboardData(const struct FString& LeaderboardShortCode, const struct FString& GameMode, int NumEntries, bool bIsSocial, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed);
	bool IsSameSquadAs(class APlayerState* OtherPlayer);
	bool IsItemInLoadout(const struct FIONSteamInventoryItem& Item);
	void GS_UpdateCredits();
	void GS_ScrapItem(const struct FIONSteamInventoryItem& ScrapItem);
	void GS_PurchaseCrate(class UIONSteamCrate* CrateToPurchase);
	void GS_OpenCrate(const struct FIONSteamInventoryItem& CrateToOpen);
	void GS_LoadStoreItems();
	void GS_LoadLoadout_Server();
	void GS_LoadLoadout();
	struct FString GetSteamSessionTicket();
	TArray<class APlayerState*> GetSquadMates();
	struct FString GetSquadID();
	void GetLogEventData(const struct FString& EventKey);
	void GetListOfDifferentLeaderboards();
	void GetKeysForAction(const struct FName& ActionName, TArray<struct FInputActionKeyMapping>* Bindings);
	class UIONSteamItem* GetItemBySteamDefIDFast(int ID);
	class UIONSteamItem* GetItemBySteamDefID(int ID);
	void GetGameSparksIDFromSteamID(const struct FString& SteamID, const struct FScriptDelegate& OnSuccess, const struct FScriptDelegate& OnFailed);
	struct FString GetCurrentAudioOutputDevice();
	struct FString GetCurrentAudioInputDevice();
	TArray<struct FString> GetAudioOutputDevices();
	TArray<struct FString> GetAudioInputDevices();
	class UIONWeaponSkin* GetAppliedSkinByWeapon(class AIONWeapon* Weapon);
	void GameSparksScrapItemDelegate__DelegateSignature(int Result);
	void GameSparksScrapFailedDelegate__DelegateSignature();
	void GameSparksPurchaseCrateFailedDelegate__DelegateSignature();
	void GameSparksPurchaseCrateDelegate__DelegateSignature(int ItemDefID);
	void GameSparksOpenCrateDelegate__DelegateSignature(int Result);
	void GameSparksLoadoutFailedDelegate__DelegateSignature();
	void GameSparksLoadoutDelegate__DelegateSignature();
	void GameSparksFaileDelegate__DelegateSignature();
	void GameSparksCreditsUpdatedDelegate__DelegateSignature();
	void GameSparksAuthenticatedDelegate__DelegateSignature();
	void FindAllWeaponSkins();
	void DelegateOnSuccessGetLeaderboard__DelegateSignature(TArray<struct FLeaderboardDataEntry> ScriptData);
	void DelegateOnSuccessGetGameSparksID__DelegateSignature(class UGameSparksScriptData* ScriptData);
	void DelegateOnFailedGetLeaderboard__DelegateSignature(TArray<struct FLeaderboardDataEntry> ScriptData);
	void DelegateOnFailedGetGameSparksID__DelegateSignature(class UGameSparksScriptData* ScriptData);
	void CloseHighlightsGroup();
	void ClientSetKickedMessage(const struct FText& KickedReason);
	void ApplyNewSkinToLoadout(const struct FIONSteamInventoryItem& NewItem);
};


// Class IONBranch.MainPlayerController
// 0x0300 (0x0C40 - 0x0940)
class AMainPlayerController : public AIONBasePlayerController
{
public:
	class UClass*                                      GameOverlayClass;                                         // 0x0940(0x0008) (ZeroConstructor, IsPlainOldData)
	class UIONGameOverlayWidget*                       GameOverlay;                                              // 0x0948(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UClass*                                      MenuWidgetClass;                                          // 0x0950(0x0008) (ZeroConstructor, IsPlainOldData)
	class UUserWidget*                                 MenuWidget;                                               // 0x0958(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x10];                                      // 0x0960(0x0010) MISSED OFFSET
	struct FMatchResults                               PlayerMatchResults;                                       // 0x0970(0x0010) (BlueprintVisible, BlueprintReadOnly)
	class UClass*                                      PlayerHUDClass;                                           // 0x0980(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      DeathHUDClass;                                            // 0x0988(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      VictoryHUDClass;                                          // 0x0990(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      PostMatchStatsHUDClass;                                   // 0x0998(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      SpectatorHUDClass;                                        // 0x09A0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      ObserverHUDClass;                                         // 0x09A8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      ObserverFirstPersonHUDClass;                              // 0x09B0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               OnKilledSound;                                            // 0x09B8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              InputViewScale;                                           // 0x09C0(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              InputViewScaleAimed;                                      // 0x09C4(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              InputViewScopeFOVScale;                                   // 0x09C8(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x09CC(0x0004) MISSED OFFSET
	class UAkAudioEvent*                               HitMarkerEvent;                                           // 0x09D0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HeadshotEvent;                                            // 0x09D8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bDisableGameDebugText;                                    // 0x09E0(0x0001) (ZeroConstructor, Config, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x09E1(0x0007) MISSED OFFSET
	class UClass*                                      CameraManClass;                                           // 0x09E8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsPrequeueLevelLoaded;                                   // 0x09F0(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsCameraMan;                                             // 0x09F1(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x6];                                       // 0x09F2(0x0006) MISSED OFFSET
	struct FString                                     PlayerSessionID;                                          // 0x09F8(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData04[0x8];                                       // 0x0A08(0x0008) MISSED OFFSET
	TArray<EIONAdminAccessLevels>                      AccessLevel;                                              // 0x0A10(0x0010) (Net, ZeroConstructor)
	bool                                               bShowPing;                                                // 0x0A20(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData05[0x7];                                       // 0x0A21(0x0007) MISSED OFFSET
	class AIONCharacter*                               OldCharacter;                                             // 0x0A28(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData06[0x8];                                       // 0x0A30(0x0008) MISSED OFFSET
	struct FPlayerMatchResult                          ReplicatedMatchResult;                                    // 0x0A38(0x0168) (BlueprintVisible, BlueprintReadOnly)
	float                                              CameramanToggleDelay;                                     // 0x0BA0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData07[0x4];                                       // 0x0BA4(0x0004) MISSED OFFSET
	class UIONAdminComponent*                          AdminComponent;                                           // 0x0BA8(0x0008) (ExportObject, Net, ZeroConstructor, Transient, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData08[0x10];                                      // 0x0BB0(0x0010) MISSED OFFSET
	class AMainAIController*                           LocalBotController;                                       // 0x0BC0(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData09[0x10];                                      // 0x0BC8(0x0010) MISSED OFFSET
	TArray<struct FProximityDebugInfo>                 DebugInfo;                                                // 0x0BD8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	unsigned char                                      UnknownData10[0x12];                                      // 0x0BE8(0x0012) MISSED OFFSET
	bool                                               bMuteAllSounds;                                           // 0x0BFA(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bMuteProximity;                                           // 0x0BFB(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bMuteSquad;                                               // 0x0BFC(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsLoggedIntoVivox;                                       // 0x0BFD(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsLoggingIntoVivox;                                      // 0x0BFE(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsLoggedIntoProximityChannel;                            // 0x0BFF(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsLoggedIntoSquadChannel;                                // 0x0C00(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData11[0x3];                                       // 0x0C01(0x0003) MISSED OFFSET
	struct FCachedProxData                             CachedData;                                               // 0x0C04(0x0024) (BlueprintVisible, BlueprintReadOnly)
	float                                              ProximityUpdateRate;                                      // 0x0C28(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bUseCustomProximityValues;                                // 0x0C2C(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData12[0x3];                                       // 0x0C2D(0x0003) MISSED OFFSET
	int                                                AudibleDistance;                                          // 0x0C30(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                ConversationalDistance;                                   // 0x0C34(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              AudioFadeIntensityByDistance;                             // 0x0C38(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EIONAudioFadeModel                                 AudioFadeModel;                                           // 0x0C3C(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData13[0x3];                                       // 0x0C3D(0x0003) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainPlayerController");
		return ptr;
	}


	void VoiceModeChanged(int NewVoiceMode);
	void UpdateVoiceUI(bool bProximity, bool bActivated);
	void ToggleScreenshotMode();
	void TogglePing();
	bool ToggleMutePlayerByPlayerState(class APlayerState* OtherPlayerState);
	void ToggleHUD();
	void ToggleHighlights();
	void ToggleFreeCam();
	void ToggleDebugText();
	void Suicide();
	void StopClientBot();
	void StartSpectating();
	void StartPlaying();
	void StartClientBot();
	void SpectatePreviousPlayer();
	void SpectateNextPlayer();
	void SpawnIn();
	void SortResultsByTimeAlive();
	void SortResultsByTeamIndex();
	void SortResultsByPlace();
	void SortResultsByKills();
	void SortResultsByHeadshotAccuracy();
	void SortResultsByDamageAmount();
	void SortResultsByAccuracy();
	void ServerToggleFreeCam();
	void ServerToggleCameraMan();
	void ServerSuicide();
	void ServerStartSpectating();
	void ServerStartPlaying();
	void ServerSpawnIn();
	void ServerSetDoorOpen(class ADoor* Door, bool bNewOpen);
	void ServerSendTeamMessage(const struct FString& S, const struct FName& Type);
	void ServerSendBEPacket(TArray<unsigned char> Packet);
	void ServerRespawn();
	void SendTeamMessage(const struct FString& S, const struct FName& Type);
	void SelfUnmuteAll();
	void SelfMuteAll();
	void Respawn();
	void ResetInputMode();
	void RemoveBots();
	void OpenMenu();
	void OnMatchResultsReceived(const struct FPlayerMatchHistory& MatchResult);
	void OnAudioLevelLoaded();
	void MoveCharacterToTransform(class AIONCharacter* CCharacter, const struct FTransform& NewTransform);
	void LogoutOfVivoxServer();
	void LoginToVivoxServer();
	void ListPrePassPolys();
	void ListPawns();
	void ListAsyncLoads(float MinLoadTime);
	bool IsStreamer();
	bool IsQA();
	bool IsDeveloper();
	bool IsAdmin();
	bool HasSpectatableTeammate();
	bool HasAccessLevel(EIONAdminAccessLevels RequestedLevel);
	void HandleReturnToMainMenu();
	void GotoBot(int BotIndex);
	float GetTimeUntilRespawn();
	struct FString GetSquadChannelName();
	float GetServerTickRate();
	struct FString GetProximityServerName();
	class UIONGameOverlayWidget* GetGameOverlay();
	class AIONCharacter* GetCharacter();
	float GetAudioEnergy();
	void EnableStaticMeshCollision();
	void DebugMenu(bool bShow);
	void ConnectToVivoxVoiceChannels();
	void ConnectToVivoxSquadChannel();
	void ConnectToVivoxProximityChannel();
	void CloseMenu();
	void CloseInventory();
	void ClientUnregisterPlayer();
	void ClientShowMatchResult(const struct FPlayerMatchHistory& MatchResult);
	void ClientShowEliminationScreen(const struct FPlayerMatchResult& Result);
	void ClientSendMatchResults(const struct FMatchResults& Results);
	void ClientSendBEPacket(TArray<unsigned char> Packet);
	void ClientRegisterPlayer();
	void ClientReceiveScoreEvent(EScoreEvent ScoreType, int Points);
	void ClientPushCmd(const struct FString& Cmd);
	void ClientPostLogin();
	void ClientOnPlayerKilled(const struct FHitInfo& LastHitInfo);
	void ClientOnPlayerDowned(const struct FHitInfo& LastHitInfo);
	void ClientConfirmHit(class AActor* HitActor, class UPrimitiveComponent* HitComponent, const struct FVector& HitRelativeLocation, bool bHeadshot);
	void ClearAsyncLoads();
	bool CanUseCameraMan();
	bool CanSpectate();
	bool CanRespawn();
	bool CanEverRespawn();
	void AddBots(int NumBots, bool bSpawnNearMe);
};


// Class IONBranch.BRPlayerController
// 0x00B8 (0x0CF8 - 0x0C40)
class ABRPlayerController : public AMainPlayerController
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0C40(0x0010) MISSED OFFSET
	bool                                               bIsMapLevelLoaded;                                        // 0x0C50(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsWaitingOnMapLoadToDrop;                                // 0x0C51(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0C52(0x0006) MISSED OFFSET
	class AActor*                                      EndGameActor;                                             // 0x0C58(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      bPlayerMapLoaded : 1;                                     // 0x0C60(0x0001)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0C61(0x0007) MISSED OFFSET
	class UClass*                                      EndGameClass;                                             // 0x0C68(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x88];                                      // 0x0C70(0x0088) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.BRPlayerController");
		return ptr;
	}


	void ServerOnPlayerMapFullyLoaded();
	void OnPrematchLevelLoaded();
	void OnMapLevelLoaded();
	void IONDebugSpawnChar(int Count);
	void IONDebugSetLocation(int Idx);
	void IONDebugSetCameraControl(int Idx);
	void IONDebugPawnTick(int bEnable);
	void IONDebugItemTick(int bEnable);
	void IONDebugItemsMat();
	void IONDebugDFShadowsMode(int Mode);
	void IONDebugDeleteHUD();
	void IONDebugDeleteAllItems();
	void IONDebugAnimation(int bEnable);
	void ForceOnPrematchLevelLoaded();
	void ForceOnMapLevelLoaded();
	void ClientSetAndHoldCameraFade(bool bEnableFading, const struct FColor& FadeColor, const struct FVector2D& FadeAlpha, float FadeTime, bool bFadeAudio);
	void ClientOnDropInCountdownStarted();
};


// Class IONBranch.MainProjectile
// 0x00C8 (0x03E8 - 0x0320)
class AMainProjectile : public AActor
{
public:
	unsigned char                                      UnknownData00[0xA0];                                      // 0x0320(0x00A0) MISSED OFFSET
	class UStaticMeshComponent*                        Mesh;                                                     // 0x03C0(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	float                                              MaxDistance;                                              // 0x03C8(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              GravityScale;                                             // 0x03CC(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Suppression;                                              // 0x03D0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Bounciness;                                               // 0x03D4(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      DamageTypeClass;                                          // 0x03D8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                NumProjectiles;                                           // 0x03E0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x03E4(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainProjectile");
		return ptr;
	}

};


// Class IONBranch.Bullet
// 0x0008 (0x03F0 - 0x03E8)
class ABullet : public AMainProjectile
{
public:
	float                                              FirstFrameTraceDist;                                      // 0x03E8(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x03EC(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.Bullet");
		return ptr;
	}

};


// Class IONBranch.Buckshot
// 0x0078 (0x0468 - 0x03F0)
class ABuckshot : public ABullet
{
public:
	struct FRuntimeFloatCurve                          SpreadCurve;                                              // 0x03F0(0x0078) (Edit, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.Buckshot");
		return ptr;
	}

};


// Class IONBranch.DamageType_DBNO
// 0x0000 (0x0048 - 0x0048)
class UDamageType_DBNO : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_DBNO");
		return ptr;
	}

};


// Class IONBranch.DamageType_Explosion
// 0x0000 (0x0048 - 0x0048)
class UDamageType_Explosion : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_Explosion");
		return ptr;
	}

};


// Class IONBranch.DamageType_ForceKill
// 0x0000 (0x0048 - 0x0048)
class UDamageType_ForceKill : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_ForceKill");
		return ptr;
	}

};


// Class IONBranch.DamageType_Knife
// 0x0000 (0x0048 - 0x0048)
class UDamageType_Knife : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_Knife");
		return ptr;
	}

};


// Class IONBranch.DamageType_PlasmaField
// 0x0000 (0x0048 - 0x0048)
class UDamageType_PlasmaField : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_PlasmaField");
		return ptr;
	}

};


// Class IONBranch.DamageType_Projectile
// 0x0000 (0x0048 - 0x0048)
class UDamageType_Projectile : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_Projectile");
		return ptr;
	}

};


// Class IONBranch.DamageType_Shotgun
// 0x0000 (0x0048 - 0x0048)
class UDamageType_Shotgun : public UDamageType_Projectile
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_Shotgun");
		return ptr;
	}

};


// Class IONBranch.DamageType_Vehicle
// 0x0000 (0x0048 - 0x0048)
class UDamageType_Vehicle : public UDamageType
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DamageType_Vehicle");
		return ptr;
	}

};


// Class IONBranch.DaytimeActor
// 0x0000 (0x0320 - 0x0320)
class ADaytimeActor : public AActor
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DaytimeActor");
		return ptr;
	}


	void OnPlayerLeftVolume(class ADaytimeVolume* Volume);
	void OnPlayerEnteredVolume(class ADaytimeVolume* Volume);
};


// Class IONBranch.DaytimeVolume
// 0x0008 (0x0360 - 0x0358)
class ADaytimeVolume : public AVolume
{
public:
	float                                              Daytime;                                                  // 0x0358(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x035C(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.DaytimeVolume");
		return ptr;
	}


	void OnBrushEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex);
	void OnBrushBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult);
};


// Class IONBranch.Door
// 0x0010 (0x0330 - 0x0320)
class ADoor : public AActor
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0320(0x0008) MISSED OFFSET
	bool                                               bIsOpen;                                                  // 0x0328(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0329(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.Door");
		return ptr;
	}


	void OpenDoor(class AMainPlayerController* PC);
	void OnRep_bIsOpen();
	void OnDoorOpened();
	void OnDoorClosed();
	void CloseDoor(class AMainPlayerController* PC);
};


// Class IONBranch.FriendsFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class UFriendsFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.FriendsFunctionLibrary");
		return ptr;
	}


	bool STATIC_IsFriendPlayingThisGame(const struct FBlueprintOnlineFriend& Friend);
	bool STATIC_IsFriendPlaying(const struct FBlueprintOnlineFriend& Friend);
	bool STATIC_IsFriendOnline(const struct FBlueprintOnlineFriend& Friend);
	bool STATIC_IsFriendJoinable(const struct FBlueprintOnlineFriend& Friend);
	bool STATIC_IsFriendInvitable(const struct FBlueprintOnlineFriend& Friend);
	struct FString STATIC_GetFriendName(const struct FBlueprintOnlineFriend& Friend);
	EBlueprintInviteStatus STATIC_GetFriendInviteStatus(const struct FBlueprintOnlineFriend& Friend);
	struct FString STATIC_GetFriendId(const struct FBlueprintOnlineFriend& Friend);
	class UTexture2D* STATIC_GetFriendAvatar(class UObject* Outer, const struct FBlueprintOnlineFriend& Friend, ESteamAvatarSize Size);
};


// Class IONBranch.GrenadeProjectile
// 0x0038 (0x0420 - 0x03E8)
class AGrenadeProjectile : public AMainProjectile
{
public:
	float                                              Damage;                                                   // 0x03E8(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DamageRadius;                                             // 0x03EC(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UParticleSystem*                             ExplosionEmitter;                                         // 0x03F0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               ExplosionSound;                                           // 0x03F8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      CameraShakeClass;                                         // 0x0400(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              CameraShakeRadius;                                        // 0x0408(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MinScale;                                                 // 0x040C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxScale;                                                 // 0x0410(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              RangeFactor;                                              // 0x0414(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bHitGround;                                               // 0x0418(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0419(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GrenadeProjectile");
		return ptr;
	}

};


// Class IONBranch.GunGameRules
// 0x0010 (0x0040 - 0x0030)
class UGunGameRules : public UObject
{
public:
	TArray<struct FGunGameStage>                       GunGameStages;                                            // 0x0030(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GunGameRules");
		return ptr;
	}

};


// Class IONBranch.IconRenderActor
// 0x0030 (0x0350 - 0x0320)
class AIconRenderActor : public AActor
{
public:
	class USceneCaptureComponent2D*                    SceneCapture;                                             // 0x0320(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        StaticMesh;                                               // 0x0328(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      SkeletalMesh;                                             // 0x0330(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	bool                                               bCaptureIcon;                                             // 0x0338(0x0001) (Edit, ZeroConstructor, Transient, IsPlainOldData)
	bool                                               bCaptureSquare;                                           // 0x0339(0x0001) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x033A(0x0006) MISSED OFFSET
	struct FString                                     Filename;                                                 // 0x0340(0x0010) (Edit, ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IconRenderActor");
		return ptr;
	}

};


// Class IONBranch.ImpactOverrideInterface
// 0x0000 (0x0030 - 0x0030)
class UImpactOverrideInterface : public UInterface
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.ImpactOverrideInterface");
		return ptr;
	}

};


// Class IONBranch.IndoorVolume
// 0x0018 (0x0370 - 0x0358)
class AIndoorVolume : public AVolume
{
public:
	class UAkAudioEvent*                               AkAudioStartEvent;                                        // 0x0358(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               AkAudioStopEvent;                                         // 0x0360(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0368(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IndoorVolume");
		return ptr;
	}


	void OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex);
	void OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult);
};


// Class IONBranch.InteractionInterface
// 0x0000 (0x0030 - 0x0030)
class UInteractionInterface : public UInterface
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.InteractionInterface");
		return ptr;
	}

};


// Class IONBranch.IONAdminComponent
// 0x0000 (0x00F8 - 0x00F8)
class UIONAdminComponent : public UActorComponent
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONAdminComponent");
		return ptr;
	}


	void ServerTogglePlasma();
	void ServerStartMatch();
	void ServerSay(const struct FString& Command);
	void ServerPausePrequeue();
	void ServerLogAdminCommand(const struct FString& Command);
	void ServerGiveSkin(int SteamDefID);
	void ServerExec(const struct FString& Command);
	void DebugGiveSkin(int SteamDefID);
	void AdminTogglePlasma();
	void AdminStartMatch();
	void AdminSay(const struct FString& Command);
	void AdminPausePrequeue();
	void AdminExec(const struct FString& Command);
};


// Class IONBranch.IONItem
// 0x00A8 (0x03C8 - 0x0320)
class AIONItem : public AActor
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0320(0x0008) MISSED OFFSET
	class UAkAudioEvent*                               ItemPickupEvent;                                          // 0x0328(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FItemDescription                            Description;                                              // 0x0330(0x0078) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FDropConfig                                 DropConfig;                                               // 0x03A8(0x0010) (Edit, DisableEditOnInstance)
	class AIONCharacter*                               LastOwner;                                                // 0x03B8(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FName                                       ItemSpawnIdentifier;                                      // 0x03C0(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONItem");
		return ptr;
	}


	void StopOutline();
	void StartOutline();
	bool RemoveFromInventory();
	void PlaceOnGround();
	bool IsInInventory(class AIONCharacter* Character);
	bool IsEquivalentTo(class AIONItem* Other);
	void Internal_PickUp(class AMainPlayerController* PC);
	struct FVector GetPickupLocation();
	struct FText GetItemTypeText();
	class UAkAudioEvent* GetItemPickupEvent();
	struct FItemDescription GetItemDescription();
	class AIONCharacter* GetCharacterChecked();
	class AIONCharacter* GetCharacter();
	void DestroyItemSafely(float LifeSpan);
	bool CanBePickedUp();
	bool CanBeDropped();
};


// Class IONBranch.IONMeshItem
// 0x0008 (0x03D0 - 0x03C8)
class AIONMeshItem : public AIONItem
{
public:
	class UStaticMeshComponent*                        Mesh;                                                     // 0x03C8(0x0008) (Edit, ExportObject, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMeshItem");
		return ptr;
	}

};


// Class IONBranch.IONStackableItem
// 0x0008 (0x03D8 - 0x03D0)
class AIONStackableItem : public AIONMeshItem
{
public:
	unsigned char                                      NumberOfItems;                                            // 0x03D0(0x0001) (Edit, Net, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x03D1(0x0003) MISSED OFFSET
	int                                                MaxItems;                                                 // 0x03D4(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONStackableItem");
		return ptr;
	}


	void SetNumberOfItems(int Value);
	void RemoveItems(int Amount);
	void RemoveAllItems();
	bool IsStackable();
	int GetNumberOfItems();
	int GetMaxItems();
	bool CanBeMergedWith(class AIONStackableItem* OtherItem);
	void AddItems(int Amount);
};


// Class IONBranch.IONAmmoBox
// 0x0010 (0x03E8 - 0x03D8)
class AIONAmmoBox : public AIONStackableItem
{
public:
	class UTexture2D*                                  RoundIcon;                                                // 0x03D8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	EFirearmType                                       AmmoType;                                                 // 0x03E0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x03E1(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONAmmoBox");
		return ptr;
	}

};


// Class IONBranch.IONCustomizationBaseWidget
// 0x0008 (0x0218 - 0x0210)
class UIONCustomizationBaseWidget : public UUserWidget
{
public:
	class UTexture2D*                                  DefaultLoadoutIcon;                                       // 0x0210(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCustomizationBaseWidget");
		return ptr;
	}


	void SetArmorVisibility(bool bNewVisibility, class UClass* WearableToHide);
	class AIONBasePlayerController* GetIONPC();
	class AIONCharacter* GetDisplayChar();
	class AIONCustomizationPawn* GetCustomizationPawn();
};


// Class IONBranch.IONAppearanceScreen
// 0x0000 (0x0218 - 0x0218)
class UIONAppearanceScreen : public UIONCustomizationBaseWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONAppearanceScreen");
		return ptr;
	}


	void UpdateSkinColorLoadoutIdx(unsigned char NewIdx);
	void UpdateHairLoadoutIdx(unsigned char NewIdx);
	void UpdateFaceLoadoutIdx(unsigned char NewIdx);
	unsigned char GetSkinColorLoadoutIdx();
	unsigned char GetHairLoadoutIdx();
	unsigned char GetFaceLoadoutIdx();
	class UTexture2D* GetFaceLoadoutIcon();
};


// Class IONBranch.IONWearable
// 0x0090 (0x0460 - 0x03D0)
class AIONWearable : public AIONMeshItem
{
public:
	EWearableSlot                                      WearableSlot;                                             // 0x03D0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x03D1(0x0003) MISSED OFFSET
	float                                              Health;                                                   // 0x03D4(0x0004) (Edit, Net, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x1];                                       // 0x03D8(0x0001) MISSED OFFSET
	bool                                               bProtectAllButSpecified;                                  // 0x03D9(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x6];                                       // 0x03DA(0x0006) MISSED OFFSET
	TArray<struct FName>                               ProtectedBones;                                           // 0x03E0(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	float                                              DamageAbsorptionFactor;                                   // 0x03F0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x03F4(0x0004) MISSED OFFSET
	struct FInventoryAttributes                        InventoryModifiers;                                       // 0x03F8(0x0068) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWearable");
		return ptr;
	}


	void SetMatParamOnBaseChar(const struct FName& ParamName, float Value);
	float GetHealthRatio();
	class AIONCharacter* GetCharacterOwner();
	void BPEvent_DetachWearable();
	void BPEvent_AttachWearable();
};


// Class IONBranch.IONArmor
// 0x0020 (0x0480 - 0x0460)
class AIONArmor : public AIONWearable
{
public:
	class USkeletalMeshComponent*                      BodyMesh;                                                 // 0x0460(0x0008) (Edit, ExportObject, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	struct FName                                       AttachSocket;                                             // 0x0468(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUseMasterPose;                                           // 0x0470(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0471(0x0007) MISSED OFFSET
	class UIONArmorSkin*                               CurrentArmorSkin;                                         // 0x0478(0x0008) (BlueprintVisible, Net, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONArmor");
		return ptr;
	}


	void UpdateArmorSkin();
	void RestoreDefaultSkin();
	void BPEvent_ApplyArmorMesh();
	void ApplyArmorSkin(class UIONArmorSkin* NewArmorSkin, bool bForce);
};


// Class IONBranch.IONSteamItem
// 0x00C0 (0x00F8 - 0x0038)
class UIONSteamItem : public UDataAsset
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0038(0x0010) MISSED OFFSET
	struct FString                                     DisplayName;                                              // 0x0048(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	class UClass*                                      Item;                                                     // 0x0058(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UIONPromotionalSet*                          PromotionalSet;                                           // 0x0060(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  SkinIcon;                                                 // 0x0068(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	EItemSkinRarity                                    ItemRarity;                                               // 0x0070(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0071(0x0007) MISSED OFFSET
	struct FString                                     AssetName;                                                // 0x0078(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                SteamItemDef;                                             // 0x0088(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsTradeable;                                             // 0x008C(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsMarketable;                                            // 0x008D(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsScrapable;                                             // 0x008E(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x1];                                       // 0x008F(0x0001) MISSED OFFSET
	struct FString                                     SteamPrice;                                               // 0x0090(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                ScrapCredits;                                             // 0x00A0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x00A4(0x0004) MISSED OFFSET
	struct FString                                     SteamDescription;                                         // 0x00A8(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	struct FString                                     SteamTags;                                                // 0x00B8(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	struct FColor                                      NameColor;                                                // 0x00C8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x4];                                       // 0x00CC(0x0004) MISSED OFFSET
	TArray<class UClass*>                              FoundInCrates;                                            // 0x00D0(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FString>                             OwnsPromo;                                                // 0x00E0(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	bool                                               bExport;                                                  // 0x00F0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData05[0x7];                                       // 0x00F1(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamItem");
		return ptr;
	}


	struct FColor GetRarityColor();
};


// Class IONBranch.IONArmorSkin
// 0x0010 (0x0108 - 0x00F8)
class UIONArmorSkin : public UIONSteamItem
{
public:
	class UClass*                                      Wearable;                                                 // 0x00F8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               ItemMesh;                                                 // 0x0100(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONArmorSkin");
		return ptr;
	}

};


// Class IONBranch.IONAttachment
// 0x0130 (0x0500 - 0x03D0)
class AIONAttachment : public AIONMeshItem
{
public:
	int                                                CompatibilityFlags;                                       // 0x03D0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x03D4(0x0004) MISSED OFFSET
	TArray<class UClass*>                              CompatibilityExceptions;                                  // 0x03D8(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	struct FFirearmModifiers                           WeaponModifiers;                                          // 0x03E8(0x00C8) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	TMap<class UClass*, struct FFirearmModifiers>      WeaponModifierOverrides;                                  // 0x04B0(0x0050) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONAttachment");
		return ptr;
	}


	bool IsCompatibleWith(class AIONFirearm* Firearm);
	TEnumAsByte<EAttachmentSlot> GetAttachmentSlot();
	void DetachFromFirearmBlueprint(class AIONFirearm* Firearm);
	void AttachToFirearmBlueprint(class AIONFirearm* Firearm);
};


// Class IONBranch.IONAudioVolume
// 0x0018 (0x0370 - 0x0358)
class AIONAudioVolume : public AVolume
{
public:
	class UAkAudioEvent*                               AkAudioStartEvent;                                        // 0x0358(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               AkAudioStopEvent;                                         // 0x0360(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0368(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONAudioVolume");
		return ptr;
	}


	void OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex);
	void OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult);
};


// Class IONBranch.IONBarrel
// 0x0018 (0x0518 - 0x0500)
class AIONBarrel : public AIONAttachment
{
public:
	EBarrelType                                        BarrelType;                                               // 0x0500(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0501(0x0003) MISSED OFFSET
	float                                              HorizontalRecoil;                                         // 0x0504(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              VerticalRecoil;                                           // 0x0508(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              RangeIncrease;                                            // 0x050C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DamageIncrease;                                           // 0x0510(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0514(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONBarrel");
		return ptr;
	}

};


// Class IONBranch.IONBaseMatchmakingUserWidget
// 0x0010 (0x0220 - 0x0210)
class UIONBaseMatchmakingUserWidget : public UUserWidget
{
public:
	class UIONGameInstance*                            GameInstance;                                             // 0x0210(0x0008) (ZeroConstructor, IsPlainOldData)
	class UIONMatchmaking*                             Matchmaking;                                              // 0x0218(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONBaseMatchmakingUserWidget");
		return ptr;
	}

};


// Class IONBranch.IONGameRuleSet
// 0x0090 (0x03B0 - 0x0320)
class AIONGameRuleSet : public AActor
{
public:
	class UClass*                                      RulesetStateClass;                                        // 0x0320(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class AIONRulesetState*                            RulesetState;                                             // 0x0328(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnTemplate, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData00[0x70];                                      // 0x0330(0x0070) MISSED OFFSET
	struct FString                                     DisplayName;                                              // 0x03A0(0x0010) (Edit, BlueprintVisible, ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameRuleSet");
		return ptr;
	}


	void SquadEliminated(class AIONSquadState* Squad);
	void SpawnPlayer(class ABRPlayerController* NewPlayer);
	void PostPlayerLogin(class APlayerController* Controller);
	void PlayerSpawned(class ABRPlayerController* NewPlayer, class APawn* Pawn, class AIONCharacter* Character);
	void PlayerLogout(class AController* Controller);
	void PlayerJoined(class AMainPlayerController* NewPlayer);
	void MatchStarted();
	void MatchLeft();
	void MatchJoined();
	bool HasBattleRoyaleStarted();
	class AIONGameMode* GetGameMode();
	struct FString GetDisplayName();
	void CheckMatchOver();
	void CharacterTookDamage(class AIONCharacter* Character, const struct FHitInfo& HitInfo, float DamageAmount);
	void CharacterRevived(class AIONCharacter* DownedCharacter, class AIONCharacter* ReviverCharacters);
	void CharacterDowned(class AIONCharacter* Character, const struct FHitInfo& LastHitInfo);
	void CharacterDied(class AIONCharacter* Character, const struct FHitInfo& LastHitInfo);
};


// Class IONBranch.IONBattleRoyaleRuleSet
// 0x0000 (0x03B0 - 0x03B0)
class AIONBattleRoyaleRuleSet : public AIONGameRuleSet
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONBattleRoyaleRuleSet");
		return ptr;
	}

};


// Class IONBranch.IONBehaviorButton
// 0x0058 (0x0440 - 0x03E8)
class UIONBehaviorButton : public UButton
{
public:
	unsigned char                                      UnknownData00[0x58];                                      // 0x03E8(0x0058) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONBehaviorButton");
		return ptr;
	}


	void HandleClick();
};


// Class IONBranch.IONButton
// 0x0020 (0x0340 - 0x0320)
class AIONButton : public AActor
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0320(0x0008) MISSED OFFSET
	struct FText                                       InteractionText;                                          // 0x0328(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONButton");
		return ptr;
	}


	void OnReleased(class AMainPlayerController* PC);
	void OnPressed(class AMainPlayerController* PC);
};


// Class IONBranch.IONCameraMan
// 0x00A0 (0x07D0 - 0x0730)
class AIONCameraMan : public ACharacter
{
public:
	class UClass*                                      SpectatorWidgetClass;                                     // 0x0730(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x26];                                      // 0x0738(0x0026) MISSED OFFSET
	bool                                               bCanSwitchPlayers;                                        // 0x075E(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x1];                                       // 0x075F(0x0001) MISSED OFFSET
	class AIONCharacter*                               AttachedPlayer;                                           // 0x0760(0x0008) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x0768(0x0004) MISSED OFFSET
	float                                              BaseTurnRate;                                             // 0x076C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	float                                              BaseLookUpRate;                                           // 0x0770(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x0774(0x0004) MISSED OFFSET
	class USpringArmComponent*                         SpringArm;                                                // 0x0778(0x0008) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCameraComponent*                            FirstPersonCameraComponent;                               // 0x0780(0x0008) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTexture*                                    CameraManHUDIconTexture;                                  // 0x0788(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData04[0x8];                                       // 0x0790(0x0008) MISSED OFFSET
	class UMaterialInterface*                          ShowSafeZoneMaterial;                                     // 0x0798(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData05[0x8];                                       // 0x07A0(0x0008) MISSED OFFSET
	bool                                               bIsTryingToGainAltitude;                                  // 0x07A8(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bIsTryingToLoseAltitude;                                  // 0x07A9(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData06[0x4];                                       // 0x07AA(0x0004) MISSED OFFSET
	bool                                               bShowPlayercards;                                         // 0x07AE(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EPlayerCardMode                                    PlayerCardViewMode;                                       // 0x07AF(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bDrawPlayerStencils;                                      // 0x07B0(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData07[0x3];                                       // 0x07B1(0x0003) MISSED OFFSET
	float                                              FlySpeedTarget;                                           // 0x07B4(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              FOVTarget;                                                // 0x07B8(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData08[0x4];                                       // 0x07BC(0x0004) MISSED OFFSET
	float                                              DOFFocalDistanceTarget;                                   // 0x07C0(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              DOFBlurSizeTarget;                                        // 0x07C4(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData09[0x8];                                       // 0x07C8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCameraMan");
		return ptr;
	}


	void SpectatePlayer(class AIONCharacter* Character);
	struct FString SpectateModeToString();
	void ServerSetFlySpeedTarget(float InFlySpeedTarget);
	void ServerDetachFromPlayer();
	void ServerAttachToPlayer(class AIONCharacter* Character, bool bFirstPerson);
	struct FString PlayercardViewModeToString();
	void OnOptionChanged(int OptionIndex);
	struct FString HUDViewToString();
	void DrawHUD(class AIONObserverCamHUD* PlayerHUD, class UCanvas* Canvas);
};


// Class IONBranch.IONCharacter
// 0x0570 (0x0CA0 - 0x0730)
class AIONCharacter : public ACharacter
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0730(0x0010) MISSED OFFSET
	float                                              MaxInteractionRange;                                      // 0x0740(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FDownedConfig                               DownedConfig;                                             // 0x0744(0x001C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	bool                                               bIsSlowingBloodLoss;                                      // 0x0760(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	ECharacterSignificanceLevel                        Significance;                                             // 0x0761(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x2];                                       // 0x0762(0x0002) MISSED OFFSET
	int                                                LastTickQuality;                                          // 0x0764(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              ViewDistance;                                             // 0x0768(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AdjustedViewDistance;                                     // 0x076C(0x0004) (ZeroConstructor, IsPlainOldData)
	uint16_t                                           RemoteViewYaw;                                            // 0x0770(0x0002) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x6];                                       // 0x0772(0x0006) MISSED OFFSET
	class APlayerState*                                DownedInstigator;                                         // 0x0778(0x0008) (ZeroConstructor, IsPlainOldData)
	class AActor*                                      DownedItem;                                               // 0x0780(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x20];                                      // 0x0788(0x0020) MISSED OFFSET
	unsigned char                                      bFiring : 1;                                              // 0x07A8(0x0001) (Net)
	unsigned char                                      bAiming : 1;                                              // 0x07A8(0x0001) (Net)
	unsigned char                                      bMapOpen : 1;                                             // 0x07A8(0x0001) (Net)
	unsigned char                                      bAttachmentActive : 1;                                    // 0x07A8(0x0001) (Net)
	unsigned char                                      UnknownData04 : 1;                                        // 0x07A8(0x0001)
	unsigned char                                      bInspectingWeapon : 1;                                    // 0x07A8(0x0001) (Net)
	unsigned char                                      UnknownData05[0x3];                                       // 0x07A9(0x0003) MISSED OFFSET
	uint32_t                                           WeaponBlockedUpdateFrame;                                 // 0x07AC(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      FireMode;                                                 // 0x07B0(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      FireRandomSeed;                                           // 0x07B1(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData06[0x1];                                       // 0x07B2(0x0001) MISSED OFFSET
	bool                                               bInspectingSkin;                                          // 0x07B3(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	float                                              HardLandingVelocityThreshold;                             // 0x07B4(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      bInteracting : 1;                                         // 0x07B8(0x0001) (BlueprintVisible, BlueprintReadOnly, Net)
	unsigned char                                      bReviving : 1;                                            // 0x07B8(0x0001) (BlueprintVisible, BlueprintReadOnly, Net)
	unsigned char                                      UnknownData07[0x7];                                       // 0x07B9(0x0007) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnRevivingStarted;                                        // 0x07C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnRevivingStopped;                                        // 0x07D0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	float                                              ReviveTime;                                               // 0x07E0(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData08[0x4];                                       // 0x07E4(0x0004) MISSED OFFSET
	TArray<struct FDamageInfo>                         DamageInfoArray;                                          // 0x07E8(0x0010) (Net, ZeroConstructor)
	unsigned char                                      UnknownData09[0x40];                                      // 0x07F8(0x0040) MISSED OFFSET
	class UClass*                                      DefaultMeleeClass;                                        // 0x0838(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class AIONWeapon*                                  CurrentWeapon;                                            // 0x0840(0x0008) (ZeroConstructor, IsPlainOldData)
	class AIONWeapon*                                  QueuedWeapon;                                             // 0x0848(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	class AIONItem*                                    PendingPickup;                                            // 0x0850(0x0008) (ZeroConstructor, IsPlainOldData)
	class AIONWeapon*                                  LastWeapon;                                               // 0x0858(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData10[0x8];                                       // 0x0860(0x0008) MISSED OFFSET
	class UAkAudioEvent*                               PickupAmmoItemEvent;                                      // 0x0868(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PickupWeaponItemEvent;                                    // 0x0870(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PickupAttachmentItemEvent;                                // 0x0878(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PickupWearableItemEvent;                                  // 0x0880(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerSwimmingEnter;                                      // 0x0888(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerSwimmingExit;                                       // 0x0890(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerUnderwaterEnter;                                    // 0x0898(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerUnderwaterExit;                                     // 0x08A0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerJumpStart1P;                                        // 0x08A8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerJumpStart3P;                                        // 0x08B0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerLaunchPadJump1P;                                    // 0x08B8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerLaunchPadJump3P;                                    // 0x08C0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerSlamLanding1P;                                      // 0x08C8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerSlamLanding3P;                                      // 0x08D0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerUnderwaterFootstep1P;                               // 0x08D8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerUnderwaterFootstep3P;                               // 0x08E0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerPlasmaEnter;                                        // 0x08E8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerPlasmaExit;                                         // 0x08F0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               CharacterFallingStart;                                    // 0x08F8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               CharacterFallingStop;                                     // 0x0900(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               CharacterDownedStart;                                     // 0x0908(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               CharacterDownedStop;                                      // 0x0910(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TMap<EFirearmType, class UClass*>                  AmmoBoxSpawnClasses;                                      // 0x0918(0x0050) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	struct FName                                       VehicleSeatName;                                          // 0x0968(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData11[0x4];                                       // 0x0970(0x0004) MISSED OFFSET
	float                                              RecoilInterpSpeed;                                        // 0x0974(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData12[0x1C];                                      // 0x0978(0x001C) MISSED OFFSET
	float                                              MaxSpread;                                                // 0x0994(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              MaxHorzRecoil;                                            // 0x0998(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              MaxVertRecoil;                                            // 0x099C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData13[0x44];                                      // 0x09A0(0x0044) MISSED OFFSET
	bool                                               bIsFreeLooking;                                           // 0x09E4(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	bool                                               bClosingFreeLook;                                         // 0x09E5(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData14[0x2];                                       // 0x09E6(0x0002) MISSED OFFSET
	struct FRotator                                    PreFreeLookRotation;                                      // 0x09E8(0x000C) (IsPlainOldData)
	unsigned char                                      UnknownData15[0x4];                                       // 0x09F4(0x0004) MISSED OFFSET
	TArray<class AActor*>                              CloseInteractiveActors;                                   // 0x09F8(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData16[0x10];                                      // 0x0A08(0x0010) MISSED OFFSET
	float                                              LastHitTime;                                              // 0x0A18(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FVector                                     LastHitLocation;                                          // 0x0A1C(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	float                                              LastHitDamage;                                            // 0x0A28(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              ProneEyeHeight;                                           // 0x0A2C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              DownedEyeHeight;                                          // 0x0A30(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      bIsSprinting : 1;                                         // 0x0A34(0x0001) (BlueprintVisible, BlueprintReadOnly, Net)
	unsigned char                                      bIsProne : 1;                                             // 0x0A34(0x0001) (BlueprintVisible, BlueprintReadOnly, Net)
	unsigned char                                      bIsDowned : 1;                                            // 0x0A34(0x0001) (BlueprintVisible, BlueprintReadOnly, Net)
	unsigned char                                      UnknownData17[0x3];                                       // 0x0A35(0x0003) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnDowned;                                                 // 0x0A38(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnRevived;                                                // 0x0A48(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnDied;                                                   // 0x0A58(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData18[0xC];                                       // 0x0A68(0x000C) MISSED OFFSET
	float                                              GrenadeThrowSpeed;                                        // 0x0A74(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              GrenadeTossSpeed;                                         // 0x0A78(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              GrenadeTossHeightOffset;                                  // 0x0A7C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              GrenadeThrowAngle;                                        // 0x0A80(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              GrenadeTossAngle;                                         // 0x0A84(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              JumpCooldownTime;                                         // 0x0A88(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SprintWeaponRecoveryTime;                                 // 0x0A8C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              KnifeThrowSpeed;                                          // 0x0A90(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              MinFallDamageHeight;                                      // 0x0A94(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              MaxFallDamageHeight;                                      // 0x0A98(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SuppressionFalloffRate;                                   // 0x0A9C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	TArray<struct FBoneGroup>                          BoneGroups;                                               // 0x0AA0(0x0010) (Edit, ZeroConstructor)
	TArray<struct FBoneDamageFactor>                   PerBoneDamage;                                            // 0x0AB0(0x0010) (Edit, ZeroConstructor)
	bool                                               bApplyFallDamage;                                         // 0x0AC0(0x0001) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData19[0x3];                                       // 0x0AC1(0x0003) MISSED OFFSET
	float                                              FOVFactorSpeed;                                           // 0x0AC4(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           JumpSequence;                                             // 0x0AC8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAnimSequenceBase*                           JumpSequenceFirstPerson;                                  // 0x0AD0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAnimSequenceBase*                           LandSequence;                                             // 0x0AD8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAnimSequenceBase*                           LandSequenceFirstPerson;                                  // 0x0AE0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAnimSequenceBase*                           WinSequence;                                              // 0x0AE8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               StartAimingSound;                                         // 0x0AF0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               StopAimingSound;                                          // 0x0AF8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerEvent;                                           // 0x0B00(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerHeadEvent;                                       // 0x0B08(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerShieldEvent;                                     // 0x0B10(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerEvent;                                          // 0x0B18(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerHeadEvent;                                      // 0x0B20(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerShieldEvent;                                    // 0x0B28(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerWitnessEvent;                                    // 0x0B30(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerHeadWitnessEvent;                                // 0x0B38(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerShieldWitnessEvent;                              // 0x0B40(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeEvent;                                      // 0x0B48(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeShieldEvent;                                // 0x0B50(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerKnifeEvent;                                     // 0x0B58(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerKnifeShieldEvent;                               // 0x0B60(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeWitnessEvent;                               // 0x0B68(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeShieldWitnessEvent;                         // 0x0B70(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeProjectileEvent;                            // 0x0B78(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeProjectileShieldEvent;                      // 0x0B80(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerKnifeProjectileEvent;                           // 0x0B88(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HurtMarkerKnifeProjectileShieldEvent;                     // 0x0B90(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeProjectileWitnessEvent;                     // 0x0B98(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitMarkerKnifeProjectileShieldWitnessEvent;               // 0x0BA0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               HitDroneEvent;                                            // 0x0BA8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               PlayerDeathEvent;                                         // 0x0BB0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               MapOpenSound;                                             // 0x0BB8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               MapCloseSound;                                            // 0x0BC0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               InventoryOpenSound;                                       // 0x0BC8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               InventoryCloseSound;                                      // 0x0BD0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               WeaponEquipAfterSprintSound;                              // 0x0BD8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               ReloadSound;                                              // 0x0BE0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               NanoMedSound;                                             // 0x0BE8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class USkeletalMeshComponent*                      FirstPersonMesh;                                          // 0x0BF0(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class USceneComponent*                             FPParent;                                                 // 0x0BF8(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UCameraComponent*                            Camera;                                                   // 0x0C00(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UWidgetComponent*                            MapWidgetComponent;                                       // 0x0C08(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UWidgetComponent*                            NametagWidgetComponent;                                   // 0x0C10(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class USceneCaptureComponent2D*                    SceneCapture;                                             // 0x0C18(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UDecalComponent*                             LaserDecal;                                               // 0x0C20(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        LaserBeamMesh;                                            // 0x0C28(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UIONInventoryComponent*                      InventoryComponent;                                       // 0x0C30(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData20[0x10];                                      // 0x0C38(0x0010) MISSED OFFSET
	float                                              CrouchCatchTime;                                          // 0x0C48(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              CrouchResetTime;                                          // 0x0C4C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bLandCrouch;                                              // 0x0C50(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData21[0x1];                                       // 0x0C51(0x0001) MISSED OFFSET
	bool                                               bIsInDrop;                                                // 0x0C52(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData22[0x1];                                       // 0x0C53(0x0001) MISSED OFFSET
	float                                              DropLandingTime;                                          // 0x0C54(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData23[0x2];                                       // 0x0C58(0x0002) MISSED OFFSET
	bool                                               bIsInSlamSequence;                                        // 0x0C5A(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData24[0x1];                                       // 0x0C5B(0x0001) MISSED OFFSET
	bool                                               bOverrideNametagVisibility;                               // 0x0C5C(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bHoldingFire;                                             // 0x0C5D(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData25[0xA];                                       // 0x0C5E(0x000A) MISSED OFFSET
	class UObject*                                     CachedInteractObject;                                     // 0x0C68(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData26[0x4];                                       // 0x0C70(0x0004) MISSED OFFSET
	float                                              NetRotationInterpSpeed;                                   // 0x0C74(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              RebasingDistance;                                         // 0x0C78(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData27[0x4];                                       // 0x0C7C(0x0004) MISSED OFFSET
	class UIONDropSkin*                                CurrentDropsuitSkin;                                      // 0x0C80(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	class AIONWearableMannequinAdvanced*               WearableMannequin;                                        // 0x0C88(0x0008) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UClass*                                      WearableMannequinClass;                                   // 0x0C90(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData28[0x8];                                       // 0x0C98(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCharacter");
		return ptr;
	}


	bool WantsToSprint();
	bool WantsToProne();
	bool WantsToFire();
	void UpdateSuppressionEffects(float SuppressionRatio);
	void UpdateShieldEffects();
	void UpdateHealthEffects(float HealthRatio, float OldHealthRatio);
	void UpdateFirstPersonVisibility();
	void UpdateEquipementSound_Wearable();
	void UpdateEquipementSound_Firearms();
	void UpdateEquipementSound_BoosterOrNade();
	void UpdateDropsuitSkin();
	void UnloadAmmo(class AIONFirearm* Weapon);
	bool TimeLeftTillImpact(float* OutTime, struct FHitResult* OutHit);
	void StopFiring();
	void StopAiming();
	bool SlotIsEnabled(int SlotIndex);
	bool SlotAcceptsItem(int SlotIndex, class AIONItem* Item);
	bool ShouldPlayAnim();
	void SetWorldOrigin(int X, int Y, int Z);
	void SetSprinting(bool NewSprinting);
	void SetProne(bool NewProne);
	void SetCrouching(bool NewCrouch);
	void SetCharacterMesh(const struct FString& MeshName);
	void ServerUpdateReviving(bool NewReviving);
	void ServerUpdateMapOpen(bool NewMapOpen);
	void ServerUpdateInteracting(bool NewInteracting);
	void ServerUpdateInspectingWeapon(bool NewInspectingWeapon);
	void ServerUpdateFiring(bool NewFiring);
	void ServerUpdateFireRandomSeed(unsigned char NewFireRandomSeed);
	void ServerUpdateAttachmentActive(bool NewAttachmentActive);
	void ServerUpdateAiming(bool NewAiming, float NewAimRatio);
	void ServerUnloadAmmo(class AIONFirearm* Weapon);
	void ServerStopRevive(class AMainPlayerController* PC, class AIONCharacter* Other);
	void ServerStartRevive(class AMainPlayerController* PC, class AIONCharacter* Other, float NewReviveTime);
	void ServerSetNewCachedInteractObject(class UObject* NewCachedInteractObject);
	void ServerSetIsSlowingBloodLoss(bool NewIsSlowing);
	void ServerSetInspectingSkin(bool bNewInspectingSkin);
	void ServerSetFreeLook(bool NewFreeLook);
	void ServerPickUpSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex);
	void ServerPickUp(class AIONItem* Item);
	void ServerMoveMagazineToSlot(class AIONFirearm* Firearm, int SlotIndex);
	void ServerMoveItemToSlotSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, int SlotIndex);
	void ServerMoveItemToSlot(class AIONItem* Item, int SlotIndex);
	void ServerMergeItemsSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, class AIONStackableItem* To);
	void ServerMergeItems(class AIONStackableItem* Item, class AIONStackableItem* To);
	void ServerExitVehicle();
	void ServerEquipWearableSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex);
	void ServerEquipWearable(class AIONWearable* Wearable);
	void ServerEquipWeapon(class AIONWeapon* Weapon);
	void ServerEnterVehicle(class AVehicleBase* Vehicle, const struct FName& SeatName);
	void ServerDropStackable(class AIONStackableItem* Item, int Amount);
	void ServerDropItem(class AIONItem* Item, bool bShuffle);
	void ServerDropAmmoPack(EFirearmType AmmoType, int Amount);
	void ServerAttachAttachmentToFirearmSpawn(class AItemSpawn* ItemSpawn, unsigned char ItemIndex, class AIONFirearm* Firearm);
	void ServerAttachAttachmentToFirearm(class AIONAttachment* Attachment, class AIONFirearm* Firearm);
	void ReviveReleased(class AMainPlayerController* PC);
	void RevivePressed(class AMainPlayerController* PC);
	void ResetBulletPaths();
	void PickUp(class AIONItem* Item);
	void OnStepped();
	void OnRevivingStopped__DelegateSignature();
	void OnRevivingStarted__DelegateSignature();
	void OnRevived__DelegateSignature();
	void OnRep_VehicleSeatName();
	void OnRep_Reviving();
	void OnRep_QueuedWeapon(class AIONWeapon* OldWeapon);
	void OnRep_IsSprinting();
	void OnRep_IsProne();
	void OnRep_IsDowned();
	void OnRep_DamageInfoArray();
	void OnRep_ChangeDropIn(bool OldIsInDrop);
	void OnInventoryChanged();
	void OnHardLanding();
	void OnDowned__DelegateSignature();
	void OnDied__DelegateSignature();
	void OnBulletSuppression(class AMainProjectile* Projectile);
	void OnAimingStateChanged(float NewAimRatio, bool bForce, class AIONWeapon* Weapon);
	void MulticastWeaponTask(EWeaponTask TaskID);
	void MulticastLaunchPadJump(const struct FVector& LaunchPadLocation, const struct FRotator& LaunchPadRotator);
	void MoveMagazineToSlot(class AIONFirearm* Firearm, int SlotIndex);
	void MoveItemToSlot(class AIONItem* Item, int SlotIndex);
	void MergeItems(class AIONStackableItem* From, class AIONStackableItem* To);
	void LaunchPadJump(const struct FVector& LaunchPadLocation, const struct FRotator& LaunchPadRotator);
	void Kill();
	bool IsSprinting();
	bool IsSameSquadAs(class APlayerState* OtherPlayer);
	bool IsProne();
	bool IsMapOpen();
	bool IsInWater();
	bool IsInspectingWeapon();
	bool IsInspectingSkin();
	bool IsInInvincibilityVolume();
	bool IsInFirstPersonView();
	bool IsHoldingMovementKeys();
	bool IsFreeLooking();
	bool IsFiring();
	bool IsDowned();
	bool IsDead();
	void InventoryStateChanged(bool bOpen);
	class AIONItem* HasItem(class UClass* ItemClass, bool* OutReturn);
	bool HasBattleRoyaleStarted();
	void GiveItem(const struct FString& ClassName);
	EWeaponTask GetWeaponTask();
	float GetViewYaw();
	float GetViewPitch();
	EItemStatusIndicator GetStatusIndicatorForItem(class AIONItem* Item);
	ECharacterStance GetStance();
	TArray<class AIONItem*> GetProximityItems(float MaxRange);
	class APlayerState* GetKillerPlayer();
	class AIONItem* GetInteractionItem();
	float GetHealth();
	float GetFreeLookAmmount();
	float GetDamageDealthByPlayer(class APlayerState* Player);
	class AIONWeapon* GetCurrentWeapon();
	class APlayerCameraManager* GetCameraManager();
	float GetAimRatio();
	bool GetAiming();
	int FindEmptySlotForItem(class AIONItem* Item);
	void EventBP_LandingTrigger();
	void EventBP_LandedOnTree(const struct FTransform& TreeTransform);
	void EventBP_DropInModeStarted();
	void EventBP_DropInModeEnded();
	void EquipWearable(class AIONWearable* Wearable);
	void EquipWeapon(class AIONWeapon* Weapon, bool bForce);
	bool EquipSlot(unsigned char Slot, EInventoryType InventoryType);
	void EquipPreviousWeapon();
	void EquipKnife();
	void EquipCosmeticItem(class AIONItem* Item);
	void DropStackable(class AIONStackableItem* Item, int Amount);
	void DropItem(class AIONItem* Item, bool bShuffle, bool bForce);
	void DropAmmoPack(EFirearmType AmmoType, int Amount);
	void ChangeShieldEffects(bool bNewActive);
	void ChangeDropInMode(bool bEnter);
	bool CanFreeLook();
	void BP_SimulatedHit(const struct FName& BoneName, float DamageImpulse, const struct FVector& ShotFromDirection);
	void BP_ProceduralFireAnimation();
	void AttachAttachmentToFirearm(class AIONAttachment* Attachment, class AIONFirearm* Firearm);
	void ApplyDropsuitSkin(class UIONDropSkin* NewDropsuitSkin);
	void ApplyArmorSkin(class UIONArmorSkin* NewArmorSkin);
};


// Class IONBranch.IONCharacterAnimInstance
// 0x0030 (0x0400 - 0x03D0)
class UIONCharacterAnimInstance : public UAnimInstance
{
public:
	struct FVector                                     RelativeVelocity;                                         // 0x03D0(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              ViewPitch;                                                // 0x03DC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	ECharacterStance                                   Stance;                                                   // 0x03E0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	TEnumAsByte<EMovementMode>                         MovementMode;                                             // 0x03E1(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	TEnumAsByte<EMovementMode>                         PrevMovementMode;                                         // 0x03E2(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x1];                                       // 0x03E3(0x0001) MISSED OFFSET
	float                                              SprintRatio;                                              // 0x03E4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              SprintInterpSpeed;                                        // 0x03E8(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwimSpeedBlend;                                           // 0x03EC(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                CrouchCount;                                              // 0x03F0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0xC];                                       // 0x03F4(0x000C) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCharacterAnimInstance");
		return ptr;
	}


	float GetMaxWalkSpeedCrouched();
	float GetMaxWalkSpeed();
	float GetMaxSwimSpeed();
};


// Class IONBranch.IONCharacterAnimInstanceFP
// 0x00F0 (0x04F0 - 0x0400)
class UIONCharacterAnimInstanceFP : public UIONCharacterAnimInstance
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0400(0x0008) MISSED OFFSET
	float                                              SwayInterpSpeed;                                          // 0x0408(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              MaxSwayAngle;                                             // 0x040C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SwayInterpSpeedAimed;                                     // 0x0410(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              MaxSwayAngleAimed;                                        // 0x0414(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	struct FVector                                     ArmsOffset;                                               // 0x0418(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FRotator                                    ArmsRotation;                                             // 0x0424(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     CameraOffset;                                             // 0x0430(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FRotator                                    CameraRotation;                                           // 0x043C(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FRotator                                    WorldRotation;                                            // 0x0448(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	float                                              AimRatio;                                                 // 0x0454(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              WalkRatio;                                                // 0x0458(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bMapOpen;                                                 // 0x045C(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bAvoidingObstacle;                                        // 0x045D(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bInspectingWeapon;                                        // 0x045E(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bIsFreeLooking;                                           // 0x045F(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              CachedViewPitch;                                          // 0x0460(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              FreeLookOffsetX;                                          // 0x0464(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              FreeLookOffsetY;                                          // 0x0468(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              FreeLookOffsetZ;                                          // 0x046C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bDoADSCurve;                                              // 0x0470(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0471(0x0003) MISSED OFFSET
	struct FVector2D                                   AimInAimOutExp;                                           // 0x0474(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSArmOffsetXCurve;                                       // 0x047C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSArmsRotationYawCurve;                                  // 0x0480(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSArmsRotationRollCurve;                                 // 0x0484(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SwayOffset;                                               // 0x0488(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SwayOffsetADS;                                            // 0x0494(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     FreeLookOffset;                                           // 0x04A0(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	float                                              DownMovementAlpha;                                        // 0x04AC(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              ReloadingMovementAlpha;                                   // 0x04B0(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x04B4(0x0004) MISSED OFFSET
	struct FInspectSkinAnimation                       CurrentInspectSkinAnimation;                              // 0x04B8(0x0028) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	bool                                               bInspectingSkin;                                          // 0x04E0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bShouldCancelInspectingSkin;                              // 0x04E1(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0xE];                                       // 0x04E2(0x000E) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCharacterAnimInstanceFP");
		return ptr;
	}

};


// Class IONBranch.IONCharacterMovementComponent
// 0x0050 (0x0820 - 0x07D0)
class UIONCharacterMovementComponent : public UCharacterMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x4];                                       // 0x07D0(0x0004) MISSED OFFSET
	float                                              MaxStrafeSpeed;                                           // 0x07D4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxSprintSpeed;                                           // 0x07D8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxProneSpeed;                                            // 0x07DC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxDownedSpeed;                                           // 0x07E0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxWalkSpeedInWater;                                      // 0x07E4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              JumpSpeedPenalty;                                         // 0x07E8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              JumpPenaltyDuration;                                      // 0x07EC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bEnableSkyDiving;                                         // 0x07F0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x07F1(0x0003) MISSED OFFSET
	float                                              SkyDivingMaxFriction;                                     // 0x07F4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SkyDivingBrakingFriction;                                 // 0x07F8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SkyDivingMinLateralSpeed;                                 // 0x07FC(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SkyDivingMaxLateralSpeed;                                 // 0x0800(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SkyDivingMinLateralAccel;                                 // 0x0804(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SkyDivingMaxLateralAccel;                                 // 0x0808(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x080C(0x0004) MISSED OFFSET
	double                                             LastFindFloorTime;                                        // 0x0810(0x0008) (ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData03[0x8];                                       // 0x0818(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCharacterMovementComponent");
		return ptr;
	}


	bool IsSprinting();
	bool IsProne();
	float GetSprintRatio();
};


// Class IONBranch.IONConfig
// 0x00E0 (0x0110 - 0x0030)
class UIONConfig : public UObject
{
public:
	unsigned char                                      UnknownData00[0xE0];                                      // 0x0030(0x00E0) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONConfig");
		return ptr;
	}

};


// Class IONBranch.IONConnectionOverlayUserWidget
// 0x0010 (0x0230 - 0x0220)
class UIONConnectionOverlayUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UTextBlock*                                  StatusTextBlock;                                          // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0228(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONConnectionOverlayUserWidget");
		return ptr;
	}


	float GetOverlayOpacity();
};


// Class IONBranch.IONGameViewportClient
// 0x0010 (0x0630 - 0x0620)
class UIONGameViewportClient : public UGameViewportClient
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0620(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameViewportClient");
		return ptr;
	}

};


// Class IONBranch.IONConsole
// 0x0030 (0x0168 - 0x0138)
class UIONConsole : public UConsole
{
public:
	bool                                               bHasErrorMessage;                                         // 0x0138(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0139(0x0007) MISSED OFFSET
	struct FString                                     ErrorMessage;                                             // 0x0140(0x0010) (ZeroConstructor)
	float                                              LastErrorMessageTime;                                     // 0x0150(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x14];                                      // 0x0154(0x0014) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONConsole");
		return ptr;
	}

};


// Class IONBranch.IONCrateScreenWidget
// 0x0018 (0x0230 - 0x0218)
class UIONCrateScreenWidget : public UIONCustomizationBaseWidget
{
public:
	struct FIONSteamInventoryItem                      SteamCrate;                                               // 0x0218(0x0010) (BlueprintVisible)
	class AActor*                                      CrateObject;                                              // 0x0228(0x0008) (BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCrateScreenWidget");
		return ptr;
	}


	void ScrapItemForCredits();
	class UIONSteamItem* OpenCrate(class UIONSteamItem* CrateToOpen);
	TArray<class UIONSteamItem*> GetCrateContent(class UIONSteamItem* Crate);
	void BPEvent_OpenCrate(class UIONSteamItem* GainedItem);
	void AddItemToInventory();
};


// Class IONBranch.IONCrateWidget
// 0x0008 (0x0220 - 0x0218)
class UIONCrateWidget : public UIONCustomizationBaseWidget
{
public:
	class UIONSteamCrate*                              Crate;                                                    // 0x0218(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCrateWidget");
		return ptr;
	}


	TArray<class UIONSteamItem*> GetCrateContent();
};


// Class IONBranch.IONCustomizationInventoryWidget
// 0x0008 (0x0220 - 0x0218)
class UIONCustomizationInventoryWidget : public UIONCustomizationBaseWidget
{
public:
	bool                                               bMarkedDirty;                                             // 0x0218(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0219(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCustomizationInventoryWidget");
		return ptr;
	}


	void RefreshInventory();
	void BPEvent_RefreshInventory();
};


// Class IONBranch.IONCustomizationPawn
// 0x0090 (0x0410 - 0x0380)
class AIONCustomizationPawn : public APawn
{
public:
	class AIONCharacter*                               PlayerDisplayChar;                                        // 0x0380(0x0008) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	TArray<class AIONItem*>                            DisplayCharWearables;                                     // 0x0388(0x0010) (BlueprintVisible, ZeroConstructor)
	class USceneComponent*                             CharacterRoot;                                            // 0x0398(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        BasePlatform;                                             // 0x03A0(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x03A8(0x0008) MISSED OFFSET
	struct FTransform                                  SpawnTransformInspectWeapon;                              // 0x03B0(0x0030) (Edit, DisableEditOnInstance, IsPlainOldData)
	class AIONWeapon*                                  CurrentInspectWeapon;                                     // 0x03E0(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class USkeletalMeshComponent*                      InspectWearableMesh;                                      // 0x03E8(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMesh*                               DefaultCharacterMesh;                                     // 0x03F0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequence*                               DefaultIdle;                                              // 0x03F8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequence*                               RifleIdle;                                                // 0x0400(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequence*                               PistolIdle;                                               // 0x0408(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONCustomizationPawn");
		return ptr;
	}


	void SkinsDebug(int Mode);
	void SetupDisplayCharacter();
	void SetArmorVisiblity(bool bNewVisibility, class UClass* WearableToHide);
	void PreviewSkinOnChar(class UIONSteamItem* NewItem);
	void InspectWearable(const struct FIONSteamInventoryItem& Wearable);
	void InspectWeaponSkin(class UIONSteamItem* Item);
	void InspectCrate(const struct FIONSteamInventoryItem& Crate);
	void DeleteInspectWeapon();
	void BPEvent_SkinsDebug(int Mode);
	void BPEvent_PreviewWeaponSkin(class AIONWeapon* Weapon);
	void BPEvent_InspectWeaponSkin(class UIONWeaponSkin* WeaponSkin);
	void BPEvent_InspectDropsuit(class UIONDropSkin* DropSkin);
	void BPEvent_InspectCrate(const struct FIONSteamInventoryItem& Crate);
	void BPEvent_InspectArmor(class UIONArmorSkin* ArmorSkin);
	void BPEvent_ApplyCurrentLoadout();
	void ApplyCurrentLoadout();
};


// Class IONBranch.IONDataSingleton
// 0x1CB8 (0x1CE8 - 0x0030)
class UIONDataSingleton : public UObject
{
public:
	float                                              DamageScoreEventMultiplier;                               // 0x0030(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0034(0x0004) MISSED OFFSET
	TMap<EScoreEvent, struct FScoreEventConfig>        ScoreEventConfigs;                                        // 0x0038(0x0050) (Edit, ZeroConstructor, DisableEditOnInstance)
	struct FPhysicalSurfaceProperties                  SurfaceTypeProperties[0x3F];                              // 0x0088(0x0070) (Edit, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FItemSpawnRateData>                  ItemSpawnRates;                                           // 0x1C18(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FRank>                               Ranks;                                                    // 0x1C28(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                MinWinPoints;                                             // 0x1C38(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxWinPoints;                                             // 0x1C3C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MinLossPoints;                                            // 0x1C40(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxLossPoints;                                            // 0x1C44(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              FirstRankLoseRatio;                                       // 0x1C48(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              LastRankLoseRatio;                                        // 0x1C4C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              FullMatchRatioExponent;                                   // 0x1C50(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x1C54(0x0004) MISSED OFFSET
	class USoundClass*                                 MasterSoundClass;                                         // 0x1C58(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USoundClass*                                 MusicSoundClass;                                          // 0x1C60(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USoundClass*                                 EffectsSoundClass;                                        // 0x1C68(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USoundClass*                                 UISoundClass;                                             // 0x1C70(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UDataTable*                                  InputDataTable;                                           // 0x1C78(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FLinearColor>                        ChatColors;                                               // 0x1C80(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	TMap<struct FString, struct FLinearColor>          ReservedPlayerColors;                                     // 0x1C90(0x0050) (Edit, ZeroConstructor, DisableEditOnInstance)
	unsigned char                                      UnknownData02[0x8];                                       // 0x1CE0(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDataSingleton");
		return ptr;
	}


	struct FRank GetRankForScore(int Score);
	struct FRank GetNextRankForScore(int Score);
	struct FScoreEventConfig GetConfigForScoreEvent(EScoreEvent ScoreEvent);
};


// Class IONBranch.IONHUD
// 0x0010 (0x0420 - 0x0410)
class AIONHUD : public AHUD
{
public:
	class UClass*                                      MainWidgetClass;                                          // 0x0410(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UUserWidget*                                 MainWidget;                                               // 0x0418(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONHUD");
		return ptr;
	}

};


// Class IONBranch.IONDeathHUD
// 0x0000 (0x0420 - 0x0420)
class AIONDeathHUD : public AIONHUD
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDeathHUD");
		return ptr;
	}

};


// Class IONBranch.IONDevHuntRuleSet
// 0x0000 (0x03B0 - 0x03B0)
class AIONDevHuntRuleSet : public AIONGameRuleSet
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDevHuntRuleSet");
		return ptr;
	}

};


// Class IONBranch.IONEvent
// 0x0000 (0x0320 - 0x0320)
class AIONEvent : public AActor
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONEvent");
		return ptr;
	}


	bool RemoveFromActiveEvents();
	class AIONEventManager* GetEventManager();
};


// Class IONBranch.IONDrone
// 0x0140 (0x0460 - 0x0320)
class AIONDrone : public AIONEvent
{
public:
	class USkeletalMeshComponent*                      Mesh;                                                     // 0x0320(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UParticleSystemComponent*                    ParticleSystem;                                           // 0x0328(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UAkComponent*                                AudioComponent;                                           // 0x0330(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x34];                                      // 0x0338(0x0034) MISSED OFFSET
	float                                              DistanceToSpeedDamping;                                   // 0x036C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxSpeed;                                                 // 0x0370(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DespawnTime;                                              // 0x0374(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FSmokeEffectConfig>                  SmokeEffectConfig;                                        // 0x0378(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	class UParticleSystem*                             DeathExplosion;                                           // 0x0388(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UParticleSystem*                             CrashedExplosion;                                         // 0x0390(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FLootTableEntry>                     WeaponsTable;                                             // 0x0398(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FLootTableEntry>                     WearablesTable;                                           // 0x03A8(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	class UClass*                                      LargeNanoMedClass;                                        // 0x03B8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      ItemSpawnClass;                                           // 0x03C0(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimMontage*                                HitMontage;                                               // 0x03C8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVectorSpringState                          VelocitySpringState;                                      // 0x03D0(0x0018)
	class UAkAudioEvent*                               DroneAmbientStart;                                        // 0x03E8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               DroneAmbientStop;                                         // 0x03F0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               DroneDeath;                                               // 0x03F8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               DroneHitGround;                                           // 0x0400(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               DroneSparkStart;                                          // 0x0408(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               DroneSparkStop;                                           // 0x0410(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0418(0x0004) MISSED OFFSET
	float                                              MovementInterpSpeed;                                      // 0x041C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DeadMovementInterpSpeed;                                  // 0x0420(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x0424(0x0004) MISSED OFFSET
	struct FTargetData                                 TargetData;                                               // 0x0428(0x0010) (BlueprintVisible, Net)
	float                                              Health;                                                   // 0x0438(0x0004) (Edit, BlueprintVisible, Net, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     CurrentVelocity;                                          // 0x043C(0x000C) (BlueprintVisible, Net, IsPlainOldData)
	bool                                               bAtLocation;                                              // 0x0448(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x3];                                       // 0x0449(0x0003) MISSED OFFSET
	float                                              TimeAtLocation;                                           // 0x044C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bCrashed;                                                 // 0x0450(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData04[0xF];                                       // 0x0451(0x000F) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDrone");
		return ptr;
	}


	bool SpawnItemFromLootTable(TArray<struct FLootTableEntry>* LootTable);
	void OnRep_Health();
	void OnRep_Crashed();
	bool IsDead();
	float GetMaxHealth();
	float FindNewTargetTime();
	struct FVector FindNewTargetLocation();
};


// Class IONBranch.IONDroneAnimInstance
// 0x0040 (0x0410 - 0x03D0)
class UIONDroneAnimInstance : public UAnimInstance
{
public:
	unsigned char                                      UnknownData00[0x4];                                       // 0x03D0(0x0004) MISSED OFFSET
	struct FVector                                     Velocity;                                                 // 0x03D4(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	float                                              RotationInterpSpeed;                                      // 0x03E0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsDead;                                                  // 0x03E4(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x03E5(0x0003) MISSED OFFSET
	struct FRotator                                    Rotation;                                                 // 0x03E8(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	struct FRotator                                    TargetRotation;                                           // 0x03F4(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	class AIONDrone*                                   OwningDrone;                                              // 0x0400(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x8];                                       // 0x0408(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDroneAnimInstance");
		return ptr;
	}

};


// Class IONBranch.IONEventSpawnBehavior
// 0x0008 (0x0038 - 0x0030)
class UIONEventSpawnBehavior : public UObject
{
public:
	int                                                EventsSpawned;                                            // 0x0030(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0034(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONEventSpawnBehavior");
		return ptr;
	}


	class AIONEvent* SpawnEvent();
	class UWorld* GetWorld();
	class AIONEventManager* GetEventManager();
	bool CanSpawn();
};


// Class IONBranch.IONDroneSpawnBehavior
// 0x0018 (0x0050 - 0x0038)
class UIONDroneSpawnBehavior : public UIONEventSpawnBehavior
{
public:
	int                                                MaxDronesSpawned;                                         // 0x0038(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DroneToPlayerRatio;                                       // 0x003C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      DroneClass;                                               // 0x0040(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MinTimeBetweenSpawns;                                     // 0x0048(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              LastSpawnTime;                                            // 0x004C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDroneSpawnBehavior");
		return ptr;
	}


	class AIONEvent* SpawnEvent();
	bool CanSpawn();
};


// Class IONBranch.IONDropSkin
// 0x0018 (0x0110 - 0x00F8)
class UIONDropSkin : public UIONSteamItem
{
public:
	TArray<class UMeshComponent*>                      ExtraMeshes;                                              // 0x00F8(0x0010) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, DisableEditOnInstance)
	class UMaterialInterface*                          DropSuitMaterial;                                         // 0x0108(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONDropSkin");
		return ptr;
	}

};


// Class IONBranch.IONEventManager
// 0x0080 (0x03A0 - 0x0320)
class AIONEventManager : public AInfo
{
public:
	TArray<class UClass*>                              SpawnBehaviors;                                           // 0x0320(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	TMap<class UClass*, class UIONEventSpawnBehavior*> BehaviorMap;                                              // 0x0330(0x0050) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	float                                              UpdateRate;                                               // 0x0380(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0384(0x0004) MISSED OFFSET
	TArray<class AIONEvent*>                           ActiveEvents;                                             // 0x0388(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0398(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONEventManager");
		return ptr;
	}


	void Update();
	bool GetActiveEventsOfClass(class UClass* Class, TArray<class AIONEvent*>* OutEvents);
};


// Class IONBranch.IONExtendedMagazine
// 0x0000 (0x0500 - 0x0500)
class AIONExtendedMagazine : public AIONAttachment
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONExtendedMagazine");
		return ptr;
	}

};


// Class IONBranch.IONModifierWearable
// 0x0000 (0x0460 - 0x0460)
class AIONModifierWearable : public AIONWearable
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONModifierWearable");
		return ptr;
	}

};


// Class IONBranch.IONExtraCapacityUnit
// 0x0000 (0x0460 - 0x0460)
class AIONExtraCapacityUnit : public AIONModifierWearable
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONExtraCapacityUnit");
		return ptr;
	}

};


// Class IONBranch.IONWeapon
// 0x0408 (0x07D0 - 0x03C8)
class AIONWeapon : public AIONItem
{
public:
	class USkeletalMeshComponent*                      Mesh;                                                     // 0x03C8(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      FirstPersonMesh;                                          // 0x03D0(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	float                                              HeadshotMultiplier;                                       // 0x03D8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x03DC(0x0004) MISSED OFFSET
	class UIONWeaponSkin*                              CurrentWeaponSkin;                                        // 0x03E0(0x0008) (BlueprintVisible, Net, ZeroConstructor, IsPlainOldData)
	struct FScriptMulticastDelegate                    OnWeaponSkinChanged;                                      // 0x03E8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	TArray<class UMaterialInterface*>                  DefaultMaterials;                                         // 0x03F8(0x0010) (BlueprintVisible, ZeroConstructor)
	int                                                MainMaterialIdx;                                          // 0x0408(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x040C(0x0004) MISSED OFFSET
	struct FTransform                                  SpawnTransformSkinRender;                                 // 0x0410(0x0030) (Edit, BlueprintVisible, EditConst, IsPlainOldData)
	struct FTransform                                  SpawnTransformMainMenu;                                   // 0x0440(0x0030) (Edit, BlueprintVisible, EditConst, IsPlainOldData)
	bool                                               bUseCustomEquipSocket;                                    // 0x0470(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0471(0x0007) MISSED OFFSET
	struct FName                                       CustomEquipSocket;                                        // 0x0478(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MovementSpeedPenalty;                                     // 0x0480(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ObstacleTraceDist;                                        // 0x0484(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      ASIAnimBlueprint;                                         // 0x0488(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SprintPitch;                                              // 0x0490(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SprintZOffset;                                            // 0x0494(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              RecoilStrength3P;                                         // 0x0498(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x049C(0x0004) MISSED OFFSET
	struct FTransform                                  SprintOffsetTransform;                                    // 0x04A0(0x0030) (Edit, IsPlainOldData)
	struct FTransform                                  WalkOffsetTransform;                                      // 0x04D0(0x0030) (Edit, IsPlainOldData)
	struct FIdleAnimation                              IdleAnimation;                                            // 0x0500(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FIdleAnimation                              IdleAltAnimation;                                         // 0x0518(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FIdleAnimation                              InspectIdleAnimation;                                     // 0x0530(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FIdleAnimation                              AvoidingObstacleAnimation;                                // 0x0548(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FWeaponLoopAnimation                        SprintAnimation;                                          // 0x0560(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	bool                                               bCustomSprintAnimation;                                   // 0x0570(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x7];                                       // 0x0571(0x0007) MISSED OFFSET
	struct FWeaponAnimation                            EquipAnimation;                                           // 0x0578(0x0070) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FWeaponAnimation                            UnequipAnimation;                                         // 0x05E8(0x0070) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FInspectSkinAnimation                       InspectSkinAnimation;                                     // 0x0658(0x0028) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData05[0x8];                                       // 0x0680(0x0008) MISSED OFFSET
	struct FWeaponTask                                 Task_Equip;                                               // 0x0688(0x0068) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Unequip;                                             // 0x06F0(0x0068) (Edit, DisableEditOnInstance)
	float                                              ArmorDamageFactor;                                        // 0x0758(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ShieldDamageFactor;                                       // 0x075C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ArmorPenetration;                                         // 0x0760(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ShieldPenetration;                                        // 0x0764(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x5C];                                      // 0x0768(0x005C) MISSED OFFSET
	float                                              WeaponTaskUpdateFrequency;                                // 0x07C4(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData07[0x8];                                       // 0x07C8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWeapon");
		return ptr;
	}


	void UpdateWeaponSkinMeshes(TArray<class UMeshComponent*> MeshList);
	void UpdateWeaponSkin();
	void ServerStartTask(EWeaponTask TaskID);
	void RestoreDefaultSkin();
	void OnWeaponSkinChanged__DelegateSignature(class UIONWeaponSkin* WeaponSkin);
	void MulticastConfirmTask(EWeaponTask TaskID);
	bool IsEquipped();
	class USkeletalMeshComponent* GetViewedMesh();
	struct FVector GetPickupLocation();
	struct FVector GetLeftHandSocketLocation();
	class USkeletalMeshComponent* GetFPMesh();
	class UIONWeaponSkin* GetCurrentWeaponSkin();
	bool GetAlternateIdle();
	class USkeletalMeshComponent* Get3PMesh();
	void ClientStartTask(EWeaponTask TaskID);
	void ClientConfirmTask(EWeaponTask TaskID);
	void BPEvent_OnPickedUp(class AIONCharacter* Character);
	void BPEvent_ApplyWeaponSkinMaterial();
	void ApplyWeaponSkin(class UIONWeaponSkin* NewWeaponSkin, bool bForce);
};


// Class IONBranch.IONFirearm
// 0x09A0 (0x1170 - 0x07D0)
class AIONFirearm : public AIONWeapon
{
public:
	struct FWeaponTask                                 Task_Fire;                                                // 0x07D0(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_DryFire;                                             // 0x0838(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Reload;                                              // 0x08A0(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_ReloadChamber;                                       // 0x0908(0x0068) (Edit, DisableEditOnInstance)
	bool                                               bUseCustomFullReload;                                     // 0x0970(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0971(0x0007) MISSED OFFSET
	struct FWeaponTask                                 Task_ReloadEmpty;                                         // 0x0978(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_SwitchFireMode;                                      // 0x09E0(0x0068) (Edit, DisableEditOnInstance)
	class UAnimSequence*                               ADSSequence;                                              // 0x0A48(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x1];                                       // 0x0A50(0x0001) MISSED OFFSET
	bool                                               bRoundChambered;                                          // 0x0A51(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      Magazine;                                                 // 0x0A52(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x5];                                       // 0x0A53(0x0005) MISSED OFFSET
	struct FFirearmAttributes                          BaseAttributes;                                           // 0x0A58(0x00F0) (Edit, DisableEditOnInstance)
	struct FRecoilAttributes                           RecoilAttributes;                                         // 0x0B48(0x0050) (Edit, DisableEditOnInstance)
	struct FFirearmAttributes                          WeaponAttributes;                                         // 0x0B98(0x00F0) (BlueprintVisible, BlueprintReadOnly)
	EFirearmSoundType                                  FirearmSoundType;                                         // 0x0C88(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EFirearmType                                       FirearmType;                                              // 0x0C89(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x6];                                       // 0x0C8A(0x0006) MISSED OFFSET
	class UClass*                                      ProjectileClass;                                          // 0x0C90(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      NumProjectiles;                                           // 0x0C98(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x7];                                       // 0x0C99(0x0007) MISSED OFFSET
	class UStaticMesh*                                 MagazineMesh;                                             // 0x0CA0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMesh*                                 ExtendedMagazineMesh;                                     // 0x0CA8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	EFireMode                                          FireMode;                                                 // 0x0CB0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      bHasSemiMode : 1;                                         // 0x0CB1(0x0001) (Edit, DisableEditOnInstance)
	unsigned char                                      bHasBurstMode : 1;                                        // 0x0CB1(0x0001) (Edit, DisableEditOnInstance)
	unsigned char                                      bHasAutoMode : 1;                                         // 0x0CB1(0x0001) (Edit, DisableEditOnInstance)
	unsigned char                                      UnknownData05[0x6];                                       // 0x0CB2(0x0006) MISSED OFFSET
	struct FBarrelProperties                           DefaultBarrel;                                            // 0x0CB8(0x0048) (Edit, DisableEditOnInstance, IsPlainOldData)
	struct FBarrelProperties                           Silencer;                                                 // 0x0D00(0x0048) (Edit, DisableEditOnInstance, IsPlainOldData)
	struct FBarrelProperties                           Compensator;                                              // 0x0D48(0x0048) (Edit, DisableEditOnInstance, IsPlainOldData)
	struct FBarrelProperties                           HeavyBarrel;                                              // 0x0D90(0x0048) (Edit, DisableEditOnInstance, IsPlainOldData)
	class AIONSight*                                   Sight;                                                    // 0x0DD8(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	class AIONAttachment*                              Siderail;                                                 // 0x0DE0(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	class AIONAttachment*                              UnderBarrelRail;                                          // 0x0DE8(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	class AIONBarrel*                                  Barrel;                                                   // 0x0DF0(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	class AIONExtendedMagazine*                        ExtendedMagazine;                                         // 0x0DF8(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	bool                                               bUseSightAdapter;                                         // 0x0E00(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x7];                                       // 0x0E01(0x0007) MISSED OFFSET
	TArray<class UClass*>                              SightsToUseOnAdapter;                                     // 0x0E08(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	class UStaticMesh*                                 SightAdapterMesh;                                         // 0x0E18(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMesh*                                 SightAdapterFirstPersonMesh;                              // 0x0E20(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUseSeperateIronSights;                                   // 0x0E28(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData07[0x7];                                       // 0x0E29(0x0007) MISSED OFFSET
	class USkeletalMesh*                               FrontIronSightMesh;                                       // 0x0E30(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               FrontIronSightFirstPersonMesh;                            // 0x0E38(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               BackIronSightMesh;                                        // 0x0E40(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               BackIronSightFirstPersonMesh;                             // 0x0E48(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class USkeletalMesh*                               ADSMesh;                                                  // 0x0E50(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMeshComponent*                        SightAdapterMeshComponent;                                // 0x0E58(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        SightAdapterFirstPersonMeshComponent;                     // 0x0E60(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      FrontIronSightMeshComponent;                              // 0x0E68(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      FrontIronSightFirstPersonMeshComponent;                   // 0x0E70(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      BackIronSightMeshComponent;                               // 0x0E78(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      BackIronSightFirstPersonMeshComponent;                    // 0x0E80(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UAnimSequenceBase*                           FireEmptySequence1P;                                      // 0x0E88(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           AimedFireSequence1P;                                      // 0x0E90(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FFireAnimation                              FireAnimation;                                            // 0x0E98(0x0080) (Edit, DisableEditOnInstance)
	struct FWeaponAnimation                            DryFireAnimation;                                         // 0x0F18(0x0070) (Edit, DisableEditOnInstance)
	struct FWeaponAnimation                            ReloadAnimation;                                          // 0x0F88(0x0070) (Edit, DisableEditOnInstance)
	struct FWeaponAnimation                            ReloadEmptyAnimation;                                     // 0x0FF8(0x0070) (Edit, DisableEditOnInstance)
	struct FWeaponAnimation                            ReloadChamberAnimation;                                   // 0x1068(0x0070) (Edit, DisableEditOnInstance)
	class UAkAudioEvent*                               DryFireSound;                                             // 0x10D8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               SwitchFireModeSound;                                      // 0x10E0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               LastBulletEvent;                                          // 0x10E8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               ShellCasingEvent;                                         // 0x10F0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkComponent*                                FireAudioComponent;                                       // 0x10F8(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData08[0x8];                                       // 0x1100(0x0008) MISSED OFFSET
	class UStaticMeshComponent*                        AttachmentComponents[0x5];                                // 0x1108(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        FirstPersonAttachmentComponents[0x5];                     // 0x1130(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              CrosshairDistanceMultiplier;                              // 0x1158(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData09[0x14];                                      // 0x115C(0x0014) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONFirearm");
		return ptr;
	}


	bool ShouldUseSightAdapter();
	void ServerNotifyHit(const struct FHitResult& Hit, const struct FVector& WorldLocation);
	void OnRep_UnderBarrelRail(class AIONAttachment* OldAttachment);
	void OnRep_Sight(class AIONSight* OldSight);
	void OnRep_Siderail(class AIONAttachment* OldAttachment);
	void OnRep_ExtendedMagazine(class AIONExtendedMagazine* OldMag);
	void OnRep_Barrel(class AIONBarrel* OldBarrel);
	void MulticastImpactNotify(const struct FHitResult& Hit, const struct FVector& WorldLocation);
	bool HasAttachmentSlot(TEnumAsByte<EAttachmentSlot> Slot);
	class UTexture2D* GetRoundIcon();
	bool GetRoundChambered();
	float GetReloadTime();
	float GetRangeStat();
	int GetMagazineCapacity();
	int GetMagazine();
	float GetFireRateStat();
	float GetDamageStat();
	struct FName GetAttachmentSocketName(TEnumAsByte<EAttachmentSlot> Slot);
	TArray<class AIONAttachment*> GetAttachments();
	void GetAttachmentComponent(TEnumAsByte<EAttachmentSlot> Slot, class UStaticMeshComponent** OutMesh, class UStaticMeshComponent** OutMeshFP);
	class AIONAttachment* GetAttachment(TEnumAsByte<EAttachmentSlot> Slot);
	struct FText GetAmmoTypeText();
	class UTexture2D* GetAmmoBoxIcon();
	struct FString GetAmmoBoxClassPath();
	class UClass* GetAmmoBoxClass();
	float GetAccuracyStat();
	float GetAccuracy();
};


// Class IONBranch.IONFriendListUserWidget
// 0x0018 (0x0238 - 0x0220)
class UIONFriendListUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UVerticalBox*                                InvitesVerticalBox;                                       // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  InviteCountTextBlock;                                     // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UClass*                                      InviteUserWidgetClass;                                    // 0x0230(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONFriendListUserWidget");
		return ptr;
	}


	void OnUpdateFriendList();
};


// Class IONBranch.IONFriendUserWidget
// 0x0000 (0x0210 - 0x0210)
class UIONFriendUserWidget : public UUserWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONFriendUserWidget");
		return ptr;
	}

};


// Class IONBranch.IONGameInstance
// 0x0690 (0x0790 - 0x0100)
class UIONGameInstance : public UGameInstance
{
public:
	class UObject*                                     SomeLongLivedPackage;                                     // 0x0100(0x0008) (ZeroConstructor, IsPlainOldData)
	class UObject*                                     SomeImportantClass;                                       // 0x0108(0x0008) (ZeroConstructor, IsPlainOldData)
	class UObject*                                     SomeImportantObject;                                      // 0x0110(0x0008) (ZeroConstructor, IsPlainOldData)
	TArray<class UObject*>                             AllObjects;                                               // 0x0118(0x0010) (ZeroConstructor)
	TArray<struct FString>                             PrequeueLevels;                                           // 0x0128(0x0010) (ZeroConstructor, Config)
	struct FString                                     GameSparksID;                                             // 0x0138(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     GameSparksAuthToken;                                      // 0x0148(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     GameSparksDisplayName;                                    // 0x0158(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	class UGameSparksObject*                           GameSparks;                                               // 0x0168(0x0008) (ZeroConstructor, IsPlainOldData)
	class UGSMessageListeners*                         GameSparksMessageListener;                                // 0x0170(0x0008) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0xF0];                                      // 0x0178(0x00F0) MISSED OFFSET
	class UClass*                                      LoadingScreenClass;                                       // 0x0268(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FString                                     ProjectDetails;                                           // 0x0270(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     ProjectVersion;                                           // 0x0280(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     LastSessionServerName;                                    // 0x0290(0x0010) (BlueprintVisible, ZeroConstructor)
	unsigned char                                      UnknownData01[0x50];                                      // 0x02A0(0x0050) UNKNOWN PROPERTY: SetProperty IONBranch.IONGameInstance.InteractiveActors
	bool                                               bAllowQAAccess;                                           // 0x02F0(0x0001) (ZeroConstructor, Config, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x02F1(0x0007) MISSED OFFSET
	TArray<class UObject*>                             AsyncLoadedObjects;                                       // 0x02F8(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData03[0x78];                                      // 0x0308(0x0078) MISSED OFFSET
	class USocketIOClientComponent*                    SIOClientComponent;                                       // 0x0380(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	TMap<int, class UIONSteamItem*>                    SkinMap;                                                  // 0x0388(0x0050) (ZeroConstructor)
	class UIONSteamInventory*                          SteamInventory;                                           // 0x03D8(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	TArray<struct FIONSteamInventoryItem>              SteamInventoryItems;                                      // 0x03E0(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst)
	unsigned char                                      UnknownData04[0x8];                                       // 0x03F0(0x0008) MISSED OFFSET
	struct FString                                     UserCurrency;                                             // 0x03F8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	unsigned char                                      UnknownData05[0x2A8];                                     // 0x0408(0x02A8) MISSED OFFSET
	class UObject*                                     NotificationImage;                                        // 0x06B0(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FSlateBrush                                 NotificationBrush;                                        // 0x06B8(0x0078)
	unsigned char                                      UnknownData06[0x48];                                      // 0x0730(0x0048) MISSED OFFSET
	class UIONMatchmaking*                             Matchmaking;                                              // 0x0778(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData07[0x10];                                      // 0x0780(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameInstance");
		return ptr;
	}


	void UpdateSteamInventoryItems();
	void UpdatePlayerStats(class AIONPlayerState* PlayerState, class AIONPlayerState* Winner);
	void UpdateLoadingScreen(const struct FString& NewMessage);
	void OnLevelActorRemoved(class AActor* InActor);
	void OnGameSparksAvailable(bool bAvailable);
	class UIONMatchmaking* GetMatchmaking();
	class UIONSteamItem* GetItemBySteamDefID(int ID);
	void EndLoadingScreen();
	void DownloadPlayerStats(class AIONPlayerState* PlayerState);
	void BeginLoadingScreen(const struct FString& MapName);
};


// Class IONBranch.IONGameMode
// 0x0808 (0x0C10 - 0x0408)
class AIONGameMode : public AGameMode
{
public:
	unsigned char                                      UnknownData00[0x50];                                      // 0x0408(0x0050) MISSED OFFSET
	TArray<struct FString>                             BannedPlayerIDs;                                          // 0x0458(0x0010) (ZeroConstructor, Config)
	struct FIONCircleConfig                            CircleConfig;                                             // 0x0468(0x0054) (Config)
	struct FCircleScale                                PlayerScaleSpawnHeight;                                   // 0x04BC(0x0008) (Config)
	unsigned char                                      UnknownData01[0x4];                                       // 0x04C4(0x0004) MISSED OFFSET
	TArray<int>                                        PlacementScoreTable;                                      // 0x04C8(0x0010) (ZeroConstructor, Config)
	class UClass*                                      DefaultBotControllerClass;                                // 0x04D8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	TArray<class UClass*>                              DefaultBotPickups;                                        // 0x04E0(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	TArray<class UClass*>                              RuleSetClassesForBR;                                      // 0x04F0(0x0010) (Edit, BlueprintVisible, ZeroConstructor, NoClear)
	TArray<class UClass*>                              RuleSetClassesForGunGame;                                 // 0x0500(0x0010) (Edit, BlueprintVisible, ZeroConstructor, NoClear)
	TArray<class UClass*>                              RuleSetClassesForAll;                                     // 0x0510(0x0010) (Edit, BlueprintVisible, ZeroConstructor, NoClear)
	TArray<class AIONGameRuleSet*>                     Rulesets;                                                 // 0x0520(0x0010) (ZeroConstructor, Transient)
	class UClass*                                      EventManagerClass;                                        // 0x0530(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class AIONEventManager*                            EventManager;                                             // 0x0538(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	TArray<struct FString>                             DebugDevelopers;                                          // 0x0540(0x0010) (ZeroConstructor, Config)
	bool                                               bAutomaticQueueDisabled;                                  // 0x0550(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Config, IsPlainOldData)
	bool                                               bShouldDestroyDeadPawns;                                  // 0x0551(0x0001) (Edit, ZeroConstructor, Config, IsPlainOldData)
	unsigned char                                      UnknownData02[0x6];                                       // 0x0552(0x0006) MISSED OFFSET
	class UClass*                                      BeamDropClass;                                            // 0x0558(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FName                                       OverrideSafeZoneGroup;                                    // 0x0560(0x0008) (ZeroConstructor, Config, IsPlainOldData)
	float                                              SpawnRadiusAllowanceFactor;                               // 0x0568(0x0004) (ZeroConstructor, Config, IsPlainOldData)
	float                                              MinSpawnRadius;                                           // 0x056C(0x0004) (ZeroConstructor, Config, IsPlainOldData)
	float                                              FirstStageFleeTimeBonus;                                  // 0x0570(0x0004) (ZeroConstructor, Config, IsPlainOldData)
	float                                              FirstStageShrinkTimeBonus;                                // 0x0574(0x0004) (ZeroConstructor, Config, IsPlainOldData)
	TArray<struct FStageRulesModifier>                 StageRulesModifiers;                                      // 0x0578(0x0010) (ZeroConstructor, Config)
	float                                              DropInSpawnHeight;                                        // 0x0588(0x0004) (ZeroConstructor, Config, IsPlainOldData)
	struct FBeamDropEventConfig                        BeamDropEventConfig;                                      // 0x058C(0x0010) (Edit, Config, DisableEditOnInstance)
	float                                              ScoreRelevanceMaxDistanceSquared;                         // 0x059C(0x0004) (Edit, ZeroConstructor, Config, DisableEditOnInstance, IsPlainOldData)
	float                                              ScoreRelevanceDistanceScaleFactor;                        // 0x05A0(0x0004) (Edit, ZeroConstructor, Config, DisableEditOnInstance, IsPlainOldData)
	float                                              SurvivalBaseCredits;                                      // 0x05A4(0x0004) (Edit, ZeroConstructor, Config, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0xB8];                                      // 0x05A8(0x00B8) MISSED OFFSET
	int                                                MinPlayerLimitBeforeMatchStart;                           // 0x0660(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData04[0x4];                                       // 0x0664(0x0004) MISSED OFFSET
	TArray<class AIONSquadState*>                      AliveSquadStates;                                         // 0x0668(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData05[0x4D8];                                     // 0x0678(0x04D8) MISSED OFFSET
	bool                                               bUseDropIn;                                               // 0x0B50(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x67];                                      // 0x0B51(0x0067) MISSED OFFSET
	bool                                               bMatchCompleted;                                          // 0x0BB8(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData07[0x7];                                       // 0x0BB9(0x0007) MISSED OFFSET
	TArray<class AIONSquad*>                           Squads;                                                   // 0x0BC0(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient)
	TArray<class AIONSquad*>                           IndexedSquads;                                            // 0x0BD0(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	class UClass*                                      SquadClass;                                               // 0x0BE0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, NoClear, IsPlainOldData)
	struct FPlasmaStage                                FinalPlasmaStage;                                         // 0x0BE8(0x001C)
	unsigned char                                      UnknownData08[0xC];                                       // 0x0C04(0x000C) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameMode");
		return ptr;
	}


	void UnbanPlayer(const struct FString& PlayerId);
	void ToggleQueueTimer();
	void TogglePlasmaField();
	void StartBattleRoyale();
	void SpawnBeamDrop();
	void SetMaxPlayers(int MaxPlayers);
	void SetDamageFactor(float Value);
	void SendStatsToCameramen();
	void RestartMatch();
	void RemoveAdmin(const struct FString& PlayerId);
	void KillPlayer(const struct FString& PlayerName);
	void KickPlayer(const struct FString& PlayerName);
	bool HasBattleRoyaleStarted();
	void GivePoints(int Num);
	void ForceSkinDrops();
	void ForceRestartMatch();
	void BanPlayer(const struct FString& PlayerName);
	void AddAdmin(const struct FString& PlayerId);
};


// Class IONBranch.IONGameOverlayWidget
// 0x0008 (0x0218 - 0x0210)
class UIONGameOverlayWidget : public UUserWidget
{
public:
	bool                                               bChatHidden;                                              // 0x0210(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bScreenshotMode;                                          // 0x0211(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bDisableGameDebugText;                                    // 0x0212(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x5];                                       // 0x0213(0x0005) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameOverlayWidget");
		return ptr;
	}


	void StartMessage(const struct FName& Type);
	void SetMapTooltipVisibility(bool bInVisible);
	void SetChatVisibility(bool bInVisible);
	void OnScoreEventReceived(EScoreEvent Type, int Points);
	void OnReceivedChatMessage(class AIONPlayerState* From, const struct FString& Message, const struct FName& Type);
	void OnPlayerDowned(const struct FHitInfo& LastHitInfo);
	void OnPlayerDied(const struct FHitInfo& LastHitInfo);
	void OnMicrophoneStateChanged(bool bEnabled);
	void OnKillConfirmed(const struct FHitInfo& LastHitInfo);
	void OnDownConfirmed(const struct FHitInfo& LastHitInfo);
	void DisplaySubtitle(const struct FText& SubtitleText);
};


// Class IONBranch.GSGetLatestMatchRequest
// 0x0010 (0x0040 - 0x0030)
class UGSGetLatestMatchRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetLatestMatchRequest");
		return ptr;
	}


	class UGSGetLatestMatchRequest* STATIC_GetLatestMatch();
};


// Class IONBranch.GSGetGameConfigRequest
// 0x0010 (0x0040 - 0x0030)
class UGSGetGameConfigRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetGameConfigRequest");
		return ptr;
	}


	class UGSGetGameConfigRequest* STATIC_GetGameConfig();
};


// Class IONBranch.GSGetPlayerProfileRequest
// 0x0020 (0x0050 - 0x0030)
class UGSGetPlayerProfileRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData00[0x10];                                      // 0x0040(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetPlayerProfileRequest");
		return ptr;
	}


	class UGSGetPlayerProfileRequest* STATIC_GetPlayerProfile(const struct FString& PlayerId);
};


// Class IONBranch.GSGetPlayerStatsRequest
// 0x0030 (0x0060 - 0x0030)
class UGSGetPlayerStatsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData00[0x20];                                      // 0x0040(0x0020) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetPlayerStatsRequest");
		return ptr;
	}


	class UGSGetPlayerStatsRequest* STATIC_GetPlayerStats(const struct FString& PlayerId, const struct FString& GameMode);
};


// Class IONBranch.GSGetToasterRequest
// 0x0010 (0x0040 - 0x0030)
class UGSGetToasterRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetToasterRequest");
		return ptr;
	}


	class UGSGetToasterRequest* STATIC_GetToaster();
};


// Class IONBranch.GSGetMenuNewsRequest
// 0x0010 (0x0040 - 0x0030)
class UGSGetMenuNewsRequest : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnResponse;                                               // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.GSGetMenuNewsRequest");
		return ptr;
	}


	class UGSGetMenuNewsRequest* STATIC_GetMenuNews();
};


// Class IONBranch.IONGameService
// 0x0000 (0x0030 - 0x0030)
class UIONGameService : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameService");
		return ptr;
	}


	struct FPlayerMatchHistory STATIC_DebugGetRandomMatchHistory();
};


// Class IONBranch.IONGameUserSettings
// 0x0020 (0x0130 - 0x0110)
class UIONGameUserSettings : public UGameUserSettings
{
public:
	float                                              FOV;                                                      // 0x0110(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              MasterVolume;                                             // 0x0114(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              MusicVolume;                                              // 0x0118(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              EffectsVolume;                                            // 0x011C(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              UIVolume;                                                 // 0x0120(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              InputVolume;                                              // 0x0124(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              OutputVolume;                                             // 0x0128(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)
	float                                              MotionBlur;                                               // 0x012C(0x0004) (BlueprintVisible, ZeroConstructor, Config, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGameUserSettings");
		return ptr;
	}


	void SetStringConfigVariable(const struct FName& VariableName, const struct FString& Value);
	void SetIntConfigVariable(const struct FName& VariableName, int Value);
	void SetFloatConfigVariable(const struct FName& VariableName, float Value);
	struct FString GetStringConfigVariable(const struct FName& VariableName);
	int GetIntConfigVariable(const struct FName& VariableName);
	float GetFloatConfigVariable(const struct FName& VariableName);
};


// Class IONBranch.IONGrenadeWeapon
// 0x0300 (0x0AD0 - 0x07D0)
class AIONGrenadeWeapon : public AIONWeapon
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x07D0(0x0008) MISSED OFFSET
	class UAkComponent*                                CookAudioComponent;                                       // 0x07D8(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	struct FWeaponTask                                 Task_Cook;                                                // 0x07E0(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Hold;                                                // 0x0848(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Throw;                                               // 0x08B0(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_TossCook;                                            // 0x0918(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_TossHold;                                            // 0x0980(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_TossThrow;                                           // 0x09E8(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Suicide;                                             // 0x0A50(0x0068) (Edit, DisableEditOnInstance)
	class UClass*                                      ProjectileClass;                                          // 0x0AB8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               CookSound;                                                // 0x0AC0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0AC8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGrenadeWeapon");
		return ptr;
	}

};


// Class IONBranch.IONGrip
// 0x0020 (0x0520 - 0x0500)
class AIONGrip : public AIONAttachment
{
public:
	class UStaticMesh*                                 GripMesh;                                                 // 0x0500(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              VerticalRecoil;                                           // 0x0508(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              HorizontalRecoil;                                         // 0x050C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwaySpeed;                                                // 0x0510(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwayRadius;                                               // 0x0514(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimTime;                                                  // 0x0518(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x051C(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGrip");
		return ptr;
	}

};


// Class IONBranch.IONGunGameRuleset
// 0x0010 (0x03C0 - 0x03B0)
class AIONGunGameRuleset : public AIONGameRuleSet
{
public:
	TArray<struct FGunGameStage>                       GunGameStages;                                            // 0x03B0(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONGunGameRuleset");
		return ptr;
	}

};


// Class IONBranch.IONOnlineUserWidget
// 0x0000 (0x0210 - 0x0210)
class UIONOnlineUserWidget : public UUserWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONOnlineUserWidget");
		return ptr;
	}


	void Connected(const struct FString& UserId, const struct FString& AuthToken);
};


// Class IONBranch.IONHomescreenUserWidget
// 0x0010 (0x0220 - 0x0210)
class UIONHomescreenUserWidget : public UIONOnlineUserWidget
{
public:
	class UButton*                                     HighlightsButton;                                         // 0x0210(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0218(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONHomescreenUserWidget");
		return ptr;
	}


	void HighlightsButtonPressed();
	void CheckIfHighlightsShouldBeVisible();
};


// Class IONBranch.IONInputBindingWidget
// 0x0008 (0x0138 - 0x0130)
class UIONInputBindingWidget : public UHorizontalBox
{
public:
	struct FName                                       ActionName;                                               // 0x0130(0x0008) (Edit, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInputBindingWidget");
		return ptr;
	}

};


// Class IONBranch.IONInputFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class UIONInputFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInputFunctionLibrary");
		return ptr;
	}


	void STATIC_SetInvertAxis(const struct FName& AxisName, bool bInvert);
	void STATIC_SaveMappings();
	void STATIC_RemoveMappings(const struct FName& Interaction, const struct FKey& Key, bool bSave);
	TArray<struct FName> STATIC_RebindKey(const struct FInputMapping& InputMapping, const struct FKey& Key, EInputKeyType KeyType);
	bool STATIC_IsKeyEventActionPressed(const struct FKeyEvent& KeyEvent, const struct FName& ActionName);
	bool STATIC_IsInputMappingInConflict(const struct FInputMapping& InputMapping);
	bool STATIC_GetInvertAxis(const struct FName& AxisName);
	void STATIC_GetInputMappings(TArray<struct FInputCategory>* OutCategories);
	struct FText STATIC_GetInputMappingDisplayName(const struct FInputMapping& InputMapping);
	struct FInputMapping STATIC_GetAxisMapping(const struct FName& AxisName, const struct FName& ActionName, bool bNegativeAxis);
	struct FInputMapping STATIC_GetActionMapping(const struct FName& MappingName, const struct FName& ActionName);
	void STATIC_ClearKeyBinding(const struct FInputMapping& InputMapping, EInputKeyType KeyType);
	void STATIC_AddMapping(const struct FName& Interaction, bool bAxis, bool bNegative, const struct FKey& Key, bool bSave);
};


// Class IONBranch.IONInputSettings
// 0x0010 (0x0040 - 0x0030)
class UIONInputSettings : public UObject
{
public:
	TArray<struct FInputKeyPreferences>                KeyPreferences;                                           // 0x0030(0x0010) (Edit, ZeroConstructor, Config)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInputSettings");
		return ptr;
	}

};


// Class IONBranch.IONInventoryComponent
// 0x0190 (0x0288 - 0x00F8)
class UIONInventoryComponent : public UActorComponent
{
public:
	TArray<int>                                        Ammo;                                                     // 0x00F8(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            Attachments;                                              // 0x0108(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            OldAttachments;                                           // 0x0118(0x0010) (ZeroConstructor)
	TArray<class AIONItem*>                            Firearms;                                                 // 0x0128(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            OldFirearms;                                              // 0x0138(0x0010) (ZeroConstructor)
	TArray<class AIONItem*>                            Boosters;                                                 // 0x0148(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            OldBoosters;                                              // 0x0158(0x0010) (ZeroConstructor)
	TArray<class AIONItem*>                            Grenades;                                                 // 0x0168(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            OldGrenades;                                              // 0x0178(0x0010) (ZeroConstructor)
	TArray<class AIONItem*>                            Wearables;                                                // 0x0188(0x0010) (Net, ZeroConstructor)
	TArray<class AIONItem*>                            OldWearables;                                             // 0x0198(0x0010) (ZeroConstructor)
	class AIONItem*                                    Knife;                                                    // 0x01A8(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	struct FInventoryAttributes                        BaseAttributes;                                           // 0x01B0(0x0068) (Edit, DisableEditOnInstance)
	struct FScriptMulticastDelegate                    OnInventoryChanged;                                       // 0x0218(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnAmmoChanged;                                            // 0x0228(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnAttachmentsChanged;                                     // 0x0238(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnFirearmsChanged;                                        // 0x0248(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnBoostersChanged;                                        // 0x0258(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnGrenadesChanged;                                        // 0x0268(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnWearablesChanged;                                       // 0x0278(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInventoryComponent");
		return ptr;
	}


	void ServerMoveItemToSlot(class AIONItem* NewItem, int Slot, class AItemSpawn* ItemSpawn, unsigned char ItemIndex);
	bool RemoveItem(class AIONItem* ItemToRemove, bool bCallOnRep);
	bool RemoveAmmo(EFirearmType AmmoType, int Ammount);
	void OnWearablesChanged__DelegateSignature();
	void OnRep_Wearables();
	void OnRep_Knife();
	void OnRep_Grenades();
	void OnRep_Firearms();
	void OnRep_Boosters();
	void OnRep_Attachments();
	void OnRep_Ammo(TArray<int> OldAmmo);
	void OnInventoryChanged__DelegateSignature();
	void OnGrenadesChanged__DelegateSignature();
	void OnFirearmsChanged__DelegateSignature();
	void OnBoostersChanged__DelegateSignature();
	void OnAttachmentsChanged__DelegateSignature();
	void OnAmmoChanged__DelegateSignature();
	void MoveItemToSlot(class AIONItem* NewItem, int Slot);
	bool HasItemOfClass(class UClass* ItemClass);
	class AIONWeapon* GetKnife();
	struct FInventoryAttributes GetInventoryAttributes();
	int GetIndexForNewItem(EInventoryType ArrayType, class AIONItem* Item);
	class AIONCharacter* GetCharacterChecked();
	class AIONCharacter* GetCharacter();
	TArray<class AIONItem*> GetArrayFromType(EInventoryType ArrayType);
	int GetAmmoOfType(EFirearmType FirearmType);
	void GetAllItems(TArray<class AIONItem*>* OutItems);
	class AIONItem* FindItemOfClass(class UClass* ItemClass);
	bool CanAddItem(class AIONItem* Item);
	bool AddItem(class AIONItem* NewItem);
};


// Class IONBranch.IONInventoryWidget
// 0x0010 (0x0220 - 0x0210)
class UIONInventoryWidget : public UUserWidget
{
public:
	class AIONItem*                                    DetailsItem;                                              // 0x0210(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class UCanvasRenderTarget2D*                       WeaponStatsRenderTarget;                                  // 0x0218(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInventoryWidget");
		return ptr;
	}


	void ShowInventoryBar(bool bInventoryFull);
	void ShowInventory();
	bool ShouldAssignItemToSlot();
	void SetDetailsItem(class AIONItem* Item);
	void OnUpdateWeaponStatsRender(class UCanvas* Canvas, int SizeX, int SizeY);
	void HideInventory();
};


// Class IONBranch.IONInvincibilityVolume
// 0x0000 (0x0358 - 0x0358)
class AIONInvincibilityVolume : public AVolume
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInvincibilityVolume");
		return ptr;
	}

};


// Class IONBranch.IONInviteReceivedUserWidget
// 0x0028 (0x0248 - 0x0220)
class UIONInviteReceivedUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0220(0x0010) MISSED OFFSET
	class UTextBlock*                                  InviteUserTextBlock;                                      // 0x0230(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     OpenFriendsListButton;                                    // 0x0238(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     DismissButton;                                            // 0x0240(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInviteReceivedUserWidget");
		return ptr;
	}


	void OpenFriendsListButtonClicked();
	void Dismiss();
};


// Class IONBranch.IONInviteUserWidget
// 0x0020 (0x0230 - 0x0210)
class UIONInviteUserWidget : public UUserWidget
{
public:
	class UTextBlock*                                  PersonaNameTextBlock;                                     // 0x0210(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          AcceptButton;                                             // 0x0218(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          RejectButton;                                             // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      AvatarImage;                                              // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONInviteUserWidget");
		return ptr;
	}

};


// Class IONBranch.IONItemIcon
// 0x0018 (0x0230 - 0x0218)
class UIONItemIcon : public UIONCustomizationBaseWidget
{
public:
	struct FIONSteamInventoryItem                      CurrentItem;                                              // 0x0218(0x0010) (Edit, BlueprintVisible, EditConst)
	bool                                               bIsInteractive;                                           // 0x0228(0x0001) (Edit, BlueprintVisible, ZeroConstructor, EditConst, IsPlainOldData)
	bool                                               bIsEquipped;                                              // 0x0229(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x022A(0x0006) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONItemIcon");
		return ptr;
	}


	void SetInteractive(bool bNewInteractive);
	void SetEquipped(bool bNewEquipped);
	bool IsInLoadout();
	void BPEvent_EquipChanged(bool bEquipped);
};


// Class IONBranch.IONItemWidget
// 0x0008 (0x0138 - 0x0130)
class UIONItemWidget : public UOverlay
{
public:
	class AIONItem*                                    CurrentItem;                                              // 0x0130(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONItemWidget");
		return ptr;
	}


	void UpdateWeaponSkin(class UIONWeaponSkin* WeaponSkin);
	void UpdateItem(class AIONItem* Item);
};


// Class IONBranch.IONKnife
// 0x0170 (0x0940 - 0x07D0)
class AIONKnife : public AIONWeapon
{
public:
	unsigned char                                      UnknownData00[0x60];                                      // 0x07D0(0x0060) MISSED OFFSET
	struct FWeaponTask                                 Task_Throw;                                               // 0x0830(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_Recharge;                                            // 0x0898(0x0068) (Edit, DisableEditOnInstance)
	float                                              Damage;                                                   // 0x0900(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxRange;                                                 // 0x0904(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BackstabDamageMultiplier;                                 // 0x0908(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BackstabMaxAngle;                                         // 0x090C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0910(0x0008) MISSED OFFSET
	TArray<class UAnimSequenceBase*>                   SlashSequences;                                           // 0x0918(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	class UClass*                                      CameraShakeClass;                                         // 0x0928(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      ProjectileClass;                                          // 0x0930(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	EKnifeMode                                         CurrentKnifeMode;                                         // 0x0938(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0939(0x0003) MISSED OFFSET
	float                                              ReloadDuration;                                           // 0x093C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONKnife");
		return ptr;
	}


	bool IsRecharging();
	void EventBP_HitSurfaceType(TEnumAsByte<EPhysicalSurface> SurfaceTypeHit);
};


// Class IONBranch.IONLaserSight
// 0x0028 (0x0528 - 0x0500)
class AIONLaserSight : public AIONAttachment
{
public:
	class UStaticMesh*                                 LaserSightMesh;                                           // 0x0500(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UMaterialInterface*                          DecalMaterial;                                            // 0x0508(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMesh*                                 BeamMesh;                                                 // 0x0510(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxLaserDist;                                             // 0x0518(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BaseDecalSize;                                            // 0x051C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxDecalSize;                                             // 0x0520(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadHip;                                                // 0x0524(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONLaserSight");
		return ptr;
	}

};


// Class IONBranch.IONLoadoutWidget
// 0x0000 (0x0218 - 0x0218)
class UIONLoadoutWidget : public UIONCustomizationBaseWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONLoadoutWidget");
		return ptr;
	}


	class UTexture2D* GetWearableLoadoutIcon(class UClass* Wearable);
	TArray<class UIONArmorSkin*> GetWearableLoadout();
	TArray<class UIONWeaponSkin*> GetWeaponLoadout();
	struct FIONSteamInventoryItem GetLoadoutForWeapon(class UClass* Weapon);
	class UTexture2D* GetDropsuitLoadoutIcon();
};


// Class IONBranch.IONLobbyMemberUserWidget
// 0x0078 (0x0298 - 0x0220)
class UIONLobbyMemberUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UButton*                                     MemberButton;                                             // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     ActionButton;                                             // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      KickImage;                                                // 0x0230(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      LeavePartyImage;                                          // 0x0238(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	struct FMatchmakingPlayer                          Player;                                                   // 0x0240(0x0048)
	class UImage*                                      AvatarImage;                                              // 0x0288(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0290(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONLobbyMemberUserWidget");
		return ptr;
	}


	void OnMemberButtonClicked();
	void LeaveParty();
	void KickPlayer();
};


// Class IONBranch.IONLobbyUserWidget
// 0x0010 (0x0230 - 0x0220)
class UIONLobbyUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UHorizontalBox*                              PlayersHorizontalBox;                                     // 0x0220(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UClass*                                      LobbyMemberUserWidgetClass;                               // 0x0228(0x0008) (Edit, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONLobbyUserWidget");
		return ptr;
	}


	void PartyPlayerLeave(const struct FMatchmakingPlayer& Player);
	void PartyPlayerJoin(const struct FMatchmakingPlayer& Player);
};


// Class IONBranch.IONMainMenuUserWidget
// 0x0038 (0x0258 - 0x0220)
class UIONMainMenuUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UTextBlock*                                  VersionTextBlock;                                         // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONLobbyUserWidget*                         LobbyUserWidget;                                          // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONInviteReceivedUserWidget*                InviteReceivedUserWidget;                                 // 0x0230(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONFriendListUserWidget*                    FriendListUserWidget;                                     // 0x0238(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONMatchmakingUserWidget*                   MatchmakingUserWidget;                                    // 0x0240(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      DisclaimerImage;                                          // 0x0248(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONConnectionOverlayUserWidget*             ConnectionOverlay;                                        // 0x0250(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMainMenuUserWidget");
		return ptr;
	}


	class AIONBasePlayerController* GetIONPC();
};


// Class IONBranch.IONMatchmaking
// 0x08A0 (0x08D0 - 0x0030)
class UIONMatchmaking : public UObject
{
public:
	unsigned char                                      UnknownData00[0x710];                                     // 0x0030(0x0710) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnMatchmakingPartyUpdated;                                // 0x0740(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData01[0x70];                                      // 0x0750(0x0070) MISSED OFFSET
	struct FScriptMulticastDelegate                    OnMatchmakingPartyMemberArmorUpdated;                     // 0x07C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData02[0x100];                                     // 0x07D0(0x0100) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMatchmaking");
		return ptr;
	}


	void UpdatePartyMembersArmor(const struct FString& SteamID, const struct FString& ArmorData);
	bool SetRegion(EIONPartyRegion region);
	bool SetPartyType(EIONPartyTypes Type);
	void SetAutomatch(bool bAutomatch);
	void MatchmakingPartyUpdated__DelegateSignature(const struct FMatchmakingParty& Party);
	void MatchmakingPartyMemberArmorUpdated__DelegateSignature(const struct FString& SteamID, const struct FString& ArmorData);
	bool IsRegionAllowed(EIONPartyRegion region);
	bool IsPartyTypeAllowed(EIONPartyTypes PartyType);
	bool IsPartyLeader();
	bool IsLeader();
	bool IsAutomatchingAllowed();
	void Invite(const struct FString& SteamID);
	int GetPartySize();
	int GetMaxPlayersForCurrentMode();
	void DisableRegion(EIONPartyRegion region);
};


// Class IONBranch.IONMatchmakingTestGameInstance
// 0x0010 (0x0110 - 0x0100)
class UIONMatchmakingTestGameInstance : public UGameInstance
{
public:
	class UClass*                                      MenuWidgetClass;                                          // 0x0100(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0108(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMatchmakingTestGameInstance");
		return ptr;
	}


	void InitializeUI();
};


// Class IONBranch.IONMatchmakingTestMenuWidget
// 0x01C8 (0x03D8 - 0x0210)
class UIONMatchmakingTestMenuWidget : public UUserWidget
{
public:
	struct FSlateBrush                                 NormalButtonStyle;                                        // 0x0210(0x0078) (Edit, DisableEditOnInstance)
	struct FSlateBrush                                 PressedButtonStyle;                                       // 0x0288(0x0078) (Edit, DisableEditOnInstance)
	class UTextBlock*                                  ConnectionStatusTextBlock;                                // 0x0300(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  SocketIDTextBlock;                                        // 0x0308(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  PlayerIDTextBlock;                                        // 0x0310(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UEditableTextBox*                            InvitePlayerTextBox;                                      // 0x0318(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     InviteButton;                                             // 0x0320(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     SoloButton;                                               // 0x0328(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     DuoButton;                                                // 0x0330(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     SquadButton;                                              // 0x0338(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     NAButton;                                                 // 0x0340(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     EUButton;                                                 // 0x0348(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     OCEButton;                                                // 0x0350(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     SEAButton;                                                // 0x0358(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     SAButton;                                                 // 0x0360(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          LeavePartyButton;                                         // 0x0368(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  LeavePartyLabelTextBlock;                                 // 0x0370(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UVerticalBox*                                InvitesVerticalBox;                                       // 0x0378(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UVerticalBox*                                PartyVerticalBox;                                         // 0x0380(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UGridPanel*                                  InviteSettingsGridPanel;                                  // 0x0388(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UVerticalBox*                                InvitesContainerVerticalBox;                              // 0x0390(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UVerticalBox*                                RightBoundaryVerticalBox;                                 // 0x0398(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UVerticalBox*                                PartyActionsVerticalBox;                                  // 0x03A0(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  PendingInvitesLabelTextBlock;                             // 0x03A8(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCheckBox*                                   AutomatchCheckBox;                                        // 0x03B0(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UClass*                                      InviteUserWidgetClass;                                    // 0x03B8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      PlayerUserWidgetClass;                                    // 0x03C0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x10];                                      // 0x03C8(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMatchmakingTestMenuWidget");
		return ptr;
	}


	void ToggleAutomatch(bool Checked);
	void OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party);
	void OnMatchmakingInviteReceived(TArray<struct FMatchmakingInvite> Invites);
	void LeaveParty();
	void InvitePlayer();
	void ChangeToSquadParty();
	void ChangeToSoloParty();
	void ChangeToSEARegion();
	void ChangeToSARegion();
	void ChangeToOCERegion();
	void ChangeToNARegion();
	void ChangeToEURegion();
	void ChangeToDuoParty();
};


// Class IONBranch.IONMatchmakingUserWidget
// 0x0040 (0x0260 - 0x0220)
class UIONMatchmakingUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UIONServerSelectionUserWidget*               ServerSelectionUserWidget;                                // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UButton*                                     ReadyButton;                                              // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCircularThrobber*                           ReadySpinner;                                             // 0x0230(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      ReadyButtonImage;                                         // 0x0238(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTexture2D*                                  ReadyImageTexture;                                        // 0x0240(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  PlayMatchImageTexture;                                    // 0x0248(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x10];                                      // 0x0250(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMatchmakingUserWidget");
		return ptr;
	}


	void UpdateReadyImage(const struct FMatchmakingParty& Party);
	void ToggleReadiness();
	void ShowServerSelection();
	void HideServerSelection();
};


// Class IONBranch.IONMatchReportWidget
// 0x0110 (0x0320 - 0x0210)
class UIONMatchReportWidget : public UUserWidget
{
public:
	float                                              ProgressFillSpeed;                                        // 0x0210(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0xF4];                                      // 0x0214(0x00F4) MISSED OFFSET
	struct FAnimatedLevelProgress                      LevelProgress;                                            // 0x0308(0x0014) (BlueprintVisible, BlueprintReadOnly)
	unsigned char                                      UnknownData01[0x4];                                       // 0x031C(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMatchReportWidget");
		return ptr;
	}


	void StartLevelProgress();
	void SetMatchHistory(const struct FPlayerMatchHistory& InMatchHistory);
	void OnScoreBucketFilled(const struct FScoreEvent& ScoreEvent);
	void OnMatchHistorySet();
	void OnLevelIncreased();
	struct FPlayerMatchHistory GetMatchHistory();
};


// Class IONBranch.IONMenuLevelScriptActor
// 0x00A8 (0x03D0 - 0x0328)
class AIONMenuLevelScriptActor : public ALevelScriptActor
{
public:
	unsigned char                                      UnknownData00[0x68];                                      // 0x0328(0x0068) MISSED OFFSET
	class UClass*                                      PartySpawnActorClass;                                     // 0x0390(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	TArray<class AIONPartySpawnActor*>                 PartySpawns;                                              // 0x0398(0x0010) (Edit, ZeroConstructor)
	class UClass*                                      MainMenuWidgetClass;                                      // 0x03A8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x03B0(0x0008) MISSED OFFSET
	class USoundWave*                                  MainMenuMusic;                                            // 0x03B8(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioBank*                                MainSoundBank;                                            // 0x03C0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x8];                                       // 0x03C8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMenuLevelScriptActor");
		return ptr;
	}


	void OnPlayerArmorUpdated(const struct FString& SteamID, const struct FString& UpdatedArmor);
	void OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party);
};


// Class IONBranch.IONMTInviteUserWidget
// 0x0018 (0x0228 - 0x0210)
class UIONMTInviteUserWidget : public UUserWidget
{
public:
	class UTextBlock*                                  IdentifierTextBlock;                                      // 0x0210(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          AcceptButton;                                             // 0x0218(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          RejectButton;                                             // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMTInviteUserWidget");
		return ptr;
	}

};


// Class IONBranch.IONMTLevelScriptActor
// 0x0008 (0x0330 - 0x0328)
class AIONMTLevelScriptActor : public ALevelScriptActor
{
public:
	class UIONMatchmakingTestGameInstance*             GameInstance;                                             // 0x0328(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMTLevelScriptActor");
		return ptr;
	}

};


// Class IONBranch.IONMTPlayerUserWidget
// 0x0080 (0x0290 - 0x0210)
class UIONMTPlayerUserWidget : public UUserWidget
{
public:
	unsigned char                                      UnknownData00[0x50];                                      // 0x0210(0x0050) MISSED OFFSET
	class UTextBlock*                                  IdentifierTextBlock;                                      // 0x0260(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  LeaderTextBlock;                                          // 0x0268(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          PromoteButton;                                            // 0x0270(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONBehaviorButton*                          KickButton;                                               // 0x0278(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCheckBox*                                   ReadyCheckBox;                                            // 0x0280(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextBlock*                                  ReadyTextBlock;                                           // 0x0288(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONMTPlayerUserWidget");
		return ptr;
	}


	void ToggleReadiness(bool Checked);
};


// Class IONBranch.IONNanoMed
// 0x0070 (0x0840 - 0x07D0)
class AIONNanoMed : public AIONWeapon
{
public:
	struct FWeaponTask                                 Task_Activate;                                            // 0x07D0(0x0068) (Edit, DisableEditOnInstance)
	float                                              HealAmount;                                               // 0x0838(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x083C(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONNanoMed");
		return ptr;
	}

};


// Class IONBranch.IONObserverCamHUD
// 0x0008 (0x0428 - 0x0420)
class AIONObserverCamHUD : public AIONHUD
{
public:
	int                                                SavedStreamingDistance;                                   // 0x0420(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0424(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONObserverCamHUD");
		return ptr;
	}

};


// Class IONBranch.IONPartySpawnActor
// 0x0050 (0x0370 - 0x0320)
class AIONPartySpawnActor : public AActor
{
public:
	class UClass*                                      PlayerPawnClass;                                          // 0x0320(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	struct FMatchmakingPlayer                          CurrentPlayer;                                            // 0x0328(0x0048) (BlueprintVisible)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPartySpawnActor");
		return ptr;
	}


	void PlayerUpdated(const struct FMatchmakingPlayer& Player);
	void DestroyPlayerPawn();
	void CreatePlayerPawn(const struct FMatchmakingPlayer& Player);
	void BPEvent_ArmorUpdated(const struct FString& NewArmor);
};


// Class IONBranch.IONPlayerHUD
// 0x0018 (0x0438 - 0x0420)
class AIONPlayerHUD : public AIONHUD
{
public:
	class AActor*                                      HighlightedActor;                                         // 0x0420(0x0008) (ZeroConstructor, IsPlainOldData)
	class UClass*                                      InventoryWidgetClass;                                     // 0x0428(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UIONInventoryWidget*                         InventoryWidget;                                          // 0x0430(0x0008) (BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPlayerHUD");
		return ptr;
	}

};


// Class IONBranch.IONPlayerState
// 0x00F0 (0x04C8 - 0x03D8)
class AIONPlayerState : public APlayerState
{
public:
	bool                                               bIsDeveloper;                                             // 0x03D8(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	bool                                               bIsQA;                                                    // 0x03D9(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	bool                                               bIsStreamer;                                              // 0x03DA(0x0001) (Net, ZeroConstructor, IsPlainOldData)
	bool                                               bHasSeenElimininationScreen;                              // 0x03DB(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	EIONVoiceActivity                                  VoiceActivity;                                            // 0x03DC(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	bool                                               bIsAdmin;                                                 // 0x03DD(0x0001) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      Rank;                                                     // 0x03DE(0x0001) (BlueprintVisible, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x1];                                       // 0x03DF(0x0001) MISSED OFFSET
	int                                                Kills;                                                    // 0x03E0(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	int                                                Deaths;                                                   // 0x03E4(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	struct FString                                     GameSparksID;                                             // 0x03E8(0x0010) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor)
	class APlayerState*                                KilledBy;                                                 // 0x03F8(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class AActor*                                      KillDamageCauser;                                         // 0x0400(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              DeathTime;                                                // 0x0408(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              DamageDealt;                                              // 0x040C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                HeadshotsHit;                                             // 0x0410(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                ShotsHit;                                                 // 0x0414(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                ShotsFired;                                               // 0x0418(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x041C(0x0004) MISSED OFFSET
	struct FString                                     FavouriteWeapon;                                          // 0x0420(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	float                                              TimeSurvived;                                             // 0x0430(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                MatchCredits;                                             // 0x0434(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	struct FString                                     MatchId;                                                  // 0x0438(0x0010) (ZeroConstructor)
	TMap<class AIONWeapon*, float>                     TrackedWeaponDamage;                                      // 0x0448(0x0050) (ZeroConstructor)
	class AIONSquadState*                              SquadState;                                               // 0x0498(0x0008) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x10];                                      // 0x04A0(0x0010) MISSED OFFSET
	TArray<struct FLinearColor>                        TeamColors;                                               // 0x04B0(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	class AIONCharacter*                               Character;                                                // 0x04C0(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, EditConst, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPlayerState");
		return ptr;
	}


	void SetVoiceActivity(EIONVoiceActivity NewVoiceActivity);
	void SetSquadState(class AIONSquadState* NewSquadState);
	void ServerSetGameSparksID(const struct FString& InGameSparksID);
	void OnRep_SquadState(class AIONSquadState* OldSquadState);
	bool IsStreamer();
	bool IsSameSquadAs(class APlayerState* Other);
	bool IsQA();
	bool IsDeveloper();
	bool IsAdmin();
	int GetTeamIndex();
	struct FLinearColor GetTeamColor();
	struct FLinearColor GetPlayerColor();
};


// Class IONBranch.IONPlayerWidget
// 0x0010 (0x0220 - 0x0210)
class UIONPlayerWidget : public UUserWidget
{
public:
	class UTextureRenderTarget2D*                      RenderTarget;                                             // 0x0210(0x0008) (ZeroConstructor, IsPlainOldData)
	class UMaterialInterface*                          InteractionWidgetMaterial;                                // 0x0218(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPlayerWidget");
		return ptr;
	}


	void UpdateInteractionWidgetWithText(const struct FText& InteractionText, float DeltaTime, bool bHideInteractButton);
	void UpdateInteractionWidget(const struct FInteractionOption& InteractionOption, float DeltaTime);
	void HideInteractionWidget();
	class UWidget* GetInteractionWidget();
};


// Class IONBranch.IONPostMatchStatsHUD
// 0x0000 (0x0420 - 0x0420)
class AIONPostMatchStatsHUD : public AIONHUD
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPostMatchStatsHUD");
		return ptr;
	}

};


// Class IONBranch.IONPromotionalSet
// 0x0020 (0x0058 - 0x0038)
class UIONPromotionalSet : public UDataAsset
{
public:
	struct FString                                     PromotionalName;                                          // 0x0038(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	class UTexture2D*                                  IconBackground;                                           // 0x0048(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  FooterImage;                                              // 0x0050(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONPromotionalSet");
		return ptr;
	}

};


// Class IONBranch.IONRenderPawn
// 0x0050 (0x03D0 - 0x0380)
class AIONRenderPawn : public APawn
{
public:
	class AIONBasePlayerController*                    IONPC;                                                    // 0x0380(0x0008) (ZeroConstructor, IsPlainOldData)
	class USceneCaptureComponent2D*                    RenderComponent;                                          // 0x0388(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USceneCaptureComponent2D*                    RenderComponentHDR;                                       // 0x0390(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UTextureRenderTarget2D*                      CamRenderTarget;                                          // 0x0398(0x0008) (ZeroConstructor, IsPlainOldData)
	class UTextureRenderTarget2D*                      CamRenderTargetHDR;                                       // 0x03A0(0x0008) (ZeroConstructor, IsPlainOldData)
	class AIONWeapon*                                  CurrentWeapon;                                            // 0x03A8(0x0008) (ZeroConstructor, IsPlainOldData)
	TArray<class UIONWeaponSkin*>                      SkinList;                                                 // 0x03B0(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData00[0x10];                                      // 0x03C0(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONRenderPawn");
		return ptr;
	}


	void ItCurrentSkins();
};


// Class IONBranch.IONRewardsScreen
// 0x0000 (0x0218 - 0x0218)
class UIONRewardsScreen : public UIONCustomizationBaseWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONRewardsScreen");
		return ptr;
	}


	void PurchaseItem(class UIONSteamItem* Item);
	void PurchaseCrate(class UIONSteamItem* Crate);
	float GetPriceForItem(class UIONSteamItem* Item);
	int GetCratePrice(class UIONSteamItem* Crate);
	TArray<class UIONSteamItem*> GetCrateContent(class UIONSteamItem* Crate);
	TArray<class UIONSteamCrate*> GetAllCrates();
};


// Class IONBranch.IONRulesetState
// 0x0000 (0x0320 - 0x0320)
class AIONRulesetState : public AInfo
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONRulesetState");
		return ptr;
	}

};


// Class IONBranch.IONScoreEventWidget
// 0x0078 (0x0288 - 0x0210)
class UIONScoreEventWidget : public UUserWidget
{
public:
	unsigned char                                      UnknownData00[0x78];                                      // 0x0210(0x0078) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONScoreEventWidget");
		return ptr;
	}


	void OnScoreStringUpdated(const struct FString& NewString);
	void OnAccumulatedScoreUpdated(int NewScore);
	void AddScoreEvent(EScoreEvent EventType, int Points);
};


// Class IONBranch.IONServerAuthWidget
// 0x0000 (0x0210 - 0x0210)
class UIONServerAuthWidget : public UUserWidget
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONServerAuthWidget");
		return ptr;
	}


	void SetupAuth(const struct FString& ServerName, const struct FScriptDelegate& NewAcceptedDelegate, const struct FScriptDelegate& NewCancelledDelegate);
};


// Class IONBranch.IONServerList
// 0x0078 (0x00A8 - 0x0030)
class UIONServerList : public UObject
{
public:
	unsigned char                                      UnknownData00[0x78];                                      // 0x0030(0x0078) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONServerList");
		return ptr;
	}

};


// Class IONBranch.IONServerSelectionButton
// 0x0010 (0x0220 - 0x0210)
class UIONServerSelectionButton : public UUserWidget
{
public:
	class UButton*                                     ActionButton;                                             // 0x0210(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      CheckImage;                                               // 0x0218(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONServerSelectionButton");
		return ptr;
	}

};


// Class IONBranch.IONServerSelectionUserWidget
// 0x0048 (0x0268 - 0x0220)
class UIONServerSelectionUserWidget : public UIONBaseMatchmakingUserWidget
{
public:
	class UIONServerSelectionButton*                   SoloButton;                                               // 0x0220(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   DuoButton;                                                // 0x0228(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   SquadButton;                                              // 0x0230(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   NAButton;                                                 // 0x0238(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   EUButton;                                                 // 0x0240(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   OCEButton;                                                // 0x0248(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   SEAButton;                                                // 0x0250(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UIONServerSelectionButton*                   SAButton;                                                 // 0x0258(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCheckBox*                                   AutomatchCheckBox;                                        // 0x0260(0x0008) (ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONServerSelectionUserWidget");
		return ptr;
	}


	void ToggleAutomatch(bool Checked);
	void OnMatchmakingPartyUpdated(const struct FMatchmakingParty& Party);
	void ChangeToSquadParty();
	void ChangeToSoloParty();
	void ChangeToSEARegion();
	void ChangeToSARegion();
	void ChangeToOCERegion();
	void ChangeToNARegion();
	void ChangeToEURegion();
	void ChangeToDuoParty();
};


// Class IONBranch.IONShield
// 0x0020 (0x0480 - 0x0460)
class AIONShield : public AIONWearable
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0460(0x0008) MISSED OFFSET
	TArray<struct FName>                               NotProtectedBones;                                        // 0x0468(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	class UParticleSystem*                             ImpactEffect;                                             // 0x0478(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONShield");
		return ptr;
	}

};


// Class IONBranch.IONSight
// 0x00B0 (0x05B0 - 0x0500)
class AIONSight : public AIONAttachment
{
public:
	class UStaticMesh*                                 SightMesh;                                                // 0x0500(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMesh*                                 PistolSightMesh;                                          // 0x0508(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FTransform                                  EyeAdjustment;                                            // 0x0510(0x0030) (Edit, DisableEditOnInstance, IsPlainOldData)
	float                                              Magnification;                                            // 0x0540(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              LenseRadius;                                              // 0x0544(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ViewRadiusOffset;                                         // 0x0548(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADS;                                                // 0x054C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimTime;                                                  // 0x0550(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUseBlurAroundScope;                                      // 0x0554(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUpdateReticleMaterial;                                   // 0x0555(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIgnoreSlightFOVIncrease;                                 // 0x0556(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x1];                                       // 0x0557(0x0001) MISSED OFFSET
	TMap<class UClass*, struct FSightFirearmModifiers> SightWeaponModifierOverrides;                             // 0x0558(0x0050) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	unsigned char                                      UnknownData01[0x8];                                       // 0x05A8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSight");
		return ptr;
	}


	void UpdateMaterialAimState(class AIONFirearm* Firearm, float AimRatio);
	float GetMagnificationStat();
};


// Class IONBranch.IONSignificanceManager
// 0x0028 (0x0128 - 0x0100)
class UIONSignificanceManager : public USignificanceManager
{
public:
	unsigned char                                      UnknownData00[0x28];                                      // 0x0100(0x0028) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSignificanceManager");
		return ptr;
	}

};


// Class IONBranch.IONSliderWidget
// 0x0010 (0x0220 - 0x0210)
class UIONSliderWidget : public UUserWidget
{
public:
	struct FScriptMulticastDelegate                    OnValueChanged;                                           // 0x0210(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSliderWidget");
		return ptr;
	}


	void SliderValueChanged(float InValue);
};


// Class IONBranch.IONSniperRifle
// 0x0140 (0x12B0 - 0x1170)
class AIONSniperRifle : public AIONFirearm
{
public:
	struct FWeaponTask                                 Task_ReloadSingle;                                        // 0x1170(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_ReloadEnd;                                           // 0x11D8(0x0068) (Edit, DisableEditOnInstance)
	struct FWeaponTask                                 Task_ReloadCancel;                                        // 0x1240(0x0068) (Edit, DisableEditOnInstance)
	bool                                               bFullReloadIntroLoadsBullet;                              // 0x12A8(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x12A9(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSniperRifle");
		return ptr;
	}

};


// Class IONBranch.IONSpecialEventSettings
// 0x0010 (0x0050 - 0x0040)
class UIONSpecialEventSettings : public UDeveloperSettings
{
public:
	TArray<struct FIONSpecialEvent>                    Events;                                                   // 0x0040(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Config, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSpecialEventSettings");
		return ptr;
	}

};


// Class IONBranch.IONSpectatorHUD
// 0x0000 (0x0420 - 0x0420)
class AIONSpectatorHUD : public AIONHUD
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSpectatorHUD");
		return ptr;
	}

};


// Class IONBranch.IONSpectatorInfo
// 0x0018 (0x0338 - 0x0320)
class AIONSpectatorInfo : public AActor
{
public:
	class UWidgetComponent*                            SpectatorWidget;                                          // 0x0320(0x0008) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UClass*                                      SpectatorWidgetClass;                                     // 0x0328(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class AIONCharacter*                               TrackedCharacter;                                         // 0x0330(0x0008) (ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSpectatorInfo");
		return ptr;
	}


	void SetupWidget();
	void SetTrackedCharacter(class AIONCharacter* Character);
	void OnViewModeChanged(bool bMinimal);
	class AIONCharacter* GetTrackedCharacter();
};


// Class IONBranch.IONSquad
// 0x0038 (0x0358 - 0x0320)
class AIONSquad : public AInfo
{
public:
	class UClass*                                      SquadStateClass;                                          // 0x0320(0x0008) (Edit, BlueprintVisible, ZeroConstructor, NoClear, IsPlainOldData)
	class AIONSquadState*                              SquadState;                                               // 0x0328(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, IsPlainOldData)
	class AMainPlayerController*                       Leader;                                                   // 0x0330(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	TArray<class AMainPlayerController*>               Players;                                                  // 0x0338(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	TArray<struct FString>                             SteamIDs;                                                 // 0x0348(0x0010) (ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSquad");
		return ptr;
	}

};


// Class IONBranch.IONSquadState
// 0x0080 (0x03A0 - 0x0320)
class AIONSquadState : public AInfo
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0320(0x0010) MISSED OFFSET
	class AIONSquad*                                   AuthoritySquad;                                           // 0x0330(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, IsPlainOldData)
	struct FString                                     ID;                                                       // 0x0338(0x0010) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient)
	struct FLinearColor                                SquadColor;                                               // 0x0348(0x0010) (BlueprintVisible, BlueprintReadOnly, Net, Transient, IsPlainOldData)
	int                                                Index;                                                    // 0x0358(0x0004) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x035C(0x0004) MISSED OFFSET
	struct FString                                     MatchId;                                                  // 0x0360(0x0010) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient)
	TArray<class AIONPlayerState*>                     PlayerStates;                                             // 0x0370(0x0010) (BlueprintVisible, BlueprintReadOnly, Net, ZeroConstructor, Transient)
	unsigned char                                      UnknownData02[0x10];                                      // 0x0380(0x0010) MISSED OFFSET
	int                                                MaxSize;                                                  // 0x0390(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, NoClear, IsPlainOldData)
	unsigned char                                      UnknownData03[0xC];                                       // 0x0394(0x000C) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSquadState");
		return ptr;
	}


	void RemovePlayerState(class AIONPlayerState* PlayerState, bool bKeepGameServiceID);
	bool IsSquadStanding();
	bool IsSquadAlive();
	int GetSquadAliveCount();
	void AddPlayerState(class AIONPlayerState* PlayerState);
};


// Class IONBranch.IONStandardScoringRuleSet
// 0x0000 (0x03B0 - 0x03B0)
class AIONStandardScoringRuleSet : public AIONGameRuleSet
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONStandardScoringRuleSet");
		return ptr;
	}

};


// Class IONBranch.IONStatics
// 0x0000 (0x0030 - 0x0030)
class UIONStatics : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONStatics");
		return ptr;
	}


	void STATIC_UpdateAudioEnvironment(class UAkComponent* AudioComponent);
	void STATIC_SetSoundClassVolume(class USoundClass* SoundClass, float Volume);
	void STATIC_SetSoundClassPitch(class USoundClass* SoundClass, float Pitch);
	void STATIC_SetAntiCheatRunning(bool bIsRunning);
	struct FText STATIC_SecondsToText(float Seconds);
	struct FText STATIC_RotationToDirectionText(const struct FRotator& Rotation);
	struct FHitResult STATIC_RebaseHitToLocalOrigin(class UObject* WorldContextObject, const struct FHitResult& Hit);
	void STATIC_OpenWebURL(const struct FString& URL);
	void STATIC_OpenShootingRange(class UObject* WorldContextObject);
	void STATIC_OpenMainMenu(class UObject* WorldContextObject);
	bool STATIC_IsNoSteam();
	bool STATIC_IsAntiCheatRunning();
	bool STATIC_IsAntiCheatEnabled();
	class AMainWorldSettings* STATIC_GetWorldSettings(class UObject* WorldContextObject);
	struct FGeometry STATIC_GetWidgetChildGeometry(class UWidget* ParentWidget, class UWidget* ChildWidget, const struct FGeometry& MyGeometry);
	struct FVector2D STATIC_GetRelativeMapLocationForPosition(class UObject* WorldContextObject, const struct FVector& Position);
	struct FString STATIC_GetPlayerStateInfo(class APlayerState* PlayerState);
	class APawn* STATIC_GetPlayerPawn(class APlayerState* Player);
	struct FString STATIC_GetPlayerControllerInfo(class APlayerController* PlayerController);
	EPlasmaState STATIC_GetPlasmaStage(class UObject* WorldContextObject, struct FVector* OutPlasmaLocation, float* OutPlasmaRadius, struct FVector* OutSafeZoneLocation, float* OutSafeZoneRadius);
	bool STATIC_GetPlasmaLocation(class UObject* WorldContextObject, struct FVector* OutPlasmaLocation, float* OutPlasmaRadius);
	struct FString STATIC_GetOrdinalWithPlace(unsigned char Place);
	struct FString STATIC_GetOrdinalForPlace(unsigned char Place);
	EOnlinePlatformType STATIC_GetOnlinePlatformType();
	EInventoryType STATIC_GetInventoryTypeFromClass(class UClass* ItemClass);
	struct FText STATIC_GetGridLocationText(class UObject* WorldContextObject, const struct FIntPoint& Coordinates);
	struct FIntPoint STATIC_GetGridLocation(class UObject* WorldContextObject, const struct FVector& Location);
	struct FString STATIC_GetGameNewsURL();
	class UIONDataSingleton* STATIC_GetDataSingleton();
	float STATIC_GetCurrentDPIScale();
	void STATIC_GetBattleRoyaleStage(class UObject* WorldContextObject, struct FText* OutTimeText, struct FText* OutStageText);
	void STATIC_DisplaySubtitle(class UObject* WorldContextObject, const struct FText& SubtitleText);
	class UUserWidget* STATIC_CreateWidgetSlow(class UObject* WorldContextObject, class UClass* WidgetType, class APlayerController* OwningPlayer);
};


// Class IONBranch.IONSteamBadge
// 0x0000 (0x00F8 - 0x00F8)
class UIONSteamBadge : public UIONSteamItem
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamBadge");
		return ptr;
	}

};


// Class IONBranch.IONSteamBundle
// 0x0050 (0x0088 - 0x0038)
class UIONSteamBundle : public UDataAsset
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0038(0x0010) MISSED OFFSET
	struct FString                                     DisplayName;                                              // 0x0048(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                SteamItemDef;                                             // 0x0058(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsTradeable;                                             // 0x005C(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsMarketable;                                            // 0x005D(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x2];                                       // 0x005E(0x0002) MISSED OFFSET
	TArray<struct FIONBundleItem>                      Items;                                                    // 0x0060(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FString>                             Promos;                                                   // 0x0070(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	bool                                               bExport;                                                  // 0x0080(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0081(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamBundle");
		return ptr;
	}

};


// Class IONBranch.IONSteamCrate
// 0x0028 (0x0120 - 0x00F8)
class UIONSteamCrate : public UIONSteamItem
{
public:
	class UIONSteamGenerator*                          ItemGenerator;                                            // 0x00F8(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Price;                                                    // 0x0100(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0104(0x0004) MISSED OFFSET
	class UClass*                                      UnlockedWith;                                             // 0x0108(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  CrateBackdrop;                                            // 0x0110(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  CrateFooterImage;                                         // 0x0118(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamCrate");
		return ptr;
	}

};


// Class IONBranch.IONSteamGenerator
// 0x0058 (0x0090 - 0x0038)
class UIONSteamGenerator : public UDataAsset
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0038(0x0010) MISSED OFFSET
	struct FString                                     DisplayName;                                              // 0x0048(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                SteamItemDef;                                             // 0x0058(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x005C(0x0004) MISSED OFFSET
	class UIONSteamCrate*                              SteamCrate;                                               // 0x0060(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FIONGeneratorItem>                   BundleItems;                                              // 0x0068(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	TArray<struct FString>                             Promos;                                                   // 0x0078(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	bool                                               bExport;                                                  // 0x0088(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x0089(0x0007) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamGenerator");
		return ptr;
	}

};


// Class IONBranch.IONSteamInventory
// 0x00B0 (0x0350 - 0x02A0)
class UIONSteamInventory : public USceneComponent
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x02A0(0x0010) MISSED OFFSET
	struct FScriptMulticastDelegate                    SteamInventoryUpdateDelegate;                             // 0x02B0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSteamBuyItemDelegate;                                   // 0x02C0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnSteamSellItemDelegate;                                  // 0x02D0(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData01[0x70];                                      // 0x02E0(0x0070) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamInventory");
		return ptr;
	}


	void TriggerItemDrop();
	void SteamInventoryUpdateDelegate__DelegateSignature();
	bool PlayerOwnsItem(int ItemToCheck);
	void OnSteamSellItem__DelegateSignature(const struct FString& ItemDefString);
	void OnSteamBuyItem__DelegateSignature(const struct FString& ItemDefString);
	bool IsAvailable();
	TArray<int> GetItemsBySteamItemDef();
	void GenerateDevItems();
};


// Class IONBranch.IONSteamInventoryBPLib
// 0x0000 (0x0030 - 0x0030)
class UIONSteamInventoryBPLib : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamInventoryBPLib");
		return ptr;
	}

};


// Class IONBranch.IONSteamTracker
// 0x0000 (0x00F8 - 0x00F8)
class UIONSteamTracker : public UIONSteamItem
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONSteamTracker");
		return ptr;
	}

};


// Class IONBranch.IONUserFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class UIONUserFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONUserFunctionLibrary");
		return ptr;
	}


	struct FString STATIC_GetSteamOwnName();
	struct FString STATIC_GetSteamID();
	struct FString STATIC_GetSteamFriendPersonaName(const struct FString& SteamID);
	class AIONPlayerState* STATIC_GetPlayerStateFromSteamId(class UObject* WorldContextObject, const struct FString& SteamID);
	class AIONPlayerState* STATIC_GetPlayerStateFromGameSparksID(class UObject* WorldContextObject, const struct FString& GameSparksID);
	struct FString STATIC_GetPlayerId(class APlayerState* PlayerState);
	class UTexture2D* STATIC_GetPlayerAvatarFromSteamID(class UObject* Outer, const struct FString& SteamID, ESteamAvatarSize Size);
	class UTexture2D* STATIC_GetPlayerAvatar(class UObject* Outer, class APlayerState* Player, ESteamAvatarSize Size);
	class UTexture2D* STATIC_GetOwnAvatar(class UObject* Outer, class APlayerController* PC, ESteamAvatarSize Size);
	struct FString STATIC_GetAuthToken();
};


// Class IONBranch.IONVictoryHUD
// 0x0000 (0x0420 - 0x0420)
class AIONVictoryHUD : public AIONHUD
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONVictoryHUD");
		return ptr;
	}

};


// Class IONBranch.IONWaterVolume
// 0x0040 (0x03A8 - 0x0368)
class AIONWaterVolume : public APhysicsVolume
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0368(0x0008) MISSED OFFSET
	class UPostProcessComponent*                       PostProcess;                                              // 0x0370(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0378(0x0008) MISSED OFFSET
	class UMaterialInterface*                          WaterMaterial;                                            // 0x0380(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class UStaticMesh*                                 WaterMesh;                                                // 0x0388(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              MeshHeightOffset;                                         // 0x0390(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              PostProcessHeightOffset;                                  // 0x0394(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x10];                                      // 0x0398(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWaterVolume");
		return ptr;
	}

};


// Class IONBranch.IONWeaponSkin
// 0x0018 (0x0110 - 0x00F8)
class UIONWeaponSkin : public UIONSteamItem
{
public:
	TArray<class UMeshComponent*>                      ExtraMeshes;                                              // 0x00F8(0x0010) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, DisableEditOnInstance)
	class UMaterialInterface*                          WeaponMaterial;                                           // 0x0108(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWeaponSkin");
		return ptr;
	}

};


// Class IONBranch.IONWearableMannequin
// 0x0098 (0x03B8 - 0x0320)
class AIONWearableMannequin : public AActor
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0320(0x0008) MISSED OFFSET
	class USkeletalMeshComponent*                      CharacterMesh;                                            // 0x0328(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      LegsMesh;                                                 // 0x0330(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      ChestMesh;                                                // 0x0338(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      ArmsMesh;                                                 // 0x0340(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      GunMesh;                                                  // 0x0348(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        HelmetMesh;                                               // 0x0350(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UCameraComponent*                            Camera;                                                   // 0x0358(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USceneCaptureComponent2D*                    CaptureComponent;                                         // 0x0360(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	unsigned char                                      UnknownData01[0x50];                                      // 0x0368(0x0050) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWearableMannequin");
		return ptr;
	}


	void OnWearableUnequipped(class AIONWearable* Wearable);
	void OnWearableEquipped(class AIONWearable* Wearable);
	void OnWeaponChanged(class AIONWeapon* Weapon);
};


// Class IONBranch.IONWearableMannequinAdvanced
// 0x0058 (0x0378 - 0x0320)
class AIONWearableMannequinAdvanced : public AActor
{
public:
	class USceneComponent*                             CharacterPivotPoint;                                      // 0x0320(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USkeletalMeshComponent*                      CharacterMesh;                                            // 0x0328(0x0008) (Edit, BlueprintVisible, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UCameraComponent*                            Camera;                                                   // 0x0330(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class USceneCaptureComponent2D*                    SceneCapture;                                             // 0x0338(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class AIONCharacter*                               Character;                                                // 0x0340(0x0008) (ZeroConstructor, IsPlainOldData)
	float                                              TimeBetweenUpdates;                                       // 0x0348(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x2C];                                      // 0x034C(0x002C) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.IONWearableMannequinAdvanced");
		return ptr;
	}


	void UpdateMannequin();
};


// Class IONBranch.ItemSpawn
// 0x0018 (0x0338 - 0x0320)
class AItemSpawn : public AActor
{
public:
	class USceneComponent*                             SpawnPoint;                                               // 0x0320(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UStaticMeshComponent*                        Mesh;                                                     // 0x0328(0x0008) (Edit, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UClass*                                      OverrideItemClass;                                        // 0x0330(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.ItemSpawn");
		return ptr;
	}

};


// Class IONBranch.JumpPad
// 0x0008 (0x0328 - 0x0320)
class AJumpPad : public AActor
{
public:
	float                                              JumpPadVelocity;                                          // 0x0320(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              JumpPadForwardVelocity;                                   // 0x0324(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.JumpPad");
		return ptr;
	}

};


// Class IONBranch.KnifeProjectile
// 0x0018 (0x0400 - 0x03E8)
class AKnifeProjectile : public AMainProjectile
{
public:
	float                                              Damage;                                                   // 0x03E8(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FRotator                                    RotationRate;                                             // 0x03EC(0x000C) (Edit, DisableEditOnInstance, IsPlainOldData)
	class UParticleSystem*                             KnifeHitEmitter;                                          // 0x03F8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.KnifeProjectile");
		return ptr;
	}

};


// Class IONBranch.LobbyFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class ULobbyFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.LobbyFunctionLibrary");
		return ptr;
	}


	void STATIC_JoinFriendLobby(class APlayerController* PC, const struct FBlueprintOnlineFriend& Friend);
	void STATIC_InviteFriendToLobby(class APlayerController* PC, const struct FBlueprintOnlineFriend& Friend);
	void STATIC_CreateLobby(class APlayerController* PC);
	bool STATIC_CancelMatchmaking();
};


// Class IONBranch.MainAIController
// 0x0030 (0x0448 - 0x0418)
class AMainAIController : public AAIController
{
public:
	bool                                               bCreatePlayerState;                                       // 0x0418(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               bHasReachedGoal;                                          // 0x0419(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x2];                                       // 0x041A(0x0002) MISSED OFFSET
	float                                              IdleTime;                                                 // 0x041C(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FVector                                     MoveToTarget;                                             // 0x0420(0x000C) (IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x042C(0x0004) MISSED OFFSET
	class AMainPlayerController*                       OverridePlayerController;                                 // 0x0430(0x0008) (ZeroConstructor, IsPlainOldData)
	int                                                BotId;                                                    // 0x0438(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FVector                                     SpawnLocation;                                            // 0x043C(0x000C) (IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainAIController");
		return ptr;
	}

};


// Class IONBranch.MainCameraManager
// 0x0010 (0x1AF0 - 0x1AE0)
class AMainCameraManager : public APlayerCameraManager
{
public:
	class UMaterialInterface*                          StencilPostProcessMaterial;                               // 0x1AE0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x1AE8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainCameraManager");
		return ptr;
	}

};


// Class IONBranch.MainGameSession
// 0x00F8 (0x0430 - 0x0338)
class AMainGameSession : public AGameSession
{
public:
	unsigned char                                      UnknownData00[0xA8];                                      // 0x0338(0x00A8) MISSED OFFSET
	TArray<struct FString>                             AdminIDs;                                                 // 0x03E0(0x0010) (ZeroConstructor, Config)
	TArray<struct FString>                             BannedPlayerIDs;                                          // 0x03F0(0x0010) (ZeroConstructor, Config)
	unsigned char                                      UnknownData01[0x30];                                      // 0x0400(0x0030) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainGameSession");
		return ptr;
	}

};


// Class IONBranch.MainLocalPlayer
// 0x00A0 (0x0258 - 0x01B8)
class UMainLocalPlayer : public ULocalPlayer
{
public:
	struct FString                                     GameSparksID;                                             // 0x01B8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     GameSparksAuthToken;                                      // 0x01C8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     PlayerSessionID;                                          // 0x01D8(0x0010) (ZeroConstructor)
	struct FScriptMulticastDelegate                    OnLoggedIntoGameSparks;                                   // 0x01E8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnBannedGameSparks;                                       // 0x01F8(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	bool                                               bIsBanned;                                                // 0x0208(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0209(0x0007) MISSED OFFSET
	struct FText                                       KickedReason;                                             // 0x0210(0x0018) (BlueprintVisible, Transient)
	unsigned char                                      UnknownData01[0x30];                                      // 0x0228(0x0030) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainLocalPlayer");
		return ptr;
	}


	void SetPartySettings(const struct FMainMenuPartySettings& PartySettings);
	struct FString K2_GetNickname();
	bool IsPlayerBanned();
	bool IsLoggedIntoGameSparks();
	struct FMainMenuPartySettings GetPartySettings();
};


// Class IONBranch.MainPhysicalMaterial
// 0x0000 (0x0088 - 0x0088)
class UMainPhysicalMaterial : public UPhysicalMaterial
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainPhysicalMaterial");
		return ptr;
	}

};


// Class IONBranch.MainSpectatorPawn
// 0x0000 (0x03A8 - 0x03A8)
class AMainSpectatorPawn : public ASpectatorPawn
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainSpectatorPawn");
		return ptr;
	}

};


// Class IONBranch.MainWorldSettings
// 0x00A8 (0x0588 - 0x04E0)
class AMainWorldSettings : public AWorldSettings
{
public:
	TMap<struct FName, float>                          SafeZoneGroupWeights;                                     // 0x04E0(0x0050) (Edit, ZeroConstructor, DisableEditOnInstance)
	struct FVector2D                                   SpawnOrigin;                                              // 0x0530(0x0008) (Edit, IsPlainOldData)
	float                                              SpawnRadius;                                              // 0x0538(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x053C(0x0004) MISSED OFFSET
	unsigned char                                      UnknownData01[0x28];                                      // 0x053C(0x0028) UNKNOWN PROPERTY: SoftObjectProperty IONBranch.MainWorldSettings.MapTexture
	struct FVector2D                                   MapOrigin;                                                // 0x0568(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	float                                              MapSize;                                                  // 0x0570(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumMapTiles;                                              // 0x0574(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	TArray<class UClass*>                              MapRulesets;                                              // 0x0578(0x0010) (Edit, BlueprintVisible, ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MainWorldSettings");
		return ptr;
	}

};


// Class IONBranch.MatchHistoryFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class UMatchHistoryFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MatchHistoryFunctionLibrary");
		return ptr;
	}


	void STATIC_GetMatchDeathCauserInfo(const struct FString& DeathCauserContext, struct FText* OutCauserText, class UTexture2D** OutCauserIcon);
};


// Class IONBranch.MatchHistorySave
// 0x0010 (0x0040 - 0x0030)
class UMatchHistorySave : public USaveGame
{
public:
	TArray<struct FMatchStats>                         MatchHistory;                                             // 0x0030(0x0010) (ZeroConstructor, SaveGame)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MatchHistorySave");
		return ptr;
	}

};


// Class IONBranch.MenuPlayerController
// 0x00B8 (0x09F8 - 0x0940)
class AMenuPlayerController : public AIONBasePlayerController
{
public:
	unsigned char                                      UnknownData00[0x18];                                      // 0x0940(0x0018) MISSED OFFSET
	TMap<struct FString, bool>                         PlayersReadyUpStatus;                                     // 0x0958(0x0050) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	TArray<struct FString>                             InvitableFriendIDs;                                       // 0x09A8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	unsigned char                                      UnknownData01[0x20];                                      // 0x09B8(0x0020) MISSED OFFSET
	bool                                               bIsDev;                                                   // 0x09D8(0x0001) (ZeroConstructor, IsPlainOldData)
	bool                                               bIsQA;                                                    // 0x09D9(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x1E];                                      // 0x09DA(0x001E) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.MenuPlayerController");
		return ptr;
	}


	void ShowDesktopNotification();
	void JoinSession(const struct FMatchmakingServerFoundResult& Result);
	bool IsQA();
	bool IsDeveloper();
	class UIONMatchmaking* GetMatchmaking();
	void GenerateFriendsList(TArray<struct FBlueprintOnlineFriend> FriendsList);
	void FindCustomServers();
	void BPEvent_ShowOKDialog(const struct FString& Message);
	void BPEvent_PopulateCustomServers(TArray<struct FIONServerInfo> CustomServers);
	void BPEvent_PlayersReadyStatusesChanged();
	void BPEvent_PartySettingsUpdated(const struct FMainMenuPartySettings& PartySettings);
	void BPEvent_PartyMemberLeave(const struct FMainMenuPartyMember& PartyMember);
	void BPEvent_PartyMemberJoined(const struct FMainMenuPartyMember& PartyMember);
	void BPEvent_PartyInviteReceived(const struct FString& PlayerName);
};


// Class IONBranch.NametagComponent
// 0x0000 (0x07D0 - 0x07D0)
class UNametagComponent : public UWidgetComponent
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.NametagComponent");
		return ptr;
	}

};


// Class IONBranch.SeatComponent
// 0x0010 (0x02B0 - 0x02A0)
class USeatComponent : public USceneComponent
{
public:
	struct FName                                       EntrySocketName;                                          // 0x02A0(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x8];                                       // 0x02A8(0x0008) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.SeatComponent");
		return ptr;
	}


	void EnterVehicle(class AMainPlayerController* PC);
};


// Class IONBranch.PassengerSeatComponent
// 0x0000 (0x02B0 - 0x02B0)
class UPassengerSeatComponent : public USeatComponent
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.PassengerSeatComponent");
		return ptr;
	}

};


// Class IONBranch.PlasmaField
// 0x05A0 (0x08C0 - 0x0320)
class APlasmaField : public AActor
{
public:
	unsigned char                                      UnknownData00[0x2C];                                      // 0x0320(0x002C) MISSED OFFSET
	float                                              InitialRadius;                                            // 0x034C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              InitialPlasmaFieldScale;                                  // 0x0350(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	float                                              InitialWaitTime;                                          // 0x0354(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	float                                              ConvergenceEaseExponent;                                  // 0x0358(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x035C(0x0004) MISSED OFFSET
	TArray<struct FPlasmaStage>                        Stages;                                                   // 0x0360(0x0010) (ZeroConstructor, Config, GlobalConfig)
	float                                              DamageInterval;                                           // 0x0370(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	float                                              DamageAmount;                                             // 0x0374(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	float                                              DamageIncreasePerStage;                                   // 0x0378(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	float                                              FinalConvergenceDamageAmount;                             // 0x037C(0x0004) (ZeroConstructor, Config, GlobalConfig, IsPlainOldData)
	struct FPostProcessSettings                        PostProcessSettings;                                      // 0x0380(0x0520) (Edit, DisableEditOnInstance)
	float                                              PostProcessBlendRadius;                                   // 0x08A0(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x08A4(0x0004) MISSED OFFSET
	class UParticleSystem*                             ImpactEffect;                                             // 0x08A8(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UStaticMeshComponent*                        Mesh;                                                     // 0x08B0(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UAkComponent*                                AudioComponent;                                           // 0x08B8(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.PlasmaField");
		return ptr;
	}

};


// Class IONBranch.POIMarker
// 0x0028 (0x0348 - 0x0320)
class APOIMarker : public AActor
{
public:
	struct FText                                       Label;                                                    // 0x0320(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly)
	class UTexture2D*                                  Icon;                                                     // 0x0338(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              Radius;                                                   // 0x0340(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                Importance;                                               // 0x0344(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.POIMarker");
		return ptr;
	}

};


// Class IONBranch.ReadFriendsListCallbackProxy
// 0x0080 (0x00B0 - 0x0030)
class UReadFriendsListCallbackProxy : public UOnlineBlueprintCallProxyBase
{
public:
	struct FScriptMulticastDelegate                    OnSuccess;                                                // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnFailure;                                                // 0x0040(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData00[0x60];                                      // 0x0050(0x0060) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.ReadFriendsListCallbackProxy");
		return ptr;
	}


	class UReadFriendsListCallbackProxy* STATIC_ReadFriendsList(class UObject* WorldContextObject, class APlayerController* PlayerController);
};


// Class IONBranch.SafeZoneMarker
// 0x0010 (0x0330 - 0x0320)
class ASafeZoneMarker : public AActor
{
public:
	float                                              Radius;                                                   // 0x0320(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0324(0x0004) MISSED OFFSET
	struct FName                                       GroupID;                                                  // 0x0328(0x0008) (Edit, ZeroConstructor, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.SafeZoneMarker");
		return ptr;
	}

};


// Class IONBranch.SettingsFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class USettingsFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.SettingsFunctionLibrary");
		return ptr;
	}


	void STATIC_SetFloatPropertyByName(class UObject* Object, const struct FName& PropertyName, float Value);
	void STATIC_SetCVarString(const struct FString& CVarName, const struct FString& Value);
	void STATIC_SetCVarInt(const struct FString& CVarName, int Value);
	void STATIC_SetCVarFloat(const struct FString& CVarName, float Value);
	void STATIC_SetCVarBool(const struct FString& CVarName, bool bValue);
	void STATIC_SetConfigString(const struct FString& Section, const struct FString& Key, const struct FString& Value);
	void STATIC_SetConfigInt(const struct FString& Section, const struct FString& Key, int Value);
	void STATIC_SetConfigFloat(const struct FString& Section, const struct FString& Key, float Value);
	void STATIC_SetConfigBool(const struct FString& Section, const struct FString& Key, bool Value);
	void STATIC_SaveObjectConfig(class UObject* Object);
	float STATIC_GetFloatPropertyByName(class UObject* Object, const struct FName& PropertyName);
	struct FString STATIC_GetCVarString(const struct FString& CVarName);
	int STATIC_GetCVarInt(const struct FString& CVarName);
	float STATIC_GetCVarFloat(const struct FString& CVarName);
	bool STATIC_GetCVarBool(const struct FString& CVarName);
	struct FString STATIC_GetConfigString(const struct FString& Section, const struct FString& Key);
	int STATIC_GetConfigInt(const struct FString& Section, const struct FString& Key);
	float STATIC_GetConfigFloat(const struct FString& Section, const struct FString& Key);
	bool STATIC_GetConfigBool(const struct FString& Section, const struct FString& Key);
};


// Class IONBranch.SpawnCapsule
// 0x0018 (0x0338 - 0x0320)
class ASpawnCapsule : public AActor
{
public:
	class UStaticMeshComponent*                        Mesh;                                                     // 0x0320(0x0008) (Edit, ExportObject, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	float                                              WaitTime;                                                 // 0x0328(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SinkTime;                                                 // 0x032C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              CapsuleHeight;                                            // 0x0330(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0334(0x0004) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.SpawnCapsule");
		return ptr;
	}


	void InitClientCapsule();
};


// Class IONBranch.UserCloudFunctionLibrary
// 0x0000 (0x0030 - 0x0030)
class UUserCloudFunctionLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.UserCloudFunctionLibrary");
		return ptr;
	}

};


// Class IONBranch.VehicleBase
// 0x0020 (0x03A0 - 0x0380)
class AVehicleBase : public APawn
{
public:
	unsigned char                                      UnknownData00[0x8];                                       // 0x0380(0x0008) MISSED OFFSET
	class AIONCharacter*                               Character;                                                // 0x0388(0x0008) (Net, ZeroConstructor, IsPlainOldData)
	class USpringArmComponent*                         SpringArm;                                                // 0x0390(0x0008) (Edit, ExportObject, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)
	class UCameraComponent*                            Camera;                                                   // 0x0398(0x0008) (Edit, ExportObject, ZeroConstructor, EditConst, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.VehicleBase");
		return ptr;
	}


	void ServerExitVehicle();
};


// Class IONBranch.WheeledVehicleBase
// 0x0010 (0x03B0 - 0x03A0)
class AWheeledVehicleBase : public AVehicleBase
{
public:
	class USkeletalMeshComponent*                      Mesh;                                                     // 0x03A0(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, EditConst, InstancedReference, IsPlainOldData)
	class UWheeledVehicleMovementComponent*            VehicleMovement;                                          // 0x03A8(0x0008) (Edit, BlueprintVisible, ExportObject, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, EditConst, InstancedReference, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class IONBranch.WheeledVehicleBase");
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
