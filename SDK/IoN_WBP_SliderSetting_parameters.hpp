#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SliderSetting.WBP_SliderSetting_C.SetValue
struct UWBP_SliderSetting_C_SetValue_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Slider Master_Value_1
struct UWBP_SliderSetting_C_Get_Slider_Master_Value_1_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Edit Master_Text_1
struct UWBP_SliderSetting_C_Get_Edit_Master_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Edit Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature
struct UWBP_SliderSetting_C_BndEvt__Edit_Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.Construct
struct UWBP_SliderSetting_C_Construct_Params
{
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.Update Value
struct UWBP_SliderSetting_C_Update_Value_Params
{
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Mouse Horizontal Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature
struct UWBP_SliderSetting_C_BndEvt__Slider_Mouse_Horizontal_Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.PreConstruct
struct UWBP_SliderSetting_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature
struct UWBP_SliderSetting_C_BndEvt__Slider_Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature_Params
{
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.ExecuteUbergraph_WBP_SliderSetting
struct UWBP_SliderSetting_C_ExecuteUbergraph_WBP_SliderSetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SliderSetting.WBP_SliderSetting_C.OnMouseEventEnd__DelegateSignature
struct UWBP_SliderSetting_C_OnMouseEventEnd__DelegateSignature_Params
{
	float                                              Slide_Value;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
