// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_IonHelmet.BP_IonHelmet_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_IonHelmet_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonHelmet.BP_IonHelmet_C.UserConstructionScript");

	ABP_IonHelmet_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_IonHelmet.BP_IonHelmet_C.ExecuteUbergraph_BP_IonHelmet
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_IonHelmet_C::ExecuteUbergraph_BP_IonHelmet(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_IonHelmet.BP_IonHelmet_C.ExecuteUbergraph_BP_IonHelmet");

	ABP_IonHelmet_C_ExecuteUbergraph_BP_IonHelmet_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
