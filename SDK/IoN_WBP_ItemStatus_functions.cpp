// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ItemStatus.WBP_ItemStatus_C.Get_Image_Status_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_ItemStatus_C::Get_Image_Status_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemStatus.WBP_ItemStatus_C.Get_Image_Status_Visibility_1");

	UWBP_ItemStatus_C_Get_Image_Status_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ItemStatus.WBP_ItemStatus_C.Get_Image_Status_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_ItemStatus_C::Get_Image_Status_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemStatus.WBP_ItemStatus_C.Get_Image_Status_Brush_1");

	UWBP_ItemStatus_C_Get_Image_Status_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ItemStatus.WBP_ItemStatus_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ItemStatus_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemStatus.WBP_ItemStatus_C.Tick");

	UWBP_ItemStatus_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ItemStatus.WBP_ItemStatus_C.ExecuteUbergraph_WBP_ItemStatus
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ItemStatus_C::ExecuteUbergraph_WBP_ItemStatus(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ItemStatus.WBP_ItemStatus_C.ExecuteUbergraph_WBP_ItemStatus");

	UWBP_ItemStatus_C_ExecuteUbergraph_WBP_ItemStatus_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
