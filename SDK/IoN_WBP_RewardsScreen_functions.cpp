// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowMainMenu
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_RewardsScreen_C::ShowMainMenu()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowMainMenu");

	UWBP_RewardsScreen_C_ShowMainMenu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.UpdatePrices
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_RewardsScreen_C::UpdatePrices()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.UpdatePrices");

	UWBP_RewardsScreen_C_UpdatePrices_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowItemScreen
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bShow                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::ShowItemScreen(bool bShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowItemScreen");

	UWBP_RewardsScreen_C_ShowItemScreen_Params params;
	params.bShow = bShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowCrateContentWidget
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_RewardsScreen_C::ShowCrateContentWidget()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ShowCrateContentWidget");

	UWBP_RewardsScreen_C_ShowCrateContentWidget_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.CrateContentSelected
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_Icon_C*             ItemBtnSelected                (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_RewardsScreen_C::CrateContentSelected(class UWBP_Icon_C* ItemBtnSelected)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.CrateContentSelected");

	UWBP_RewardsScreen_C_CrateContentSelected_Params params;
	params.ItemBtnSelected = ItemBtnSelected;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.TryConfirmPurchase
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::TryConfirmPurchase(class UIONSteamCrate* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.TryConfirmPurchase");

	UWBP_RewardsScreen_C_TryConfirmPurchase_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ViewCrateContents
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamCrate*          CrateToShow                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::ViewCrateContents(class UIONSteamCrate* CrateToShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ViewCrateContents");

	UWBP_RewardsScreen_C_ViewCrateContents_Params params;
	params.CrateToShow = CrateToShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ToogleConfirmDialouge
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bShow                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::ToogleConfirmDialouge(bool bShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ToogleConfirmDialouge");

	UWBP_RewardsScreen_C_ToogleConfirmDialouge_Params params;
	params.bShow = bShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_RewardsScreen_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.Construct");

	UWBP_RewardsScreen_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RewardsScreen_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_65_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RewardsScreen_C::BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__ErrorCloseBtn_K2Node_ComponentBoundEvent_196_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_RewardsScreen_C::BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__ConfirmBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.PurchaseConfirmed
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            ItemDefID                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::PurchaseConfirmed(int ItemDefID)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.PurchaseConfirmed");

	UWBP_RewardsScreen_C_PurchaseConfirmed_Params params;
	params.ItemDefID = ItemDefID;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Weapon_K2Node_ComponentBoundEvent_12_PurchaseItem__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Weapon_0_K2Node_ComponentBoundEvent_60_PurchaseItem__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_RewardsScreen_C::BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_25_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.CratePurchaseFailed
// (BlueprintCallable, BlueprintEvent)

void UWBP_RewardsScreen_C::CratePurchaseFailed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.CratePurchaseFailed");

	UWBP_RewardsScreen_C_CratePurchaseFailed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_StoreItem_Armor_0_K2Node_ComponentBoundEvent_109_PurchaseItem__DelegateSignature_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_RewardsScreen_C::BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_MMBtn_K2Node_ComponentBoundEvent_171_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Crate                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature(class UIONSteamItem* Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_200_PurchaseItem__DelegateSignature_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__StoreItem_Weapon_1_K2Node_ComponentBoundEvent_1_ViewItemDetails__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__StoreItem_Weapon_2_K2Node_ComponentBoundEvent_44_ViewItemDetails__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_Nebula_K2Node_ComponentBoundEvent_97_ViewDetailsItem__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature
// (BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature");

	UWBP_RewardsScreen_C_BndEvt__WBP_Proginator_K2Node_ComponentBoundEvent_123_ViewDetailsItem__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_RewardsScreen.WBP_RewardsScreen_C.ExecuteUbergraph_WBP_RewardsScreen
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_RewardsScreen_C::ExecuteUbergraph_WBP_RewardsScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_RewardsScreen.WBP_RewardsScreen_C.ExecuteUbergraph_WBP_RewardsScreen");

	UWBP_RewardsScreen_C_ExecuteUbergraph_WBP_RewardsScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
