#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ChatBox.WBP_ChatBox_C.GetText_1
struct UWBP_ChatBox_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ChatBox.WBP_ChatBox_C.Get_ChatTextBox_HintText_1
struct UWBP_ChatBox_C_Get_ChatTextBox_HintText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ChatBox.WBP_ChatBox_C.CleanChat
struct UWBP_ChatBox_C_CleanChat_Params
{
};

// Function WBP_ChatBox.WBP_ChatBox_C.Construct
struct UWBP_ChatBox_C_Construct_Params
{
};

// Function WBP_ChatBox.WBP_ChatBox_C.Start Message
struct UWBP_ChatBox_C_Start_Message_Params
{
	struct FName                                       Type;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature
struct UWBP_ChatBox_C_BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature
struct UWBP_ChatBox_C_BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_ChatBox.WBP_ChatBox_C.OnReceivedChatMessage
struct UWBP_ChatBox_C_OnReceivedChatMessage_Params
{
	class AIONPlayerState*                             From;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FName                                       Type;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)
};

// Function WBP_ChatBox.WBP_ChatBox_C.Show Chat
struct UWBP_ChatBox_C_Show_Chat_Params
{
};

// Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat
struct UWBP_ChatBox_C_Hide_Chat_Params
{
};

// Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat Hint
struct UWBP_ChatBox_C_Hide_Chat_Hint_Params
{
	bool                                               Instant;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ChatBox.WBP_ChatBox_C.OnAnimationFinished
struct UWBP_ChatBox_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_ChatBox.WBP_ChatBox_C.ExecuteUbergraph_WBP_ChatBox
struct UWBP_ChatBox_C_ExecuteUbergraph_WBP_ChatBox_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
