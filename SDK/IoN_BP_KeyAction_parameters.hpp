#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_KeyAction.BP_KeyAction_C.Get Mapping
struct UBP_KeyAction_C_Get_Mapping_Params
{
	struct FString                                     Mapping_Name;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UBP_KeyMapping_C*                            Mapping;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Success;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyAction.BP_KeyAction_C.Load Action
struct UBP_KeyAction_C_Load_Action_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyAction.BP_KeyAction_C.Save Action
struct UBP_KeyAction_C_Save_Action_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyAction.BP_KeyAction_C.Key Action Current State
struct UBP_KeyAction_C_Key_Action_Current_State_Params
{
	class APlayerController*                           Player_Controller;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Action_Axis_Value;                                        // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyAction.BP_KeyAction_C.Init Key Action
struct UBP_KeyAction_C_Init_Key_Action_Params
{
	struct FSKeyAction                                 Key_Action;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FString                                     Action_Name;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UBP_KeyAction_C*                             Action;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
