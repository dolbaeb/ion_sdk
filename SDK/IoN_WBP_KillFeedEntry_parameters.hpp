#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_Border_Distance_Visibility_1
struct UWBP_KillFeedEntry_C_Get_Border_Distance_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetText_Distance
struct UWBP_KillFeedEntry_C_GetText_Distance_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_ColorAndOpacity_1
struct UWBP_KillFeedEntry_C_Get_WeaponIcon_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetRelationShipColor
struct UWBP_KillFeedEntry_C_GetRelationShipColor_Params
{
	class APlayerState*                                OtherPlayer;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_InfoText
struct UWBP_KillFeedEntry_C_Get_InfoText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_Brush_1
struct UWBP_KillFeedEntry_C_Get_WeaponIcon_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_VictimText_Text_1
struct UWBP_KillFeedEntry_C_Get_VictimText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_KillerText_Text_1
struct UWBP_KillFeedEntry_C_Get_KillerText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_HeadshotIcon_Visibility_1
struct UWBP_KillFeedEntry_C_Get_HeadshotIcon_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnAnimationFinished
struct UWBP_KillFeedEntry_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Construct
struct UWBP_KillFeedEntry_C_Construct_Params
{
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.PreConstruct
struct UWBP_KillFeedEntry_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.ExecuteUbergraph_WBP_KillFeedEntry
struct UWBP_KillFeedEntry_C_ExecuteUbergraph_WBP_KillFeedEntry_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnFinished__DelegateSignature
struct UWBP_KillFeedEntry_C_OnFinished__DelegateSignature_Params
{
	class UWBP_KillFeedEntry_C*                        Entry;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
