// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_KeyAction.BP_KeyAction_C.Get Mapping
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Mapping_Name                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UBP_KeyMapping_C*        Mapping                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Success                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyAction_C::Get_Mapping(const struct FString& Mapping_Name, class UBP_KeyMapping_C** Mapping, bool* Success)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyAction.BP_KeyAction_C.Get Mapping");

	UBP_KeyAction_C_Get_Mapping_Params params;
	params.Mapping_Name = Mapping_Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Mapping != nullptr)
		*Mapping = params.Mapping;
	if (Success != nullptr)
		*Success = params.Success;
}


// Function BP_KeyAction.BP_KeyAction_C.Load Action
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyAction_C::Load_Action(class UBP_GameSettings_C* Game_Settings)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyAction.BP_KeyAction_C.Load Action");

	UBP_KeyAction_C_Load_Action_Params params;
	params.Game_Settings = Game_Settings;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyAction.BP_KeyAction_C.Save Action
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Game_Settings                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_KeyAction_C::Save_Action(class UBP_GameSettings_C* Game_Settings)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyAction.BP_KeyAction_C.Save Action");

	UBP_KeyAction_C_Save_Action_Params params;
	params.Game_Settings = Game_Settings;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_KeyAction.BP_KeyAction_C.Key Action Current State
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerController*       Player_Controller              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Action_Axis_Value              (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Pressed                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Just_Released                  (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyAction_C::Key_Action_Current_State(class APlayerController* Player_Controller, float* Action_Axis_Value, bool* Just_Pressed, bool* Just_Released)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyAction.BP_KeyAction_C.Key Action Current State");

	UBP_KeyAction_C_Key_Action_Current_State_Params params;
	params.Player_Controller = Player_Controller;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Action_Axis_Value != nullptr)
		*Action_Axis_Value = params.Action_Axis_Value;
	if (Just_Pressed != nullptr)
		*Just_Pressed = params.Just_Pressed;
	if (Just_Released != nullptr)
		*Just_Released = params.Just_Released;
}


// Function BP_KeyAction.BP_KeyAction_C.Init Key Action
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyAction             Key_Action                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FString                 Action_Name                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UBP_KeyAction_C*         Action                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_KeyAction_C::Init_Key_Action(const struct FSKeyAction& Key_Action, const struct FString& Action_Name, class UBP_KeyAction_C** Action)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_KeyAction.BP_KeyAction_C.Init Key Action");

	UBP_KeyAction_C_Init_Key_Action_Params params;
	params.Key_Action = Key_Action;
	params.Action_Name = Action_Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Action != nullptr)
		*Action = params.Action;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
