#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// Enum SocketIOClient.ESIOConnectionCloseReason
enum class ESIOConnectionCloseReason : uint8_t
{
	CLOSE_REASON_NORMAL            = 0,
	CLOSE_REASON_DROP              = 1,
	CLOSE_REASON_MAX               = 2
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
