// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnMouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_PlayerStatsPopup_C::OnMouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnMouseButtonDown_1");

	UWBP_PlayerStatsPopup_C_OnMouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnResponse_4EA7138F45BE4EC38F18A393EDE067EB
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FPlayerStats            PlayerStats                    (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           bHasErrors                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatsPopup_C::OnResponse_4EA7138F45BE4EC38F18A393EDE067EB(const struct FPlayerStats& PlayerStats, bool bHasErrors)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnResponse_4EA7138F45BE4EC38F18A393EDE067EB");

	UWBP_PlayerStatsPopup_C_OnResponse_4EA7138F45BE4EC38F18A393EDE067EB_Params params;
	params.PlayerStats = PlayerStats;
	params.bHasErrors = bHasErrors;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PlayerStatsPopup_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.Construct");

	UWBP_PlayerStatsPopup_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.ExecuteUbergraph_WBP_PlayerStatsPopup
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerStatsPopup_C::ExecuteUbergraph_WBP_PlayerStatsPopup(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.ExecuteUbergraph_WBP_PlayerStatsPopup");

	UWBP_PlayerStatsPopup_C_ExecuteUbergraph_WBP_PlayerStatsPopup_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_PlayerStatsPopup_C::OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerStatsPopup.WBP_PlayerStatsPopup_C.OnClicked__DelegateSignature");

	UWBP_PlayerStatsPopup_C_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
