#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_BulletShake_Pistol.BP_BulletShake_Pistol_C.NewFunction_1
struct UBP_BulletShake_Pistol_C_NewFunction_1_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
