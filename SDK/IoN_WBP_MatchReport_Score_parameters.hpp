#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Score_Total_Text_1
struct UWBP_MatchReport_Score_C_Get_Score_Total_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Kill_Text_1
struct UWBP_MatchReport_Score_C_Get_Sub_Kill_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Placement_Text_1
struct UWBP_MatchReport_Score_C_Get_Sub_Placement_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_Sub_Damage_Text_1
struct UWBP_MatchReport_Score_C_Get_Sub_Damage_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_NextLevel_Text_1
struct UWBP_MatchReport_Score_C_Get_TextBlock_NextLevel_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Get_TextBlock_CurrentLevel_Text_1
struct UWBP_MatchReport_Score_C_Get_TextBlock_CurrentLevel_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ScoreBarAppear
struct UWBP_MatchReport_Score_C_ScoreBarAppear_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnMinimize
struct UWBP_MatchReport_Score_C_OnMinimize_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnAnimationFinished
struct UWBP_MatchReport_Score_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Appear
struct UWBP_MatchReport_Score_C_Appear_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Disappear
struct UWBP_MatchReport_Score_C_Disappear_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnSetMatchHistory
struct UWBP_MatchReport_Score_C_OnSetMatchHistory_Params
{
	struct FPlayerMatchHistory                         MatchHistory;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature
struct UWBP_MatchReport_Score_C_BndEvt__Kill_Score_K2Node_ComponentBoundEvent_0_OnValueUpdated__DelegateSignature_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature
struct UWBP_MatchReport_Score_C_BndEvt__Damage_Score_K2Node_ComponentBoundEvent_1_OnValueUpdated__DelegateSignature_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature
struct UWBP_MatchReport_Score_C_BndEvt__Placement_Score_K2Node_ComponentBoundEvent_2_OnValueUpdated__DelegateSignature_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature
struct UWBP_MatchReport_Score_C_BndEvt__WBP_LevelScoreBar_K2Node_ComponentBoundEvent_3_OnLevelUp__DelegateSignature_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Begin Animation
struct UWBP_MatchReport_Score_C_Begin_Animation_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ShowRewards
struct UWBP_MatchReport_Score_C_ShowRewards_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.OnReportClicked
struct UWBP_MatchReport_Score_C_OnReportClicked_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Fast Forward
struct UWBP_MatchReport_Score_C_Fast_Forward_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountPlacementScore
struct UWBP_MatchReport_Score_C_CountPlacementScore_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountDamageScore
struct UWBP_MatchReport_Score_C_CountDamageScore_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.CountKillScore
struct UWBP_MatchReport_Score_C_CountKillScore_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.Construct
struct UWBP_MatchReport_Score_C_Construct_Params
{
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.ExecuteUbergraph_WBP_MatchReport_Score
struct UWBP_MatchReport_Score_C_ExecuteUbergraph_WBP_MatchReport_Score_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MatchReport_Score.WBP_MatchReport_Score_C.AnimationFinished__DelegateSignature
struct UWBP_MatchReport_Score_C_AnimationFinished__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
