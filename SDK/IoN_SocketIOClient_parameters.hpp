#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function SocketIOClient.SocketIOClientComponent.EmitWithCallBack
struct USocketIOClientComponent_EmitWithCallBack_Params
{
	struct FString                                     EventName;                                                // (Parm, ZeroConstructor)
	class USIOJsonValue*                               Message;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     CallbackFunctionName;                                     // (Parm, ZeroConstructor)
	class UObject*                                     Target;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Namespace;                                                // (Parm, ZeroConstructor)
};

// Function SocketIOClient.SocketIOClientComponent.Emit
struct USocketIOClientComponent_Emit_Params
{
	struct FString                                     EventName;                                                // (Parm, ZeroConstructor)
	class USIOJsonValue*                               Message;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Namespace;                                                // (Parm, ZeroConstructor)
};

// Function SocketIOClient.SocketIOClientComponent.Disconnect
struct USocketIOClientComponent_Disconnect_Params
{
};

// Function SocketIOClient.SocketIOClientComponent.Connect
struct USocketIOClientComponent_Connect_Params
{
	struct FString                                     InAddressAndPort;                                         // (Parm, ZeroConstructor)
	class USIOJsonObject*                              Query;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonObject*                              Headers;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SocketIOClient.SocketIOClientComponent.BindEventToFunction
struct USocketIOClientComponent_BindEventToFunction_Params
{
	struct FString                                     EventName;                                                // (Parm, ZeroConstructor)
	struct FString                                     FunctionName;                                             // (Parm, ZeroConstructor)
	class UObject*                                     Target;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Namespace;                                                // (Parm, ZeroConstructor)
};

// Function SocketIOClient.SocketIOClientComponent.BindEvent
struct USocketIOClientComponent_BindEvent_Params
{
	struct FString                                     EventName;                                                // (Parm, ZeroConstructor)
	struct FString                                     Namespace;                                                // (Parm, ZeroConstructor)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
