#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_NewItemReceived.WBP_NewItemReceived_C.SetSteamItem
struct UWBP_NewItemReceived_C_SetSteamItem_Params
{
	struct FIONSteamInventoryItem                      Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_NewItemReceived.WBP_NewItemReceived_C.Construct
struct UWBP_NewItemReceived_C_Construct_Params
{
};

// Function WBP_NewItemReceived.WBP_NewItemReceived_C.BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature
struct UWBP_NewItemReceived_C_BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature_Params
{
};

// Function WBP_NewItemReceived.WBP_NewItemReceived_C.ExecuteUbergraph_WBP_NewItemReceived
struct UWBP_NewItemReceived_C_ExecuteUbergraph_WBP_NewItemReceived_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
