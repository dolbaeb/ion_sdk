#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EKeyCombinationDisplay.EKeyCombinationDisplay
enum class EKeyCombinationDisplay : uint8_t
{
	EKeyCombinationDisplay__NewEnumerator0 = 0,
	EKeyCombinationDisplay__NewEnumerator1 = 1,
	EKeyCombinationDisplay__NewEnumerator2 = 2,
	EKeyCombinationDisplay__EKeyCombinationDisplay_MAX = 3
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
