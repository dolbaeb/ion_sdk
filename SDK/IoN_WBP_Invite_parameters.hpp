#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Invite.WBP_Invite_C.GetbIsEnabled_1
struct UWBP_Invite_C_GetbIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Invite.WBP_Invite_C.OnGetMenuContent_1
struct UWBP_Invite_C_OnGetMenuContent_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_Invite.WBP_Invite_C.GetText_1
struct UWBP_Invite_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Invite.WBP_Invite_C.Get_Button_Invite_Visibility_1
struct UWBP_Invite_C_Get_Button_Invite_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Invite.WBP_Invite_C.GetColorAndOpacity_1
struct UWBP_Invite_C_GetColorAndOpacity_1_Params
{
	struct FSlateColor                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Invite.WBP_Invite_C.AddToPartyBtnPressed__DelegateSignature
struct UWBP_Invite_C_AddToPartyBtnPressed__DelegateSignature_Params
{
	struct FBlueprintOnlineFriend                      OnlineFriend;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
