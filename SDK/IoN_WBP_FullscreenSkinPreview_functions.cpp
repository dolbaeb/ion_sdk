// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CheckRarityFX
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_FullscreenSkinPreview_C::CheckRarityFX()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CheckRarityFX");

	UWBP_FullscreenSkinPreview_C_CheckRarityFX_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.SetSteamItem
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_FullscreenSkinPreview_C::SetSteamItem(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.SetSteamItem");

	UWBP_FullscreenSkinPreview_C_SetSteamItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_FullscreenSkinPreview_C::BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature");

	UWBP_FullscreenSkinPreview_C_BndEvt__CloseBtn_K2Node_ComponentBoundEvent_27_BtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.ExecuteUbergraph_WBP_FullscreenSkinPreview
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_FullscreenSkinPreview_C::ExecuteUbergraph_WBP_FullscreenSkinPreview(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.ExecuteUbergraph_WBP_FullscreenSkinPreview");

	UWBP_FullscreenSkinPreview_C_ExecuteUbergraph_WBP_FullscreenSkinPreview_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CloseBtnPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_FullscreenSkinPreview_C::CloseBtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FullscreenSkinPreview.WBP_FullscreenSkinPreview_C.CloseBtnPressed__DelegateSignature");

	UWBP_FullscreenSkinPreview_C_CloseBtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
