#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Friend.WBP_Friend_C.GetbIsEnabled_1
struct UWBP_Friend_C_GetbIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Friend.WBP_Friend_C.OnGetMenuContent_1
struct UWBP_Friend_C_OnGetMenuContent_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_Friend.WBP_Friend_C.GetText_1
struct UWBP_Friend_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Friend.WBP_Friend_C.Get_Button_Invite_Visibility_1
struct UWBP_Friend_C_Get_Button_Invite_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Friend.WBP_Friend_C.GetColorAndOpacity_1
struct UWBP_Friend_C_GetColorAndOpacity_1_Params
{
	struct FSlateColor                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature
struct UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_Friend.WBP_Friend_C.SetFriend
struct UWBP_Friend_C_SetFriend_Params
{
	struct FBlueprintOnlineFriend                      Friend;                                                   // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature
struct UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature
struct UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_Friend.WBP_Friend_C.BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature
struct UWBP_Friend_C_BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_Friend.WBP_Friend_C.ExecuteUbergraph_WBP_Friend
struct UWBP_Friend_C_ExecuteUbergraph_WBP_Friend_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Friend.WBP_Friend_C.AddToPartyBtnPressed__DelegateSignature
struct UWBP_Friend_C_AddToPartyBtnPressed__DelegateSignature_Params
{
	struct FBlueprintOnlineFriend                      OnlineFriend;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
