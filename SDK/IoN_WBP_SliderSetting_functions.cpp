// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SliderSetting.WBP_SliderSetting_C.SetValue
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::SetValue(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.SetValue");

	UWBP_SliderSetting_C_SetValue_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Slider Master_Value_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_SliderSetting_C::Get_Slider_Master_Value_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Slider Master_Value_1");

	UWBP_SliderSetting_C_Get_Slider_Master_Value_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Edit Master_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SliderSetting_C::Get_Edit_Master_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.Get_Edit Master_Text_1");

	UWBP_SliderSetting_C_Get_Edit_Master_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Edit Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::BndEvt__Edit_Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Edit Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature");

	UWBP_SliderSetting_C_BndEvt__Edit_Master_K2Node_ComponentBoundEvent_30_OnEditableTextCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SliderSetting_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.Construct");

	UWBP_SliderSetting_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.Update Value
// (BlueprintCallable, BlueprintEvent)

void UWBP_SliderSetting_C::Update_Value()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.Update Value");

	UWBP_SliderSetting_C_Update_Value_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Mouse Horizontal Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::BndEvt__Slider_Mouse_Horizontal_Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Mouse Horizontal Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature");

	UWBP_SliderSetting_C_BndEvt__Slider_Mouse_Horizontal_Sensitivity_K2Node_ComponentBoundEvent_83_OnFloatValueChangedEvent__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.PreConstruct");

	UWBP_SliderSetting_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SliderSetting_C::BndEvt__Slider_Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.BndEvt__Slider Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature");

	UWBP_SliderSetting_C_BndEvt__Slider_Master_K2Node_ComponentBoundEvent_0_OnMouseCaptureEndEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.ExecuteUbergraph_WBP_SliderSetting
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::ExecuteUbergraph_WBP_SliderSetting(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.ExecuteUbergraph_WBP_SliderSetting");

	UWBP_SliderSetting_C_ExecuteUbergraph_WBP_SliderSetting_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SliderSetting.WBP_SliderSetting_C.OnMouseEventEnd__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Slide_Value                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SliderSetting_C::OnMouseEventEnd__DelegateSignature(float Slide_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SliderSetting.WBP_SliderSetting_C.OnMouseEventEnd__DelegateSignature");

	UWBP_SliderSetting_C_OnMouseEventEnd__DelegateSignature_Params params;
	params.Slide_Value = Slide_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
