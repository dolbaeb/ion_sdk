#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardOptions_C_BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardOptions_C_BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Select Option
struct UWBP_LeaderboardOptions_C_Select_Option_Params
{
	class UTextBlock*                                  Text;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UImage*                                      Image;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardOptions_C_BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardOptions_C_BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Construct
struct UWBP_LeaderboardOptions_C_Construct_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardOptions_C_BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.ExecuteUbergraph_WBP_LeaderboardOptions
struct UWBP_LeaderboardOptions_C_ExecuteUbergraph_WBP_LeaderboardOptions_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.OnCategoryChanged__DelegateSignature
struct UWBP_LeaderboardOptions_C_OnCategoryChanged__DelegateSignature_Params
{
	TEnumAsByte<ELeaderboardCategories>                Category;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
