// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ProximityLoot.WBP_ProximityLoot_C.AddItemToSlots
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONItem*                Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityLoot_C::AddItemToSlots(class AIONItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.AddItemToSlots");

	UWBP_ProximityLoot_C_AddItemToSlots_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetListForItem
// (Private, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONItem*                Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class UVerticalBox*            ListBox                        (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_ProximityLoot_C::GetListForItem(class AIONItem* Item, class UVerticalBox** ListBox)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetListForItem");

	UWBP_ProximityLoot_C_GetListForItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (ListBox != nullptr)
		*ListBox = params.ListBox;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetCurrentItems
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// TArray<class AIONItem*>        ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm)

TArray<class AIONItem*> UWBP_ProximityLoot_C::GetCurrentItems()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.GetCurrentItems");

	UWBP_ProximityLoot_C_GetCurrentItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.SetProximityItems
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<class AIONItem*>        ProximityItems                 (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_ProximityLoot_C::SetProximityItems(TArray<class AIONItem*>* ProximityItems)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.SetProximityItems");

	UWBP_ProximityLoot_C_SetProximityItems_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (ProximityItems != nullptr)
		*ProximityItems = params.ProximityItems;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ProximityLoot_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.Construct");

	UWBP_ProximityLoot_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityLoot_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.Tick");

	UWBP_ProximityLoot_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ProximityLoot.WBP_ProximityLoot_C.ExecuteUbergraph_WBP_ProximityLoot
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ProximityLoot_C::ExecuteUbergraph_WBP_ProximityLoot(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ProximityLoot.WBP_ProximityLoot_C.ExecuteUbergraph_WBP_ProximityLoot");

	UWBP_ProximityLoot_C_ExecuteUbergraph_WBP_ProximityLoot_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
