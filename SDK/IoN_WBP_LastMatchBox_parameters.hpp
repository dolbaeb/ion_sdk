#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseButtonDown
struct UWBP_LastMatchBox_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_Highlight_Visibility_1
struct UWBP_LastMatchBox_C_Get_Highlight_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Mode_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Mode_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Score_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Score_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Visibility_1
struct UWBP_LastMatchBox_C_Get_TextBlock_KilledBy_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledByWeapon_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_KilledByWeapon_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Date_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Date_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Time_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Time_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_MapName_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_MapName_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_KilledBy_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_KilledBy_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Duration_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Duration_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_PlacementSuffix_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_PlacementSuffix_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_Placement_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_Placement_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Get_TextBlock_NumKills_Text_1
struct UWBP_LastMatchBox_C_Get_TextBlock_NumKills_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnResponse_451DE5964F9B8049D003B7840A2EFAB3
struct UWBP_LastMatchBox_C_OnResponse_451DE5964F9B8049D003B7840A2EFAB3_Params
{
	struct FPlayerMatchHistory                         PlayerMatchHistory;                                       // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               bHasErrors;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseEnter
struct UWBP_LastMatchBox_C_OnMouseEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.OnMouseLeave
struct UWBP_LastMatchBox_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Construct
struct UWBP_LastMatchBox_C_Construct_Params
{
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Connected
struct UWBP_LastMatchBox_C_Connected_Params
{
	struct FString*                                    UserId;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString*                                    AuthToken;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature
struct UWBP_LastMatchBox_C_BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_0_OnRetryClicked__DelegateSignature_Params
{
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.Reconnect
struct UWBP_LastMatchBox_C_Reconnect_Params
{
};

// Function WBP_LastMatchBox.WBP_LastMatchBox_C.ExecuteUbergraph_WBP_LastMatchBox
struct UWBP_LastMatchBox_C_ExecuteUbergraph_WBP_LastMatchBox_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
