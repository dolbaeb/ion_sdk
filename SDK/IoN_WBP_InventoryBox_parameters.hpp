#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InventoryBox.WBP_InventoryBox_C.DeselectItem
struct UWBP_InventoryBox_C_DeselectItem_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.ClearBtnSelectionState
struct UWBP_InventoryBox_C_ClearBtnSelectionState_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.CacheCategoryButtons
struct UWBP_InventoryBox_C_CacheCategoryButtons_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.SetBtnSelectionState
struct UWBP_InventoryBox_C_SetBtnSelectionState_Params
{
	class UWBP_WeaponCategoryBtn_C*                    SelectedButton;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.ToogleInspectPanel
struct UWBP_InventoryBox_C_ToogleInspectPanel_Params
{
	bool                                               bShow;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.ShowCrateButtons
struct UWBP_InventoryBox_C_ShowCrateButtons_Params
{
	bool                                               bShow;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.NewItemSelected
struct UWBP_InventoryBox_C_NewItemSelected_Params
{
	struct FIONSteamInventoryItem                      NewItem;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.GetCurrentSelection
struct UWBP_InventoryBox_C_GetCurrentSelection_Params
{
	class UWBP_Icon_C*                                 CurrentSelection;                                         // (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.Construct
struct UWBP_InventoryBox_C_Construct_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature_Params
{
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature
struct UWBP_InventoryBox_C_BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.ExecuteUbergraph_WBP_InventoryBox
struct UWBP_InventoryBox_C_ExecuteUbergraph_WBP_InventoryBox_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryBox.WBP_InventoryBox_C.CrateOpen__DelegateSignature
struct UWBP_InventoryBox_C_CrateOpen__DelegateSignature_Params
{
	struct FIONSteamInventoryItem                      CrateToOpen;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
