#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerBox.WBP_PlayerBox_C.OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1
struct UWBP_PlayerBox_C_OnResponse_E5D6CAE141B9FFBBE7A91EA89B8D3CE1_Params
{
	struct FPlayerProfile                              PlayerProfile;                                            // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               bHasErrors;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.Connected
struct UWBP_PlayerBox_C_Connected_Params
{
	struct FString*                                    UserId;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString*                                    AuthToken;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.Construct
struct UWBP_PlayerBox_C_Construct_Params
{
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.UpdateModeInfo
struct UWBP_PlayerBox_C_UpdateModeInfo_Params
{
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature
struct UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_K2Node_ComponentBoundEvent_5_OnModeButtonClicked__DelegateSignature_Params
{
	int                                                Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature
struct UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_0_K2Node_ComponentBoundEvent_6_OnModeButtonClicked__DelegateSignature_Params
{
	int                                                Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature
struct UWBP_PlayerBox_C_BndEvt__WBP_PlayerBoxModeTab_1_K2Node_ComponentBoundEvent_8_OnModeButtonClicked__DelegateSignature_Params
{
	int                                                Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature
struct UWBP_PlayerBox_C_BndEvt__WBP_ConnectionErrorOverlay_K2Node_ComponentBoundEvent_1_OnRetryClicked__DelegateSignature_Params
{
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.Reconnect
struct UWBP_PlayerBox_C_Reconnect_Params
{
};

// Function WBP_PlayerBox.WBP_PlayerBox_C.ExecuteUbergraph_WBP_PlayerBox
struct UWBP_PlayerBox_C_ExecuteUbergraph_WBP_PlayerBox_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
