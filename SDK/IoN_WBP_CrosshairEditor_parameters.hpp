#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature
struct UWBP_CrosshairEditor_C_BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.PreConstruct
struct UWBP_CrosshairEditor_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.Construct
struct UWBP_CrosshairEditor_C_Construct_Params
{
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.UpdateCrosshairPreview
struct UWBP_CrosshairEditor_C_UpdateCrosshairPreview_Params
{
};

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.ExecuteUbergraph_WBP_CrosshairEditor
struct UWBP_CrosshairEditor_C_ExecuteUbergraph_WBP_CrosshairEditor_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
