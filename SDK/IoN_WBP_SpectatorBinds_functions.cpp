// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorBinds_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.PreConstruct");

	UWBP_SpectatorBinds_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SpectatorBinds_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Construct");

	UWBP_SpectatorBinds_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorBinds_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Tick");

	UWBP_SpectatorBinds_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Set Output Text
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_SpectatorBinds_C::Set_Output_Text(const struct FText& Text)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.Set Output Text");

	UWBP_SpectatorBinds_C_Set_Output_Text_Params params;
	params.Text = Text;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.ExecuteUbergraph_WBP_SpectatorBinds
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorBinds_C::ExecuteUbergraph_WBP_SpectatorBinds(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorBinds.WBP_SpectatorBinds_C.ExecuteUbergraph_WBP_SpectatorBinds");

	UWBP_SpectatorBinds_C_ExecuteUbergraph_WBP_SpectatorBinds_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
