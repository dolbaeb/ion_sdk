#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.Get_SelectedBorder_BrushColor_1
struct UWBP_PlayerBoxModeTab_C_Get_SelectedBorder_BrushColor_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.PreConstruct
struct UWBP_PlayerBoxModeTab_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature
struct UWBP_PlayerBoxModeTab_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.ExecuteUbergraph_WBP_PlayerBoxModeTab
struct UWBP_PlayerBoxModeTab_C_ExecuteUbergraph_WBP_PlayerBoxModeTab_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.OnModeButtonClicked__DelegateSignature
struct UWBP_PlayerBoxModeTab_C_OnModeButtonClicked__DelegateSignature_Params
{
	int                                                Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
