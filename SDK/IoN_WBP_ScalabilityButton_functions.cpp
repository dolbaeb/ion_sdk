// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.EvaluateIndex
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// int                            ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

int UWBP_ScalabilityButton_C::EvaluateIndex()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.EvaluateIndex");

	UWBP_ScalabilityButton_C_EvaluateIndex_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.GetCheckedState_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ECheckBoxState                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ECheckBoxState UWBP_ScalabilityButton_C::GetCheckedState_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.GetCheckedState_1");

	UWBP_ScalabilityButton_C_GetCheckedState_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScalabilityButton_C::BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_ScalabilityButton_C_BndEvt__CheckBox_73_K2Node_ComponentBoundEvent_13_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.ExecuteUbergraph_WBP_ScalabilityButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScalabilityButton_C::ExecuteUbergraph_WBP_ScalabilityButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScalabilityButton.WBP_ScalabilityButton_C.ExecuteUbergraph_WBP_ScalabilityButton");

	UWBP_ScalabilityButton_C_ExecuteUbergraph_WBP_ScalabilityButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
