// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_NewItemReceived.WBP_NewItemReceived_C.SetSteamItem
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_NewItemReceived_C::SetSteamItem(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_NewItemReceived.WBP_NewItemReceived_C.SetSteamItem");

	UWBP_NewItemReceived_C_SetSteamItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_NewItemReceived.WBP_NewItemReceived_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_NewItemReceived_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_NewItemReceived.WBP_NewItemReceived_C.Construct");

	UWBP_NewItemReceived_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_NewItemReceived.WBP_NewItemReceived_C.BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature
// (BlueprintEvent)

void UWBP_NewItemReceived_C::BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_NewItemReceived.WBP_NewItemReceived_C.BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature");

	UWBP_NewItemReceived_C_BndEvt__WBP_FullscreenSkinPreview_K2Node_ComponentBoundEvent_23_CloseBtnPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_NewItemReceived.WBP_NewItemReceived_C.ExecuteUbergraph_WBP_NewItemReceived
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_NewItemReceived_C::ExecuteUbergraph_WBP_NewItemReceived(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_NewItemReceived.WBP_NewItemReceived_C.ExecuteUbergraph_WBP_NewItemReceived");

	UWBP_NewItemReceived_C_ExecuteUbergraph_WBP_NewItemReceived_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
