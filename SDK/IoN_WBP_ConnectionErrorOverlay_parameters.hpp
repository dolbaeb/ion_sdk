#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
struct UWBP_ConnectionErrorOverlay_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.ExecuteUbergraph_WBP_ConnectionErrorOverlay
struct UWBP_ConnectionErrorOverlay_C_ExecuteUbergraph_WBP_ConnectionErrorOverlay_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.OnRetryClicked__DelegateSignature
struct UWBP_ConnectionErrorOverlay_C_OnRetryClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
