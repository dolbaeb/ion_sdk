#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_DesertEagle.ABP_DesertEagle_C
// 0x0211 (0x05E1 - 0x03D0)
class UABP_DesertEagle_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_TwoWayBlend                       AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4;// 0x03D8(0x0078)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_86C4812B4D7D4FC2232DACB72683DA7A;      // 0x0450(0x0068)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_F0635F634BA20673DE6233BFA3E64266;// 0x04B8(0x0070)
	struct FAnimNode_Root                              AnimGraphNode_Root_44A29A5046F83A3A7EDDF88576E23019;      // 0x0528(0x0048)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_1E9EA635413CED00206103963A8A9634;// 0x0570(0x0070)
	bool                                               bRoundChambered;                                          // 0x05E0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_DesertEagle.ABP_DesertEagle_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_DesertEagle_AnimGraphNode_TwoWayBlend_554A3D064AE4A3FF1985A88BAF8153F4();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void ExecuteUbergraph_ABP_DesertEagle(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
