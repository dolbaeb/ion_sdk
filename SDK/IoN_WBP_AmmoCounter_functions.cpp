// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_AmmoCounter_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnMouseButtonDown");

	UWBP_AmmoCounter_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_AmmoCounter.WBP_AmmoCounter_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_AmmoCounter_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.Construct");

	UWBP_AmmoCounter_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AmmoCounter.WBP_AmmoCounter_C.On Ammo Changed
// (BlueprintCallable, BlueprintEvent)

void UWBP_AmmoCounter_C::On_Ammo_Changed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.On Ammo Changed");

	UWBP_AmmoCounter_C_On_Ammo_Changed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnWearablesChanged
// (BlueprintCallable, BlueprintEvent)

void UWBP_AmmoCounter_C::OnWearablesChanged()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.OnWearablesChanged");

	UWBP_AmmoCounter_C_OnWearablesChanged_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AmmoCounter.WBP_AmmoCounter_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AmmoCounter_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.PreConstruct");

	UWBP_AmmoCounter_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AmmoCounter.WBP_AmmoCounter_C.ExecuteUbergraph_WBP_AmmoCounter
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AmmoCounter_C::ExecuteUbergraph_WBP_AmmoCounter(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AmmoCounter.WBP_AmmoCounter_C.ExecuteUbergraph_WBP_AmmoCounter");

	UWBP_AmmoCounter_C_ExecuteUbergraph_WBP_AmmoCounter_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
