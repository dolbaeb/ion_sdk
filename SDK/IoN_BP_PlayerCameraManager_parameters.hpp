#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.PhotographyCameraModify
struct ABP_PlayerCameraManager_C_PhotographyCameraModify_Params
{
	struct FVector*                                    NewCameraLocation;                                        // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FVector*                                    PreviousCameraLocation;                                   // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FVector*                                    OriginalCameraLocation;                                   // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FVector                                     ResultCameraLocation;                                     // (Parm, OutParm, IsPlainOldData)
};

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.UserConstructionScript
struct ABP_PlayerCameraManager_C_UserConstructionScript_Params
{
};

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionStart
struct ABP_PlayerCameraManager_C_OnPhotographySessionStart_Params
{
};

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.OnPhotographySessionEnd
struct ABP_PlayerCameraManager_C_OnPhotographySessionEnd_Params
{
};

// Function BP_PlayerCameraManager.BP_PlayerCameraManager_C.ExecuteUbergraph_BP_PlayerCameraManager
struct ABP_PlayerCameraManager_C_ExecuteUbergraph_BP_PlayerCameraManager_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
