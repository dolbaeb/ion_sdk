// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.SetItemPrice
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Price                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_StoreItem_Weapon_C::SetItemPrice(const struct FString& Price)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.SetItemPrice");

	UWBP_StoreItem_Weapon_C_SetItemPrice_Params params;
	params.Price = Price;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_StoreItem_Weapon_C::BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature");

	UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_StoreItem_Weapon_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PreConstruct");

	UWBP_StoreItem_Weapon_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_StoreItem_Weapon_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.Construct");

	UWBP_StoreItem_Weapon_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ExecuteUbergraph_WBP_StoreItem_Weapon
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_StoreItem_Weapon_C::ExecuteUbergraph_WBP_StoreItem_Weapon(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ExecuteUbergraph_WBP_StoreItem_Weapon");

	UWBP_StoreItem_Weapon_C_ExecuteUbergraph_WBP_StoreItem_Weapon_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ViewItemDetails__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_StoreItem_Weapon_C::ViewItemDetails__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ViewItemDetails__DelegateSignature");

	UWBP_StoreItem_Weapon_C_ViewItemDetails__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PurchaseItem__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UIONSteamItem*           Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_StoreItem_Weapon_C::PurchaseItem__DelegateSignature(class UIONSteamItem* Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PurchaseItem__DelegateSignature");

	UWBP_StoreItem_Weapon_C_PurchaseItem__DelegateSignature_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
