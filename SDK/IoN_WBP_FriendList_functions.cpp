// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_FriendList.WBP_FriendList_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_FriendList_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.OnMouseButtonDown");

	UWBP_FriendList_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_1_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_FriendList_C::Get_TextBlock_1_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_1_Text_1");

	UWBP_FriendList_C_Get_TextBlock_1_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_0_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_FriendList_C::Get_TextBlock_0_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_0_Text_1");

	UWBP_FriendList_C_Get_TextBlock_0_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_FriendList.WBP_FriendList_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_FriendList_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.GetText_1");

	UWBP_FriendList_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_FriendList.WBP_FriendList_C.OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FBlueprintOnlineFriend> FriendsList                    (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_FriendList_C::OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2(TArray<struct FBlueprintOnlineFriend> FriendsList)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2");

	UWBP_FriendList_C_OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2_Params params;
	params.FriendsList = FriendsList;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<struct FBlueprintOnlineFriend> FriendsList                    (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_FriendList_C::OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2(TArray<struct FBlueprintOnlineFriend> FriendsList)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2");

	UWBP_FriendList_C_OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2_Params params;
	params.FriendsList = FriendsList;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_FriendList_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.Construct");

	UWBP_FriendList_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.UpdateFriendsList
// (BlueprintCallable, BlueprintEvent)

void UWBP_FriendList_C::UpdateFriendsList()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.UpdateFriendsList");

	UWBP_FriendList_C_UpdateFriendsList_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.AddPartyBtnPressed
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FBlueprintOnlineFriend  OnlineFriend                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_FriendList_C::AddPartyBtnPressed(const struct FBlueprintOnlineFriend& OnlineFriend)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.AddPartyBtnPressed");

	UWBP_FriendList_C_AddPartyBtnPressed_Params params;
	params.OnlineFriend = OnlineFriend;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_FriendList_C::BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature");

	UWBP_FriendList_C_BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.OnUpdateFriendList
// (Event, Public, BlueprintEvent)

void UWBP_FriendList_C::OnUpdateFriendList()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.OnUpdateFriendList");

	UWBP_FriendList_C_OnUpdateFriendList_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.ExecuteUbergraph_WBP_FriendList
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_FriendList_C::ExecuteUbergraph_WBP_FriendList(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.ExecuteUbergraph_WBP_FriendList");

	UWBP_FriendList_C_ExecuteUbergraph_WBP_FriendList_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_FriendList.WBP_FriendList_C.AddPartyMember__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FBlueprintOnlineFriend  OnlineFriend                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_FriendList_C::AddPartyMember__DelegateSignature(const struct FBlueprintOnlineFriend& OnlineFriend)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_FriendList.WBP_FriendList_C.AddPartyMember__DelegateSignature");

	UWBP_FriendList_C_AddPartyMember__DelegateSignature_Params params;
	params.OnlineFriend = OnlineFriend;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
