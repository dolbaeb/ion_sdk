// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetPlayercardModeText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetPlayercardModeText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetPlayercardModeText");

	UWBP_SpectatorOptions_C_GetPlayercardModeText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonUp
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SpectatorOptions_C::OnMouseButtonUp(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonUp");

	UWBP_SpectatorOptions_C_OnMouseButtonUp_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SpectatorOptions_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseButtonDown");

	UWBP_SpectatorOptions_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseWheel
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SpectatorOptions_C::OnMouseWheel(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnMouseWheel");

	UWBP_SpectatorOptions_C_OnMouseWheel_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Get Spectator Background Color
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_SpectatorOptions_C::Get_Spectator_Background_Color()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Get Spectator Background Color");

	UWBP_SpectatorOptions_C_Get_Spectator_Background_Color_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ShowMinimapOptionText
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::ShowMinimapOptionText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ShowMinimapOptionText");

	UWBP_SpectatorOptions_C_ShowMinimapOptionText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Set Map Open
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Open_Map_                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::Set_Map_Open(bool Open_Map_)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Set Map Open");

	UWBP_SpectatorOptions_C_Set_Map_Open_Params params;
	params.Open_Map_ = Open_Map_;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnKeyDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_SpectatorOptions_C::OnKeyDown(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.OnKeyDown");

	UWBP_SpectatorOptions_C_OnKeyDown_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.SetMinimapOpen
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Maximize                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::SetMinimapOpen(bool Maximize)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.SetMinimapOpen");

	UWBP_SpectatorOptions_C_SetMinimapOpen_Params params;
	params.Maximize = Maximize;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerState
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class APlayerState*            State                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::GetSpectatedPlayerState(class APlayerState** State)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerState");

	UWBP_SpectatorOptions_C_GetSpectatedPlayerState_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (State != nullptr)
		*State = params.State;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateTextVisibility
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_SpectatorOptions_C::GetSpectateTextVisibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateTextVisibility");

	UWBP_SpectatorOptions_C_GetSpectateTextVisibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateModeText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetSpectateModeText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectateModeText");

	UWBP_SpectatorOptions_C_GetSpectateModeText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetMapVisibilityText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetMapVisibilityText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetMapVisibilityText");

	UWBP_SpectatorOptions_C_GetMapVisibilityText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerName
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetSpectatedPlayerName()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetSpectatedPlayerName");

	UWBP_SpectatorOptions_C_GetSpectatedPlayerName_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetBlurText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetBlurText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetBlurText");

	UWBP_SpectatorOptions_C_GetBlurText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFocalDistanceText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetFocalDistanceText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFocalDistanceText");

	UWBP_SpectatorOptions_C_GetFocalDistanceText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFOVText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetFOVText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFOVText");

	UWBP_SpectatorOptions_C_GetFOVText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFlySpeedText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_SpectatorOptions_C::GetFlySpeedText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.GetFlySpeedText");

	UWBP_SpectatorOptions_C_GetFlySpeedText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Option Changed
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            OptionIndex                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::On_Option_Changed(int OptionIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Option Changed");

	UWBP_SpectatorOptions_C_On_Option_Changed_Params params;
	params.OptionIndex = OptionIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Select Option
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTextBlock*              Text                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_SpectatorOptions_C::Select_Option(class UTextBlock* Text)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Select Option");

	UWBP_SpectatorOptions_C_Select_Option_Params params;
	params.Text = Text;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SpectatorOptions_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Construct");

	UWBP_SpectatorOptions_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Scrolled
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Scroll_Delta                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::On_Scrolled(float Scroll_Delta)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.On Scrolled");

	UWBP_SpectatorOptions_C_On_Scrolled_Params params;
	params.Scroll_Delta = Scroll_Delta;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.Tick");

	UWBP_SpectatorOptions_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ExecuteUbergraph_WBP_SpectatorOptions
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorOptions_C::ExecuteUbergraph_WBP_SpectatorOptions(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorOptions.WBP_SpectatorOptions_C.ExecuteUbergraph_WBP_SpectatorOptions");

	UWBP_SpectatorOptions_C_ExecuteUbergraph_WBP_SpectatorOptions_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
