#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.UserConstructionScript
struct ABP_SpectatorInfoWidget_C_UserConstructionScript_Params
{
};

// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.SetupWidget
struct ABP_SpectatorInfoWidget_C_SetupWidget_Params
{
};

// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.OnViewModeChanged
struct ABP_SpectatorInfoWidget_C_OnViewModeChanged_Params
{
	bool*                                              bMinimal;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_SpectatorInfoWidget.BP_SpectatorInfoWidget_C.ExecuteUbergraph_BP_SpectatorInfoWidget
struct ABP_SpectatorInfoWidget_C_ExecuteUbergraph_BP_SpectatorInfoWidget_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
