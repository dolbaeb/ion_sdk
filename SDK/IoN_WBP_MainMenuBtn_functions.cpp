// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenuBtn_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.PreConstruct");

	UWBP_MainMenuBtn_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuBtn_C::BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature");

	UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuBtn_C::BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MainMenuBtn_C::BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature");

	UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.ExecuteUbergraph_WBP_MainMenuBtn
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MainMenuBtn_C::ExecuteUbergraph_WBP_MainMenuBtn(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.ExecuteUbergraph_WBP_MainMenuBtn");

	UWBP_MainMenuBtn_C_ExecuteUbergraph_WBP_MainMenuBtn_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_MainMenuBtn_C::BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BtnClicked__DelegateSignature");

	UWBP_MainMenuBtn_C_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
