// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_KillFeed.WBP_KillFeed_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_KillFeed_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeed.WBP_KillFeed_C.Construct");

	UWBP_KillFeed_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeed.WBP_KillFeed_C.RemoveEntry
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_KillFeedEntry_C*    Entry                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_KillFeed_C::RemoveEntry(class UWBP_KillFeedEntry_C* Entry)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeed.WBP_KillFeed_C.RemoveEntry");

	UWBP_KillFeed_C_RemoveEntry_Params params;
	params.Entry = Entry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDied
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_KillFeed_C::OnPlayerDied(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDied");

	UWBP_KillFeed_C_OnPlayerDied_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDowned
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FHitInfo                LastHitInfo                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_KillFeed_C::OnPlayerDowned(const struct FHitInfo& LastHitInfo)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeed.WBP_KillFeed_C.OnPlayerDowned");

	UWBP_KillFeed_C_OnPlayerDowned_Params params;
	params.LastHitInfo = LastHitInfo;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeed.WBP_KillFeed_C.ExecuteUbergraph_WBP_KillFeed
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_KillFeed_C::ExecuteUbergraph_WBP_KillFeed(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeed.WBP_KillFeed_C.ExecuteUbergraph_WBP_KillFeed");

	UWBP_KillFeed_C_ExecuteUbergraph_WBP_KillFeed_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
