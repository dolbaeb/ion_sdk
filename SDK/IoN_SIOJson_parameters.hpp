#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function SIOJson.SIOJLibrary.StructToJsonObject
struct USIOJLibrary_StructToJsonObject_Params
{
	class UProperty*                                   AnyStruct;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.StringToJsonValueArray
struct USIOJLibrary_StringToJsonValueArray_Params
{
	struct FString                                     JSONString;                                               // (Parm, ZeroConstructor)
	TArray<class USIOJsonValue*>                       OutJsonValueArray;                                        // (Parm, OutParm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.PercentEncode
struct USIOJLibrary_PercentEncode_Params
{
	struct FString                                     Source;                                                   // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJLibrary.JsonObjectToStruct
struct USIOJLibrary_JsonObjectToStruct_Params
{
	class USIOJsonObject*                              JsonObject;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UProperty*                                   AnyStruct;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_StringToJsonValue
struct USIOJLibrary_Conv_StringToJsonValue_Params
{
	struct FString                                     inString;                                                 // (Parm, ZeroConstructor)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_JsonValueToJsonObject
struct USIOJLibrary_Conv_JsonValueToJsonObject_Params
{
	class USIOJsonValue*                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_JsonValueToInt
struct USIOJLibrary_Conv_JsonValueToInt_Params
{
	class USIOJsonValue*                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_JsonValueToFloat
struct USIOJLibrary_Conv_JsonValueToFloat_Params
{
	class USIOJsonValue*                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_JsonValueToBytes
struct USIOJLibrary_Conv_JsonValueToBytes_Params
{
	class USIOJsonValue*                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<unsigned char>                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJLibrary.Conv_JsonValueToBool
struct USIOJLibrary_Conv_JsonValueToBool_Params
{
	class USIOJsonValue*                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_JsonObjectToString
struct USIOJLibrary_Conv_JsonObjectToString_Params
{
	class USIOJsonObject*                              InObject;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJLibrary.Conv_JsonObjectToJsonValue
struct USIOJLibrary_Conv_JsonObjectToJsonValue_Params
{
	class USIOJsonObject*                              InObject;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_IntToJsonValue
struct USIOJLibrary_Conv_IntToJsonValue_Params
{
	int                                                inInt;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_FloatToJsonValue
struct USIOJLibrary_Conv_FloatToJsonValue_Params
{
	float                                              InFloat;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_BytesToJsonValue
struct USIOJLibrary_Conv_BytesToJsonValue_Params
{
	TArray<unsigned char>                              InBytes;                                                  // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_BoolToJsonValue
struct USIOJLibrary_Conv_BoolToJsonValue_Params
{
	bool                                               InBool;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.Conv_ArrayToJsonValue
struct USIOJLibrary_Conv_ArrayToJsonValue_Params
{
	TArray<class USIOJsonValue*>                       inArray;                                                  // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJLibrary.CallURL
struct USIOJLibrary_CallURL_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     URL;                                                      // (Parm, ZeroConstructor)
	ESIORequestVerb                                    Verb;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	ESIORequestContentType                             ContentType;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonObject*                              SIOJJson;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FScriptDelegate                             Callback;                                                 // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJLibrary.Base64Encode
struct USIOJLibrary_Base64Encode_Params
{
	struct FString                                     Source;                                                   // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJLibrary.Base64Decode
struct USIOJLibrary_Base64Decode_Params
{
	struct FString                                     Source;                                                   // (Parm, ZeroConstructor)
	struct FString                                     Dest;                                                     // (Parm, OutParm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.SetVerb
struct USIOJRequestJSON_SetVerb_Params
{
	ESIORequestVerb                                    Verb;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.SetResponseObject
struct USIOJRequestJSON_SetResponseObject_Params
{
	class USIOJsonObject*                              JsonObject;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.SetRequestObject
struct USIOJRequestJSON_SetRequestObject_Params
{
	class USIOJsonObject*                              JsonObject;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.SetHeader
struct USIOJRequestJSON_SetHeader_Params
{
	struct FString                                     HeaderName;                                               // (Parm, ZeroConstructor)
	struct FString                                     HeaderValue;                                              // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJRequestJSON.SetCustomVerb
struct USIOJRequestJSON_SetCustomVerb_Params
{
	struct FString                                     Verb;                                                     // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJRequestJSON.SetContentType
struct USIOJRequestJSON_SetContentType_Params
{
	ESIORequestContentType                             ContentType;                                              // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.SetBinaryRequestContent
struct USIOJRequestJSON_SetBinaryRequestContent_Params
{
	TArray<unsigned char>                              Content;                                                  // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJRequestJSON.SetBinaryContentType
struct USIOJRequestJSON_SetBinaryContentType_Params
{
	struct FString                                     ContentType;                                              // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJRequestJSON.ResetResponseData
struct USIOJRequestJSON_ResetResponseData_Params
{
};

// Function SIOJson.SIOJRequestJSON.ResetRequestData
struct USIOJRequestJSON_ResetRequestData_Params
{
};

// Function SIOJson.SIOJRequestJSON.ResetData
struct USIOJRequestJSON_ResetData_Params
{
};

// Function SIOJson.SIOJRequestJSON.RemoveTag
struct USIOJRequestJSON_RemoveTag_Params
{
	struct FName                                       Tag;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.ProcessURL
struct USIOJRequestJSON_ProcessURL_Params
{
	struct FString                                     URL;                                                      // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJRequestJSON.HasTag
struct USIOJRequestJSON_HasTag_Params
{
	struct FName                                       Tag;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.GetUrl
struct USIOJRequestJSON_GetUrl_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJRequestJSON.GetStatus
struct USIOJRequestJSON_GetStatus_Params
{
	ESIORequestStatus                                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.GetResponseObject
struct USIOJRequestJSON_GetResponseObject_Params
{
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.GetResponseHeader
struct USIOJRequestJSON_GetResponseHeader_Params
{
	struct FString                                     HeaderName;                                               // (ConstParm, Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJRequestJSON.GetResponseCode
struct USIOJRequestJSON_GetResponseCode_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.GetRequestObject
struct USIOJRequestJSON_GetRequestObject_Params
{
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.GetAllResponseHeaders
struct USIOJRequestJSON_GetAllResponseHeaders_Params
{
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJRequestJSON.ConstructRequestExt
struct USIOJRequestJSON_ConstructRequestExt_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	ESIORequestVerb                                    Verb;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	ESIORequestContentType                             ContentType;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJRequestJSON*                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.ConstructRequest
struct USIOJRequestJSON_ConstructRequest_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJRequestJSON*                            ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJRequestJSON.Cancel
struct USIOJRequestJSON_Cancel_Params
{
};

// Function SIOJson.SIOJRequestJSON.ApplyURL
struct USIOJRequestJSON_ApplyURL_Params
{
	struct FString                                     URL;                                                      // (Parm, ZeroConstructor)
	class USIOJsonObject*                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FLatentActionInfo                           LatentInfo;                                               // (Parm)
};

// Function SIOJson.SIOJRequestJSON.AddTag
struct USIOJRequestJSON_AddTag_Params
{
	struct FName                                       Tag;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.SetStringField
struct USIOJsonObject_SetStringField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	struct FString                                     StringValue;                                              // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJsonObject.SetStringArrayField
struct USIOJsonObject_SetStringArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<struct FString>                             StringArray;                                              // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.SetObjectField
struct USIOJsonObject_SetObjectField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	class USIOJsonObject*                              JsonObject;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.SetObjectArrayField
struct USIOJsonObject_SetObjectArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<class USIOJsonObject*>                      ObjectArray;                                              // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.SetNumberField
struct USIOJsonObject_SetNumberField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	float                                              Number;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.SetNumberArrayField
struct USIOJsonObject_SetNumberArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<float>                                      NumberArray;                                              // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.SetField
struct USIOJsonObject_SetField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	class USIOJsonValue*                               JsonValue;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.SetBoolField
struct USIOJsonObject_SetBoolField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	bool                                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.SetBoolArrayField
struct USIOJsonObject_SetBoolArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<bool>                                       BoolArray;                                                // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.SetBinaryField
struct USIOJsonObject_SetBinaryField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<unsigned char>                              Bytes;                                                    // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.SetArrayField
struct USIOJsonObject_SetArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<class USIOJsonValue*>                       inArray;                                                  // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function SIOJson.SIOJsonObject.Reset
struct USIOJsonObject_Reset_Params
{
};

// Function SIOJson.SIOJsonObject.RemoveField
struct USIOJsonObject_RemoveField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
};

// Function SIOJson.SIOJsonObject.MergeJsonObject
struct USIOJsonObject_MergeJsonObject_Params
{
	class USIOJsonObject*                              InJsonObject;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Overwrite;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.HasField
struct USIOJsonObject_HasField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.GetStringField
struct USIOJsonObject_GetStringField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetStringArrayField
struct USIOJsonObject_GetStringArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetObjectField
struct USIOJsonObject_GetObjectField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.GetObjectArrayField
struct USIOJsonObject_GetObjectArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<class USIOJsonObject*>                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetNumberField
struct USIOJsonObject_GetNumberField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.GetNumberArrayField
struct USIOJsonObject_GetNumberArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<float>                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetFieldNames
struct USIOJsonObject_GetFieldNames_Params
{
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetField
struct USIOJsonObject_GetField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.GetBoolField
struct USIOJsonObject_GetBoolField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.GetBoolArrayField
struct USIOJsonObject_GetBoolArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<bool>                                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetBinaryField
struct USIOJsonObject_GetBinaryField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<unsigned char>                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.GetArrayField
struct USIOJsonObject_GetArrayField_Params
{
	struct FString                                     FieldName;                                                // (Parm, ZeroConstructor)
	TArray<class USIOJsonValue*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.EncodeJsonToSingleString
struct USIOJsonObject_EncodeJsonToSingleString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.EncodeJson
struct USIOJsonObject_EncodeJson_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonObject.DecodeJson
struct USIOJsonObject_DecodeJson_Params
{
	struct FString                                     JSONString;                                               // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonObject.ConstructJsonObject
struct USIOJsonObject_ConstructJsonObject_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ValueFromJsonString
struct USIOJsonValue_ValueFromJsonString_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     StringValue;                                              // (Parm, ZeroConstructor)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.IsNull
struct USIOJsonValue_IsNull_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.GetTypeString
struct USIOJsonValue_GetTypeString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonValue.GetType
struct USIOJsonValue_GetType_Params
{
	TEnumAsByte<ESIOJson>                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.EncodeJson
struct USIOJsonValue_EncodeJson_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueString
struct USIOJsonValue_ConstructJsonValueString_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     StringValue;                                              // (Parm, ZeroConstructor)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueObject
struct USIOJsonValue_ConstructJsonValueObject_Params
{
	class USIOJsonObject*                              JsonObject;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueNumber
struct USIOJsonValue_ConstructJsonValueNumber_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Number;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueBool
struct USIOJsonValue_ConstructJsonValueBool_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InValue;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueBinary
struct USIOJsonValue_ConstructJsonValueBinary_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<unsigned char>                              ByteArray;                                                // (Parm, ZeroConstructor)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.ConstructJsonValueArray
struct USIOJsonValue_ConstructJsonValueArray_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<class USIOJsonValue*>                       inArray;                                                  // (ConstParm, Parm, OutParm, ZeroConstructor, ReferenceParm)
	class USIOJsonValue*                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.AsString
struct USIOJsonValue_AsString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonValue.AsObject
struct USIOJsonValue_AsObject_Params
{
	class USIOJsonObject*                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.AsNumber
struct USIOJsonValue_AsNumber_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.AsBool
struct USIOJsonValue_AsBool_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function SIOJson.SIOJsonValue.AsBinary
struct USIOJsonValue_AsBinary_Params
{
	TArray<unsigned char>                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function SIOJson.SIOJsonValue.AsArray
struct USIOJsonValue_AsArray_Params
{
	TArray<class USIOJsonValue*>                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
