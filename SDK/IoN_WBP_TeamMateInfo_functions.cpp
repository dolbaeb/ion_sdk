// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Get_TextBlock_Kills_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_TeamMateInfo_C::Get_TextBlock_Kills_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Get_TextBlock_Kills_Text_1");

	UWBP_TeamMateInfo_C_Get_TextBlock_Kills_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Update Muted
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_TeamMateInfo_C::Update_Muted()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Update Muted");

	UWBP_TeamMateInfo_C_Update_Muted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_TeamMateInfo_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseButtonDown");

	UWBP_TeamMateInfo_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TeamMateInfo_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Tick");

	UWBP_TeamMateInfo_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseEnter
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_TeamMateInfo_C::OnMouseEnter(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseEnter");

	UWBP_TeamMateInfo_C_OnMouseEnter_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseLeave
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintEvent)
// Parameters:
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_TeamMateInfo_C::OnMouseLeave(struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseLeave");

	UWBP_TeamMateInfo_C_OnMouseLeave_Params params;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Force Close
// (BlueprintCallable, BlueprintEvent)

void UWBP_TeamMateInfo_C::Force_Close()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Force Close");

	UWBP_TeamMateInfo_C_Force_Close_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnDowned
// (BlueprintCallable, BlueprintEvent)

void UWBP_TeamMateInfo_C::OnDowned()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnDowned");

	UWBP_TeamMateInfo_C_OnDowned_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_TeamMateInfo_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Construct");

	UWBP_TeamMateInfo_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Stop Animation
// (BlueprintCallable, BlueprintEvent)

void UWBP_TeamMateInfo_C::Stop_Animation()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Stop Animation");

	UWBP_TeamMateInfo_C_Stop_Animation_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.ExecuteUbergraph_WBP_TeamMateInfo
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TeamMateInfo_C::ExecuteUbergraph_WBP_TeamMateInfo(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.ExecuteUbergraph_WBP_TeamMateInfo");

	UWBP_TeamMateInfo_C_ExecuteUbergraph_WBP_TeamMateInfo_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnPlayerLeft__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_TeamMateInfo_C*     Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_TeamMateInfo_C::OnPlayerLeft__DelegateSignature(class UWBP_TeamMateInfo_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnPlayerLeft__DelegateSignature");

	UWBP_TeamMateInfo_C_OnPlayerLeft__DelegateSignature_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
