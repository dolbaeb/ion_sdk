#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Get_TextBlock_Kills_Text_1
struct UWBP_TeamMateInfo_C_Get_TextBlock_Kills_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Update Muted
struct UWBP_TeamMateInfo_C_Update_Muted_Params
{
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseButtonDown
struct UWBP_TeamMateInfo_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Tick
struct UWBP_TeamMateInfo_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseEnter
struct UWBP_TeamMateInfo_C_OnMouseEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnMouseLeave
struct UWBP_TeamMateInfo_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Force Close
struct UWBP_TeamMateInfo_C_Force_Close_Params
{
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnDowned
struct UWBP_TeamMateInfo_C_OnDowned_Params
{
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Construct
struct UWBP_TeamMateInfo_C_Construct_Params
{
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.Stop Animation
struct UWBP_TeamMateInfo_C_Stop_Animation_Params
{
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.ExecuteUbergraph_WBP_TeamMateInfo
struct UWBP_TeamMateInfo_C_ExecuteUbergraph_WBP_TeamMateInfo_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_TeamMateInfo.WBP_TeamMateInfo_C.OnPlayerLeft__DelegateSignature
struct UWBP_TeamMateInfo_C_OnPlayerLeft__DelegateSignature_Params
{
	class UWBP_TeamMateInfo_C*                         Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
