#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EChatChannel.EChatChannel
enum class EChatChannel : uint8_t
{
	EChatChannel__NewEnumerator0   = 0,
	EChatChannel__EChatChannel_MAX = 1
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
