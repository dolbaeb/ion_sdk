// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BPEvent_RefreshInventory
// (Event, Public, BlueprintEvent)

void UWBP_InventoryScreen_C::BPEvent_RefreshInventory()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryScreen.WBP_InventoryScreen_C.BPEvent_RefreshInventory");

	UWBP_InventoryScreen_C_BPEvent_RefreshInventory_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryScreen_C::BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_InventoryScreen_C_BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryScreen_C::BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryScreen.WBP_InventoryScreen_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_InventoryScreen_C_BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_2_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryScreen.WBP_InventoryScreen_C.ExecuteUbergraph_WBP_InventoryScreen
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryScreen_C::ExecuteUbergraph_WBP_InventoryScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryScreen.WBP_InventoryScreen_C.ExecuteUbergraph_WBP_InventoryScreen");

	UWBP_InventoryScreen_C_ExecuteUbergraph_WBP_InventoryScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
