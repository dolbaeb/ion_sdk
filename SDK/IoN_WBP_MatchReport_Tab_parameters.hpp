#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseButtonDown
struct UWBP_MatchReport_Tab_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Construct
struct UWBP_MatchReport_Tab_C_Construct_Params
{
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Show Placeholder
struct UWBP_MatchReport_Tab_C_Show_Placeholder_Params
{
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Activate
struct UWBP_MatchReport_Tab_C_Activate_Params
{
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseEnter
struct UWBP_MatchReport_Tab_C_OnMouseEnter_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.OnMouseLeave
struct UWBP_MatchReport_Tab_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Select
struct UWBP_MatchReport_Tab_C_Select_Params
{
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.PreConstruct
struct UWBP_MatchReport_Tab_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.On Unhover
struct UWBP_MatchReport_Tab_C_On_Unhover_Params
{
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.ExecuteUbergraph_WBP_MatchReport_Tab
struct UWBP_MatchReport_Tab_C_ExecuteUbergraph_WBP_MatchReport_Tab_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MatchReport_Tab.WBP_MatchReport_Tab_C.Clicked__DelegateSignature
struct UWBP_MatchReport_Tab_C_Clicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
