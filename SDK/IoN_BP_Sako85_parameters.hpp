#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_Sako85.BP_Sako85_C.GetPickupLocation
struct ABP_Sako85_C_GetPickupLocation_Params
{
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function BP_Sako85.BP_Sako85_C.UserConstructionScript
struct ABP_Sako85_C_UserConstructionScript_Params
{
};

// Function BP_Sako85.BP_Sako85_C.ReceiveTick
struct ABP_Sako85_C_ReceiveTick_Params
{
	float*                                             DeltaSeconds;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_Sako85.BP_Sako85_C.ExecuteUbergraph_BP_Sako85
struct ABP_Sako85_C_ExecuteUbergraph_BP_Sako85_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
