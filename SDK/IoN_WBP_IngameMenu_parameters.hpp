#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_IngameMenu.WBP_IngameMenu_C.OnMouseButtonDown_1
struct UWBP_IngameMenu_C_OnMouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.OnKeyDown
struct UWBP_IngameMenu_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.OnSettingsMenuClosed
struct UWBP_IngameMenu_C_OnSettingsMenuClosed_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature
struct UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature
struct UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature
struct UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.OnAnimationFinished
struct UWBP_IngameMenu_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature
struct UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.Construct
struct UWBP_IngameMenu_C_Construct_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.On Close Menu
struct UWBP_IngameMenu_C_On_Close_Menu_Params
{
};

// Function WBP_IngameMenu.WBP_IngameMenu_C.ExecuteUbergraph_WBP_IngameMenu
struct UWBP_IngameMenu_C_ExecuteUbergraph_WBP_IngameMenu_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
