// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.MakeMessageID
// (Private, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FGuid                   MessageId                      (Parm, OutParm, IsPlainOldData)

void UWBP_ChatBoxNew_C::MakeMessageID(struct FGuid* MessageId)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.MakeMessageID");

	UWBP_ChatBoxNew_C_MakeMessageID_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MessageId != nullptr)
		*MessageId = params.MessageId;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ChatBoxNew_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.Construct");

	UWBP_ChatBoxNew_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBoxNew_C::BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature");

	UWBP_ChatBoxNew_C_BndEvt__EditableTextBox_456_K2Node_ComponentBoundEvent_1_OnEditableTextBoxCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearEnteredMessage
// (BlueprintCallable, BlueprintEvent)

void UWBP_ChatBoxNew_C::ClearEnteredMessage()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearEnteredMessage");

	UWBP_ChatBoxNew_C_ClearEnteredMessage_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.SetChannelMessages
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EChatChannel>      Channel                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// TArray<struct FSChannelMessage> Messages                       (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)

void UWBP_ChatBoxNew_C::SetChannelMessages(TEnumAsByte<EChatChannel> Channel, TArray<struct FSChannelMessage> Messages)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.SetChannelMessages");

	UWBP_ChatBoxNew_C_SetChannelMessages_Params params;
	params.Channel = Channel;
	params.Messages = Messages;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearChannelMessages
// (BlueprintCallable, BlueprintEvent)

void UWBP_ChatBoxNew_C::ClearChannelMessages()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ClearChannelMessages");

	UWBP_ChatBoxNew_C_ClearChannelMessages_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ExecuteUbergraph_WBP_ChatBoxNew
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBoxNew_C::ExecuteUbergraph_WBP_ChatBoxNew(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.ExecuteUbergraph_WBP_ChatBoxNew");

	UWBP_ChatBoxNew_C_ExecuteUbergraph_WBP_ChatBoxNew_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.OnChannelChanged__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EChatChannel>      Channel                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBoxNew_C::OnChannelChanged__DelegateSignature(TEnumAsByte<EChatChannel> Channel)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBoxNew.WBP_ChatBoxNew_C.OnChannelChanged__DelegateSignature");

	UWBP_ChatBoxNew_C_OnChannelChanged__DelegateSignature_Params params;
	params.Channel = Channel;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
