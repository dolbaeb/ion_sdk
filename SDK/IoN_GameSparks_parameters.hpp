#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function GameSparks.GameSparksComponent.SetApiStage
struct UGameSparksComponent_SetApiStage_Params
{
	struct FString                                     stage;                                                    // (Parm, ZeroConstructor)
};

// Function GameSparks.GameSparksComponent.SetApiDomain
struct UGameSparksComponent_SetApiDomain_Params
{
	struct FString                                     domain;                                                   // (Parm, ZeroConstructor)
};

// Function GameSparks.GameSparksComponent.SetApiCredential
struct UGameSparksComponent_SetApiCredential_Params
{
	struct FString                                     credential;                                               // (Parm, ZeroConstructor)
};

// DelegateFunction GameSparks.GameSparksComponent.OnGameSparksLogEvent__DelegateSignature
struct UGameSparksComponent_OnGameSparksLogEvent__DelegateSignature_Params
{
	struct FString                                     logMessage;                                               // (Parm, ZeroConstructor)
};

// DelegateFunction GameSparks.GameSparksComponent.OnGameSparksAvailable__DelegateSignature
struct UGameSparksComponent_OnGameSparksAvailable__DelegateSignature_Params
{
	bool                                               available;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function GameSparks.GameSparksComponent.Logout
struct UGameSparksComponent_Logout_Params
{
};

// Function GameSparks.GameSparksComponent.IsAvailable
struct UGameSparksComponent_IsAvailable_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksComponent.IsAuthenticated
struct UGameSparksComponent_IsAuthenticated_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksComponent.Disconnect
struct UGameSparksComponent_Disconnect_Params
{
};

// Function GameSparks.GameSparksComponent.Connect
struct UGameSparksComponent_Connect_Params
{
	struct FString                                     apikey;                                                   // (Parm, ZeroConstructor)
	struct FString                                     secret;                                                   // (Parm, ZeroConstructor)
	bool                                               previewServer;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               clearCachedAuthentication;                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function GameSparks.GameSparksLogEventData.SetString
struct UGameSparksLogEventData_SetString_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
	class UGameSparksLogEventData*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksLogEventData.SetObject
struct UGameSparksLogEventData_SetObject_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksLogEventData*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksLogEventData.SetNumber
struct UGameSparksLogEventData_SetNumber_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	int                                                Value;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksLogEventData*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksLogEventData.CreateGameSparksLogEventAttributes
struct UGameSparksLogEventData_CreateGameSparksLogEventAttributes_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksLogEventData*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksObject.SetApiStage
struct UGameSparksObject_SetApiStage_Params
{
	struct FString                                     stage;                                                    // (Parm, ZeroConstructor)
};

// Function GameSparks.GameSparksObject.SetApiDomain
struct UGameSparksObject_SetApiDomain_Params
{
	struct FString                                     domain;                                                   // (Parm, ZeroConstructor)
};

// Function GameSparks.GameSparksObject.SetApiCredential
struct UGameSparksObject_SetApiCredential_Params
{
	struct FString                                     credential;                                               // (Parm, ZeroConstructor)
};

// DelegateFunction GameSparks.GameSparksObject.OnGameSparksLogEvent__DelegateSignature
struct UGameSparksObject_OnGameSparksLogEvent__DelegateSignature_Params
{
	struct FString                                     logMessage;                                               // (Parm, ZeroConstructor)
};

// DelegateFunction GameSparks.GameSparksObject.OnGameSparksAvailable__DelegateSignature
struct UGameSparksObject_OnGameSparksAvailable__DelegateSignature_Params
{
	bool                                               available;                                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function GameSparks.GameSparksObject.Logout
struct UGameSparksObject_Logout_Params
{
};

// Function GameSparks.GameSparksObject.IsAvailable
struct UGameSparksObject_IsAvailable_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksObject.IsAuthenticated
struct UGameSparksObject_IsAuthenticated_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksObject.Disconnect
struct UGameSparksObject_Disconnect_Params
{
};

// Function GameSparks.GameSparksObject.Connect
struct UGameSparksObject_Connect_Params
{
	struct FString                                     apikey;                                                   // (Parm, ZeroConstructor)
	struct FString                                     secret;                                                   // (Parm, ZeroConstructor)
	bool                                               previewServer;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               clearCachedAuthentication;                                // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function GameSparks.GameSparksRequestArray.CreateGameSparksRequestArray
struct UGameSparksRequestArray_CreateGameSparksRequestArray_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.ToString
struct UGameSparksScriptData_ToString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.SetStringArray
struct UGameSparksScriptData_SetStringArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<struct FString>                             Value;                                                    // (ConstParm, Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetString
struct UGameSparksScriptData_SetString_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetNumberArray
struct UGameSparksScriptData_SetNumberArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<int>                                        Value;                                                    // (ConstParm, Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetNumber
struct UGameSparksScriptData_SetNumber_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	int                                                Value;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetGSDataArray
struct UGameSparksScriptData_SetGSDataArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<class UGameSparksScriptData*>               Value;                                                    // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetGSData
struct UGameSparksScriptData_SetGSData_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetFloatArray
struct UGameSparksScriptData_SetFloatArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<float>                                      Value;                                                    // (ConstParm, Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetFloat
struct UGameSparksScriptData_SetFloat_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	float                                              Value;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.SetBoolean
struct UGameSparksScriptData_SetBoolean_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               Value;                                                    // (ConstParm, Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.JSONString
struct UGameSparksScriptData_JSONString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.HasStringArray
struct UGameSparksScriptData_HasStringArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasString
struct UGameSparksScriptData_HasString_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasNumberArray
struct UGameSparksScriptData_HasNumberArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasNumber
struct UGameSparksScriptData_HasNumber_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasGSData
struct UGameSparksScriptData_HasGSData_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasFloatArray
struct UGameSparksScriptData_HasFloatArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasFloat
struct UGameSparksScriptData_HasFloat_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.HasBoolean
struct UGameSparksScriptData_HasBoolean_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.GetStringArray
struct UGameSparksScriptData_GetStringArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetString
struct UGameSparksScriptData_GetString_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetNumberArray
struct UGameSparksScriptData_GetNumberArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<int>                                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetNumber
struct UGameSparksScriptData_GetNumber_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.GetKeys
struct UGameSparksScriptData_GetKeys_Params
{
	TArray<struct FString>                             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetGSDataArray
struct UGameSparksScriptData_GetGSDataArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<class UGameSparksScriptData*>               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetGSData
struct UGameSparksScriptData_GetGSData_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.GetFloatArray
struct UGameSparksScriptData_GetFloatArray_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	TArray<float>                                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GameSparksScriptData.GetFloat
struct UGameSparksScriptData_GetFloat_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.GetBoolean
struct UGameSparksScriptData_GetBoolean_Params
{
	struct FString                                     Name;                                                     // (Parm, ZeroConstructor)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GameSparksScriptData.CreateGameSparksScriptData
struct UGameSparksScriptData_CreateGameSparksScriptData_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAcceptChallengeRequest.SendAcceptChallengeRequest
struct UGSAcceptChallengeRequest_SendAcceptChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAcceptChallengeRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAccountDetailsRequest.SendAccountDetailsRequest
struct UGSAccountDetailsRequest_SendAccountDetailsRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAccountDetailsRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAmazonBuyGoodsRequest.SendAmazonBuyGoodsRequest
struct UGSAmazonBuyGoodsRequest_SendAmazonBuyGoodsRequest_Params
{
	struct FString                                     AmazonUserId;                                             // (Parm, ZeroConstructor)
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     ReceiptId;                                                // (Parm, ZeroConstructor)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAmazonBuyGoodsRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAmazonConnectRequest.SendAmazonConnectRequest
struct UGSAmazonConnectRequest_SendAmazonConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAmazonConnectRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAnalyticsRequest.SendAnalyticsRequest
struct UGSAnalyticsRequest_SendAnalyticsRequest_Params
{
	class UGameSparksScriptData*                       Data;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               End;                                                      // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Key;                                                      // (Parm, ZeroConstructor)
	bool                                               Start;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAnalyticsRequest*                         ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAroundMeLeaderboardRequest.SendAroundMeLeaderboardRequest
struct UGSAroundMeLeaderboardRequest_SendAroundMeLeaderboardRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       CustomIdFilter;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               DontErrorOnNotSocial;                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     LeaderboardShortCode;                                     // (Parm, ZeroConstructor)
	bool                                               Social;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamIds;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAroundMeLeaderboardRequest*               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSAuthenticationRequest.SendAuthenticationRequest
struct UGSAuthenticationRequest_SendAuthenticationRequest_Params
{
	struct FString                                     Password;                                                 // (Parm, ZeroConstructor)
	struct FString                                     UserName;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSAuthenticationRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSBatchAdminRequest.SendBatchAdminRequest
struct UGSBatchAdminRequest_SendBatchAdminRequest_Params
{
	class UGameSparksRequestArray*                     PlayerIds;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Request;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSBatchAdminRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSBuyVirtualGoodsRequest.SendBuyVirtualGoodsRequest
struct UGSBuyVirtualGoodsRequest_SendBuyVirtualGoodsRequest_Params
{
	int                                                CurrencyType;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Quantity;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ShortCode;                                                // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSBuyVirtualGoodsRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSCancelBulkJobAdminRequest.SendCancelBulkJobAdminRequest
struct UGSCancelBulkJobAdminRequest_SendCancelBulkJobAdminRequest_Params
{
	class UGameSparksRequestArray*                     BulkJobIds;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSCancelBulkJobAdminRequest*                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSChangeUserDetailsRequest.SendChangeUserDetailsRequest
struct UGSChangeUserDetailsRequest_SendChangeUserDetailsRequest_Params
{
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	struct FString                                     Language;                                                 // (Parm, ZeroConstructor)
	struct FString                                     NewPassword;                                              // (Parm, ZeroConstructor)
	struct FString                                     OldPassword;                                              // (Parm, ZeroConstructor)
	struct FString                                     UserName;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSChangeUserDetailsRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSChatOnChallengeRequest.SendChatOnChallengeRequest
struct UGSChatOnChallengeRequest_SendChatOnChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSChatOnChallengeRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSConsumeVirtualGoodRequest.SendConsumeVirtualGoodRequest
struct UGSConsumeVirtualGoodRequest_SendConsumeVirtualGoodRequest_Params
{
	int                                                Quantity;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ShortCode;                                                // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSConsumeVirtualGoodRequest*                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSCreateChallengeRequest.SendCreateChallengeRequest
struct UGSCreateChallengeRequest_SendCreateChallengeRequest_Params
{
	struct FString                                     AccessType;                                               // (Parm, ZeroConstructor)
	bool                                               AutoStartJoinedChallengeOnMaxPlayers;                     // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ChallengeMessage;                                         // (Parm, ZeroConstructor)
	struct FString                                     ChallengeShortCode;                                       // (Parm, ZeroConstructor)
	int                                                Currency1Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Currency2Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Currency3Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Currency4Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Currency5Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Currency6Wager;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       EligibilityCriteria;                                      // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     EndTime;                                                  // (Parm, ZeroConstructor)
	struct FString                                     ExpiryTime;                                               // (Parm, ZeroConstructor)
	int                                                MaxAttempts;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                MaxPlayers;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                MinPlayers;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Silent;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     StartTime;                                                // (Parm, ZeroConstructor)
	class UGameSparksRequestArray*                     UsersToChallenge;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSCreateChallengeRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSCreateTeamRequest.SendCreateTeamRequest
struct UGSCreateTeamRequest_SendCreateTeamRequest_Params
{
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamName;                                                 // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSCreateTeamRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSDeclineChallengeRequest.SendDeclineChallengeRequest
struct UGSDeclineChallengeRequest_SendDeclineChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSDeclineChallengeRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSDeviceAuthenticationRequest.SendDeviceAuthenticationRequest
struct UGSDeviceAuthenticationRequest_SendDeviceAuthenticationRequest_Params
{
	struct FString                                     DeviceID;                                                 // (Parm, ZeroConstructor)
	struct FString                                     DeviceModel;                                              // (Parm, ZeroConstructor)
	struct FString                                     DeviceName;                                               // (Parm, ZeroConstructor)
	struct FString                                     DeviceOS;                                                 // (Parm, ZeroConstructor)
	struct FString                                     DeviceType;                                               // (Parm, ZeroConstructor)
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	struct FString                                     OperatingSystem;                                          // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSDeviceAuthenticationRequest*              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSDismissMessageRequest.SendDismissMessageRequest
struct UGSDismissMessageRequest_SendDismissMessageRequest_Params
{
	struct FString                                     MessageId;                                                // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSDismissMessageRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSDismissMultipleMessagesRequest.SendDismissMultipleMessagesRequest
struct UGSDismissMultipleMessagesRequest_SendDismissMultipleMessagesRequest_Params
{
	class UGameSparksRequestArray*                     MessageIds;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSDismissMultipleMessagesRequest*           ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSDropTeamRequest.SendDropTeamRequest
struct UGSDropTeamRequest_SendDropTeamRequest_Params
{
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSDropTeamRequest*                          ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSEndSessionRequest.SendEndSessionRequest
struct UGSEndSessionRequest_SendEndSessionRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSEndSessionRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSFacebookConnectRequest.SendFacebookConnectRequest
struct UGSFacebookConnectRequest_SendFacebookConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	struct FString                                     Code;                                                     // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSFacebookConnectRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSFindChallengeRequest.SendFindChallengeRequest
struct UGSFindChallengeRequest_SendFindChallengeRequest_Params
{
	struct FString                                     AccessType;                                               // (Parm, ZeroConstructor)
	int                                                Count;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Eligibility;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     ShortCode;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSFindChallengeRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSFindMatchRequest.SendFindMatchRequest
struct UGSFindMatchRequest_SendFindMatchRequest_Params
{
	struct FString                                     Action;                                                   // (Parm, ZeroConstructor)
	struct FString                                     MatchGroup;                                               // (Parm, ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // (Parm, ZeroConstructor)
	int                                                Skill;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSFindMatchRequest*                         ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSFindPendingMatchesRequest.SendFindPendingMatchesRequest
struct UGSFindPendingMatchesRequest_SendFindPendingMatchesRequest_Params
{
	struct FString                                     MatchGroup;                                               // (Parm, ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // (Parm, ZeroConstructor)
	int                                                MaxMatchesToFind;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSFindPendingMatchesRequest*                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGameCenterConnectRequest.SendGameCenterConnectRequest
struct UGSGameCenterConnectRequest_SendGameCenterConnectRequest_Params
{
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ExternalPlayerId;                                         // (Parm, ZeroConstructor)
	struct FString                                     PublicKeyUrl;                                             // (Parm, ZeroConstructor)
	struct FString                                     Salt;                                                     // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Signature;                                                // (Parm, ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Timestamp;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGameCenterConnectRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetChallengeRequest.SendGetChallengeRequest
struct UGSGetChallengeRequest_SendGetChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetChallengeRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetDownloadableRequest.SendGetDownloadableRequest
struct UGSGetDownloadableRequest_SendGetDownloadableRequest_Params
{
	struct FString                                     ShortCode;                                                // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetDownloadableRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetLeaderboardEntriesRequest.SendGetLeaderboardEntriesRequest
struct UGSGetLeaderboardEntriesRequest_SendGetLeaderboardEntriesRequest_Params
{
	class UGameSparksRequestArray*                     Challenges;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     Leaderboards;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Player;                                                   // (Parm, ZeroConstructor)
	bool                                               Social;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetLeaderboardEntriesRequest*             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetMessageRequest.SendGetMessageRequest
struct UGSGetMessageRequest_SendGetMessageRequest_Params
{
	struct FString                                     MessageId;                                                // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetMessageRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetMyTeamsRequest.SendGetMyTeamsRequest
struct UGSGetMyTeamsRequest_SendGetMyTeamsRequest_Params
{
	bool                                               OwnedOnly;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetMyTeamsRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetPropertyRequest.SendGetPropertyRequest
struct UGSGetPropertyRequest_SendGetPropertyRequest_Params
{
	struct FString                                     PropertyShortCode;                                        // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetPropertyRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetPropertySetRequest.SendGetPropertySetRequest
struct UGSGetPropertySetRequest_SendGetPropertySetRequest_Params
{
	struct FString                                     PropertySetShortCode;                                     // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetPropertySetRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetTeamRequest.SendGetTeamRequest
struct UGSGetTeamRequest_SendGetTeamRequest_Params
{
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetTeamRequest*                           ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetUploadUrlRequest.SendGetUploadUrlRequest
struct UGSGetUploadUrlRequest_SendGetUploadUrlRequest_Params
{
	class UGameSparksScriptData*                       UploadData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetUploadUrlRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGetUploadedRequest.SendGetUploadedRequest
struct UGSGetUploadedRequest_SendGetUploadedRequest_Params
{
	struct FString                                     UploadId;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGetUploadedRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGooglePlayBuyGoodsRequest.SendGooglePlayBuyGoodsRequest
struct UGSGooglePlayBuyGoodsRequest_SendGooglePlayBuyGoodsRequest_Params
{
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     Signature;                                                // (Parm, ZeroConstructor)
	struct FString                                     SignedData;                                               // (Parm, ZeroConstructor)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGooglePlayBuyGoodsRequest*                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGooglePlayConnectRequest.SendGooglePlayConnectRequest
struct UGSGooglePlayConnectRequest_SendGooglePlayConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	struct FString                                     Code;                                                     // (Parm, ZeroConstructor)
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               GooglePlusScope;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ProfileScope;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     RedirectUri;                                              // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGooglePlayConnectRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSGooglePlusConnectRequest.SendGooglePlusConnectRequest
struct UGSGooglePlusConnectRequest_SendGooglePlusConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	struct FString                                     Code;                                                     // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     RedirectUri;                                              // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSGooglePlusConnectRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSIOSBuyGoodsRequest.SendIOSBuyGoodsRequest
struct UGSIOSBuyGoodsRequest_SendIOSBuyGoodsRequest_Params
{
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     Receipt;                                                  // (Parm, ZeroConstructor)
	bool                                               Sandbox;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSIOSBuyGoodsRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSJoinChallengeRequest.SendJoinChallengeRequest
struct UGSJoinChallengeRequest_SendJoinChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Eligibility;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSJoinChallengeRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSJoinPendingMatchRequest.SendJoinPendingMatchRequest
struct UGSJoinPendingMatchRequest_SendJoinPendingMatchRequest_Params
{
	struct FString                                     MatchGroup;                                               // (Parm, ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // (Parm, ZeroConstructor)
	struct FString                                     PendingMatchId;                                           // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSJoinPendingMatchRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSJoinTeamRequest.SendJoinTeamRequest
struct UGSJoinTeamRequest_SendJoinTeamRequest_Params
{
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSJoinTeamRequest*                          ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSKongregateConnectRequest.SendKongregateConnectRequest
struct UGSKongregateConnectRequest_SendKongregateConnectRequest_Params
{
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     GameAuthToken;                                            // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     UserId;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSKongregateConnectRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSLeaderboardDataRequest.SendLeaderboardDataRequest
struct UGSLeaderboardDataRequest_SendLeaderboardDataRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	bool                                               DontErrorOnNotSocial;                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     LeaderboardShortCode;                                     // (Parm, ZeroConstructor)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Social;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamIds;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSLeaderboardDataRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSLeaderboardsEntriesRequest.SendLeaderboardsEntriesRequest
struct UGSLeaderboardsEntriesRequest_SendLeaderboardsEntriesRequest_Params
{
	class UGameSparksRequestArray*                     Challenges;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     Leaderboards;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Player;                                                   // (Parm, ZeroConstructor)
	bool                                               Social;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSLeaderboardsEntriesRequest*               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSLeaveTeamRequest.SendLeaveTeamRequest
struct UGSLeaveTeamRequest_SendLeaveTeamRequest_Params
{
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSLeaveTeamRequest*                         ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListAchievementsRequest.SendListAchievementsRequest
struct UGSListAchievementsRequest_SendListAchievementsRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListAchievementsRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListBulkJobsAdminRequest.SendListBulkJobsAdminRequest
struct UGSListBulkJobsAdminRequest_SendListBulkJobsAdminRequest_Params
{
	class UGameSparksRequestArray*                     BulkJobIds;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListBulkJobsAdminRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListChallengeRequest.SendListChallengeRequest
struct UGSListChallengeRequest_SendListChallengeRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ShortCode;                                                // (Parm, ZeroConstructor)
	struct FString                                     State;                                                    // (Parm, ZeroConstructor)
	class UGameSparksRequestArray*                     States;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListChallengeRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListChallengeTypeRequest.SendListChallengeTypeRequest
struct UGSListChallengeTypeRequest_SendListChallengeTypeRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListChallengeTypeRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListGameFriendsRequest.SendListGameFriendsRequest
struct UGSListGameFriendsRequest_SendListGameFriendsRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListGameFriendsRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListInviteFriendsRequest.SendListInviteFriendsRequest
struct UGSListInviteFriendsRequest_SendListInviteFriendsRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListInviteFriendsRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListLeaderboardsRequest.SendListLeaderboardsRequest
struct UGSListLeaderboardsRequest_SendListLeaderboardsRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListLeaderboardsRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListMessageDetailRequest.SendListMessageDetailRequest
struct UGSListMessageDetailRequest_SendListMessageDetailRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Include;                                                  // (Parm, ZeroConstructor)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Status;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListMessageDetailRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListMessageRequest.SendListMessageRequest
struct UGSListMessageRequest_SendListMessageRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Include;                                                  // (Parm, ZeroConstructor)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListMessageRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListMessageSummaryRequest.SendListMessageSummaryRequest
struct UGSListMessageSummaryRequest_SendListMessageSummaryRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListMessageSummaryRequest*                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListTeamChatRequest.SendListTeamChatRequest
struct UGSListTeamChatRequest_SendListTeamChatRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListTeamChatRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListTeamsRequest.SendListTeamsRequest
struct UGSListTeamsRequest_SendListTeamsRequest_Params
{
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     TeamNameFilter;                                           // (Parm, ZeroConstructor)
	struct FString                                     TeamTypeFilter;                                           // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListTeamsRequest*                         ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListTransactionsRequest.SendListTransactionsRequest
struct UGSListTransactionsRequest_SendListTransactionsRequest_Params
{
	struct FString                                     DateFrom;                                                 // (Parm, ZeroConstructor)
	struct FString                                     DateTo;                                                   // (Parm, ZeroConstructor)
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Include;                                                  // (Parm, ZeroConstructor)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListTransactionsRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSListVirtualGoodsRequest.SendListVirtualGoodsRequest
struct UGSListVirtualGoodsRequest_SendListVirtualGoodsRequest_Params
{
	bool                                               IncludeDisabled;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     Tags;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSListVirtualGoodsRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSLogChallengeEventRequest.SendLogChallengeEventRequest
struct UGSLogChallengeEventRequest_SendLogChallengeEventRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     EventKey;                                                 // (Parm, ZeroConstructor)
	class UGameSparksLogEventData*                     LogEventData;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSLogChallengeEventRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSLogEventRequest.SendLogEventRequest
struct UGSLogEventRequest_SendLogEventRequest_Params
{
	struct FString                                     EventKey;                                                 // (Parm, ZeroConstructor)
	class UGameSparksLogEventData*                     LogEventData;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSLogEventRequest*                          ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSMatchDetailsRequest.SendMatchDetailsRequest
struct UGSMatchDetailsRequest_SendMatchDetailsRequest_Params
{
	struct FString                                     MatchId;                                                  // (Parm, ZeroConstructor)
	bool                                               RealtimeEnabled;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSMatchDetailsRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSMatchmakingRequest.SendMatchmakingRequest
struct UGSMatchmakingRequest_SendMatchmakingRequest_Params
{
	struct FString                                     Action;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       CustomQuery;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       MatchData;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     MatchGroup;                                               // (Parm, ZeroConstructor)
	struct FString                                     MatchShortCode;                                           // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ParticipantData;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Skill;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSMatchmakingRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSPSNConnectRequest.SendPSNConnectRequest
struct UGSPSNConnectRequest_SendPSNConnectRequest_Params
{
	struct FString                                     AuthorizationCode;                                        // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     RedirectUri;                                              // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSPSNConnectRequest*                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSPsnBuyGoodsRequest.SendPsnBuyGoodsRequest
struct UGSPsnBuyGoodsRequest_SendPsnBuyGoodsRequest_Params
{
	struct FString                                     AuthorizationCode;                                        // (Parm, ZeroConstructor)
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     EntitlementLabel;                                         // (Parm, ZeroConstructor)
	struct FString                                     RedirectUri;                                              // (Parm, ZeroConstructor)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                UseCount;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSPsnBuyGoodsRequest*                       ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSPushRegistrationRequest.SendPushRegistrationRequest
struct UGSPushRegistrationRequest_SendPushRegistrationRequest_Params
{
	struct FString                                     DeviceOS;                                                 // (Parm, ZeroConstructor)
	struct FString                                     PushId;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSPushRegistrationRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSQQConnectRequest.SendQQConnectRequest
struct UGSQQConnectRequest_SendQQConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSQQConnectRequest*                         ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRegistrationRequest.SendRegistrationRequest
struct UGSRegistrationRequest_SendRegistrationRequest_Params
{
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	struct FString                                     Password;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     UserName;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRegistrationRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRevokePurchaseGoodsRequest.SendRevokePurchaseGoodsRequest
struct UGSRevokePurchaseGoodsRequest_SendRevokePurchaseGoodsRequest_Params
{
	struct FString                                     PlayerId;                                                 // (Parm, ZeroConstructor)
	struct FString                                     StoreType;                                                // (Parm, ZeroConstructor)
	class UGameSparksRequestArray*                     TransactionIds;                                           // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRevokePurchaseGoodsRequest*               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSScheduleBulkJobAdminRequest.SendScheduleBulkJobAdminRequest
struct UGSScheduleBulkJobAdminRequest_SendScheduleBulkJobAdminRequest_Params
{
	class UGameSparksScriptData*                       Data;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ModuleShortCode;                                          // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       PlayerQuery;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ScheduledTime;                                            // (Parm, ZeroConstructor)
	struct FString                                     Script;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSScheduleBulkJobAdminRequest*              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSendFriendMessageRequest.SendSendFriendMessageRequest
struct UGSSendFriendMessageRequest_SendSendFriendMessageRequest_Params
{
	class UGameSparksRequestArray*                     FriendIds;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSendFriendMessageRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSendTeamChatMessageRequest.SendSendTeamChatMessageRequest
struct UGSSendTeamChatMessageRequest_SendSendTeamChatMessageRequest_Params
{
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	struct FString                                     OwnerId;                                                  // (Parm, ZeroConstructor)
	struct FString                                     TeamId;                                                   // (Parm, ZeroConstructor)
	struct FString                                     TeamType;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSendTeamChatMessageRequest*               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSocialDisconnectRequest.SendSocialDisconnectRequest
struct UGSSocialDisconnectRequest_SendSocialDisconnectRequest_Params
{
	struct FString                                     SystemId;                                                 // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSocialDisconnectRequest*                  ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSocialLeaderboardDataRequest.SendSocialLeaderboardDataRequest
struct UGSSocialLeaderboardDataRequest_SendSocialLeaderboardDataRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	bool                                               DontErrorOnNotSocial;                                     // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                EntryCount;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     FriendIds;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeFirst;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                IncludeLast;                                              // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               InverseSocial;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     LeaderboardShortCode;                                     // (Parm, ZeroConstructor)
	int                                                Offset;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Social;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamIds;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksRequestArray*                     TeamTypes;                                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSocialLeaderboardDataRequest*             ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSocialStatusRequest.SendSocialStatusRequest
struct UGSSocialStatusRequest_SendSocialStatusRequest_Params
{
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSocialStatusRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSteamBuyGoodsRequest.SendSteamBuyGoodsRequest
struct UGSSteamBuyGoodsRequest_SendSteamBuyGoodsRequest_Params
{
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     OrderId;                                                  // (Parm, ZeroConstructor)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSteamBuyGoodsRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSSteamConnectRequest.SendSteamConnectRequest
struct UGSSteamConnectRequest_SendSteamConnectRequest_Params
{
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     SessionTicket;                                            // (Parm, ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSSteamConnectRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSTwitchConnectRequest.SendTwitchConnectRequest
struct UGSTwitchConnectRequest_SendTwitchConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSTwitchConnectRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSTwitterConnectRequest.SendTwitterConnectRequest
struct UGSTwitterConnectRequest_SendTwitterConnectRequest_Params
{
	struct FString                                     AccessSecret;                                             // (Parm, ZeroConstructor)
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSTwitterConnectRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSUpdateMessageRequest.SendUpdateMessageRequest
struct UGSUpdateMessageRequest_SendUpdateMessageRequest_Params
{
	struct FString                                     MessageId;                                                // (Parm, ZeroConstructor)
	struct FString                                     Status;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSUpdateMessageRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSViberConnectRequest.SendViberConnectRequest
struct UGSViberConnectRequest_SendViberConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               DoNotRegisterForPush;                                     // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSViberConnectRequest*                      ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSWeChatConnectRequest.SendWeChatConnectRequest
struct UGSWeChatConnectRequest_SendWeChatConnectRequest_Params
{
	struct FString                                     AccessToken;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     OpenId;                                                   // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSWeChatConnectRequest*                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSWindowsBuyGoodsRequest.SendWindowsBuyGoodsRequest
struct UGSWindowsBuyGoodsRequest_SendWindowsBuyGoodsRequest_Params
{
	struct FString                                     CurrencyCode;                                             // (Parm, ZeroConstructor)
	struct FString                                     Platform;                                                 // (Parm, ZeroConstructor)
	struct FString                                     Receipt;                                                  // (Parm, ZeroConstructor)
	float                                              SubUnitPrice;                                             // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               UniqueTransactionByPlayer;                                // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSWindowsBuyGoodsRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSWithdrawChallengeRequest.SendWithdrawChallengeRequest
struct UGSWithdrawChallengeRequest_SendWithdrawChallengeRequest_Params
{
	struct FString                                     ChallengeInstanceId;                                      // (Parm, ZeroConstructor)
	struct FString                                     Message;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSWithdrawChallengeRequest*                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSXBOXLiveConnectRequest.SendXBOXLiveConnectRequest
struct UGSXBOXLiveConnectRequest_SendXBOXLiveConnectRequest_Params
{
	struct FString                                     DisplayName;                                              // (Parm, ZeroConstructor)
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     StsTokenString;                                           // (Parm, ZeroConstructor)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSXBOXLiveConnectRequest*                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSXboxOneConnectRequest.SendXboxOneConnectRequest
struct UGSXboxOneConnectRequest_SendXboxOneConnectRequest_Params
{
	bool                                               DoNotLinkToCurrentPlayer;                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ErrorOnSwitch;                                            // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Sandbox;                                                  // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       Segments;                                                 // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SwitchIfPossible;                                         // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               SyncDisplayName;                                          // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     token;                                                    // (Parm, ZeroConstructor)
	class UGameSparksScriptData*                       ScriptData;                                               // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Durable;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                RequestTimeoutSeconds;                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSXboxOneConnectRequest*                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.ToString
struct UGSRTData_ToString_Params
{
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GSRTData.SetVector
struct UGSRTData_SetVector_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.SetString
struct UGSRTData_SetString_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Value;                                                    // (Parm, ZeroConstructor)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.SetInt
struct UGSRTData_SetInt_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.SetFVector
struct UGSRTData_SetFVector_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     Value;                                                    // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.SetFloat
struct UGSRTData_SetFloat_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.SetData
struct UGSRTData_SetData_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   Value;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.HasVector
struct UGSRTData_HasVector_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.HasString
struct UGSRTData_HasString_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.HasInt
struct UGSRTData_HasInt_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.HasFloat
struct UGSRTData_HasFloat_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.HasData
struct UGSRTData_HasData_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.GetVector
struct UGSRTData_GetVector_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.GetString
struct UGSRTData_GetString_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GSRTData.GetInt
struct UGSRTData_GetInt_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.GetFVector
struct UGSRTData_GetFVector_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	struct FVector                                     ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.GetFloat
struct UGSRTData_GetFloat_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.GetData
struct UGSRTData_GetData_Params
{
	int                                                Index;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTData.CreateRTData
struct UGSRTData_CreateRTData_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTSession.Stop
struct UGSRTSession_Stop_Params
{
};

// Function GameSparks.GSRTSession.Start
struct UGSRTSession_Start_Params
{
};

// Function GameSparks.GSRTSession.Send
struct UGSRTSession_Send_Params
{
	int                                                opCode;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	EDeliveryIntent                                    intent;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   Data;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
	TArray<int>                                        peerIds;                                                  // (Parm, ZeroConstructor)
};

// DelegateFunction GameSparks.GSRTSession.OnReady__DelegateSignature
struct UGSRTSession_OnReady__DelegateSignature_Params
{
	class UGSRTSession*                                session;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ready;                                                    // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction GameSparks.GSRTSession.OnPlayerDisconnect__DelegateSignature
struct UGSRTSession_OnPlayerDisconnect__DelegateSignature_Params
{
	class UGSRTSession*                                session;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                PeerId;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction GameSparks.GSRTSession.OnPlayerConnect__DelegateSignature
struct UGSRTSession_OnPlayerConnect__DelegateSignature_Params
{
	class UGSRTSession*                                session;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                PeerId;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
};

// DelegateFunction GameSparks.GSRTSession.OnData__DelegateSignature
struct UGSRTSession_OnData__DelegateSignature_Params
{
	class UGSRTSession*                                session;                                                  // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                sender;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	int                                                opCode;                                                   // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTData*                                   Data;                                                     // (Parm, ZeroConstructor, IsPlainOldData)
};

// Function GameSparks.GSRTSession.GetPeerId
struct UGSRTSession_GetPeerId_Params
{
	int                                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTSession.GetActivePeers
struct UGSRTSession_GetActivePeers_Params
{
	TArray<int>                                        ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm)
};

// Function GameSparks.GSRTSession.CreateRTSession
struct UGSRTSession_CreateRTSession_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Host;                                                     // (Parm, ZeroConstructor)
	struct FString                                     Port;                                                     // (Parm, ZeroConstructor)
	struct FString                                     token;                                                    // (Parm, ZeroConstructor)
	class UGSRTSession*                                ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetZ
struct UGSRTVector_SetZ_Params
{
	float                                              Z;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetY
struct UGSRTVector_SetY_Params
{
	float                                              Y;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetXYZW
struct UGSRTVector_SetXYZW_Params
{
	float                                              X;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Y;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Z;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              W;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetXYZ
struct UGSRTVector_SetXYZ_Params
{
	float                                              X;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Y;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Z;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetXY
struct UGSRTVector_SetXY_Params
{
	float                                              X;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	float                                              Y;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetX
struct UGSRTVector_SetX_Params
{
	float                                              X;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetW
struct UGSRTVector_SetW_Params
{
	float                                              W;                                                        // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.SetFromFVector
struct UGSRTVector_SetFromFVector_Params
{
	struct FVector                                     V;                                                        // (ConstParm, Parm, OutParm, ReferenceParm, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.HasZ
struct UGSRTVector_HasZ_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.HasY
struct UGSRTVector_HasY_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.HasX
struct UGSRTVector_HasX_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.HasW
struct UGSRTVector_HasW_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.GetZ
struct UGSRTVector_GetZ_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.GetY
struct UGSRTVector_GetY_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.GetX
struct UGSRTVector_GetX_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.GetW
struct UGSRTVector_GetW_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function GameSparks.GSRTVector.CreateRTVector
struct UGSRTVector_CreateRTVector_Params
{
	class UObject*                                     WorldContextObject;                                       // (Parm, ZeroConstructor, IsPlainOldData)
	class UGSRTVector*                                 ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
