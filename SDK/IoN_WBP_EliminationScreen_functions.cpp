// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_EliminationScreen.WBP_EliminationScreen_C.On_BackgroundImage_MouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_EliminationScreen_C::On_BackgroundImage_MouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.On_BackgroundImage_MouseButtonDown_1");

	UWBP_EliminationScreen_C_On_BackgroundImage_MouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_EliminationScreen_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.Construct");

	UWBP_EliminationScreen_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_EliminationScreen_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature");

	UWBP_EliminationScreen_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_315_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_EliminationScreen_C::BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature");

	UWBP_EliminationScreen_C_BndEvt__Button_1_K2Node_ComponentBoundEvent_335_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Battle Royale
// (BlueprintCallable, BlueprintEvent)

void UWBP_EliminationScreen_C::Setup_Battle_Royale()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Battle Royale");

	UWBP_EliminationScreen_C_Setup_Battle_Royale_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_EliminationScreen_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.OnAnimationFinished");

	UWBP_EliminationScreen_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Gun Game
// (BlueprintCallable, BlueprintEvent)

void UWBP_EliminationScreen_C::Setup_Gun_Game()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.Setup Gun Game");

	UWBP_EliminationScreen_C_Setup_Gun_Game_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_EliminationScreen_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.Tick");

	UWBP_EliminationScreen_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_EliminationScreen.WBP_EliminationScreen_C.ExecuteUbergraph_WBP_EliminationScreen
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_EliminationScreen_C::ExecuteUbergraph_WBP_EliminationScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_EliminationScreen.WBP_EliminationScreen_C.ExecuteUbergraph_WBP_EliminationScreen");

	UWBP_EliminationScreen_C_ExecuteUbergraph_WBP_EliminationScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
