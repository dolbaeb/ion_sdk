#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Label_Text_Visibility_1
struct UWBP_InventoryItemSlot_C_Get_Label_Text_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_DragReceive_Background_1
struct UWBP_InventoryItemSlot_C_Get_DragReceive_Background_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Highlight_Background_1
struct UWBP_InventoryItemSlot_C_Get_Highlight_Background_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_Frame_Brush_1
struct UWBP_InventoryItemSlot_C_Get_Frame_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.AcceptsDropForItem
struct UWBP_InventoryItemSlot_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnDrop
struct UWBP_InventoryItemSlot_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetCurrentItem
struct UWBP_InventoryItemSlot_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.GetText_1
struct UWBP_InventoryItemSlot_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.SetAssignFullVisibility
struct UWBP_InventoryItemSlot_C_SetAssignFullVisibility_Params
{
	bool                                               bVisible;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_UnloadAmmoButton_bIsEnabled_1
struct UWBP_InventoryItemSlot_C_Get_UnloadAmmoButton_bIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmountText_Text_1
struct UWBP_InventoryItemSlot_C_Get_AmountText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Get_AmmoText_Text_1
struct UWBP_InventoryItemSlot_C_Get_AmmoText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemChanged
struct UWBP_InventoryItemSlot_C_OnItemChanged_Params
{
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Tick
struct UWBP_InventoryItemSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.Construct
struct UWBP_InventoryItemSlot_C_Construct_Params
{
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnItemClicked
struct UWBP_InventoryItemSlot_C_OnItemClicked_Params
{
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnHovered
struct UWBP_InventoryItemSlot_C_OnHovered_Params
{
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.OnUnhovered
struct UWBP_InventoryItemSlot_C_OnUnhovered_Params
{
};

// Function WBP_InventoryItemSlot.WBP_InventoryItemSlot_C.ExecuteUbergraph_WBP_InventoryItemSlot
struct UWBP_InventoryItemSlot_C_ExecuteUbergraph_WBP_InventoryItemSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
