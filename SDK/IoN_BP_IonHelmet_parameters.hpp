#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_IonHelmet.BP_IonHelmet_C.UserConstructionScript
struct ABP_IonHelmet_C_UserConstructionScript_Params
{
};

// Function BP_IonHelmet.BP_IonHelmet_C.ExecuteUbergraph_BP_IonHelmet
struct ABP_IonHelmet_C_ExecuteUbergraph_BP_IonHelmet_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
