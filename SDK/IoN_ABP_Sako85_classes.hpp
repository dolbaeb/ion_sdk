#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_Sako85.ABP_Sako85_C
// 0x0341 (0x0711 - 0x03D0)
class UABP_Sako85_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_53E30A2443E8A1C899C73C82C3DC93B7;      // 0x03D8(0x0048)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_FF207F6745520E61FAAE77B454F414E3;      // 0x0420(0x0068)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_527F33034E8E2D7EAD7B46994E92A618;// 0x0488(0x0038)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105;// 0x04C0(0x00D0)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_1D25448B435C577F10A6BBBFEB82DD29;// 0x0590(0x0038)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_077E2BF54B1B3A8060D578B7766054B4;// 0x05C8(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_A042EE494C2D64E828DBD3B5BF67684E;// 0x0610(0x00B8)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_9AE9EEB84DDFD80934D31EA441CD84A2;// 0x06C8(0x0048)
	bool                                               Is_Montage_Playing;                                       // 0x0710(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_Sako85.ABP_Sako85_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void ExecuteUbergraph_ABP_Sako85(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
