#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.SetPrice
struct UWBP_RedeemBtn_C_SetPrice_Params
{
	int                                                NewPrice;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature
struct UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
struct UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
struct UWBP_RedeemBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.Construct
struct UWBP_RedeemBtn_C_Construct_Params
{
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.ExecuteUbergraph_WBP_RedeemBtn
struct UWBP_RedeemBtn_C_ExecuteUbergraph_WBP_RedeemBtn_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_RedeemBtn.WBP_RedeemBtn_C.RedeemCratePressed__DelegateSignature
struct UWBP_RedeemBtn_C_RedeemCratePressed__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
