#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.GetVisibility_1
struct UWBP_MapArrow_Spectator_C_GetVisibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Construct
struct UWBP_MapArrow_Spectator_C_Construct_Params
{
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Tick
struct UWBP_MapArrow_Spectator_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.Toggle Highlight
struct UWBP_MapArrow_Spectator_C_Toggle_Highlight_Params
{
	bool                                               Highlight;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature
struct UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_10_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature
struct UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_6_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature
struct UWBP_MapArrow_Spectator_C_BndEvt__SpectateButton_K2Node_ComponentBoundEvent_16_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.ExecuteUbergraph_WBP_MapArrow_Spectator
struct UWBP_MapArrow_Spectator_C_ExecuteUbergraph_WBP_MapArrow_Spectator_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MapArrow_Spectator.WBP_MapArrow_Spectator_C.OnPlayerLeave__DelegateSignature
struct UWBP_MapArrow_Spectator_C_OnPlayerLeave__DelegateSignature_Params
{
	class UWBP_MapArrow_Spectator_C*                   Widget;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
