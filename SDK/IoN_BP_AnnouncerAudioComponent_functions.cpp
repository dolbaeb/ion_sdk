// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleStarted
// (Event, Public, BlueprintEvent)

void UBP_AnnouncerAudioComponent_C::OnBattleRoyaleStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleStarted");

	UBP_AnnouncerAudioComponent_C_OnBattleRoyaleStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleCountdownStarted
// (Event, Public, BlueprintEvent)

void UBP_AnnouncerAudioComponent_C::OnBattleRoyaleCountdownStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnBattleRoyaleCountdownStarted");

	UBP_AnnouncerAudioComponent_C_OnBattleRoyaleCountdownStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnMatchEnded
// (Event, Public, BlueprintEvent)

void UBP_AnnouncerAudioComponent_C::OnMatchEnded()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnMatchEnded");

	UBP_AnnouncerAudioComponent_C_OnMatchEnded_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnFinalConvergenceStarted
// (Event, Public, BlueprintEvent)

void UBP_AnnouncerAudioComponent_C::OnFinalConvergenceStarted()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.OnFinalConvergenceStarted");

	UBP_AnnouncerAudioComponent_C_OnFinalConvergenceStarted_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.ExecuteUbergraph_BP_AnnouncerAudioComponent
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_AnnouncerAudioComponent_C::ExecuteUbergraph_BP_AnnouncerAudioComponent(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AnnouncerAudioComponent.BP_AnnouncerAudioComponent_C.ExecuteUbergraph_BP_AnnouncerAudioComponent");

	UBP_AnnouncerAudioComponent_C_ExecuteUbergraph_BP_AnnouncerAudioComponent_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
