#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19
struct UABP_MK18_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19_Params
{
};

// Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C
struct UABP_MK18_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C_Params
{
};

// Function ABP_MK18.ABP_MK18_C.BlueprintUpdateAnimation
struct UABP_MK18_C_BlueprintUpdateAnimation_Params
{
	float*                                             DeltaTimeX;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function ABP_MK18.ABP_MK18_C.ExecuteUbergraph_ABP_MK18
struct UABP_MK18_C_ExecuteUbergraph_ABP_MK18_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
