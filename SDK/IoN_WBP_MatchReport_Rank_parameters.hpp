#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Get_BadgeText_Text_1
struct UWBP_MatchReport_Rank_C_Get_BadgeText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Begin Animation
struct UWBP_MatchReport_Rank_C_Begin_Animation_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnAnimationFinished
struct UWBP_MatchReport_Rank_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Appear
struct UWBP_MatchReport_Rank_C_Appear_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Disappear
struct UWBP_MatchReport_Rank_C_Disappear_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Construct
struct UWBP_MatchReport_Rank_C_Construct_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnSetMatchHistory
struct UWBP_MatchReport_Rank_C_OnSetMatchHistory_Params
{
	struct FPlayerMatchHistory                         MatchHistory;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.OnReportClicked
struct UWBP_MatchReport_Rank_C_OnReportClicked_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Set Final Rank State
struct UWBP_MatchReport_Rank_C_Set_Final_Rank_State_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.Fast Forward
struct UWBP_MatchReport_Rank_C_Fast_Forward_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature
struct UWBP_MatchReport_Rank_C_BndEvt__WBP_RankingPoints_C_0_K2Node_ComponentBoundEvent_0_ProgressFinished__DelegateSignature_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.CheckRankChange
struct UWBP_MatchReport_Rank_C_CheckRankChange_Params
{
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.ExecuteUbergraph_WBP_MatchReport_Rank
struct UWBP_MatchReport_Rank_C_ExecuteUbergraph_WBP_MatchReport_Rank_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MatchReport_Rank.WBP_MatchReport_Rank_C.AnimationFinished__DelegateSignature
struct UWBP_MatchReport_Rank_C_AnimationFinished__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
