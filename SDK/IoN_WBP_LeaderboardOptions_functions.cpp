// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardOptions_C::BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardOptions_C_BndEvt__ButtonMostKills_K2Node_ComponentBoundEvent_45_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardOptions_C::BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardOptions_C_BndEvt__ButtonMatchesWon_K2Node_ComponentBoundEvent_64_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Select Option
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTextBlock*              Text                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// class UImage*                  Image                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_LeaderboardOptions_C::Select_Option(class UTextBlock* Text, class UImage* Image)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Select Option");

	UWBP_LeaderboardOptions_C_Select_Option_Params params;
	params.Text = Text;
	params.Image = Image;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardOptions_C::BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardOptions_C_BndEvt__ButtonTotalKills_K2Node_ComponentBoundEvent_27_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardOptions_C::BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardOptions_C_BndEvt__ButtonRankPoints_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_LeaderboardOptions_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.Construct");

	UWBP_LeaderboardOptions_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardOptions_C::BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardOptions_C_BndEvt__ButtonMostExperience_K2Node_ComponentBoundEvent_69_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.ExecuteUbergraph_WBP_LeaderboardOptions
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardOptions_C::ExecuteUbergraph_WBP_LeaderboardOptions(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.ExecuteUbergraph_WBP_LeaderboardOptions");

	UWBP_LeaderboardOptions_C_ExecuteUbergraph_WBP_LeaderboardOptions_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.OnCategoryChanged__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<ELeaderboardCategories> Category                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardOptions_C::OnCategoryChanged__DelegateSignature(TEnumAsByte<ELeaderboardCategories> Category)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardOptions.WBP_LeaderboardOptions_C.OnCategoryChanged__DelegateSignature");

	UWBP_LeaderboardOptions_C_OnCategoryChanged__DelegateSignature_Params params;
	params.Category = Category;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
