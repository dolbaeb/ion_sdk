// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_BoolSetting.WBP_BoolSetting_C.Get_CheckBox_CheckedState_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ECheckBoxState                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ECheckBoxState UWBP_BoolSetting_C::Get_CheckBox_CheckedState_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BoolSetting.WBP_BoolSetting_C.Get_CheckBox_CheckedState_1");

	UWBP_BoolSetting_C_Get_CheckBox_CheckedState_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_BoolSetting.WBP_BoolSetting_C.BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BoolSetting_C::BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BoolSetting.WBP_BoolSetting_C.BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_BoolSetting_C_BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BoolSetting.WBP_BoolSetting_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BoolSetting_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BoolSetting.WBP_BoolSetting_C.PreConstruct");

	UWBP_BoolSetting_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BoolSetting.WBP_BoolSetting_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_BoolSetting_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BoolSetting.WBP_BoolSetting_C.Construct");

	UWBP_BoolSetting_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BoolSetting.WBP_BoolSetting_C.ExecuteUbergraph_WBP_BoolSetting
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BoolSetting_C::ExecuteUbergraph_WBP_BoolSetting(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BoolSetting.WBP_BoolSetting_C.ExecuteUbergraph_WBP_BoolSetting");

	UWBP_BoolSetting_C_ExecuteUbergraph_WBP_BoolSetting_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
