// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_IconHover.WBP_IconHover_C.GetSteamItem
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// struct FIONSteamInventoryItem  SteamItem                      (Parm, OutParm)

void UWBP_IconHover_C::GetSteamItem(struct FIONSteamInventoryItem* SteamItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.GetSteamItem");

	UWBP_IconHover_C_GetSteamItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (SteamItem != nullptr)
		*SteamItem = params.SteamItem;
}


// Function WBP_IconHover.WBP_IconHover_C.SetSteamItem
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  SteamItem                      (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_IconHover_C::SetSteamItem(const struct FIONSteamInventoryItem& SteamItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.SetSteamItem");

	UWBP_IconHover_C_SetSteamItem_Params params;
	params.SteamItem = SteamItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IconHover.WBP_IconHover_C.SetItemRarity
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 NewRarity                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_IconHover_C::SetItemRarity(const struct FString& NewRarity)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.SetItemRarity");

	UWBP_IconHover_C_SetItemRarity_Params params;
	params.NewRarity = NewRarity;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IconHover.WBP_IconHover_C.SetIconTexture
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTexture2D*              NewTexture                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_IconHover_C::SetIconTexture(class UTexture2D* NewTexture)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.SetIconTexture");

	UWBP_IconHover_C_SetIconTexture_Params params;
	params.NewTexture = NewTexture;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IconHover.WBP_IconHover_C.SetIconName
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Name                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_IconHover_C::SetIconName(const struct FString& Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.SetIconName");

	UWBP_IconHover_C_SetIconName_Params params;
	params.Name = Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IconHover.WBP_IconHover_C.OnMouseButtonDown
// (BlueprintCosmetic, Event, Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent*          MouseEvent                     (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_IconHover_C::OnMouseButtonDown(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.OnMouseButtonDown");

	UWBP_IconHover_C_OnMouseButtonDown_Params params;
	params.MyGeometry = MyGeometry;
	params.MouseEvent = MouseEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_IconHover.WBP_IconHover_C.ItemSelected__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_Icon_C*             BtnRef                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_IconHover_C::ItemSelected__DelegateSignature(class UWBP_Icon_C* BtnRef)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IconHover.WBP_IconHover_C.ItemSelected__DelegateSignature");

	UWBP_IconHover_C_ItemSelected__DelegateSignature_Params params;
	params.BtnRef = BtnRef;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
