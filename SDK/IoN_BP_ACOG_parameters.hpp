#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_ACOG.BP_ACOG_C.UserConstructionScript
struct ABP_ACOG_C_UserConstructionScript_Params
{
};

// Function BP_ACOG.BP_ACOG_C.UpdateMaterialAimState
struct ABP_ACOG_C_UpdateMaterialAimState_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float*                                             AimRatio;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ACOG.BP_ACOG_C.DetachFromFirearmBlueprint
struct ABP_ACOG_C_DetachFromFirearmBlueprint_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ACOG.BP_ACOG_C.AttachToFirearmBlueprint
struct ABP_ACOG_C_AttachToFirearmBlueprint_Params
{
	class AIONFirearm**                                Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_ACOG.BP_ACOG_C.ExecuteUbergraph_BP_ACOG
struct ABP_ACOG_C_ExecuteUbergraph_BP_ACOG_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
