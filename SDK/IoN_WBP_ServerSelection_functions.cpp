// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ServerSelection.WBP_ServerSelection_C.SwitchPanel
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            WidgetIndex                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ServerSelection_C::SwitchPanel(int WidgetIndex)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.SwitchPanel");

	UWBP_ServerSelection_C_SwitchPanel_Params params;
	params.WidgetIndex = WidgetIndex;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelection.WBP_ServerSelection_C.Update Matchmaking Button Style
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_ServerSelection_C::Update_Matchmaking_Button_Style()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.Update Matchmaking Button Style");

	UWBP_ServerSelection_C_Update_Matchmaking_Button_Style_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelection.WBP_ServerSelection_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ServerSelection_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.Construct");

	UWBP_ServerSelection_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToCustomPanel
// (BlueprintCallable, BlueprintEvent)

void UWBP_ServerSelection_C::OnSwitchToCustomPanel()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToCustomPanel");

	UWBP_ServerSelection_C_OnSwitchToCustomPanel_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToPublicPanel
// (BlueprintCallable, BlueprintEvent)

void UWBP_ServerSelection_C::OnSwitchToPublicPanel()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToPublicPanel");

	UWBP_ServerSelection_C_OnSwitchToPublicPanel_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelection.WBP_ServerSelection_C.ExecuteUbergraph_WBP_ServerSelection
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ServerSelection_C::ExecuteUbergraph_WBP_ServerSelection(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelection.WBP_ServerSelection_C.ExecuteUbergraph_WBP_ServerSelection");

	UWBP_ServerSelection_C_ExecuteUbergraph_WBP_ServerSelection_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
