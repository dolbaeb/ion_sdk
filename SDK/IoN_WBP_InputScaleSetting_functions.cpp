// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetToolTipWidget_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_InputScaleSetting_C::GetToolTipWidget_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetToolTipWidget_1");

	UWBP_InputScaleSetting_C_GetToolTipWidget_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_Slider_Value_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_InputScaleSetting_C::Get_Slider_Value_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_Slider_Value_1");

	UWBP_InputScaleSetting_C_Get_Slider_Value_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_TextBox_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_InputScaleSetting_C::Get_TextBox_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_TextBox_Text_1");

	UWBP_InputScaleSetting_C_Get_TextBox_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.SetValue
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InputScaleSetting_C::SetValue(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.SetValue");

	UWBP_InputScaleSetting_C_SetValue_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetCurrentValue
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// float                          ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

float UWBP_InputScaleSetting_C::GetCurrentValue()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetCurrentValue");

	UWBP_InputScaleSetting_C_GetCurrentValue_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InputScaleSetting_C::BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature");

	UWBP_InputScaleSetting_C_BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InputScaleSetting_C::BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature");

	UWBP_InputScaleSetting_C_BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_InputScaleSetting_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Construct");

	UWBP_InputScaleSetting_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InputScaleSetting_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.PreConstruct");

	UWBP_InputScaleSetting_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.ExecuteUbergraph_WBP_InputScaleSetting
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InputScaleSetting_C::ExecuteUbergraph_WBP_InputScaleSetting(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.ExecuteUbergraph_WBP_InputScaleSetting");

	UWBP_InputScaleSetting_C_ExecuteUbergraph_WBP_InputScaleSetting_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
