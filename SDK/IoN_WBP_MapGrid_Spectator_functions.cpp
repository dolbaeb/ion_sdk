// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ClampMapWithinBounds
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FVector2D               New_Location                   (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FVector2D               Clamped_Location               (Parm, OutParm, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::ClampMapWithinBounds(const struct FVector2D& New_Location, struct FVector2D* Clamped_Location)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ClampMapWithinBounds");

	UWBP_MapGrid_Spectator_C_ClampMapWithinBounds_Params params;
	params.New_Location = New_Location;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Clamped_Location != nullptr)
		*Clamped_Location = params.Clamped_Location;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.UpdateFieldMaterial
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UMaterialInstanceDynamic* Mid                            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FName                   ParamName                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FVector                 Location                       (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float                          Radius                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::UpdateFieldMaterial(class UMaterialInstanceDynamic* Mid, const struct FName& ParamName, const struct FVector& Location, float Radius)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.UpdateFieldMaterial");

	UWBP_MapGrid_Spectator_C_UpdateFieldMaterial_Params params;
	params.Mid = Mid;
	params.ParamName = ParamName;
	params.Location = Location;
	params.Radius = Radius;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_MapGrid_Spectator_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Construct");

	UWBP_MapGrid_Spectator_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Tick");

	UWBP_MapGrid_Spectator_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.On Player Left
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_MapArrow_Spectator_C* Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::On_Player_Left(class UWBP_MapArrow_Spectator_C* Widget)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.On Player Left");

	UWBP_MapGrid_Spectator_C_On_Player_Left_Params params;
	params.Widget = Widget;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_MapGrid_Spectator_C::BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_MapGrid_Spectator_C_BndEvt__Button_99_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Zoom Map
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Zoom_Amount                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::Zoom_Map(float Zoom_Amount)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Zoom Map");

	UWBP_MapGrid_Spectator_C_Zoom_Map_Params params;
	params.Zoom_Amount = Zoom_Amount;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Drag Map
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FVector2D               Drag_Delta                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::Drag_Map(const struct FVector2D& Drag_Delta)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Drag Map");

	UWBP_MapGrid_Spectator_C_Drag_Map_Params params;
	params.Drag_Delta = Drag_Delta;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Update Zoom Scale Text
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FVector2D               Scale                          (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::Update_Zoom_Scale_Text(const struct FVector2D& Scale)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.Update Zoom Scale Text");

	UWBP_MapGrid_Spectator_C_Update_Zoom_Scale_Text_Params params;
	params.Scale = Scale;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ExecuteUbergraph_WBP_MapGrid_Spectator
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_MapGrid_Spectator_C::ExecuteUbergraph_WBP_MapGrid_Spectator(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_MapGrid_Spectator.WBP_MapGrid_Spectator_C.ExecuteUbergraph_WBP_MapGrid_Spectator");

	UWBP_MapGrid_Spectator_C_ExecuteUbergraph_WBP_MapGrid_Spectator_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
