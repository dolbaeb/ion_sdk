#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_KeyAction.BP_KeyAction_C
// 0x0030 (0x0060 - 0x0030)
class UBP_KeyAction_C : public UObject
{
public:
	struct FString                                     Action_Name;                                              // 0x0030(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	struct FString                                     Category;                                                 // 0x0040(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	TArray<class UBP_KeyMapping_C*>                    Key_Mappings;                                             // 0x0050(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_KeyAction.BP_KeyAction_C");
		return ptr;
	}


	void Get_Mapping(const struct FString& Mapping_Name, class UBP_KeyMapping_C** Mapping, bool* Success);
	void Load_Action(class UBP_GameSettings_C* Game_Settings);
	void Save_Action(class UBP_GameSettings_C* Game_Settings);
	void Key_Action_Current_State(class APlayerController* Player_Controller, float* Action_Axis_Value, bool* Just_Pressed, bool* Just_Released);
	void Init_Key_Action(const struct FSKeyAction& Key_Action, const struct FString& Action_Name, class UBP_KeyAction_C** Action);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
