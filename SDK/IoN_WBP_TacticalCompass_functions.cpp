// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_TacticalCompass.WBP_TacticalCompass_C.GetMarkerForPlayer
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// class AIONPlayerState*         Player                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class UWBP_TacticalCompassMarker_C* Array_Element                  (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_TacticalCompass_C::GetMarkerForPlayer(class AIONPlayerState* Player, class UWBP_TacticalCompassMarker_C** Array_Element)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompass.WBP_TacticalCompass_C.GetMarkerForPlayer");

	UWBP_TacticalCompass_C_GetMarkerForPlayer_Params params;
	params.Player = Player;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Array_Element != nullptr)
		*Array_Element = params.Array_Element;
}


// Function WBP_TacticalCompass.WBP_TacticalCompass_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_TacticalCompass_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompass.WBP_TacticalCompass_C.Construct");

	UWBP_TacticalCompass_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TacticalCompass.WBP_TacticalCompass_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TacticalCompass_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompass.WBP_TacticalCompass_C.Tick");

	UWBP_TacticalCompass_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_TacticalCompass.WBP_TacticalCompass_C.ExecuteUbergraph_WBP_TacticalCompass
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_TacticalCompass_C::ExecuteUbergraph_WBP_TacticalCompass(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_TacticalCompass.WBP_TacticalCompass_C.ExecuteUbergraph_WBP_TacticalCompass");

	UWBP_TacticalCompass_C_ExecuteUbergraph_WBP_TacticalCompass_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
