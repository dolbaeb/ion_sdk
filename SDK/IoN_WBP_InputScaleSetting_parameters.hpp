#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetToolTipWidget_1
struct UWBP_InputScaleSetting_C_GetToolTipWidget_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_Slider_Value_1
struct UWBP_InputScaleSetting_C_Get_Slider_Value_1_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Get_TextBox_Text_1
struct UWBP_InputScaleSetting_C_Get_TextBox_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.SetValue
struct UWBP_InputScaleSetting_C_SetValue_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.GetCurrentValue
struct UWBP_InputScaleSetting_C_GetCurrentValue_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature
struct UWBP_InputScaleSetting_C_BndEvt__Slider_K2Node_ComponentBoundEvent_125_OnFloatValueChangedEvent__DelegateSignature_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature
struct UWBP_InputScaleSetting_C_BndEvt__TextBox_K2Node_ComponentBoundEvent_265_OnEditableTextCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.Construct
struct UWBP_InputScaleSetting_C_Construct_Params
{
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.PreConstruct
struct UWBP_InputScaleSetting_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InputScaleSetting.WBP_InputScaleSetting_C.ExecuteUbergraph_WBP_InputScaleSetting
struct UWBP_InputScaleSetting_C_ExecuteUbergraph_WBP_InputScaleSetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
