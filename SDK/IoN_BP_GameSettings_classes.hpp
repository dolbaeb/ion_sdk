#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_GameSettings.BP_GameSettings_C
// 0x012D (0x015D - 0x0030)
class UBP_GameSettings_C : public USaveGame
{
public:
	float                                              Look_Horizontal_Sensitivity;                              // 0x0030(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Look_Vertical_Sensitivity;                                // 0x0034(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               Look_Horizontal_Invert;                                   // 0x0038(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               Look_Vertical_Invert;                                     // 0x0039(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x003A(0x0006) MISSED OFFSET
	TScriptInterface<class UBPI_GameSettingsInterface_C> Game_Settings_Interface;                                  // 0x0040(0x0010) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	struct FString                                     Save_File_Name;                                           // 0x0050(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	int                                                Save_File_User_Index;                                     // 0x0060(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
	TArray<struct FSAudioUpdateStruct>                 AudioEmittors;                                            // 0x0068(0x0010) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance)
	float                                              Audio_Multiplier_Master;                                  // 0x0078(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Audio_Multiplier_Music;                                   // 0x007C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Audio_Multiplier_Voice;                                   // 0x0080(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Audio_Multiplier_Effect;                                  // 0x0084(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Audio_Multiplier_Ambient;                                 // 0x0088(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Audio_Multiplier_UI;                                      // 0x008C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Volume_Master;                                            // 0x0090(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              Volume_Music;                                             // 0x0094(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              Volume_Voice;                                             // 0x0098(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              Volume_Effect;                                            // 0x009C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	float                                              Volume_Ambient;                                           // 0x00A0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	int                                                Video_X_Resolution;                                       // 0x00A4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Video_Y_Resolution;                                       // 0x00A8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x00AC(0x0004) MISSED OFFSET
	TArray<class UCameraComponent*>                    Camera_List;                                              // 0x00B0(0x0010) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance)
	TEnumAsByte<EWindowMode>                           Screen_Mode;                                              // 0x00C0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x3];                                       // 0x00C1(0x0003) MISSED OFFSET
	int                                                Resolution_Scale_Quality;                                 // 0x00C4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              View_Distance_Scale;                                      // 0x00C8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Anti_Aliasing_Quality;                                    // 0x00CC(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Post_Processing_Quality;                                  // 0x00D0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Shadow_Quality;                                           // 0x00D4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Texture_Quality;                                          // 0x00D8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Effect_Quality;                                           // 0x00DC(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                Foliage_Quality;                                          // 0x00E0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Field_Of_View_FOV;                                        // 0x00E4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Motion_Blur_Strength;                                     // 0x00E8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               VSync_Enabled;                                            // 0x00EC(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               My_Custom_Checkbox;                                       // 0x00ED(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x2];                                       // 0x00EE(0x0002) MISSED OFFSET
	int                                                My_Custom_RadioBox;                                       // 0x00F0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              My_Custom_Slider;                                         // 0x00F4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Bloom_Intensity;                                          // 0x00F8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Gamma_Intensity;                                          // 0x00FC(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Gain_Intensity;                                           // 0x0100(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData05[0x4];                                       // 0x0104(0x0004) MISSED OFFSET
	class UBP_GameSettings_C*                          Previous_Setting_State;                                   // 0x0108(0x0008) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	struct FString                                     My_Custom_Combobox;                                       // 0x0110(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	float                                              Current_Frame_Time;                                       // 0x0120(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x4];                                       // 0x0124(0x0004) MISSED OFFSET
	TArray<class UBP_KeyAction_C*>                     Key_Actions;                                              // 0x0128(0x0010) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance)
	TArray<class UBP_KeyInput_C*>                      Input_Float_Axis_List;                                    // 0x0138(0x0010) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance)
	TArray<struct FSKeyActionSave>                     Saved_Key_Inputs;                                         // 0x0148(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	float                                              Volume_UI;                                                // 0x0158(0x0004) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)
	TEnumAsByte<EWindowMode>                           Old_ScreenMode_State;                                     // 0x015C(0x0001) (Edit, BlueprintVisible, ZeroConstructor, Transient, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_GameSettings.BP_GameSettings_C");
		return ptr;
	}


	void Get_Foliage_Quality(int* Value, struct FString* Formatted);
	void Set_Foliage_Quality(int Value, bool Apply, int* Result);
	void Modify_Foliage_Quality(TEnumAsByte<EModifySetting> Modify);
	void Get_Volume_UI(float* Value);
	void Get_Audio_Multiplier_UI(float* Value);
	void Set_Audio_Multiplier_UI(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_UI(TEnumAsByte<EModifySetting> Modify);
	void Save_ini_Settings();
	void Load_ini_Settings();
	void Init_Save_Game_Settings(const TScriptInterface<class UBPI_GameSettingsInterface_C>& Game_Settings_Interface);
	void Get_Saved_Key_Inputs(TArray<struct FSKeyActionSave>* Saved_Key_Inputs);
	void Get_All_Key_Actions(TArray<class UBP_KeyAction_C*>* Key_Actions);
	void Set_Save_File_User_Index(int Save_File_User_Index);
	void Set_Save_File_Name(const struct FString& Save_File_Name);
	void Set_Game_Settings_Interface(const TScriptInterface<class UBPI_GameSettingsInterface_C>& Game_Settings_Interface);
	void Get_All_Combinations(TArray<class UBP_KeyCombination_C*>* Combinations);
	void Generate_Keybinding_Conflicts();
	void Modify_Keybindings(TEnumAsByte<EModifySetting> Modify);
	void Load_Key_Actions();
	void Save_Key_Actions();
	void Store_Key_Input(const struct FSKeyActionSave& KeySave);
	void Get_Key_Action(const struct FString& Input_Action_Name, class UBP_KeyAction_C** Input_Action, bool* Success);
	void Fill_Float_Axis_Inputs_List();
	void Init_Key_Bindings();
	void Update_Actions_Input_State(float Real_Time_Seconds, float World_Delta_Seconds, class APlayerController* PlayerController);
	void Delete_Settings_Save_File();
	void Get_Look_Sensitivity_Combined_X(float Input_Axis_X, float World_Delta, float* Horizontal_X);
	void Get_My_Custom_Combobox(struct FString* Value);
	void Set_My_Custom_Combobox(const struct FString& Value, bool Apply, struct FString* Result);
	void Modify_My_Custom_Combobox(TEnumAsByte<EModifySetting> Modify);
	void Get_My_Custom_Radiobox(int* Value);
	void Set_My_Custom_Radiobox(int Value, bool Apply, int* Result);
	void Modify_My_Custom_Radiobox(TEnumAsByte<EModifySetting> Modify);
	void Get_My_Custom_Slider(float* Value);
	void Set_My_Custom_Slider(float Value, bool Apply, float* Result);
	void Modify_My_Custom_Slider(TEnumAsByte<EModifySetting> Modify);
	void Get_My_Custom_Checkbox(bool* Value);
	void Set_My_Custom_Checkbox(bool Value, bool Apply, bool* Result);
	void Modify_My_Custom_Checkbox(TEnumAsByte<EModifySetting> Modify);
	void Modify_All_MyCustom_Settings(TEnumAsByte<EModifySetting> Modify);
	void Get_Resolution_Scale(int* Value);
	void Set_Resolution_Scale(int Value, bool Apply, int* Result);
	void Modify_Resolution_Scale(TEnumAsByte<EModifySetting> Modify);
	void Prepeare_Previus_Settings_State();
	void Save_All_Settings();
	void Modify_All_Settings(TEnumAsByte<EModifySetting> Modify);
	void Modify_All_Audio_Settings(TEnumAsByte<EModifySetting> Modify);
	void Modify_All_Look_Settings(TEnumAsByte<EModifySetting> Modify);
	void Get_Look_Sensitivity_Combined_Y(float Input_Axis_Y, float World_Delta, float* Vertical_Y);
	void Get_Volume_Ambient(float* Value);
	void Get_Audio_Multiplier_Ambient(float* Value);
	void Set_Audio_Multiplier_Ambient(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_Ambient(TEnumAsByte<EModifySetting> Modify);
	void Get_Volume_Effect(float* Value);
	void Get_Audio_Multiplier_Effect(float* Value);
	void Set_Audio_Multiplier_Effect(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_Effect(TEnumAsByte<EModifySetting> Modify);
	void Get_Volume_Voice(float* Value);
	void Get_Audio_Multiplier_Voice(float* Value);
	void Set_Audio_Multiplier_Voice(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_Voice(TEnumAsByte<EModifySetting> Modify);
	void Get_Volume_Music(float* Value);
	void Get_Audio_Multiplier_Music(float* Value);
	void Set_Audio_Multiplier_Music(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_Music(TEnumAsByte<EModifySetting> Modify);
	void Get_Volume_Master(float* Value);
	void Add_Volume_Control(class UAudioComponent* Audio_Emittor, TEnumAsByte<EAudioType> Audio_Channel);
	void Apply_Audio_Settings(TEnumAsByte<EAudioType> Audio_Channel);
	void Get_Audio_Multiplier_Master(float* Value);
	void Set_Audio_Multiplier_Master(float Set_Value, bool Apply);
	void Modify_Audio_Multiplier_Master(TEnumAsByte<EModifySetting> Modify);
	void Get_Look_Vertical_Invert(bool* Value);
	void Set_Look_Vertical_Invert(bool Set_Value);
	void Modify_Look_Vertical_Invert(TEnumAsByte<EModifySetting> Modify);
	void Get_Look_Horizontal_Invert(bool* Value);
	void Set_Look_Horizontal_Invert(bool Set_Value);
	void Modify_Look_Horizontal_Invert(TEnumAsByte<EModifySetting> Modify);
	void Get_Look_Vertical_Sensitivity(float* Value);
	void Set_Look_Vertical_Sensitivity(float Set_Value);
	void Modify_Look_Vertical_Sensitivity(TEnumAsByte<EModifySetting> Modify);
	void Get_Look_Horizontal_Sensitivity(float* Value);
	void Set_Look_Horizontal_Sensitivity(float Set_Value);
	void Modify_Look_Horizontal_Sensitivity(TEnumAsByte<EModifySetting> Modify);
	void Apply_Screen_Settings();
	void Modify_Screen_Mode(TEnumAsByte<EModifySetting> Modify);
	void Get_Screen_Mode(TEnumAsByte<EWindowMode>* Screen_Mode, struct FString* Command);
	void Get_Screen_Resolution(struct FSVideoResolution* Resolution);
	void Set_Screen_Mode(TEnumAsByte<EWindowMode> Screen_Mode, bool Apply, TEnumAsByte<EWindowMode>* Result);
	void Set_Screen_Resolution(const struct FSVideoResolution& Resolution, bool Apply, struct FSVideoResolution* Result);
	void Modify_Screen_Resolution(TEnumAsByte<EModifySetting> Modify);
	void Get_Motion_Blur_Strength(float* Value);
	void Set_Motion_Blur_Strength(float Value, bool Apply, float* Result);
	void Modify_Motion_Blur_Strength(TEnumAsByte<EModifySetting> Modify);
	void Get_Gain_Intensity(float* Value);
	void Set_Gain_Intensity(float Value, bool Apply, float* Result);
	void Modify_Gain_Intensity(TEnumAsByte<EModifySetting> Modify);
	void Get_Gamma_Intensity(float* Value);
	void Set_Gamma_Intensity(float Value, bool Apply, float* Result);
	void Modify_Gamma_Intensity(TEnumAsByte<EModifySetting> Modify);
	void Get_Bloom_Intensity(float* Value);
	void Set_Bloom_Intensity(float Value, bool Apply, float* Result);
	void Modify_Bloom_Intensity(TEnumAsByte<EModifySetting> Modify);
	void Get_Vsync(bool* Value);
	void Set_Vsync(bool Value, bool Apply, bool* Result);
	void Modify_Vsync(TEnumAsByte<EModifySetting> Modify);
	void Remove_Field_Of_View_Control_From_Camera(class UCameraComponent* Camera);
	void Add_Field_Of_View_Control_To_Camera(class UCameraComponent* Camera);
	void Get_Field_Of_View(float* Value);
	void Set_Field_Of_View(float Value, bool Apply, float* Result);
	void Modify_Field_Of_View(TEnumAsByte<EModifySetting> Modify);
	void Get_View_Distance(float* Value);
	void Set_View_Distance(float Value, bool Apply, float* Result);
	void Modify_View_Distance(TEnumAsByte<EModifySetting> Modify);
	void Get_Effect_Quality(int* Value, struct FString* Formatted);
	void Set_Effect_Quality(int Value, bool Apply, int* Result);
	void Modify_Effect_Quality(TEnumAsByte<EModifySetting> Modify);
	void Get_Texture_Quality(int* Value, struct FString* Formatted);
	void Set_Texture_Quality(int Value, bool Apply, int* Result);
	void Modify_Texture_Quality(TEnumAsByte<EModifySetting> Modify);
	void Get_Shadow_Quality(int* Value, struct FString* Formatted);
	void Set_Shadow_Quality(int Value, bool Apply, int* Result);
	void Modify_Shadow_Quality(TEnumAsByte<EModifySetting> Modify);
	void Modify_All_Video_Settings(TEnumAsByte<EModifySetting> Modify);
	void Get_Anti_Aliasing_Quality(int* Value, struct FString* Formatted);
	void Set_Anti_Aliasing_Quality(int Value, bool Apply, int* Result);
	void Modify_Anti_Aliasing_Quality(TEnumAsByte<EModifySetting> Modify);
	void Get_Text_Format_Quality_Level(int Quality_Level, struct FString* Formatted);
	void Get_Post_Process_Quality(int* Value, struct FString* Formatted);
	void Set_Post_Process_Quality(int Value, bool Apply, int* Result);
	void Modify_Post_Process_Quality(TEnumAsByte<EModifySetting> Modify);
	void Update_Audio_Emittor(const struct FSAudioUpdateStruct& Emittor, bool* Is_Valid);
	void Create_Clone(class UBP_GameSettings_C** Cloned_Game_Settings);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
