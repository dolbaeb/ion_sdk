// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_DebugMenuButton_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.PreConstruct");

	UWBP_DebugMenuButton_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_DebugMenuButton_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Construct");

	UWBP_DebugMenuButton_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_DebugMenuButton_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature");

	UWBP_DebugMenuButton_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Reset
// (BlueprintCallable, BlueprintEvent)

void UWBP_DebugMenuButton_C::Reset()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Reset");

	UWBP_DebugMenuButton_C_Reset_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Select
// (BlueprintCallable, BlueprintEvent)

void UWBP_DebugMenuButton_C::Select()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.Select");

	UWBP_DebugMenuButton_C_Select_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.ExecuteUbergraph_WBP_DebugMenuButton
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_DebugMenuButton_C::ExecuteUbergraph_WBP_DebugMenuButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.ExecuteUbergraph_WBP_DebugMenuButton");

	UWBP_DebugMenuButton_C_ExecuteUbergraph_WBP_DebugMenuButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.On Clicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_DebugMenuButton_C*  Object_To_Ignore               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_DebugMenuButton_C::On_Clicked__DelegateSignature(class UWBP_DebugMenuButton_C* Object_To_Ignore)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenuButton.WBP_DebugMenuButton_C.On Clicked__DelegateSignature");

	UWBP_DebugMenuButton_C_On_Clicked__DelegateSignature_Params params;
	params.Object_To_Ignore = Object_To_Ignore;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
