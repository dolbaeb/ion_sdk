#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_Sako85.BP_Sako85_C
// 0x0008 (0x12B8 - 0x12B0)
class ABP_Sako85_C : public AIONSniperRifle
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x12B0(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_Sako85.BP_Sako85_C");
		return ptr;
	}


	struct FVector GetPickupLocation();
	void UserConstructionScript();
	void ReceiveTick(float* DeltaSeconds);
	void ExecuteUbergraph_BP_Sako85(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
