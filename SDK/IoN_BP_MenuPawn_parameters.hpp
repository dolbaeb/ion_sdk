#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_MenuPawn.BP_MenuPawn_C.StartCrateAnim
struct ABP_MenuPawn_C_StartCrateAnim_Params
{
	class UWBP_CrateScreen_C*                          CurrentCrateScreen;                                       // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              TotalDistance;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.CleanupWearableItem
struct ABP_MenuPawn_C_CleanupWearableItem_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.DoWeaponRotation
struct ABP_MenuPawn_C_DoWeaponRotation_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.CleanupCrateItem
struct ABP_MenuPawn_C_CleanupCrateItem_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.CleanupDisplayItems
struct ABP_MenuPawn_C_CleanupDisplayItems_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.SwitchToSkinCam
struct ABP_MenuPawn_C_SwitchToSkinCam_Params
{
	bool                                               bSwitch;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.HideEquippedItems
struct ABP_MenuPawn_C_HideEquippedItems_Params
{
	bool                                               HideItems;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.ApplyWeaponSkin
struct ABP_MenuPawn_C_ApplyWeaponSkin_Params
{
	class UIONWeaponSkin*                              NewSkin;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.TickObjectTransforms
struct ABP_MenuPawn_C_TickObjectTransforms_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.ChangeZoom
struct ABP_MenuPawn_C_ChangeZoom_Params
{
	int                                                ZoomLevel;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.ZoomIn
struct ABP_MenuPawn_C_ZoomIn_Params
{
	bool                                               In;                                                       // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.TickCameraPosition
struct ABP_MenuPawn_C_TickCameraPosition_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.UserConstructionScript
struct ABP_MenuPawn_C_UserConstructionScript_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_Ctrl+Shift_K_K2Node_InputKeyEvent_3
struct ABP_MenuPawn_C_InpActEvt_Ctrl_Shift_K_K2Node_InputKeyEvent_3_Params
{
	struct FKey                                        Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2
struct ABP_MenuPawn_C_InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2_Params
{
	struct FKey                                        Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_MenuPawn.BP_MenuPawn_C.InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1
struct ABP_MenuPawn_C_InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1_Params
{
	struct FKey                                        Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_SkinsDebug
struct ABP_MenuPawn_C_BPEvent_SkinsDebug_Params
{
	int*                                               Mode;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1
struct ABP_MenuPawn_C_InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1_Params
{
	float                                              AxisValue;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4
struct ABP_MenuPawn_C_InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4_Params
{
	float                                              AxisValue;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.ReceiveBeginPlay
struct ABP_MenuPawn_C_ReceiveBeginPlay_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.ReceiveTick
struct ABP_MenuPawn_C_ReceiveTick_Params
{
	float*                                             DeltaSeconds;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.OpenCrateAnimation
struct ABP_MenuPawn_C_OpenCrateAnimation_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectWeaponSkin
struct ABP_MenuPawn_C_BPEvent_InspectWeaponSkin_Params
{
	class UIONWeaponSkin**                             WeaponSkin;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.LoadoutLoaded
struct ABP_MenuPawn_C_LoadoutLoaded_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.ShowDefaultChar
struct ABP_MenuPawn_C_ShowDefaultChar_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectCrate
struct ABP_MenuPawn_C_BPEvent_InspectCrate_Params
{
	struct FIONSteamInventoryItem*                     Crate;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function BP_MenuPawn.BP_MenuPawn_C.PlayGoldEffect
struct ABP_MenuPawn_C_PlayGoldEffect_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.StopGoldEffect
struct ABP_MenuPawn_C_StopGoldEffect_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectArmor
struct ABP_MenuPawn_C_BPEvent_InspectArmor_Params
{
	class UIONArmorSkin**                              ArmorSkin;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_InspectDropsuit
struct ABP_MenuPawn_C_BPEvent_InspectDropsuit_Params
{
	class UIONDropSkin**                               DropSkin;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.BPEvent_PreviewWeaponSkin
struct ABP_MenuPawn_C_BPEvent_PreviewWeaponSkin_Params
{
	class AIONWeapon**                                 Weapon;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MenuPawn.BP_MenuPawn_C.CrateTransitionToOpening
struct ABP_MenuPawn_C_CrateTransitionToOpening_Params
{
};

// Function BP_MenuPawn.BP_MenuPawn_C.ExecuteUbergraph_BP_MenuPawn
struct ABP_MenuPawn_C_ExecuteUbergraph_BP_MenuPawn_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
