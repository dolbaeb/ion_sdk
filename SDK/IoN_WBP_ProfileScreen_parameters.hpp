#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ProfileScreen.WBP_ProfileScreen_C.OnShow
struct UWBP_ProfileScreen_C_OnShow_Params
{
};

// Function WBP_ProfileScreen.WBP_ProfileScreen_C.Construct
struct UWBP_ProfileScreen_C_Construct_Params
{
};

// Function WBP_ProfileScreen.WBP_ProfileScreen_C.ExecuteUbergraph_WBP_ProfileScreen
struct UWBP_ProfileScreen_C_ExecuteUbergraph_WBP_ProfileScreen_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
