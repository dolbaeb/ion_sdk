#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_KeyInput.BP_KeyInput_C.Generate Display Name
struct UBP_KeyInput_C_Generate_Display_Name_Params
{
};

// Function BP_KeyInput.BP_KeyInput_C.Save Key Input
struct UBP_KeyInput_C_Save_Key_Input_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FSKeyActionSave                             KeySave;                                                  // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_KeyInput.BP_KeyInput_C.Update Analog Axis Value
struct UBP_KeyInput_C_Update_Analog_Axis_Value_Params
{
	float                                              World_Delta_Seconds;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class APlayerController*                           Player_Controller;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyInput.BP_KeyInput_C.Key Input Current State
struct UBP_KeyInput_C_Key_Input_Current_State_Params
{
	class APlayerController*                           Controller;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Axis_Value;                                               // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Down;                                                     // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyInput.BP_KeyInput_C.Init Key Input
struct UBP_KeyInput_C_Init_Key_Input_Params
{
	struct FSKeyInput                                  Key_Input;                                                // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UBP_KeyInput_C*                              Input;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
