#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function ABP_Sako85.ABP_Sako85_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105
struct UABP_Sako85_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105_Params
{
};

// Function ABP_Sako85.ABP_Sako85_C.BlueprintUpdateAnimation
struct UABP_Sako85_C_BlueprintUpdateAnimation_Params
{
	float*                                             DeltaTimeX;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function ABP_Sako85.ABP_Sako85_C.ExecuteUbergraph_ABP_Sako85
struct UABP_Sako85_C_ExecuteUbergraph_ABP_Sako85_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
