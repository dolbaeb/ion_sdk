#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_InvertVerticalSetting.WBP_InvertVerticalSetting_C.Get_CheckBox_CheckedState_1
struct UWBP_InvertVerticalSetting_C_Get_CheckBox_CheckedState_1_Params
{
	ECheckBoxState                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_InvertVerticalSetting.WBP_InvertVerticalSetting_C.BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_InvertVerticalSetting_C_BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_InvertVerticalSetting.WBP_InvertVerticalSetting_C.ExecuteUbergraph_WBP_InvertVerticalSetting
struct UWBP_InvertVerticalSetting_C_ExecuteUbergraph_WBP_InvertVerticalSetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
