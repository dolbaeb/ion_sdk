#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.KeyToWindowMode
struct UWBP_ResolutionSetting_C_KeyToWindowMode_Params
{
	struct FString                                     Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<EWindowMode>                           ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxScreenMode_GenerateWidget_1
struct UWBP_ResolutionSetting_C_On_ComboBoxScreenMode_GenerateWidget_1_Params
{
	struct FString                                     Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.On_ComboBoxResolutions_GenerateWidget_1
struct UWBP_ResolutionSetting_C_On_ComboBoxResolutions_GenerateWidget_1_Params
{
	struct FString                                     Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ResolutionToText
struct UWBP_ResolutionSetting_C_ResolutionToText_Params
{
	int                                                X;                                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Y;                                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.RefreshResolution
struct UWBP_ResolutionSetting_C_RefreshResolution_Params
{
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Construct
struct UWBP_ResolutionSetting_C_Construct_Params
{
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature
struct UWBP_ResolutionSetting_C_BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_171_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature
struct UWBP_ResolutionSetting_C_BndEvt__ComboBoxScreenMode_K2Node_ComponentBoundEvent_294_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.Tick
struct UWBP_ResolutionSetting_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature
struct UWBP_ResolutionSetting_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_293_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ResolutionSetting.WBP_ResolutionSetting_C.ExecuteUbergraph_WBP_ResolutionSetting
struct UWBP_ResolutionSetting_C_ExecuteUbergraph_WBP_ResolutionSetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
