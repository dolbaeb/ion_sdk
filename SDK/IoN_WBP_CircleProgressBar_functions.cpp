// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Ring Size
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          New_Ring_Size                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Ring_Size(float New_Ring_Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Ring Size");

	UWBP_CircleProgressBar_C_Update_Ring_Size_Params params;
	params.New_Ring_Size = New_Ring_Size;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Clockwise
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           New_Clockwise                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Clockwise(bool New_Clockwise)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Clockwise");

	UWBP_CircleProgressBar_C_Update_Clockwise_Params params;
	params.New_Clockwise = New_Clockwise;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Step
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           New_Step                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          New_Step_Ammount               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Step(bool New_Step, float New_Step_Ammount)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Step");

	UWBP_CircleProgressBar_C_Update_Step_Params params;
	params.New_Step = New_Step;
	params.New_Step_Ammount = New_Step_Ammount;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Space
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           New_Space                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          New_Space_Size                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Space(bool New_Space, float New_Space_Size)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Space");

	UWBP_CircleProgressBar_C_Update_Space_Params params;
	params.New_Space = New_Space;
	params.New_Space_Size = New_Space_Size;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Empty Color
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FLinearColor            New_Color                      (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Empty_Color(const struct FLinearColor& New_Color)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Empty Color");

	UWBP_CircleProgressBar_C_Update_Empty_Color_Params params;
	params.New_Color = New_Color;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Fill Color
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FLinearColor            New_Color                      (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Fill_Color(const struct FLinearColor& New_Color)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Fill Color");

	UWBP_CircleProgressBar_C_Update_Fill_Color_Params params;
	params.New_Color = New_Color;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Percentage
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          New_Percentage                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::Update_Percentage(float New_Percentage)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Percentage");

	UWBP_CircleProgressBar_C_Update_Percentage_Params params;
	params.New_Percentage = New_Percentage;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.PreConstruct");

	UWBP_CircleProgressBar_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CircleProgressBar_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Construct");

	UWBP_CircleProgressBar_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.ExecuteUbergraph_WBP_CircleProgressBar
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CircleProgressBar_C::ExecuteUbergraph_WBP_CircleProgressBar(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.ExecuteUbergraph_WBP_CircleProgressBar");

	UWBP_CircleProgressBar_C_ExecuteUbergraph_WBP_CircleProgressBar_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
