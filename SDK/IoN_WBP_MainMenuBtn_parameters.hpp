#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.PreConstruct
struct UWBP_MainMenuBtn_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature
struct UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_60_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature
struct UWBP_MainMenuBtn_C_BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.ExecuteUbergraph_WBP_MainMenuBtn
struct UWBP_MainMenuBtn_C_ExecuteUbergraph_WBP_MainMenuBtn_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MainMenuBtn.WBP_MainMenuBtn_C.BtnClicked__DelegateSignature
struct UWBP_MainMenuBtn_C_BtnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
