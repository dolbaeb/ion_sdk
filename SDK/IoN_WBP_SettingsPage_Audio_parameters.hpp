#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Construct
struct UWBP_SettingsPage_Audio_C_Construct_Params
{
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxInputMode_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxInputDevices_K2Node_ComponentBoundEvent_24_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Audio_C_BndEvt__ComboBoxOutputDevices_K2Node_ComponentBoundEvent_29_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.Refresh Input
struct UWBP_SettingsPage_Audio_C_Refresh_Input_Params
{
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature
struct UWBP_SettingsPage_Audio_C_BndEvt__WBP_InputVolumeSlider_K2Node_ComponentBoundEvent_20_OnMouseEventEnd__DelegateSignature_Params
{
	float                                              Slide_Value;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature
struct UWBP_SettingsPage_Audio_C_BndEvt__WBP_OutputVolumeSlider_K2Node_ComponentBoundEvent_1_OnMouseEventEnd__DelegateSignature_Params
{
	float                                              Slide_Value;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Audio.WBP_SettingsPage_Audio_C.ExecuteUbergraph_WBP_SettingsPage_Audio
struct UWBP_SettingsPage_Audio_C_ExecuteUbergraph_WBP_SettingsPage_Audio_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
