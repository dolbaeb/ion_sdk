#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_PlayerCameraManager.BP_PlayerCameraManager_C
// 0x0009 (0x1AF9 - 0x1AF0)
class ABP_PlayerCameraManager_C : public AMainCameraManager
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x1AF0(0x0008) (Transient, DuplicateTransient)
	bool                                               bInPhotoMode;                                             // 0x1AF8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_PlayerCameraManager.BP_PlayerCameraManager_C");
		return ptr;
	}


	void PhotographyCameraModify(struct FVector* NewCameraLocation, struct FVector* PreviousCameraLocation, struct FVector* OriginalCameraLocation, struct FVector* ResultCameraLocation);
	void UserConstructionScript();
	void OnPhotographySessionStart();
	void OnPhotographySessionEnd();
	void ExecuteUbergraph_BP_PlayerCameraManager(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
