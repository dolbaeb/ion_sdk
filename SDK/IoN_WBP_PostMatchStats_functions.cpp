// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__TeamButton_K2Node_ComponentBoundEvent_92_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__PlaceButton_K2Node_ComponentBoundEvent_110_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__KillsButton_K2Node_ComponentBoundEvent_128_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__DamageButton_K2Node_ComponentBoundEvent_147_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Highlight Text
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UTextBlock*              Text                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_PostMatchStats_C::Highlight_Text(class UTextBlock* Text)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Highlight Text");

	UWBP_PostMatchStats_C_Highlight_Text_Params params;
	params.Text = Text;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Unhighlight All Text
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchStats_C::Unhighlight_All_Text()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Unhighlight All Text");

	UWBP_PostMatchStats_C_Unhighlight_All_Text_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__HsButton_K2Node_ComponentBoundEvent_203_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__AccuracyButton_K2Node_ComponentBoundEvent_237_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__TimeAliveButton_K2Node_ComponentBoundEvent_254_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__TeamsButton_K2Node_ComponentBoundEvent_325_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PostMatchStats_C::BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature");

	UWBP_PostMatchStats_C_BndEvt__SoloButton_K2Node_ComponentBoundEvent_342_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Update Result Text
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchStats_C::Update_Result_Text()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Update Result Text");

	UWBP_PostMatchStats_C_Update_Result_Text_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Change Teams View
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           View_Teams                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchStats_C::Change_Teams_View(bool View_Teams)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Change Teams View");

	UWBP_PostMatchStats_C_Change_Teams_View_Params params;
	params.View_Teams = View_Teams;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PostMatchStats_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Construct");

	UWBP_PostMatchStats_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchStats_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.Tick");

	UWBP_PostMatchStats_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchStats.WBP_PostMatchStats_C.ExecuteUbergraph_WBP_PostMatchStats
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchStats_C::ExecuteUbergraph_WBP_PostMatchStats(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchStats.WBP_PostMatchStats_C.ExecuteUbergraph_WBP_PostMatchStats");

	UWBP_PostMatchStats_C_ExecuteUbergraph_WBP_PostMatchStats_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
