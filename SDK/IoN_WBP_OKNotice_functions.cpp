// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_OKNotice.WBP_OKNotice_C.SetMessage
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_OKNotice_C::SetMessage(const struct FString& Message)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_OKNotice.WBP_OKNotice_C.SetMessage");

	UWBP_OKNotice_C_SetMessage_Params params;
	params.Message = Message;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_OKNotice.WBP_OKNotice_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_OKNotice_C::BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_OKNotice.WBP_OKNotice_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature");

	UWBP_OKNotice_C_BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_OKNotice.WBP_OKNotice_C.ExecuteUbergraph_WBP_OKNotice
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_OKNotice_C::ExecuteUbergraph_WBP_OKNotice(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_OKNotice.WBP_OKNotice_C.ExecuteUbergraph_WBP_OKNotice");

	UWBP_OKNotice_C_ExecuteUbergraph_WBP_OKNotice_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_OKNotice.WBP_OKNotice_C.AcceptBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_OKNotice_C::AcceptBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_OKNotice.WBP_OKNotice_C.AcceptBtnClicked__DelegateSignature");

	UWBP_OKNotice_C_AcceptBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
