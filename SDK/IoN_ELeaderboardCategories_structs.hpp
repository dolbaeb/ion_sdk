#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum ELeaderboardCategories.ELeaderboardCategories
enum class ELeaderboardCategories : uint8_t
{
	ELeaderboardCategories__NewEnumerator1 = 0,
	ELeaderboardCategories__NewEnumerator6 = 1,
	ELeaderboardCategories__NewEnumerator7 = 2,
	ELeaderboardCategories__NewEnumerator2 = 3,
	ELeaderboardCategories__NewEnumerator3 = 4,
	ELeaderboardCategories__ELeaderboardCategories_MAX = 5
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
