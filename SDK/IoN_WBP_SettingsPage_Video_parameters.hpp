#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.On_ComboBoxResolutions_GenerateWidget_1
struct UWBP_SettingsPage_Video_C_On_ComboBoxResolutions_GenerateWidget_1_Params
{
	struct FString                                     Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.Construct
struct UWBP_SettingsPage_Video_C_Construct_Params
{
};

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Video_C_BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature
struct UWBP_SettingsPage_Video_C_BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature_Params
{
	struct FString                                     SelectedItem;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	TEnumAsByte<ESelectInfo>                           SelectionType;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.ExecuteUbergraph_WBP_SettingsPage_Video
struct UWBP_SettingsPage_Video_C_ExecuteUbergraph_WBP_SettingsPage_Video_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
