// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Friend.WBP_Friend_C.GetbIsEnabled_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

bool UWBP_Friend_C::GetbIsEnabled_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.GetbIsEnabled_1");

	UWBP_Friend_C_GetbIsEnabled_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Friend.WBP_Friend_C.OnGetMenuContent_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_Friend_C::OnGetMenuContent_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.OnGetMenuContent_1");

	UWBP_Friend_C_OnGetMenuContent_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Friend.WBP_Friend_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_Friend_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.GetText_1");

	UWBP_Friend_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Friend.WBP_Friend_C.Get_Button_Invite_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_Friend_C::Get_Button_Invite_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.Get_Button_Invite_Visibility_1");

	UWBP_Friend_C_Get_Button_Invite_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Friend.WBP_Friend_C.GetColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateColor             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateColor UWBP_Friend_C::GetColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.GetColorAndOpacity_1");

	UWBP_Friend_C_GetColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_Friend_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature");

	UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.SetFriend
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FBlueprintOnlineFriend  Friend                         (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_Friend_C::SetFriend(const struct FBlueprintOnlineFriend& Friend)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.SetFriend");

	UWBP_Friend_C_SetFriend_Params params;
	params.Friend = Friend;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_Friend_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature");

	UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_21_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_Friend_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature");

	UWBP_Friend_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_54_OnButtonHoverEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_Friend_C::BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature");

	UWBP_Friend_C_BndEvt__AddToPartyBtn_K2Node_ComponentBoundEvent_124_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.ExecuteUbergraph_WBP_Friend
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Friend_C::ExecuteUbergraph_WBP_Friend(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.ExecuteUbergraph_WBP_Friend");

	UWBP_Friend_C_ExecuteUbergraph_WBP_Friend_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Friend.WBP_Friend_C.AddToPartyBtnPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FBlueprintOnlineFriend  OnlineFriend                   (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_Friend_C::AddToPartyBtnPressed__DelegateSignature(const struct FBlueprintOnlineFriend& OnlineFriend)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Friend.WBP_Friend_C.AddToPartyBtnPressed__DelegateSignature");

	UWBP_Friend_C_AddToPartyBtnPressed__DelegateSignature_Params params;
	params.OnlineFriend = OnlineFriend;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
