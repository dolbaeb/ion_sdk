#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_Character.ABP_Character_C
// 0x35EA (0x39EA - 0x0400)
class UABP_Character_C : public UIONCharacterAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0400(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_3D8A76BE45558E8961C0DB8FF3C4EA43;      // 0x0408(0x0048)
	struct FAnimNode_LayeredBoneBlend                  AnimGraphNode_LayeredBoneBlend_663E1DF04D2A620EC17A219EF6C3C440;// 0x0450(0x00E0)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_4AA5E00A4468BC19C88D5DBEACD2B80D;// 0x0530(0x0070)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_88C2A05848D72F3830A174BD66D8958C;      // 0x05A0(0x0068)
	unsigned char                                      UnknownData00[0x8];                                       // 0x0608(0x0008) MISSED OFFSET
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_A218AD9E4D7AA22CF061499CF7429DDD;// 0x0610(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_CF4CB1084CFBD903CD389C8966DEA83D;// 0x0690(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_9C6C5EA2420845480A78F38CF19E08B3;// 0x0710(0x0080)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_9DAE30484B41FCF00132F8AAFE03AAC2;// 0x0790(0x0070)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_9D18F2744F3F6E9E40DBA68B93BF584F;// 0x0800(0x0070)
	struct FAnimNode_TwoWayBlend                       AnimGraphNode_TwoWayBlend_B66901B84D0214FF6EF6ABAE8E2BCC26;// 0x0870(0x0078)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_5422F916478B652F48E5469B4AFDB19F;// 0x08E8(0x0048)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_126B54E44C6C1592300F399E5DED6490;// 0x0930(0x0070)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_C2948F464CD41D72B3A420A6CB00EB06;// 0x09A0(0x00D0)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_C8A59C9F4223ED9FFCB22E93F791120D;// 0x0A70(0x0070)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_FAE2C3DF4F5BF93526985B85D4A745FE;// 0x0AE0(0x0048)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0B28(0x0008) MISSED OFFSET
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_7EB78AFB4C8B455B0F16C4824885CDF1;// 0x0B30(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_11F14D89456CCE731407B3A58815AA50;// 0x0BB0(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_6F4F950A4518568BD1A2AB8A55BE7284;// 0x0C30(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_567952294D7AB2381A9C4982026A2C99;// 0x0CB0(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_58453E484F6D8C8CB29F7E9CC763F68D;// 0x0D30(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_D2C5D4484CE5E0CEF40AF6A1A68BA432;// 0x0DB0(0x0080)
	struct FAnimNode_TransitionResult                  AnimGraphNode_TransitionResult_CC7A2E974EE99A5266B64DBAC0320538;// 0x0E30(0x0080)
	struct FAnimNode_BlendSpacePlayer                  AnimGraphNode_BlendSpacePlayer_5F44426C4C69B9908310AF9887A563A7;// 0x0EB0(0x0128)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_6F2056CE43BDBE587B48D7BB73F5FF63;// 0x0FD8(0x0048)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_3E1250434D5A42B8887EA59C01DF3884;// 0x1020(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_1151CF9B4E3FB071C960ED8713C10764;// 0x1068(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_4B9D3E164E78949FF16FE5941A1B6856;// 0x1120(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_34E6BDA94928105FADA3F28F8EDB65E2;// 0x11D8(0x00B8)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_A709FE564E4B149E220468AB19610E3E;// 0x1290(0x0048)
	struct FAnimNode_BlendSpacePlayer                  AnimGraphNode_BlendSpacePlayer_114A687A4A45E355D9A497A0FD234560;// 0x12D8(0x0128)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_942A26C44F558A45FA478A9B1F04AB3B;// 0x1400(0x0048)
	struct FAnimNode_BlendSpacePlayer                  AnimGraphNode_BlendSpacePlayer_C02CCEDB47A861F66ECD9F9552CF9D1E;// 0x1448(0x0128)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_33E7D3404E0C8726B29CCBA792DD5798;// 0x1570(0x00D0)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_A26FC299476055A94C292192C3BCED5F;// 0x1640(0x0070)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_C898AD564CA1F2BD23A9759F1294A5DB;// 0x16B0(0x0048)
	struct FAnimNode_StateMachine                      AnimGraphNode_StateMachine_5D851D124316EC53373FDAAA1F71938B;// 0x16F8(0x00D8)
	struct FAnimNode_Root                              AnimGraphNode_StateResult_C69E0D7D45839056EF03D49D6E11173F;// 0x17D0(0x0048)
	struct FAnimNode_StateMachine                      AnimGraphNode_StateMachine_C390BD244F08913FE808FEBADC496B1C;// 0x1818(0x00D8)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_0D5944074C95F63538EAC9B22F6D9CC7;      // 0x18F0(0x0068)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_C2DF40AD46FA680D218EC3916A525634;// 0x1958(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_CE92A2364DEC622AFE949EAC9E74DAE0;// 0x1A00(0x0050)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_6D94C130457FC9B81AB11BA4750CBC2E;// 0x1A50(0x0050)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_311EAC43434407881D0A48B8682933C1;      // 0x1AA0(0x0068)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_D11D20B14C704BEB80BE258A257A5C90;// 0x1B08(0x0070)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_1E84472F4E71424F57C654871897F2D4;// 0x1B78(0x00D0)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_0393C0C7475ACE2EA97D0FA1FC3CE0BD;// 0x1C48(0x00D0)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_32E61D9F4D062C065F65FB9D18C4351C;// 0x1D18(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_D86211064F0DDD989598C7884C827DC6;// 0x1DC0(0x0050)
	struct FAnimNode_TwoBoneIK                         AnimGraphNode_TwoBoneIK_8C4D2DEB4C5133ABF72029B1DB29343F; // 0x1E10(0x01C0)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_971808C247AE1AE3FEB2B99C91F47E69;// 0x1FD0(0x0048)
	unsigned char                                      UnknownData02[0x8];                                       // 0x2018(0x0008) MISSED OFFSET
	struct FAnimNode_TwoBoneIK                         AnimGraphNode_TwoBoneIK_D38EEFD8442C5DD1B684C28A2ED0EF94; // 0x2020(0x01C0)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_19BFAD1E4A8E6EA387120FA9DEF9AEFD;// 0x21E0(0x00A8)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_7F143A02492F6995571738B92A607AD9;// 0x2288(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_F8A1231B4D2CD4B2C4D8269DA16EB632;// 0x22D0(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_1B9B2FD34A197F1167F0D4B789A24A61;// 0x2388(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_2D0FFA9F4282E620D0EAD682577A5469;// 0x2440(0x00B8)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_8B1E613F41424D2AD1DBAB8B4AD09B22;// 0x24F8(0x0048)
	struct FAnimNode_LayeredBoneBlend                  AnimGraphNode_LayeredBoneBlend_1D9FAD9A43B21FE022832092D5D3DC56;// 0x2540(0x00E0)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_2FCF1B58445D5F96AB7340B7B363916B;// 0x2620(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_908CECA1440311E0A3061EB8FB59500A;// 0x26D8(0x00B8)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_6F7A61FD425778F2D982A59B28FE2F5D;// 0x2790(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_1997F8BC4EB6868FBA91728CE817E432;// 0x2838(0x0050)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_123A054E46BF5EA6AE9AD2A8F66D5356;// 0x2888(0x00D0)
	struct FAnimNode_LayeredBoneBlend                  AnimGraphNode_LayeredBoneBlend_B86275774620EDAFDD37BFB48BE47C53;// 0x2958(0x00E0)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_C480F2CB432DA36DFB150E934FA588FC;// 0x2A38(0x0050)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_E17EFA404D2CD613D9EF32B54C7BCE74;// 0x2A88(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_7B1B14C84CF0370246C9F1B95FAD21E5;// 0x2B30(0x0050)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_D84DE6AA46C153E380D10C9CAE4681F3;// 0x2B80(0x0050)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_CFBCA2764CCDEFD2F2F08282A81EDC5C;// 0x2BD0(0x0070)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_E287B8EC4198A85FA9C6A7B42AE8B628;// 0x2C40(0x00D0)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_985A7D5B47C9D5F560171AA23F8C2E78;// 0x2D10(0x0050)
	struct FAnimNode_LayeredBoneBlend                  AnimGraphNode_LayeredBoneBlend_A9B18E2C4601128E85B91DB62CCE3A70;// 0x2D60(0x00E0)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_4F92E5F94316BADCB3F7C087A02A56BD;// 0x2E40(0x00D0)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_A64628164E999C54075D908D2339A00F;// 0x2F10(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_1DE47EB84957585BD274A2A212B4C443;// 0x2F58(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_9F64688F46FD7E5345B03F8D47C42742;// 0x3010(0x00B8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_206AEE9A42119E3D663DE58FB3A580C2;// 0x30C8(0x0050)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_A6829D8D478843906CE1E99845EAB726;// 0x3118(0x0050)
	struct FAnimNode_BlendSpacePlayer                  AnimGraphNode_BlendSpacePlayer_A9E2E60A467F5C807EFDEBB559972646;// 0x3168(0x0128)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_19FDEBE046D552C747D87396278914BA;// 0x3290(0x00D0)
	struct FAnimNode_BlendSpacePlayer                  AnimGraphNode_BlendSpacePlayer_B6F044FF4099D362C12E639FC4E0B68C;// 0x3360(0x0128)
	struct FAnimNode_BlendListByBool                   AnimGraphNode_BlendListByBool_17AC8C1541B5D02BDD6CC9AB88F8C59C;// 0x3488(0x00D0)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_5D82686641ABC156C61CBBB5D3997842;// 0x3558(0x0070)
	struct FAnimNode_SaveCachedPose                    AnimGraphNode_SaveCachedPose_7EC74CF74F77E4794FB51BB16B1CDDF2;// 0x35C8(0x00A8)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_54D462A443AC85D24A6D82947A6EF462;// 0x3670(0x0050)
	struct FAnimNode_LayeredBoneBlend                  AnimGraphNode_LayeredBoneBlend_C418F16D465C0A423BFFF798DD5376B7;// 0x36C0(0x00E0)
	struct FAnimNode_UseCachedPose                     AnimGraphNode_UseCachedPose_804605074E88CB0E3E97DFA945275AA8;// 0x37A0(0x0050)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_DA8E276E40C59D4EA19BD29963AAED89;// 0x37F0(0x00B8)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_9CFFA4F44A1E16CB6AA0B0A5D1D71AE0;// 0x38A8(0x0048)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_E22DA5844DA65903BFF1F185E2F87022;// 0x38F0(0x0048)
	class UAnimSequenceBase*                           IdleSequence;                                             // 0x3938(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           SprintSequence;                                           // 0x3940(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBlendSpace*                                 WalkBlendspace;                                           // 0x3948(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBlendSpace*                                 CrouchBlendspace;                                         // 0x3950(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUseAlternateIdleAnim;                                    // 0x3958(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x7];                                       // 0x3959(0x0007) MISSED OFFSET
	class UAnimSequenceBase*                           IdleAltSequence;                                          // 0x3960(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxWalkSpeedCrouched;                                     // 0x3968(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MaxWalkSpeed;                                             // 0x396C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bHasValidIdle;                                            // 0x3970(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x7];                                       // 0x3971(0x0007) MISSED OFFSET
	class ABP_MainCharacter_C*                         MainChar;                                                 // 0x3978(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance, IsPlainOldData)
	EFirearmType                                       FireArm_Type;                                             // 0x3980(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData05[0x3];                                       // 0x3981(0x0003) MISSED OFFSET
	float                                              FreeLookYaw;                                              // 0x3984(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class AIONWeapon*                                  MainCharWeapon;                                           // 0x3988(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance, IsPlainOldData)
	float                                              Speed;                                                    // 0x3990(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Direction;                                                // 0x3994(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Aim_Ratio;                                                // 0x3998(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x4];                                       // 0x399C(0x0004) MISSED OFFSET
	class UAnimSequenceBase*                           ADSSequence;                                              // 0x39A0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UBlendSpace1D*                               DefaultSprintSequence;                                    // 0x39A8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Aim_State_Ratio;                                          // 0x39B0(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsInDrop;                                                // 0x39B4(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bIsAiming;                                                // 0x39B5(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bHasValidADS;                                             // 0x39B6(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData07[0x1];                                       // 0x39B7(0x0001) MISSED OFFSET
	struct FVector                                     LeftHandIKLoc;                                            // 0x39B8(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SMGIKLoc;                                                 // 0x39C4(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     ADSIKOffset;                                              // 0x39D0(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     FireIKOffset;                                             // 0x39DC(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	bool                                               bDBNO;                                                    // 0x39E8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bHoldingBoosterWhileFalling;                              // 0x39E9(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_Character.ABP_Character_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_567952294D7AB2381A9C4982026A2C99();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendSpacePlayer_C02CCEDB47A861F66ECD9F9552CF9D1E();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_33E7D3404E0C8726B29CCBA792DD5798();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_SequencePlayer_A26FC299476055A94C292192C3BCED5F();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_6F4F950A4518568BD1A2AB8A55BE7284();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_SequencePlayer_D11D20B14C704BEB80BE258A257A5C90();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_1E84472F4E71424F57C654871897F2D4();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_0393C0C7475ACE2EA97D0FA1FC3CE0BD();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_1B9B2FD34A197F1167F0D4B789A24A61();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_2D0FFA9F4282E620D0EAD682577A5469();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_2FCF1B58445D5F96AB7340B7B363916B();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_908CECA1440311E0A3061EB8FB59500A();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_123A054E46BF5EA6AE9AD2A8F66D5356();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_SequencePlayer_CFBCA2764CCDEFD2F2F08282A81EDC5C();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_E287B8EC4198A85FA9C6A7B42AE8B628();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_4F92E5F94316BADCB3F7C087A02A56BD();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_1DE47EB84957585BD274A2A212B4C443();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_9F64688F46FD7E5345B03F8D47C42742();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendSpacePlayer_A9E2E60A467F5C807EFDEBB559972646();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_19FDEBE046D552C747D87396278914BA();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendSpacePlayer_B6F044FF4099D362C12E639FC4E0B68C();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_17AC8C1541B5D02BDD6CC9AB88F8C59C();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_11F14D89456CCE731407B3A58815AA50();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_7EB78AFB4C8B455B0F16C4824885CDF1();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendSpacePlayer_114A687A4A45E355D9A497A0FD234560();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendListByBool_C2948F464CD41D72B3A420A6CB00EB06();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_34E6BDA94928105FADA3F28F8EDB65E2();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TwoWayBlend_B66901B84D0214FF6EF6ABAE8E2BCC26();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_9C6C5EA2420845480A78F38CF19E08B3();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_4B9D3E164E78949FF16FE5941A1B6856();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_ModifyBone_1151CF9B4E3FB071C960ED8713C10764();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_CF4CB1084CFBD903CD389C8966DEA83D();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_A218AD9E4D7AA22CF061499CF7429DDD();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_SequencePlayer_4AA5E00A4468BC19C88D5DBEACD2B80D();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_LayeredBoneBlend_663E1DF04D2A620EC17A219EF6C3C440();
	void AnimNotify_step();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_BlendSpacePlayer_5F44426C4C69B9908310AF9887A563A7();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_CC7A2E974EE99A5266B64DBAC0320538();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_D2C5D4484CE5E0CEF40AF6A1A68BA432();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Character_AnimGraphNode_TransitionResult_58453E484F6D8C8CB29F7E9CC763F68D();
	void ExecuteUbergraph_ABP_Character(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
