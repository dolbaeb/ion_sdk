// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ConfirmVideoSettings_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.GetText_1");

	UWBP_ConfirmVideoSettings_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ConfirmVideoSettings_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.Construct");

	UWBP_ConfirmVideoSettings_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.RevertSettings
// (BlueprintCallable, BlueprintEvent)

void UWBP_ConfirmVideoSettings_C::RevertSettings()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.RevertSettings");

	UWBP_ConfirmVideoSettings_C_RevertSettings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ConfirmVideoSettings_C::BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature");

	UWBP_ConfirmVideoSettings_C_BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ConfirmVideoSettings_C::BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature");

	UWBP_ConfirmVideoSettings_C_BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.ExecuteUbergraph_WBP_ConfirmVideoSettings
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ConfirmVideoSettings_C::ExecuteUbergraph_WBP_ConfirmVideoSettings(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.ExecuteUbergraph_WBP_ConfirmVideoSettings");

	UWBP_ConfirmVideoSettings_C_ExecuteUbergraph_WBP_ConfirmVideoSettings_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
