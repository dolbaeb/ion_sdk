#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// ScriptStruct AnalyticsIo.TimeUUID
// 0x0008
struct FTimeUUID
{
	unsigned char                                      UnknownData00[0x8];                                       // 0x0000(0x0008) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.AnalyticsEvent
// 0x0018
struct FAnalyticsEvent
{
	struct FTimeUUID                                   EventTime;                                                // 0x0000(0x0008) (Edit)
	int                                                BuildCL;                                                  // 0x0008(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                Random;                                                   // 0x000C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	bool                                               bLogged;                                                  // 0x0010(0x0001) (ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0011(0x0007) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.EmbeddedProperty
// 0x0028
struct FEmbeddedProperty
{
	TWeakObjectPtr<class UProperty>                    Property;                                                 // 0x0000(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	struct FString                                     EmbeddedName;                                             // 0x0008(0x0010) (Edit, ZeroConstructor)
	TArray<TWeakObjectPtr<class UProperty>>            EmbeddedPropertyChain;                                    // 0x0018(0x0010) (Edit, ZeroConstructor)
};

// ScriptStruct AnalyticsIo.AnalyticsEvent_FrameHitch
// 0x0060 (0x0078 - 0x0018)
struct FAnalyticsEvent_FrameHitch : public FAnalyticsEvent
{
	float                                              Duration;                                                 // 0x0018(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              DumpThreshold;                                            // 0x001C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SampleThreshold;                                          // 0x0020(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              SampleInterval;                                           // 0x0024(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                SampleCount;                                              // 0x0028(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              GameOverheadTime;                                         // 0x002C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              RenderOverheadTime;                                       // 0x0030(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0034(0x0004) MISSED OFFSET
	struct FString                                     MapName;                                                  // 0x0038(0x0010) (Edit, ZeroConstructor)
	struct FString                                     MatchState;                                               // 0x0048(0x0010) (Edit, ZeroConstructor)
	struct FString                                     GameThread;                                               // 0x0058(0x0010) (Edit, ZeroConstructor)
	struct FString                                     RenderThread;                                             // 0x0068(0x0010) (Edit, ZeroConstructor)
};

// ScriptStruct AnalyticsIo.AnalyticsEvent_ServerPerMinuteBucket
// 0x0068 (0x0080 - 0x0018)
struct FAnalyticsEvent_ServerPerMinuteBucket : public FAnalyticsEvent
{
	struct FString                                     MatchId;                                                  // 0x0018(0x0010) (Edit, ZeroConstructor)
	struct FString                                     MapName;                                                  // 0x0028(0x0010) (Edit, ZeroConstructor)
	struct FString                                     GameModeName;                                             // 0x0038(0x0010) (Edit, ZeroConstructor)
	struct FString                                     ComputerName;                                             // 0x0048(0x0010) (Edit, ZeroConstructor)
	int                                                TimeMinutes;                                              // 0x0058(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NumClients;                                               // 0x005C(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AverageFrameTime;                                         // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AveragePingTime;                                          // 0x0064(0x0004) (ZeroConstructor, IsPlainOldData)
	int64_t                                            AvailablePhysicalMemory;                                  // 0x0068(0x0008) (ZeroConstructor, IsPlainOldData)
	int64_t                                            UsedVirtualMemory;                                        // 0x0070(0x0008) (ZeroConstructor, IsPlainOldData)
	float                                              ProcessUptime;                                            // 0x0078(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x007C(0x0004) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.FrameTimeBucket
// 0x0004
struct FFrameTimeBucket
{
	int                                                Count;                                                    // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.FrameTimeBuckets
// 0x0028
struct FFrameTimeBuckets
{
	struct FFrameTimeBucket                            FrameTime0_17ms;                                          // 0x0000(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime17_30ms;                                         // 0x0004(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime30_50ms;                                         // 0x0008(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime50_75ms;                                         // 0x000C(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime75_100ms;                                        // 0x0010(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime100_150ms;                                       // 0x0014(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime150_250ms;                                       // 0x0018(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime250_500ms;                                       // 0x001C(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime500_1000ms;                                      // 0x0020(0x0004) (IsPlainOldData)
	struct FFrameTimeBucket                            FrameTime1000ms;                                          // 0x0024(0x0004) (IsPlainOldData)
};

// ScriptStruct AnalyticsIo.PingTimeBucket
// 0x0004
struct FPingTimeBucket
{
	int                                                Count;                                                    // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.PingTimeBuckets
// 0x002C
struct FPingTimeBuckets
{
	struct FPingTimeBucket                             PingTime0_30ms;                                           // 0x0000(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime30_60ms;                                          // 0x0004(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime60_100ms;                                         // 0x0008(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime100_150ms;                                        // 0x000C(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime150_200ms;                                        // 0x0010(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime200_300ms;                                        // 0x0014(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime300_400ms;                                        // 0x0018(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime400_500ms;                                        // 0x001C(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime500_750ms;                                        // 0x0020(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime750_1000ms;                                       // 0x0024(0x0004) (IsPlainOldData)
	struct FPingTimeBucket                             PingTime1000ms;                                           // 0x0028(0x0004) (IsPlainOldData)
};

// ScriptStruct AnalyticsIo.AvailableCPUBuckets
// 0x001C
struct FAvailableCPUBuckets
{
	int                                                Percent0_11;                                              // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Percent10_21;                                             // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Percent20_31;                                             // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Percent30_41;                                             // 0x000C(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Percent40_51;                                             // 0x0010(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                Percent50_101;                                            // 0x0014(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                SampleCount;                                              // 0x0018(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.PacketLossData
// 0x0018
struct FPacketLossData
{
	int                                                NetInPackets;                                             // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetInPacketsLost;                                         // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetInOutOfOrderPackets;                                   // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetOutPackets;                                            // 0x000C(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetOutPacketsLost;                                        // 0x0010(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetOutOutOfOrderPackets;                                  // 0x0014(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.AnalyticsEvent_ServerPostMatchInfo
// 0x01B8 (0x01D0 - 0x0018)
struct FAnalyticsEvent_ServerPostMatchInfo : public FAnalyticsEvent
{
	struct FString                                     MatchId;                                                  // 0x0018(0x0010) (Edit, ZeroConstructor)
	struct FString                                     MapName;                                                  // 0x0028(0x0010) (Edit, ZeroConstructor)
	struct FString                                     GameModeName;                                             // 0x0038(0x0010) (Edit, ZeroConstructor)
	struct FString                                     ComputerName;                                             // 0x0048(0x0010) (Edit, ZeroConstructor)
	struct FString                                     ServerName;                                               // 0x0058(0x0010) (Edit, ZeroConstructor)
	struct FString                                     IpAddress;                                                // 0x0068(0x0010) (Edit, ZeroConstructor)
	float                                              Duration;                                                 // 0x0078(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	struct FFrameTimeBuckets                           FrameBuckets;                                             // 0x007C(0x0028) (Edit)
	struct FFrameTimeBuckets                           StatFrameBuckets;                                         // 0x00A4(0x0028) (Edit)
	struct FPingTimeBuckets                            PingBuckets;                                              // 0x00CC(0x002C) (Edit)
	struct FAvailableCPUBuckets                        AvailableCpu;                                             // 0x00F8(0x001C) (Edit)
	int                                                PingTotalSamples;                                         // 0x0114(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalRetryReliableCount;                                  // 0x0118(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalRetryReliableBytes;                                  // 0x0120(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalRetryUnreliableCount;                                // 0x0128(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalRetryUnreliableBytes;                                // 0x0130(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            SendFailTotalBytes;                                       // 0x0138(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AverageServerFrameTime;                                   // 0x0140(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AverageClientFrameTime;                                   // 0x0144(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AveragePingTime;                                          // 0x0148(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AverageRTT;                                               // 0x014C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                TotalAnalyticsErrors;                                     // 0x0150(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                TotalAnalyticsTasks;                                      // 0x0154(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	struct FPacketLossData                             PacketLossData;                                           // 0x0158(0x0018)
	int                                                SkippedServerMoves;                                       // 0x0170(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                SkippedServerMoveFrames;                                  // 0x0174(0x0004) (ZeroConstructor, IsPlainOldData)
	int64_t                                            AvailablePhysicalMemoryStart;                             // 0x0178(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            AvailablePhysicalMemoryEnd;                               // 0x0180(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              ProcessUptimeAtStart;                                     // 0x0188(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x018C(0x0004) MISSED OFFSET
	int64_t                                            UsedVirtualMemoryStart;                                   // 0x0190(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            UsedVirtualMemoryEnd;                                     // 0x0198(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AverageOutBytesPerClient;                                 // 0x01A0(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              AverageNumClients;                                        // 0x01A4(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                TotalFrames;                                              // 0x01A8(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                NetSaturatedFrames;                                       // 0x01AC(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x10];                                      // 0x01B0(0x0010) MISSED OFFSET
	class AGameStateBase*                              GameState;                                                // 0x01C0(0x0008) (ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData02[0x8];                                       // 0x01C8(0x0008) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.MovementCorrectionData
// 0x000C
struct FMovementCorrectionData
{
	int                                                NetAutonomousMovementCorrections;                         // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetAutonomousMovementCorrectionBunches;                   // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NetSimulatedMovementCorrections;                          // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.AnalyticsHardwareInfo
// 0x0068
struct FAnalyticsHardwareInfo
{
	int                                                TotalPhysicalGBRam;                                       // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NumberOfCoresPhysical;                                    // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NumberOfCoresLogical;                                     // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
	struct FString                                     RHIAdapterName;                                           // 0x0010(0x0010) (ZeroConstructor)
	int                                                RHIVendorId;                                              // 0x0020(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0024(0x0004) MISSED OFFSET
	int64_t                                            DedicatedVideoMemory;                                     // 0x0028(0x0008) (ZeroConstructor, IsPlainOldData)
	int64_t                                            DedicatedSystemMemory;                                    // 0x0030(0x0008) (ZeroConstructor, IsPlainOldData)
	int64_t                                            SharedSystemMemory;                                       // 0x0038(0x0008) (ZeroConstructor, IsPlainOldData)
	struct FString                                     CPUVendor;                                                // 0x0040(0x0010) (ZeroConstructor)
	struct FString                                     CPUBrand;                                                 // 0x0050(0x0010) (ZeroConstructor)
	int                                                CPUInfo;                                                  // 0x0060(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.AnalyticsQualityLevels
// 0x001C
struct FAnalyticsQualityLevels
{
	int                                                ResolutionQuality;                                        // 0x0000(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                ViewDistanceQuality;                                      // 0x0004(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                AntiAliasingQuality;                                      // 0x0008(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                ShadowQuality;                                            // 0x000C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                PostProcessQuality;                                       // 0x0010(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                TextureQuality;                                           // 0x0014(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	int                                                EffectsQuality;                                           // 0x0018(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct AnalyticsIo.ClientPostMatchInfo
// 0x0188
struct FClientPostMatchInfo
{
	struct FFrameTimeBuckets                           FrameTimeBuckets;                                         // 0x0000(0x0028)
	struct FFrameTimeBuckets                           StatFrameTimeBuckets;                                     // 0x0028(0x0028)
	struct FPingTimeBuckets                            PingTimeBuckets;                                          // 0x0050(0x002C)
	float                                              LoadIntoMainMenuTime;                                     // 0x007C(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                TotalPingSamples;                                         // 0x0080(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                TotalFrames;                                              // 0x0084(0x0004) (ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalReliableRetryCount;                                  // 0x0088(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalReliableRetryBytes;                                  // 0x0090(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalUnreliableRetryCount;                                // 0x0098(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            TotalUnrealiableRetryBytes;                               // 0x00A0(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	int64_t                                            SendFailTotalBytes;                                       // 0x00A8(0x0008) (ZeroConstructor, IsPlainOldData)
	float                                              AverageFrameTime;                                         // 0x00B0(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AveragePingTime;                                          // 0x00B4(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FAvailableCPUBuckets                        AvailableCpu;                                             // 0x00B8(0x001C)
	struct FPacketLossData                             PacketLossData;                                           // 0x00D4(0x0018)
	struct FMovementCorrectionData                     MovementCorrectionData;                                   // 0x00EC(0x000C)
	struct FAnalyticsHardwareInfo                      HardwareInfo;                                             // 0x00F8(0x0068)
	float                                              CPUPerfIndex;                                             // 0x0160(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              GPUPerfIndex;                                             // 0x0164(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FAnalyticsQualityLevels                     QualityLevels;                                            // 0x0168(0x001C)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0184(0x0004) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.PerMinuteBucket
// 0x0028
struct FPerMinuteBucket
{
	int                                                TimeMinutes;                                              // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AverageFrameTime;                                         // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              AveragePingTime;                                          // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
	int64_t                                            AvailablePhysicalMemory;                                  // 0x0010(0x0008) (ZeroConstructor, IsPlainOldData)
	int64_t                                            UsedVirtualMemory;                                        // 0x0018(0x0008) (ZeroConstructor, IsPlainOldData)
	float                                              ProcessUptime;                                            // 0x0020(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0024(0x0004) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.CassandraType
// 0x0030
struct FCassandraType
{
	unsigned char                                      UnknownData00[0x30];                                      // 0x0000(0x0030) MISSED OFFSET
};

// ScriptStruct AnalyticsIo.CassandraTypeBoolean
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeBoolean : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeTimeUUID
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeTimeUUID : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeTimestamp
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeTimestamp : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeText
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeText : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeDouble
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeDouble : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeFloat
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeFloat : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeInt64
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeInt64 : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeInt32
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeInt32 : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeInt16
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeInt16 : public FCassandraType
{

};

// ScriptStruct AnalyticsIo.CassandraTypeInt8
// 0x0000 (0x0030 - 0x0030)
struct FCassandraTypeInt8 : public FCassandraType
{

};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
