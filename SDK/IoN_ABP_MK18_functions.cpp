// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19
// (BlueprintEvent)

void UABP_MK18_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19");

	UABP_MK18_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C
// (BlueprintEvent)

void UABP_MK18_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MK18.ABP_MK18_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C");

	UABP_MK18_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_MK18.ABP_MK18_C.BlueprintUpdateAnimation
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaTimeX                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_MK18_C::BlueprintUpdateAnimation(float* DeltaTimeX)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MK18.ABP_MK18_C.BlueprintUpdateAnimation");

	UABP_MK18_C_BlueprintUpdateAnimation_Params params;
	params.DeltaTimeX = DeltaTimeX;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_MK18.ABP_MK18_C.ExecuteUbergraph_ABP_MK18
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_MK18_C::ExecuteUbergraph_ABP_MK18(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MK18.ABP_MK18_C.ExecuteUbergraph_ABP_MK18");

	UABP_MK18_C_ExecuteUbergraph_ABP_MK18_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
