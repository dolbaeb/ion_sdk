#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_MainCharacter.BP_MainCharacter_C.ShowMapToolTip
struct ABP_MainCharacter_C_ShowMapToolTip_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.GetBoneForHitSim
struct ABP_MainCharacter_C_GetBoneForHitSim_Params
{
	struct FName                                       BoneName;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FName                                       OutBone;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.UserConstructionScript
struct ABP_MainCharacter_C_UserConstructionScript_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__FinishedFunc
struct ABP_MainCharacter_C_Timeline_0__FinishedFunc_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_0__UpdateFunc
struct ABP_MainCharacter_C_Timeline_0__UpdateFunc_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__FinishedFunc
struct ABP_MainCharacter_C_Timeline_1__FinishedFunc_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.Timeline_1__UpdateFunc
struct ABP_MainCharacter_C_Timeline_1__UpdateFunc_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.BeginPlayDebug
struct ABP_MainCharacter_C_BeginPlayDebug_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.SetDropsuitMaterial
struct ABP_MainCharacter_C_SetDropsuitMaterial_Params
{
	class UMaterialInterface*                          Mat;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.CamBlur
struct ABP_MainCharacter_C_CamBlur_Params
{
	float                                              Strength;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.EventBP_DropInModeStarted
struct ABP_MainCharacter_C_EventBP_DropInModeStarted_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.LandingFX
struct ABP_MainCharacter_C_LandingFX_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.EventBP_LandingTrigger
struct ABP_MainCharacter_C_EventBP_LandingTrigger_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.Broadcast_LandingTrigger
struct ABP_MainCharacter_C_Broadcast_LandingTrigger_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature
struct ABP_MainCharacter_C_BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_0_ComponentBeginOverlapSignature__DelegateSignature_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               bFromSweep;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FHitResult                                  SweepResult;                                              // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature
struct ABP_MainCharacter_C_BndEvt__CapsuleComponent_K2Node_ComponentBoundEvent_1_ComponentEndOverlapSignature__DelegateSignature_Params
{
	class UPrimitiveComponent*                         OverlappedComponent;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	class AActor*                                      OtherActor;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class UPrimitiveComponent*                         OtherComp;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                OtherBodyIndex;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.UpdateSuppressionEffects
struct ABP_MainCharacter_C_UpdateSuppressionEffects_Params
{
	float*                                             SuppressionRatio;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.UpdateHealthEffects
struct ABP_MainCharacter_C_UpdateHealthEffects_Params
{
	float*                                             HealthRatio;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float*                                             OldHealthRatio;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.ReceiveBeginPlay
struct ABP_MainCharacter_C_ReceiveBeginPlay_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.BP_SimulatedHit
struct ABP_MainCharacter_C_BP_SimulatedHit_Params
{
	struct FName*                                      BoneName;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float*                                             DamageImpulse;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FVector*                                    ShotFromDirection;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.OnBulletSuppression
struct ABP_MainCharacter_C_OnBulletSuppression_Params
{
	class AMainProjectile**                            Projectile;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.OnAimingStateChanged
struct ABP_MainCharacter_C_OnAimingStateChanged_Params
{
	float*                                             NewAimRatio;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool*                                              bForce;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class AIONWeapon**                                 Weapon;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.InventoryStateChanged
struct ABP_MainCharacter_C_InventoryStateChanged_Params
{
	bool*                                              bOpen;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_MainCharacter.BP_MainCharacter_C.BP_ProceduralFireAnimation
struct ABP_MainCharacter_C_BP_ProceduralFireAnimation_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.OnHardLanding
struct ABP_MainCharacter_C_OnHardLanding_Params
{
};

// Function BP_MainCharacter.BP_MainCharacter_C.ExecuteUbergraph_BP_MainCharacter
struct ABP_MainCharacter_C_ExecuteUbergraph_BP_MainCharacter_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
