#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ServerSelection.WBP_ServerSelection_C.SwitchPanel
struct UWBP_ServerSelection_C_SwitchPanel_Params
{
	int                                                WidgetIndex;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ServerSelection.WBP_ServerSelection_C.Update Matchmaking Button Style
struct UWBP_ServerSelection_C_Update_Matchmaking_Button_Style_Params
{
};

// Function WBP_ServerSelection.WBP_ServerSelection_C.Construct
struct UWBP_ServerSelection_C_Construct_Params
{
};

// Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToCustomPanel
struct UWBP_ServerSelection_C_OnSwitchToCustomPanel_Params
{
};

// Function WBP_ServerSelection.WBP_ServerSelection_C.OnSwitchToPublicPanel
struct UWBP_ServerSelection_C_OnSwitchToPublicPanel_Params
{
};

// Function WBP_ServerSelection.WBP_ServerSelection_C.ExecuteUbergraph_WBP_ServerSelection
struct UWBP_ServerSelection_C_ExecuteUbergraph_WBP_ServerSelection_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
