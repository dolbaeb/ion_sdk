#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EKeyConflict.EKeyConflict
enum class EKeyConflict : uint8_t
{
	EKeyConflict__NewEnumerator0   = 0,
	EKeyConflict__NewEnumerator2   = 1,
	EKeyConflict__NewEnumerator3   = 2,
	EKeyConflict__NewEnumerator1   = 3,
	EKeyConflict__NewEnumerator4   = 4,
	EKeyConflict__NewEnumerator5   = 5,
	EKeyConflict__EKeyConflict_MAX = 6
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
