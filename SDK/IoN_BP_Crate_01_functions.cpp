// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_Crate_01.BP_Crate_01_C.SetCrate
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Crate                          (BlueprintVisible, BlueprintReadOnly, Parm)

void ABP_Crate_01_C::SetCrate(const struct FIONSteamInventoryItem& Crate)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Crate_01.BP_Crate_01_C.SetCrate");

	ABP_Crate_01_C_SetCrate_Params params;
	params.Crate = Crate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Crate_01.BP_Crate_01_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_Crate_01_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Crate_01.BP_Crate_01_C.UserConstructionScript");

	ABP_Crate_01_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Crate_01.BP_Crate_01_C.PlayOpen
// (BlueprintCallable, BlueprintEvent)

void ABP_Crate_01_C::PlayOpen()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Crate_01.BP_Crate_01_C.PlayOpen");

	ABP_Crate_01_C_PlayOpen_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Crate_01.BP_Crate_01_C.ExecuteUbergraph_BP_Crate_01
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Crate_01_C::ExecuteUbergraph_BP_Crate_01(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Crate_01.BP_Crate_01_C.ExecuteUbergraph_BP_Crate_01");

	ABP_Crate_01_C_ExecuteUbergraph_BP_Crate_01_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
