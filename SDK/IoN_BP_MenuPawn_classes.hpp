#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_MenuPawn.BP_MenuPawn_C
// 0x0268 (0x0678 - 0x0410)
class ABP_MenuPawn_C : public AIONCustomizationPawn
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0410(0x0008) (Transient, DuplicateTransient)
	class USkeletalMeshComponent*                      WearableDisplay;                                          // 0x0418(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USceneComponent*                             CharSkin;                                                 // 0x0420(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USceneComponent*                             SkinRoot;                                                 // 0x0428(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UParticleSystemComponent*                    SkinCamGoldCircle;                                        // 0x0430(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UParticleSystemComponent*                    SkinCamShockwave;                                         // 0x0438(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UWidgetComponent*                            BackgroundWidget;                                         // 0x0440(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCineCameraComponent*                        SkinCam;                                                  // 0x0448(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USpringArmComponent*                         SpringArm;                                                // 0x0450(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UChildActorComponent*                        MainChar;                                                 // 0x0458(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class UCineCameraComponent*                        CineCamera;                                               // 0x0460(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class USceneComponent*                             Root;                                                     // 0x0468(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              SpringArmTargetLength;                                    // 0x0470(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              TargetArmLength_ZoomedIn;                                 // 0x0474(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              TargetArmLength_ZoomedOut;                                // 0x0478(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SpringArmTargetOffset;                                    // 0x047C(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SpringArmTargetOffset_ZoomedIn;                           // 0x0488(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     SpringArmTargetOffset_ZoomedOut;                          // 0x0494(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FSZoomData>                          ZoomStates;                                               // 0x04A0(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	bool                                               LeftMouseBtnDown;                                         // 0x04B0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x04B1(0x0003) MISSED OFFSET
	float                                              CurrentRotSpeed;                                          // 0x04B4(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpringArmTargetLength_Min;                                // 0x04B8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpringArmTargetLength_Max;                                // 0x04BC(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<class UClass*>                              DefaultTestItems;                                         // 0x04C0(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	TArray<class AIONItem*>                            EquippedItems;                                            // 0x04D0(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance)
	bool                                               HideMainMenuArmor;                                        // 0x04E0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x04E1(0x0003) MISSED OFFSET
	struct FRotator                                    CameraTargetRotation;                                     // 0x04E4(0x000C) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	class UWBP_MainMenu_C*                             MainMenuWidget;                                           // 0x04F0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	bool                                               bFirstLoadout;                                            // 0x04F8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x04F9(0x0007) MISSED OFFSET
	struct FTransform                                  CrateSpawnTransform;                                      // 0x0500(0x0030) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	bool                                               bReady;                                                   // 0x0530(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x7];                                       // 0x0531(0x0007) MISSED OFFSET
	class ABP_Crate_01_C*                              CurrentCrateObject;                                       // 0x0538(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance, IsPlainOldData)
	struct FTransform                                  SkinCamTransform;                                         // 0x0540(0x0030) (Edit, BlueprintVisible, IsPlainOldData)
	TArray<class UIONSteamItem*>                       DebugRefForCrates;                                        // 0x0570(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)
	bool                                               bInSkinCam;                                               // 0x0580(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData04[0x3];                                       // 0x0581(0x0003) MISSED OFFSET
	float                                              WeaponRotationImpact;                                     // 0x0584(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData05[0x8];                                       // 0x0588(0x0008) MISSED OFFSET
	struct FTransform                                  CrateOpeningTransform;                                    // 0x0590(0x0030) (Edit, BlueprintVisible, DisableEditOnInstance, IsPlainOldData)
	bool                                               bCanRotateCrate;                                          // 0x05C0(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData06[0x7];                                       // 0x05C1(0x0007) MISSED OFFSET
	TMap<class UClass*, struct FVector>                InspectWearableOffset;                                    // 0x05C8(0x0050) (Edit, BlueprintVisible, ZeroConstructor)
	class UWBP_CrateScreen_C*                          CurrentCrateScreen;                                       // 0x0618(0x0008) (Edit, BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	float                                              DistanceLeft;                                             // 0x0620(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              TotalDistance;                                            // 0x0624(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TMap<class UClass*, class UAnimSequence*>          WeaponIdleAnimations;                                     // 0x0628(0x0050) (Edit, BlueprintVisible, ZeroConstructor)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_MenuPawn.BP_MenuPawn_C");
		return ptr;
	}


	void StartCrateAnim(class UWBP_CrateScreen_C* CurrentCrateScreen, float TotalDistance);
	void CleanupWearableItem();
	void DoWeaponRotation();
	void CleanupCrateItem();
	void CleanupDisplayItems();
	void SwitchToSkinCam(bool bSwitch);
	void HideEquippedItems(bool HideItems);
	void ApplyWeaponSkin(class UIONWeaponSkin* NewSkin);
	void TickObjectTransforms();
	void ChangeZoom(int ZoomLevel);
	void ZoomIn(bool In);
	void TickCameraPosition();
	void UserConstructionScript();
	void InpActEvt_Ctrl_Shift_K_K2Node_InputKeyEvent_3(const struct FKey& Key);
	void InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_2(const struct FKey& Key);
	void InpActEvt_LeftMouseButton_K2Node_InputKeyEvent_1(const struct FKey& Key);
	void BPEvent_SkinsDebug(int* Mode);
	void InpAxisKeyEvt_MouseX_K2Node_InputAxisKeyEvent_1(float AxisValue);
	void InpAxisKeyEvt_MouseWheelAxis_K2Node_InputAxisKeyEvent_4(float AxisValue);
	void ReceiveBeginPlay();
	void ReceiveTick(float* DeltaSeconds);
	void OpenCrateAnimation();
	void BPEvent_InspectWeaponSkin(class UIONWeaponSkin** WeaponSkin);
	void LoadoutLoaded();
	void ShowDefaultChar();
	void BPEvent_InspectCrate(struct FIONSteamInventoryItem* Crate);
	void PlayGoldEffect();
	void StopGoldEffect();
	void BPEvent_InspectArmor(class UIONArmorSkin** ArmorSkin);
	void BPEvent_InspectDropsuit(class UIONDropSkin** DropSkin);
	void BPEvent_PreviewWeaponSkin(class AIONWeapon** Weapon);
	void CrateTransitionToOpening();
	void ExecuteUbergraph_BP_MenuPawn(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
