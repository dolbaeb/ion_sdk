#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.UserConstructionScript
struct ABP_WearableMannequinAdvanced_C_UserConstructionScript_Params
{
};

// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__FinishedFunc
struct ABP_WearableMannequinAdvanced_C_CharacterSpin__FinishedFunc_Params
{
};

// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.CharacterSpin__UpdateFunc
struct ABP_WearableMannequinAdvanced_C_CharacterSpin__UpdateFunc_Params
{
};

// Function BP_WearableMannequinAdvanced.BP_WearableMannequinAdvanced_C.ExecuteUbergraph_BP_WearableMannequinAdvanced
struct ABP_WearableMannequinAdvanced_C_ExecuteUbergraph_BP_WearableMannequinAdvanced_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
