#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_PartySpawn.BP_PartySpawn_C
// 0x0018 (0x0388 - 0x0370)
class ABP_PartySpawn_C : public AIONPartySpawnActor
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0370(0x0008) (Transient, DuplicateTransient)
	class USceneComponent*                             DefaultSceneRoot;                                         // 0x0378(0x0008) (BlueprintVisible, ZeroConstructor, InstancedReference, IsPlainOldData)
	class ABP_MenuPawn_C*                              MenuPawn;                                                 // 0x0380(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnTemplate, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_PartySpawn.BP_PartySpawn_C");
		return ptr;
	}


	void GetCharacter(class AIONCharacter** PlayerCharacter);
	void SetPlayerDetails(const struct FMatchmakingPlayer& Player);
	void FillSlot(const struct FMatchmakingPlayer& Player);
	void ClearSlot();
	void IsSlotAvailable(bool* IsEmpty);
	void UserConstructionScript();
	void CreatePlayerPawn(struct FMatchmakingPlayer* Player);
	void DestroyPlayerPawn();
	void PlayerUpdated(struct FMatchmakingPlayer* Player);
	void BPEvent_ArmorUpdated(struct FString* NewArmor);
	void ExecuteUbergraph_BP_PartySpawn(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
