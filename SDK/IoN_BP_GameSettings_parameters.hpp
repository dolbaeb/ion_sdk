#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_GameSettings.BP_GameSettings_C.Get Foliage Quality
struct UBP_GameSettings_C_Get_Foliage_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Foliage Quality
struct UBP_GameSettings_C_Set_Foliage_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Foliage Quality
struct UBP_GameSettings_C_Modify_Foliage_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume UI
struct UBP_GameSettings_C_Get_Volume_UI_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier UI
struct UBP_GameSettings_C_Get_Audio_Multiplier_UI_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier UI
struct UBP_GameSettings_C_Set_Audio_Multiplier_UI_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier UI
struct UBP_GameSettings_C_Modify_Audio_Multiplier_UI_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Save ini Settings
struct UBP_GameSettings_C_Save_ini_Settings_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Load ini Settings
struct UBP_GameSettings_C_Load_ini_Settings_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Init Save Game Settings
struct UBP_GameSettings_C_Init_Save_Game_Settings_Params
{
	TScriptInterface<class UBPI_GameSettingsInterface_C> Game_Settings_Interface;                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Saved Key Inputs
struct UBP_GameSettings_C_Get_Saved_Key_Inputs_Params
{
	TArray<struct FSKeyActionSave>                     Saved_Key_Inputs;                                         // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Get All Key Actions
struct UBP_GameSettings_C_Get_All_Key_Actions_Params
{
	TArray<class UBP_KeyAction_C*>                     Key_Actions;                                              // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Save File User Index
struct UBP_GameSettings_C_Set_Save_File_User_Index_Params
{
	int                                                Save_File_User_Index;                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Save File Name
struct UBP_GameSettings_C_Set_Save_File_Name_Params
{
	struct FString                                     Save_File_Name;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Game Settings Interface
struct UBP_GameSettings_C_Set_Game_Settings_Interface_Params
{
	TScriptInterface<class UBPI_GameSettingsInterface_C> Game_Settings_Interface;                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get All Combinations
struct UBP_GameSettings_C_Get_All_Combinations_Params
{
	TArray<class UBP_KeyCombination_C*>                Combinations;                                             // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Generate Keybinding Conflicts
struct UBP_GameSettings_C_Generate_Keybinding_Conflicts_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Keybindings
struct UBP_GameSettings_C_Modify_Keybindings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Load Key Actions
struct UBP_GameSettings_C_Load_Key_Actions_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Save Key Actions
struct UBP_GameSettings_C_Save_Key_Actions_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Store Key Input
struct UBP_GameSettings_C_Store_Key_Input_Params
{
	struct FSKeyActionSave                             KeySave;                                                  // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Key Action
struct UBP_GameSettings_C_Get_Key_Action_Params
{
	struct FString                                     Input_Action_Name;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	class UBP_KeyAction_C*                             Input_Action;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Success;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Fill Float Axis Inputs List
struct UBP_GameSettings_C_Fill_Float_Axis_Inputs_List_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Init Key Bindings
struct UBP_GameSettings_C_Init_Key_Bindings_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Update Actions Input State
struct UBP_GameSettings_C_Update_Actions_Input_State_Params
{
	float                                              Real_Time_Seconds;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              World_Delta_Seconds;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class APlayerController*                           PlayerController;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Delete Settings Save File
struct UBP_GameSettings_C_Delete_Settings_Save_File_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined X
struct UBP_GameSettings_C_Get_Look_Sensitivity_Combined_X_Params
{
	float                                              Input_Axis_X;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              World_Delta;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Horizontal_X;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Combobox
struct UBP_GameSettings_C_Get_My_Custom_Combobox_Params
{
	struct FString                                     Value;                                                    // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Combobox
struct UBP_GameSettings_C_Set_My_Custom_Combobox_Params
{
	struct FString                                     Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Result;                                                   // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Combobox
struct UBP_GameSettings_C_Modify_My_Custom_Combobox_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Radiobox
struct UBP_GameSettings_C_Get_My_Custom_Radiobox_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Radiobox
struct UBP_GameSettings_C_Set_My_Custom_Radiobox_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Radiobox
struct UBP_GameSettings_C_Modify_My_Custom_Radiobox_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Slider
struct UBP_GameSettings_C_Get_My_Custom_Slider_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Slider
struct UBP_GameSettings_C_Set_My_Custom_Slider_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Slider
struct UBP_GameSettings_C_Modify_My_Custom_Slider_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Checkbox
struct UBP_GameSettings_C_Get_My_Custom_Checkbox_Params
{
	bool                                               Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Checkbox
struct UBP_GameSettings_C_Set_My_Custom_Checkbox_Params
{
	bool                                               Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Checkbox
struct UBP_GameSettings_C_Modify_My_Custom_Checkbox_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify All MyCustom Settings
struct UBP_GameSettings_C_Modify_All_MyCustom_Settings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Resolution Scale
struct UBP_GameSettings_C_Get_Resolution_Scale_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Resolution Scale
struct UBP_GameSettings_C_Set_Resolution_Scale_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Resolution Scale
struct UBP_GameSettings_C_Modify_Resolution_Scale_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Prepeare Previus Settings State
struct UBP_GameSettings_C_Prepeare_Previus_Settings_State_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Save All Settings
struct UBP_GameSettings_C_Save_All_Settings_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Modify All Settings
struct UBP_GameSettings_C_Modify_All_Settings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify All Audio Settings
struct UBP_GameSettings_C_Modify_All_Audio_Settings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify All Look Settings
struct UBP_GameSettings_C_Modify_All_Look_Settings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined Y
struct UBP_GameSettings_C_Get_Look_Sensitivity_Combined_Y_Params
{
	float                                              Input_Axis_Y;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              World_Delta;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Vertical_Y;                                               // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume Ambient
struct UBP_GameSettings_C_Get_Volume_Ambient_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Ambient
struct UBP_GameSettings_C_Get_Audio_Multiplier_Ambient_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Ambient
struct UBP_GameSettings_C_Set_Audio_Multiplier_Ambient_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Ambient
struct UBP_GameSettings_C_Modify_Audio_Multiplier_Ambient_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume Effect
struct UBP_GameSettings_C_Get_Volume_Effect_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Effect
struct UBP_GameSettings_C_Get_Audio_Multiplier_Effect_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Effect
struct UBP_GameSettings_C_Set_Audio_Multiplier_Effect_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Effect
struct UBP_GameSettings_C_Modify_Audio_Multiplier_Effect_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume Voice
struct UBP_GameSettings_C_Get_Volume_Voice_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Voice
struct UBP_GameSettings_C_Get_Audio_Multiplier_Voice_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Voice
struct UBP_GameSettings_C_Set_Audio_Multiplier_Voice_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Voice
struct UBP_GameSettings_C_Modify_Audio_Multiplier_Voice_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume Music
struct UBP_GameSettings_C_Get_Volume_Music_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Music
struct UBP_GameSettings_C_Get_Audio_Multiplier_Music_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Music
struct UBP_GameSettings_C_Set_Audio_Multiplier_Music_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Music
struct UBP_GameSettings_C_Modify_Audio_Multiplier_Music_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Volume Master
struct UBP_GameSettings_C_Get_Volume_Master_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Add Volume Control
struct UBP_GameSettings_C_Add_Volume_Control_Params
{
	class UAudioComponent*                             Audio_Emittor;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	TEnumAsByte<EAudioType>                            Audio_Channel;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Apply Audio Settings
struct UBP_GameSettings_C_Apply_Audio_Settings_Params
{
	TEnumAsByte<EAudioType>                            Audio_Channel;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Master
struct UBP_GameSettings_C_Get_Audio_Multiplier_Master_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Master
struct UBP_GameSettings_C_Set_Audio_Multiplier_Master_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Master
struct UBP_GameSettings_C_Modify_Audio_Multiplier_Master_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Invert
struct UBP_GameSettings_C_Get_Look_Vertical_Invert_Params
{
	bool                                               Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Invert
struct UBP_GameSettings_C_Set_Look_Vertical_Invert_Params
{
	bool                                               Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Invert
struct UBP_GameSettings_C_Modify_Look_Vertical_Invert_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Invert
struct UBP_GameSettings_C_Get_Look_Horizontal_Invert_Params
{
	bool                                               Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Invert
struct UBP_GameSettings_C_Set_Look_Horizontal_Invert_Params
{
	bool                                               Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Invert
struct UBP_GameSettings_C_Modify_Look_Horizontal_Invert_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Sensitivity
struct UBP_GameSettings_C_Get_Look_Vertical_Sensitivity_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Sensitivity
struct UBP_GameSettings_C_Set_Look_Vertical_Sensitivity_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Sensitivity
struct UBP_GameSettings_C_Modify_Look_Vertical_Sensitivity_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Sensitivity
struct UBP_GameSettings_C_Get_Look_Horizontal_Sensitivity_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Sensitivity
struct UBP_GameSettings_C_Set_Look_Horizontal_Sensitivity_Params
{
	float                                              Set_Value;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Sensitivity
struct UBP_GameSettings_C_Modify_Look_Horizontal_Sensitivity_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Apply Screen Settings
struct UBP_GameSettings_C_Apply_Screen_Settings_Params
{
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Screen Mode
struct UBP_GameSettings_C_Modify_Screen_Mode_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Screen Mode
struct UBP_GameSettings_C_Get_Screen_Mode_Params
{
	TEnumAsByte<EWindowMode>                           Screen_Mode;                                              // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Command;                                                  // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Screen Resolution
struct UBP_GameSettings_C_Get_Screen_Resolution_Params
{
	struct FSVideoResolution                           Resolution;                                               // (Parm, OutParm)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Screen Mode
struct UBP_GameSettings_C_Set_Screen_Mode_Params
{
	TEnumAsByte<EWindowMode>                           Screen_Mode;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	TEnumAsByte<EWindowMode>                           Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Screen Resolution
struct UBP_GameSettings_C_Set_Screen_Resolution_Params
{
	struct FSVideoResolution                           Resolution;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FSVideoResolution                           Result;                                                   // (Parm, OutParm)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Screen Resolution
struct UBP_GameSettings_C_Modify_Screen_Resolution_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Motion Blur Strength
struct UBP_GameSettings_C_Get_Motion_Blur_Strength_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Motion Blur Strength
struct UBP_GameSettings_C_Set_Motion_Blur_Strength_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Motion Blur Strength
struct UBP_GameSettings_C_Modify_Motion_Blur_Strength_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Gain Intensity
struct UBP_GameSettings_C_Get_Gain_Intensity_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Gain Intensity
struct UBP_GameSettings_C_Set_Gain_Intensity_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Gain Intensity
struct UBP_GameSettings_C_Modify_Gain_Intensity_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Gamma Intensity
struct UBP_GameSettings_C_Get_Gamma_Intensity_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Gamma Intensity
struct UBP_GameSettings_C_Set_Gamma_Intensity_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Gamma Intensity
struct UBP_GameSettings_C_Modify_Gamma_Intensity_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Bloom Intensity
struct UBP_GameSettings_C_Get_Bloom_Intensity_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Bloom Intensity
struct UBP_GameSettings_C_Set_Bloom_Intensity_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Bloom Intensity
struct UBP_GameSettings_C_Modify_Bloom_Intensity_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Vsync
struct UBP_GameSettings_C_Get_Vsync_Params
{
	bool                                               Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Vsync
struct UBP_GameSettings_C_Set_Vsync_Params
{
	bool                                               Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Vsync
struct UBP_GameSettings_C_Modify_Vsync_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Remove Field Of View Control From Camera
struct UBP_GameSettings_C_Remove_Field_Of_View_Control_From_Camera_Params
{
	class UCameraComponent*                            Camera;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Add Field Of View Control To Camera
struct UBP_GameSettings_C_Add_Field_Of_View_Control_To_Camera_Params
{
	class UCameraComponent*                            Camera;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Field Of View
struct UBP_GameSettings_C_Get_Field_Of_View_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Field Of View
struct UBP_GameSettings_C_Set_Field_Of_View_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Field Of View
struct UBP_GameSettings_C_Modify_Field_Of_View_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get View Distance
struct UBP_GameSettings_C_Get_View_Distance_Params
{
	float                                              Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Set View Distance
struct UBP_GameSettings_C_Set_View_Distance_Params
{
	float                                              Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify View Distance
struct UBP_GameSettings_C_Modify_View_Distance_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Effect Quality
struct UBP_GameSettings_C_Get_Effect_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Effect Quality
struct UBP_GameSettings_C_Set_Effect_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Effect Quality
struct UBP_GameSettings_C_Modify_Effect_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Texture Quality
struct UBP_GameSettings_C_Get_Texture_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Texture Quality
struct UBP_GameSettings_C_Set_Texture_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Texture Quality
struct UBP_GameSettings_C_Modify_Texture_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Shadow Quality
struct UBP_GameSettings_C_Get_Shadow_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Shadow Quality
struct UBP_GameSettings_C_Set_Shadow_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Shadow Quality
struct UBP_GameSettings_C_Modify_Shadow_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify All Video Settings
struct UBP_GameSettings_C_Modify_All_Video_Settings_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Anti Aliasing Quality
struct UBP_GameSettings_C_Get_Anti_Aliasing_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Anti Aliasing Quality
struct UBP_GameSettings_C_Set_Anti_Aliasing_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Anti Aliasing Quality
struct UBP_GameSettings_C_Modify_Anti_Aliasing_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Text Format Quality Level
struct UBP_GameSettings_C_Get_Text_Format_Quality_Level_Params
{
	int                                                Quality_Level;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Get Post Process Quality
struct UBP_GameSettings_C_Get_Post_Process_Quality_Params
{
	int                                                Value;                                                    // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Formatted;                                                // (Parm, OutParm, ZeroConstructor)
};

// Function BP_GameSettings.BP_GameSettings_C.Set Post Process Quality
struct UBP_GameSettings_C_Set_Post_Process_Quality_Params
{
	int                                                Value;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               Apply;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	int                                                Result;                                                   // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Modify Post Process Quality
struct UBP_GameSettings_C_Modify_Post_Process_Quality_Params
{
	TEnumAsByte<EModifySetting>                        Modify;                                                   // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Update Audio Emittor
struct UBP_GameSettings_C_Update_Audio_Emittor_Params
{
	struct FSAudioUpdateStruct                         Emittor;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm)
	bool                                               Is_Valid;                                                 // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_GameSettings.BP_GameSettings_C.Create Clone
struct UBP_GameSettings_C_Create_Clone_Params
{
	class UBP_GameSettings_C*                          Cloned_Game_Settings;                                     // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
