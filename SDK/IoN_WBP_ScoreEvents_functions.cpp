// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ScoreEvents.WBP_ScoreEvents_C.GetVisibility_Developer
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_ScoreEvents_C::GetVisibility_Developer()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.GetVisibility_Developer");

	UWBP_ScoreEvents_C_GetVisibility_Developer_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.Get_VictimName_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ScoreEvents_C::Get_VictimName_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.Get_VictimName_Text_1");

	UWBP_ScoreEvents_C_Get_VictimName_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKillConfirmed
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         Victim                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHeadshot                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreEvents_C::OnKillConfirmed(class AIONPlayerState* Victim, bool bHeadshot)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKillConfirmed");

	UWBP_ScoreEvents_C_OnKillConfirmed_Params params;
	params.Victim = Victim;
	params.bHeadshot = bHeadshot;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKnockout
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         Victim                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           bHeadshot                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreEvents_C::OnKnockout(class AIONPlayerState* Victim, bool bHeadshot)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnKnockout");

	UWBP_ScoreEvents_C_OnKnockout_Params params;
	params.Victim = Victim;
	params.bHeadshot = bHeadshot;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnScoreStringUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// struct FString*                NewString                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_ScoreEvents_C::OnScoreStringUpdated(struct FString* NewString)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnScoreStringUpdated");

	UWBP_ScoreEvents_C_OnScoreStringUpdated_Params params;
	params.NewString = NewString;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ScoreEvents_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.Construct");

	UWBP_ScoreEvents_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnAccumulatedScoreUpdated
// (Event, Public, BlueprintEvent)
// Parameters:
// int*                           NewScore                       (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreEvents_C::OnAccumulatedScoreUpdated(int* NewScore)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.OnAccumulatedScoreUpdated");

	UWBP_ScoreEvents_C_OnAccumulatedScoreUpdated_Params params;
	params.NewScore = NewScore;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ScoreEvents.WBP_ScoreEvents_C.ExecuteUbergraph_WBP_ScoreEvents
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ScoreEvents_C::ExecuteUbergraph_WBP_ScoreEvents(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ScoreEvents.WBP_ScoreEvents_C.ExecuteUbergraph_WBP_ScoreEvents");

	UWBP_ScoreEvents_C_ExecuteUbergraph_WBP_ScoreEvents_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
