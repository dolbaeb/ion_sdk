#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_RedDot.BP_RedDot_C
// 0x0010 (0x05C0 - 0x05B0)
class ABP_RedDot_C : public AIONSight
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x05B0(0x0008) (Transient, DuplicateTransient)
	class UMaterialInstanceDynamic*                    Scope_Material_Instance;                                  // 0x05B8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_RedDot.BP_RedDot_C");
		return ptr;
	}


	void UserConstructionScript();
	void UpdateMaterialAimState(class AIONFirearm** Firearm, float* AimRatio);
	void DetachFromFirearmBlueprint(class AIONFirearm** Firearm);
	void AttachToFirearmBlueprint(class AIONFirearm** Firearm);
	void ExecuteUbergraph_BP_RedDot(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
