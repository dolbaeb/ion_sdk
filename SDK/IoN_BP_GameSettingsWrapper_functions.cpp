// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_GameSettingsWrapper.BP_GameSettingsWrapper_C.ExecuteUbergraph_BP_GameSettingsWrapper
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettingsWrapper_C::ExecuteUbergraph_BP_GameSettingsWrapper(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettingsWrapper.BP_GameSettingsWrapper_C.ExecuteUbergraph_BP_GameSettingsWrapper");

	UBP_GameSettingsWrapper_C_ExecuteUbergraph_BP_GameSettingsWrapper_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
