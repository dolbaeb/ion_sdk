// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PartyInvite.WBP_PartyInvite_C.SetPlayerName
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 PlayerName                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UWBP_PartyInvite_C::SetPlayerName(const struct FString& PlayerName)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.SetPlayerName");

	UWBP_PartyInvite_C_SetPlayerName_Params params;
	params.PlayerName = PlayerName;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PartyInvite_C::BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature");

	UWBP_PartyInvite_C_BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PartyInvite_C::BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature");

	UWBP_PartyInvite_C_BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PartyInvite.WBP_PartyInvite_C.ExecuteUbergraph_WBP_PartyInvite
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PartyInvite_C::ExecuteUbergraph_WBP_PartyInvite(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.ExecuteUbergraph_WBP_PartyInvite");

	UWBP_PartyInvite_C_ExecuteUbergraph_WBP_PartyInvite_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PartyInvite.WBP_PartyInvite_C.DeclineBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_PartyInvite_C::DeclineBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.DeclineBtnClicked__DelegateSignature");

	UWBP_PartyInvite_C_DeclineBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PartyInvite.WBP_PartyInvite_C.AcceptBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_PartyInvite_C::AcceptBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PartyInvite.WBP_PartyInvite_C.AcceptBtnClicked__DelegateSignature");

	UWBP_PartyInvite_C_AcceptBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
