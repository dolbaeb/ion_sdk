#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_CameraMan.BP_CameraMan_C
// 0x0018 (0x07E8 - 0x07D0)
class ABP_CameraMan_C : public AIONCameraMan
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x07D0(0x0008) (Transient, DuplicateTransient)
	class UWBP_SpectatorOptions_C*                     OptionsMenuWidget;                                        // 0x07D8(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, InstancedReference, IsPlainOldData)
	class UClass*                                      OptionsMenuWidgetClass;                                   // 0x07E0(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_CameraMan.BP_CameraMan_C");
		return ptr;
	}


	void UserConstructionScript();
	void InpActEvt_OBS_Map_K2Node_InputActionEvent_1(const struct FKey& Key);
	void ReceiveBeginPlay();
	void Add_Options_Menu();
	void OnOptionChanged(int* OptionIndex);
	void ReceiveDestroyed();
	void ReceivePossessed(class AController** NewController);
	void ExecuteUbergraph_BP_CameraMan(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
