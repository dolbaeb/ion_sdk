#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_Sako85_Bullet.BP_Sako85_Bullet_C.UserConstructionScript
struct ABP_Sako85_Bullet_C_UserConstructionScript_Params
{
};

// Function BP_Sako85_Bullet.BP_Sako85_Bullet_C.ReceiveBeginPlay
struct ABP_Sako85_Bullet_C_ReceiveBeginPlay_Params
{
};

// Function BP_Sako85_Bullet.BP_Sako85_Bullet_C.ReceiveEndPlay
struct ABP_Sako85_Bullet_C_ReceiveEndPlay_Params
{
	TEnumAsByte<EEndPlayReason>*                       EndPlayReason;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function BP_Sako85_Bullet.BP_Sako85_Bullet_C.ExecuteUbergraph_BP_Sako85_Bullet
struct ABP_Sako85_Bullet_C_ExecuteUbergraph_BP_Sako85_Bullet_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
