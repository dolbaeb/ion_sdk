// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ChatBox.WBP_ChatBox_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ChatBox_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.GetText_1");

	UWBP_ChatBox_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Get_ChatTextBox_HintText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_ChatBox_C::Get_ChatTextBox_HintText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Get_ChatTextBox_HintText_1");

	UWBP_ChatBox_C_Get_ChatTextBox_HintText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_ChatBox.WBP_ChatBox_C.CleanChat
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_ChatBox_C::CleanChat()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.CleanChat");

	UWBP_ChatBox_C_CleanChat_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ChatBox_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Construct");

	UWBP_ChatBox_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Start Message
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FName                   Type                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBox_C::Start_Message(const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Start Message");

	UWBP_ChatBox_C_Start_Message_Params params;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// TEnumAsByte<ETextCommit>       CommitMethod                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBox_C::BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature(const struct FText& Text, TEnumAsByte<ETextCommit> CommitMethod)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature");

	UWBP_ChatBox_C_BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_5_OnEditableTextBoxCommittedEvent__DelegateSignature_Params params;
	params.Text = Text;
	params.CommitMethod = CommitMethod;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature
// (HasOutParms, BlueprintEvent)
// Parameters:
// struct FText                   Text                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UWBP_ChatBox_C::BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature(const struct FText& Text)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature");

	UWBP_ChatBox_C_BndEvt__ChatTextBox_K2Node_ComponentBoundEvent_3_OnEditableTextBoxChangedEvent__DelegateSignature_Params params;
	params.Text = Text;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.OnReceivedChatMessage
// (HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         From                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Message                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FName                   Type                           (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UWBP_ChatBox_C::OnReceivedChatMessage(class AIONPlayerState* From, const struct FString& Message, const struct FName& Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.OnReceivedChatMessage");

	UWBP_ChatBox_C_OnReceivedChatMessage_Params params;
	params.From = From;
	params.Message = Message;
	params.Type = Type;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Show Chat
// (BlueprintCallable, BlueprintEvent)

void UWBP_ChatBox_C::Show_Chat()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Show Chat");

	UWBP_ChatBox_C_Show_Chat_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat
// (BlueprintCallable, BlueprintEvent)

void UWBP_ChatBox_C::Hide_Chat()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat");

	UWBP_ChatBox_C_Hide_Chat_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat Hint
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Instant                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBox_C::Hide_Chat_Hint(bool Instant)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.Hide Chat Hint");

	UWBP_ChatBox_C_Hide_Chat_Hint_Params params;
	params.Instant = Instant;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_ChatBox_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.OnAnimationFinished");

	UWBP_ChatBox_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatBox.WBP_ChatBox_C.ExecuteUbergraph_WBP_ChatBox
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatBox_C::ExecuteUbergraph_WBP_ChatBox(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatBox.WBP_ChatBox_C.ExecuteUbergraph_WBP_ChatBox");

	UWBP_ChatBox_C_ExecuteUbergraph_WBP_ChatBox_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
