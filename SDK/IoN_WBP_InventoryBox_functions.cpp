// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_InventoryBox.WBP_InventoryBox_C.DeselectItem
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryBox_C::DeselectItem()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.DeselectItem");

	UWBP_InventoryBox_C_DeselectItem_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.ClearBtnSelectionState
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryBox_C::ClearBtnSelectionState()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.ClearBtnSelectionState");

	UWBP_InventoryBox_C_ClearBtnSelectionState_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.CacheCategoryButtons
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_InventoryBox_C::CacheCategoryButtons()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.CacheCategoryButtons");

	UWBP_InventoryBox_C_CacheCategoryButtons_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.SetBtnSelectionState
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_WeaponCategoryBtn_C* SelectedButton                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_InventoryBox_C::SetBtnSelectionState(class UWBP_WeaponCategoryBtn_C* SelectedButton)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.SetBtnSelectionState");

	UWBP_InventoryBox_C_SetBtnSelectionState_Params params;
	params.SelectedButton = SelectedButton;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.ToogleInspectPanel
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bShow                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryBox_C::ToogleInspectPanel(bool bShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.ToogleInspectPanel");

	UWBP_InventoryBox_C_ToogleInspectPanel_Params params;
	params.bShow = bShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.ShowCrateButtons
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           bShow                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryBox_C::ShowCrateButtons(bool bShow)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.ShowCrateButtons");

	UWBP_InventoryBox_C_ShowCrateButtons_Params params;
	params.bShow = bShow;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.NewItemSelected
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  NewItem                        (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_InventoryBox_C::NewItemSelected(const struct FIONSteamInventoryItem& NewItem)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.NewItemSelected");

	UWBP_InventoryBox_C_NewItemSelected_Params params;
	params.NewItem = NewItem;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.GetCurrentSelection
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure, Const)
// Parameters:
// class UWBP_Icon_C*             CurrentSelection               (Parm, OutParm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_InventoryBox_C::GetCurrentSelection(class UWBP_Icon_C** CurrentSelection)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.GetCurrentSelection");

	UWBP_InventoryBox_C_GetCurrentSelection_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (CurrentSelection != nullptr)
		*CurrentSelection = params.CurrentSelection;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__Open_K2Node_ComponentBoundEvent_112_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_InventoryBox_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.Construct");

	UWBP_InventoryBox_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__ViewBtn_K2Node_ComponentBoundEvent_52_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__AK47Btn_K2Node_ComponentBoundEvent_699_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__Mk18Btn_K2Node_ComponentBoundEvent_700_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__AllBtn_K2Node_ComponentBoundEvent_732_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__WBP_WeaponCategoryBtn_K2Node_ComponentBoundEvent_740_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__WBP_WeaponCategoryBtn_C_3_K2Node_ComponentBoundEvent_749_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__StormBtn_K2Node_ComponentBoundEvent_1171_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__SakoBtn_K2Node_ComponentBoundEvent_1182_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__DEBtn_K2Node_ComponentBoundEvent_1194_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__M9Btn_K2Node_ComponentBoundEvent_1207_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__M1014Btn_K2Node_ComponentBoundEvent_1221_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__KnifeBtn_K2Node_ComponentBoundEvent_1236_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__EquipBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__HelmetBtn_K2Node_ComponentBoundEvent_600_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__ChestBtn_K2Node_ComponentBoundEvent_622_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__ArmsBtn_K2Node_ComponentBoundEvent_623_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__LegsBtn_K2Node_ComponentBoundEvent_646_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__WBP_ItemCategory_K2Node_ComponentBoundEvent_404_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__ArmorBtn_K2Node_ComponentBoundEvent_827_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__AllBtn__K2Node_ComponentBoundEvent_1211_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__DropsuitsBtn_K2Node_ComponentBoundEvent_1234_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__CratesBtn_K2Node_ComponentBoundEvent_1258_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_InventoryBox_C::BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__ArmorAllBtn_K2Node_ComponentBoundEvent_1_BtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryBox_C::BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature");

	UWBP_InventoryBox_C_BndEvt__DropDown_K2Node_ComponentBoundEvent_0_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.ExecuteUbergraph_WBP_InventoryBox
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_InventoryBox_C::ExecuteUbergraph_WBP_InventoryBox(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.ExecuteUbergraph_WBP_InventoryBox");

	UWBP_InventoryBox_C_ExecuteUbergraph_WBP_InventoryBox_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_InventoryBox.WBP_InventoryBox_C.CrateOpen__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  CrateToOpen                    (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_InventoryBox_C::CrateOpen__DelegateSignature(const struct FIONSteamInventoryItem& CrateToOpen)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_InventoryBox.WBP_InventoryBox_C.CrateOpen__DelegateSignature");

	UWBP_InventoryBox_C_CrateOpen__DelegateSignature_Params params;
	params.CrateToOpen = CrateToOpen;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
