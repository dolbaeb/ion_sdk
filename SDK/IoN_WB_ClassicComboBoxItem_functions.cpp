// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WB_ClassicComboBoxItem.WB_ClassicComboBoxItem_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWB_ClassicComboBoxItem_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicComboBoxItem.WB_ClassicComboBoxItem_C.Construct");

	UWB_ClassicComboBoxItem_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WB_ClassicComboBoxItem.WB_ClassicComboBoxItem_C.ExecuteUbergraph_WB_ClassicComboBoxItem
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWB_ClassicComboBoxItem_C::ExecuteUbergraph_WB_ClassicComboBoxItem(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WB_ClassicComboBoxItem.WB_ClassicComboBoxItem_C.ExecuteUbergraph_WB_ClassicComboBoxItem");

	UWB_ClassicComboBoxItem_C_ExecuteUbergraph_WB_ClassicComboBoxItem_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
