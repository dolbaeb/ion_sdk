// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_PostMatchPlayer_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Construct");

	UWBP_PostMatchPlayer_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Fill UI Values
// (BlueprintCallable, BlueprintEvent)

void UWBP_PostMatchPlayer_C::Fill_UI_Values()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.Fill UI Values");

	UWBP_PostMatchPlayer_C_Fill_UI_Values_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.ExecuteUbergraph_WBP_PostMatchPlayer
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PostMatchPlayer_C::ExecuteUbergraph_WBP_PostMatchPlayer(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PostMatchPlayer.WBP_PostMatchPlayer_C.ExecuteUbergraph_WBP_PostMatchPlayer");

	UWBP_PostMatchPlayer_C_ExecuteUbergraph_WBP_PostMatchPlayer_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
