#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EModifySetting.EModifySetting
enum class EModifySetting : uint8_t
{
	EModifySetting__NewEnumerator0 = 0,
	EModifySetting__NewEnumerator1 = 1,
	EModifySetting__NewEnumerator2 = 2,
	EModifySetting__EModifySetting_MAX = 3
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
