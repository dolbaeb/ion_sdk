#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_FriendList.WBP_FriendList_C.OnMouseButtonDown
struct UWBP_FriendList_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_1_Text_1
struct UWBP_FriendList_C_Get_TextBlock_1_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_FriendList.WBP_FriendList_C.Get_TextBlock_0_Text_1
struct UWBP_FriendList_C_Get_TextBlock_0_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_FriendList.WBP_FriendList_C.GetText_1
struct UWBP_FriendList_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_FriendList.WBP_FriendList_C.OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2
struct UWBP_FriendList_C_OnFailure_EB4618DD4AA4523F33C54989A1F2F1B2_Params
{
	TArray<struct FBlueprintOnlineFriend>              FriendsList;                                              // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_FriendList.WBP_FriendList_C.OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2
struct UWBP_FriendList_C_OnSuccess_EB4618DD4AA4523F33C54989A1F2F1B2_Params
{
	TArray<struct FBlueprintOnlineFriend>              FriendsList;                                              // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_FriendList.WBP_FriendList_C.Construct
struct UWBP_FriendList_C_Construct_Params
{
};

// Function WBP_FriendList.WBP_FriendList_C.UpdateFriendsList
struct UWBP_FriendList_C_UpdateFriendsList_Params
{
};

// Function WBP_FriendList.WBP_FriendList_C.AddPartyBtnPressed
struct UWBP_FriendList_C_AddPartyBtnPressed_Params
{
	struct FBlueprintOnlineFriend                      OnlineFriend;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_FriendList.WBP_FriendList_C.BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
struct UWBP_FriendList_C_BndEvt__RefreshBtn_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_FriendList.WBP_FriendList_C.OnUpdateFriendList
struct UWBP_FriendList_C_OnUpdateFriendList_Params
{
};

// Function WBP_FriendList.WBP_FriendList_C.ExecuteUbergraph_WBP_FriendList
struct UWBP_FriendList_C_ExecuteUbergraph_WBP_FriendList_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_FriendList.WBP_FriendList_C.AddPartyMember__DelegateSignature
struct UWBP_FriendList_C_AddPartyMember__DelegateSignature_Params
{
	struct FBlueprintOnlineFriend                      OnlineFriend;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
