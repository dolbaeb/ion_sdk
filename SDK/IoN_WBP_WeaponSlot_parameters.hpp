#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetToolTipWidget_1
struct UWBP_WeaponSlot_C_GetToolTipWidget_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.UpdateWeaponSkin
struct UWBP_WeaponSlot_C_UpdateWeaponSkin_Params
{
	class UIONSteamItem*                               WeaponSkin;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_DragReceive_Background_1
struct UWBP_WeaponSlot_C_Get_DragReceive_Background_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Highlight_Background_1
struct UWBP_WeaponSlot_C_Get_Highlight_Background_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetLabelText
struct UWBP_WeaponSlot_C_GetLabelText_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_Frame_Brush_1
struct UWBP_WeaponSlot_C_Get_Frame_Brush_1_Params
{
	struct FSlateBrush                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetCurrentItem
struct UWBP_WeaponSlot_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.GetText_1
struct UWBP_WeaponSlot_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.AcceptsDropForItem
struct UWBP_WeaponSlot_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmountText_Text_1
struct UWBP_WeaponSlot_C_Get_AmountText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Get_AmmoText_Text_1
struct UWBP_WeaponSlot_C_Get_AmmoText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnDrop
struct UWBP_WeaponSlot_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemChanged
struct UWBP_WeaponSlot_C_OnItemChanged_Params
{
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnItemClicked
struct UWBP_WeaponSlot_C_OnItemClicked_Params
{
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Construct
struct UWBP_WeaponSlot_C_Construct_Params
{
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.Tick
struct UWBP_WeaponSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.OnWeaponSkinChanged
struct UWBP_WeaponSlot_C_OnWeaponSkinChanged_Params
{
	class UIONWeaponSkin*                              WeaponSkin;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponSlot.WBP_WeaponSlot_C.ExecuteUbergraph_WBP_WeaponSlot
struct UWBP_WeaponSlot_C_ExecuteUbergraph_WBP_WeaponSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
