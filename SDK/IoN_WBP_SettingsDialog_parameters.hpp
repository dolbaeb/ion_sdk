#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnMouseButtonDown_1
struct UWBP_SettingsDialog_C_OnMouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.OnKeyDown
struct UWBP_SettingsDialog_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Switch Tab
struct UWBP_SettingsDialog_C_Switch_Tab_Params
{
	class UWBP_SettingsSectionButton_C*                Selected_Button;                                          // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                Active_Widget_Index;                                      // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Cancel Save Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__Cancel_Save_Button_K2Node_ComponentBoundEvent_318_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Open Menu
struct UWBP_SettingsDialog_C_Open_Menu_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Close Menu
struct UWBP_SettingsDialog_C_Close_Menu_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_761_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Apply And Close
struct UWBP_SettingsDialog_C_Apply_And_Close_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_182_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__Button_Close_K2Node_ComponentBoundEvent_228_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__WBP_SettingsSectionButton_121_K2Node_ComponentBoundEvent_628_OnClicked__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__TabButton_Audio_K2Node_ComponentBoundEvent_1631_OnClicked__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__TabButton_Controls_K2Node_ComponentBoundEvent_1698_OnClicked__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Begin Tab Switch
struct UWBP_SettingsDialog_C_Begin_Tab_Switch_Params
{
	class UWBP_SettingsSectionButton_C*                SelectedButton;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
	int                                                ActiveWidgetIndex;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__TabButton_Bindings_K2Node_ComponentBoundEvent_174_OnClicked__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature
struct UWBP_SettingsDialog_C_BndEvt__TabButton_Gameplay_K2Node_ComponentBoundEvent_348_OnClicked__DelegateSignature_Params
{
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.ExecuteUbergraph_WBP_SettingsDialog
struct UWBP_SettingsDialog_C_ExecuteUbergraph_WBP_SettingsDialog_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_SettingsDialog.WBP_SettingsDialog_C.Menu Closed__DelegateSignature
struct UWBP_SettingsDialog_C_Menu_Closed__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
