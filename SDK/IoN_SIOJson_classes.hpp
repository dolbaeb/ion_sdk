#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class SIOJson.SIOJConvert
// 0x0000 (0x0030 - 0x0030)
class USIOJConvert : public UObject
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SIOJson.SIOJConvert");
		return ptr;
	}

};


// Class SIOJson.SIOJLibrary
// 0x0000 (0x0030 - 0x0030)
class USIOJLibrary : public UBlueprintFunctionLibrary
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SIOJson.SIOJLibrary");
		return ptr;
	}


	class USIOJsonObject* STATIC_StructToJsonObject(class UProperty* AnyStruct);
	bool STATIC_StringToJsonValueArray(const struct FString& JSONString, TArray<class USIOJsonValue*>* OutJsonValueArray);
	struct FString STATIC_PercentEncode(const struct FString& Source);
	bool STATIC_JsonObjectToStruct(class USIOJsonObject* JsonObject, class UProperty* AnyStruct);
	class USIOJsonValue* STATIC_Conv_StringToJsonValue(const struct FString& inString);
	class USIOJsonObject* STATIC_Conv_JsonValueToJsonObject(class USIOJsonValue* InValue);
	int STATIC_Conv_JsonValueToInt(class USIOJsonValue* InValue);
	float STATIC_Conv_JsonValueToFloat(class USIOJsonValue* InValue);
	TArray<unsigned char> STATIC_Conv_JsonValueToBytes(class USIOJsonValue* InValue);
	bool STATIC_Conv_JsonValueToBool(class USIOJsonValue* InValue);
	struct FString STATIC_Conv_JsonObjectToString(class USIOJsonObject* InObject);
	class USIOJsonValue* STATIC_Conv_JsonObjectToJsonValue(class USIOJsonObject* InObject);
	class USIOJsonValue* STATIC_Conv_IntToJsonValue(int inInt);
	class USIOJsonValue* STATIC_Conv_FloatToJsonValue(float InFloat);
	class USIOJsonValue* STATIC_Conv_BytesToJsonValue(TArray<unsigned char> InBytes);
	class USIOJsonValue* STATIC_Conv_BoolToJsonValue(bool InBool);
	class USIOJsonValue* STATIC_Conv_ArrayToJsonValue(TArray<class USIOJsonValue*> inArray);
	void STATIC_CallURL(class UObject* WorldContextObject, const struct FString& URL, ESIORequestVerb Verb, ESIORequestContentType ContentType, class USIOJsonObject* SIOJJson, const struct FScriptDelegate& Callback);
	struct FString STATIC_Base64Encode(const struct FString& Source);
	bool STATIC_Base64Decode(const struct FString& Source, struct FString* Dest);
};


// Class SIOJson.SIOJRequestJSON
// 0x0230 (0x0260 - 0x0030)
class USIOJRequestJSON : public UObject
{
public:
	struct FScriptMulticastDelegate                    OnRequestComplete;                                        // 0x0030(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	struct FScriptMulticastDelegate                    OnRequestFail;                                            // 0x0040(0x0010) (ZeroConstructor, InstancedReference, BlueprintAssignable)
	unsigned char                                      UnknownData00[0xF0];                                      // 0x0050(0x00F0) MISSED OFFSET
	struct FString                                     ResponseContent;                                          // 0x0140(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst)
	bool                                               bIsValidJsonResponse;                                     // 0x0150(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData01[0xF];                                       // 0x0151(0x000F) MISSED OFFSET
	class USIOJsonObject*                              RequestJsonObj;                                           // 0x0160(0x0008) (ZeroConstructor, IsPlainOldData)
	TArray<unsigned char>                              RequestBytes;                                             // 0x0168(0x0010) (ZeroConstructor)
	struct FString                                     BinaryContentType;                                        // 0x0178(0x0010) (ZeroConstructor)
	class USIOJsonObject*                              ResponseJsonObj;                                          // 0x0188(0x0008) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0xD0];                                      // 0x0190(0x00D0) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SIOJson.SIOJRequestJSON");
		return ptr;
	}


	void SetVerb(ESIORequestVerb Verb);
	void SetResponseObject(class USIOJsonObject* JsonObject);
	void SetRequestObject(class USIOJsonObject* JsonObject);
	void SetHeader(const struct FString& HeaderName, const struct FString& HeaderValue);
	void SetCustomVerb(const struct FString& Verb);
	void SetContentType(ESIORequestContentType ContentType);
	void SetBinaryRequestContent(TArray<unsigned char> Content);
	void SetBinaryContentType(const struct FString& ContentType);
	void ResetResponseData();
	void ResetRequestData();
	void ResetData();
	int RemoveTag(const struct FName& Tag);
	void ProcessURL(const struct FString& URL);
	bool HasTag(const struct FName& Tag);
	struct FString GetUrl();
	ESIORequestStatus GetStatus();
	class USIOJsonObject* GetResponseObject();
	struct FString GetResponseHeader(const struct FString& HeaderName);
	int GetResponseCode();
	class USIOJsonObject* GetRequestObject();
	TArray<struct FString> GetAllResponseHeaders();
	class USIOJRequestJSON* STATIC_ConstructRequestExt(class UObject* WorldContextObject, ESIORequestVerb Verb, ESIORequestContentType ContentType);
	class USIOJRequestJSON* STATIC_ConstructRequest(class UObject* WorldContextObject);
	void Cancel();
	void ApplyURL(const struct FString& URL, class UObject* WorldContextObject, const struct FLatentActionInfo& LatentInfo, class USIOJsonObject** Result);
	void AddTag(const struct FName& Tag);
};


// Class SIOJson.SIOJsonObject
// 0x0010 (0x0040 - 0x0030)
class USIOJsonObject : public UObject
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0030(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SIOJson.SIOJsonObject");
		return ptr;
	}


	void SetStringField(const struct FString& FieldName, const struct FString& StringValue);
	void SetStringArrayField(const struct FString& FieldName, TArray<struct FString> StringArray);
	void SetObjectField(const struct FString& FieldName, class USIOJsonObject* JsonObject);
	void SetObjectArrayField(const struct FString& FieldName, TArray<class USIOJsonObject*> ObjectArray);
	void SetNumberField(const struct FString& FieldName, float Number);
	void SetNumberArrayField(const struct FString& FieldName, TArray<float> NumberArray);
	void SetField(const struct FString& FieldName, class USIOJsonValue* JsonValue);
	void SetBoolField(const struct FString& FieldName, bool InValue);
	void SetBoolArrayField(const struct FString& FieldName, TArray<bool> BoolArray);
	void SetBinaryField(const struct FString& FieldName, TArray<unsigned char> Bytes);
	void SetArrayField(const struct FString& FieldName, TArray<class USIOJsonValue*> inArray);
	void Reset();
	void RemoveField(const struct FString& FieldName);
	void MergeJsonObject(class USIOJsonObject* InJsonObject, bool Overwrite);
	bool HasField(const struct FString& FieldName);
	struct FString GetStringField(const struct FString& FieldName);
	TArray<struct FString> GetStringArrayField(const struct FString& FieldName);
	class USIOJsonObject* GetObjectField(const struct FString& FieldName);
	TArray<class USIOJsonObject*> GetObjectArrayField(const struct FString& FieldName);
	float GetNumberField(const struct FString& FieldName);
	TArray<float> GetNumberArrayField(const struct FString& FieldName);
	TArray<struct FString> GetFieldNames();
	class USIOJsonValue* GetField(const struct FString& FieldName);
	bool GetBoolField(const struct FString& FieldName);
	TArray<bool> GetBoolArrayField(const struct FString& FieldName);
	TArray<unsigned char> GetBinaryField(const struct FString& FieldName);
	TArray<class USIOJsonValue*> GetArrayField(const struct FString& FieldName);
	struct FString EncodeJsonToSingleString();
	struct FString EncodeJson();
	bool DecodeJson(const struct FString& JSONString);
	class USIOJsonObject* STATIC_ConstructJsonObject(class UObject* WorldContextObject);
};


// Class SIOJson.SIOJsonValue
// 0x0010 (0x0040 - 0x0030)
class USIOJsonValue : public UObject
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0030(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("Class SIOJson.SIOJsonValue");
		return ptr;
	}


	class USIOJsonValue* STATIC_ValueFromJsonString(class UObject* WorldContextObject, const struct FString& StringValue);
	bool IsNull();
	struct FString GetTypeString();
	TEnumAsByte<ESIOJson> GetType();
	struct FString EncodeJson();
	class USIOJsonValue* STATIC_ConstructJsonValueString(class UObject* WorldContextObject, const struct FString& StringValue);
	class USIOJsonValue* STATIC_ConstructJsonValueObject(class USIOJsonObject* JsonObject, class UObject* WorldContextObject);
	class USIOJsonValue* STATIC_ConstructJsonValueNumber(class UObject* WorldContextObject, float Number);
	class USIOJsonValue* STATIC_ConstructJsonValueBool(class UObject* WorldContextObject, bool InValue);
	class USIOJsonValue* STATIC_ConstructJsonValueBinary(class UObject* WorldContextObject, TArray<unsigned char> ByteArray);
	class USIOJsonValue* STATIC_ConstructJsonValueArray(class UObject* WorldContextObject, TArray<class USIOJsonValue*> inArray);
	struct FString AsString();
	class USIOJsonObject* AsObject();
	float AsNumber();
	bool AsBool();
	TArray<unsigned char> AsBinary();
	TArray<class USIOJsonValue*> AsArray();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
