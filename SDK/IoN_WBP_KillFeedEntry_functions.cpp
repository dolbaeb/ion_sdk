// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_Border_Distance_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_KillFeedEntry_C::Get_Border_Distance_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_Border_Distance_Visibility_1");

	UWBP_KillFeedEntry_C_Get_Border_Distance_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetText_Distance
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_KillFeedEntry_C::GetText_Distance()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetText_Distance");

	UWBP_KillFeedEntry_C_GetText_Distance_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_ColorAndOpacity_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_KillFeedEntry_C::Get_WeaponIcon_ColorAndOpacity_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_ColorAndOpacity_1");

	UWBP_KillFeedEntry_C_Get_WeaponIcon_ColorAndOpacity_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetRelationShipColor
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class APlayerState*            OtherPlayer                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_KillFeedEntry_C::GetRelationShipColor(class APlayerState* OtherPlayer)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.GetRelationShipColor");

	UWBP_KillFeedEntry_C_GetRelationShipColor_Params params;
	params.OtherPlayer = OtherPlayer;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_InfoText
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_KillFeedEntry_C::Get_InfoText()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_InfoText");

	UWBP_KillFeedEntry_C_Get_InfoText_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_Brush_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSlateBrush             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FSlateBrush UWBP_KillFeedEntry_C::Get_WeaponIcon_Brush_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_WeaponIcon_Brush_1");

	UWBP_KillFeedEntry_C_Get_WeaponIcon_Brush_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_VictimText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_KillFeedEntry_C::Get_VictimText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_VictimText_Text_1");

	UWBP_KillFeedEntry_C_Get_VictimText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_KillerText_Text_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_KillFeedEntry_C::Get_KillerText_Text_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_KillerText_Text_1");

	UWBP_KillFeedEntry_C_Get_KillerText_Text_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_HeadshotIcon_Visibility_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// ESlateVisibility               ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)

ESlateVisibility UWBP_KillFeedEntry_C::Get_HeadshotIcon_Visibility_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Get_HeadshotIcon_Visibility_1");

	UWBP_KillFeedEntry_C_Get_HeadshotIcon_Visibility_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_KillFeedEntry_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnAnimationFinished");

	UWBP_KillFeedEntry_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_KillFeedEntry_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.Construct");

	UWBP_KillFeedEntry_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_KillFeedEntry_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.PreConstruct");

	UWBP_KillFeedEntry_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.ExecuteUbergraph_WBP_KillFeedEntry
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_KillFeedEntry_C::ExecuteUbergraph_WBP_KillFeedEntry(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.ExecuteUbergraph_WBP_KillFeedEntry");

	UWBP_KillFeedEntry_C_ExecuteUbergraph_WBP_KillFeedEntry_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnFinished__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_KillFeedEntry_C*    Entry                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_KillFeedEntry_C::OnFinished__DelegateSignature(class UWBP_KillFeedEntry_C* Entry)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_KillFeedEntry.WBP_KillFeedEntry_C.OnFinished__DelegateSignature");

	UWBP_KillFeedEntry_C_OnFinished__DelegateSignature_Params params;
	params.Entry = Entry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
