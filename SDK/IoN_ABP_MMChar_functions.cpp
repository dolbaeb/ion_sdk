// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_MMChar.ABP_MMChar_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA
// (BlueprintEvent)

void UABP_MMChar_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MMChar.ABP_MMChar_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA");

	UABP_MMChar_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MMChar_AnimGraphNode_SequencePlayer_DA5650434EEC0310151098B1AF9E09BA_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_MMChar.ABP_MMChar_C.ExecuteUbergraph_ABP_MMChar
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_MMChar_C::ExecuteUbergraph_ABP_MMChar(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_MMChar.ABP_MMChar_C.ExecuteUbergraph_ABP_MMChar");

	UABP_MMChar_C_ExecuteUbergraph_ABP_MMChar_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
