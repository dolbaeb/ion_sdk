#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PartyInvite.WBP_PartyInvite_C.SetPlayerName
struct UWBP_PartyInvite_C_SetPlayerName_Params
{
	struct FString                                     PlayerName;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature
struct UWBP_PartyInvite_C_BndEvt__AcceptBtn_K2Node_ComponentBoundEvent_105_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PartyInvite.WBP_PartyInvite_C.BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
struct UWBP_PartyInvite_C_BndEvt__DeclineBtn_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PartyInvite.WBP_PartyInvite_C.ExecuteUbergraph_WBP_PartyInvite
struct UWBP_PartyInvite_C_ExecuteUbergraph_WBP_PartyInvite_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PartyInvite.WBP_PartyInvite_C.DeclineBtnClicked__DelegateSignature
struct UWBP_PartyInvite_C_DeclineBtnClicked__DelegateSignature_Params
{
};

// Function WBP_PartyInvite.WBP_PartyInvite_C.AcceptBtnClicked__DelegateSignature
struct UWBP_PartyInvite_C_AcceptBtnClicked__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
