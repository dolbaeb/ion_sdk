// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_GameSettings.BP_GameSettings_C.Get Foliage Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Foliage_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Foliage Quality");

	UBP_GameSettings_C_Get_Foliage_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Foliage Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Foliage_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Foliage Quality");

	UBP_GameSettings_C_Set_Foliage_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Foliage Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Foliage_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Foliage Quality");

	UBP_GameSettings_C_Modify_Foliage_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume UI
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_UI(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume UI");

	UBP_GameSettings_C_Get_Volume_UI_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier UI
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_UI(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier UI");

	UBP_GameSettings_C_Get_Audio_Multiplier_UI_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier UI
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_UI(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier UI");

	UBP_GameSettings_C_Set_Audio_Multiplier_UI_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier UI
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_UI(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier UI");

	UBP_GameSettings_C_Modify_Audio_Multiplier_UI_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Save ini Settings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Save_ini_Settings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Save ini Settings");

	UBP_GameSettings_C_Save_ini_Settings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Load ini Settings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Load_ini_Settings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Load ini Settings");

	UBP_GameSettings_C_Load_ini_Settings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Init Save Game Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TScriptInterface<class UBPI_GameSettingsInterface_C> Game_Settings_Interface        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Init_Save_Game_Settings(const TScriptInterface<class UBPI_GameSettingsInterface_C>& Game_Settings_Interface)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Init Save Game Settings");

	UBP_GameSettings_C_Init_Save_Game_Settings_Params params;
	params.Game_Settings_Interface = Game_Settings_Interface;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Saved Key Inputs
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// TArray<struct FSKeyActionSave> Saved_Key_Inputs               (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Saved_Key_Inputs(TArray<struct FSKeyActionSave>* Saved_Key_Inputs)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Saved Key Inputs");

	UBP_GameSettings_C_Get_Saved_Key_Inputs_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Saved_Key_Inputs != nullptr)
		*Saved_Key_Inputs = params.Saved_Key_Inputs;
}


// Function BP_GameSettings.BP_GameSettings_C.Get All Key Actions
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// TArray<class UBP_KeyAction_C*> Key_Actions                    (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_All_Key_Actions(TArray<class UBP_KeyAction_C*>* Key_Actions)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get All Key Actions");

	UBP_GameSettings_C_Get_All_Key_Actions_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Key_Actions != nullptr)
		*Key_Actions = params.Key_Actions;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Save File User Index
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Save_File_User_Index           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Save_File_User_Index(int Save_File_User_Index)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Save File User Index");

	UBP_GameSettings_C_Set_Save_File_User_Index_Params params;
	params.Save_File_User_Index = Save_File_User_Index;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Save File Name
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Save_File_Name                 (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)

void UBP_GameSettings_C::Set_Save_File_Name(const struct FString& Save_File_Name)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Save File Name");

	UBP_GameSettings_C_Set_Save_File_Name_Params params;
	params.Save_File_Name = Save_File_Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Game Settings Interface
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TScriptInterface<class UBPI_GameSettingsInterface_C> Game_Settings_Interface        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Game_Settings_Interface(const TScriptInterface<class UBPI_GameSettingsInterface_C>& Game_Settings_Interface)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Game Settings Interface");

	UBP_GameSettings_C_Set_Game_Settings_Interface_Params params;
	params.Game_Settings_Interface = Game_Settings_Interface;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get All Combinations
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TArray<class UBP_KeyCombination_C*> Combinations                   (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_All_Combinations(TArray<class UBP_KeyCombination_C*>* Combinations)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get All Combinations");

	UBP_GameSettings_C_Get_All_Combinations_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Combinations != nullptr)
		*Combinations = params.Combinations;
}


// Function BP_GameSettings.BP_GameSettings_C.Generate Keybinding Conflicts
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Generate_Keybinding_Conflicts()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Generate Keybinding Conflicts");

	UBP_GameSettings_C_Generate_Keybinding_Conflicts_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Keybindings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Keybindings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Keybindings");

	UBP_GameSettings_C_Modify_Keybindings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Load Key Actions
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Load_Key_Actions()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Load Key Actions");

	UBP_GameSettings_C_Load_Key_Actions_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Save Key Actions
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Save_Key_Actions()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Save Key Actions");

	UBP_GameSettings_C_Save_Key_Actions_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Store Key Input
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyActionSave         KeySave                        (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)

void UBP_GameSettings_C::Store_Key_Input(const struct FSKeyActionSave& KeySave)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Store Key Input");

	UBP_GameSettings_C_Store_Key_Input_Params params;
	params.KeySave = KeySave;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Key Action
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Input_Action_Name              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UBP_KeyAction_C*         Input_Action                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// bool                           Success                        (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Key_Action(const struct FString& Input_Action_Name, class UBP_KeyAction_C** Input_Action, bool* Success)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Key Action");

	UBP_GameSettings_C_Get_Key_Action_Params params;
	params.Input_Action_Name = Input_Action_Name;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Input_Action != nullptr)
		*Input_Action = params.Input_Action;
	if (Success != nullptr)
		*Success = params.Success;
}


// Function BP_GameSettings.BP_GameSettings_C.Fill Float Axis Inputs List
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Fill_Float_Axis_Inputs_List()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Fill Float Axis Inputs List");

	UBP_GameSettings_C_Fill_Float_Axis_Inputs_List_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Init Key Bindings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Init_Key_Bindings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Init Key Bindings");

	UBP_GameSettings_C_Init_Key_Bindings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Update Actions Input State
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Real_Time_Seconds              (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          World_Delta_Seconds            (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// class APlayerController*       PlayerController               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Update_Actions_Input_State(float Real_Time_Seconds, float World_Delta_Seconds, class APlayerController* PlayerController)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Update Actions Input State");

	UBP_GameSettings_C_Update_Actions_Input_State_Params params;
	params.Real_Time_Seconds = Real_Time_Seconds;
	params.World_Delta_Seconds = World_Delta_Seconds;
	params.PlayerController = PlayerController;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Delete Settings Save File
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Delete_Settings_Save_File()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Delete Settings Save File");

	UBP_GameSettings_C_Delete_Settings_Save_File_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined X
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Input_Axis_X                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          World_Delta                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Horizontal_X                   (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Sensitivity_Combined_X(float Input_Axis_X, float World_Delta, float* Horizontal_X)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined X");

	UBP_GameSettings_C_Get_Look_Sensitivity_Combined_X_Params params;
	params.Input_Axis_X = Input_Axis_X;
	params.World_Delta = World_Delta;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Horizontal_X != nullptr)
		*Horizontal_X = params.Horizontal_X;
}


// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Combobox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FString                 Value                          (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_My_Custom_Combobox(struct FString* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get My Custom Combobox");

	UBP_GameSettings_C_Get_My_Custom_Combobox_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Combobox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Result                         (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Set_My_Custom_Combobox(const struct FString& Value, bool Apply, struct FString* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set My Custom Combobox");

	UBP_GameSettings_C_Set_My_Custom_Combobox_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Combobox
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_My_Custom_Combobox(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Combobox");

	UBP_GameSettings_C_Modify_My_Custom_Combobox_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Radiobox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_My_Custom_Radiobox(int* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get My Custom Radiobox");

	UBP_GameSettings_C_Get_My_Custom_Radiobox_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Radiobox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_My_Custom_Radiobox(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set My Custom Radiobox");

	UBP_GameSettings_C_Set_My_Custom_Radiobox_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Radiobox
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_My_Custom_Radiobox(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Radiobox");

	UBP_GameSettings_C_Modify_My_Custom_Radiobox_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Slider
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_My_Custom_Slider(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get My Custom Slider");

	UBP_GameSettings_C_Get_My_Custom_Slider_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Slider
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_My_Custom_Slider(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set My Custom Slider");

	UBP_GameSettings_C_Set_My_Custom_Slider_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Slider
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_My_Custom_Slider(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Slider");

	UBP_GameSettings_C_Modify_My_Custom_Slider_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get My Custom Checkbox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_My_Custom_Checkbox(bool* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get My Custom Checkbox");

	UBP_GameSettings_C_Get_My_Custom_Checkbox_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set My Custom Checkbox
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_My_Custom_Checkbox(bool Value, bool Apply, bool* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set My Custom Checkbox");

	UBP_GameSettings_C_Set_My_Custom_Checkbox_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Checkbox
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_My_Custom_Checkbox(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify My Custom Checkbox");

	UBP_GameSettings_C_Modify_My_Custom_Checkbox_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify All MyCustom Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_All_MyCustom_Settings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify All MyCustom Settings");

	UBP_GameSettings_C_Modify_All_MyCustom_Settings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Resolution Scale
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Resolution_Scale(int* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Resolution Scale");

	UBP_GameSettings_C_Get_Resolution_Scale_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Resolution Scale
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Resolution_Scale(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Resolution Scale");

	UBP_GameSettings_C_Set_Resolution_Scale_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Resolution Scale
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Resolution_Scale(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Resolution Scale");

	UBP_GameSettings_C_Modify_Resolution_Scale_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Prepeare Previus Settings State
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Prepeare_Previus_Settings_State()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Prepeare Previus Settings State");

	UBP_GameSettings_C_Prepeare_Previus_Settings_State_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Save All Settings
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Save_All_Settings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Save All Settings");

	UBP_GameSettings_C_Save_All_Settings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify All Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_All_Settings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify All Settings");

	UBP_GameSettings_C_Modify_All_Settings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify All Audio Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_All_Audio_Settings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify All Audio Settings");

	UBP_GameSettings_C_Modify_All_Audio_Settings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify All Look Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_All_Look_Settings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify All Look Settings");

	UBP_GameSettings_C_Modify_All_Look_Settings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined Y
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Input_Axis_Y                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          World_Delta                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Vertical_Y                     (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Sensitivity_Combined_Y(float Input_Axis_Y, float World_Delta, float* Vertical_Y)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Sensitivity Combined Y");

	UBP_GameSettings_C_Get_Look_Sensitivity_Combined_Y_Params params;
	params.Input_Axis_Y = Input_Axis_Y;
	params.World_Delta = World_Delta;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Vertical_Y != nullptr)
		*Vertical_Y = params.Vertical_Y;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume Ambient
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_Ambient(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume Ambient");

	UBP_GameSettings_C_Get_Volume_Ambient_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Ambient
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_Ambient(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Ambient");

	UBP_GameSettings_C_Get_Audio_Multiplier_Ambient_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Ambient
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_Ambient(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Ambient");

	UBP_GameSettings_C_Set_Audio_Multiplier_Ambient_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Ambient
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_Ambient(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Ambient");

	UBP_GameSettings_C_Modify_Audio_Multiplier_Ambient_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume Effect
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_Effect(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume Effect");

	UBP_GameSettings_C_Get_Volume_Effect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Effect
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_Effect(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Effect");

	UBP_GameSettings_C_Get_Audio_Multiplier_Effect_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Effect
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_Effect(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Effect");

	UBP_GameSettings_C_Set_Audio_Multiplier_Effect_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Effect
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_Effect(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Effect");

	UBP_GameSettings_C_Modify_Audio_Multiplier_Effect_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume Voice
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_Voice(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume Voice");

	UBP_GameSettings_C_Get_Volume_Voice_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Voice
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_Voice(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Voice");

	UBP_GameSettings_C_Get_Audio_Multiplier_Voice_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Voice
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_Voice(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Voice");

	UBP_GameSettings_C_Set_Audio_Multiplier_Voice_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Voice
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_Voice(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Voice");

	UBP_GameSettings_C_Modify_Audio_Multiplier_Voice_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume Music
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_Music(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume Music");

	UBP_GameSettings_C_Get_Volume_Music_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Music
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_Music(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Music");

	UBP_GameSettings_C_Get_Audio_Multiplier_Music_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Music
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_Music(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Music");

	UBP_GameSettings_C_Set_Audio_Multiplier_Music_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Music
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_Music(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Music");

	UBP_GameSettings_C_Modify_Audio_Multiplier_Music_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Volume Master
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Volume_Master(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Volume Master");

	UBP_GameSettings_C_Get_Volume_Master_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Add Volume Control
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UAudioComponent*         Audio_Emittor                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// TEnumAsByte<EAudioType>        Audio_Channel                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Add_Volume_Control(class UAudioComponent* Audio_Emittor, TEnumAsByte<EAudioType> Audio_Channel)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Add Volume Control");

	UBP_GameSettings_C_Add_Volume_Control_Params params;
	params.Audio_Emittor = Audio_Emittor;
	params.Audio_Channel = Audio_Channel;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Apply Audio Settings
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EAudioType>        Audio_Channel                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Apply_Audio_Settings(TEnumAsByte<EAudioType> Audio_Channel)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Apply Audio Settings");

	UBP_GameSettings_C_Apply_Audio_Settings_Params params;
	params.Audio_Channel = Audio_Channel;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Master
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Audio_Multiplier_Master(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Audio Multiplier Master");

	UBP_GameSettings_C_Get_Audio_Multiplier_Master_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Master
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Audio_Multiplier_Master(float Set_Value, bool Apply)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Audio Multiplier Master");

	UBP_GameSettings_C_Set_Audio_Multiplier_Master_Params params;
	params.Set_Value = Set_Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Master
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Audio_Multiplier_Master(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Audio Multiplier Master");

	UBP_GameSettings_C_Modify_Audio_Multiplier_Master_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Invert
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Vertical_Invert(bool* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Invert");

	UBP_GameSettings_C_Get_Look_Vertical_Invert_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Invert
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Look_Vertical_Invert(bool Set_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Invert");

	UBP_GameSettings_C_Set_Look_Vertical_Invert_Params params;
	params.Set_Value = Set_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Invert
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Look_Vertical_Invert(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Invert");

	UBP_GameSettings_C_Modify_Look_Vertical_Invert_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Invert
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Horizontal_Invert(bool* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Invert");

	UBP_GameSettings_C_Get_Look_Horizontal_Invert_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Invert
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Look_Horizontal_Invert(bool Set_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Invert");

	UBP_GameSettings_C_Set_Look_Horizontal_Invert_Params params;
	params.Set_Value = Set_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Invert
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Look_Horizontal_Invert(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Invert");

	UBP_GameSettings_C_Modify_Look_Horizontal_Invert_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Sensitivity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Vertical_Sensitivity(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Vertical Sensitivity");

	UBP_GameSettings_C_Get_Look_Vertical_Sensitivity_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Sensitivity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Look_Vertical_Sensitivity(float Set_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Look Vertical Sensitivity");

	UBP_GameSettings_C_Set_Look_Vertical_Sensitivity_Params params;
	params.Set_Value = Set_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Sensitivity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Look_Vertical_Sensitivity(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Look Vertical Sensitivity");

	UBP_GameSettings_C_Modify_Look_Vertical_Sensitivity_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Sensitivity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Look_Horizontal_Sensitivity(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Look Horizontal Sensitivity");

	UBP_GameSettings_C_Get_Look_Horizontal_Sensitivity_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Sensitivity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Set_Value                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Look_Horizontal_Sensitivity(float Set_Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Look Horizontal Sensitivity");

	UBP_GameSettings_C_Set_Look_Horizontal_Sensitivity_Params params;
	params.Set_Value = Set_Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Sensitivity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Look_Horizontal_Sensitivity(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Look Horizontal Sensitivity");

	UBP_GameSettings_C_Modify_Look_Horizontal_Sensitivity_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Apply Screen Settings
// (Private, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_GameSettings_C::Apply_Screen_Settings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Apply Screen Settings");

	UBP_GameSettings_C_Apply_Screen_Settings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Screen Mode
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Screen_Mode(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Screen Mode");

	UBP_GameSettings_C_Modify_Screen_Mode_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Screen Mode
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// TEnumAsByte<EWindowMode>       Screen_Mode                    (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Command                        (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Screen_Mode(TEnumAsByte<EWindowMode>* Screen_Mode, struct FString* Command)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Screen Mode");

	UBP_GameSettings_C_Get_Screen_Mode_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Screen_Mode != nullptr)
		*Screen_Mode = params.Screen_Mode;
	if (Command != nullptr)
		*Command = params.Command;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Screen Resolution
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FSVideoResolution       Resolution                     (Parm, OutParm)

void UBP_GameSettings_C::Get_Screen_Resolution(struct FSVideoResolution* Resolution)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Screen Resolution");

	UBP_GameSettings_C_Get_Screen_Resolution_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Resolution != nullptr)
		*Resolution = params.Resolution;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Screen Mode
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EWindowMode>       Screen_Mode                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// TEnumAsByte<EWindowMode>       Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Screen_Mode(TEnumAsByte<EWindowMode> Screen_Mode, bool Apply, TEnumAsByte<EWindowMode>* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Screen Mode");

	UBP_GameSettings_C_Set_Screen_Mode_Params params;
	params.Screen_Mode = Screen_Mode;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Screen Resolution
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSVideoResolution       Resolution                     (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FSVideoResolution       Result                         (Parm, OutParm)

void UBP_GameSettings_C::Set_Screen_Resolution(const struct FSVideoResolution& Resolution, bool Apply, struct FSVideoResolution* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Screen Resolution");

	UBP_GameSettings_C_Set_Screen_Resolution_Params params;
	params.Resolution = Resolution;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Screen Resolution
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Screen_Resolution(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Screen Resolution");

	UBP_GameSettings_C_Modify_Screen_Resolution_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Motion Blur Strength
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Motion_Blur_Strength(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Motion Blur Strength");

	UBP_GameSettings_C_Get_Motion_Blur_Strength_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Motion Blur Strength
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Motion_Blur_Strength(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Motion Blur Strength");

	UBP_GameSettings_C_Set_Motion_Blur_Strength_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Motion Blur Strength
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Motion_Blur_Strength(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Motion Blur Strength");

	UBP_GameSettings_C_Modify_Motion_Blur_Strength_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Gain Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Gain_Intensity(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Gain Intensity");

	UBP_GameSettings_C_Get_Gain_Intensity_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Gain Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Gain_Intensity(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Gain Intensity");

	UBP_GameSettings_C_Set_Gain_Intensity_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Gain Intensity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Gain_Intensity(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Gain Intensity");

	UBP_GameSettings_C_Modify_Gain_Intensity_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Gamma Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Gamma_Intensity(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Gamma Intensity");

	UBP_GameSettings_C_Get_Gamma_Intensity_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Gamma Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Gamma_Intensity(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Gamma Intensity");

	UBP_GameSettings_C_Set_Gamma_Intensity_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Gamma Intensity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Gamma_Intensity(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Gamma Intensity");

	UBP_GameSettings_C_Modify_Gamma_Intensity_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Bloom Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Bloom_Intensity(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Bloom Intensity");

	UBP_GameSettings_C_Get_Bloom_Intensity_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Bloom Intensity
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Bloom_Intensity(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Bloom Intensity");

	UBP_GameSettings_C_Set_Bloom_Intensity_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Bloom Intensity
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Bloom_Intensity(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Bloom Intensity");

	UBP_GameSettings_C_Modify_Bloom_Intensity_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Vsync
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// bool                           Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Vsync(bool* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Vsync");

	UBP_GameSettings_C_Get_Vsync_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Vsync
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Vsync(bool Value, bool Apply, bool* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Vsync");

	UBP_GameSettings_C_Set_Vsync_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Vsync
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Vsync(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Vsync");

	UBP_GameSettings_C_Modify_Vsync_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Remove Field Of View Control From Camera
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UCameraComponent*        Camera                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UBP_GameSettings_C::Remove_Field_Of_View_Control_From_Camera(class UCameraComponent* Camera)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Remove Field Of View Control From Camera");

	UBP_GameSettings_C_Remove_Field_Of_View_Control_From_Camera_Params params;
	params.Camera = Camera;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Add Field Of View Control To Camera
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UCameraComponent*        Camera                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UBP_GameSettings_C::Add_Field_Of_View_Control_To_Camera(class UCameraComponent* Camera)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Add Field Of View Control To Camera");

	UBP_GameSettings_C_Add_Field_Of_View_Control_To_Camera_Params params;
	params.Camera = Camera;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Field Of View
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_Field_Of_View(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Field Of View");

	UBP_GameSettings_C_Get_Field_Of_View_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Field Of View
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Field_Of_View(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Field Of View");

	UBP_GameSettings_C_Set_Field_Of_View_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Field Of View
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Field_Of_View(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Field Of View");

	UBP_GameSettings_C_Modify_Field_Of_View_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get View Distance
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// float                          Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Get_View_Distance(float* Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get View Distance");

	UBP_GameSettings_C_Get_View_Distance_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
}


// Function BP_GameSettings.BP_GameSettings_C.Set View Distance
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_View_Distance(float Value, bool Apply, float* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set View Distance");

	UBP_GameSettings_C_Set_View_Distance_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify View Distance
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_View_Distance(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify View Distance");

	UBP_GameSettings_C_Modify_View_Distance_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Effect Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Effect_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Effect Quality");

	UBP_GameSettings_C_Get_Effect_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Effect Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Effect_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Effect Quality");

	UBP_GameSettings_C_Set_Effect_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Effect Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Effect_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Effect Quality");

	UBP_GameSettings_C_Modify_Effect_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Texture Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Texture_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Texture Quality");

	UBP_GameSettings_C_Get_Texture_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Texture Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Texture_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Texture Quality");

	UBP_GameSettings_C_Set_Texture_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Texture Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Texture_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Texture Quality");

	UBP_GameSettings_C_Modify_Texture_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Shadow Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Shadow_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Shadow Quality");

	UBP_GameSettings_C_Get_Shadow_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Shadow Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Shadow_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Shadow Quality");

	UBP_GameSettings_C_Set_Shadow_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Shadow Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Shadow_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Shadow Quality");

	UBP_GameSettings_C_Modify_Shadow_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify All Video Settings
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_All_Video_Settings(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify All Video Settings");

	UBP_GameSettings_C_Modify_All_Video_Settings_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Anti Aliasing Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Anti_Aliasing_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Anti Aliasing Quality");

	UBP_GameSettings_C_Get_Anti_Aliasing_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Anti Aliasing Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Anti_Aliasing_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Anti Aliasing Quality");

	UBP_GameSettings_C_Set_Anti_Aliasing_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Anti Aliasing Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Anti_Aliasing_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Anti Aliasing Quality");

	UBP_GameSettings_C_Modify_Anti_Aliasing_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Text Format Quality Level
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Quality_Level                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Text_Format_Quality_Level(int Quality_Level, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Text Format Quality Level");

	UBP_GameSettings_C_Get_Text_Format_Quality_Level_Params params;
	params.Quality_Level = Quality_Level;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Get Post Process Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// int                            Value                          (Parm, OutParm, ZeroConstructor, IsPlainOldData)
// struct FString                 Formatted                      (Parm, OutParm, ZeroConstructor)

void UBP_GameSettings_C::Get_Post_Process_Quality(int* Value, struct FString* Formatted)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Get Post Process Quality");

	UBP_GameSettings_C_Get_Post_Process_Quality_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Value != nullptr)
		*Value = params.Value;
	if (Formatted != nullptr)
		*Formatted = params.Formatted;
}


// Function BP_GameSettings.BP_GameSettings_C.Set Post Process Quality
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// bool                           Apply                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// int                            Result                         (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Set_Post_Process_Quality(int Value, bool Apply, int* Result)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Set Post Process Quality");

	UBP_GameSettings_C_Set_Post_Process_Quality_Params params;
	params.Value = Value;
	params.Apply = Apply;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Result != nullptr)
		*Result = params.Result;
}


// Function BP_GameSettings.BP_GameSettings_C.Modify Post Process Quality
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// TEnumAsByte<EModifySetting>    Modify                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Modify_Post_Process_Quality(TEnumAsByte<EModifySetting> Modify)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Modify Post Process Quality");

	UBP_GameSettings_C_Modify_Post_Process_Quality_Params params;
	params.Modify = Modify;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_GameSettings.BP_GameSettings_C.Update Audio Emittor
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSAudioUpdateStruct     Emittor                        (BlueprintVisible, BlueprintReadOnly, Parm)
// bool                           Is_Valid                       (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Update_Audio_Emittor(const struct FSAudioUpdateStruct& Emittor, bool* Is_Valid)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Update Audio Emittor");

	UBP_GameSettings_C_Update_Audio_Emittor_Params params;
	params.Emittor = Emittor;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Is_Valid != nullptr)
		*Is_Valid = params.Is_Valid;
}


// Function BP_GameSettings.BP_GameSettings_C.Create Clone
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UBP_GameSettings_C*      Cloned_Game_Settings           (Parm, OutParm, ZeroConstructor, IsPlainOldData)

void UBP_GameSettings_C::Create_Clone(class UBP_GameSettings_C** Cloned_Game_Settings)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_GameSettings.BP_GameSettings_C.Create Clone");

	UBP_GameSettings_C_Create_Clone_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Cloned_Game_Settings != nullptr)
		*Cloned_Game_Settings = params.Cloned_Game_Settings;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
