#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Loadout.WBP_Loadout_C.UpdateWeaponSkins
struct UWBP_Loadout_C_UpdateWeaponSkins_Params
{
};

// Function WBP_Loadout.WBP_Loadout_C.RefreshLoadout
struct UWBP_Loadout_C_RefreshLoadout_Params
{
};

// Function WBP_Loadout.WBP_Loadout_C.UpdateArmorIcons
struct UWBP_Loadout_C_UpdateArmorIcons_Params
{
};

// Function WBP_Loadout.WBP_Loadout_C.Construct
struct UWBP_Loadout_C_Construct_Params
{
};

// Function WBP_Loadout.WBP_Loadout_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_Loadout_C_BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Loadout.WBP_Loadout_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_Loadout_C_BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Loadout.WBP_Loadout_C.ExecuteUbergraph_WBP_Loadout
struct UWBP_Loadout_C_ExecuteUbergraph_WBP_Loadout_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Loadout.WBP_Loadout_C.ClearActionsMenu__DelegateSignature
struct UWBP_Loadout_C_ClearActionsMenu__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
