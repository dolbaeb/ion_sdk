#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BPI_RegisterKeyBinding.BPI_RegisterKeyBinding_C
// 0x0000 (0x0030 - 0x0030)
class UBPI_RegisterKeyBinding_C : public UInterface
{
public:

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BPI_RegisterKeyBinding.BPI_RegisterKeyBinding_C");
		return ptr;
	}


	void Key_Pressed(const struct FSKeyInput& New_Keybinding, bool* _);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
