#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_IonLegArmor.BP_IonLegArmor_C.UserConstructionScript
struct ABP_IonLegArmor_C_UserConstructionScript_Params
{
};

// Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_AttachWearable
struct ABP_IonLegArmor_C_BPEvent_AttachWearable_Params
{
};

// Function BP_IonLegArmor.BP_IonLegArmor_C.BPEvent_DetachWearable
struct ABP_IonLegArmor_C_BPEvent_DetachWearable_Params
{
};

// Function BP_IonLegArmor.BP_IonLegArmor_C.ExecuteUbergraph_BP_IonLegArmor
struct ABP_IonLegArmor_C_ExecuteUbergraph_BP_IonLegArmor_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
