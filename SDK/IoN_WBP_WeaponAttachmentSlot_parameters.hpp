#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Get_AttachmentFrame_ColorAndOpacity_1
struct UWBP_WeaponAttachmentSlot_C_Get_AttachmentFrame_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnDrop
struct UWBP_WeaponAttachmentSlot_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.GetCurrentItem
struct UWBP_WeaponAttachmentSlot_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.AcceptsDropForItem
struct UWBP_WeaponAttachmentSlot_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.InitAttachmentSlot
struct UWBP_WeaponAttachmentSlot_C_InitAttachmentSlot_Params
{
	TEnumAsByte<EAttachmentSlot>                       AttachmentSlot;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	class AIONFirearm*                                 Firearm;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.OnItemChanged
struct UWBP_WeaponAttachmentSlot_C_OnItemChanged_Params
{
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Tick
struct UWBP_WeaponAttachmentSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.Construct
struct UWBP_WeaponAttachmentSlot_C_Construct_Params
{
};

// Function WBP_WeaponAttachmentSlot.WBP_WeaponAttachmentSlot_C.ExecuteUbergraph_WBP_WeaponAttachmentSlot
struct UWBP_WeaponAttachmentSlot_C_ExecuteUbergraph_WBP_WeaponAttachmentSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
