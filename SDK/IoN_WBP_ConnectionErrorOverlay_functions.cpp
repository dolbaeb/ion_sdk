// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ConnectionErrorOverlay_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature");

	UWBP_ConnectionErrorOverlay_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.ExecuteUbergraph_WBP_ConnectionErrorOverlay
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ConnectionErrorOverlay_C::ExecuteUbergraph_WBP_ConnectionErrorOverlay(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.ExecuteUbergraph_WBP_ConnectionErrorOverlay");

	UWBP_ConnectionErrorOverlay_C_ExecuteUbergraph_WBP_ConnectionErrorOverlay_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.OnRetryClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ConnectionErrorOverlay_C::OnRetryClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ConnectionErrorOverlay.WBP_ConnectionErrorOverlay_C.OnRetryClicked__DelegateSignature");

	UWBP_ConnectionErrorOverlay_C_OnRetryClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
