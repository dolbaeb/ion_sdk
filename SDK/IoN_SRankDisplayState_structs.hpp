#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SRankDisplayState.SRankDisplayState
// 0x003D
struct FSRankDisplayState
{
	struct FRank                                       Rank_2_AB610B4C46089477D5C797B1530BAFDE;                  // 0x0000(0x0030) (Edit, BlueprintVisible)
	int                                                RankingPoints_31_F220A77749B494664BF00D9562BD588B;        // 0x0030(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                RankingPointsChange_32_625839BC44D4172CF53B5DA7E72CA776;  // 0x0034(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                PointsToAnimate_36_6E780DC948995C28AAE13AAD1B56AAA4;      // 0x0038(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bChampion_27_B7B15D8F4AFDFB0277085EA71A07507B;            // 0x003C(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
