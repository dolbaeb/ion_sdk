#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.SetFocus
struct UWB_ClassicMappingItem_C_SetFocus_Params
{
	bool                                               PrimaryKey;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseWheel
struct UWB_ClassicMappingItem_C_OnMouseWheel_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.RefreshInputMappings
struct UWB_ClassicMappingItem_C_RefreshInputMappings_Params
{
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_GamepadKeyButton_Visibility_1
struct UWB_ClassicMappingItem_C_Get_GamepadKeyButton_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyText_Visibility_1
struct UWB_ClassicMappingItem_C_Get_KeyText_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseMove
struct UWB_ClassicMappingItem_C_OnMouseMove_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnAnalogValueChanged
struct UWB_ClassicMappingItem_C_OnAnalogValueChanged_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FAnalogInputEvent*                          InAnalogInputEvent;                                       // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnInputKeyDown
struct UWB_ClassicMappingItem_C_OnInputKeyDown_Params
{
	struct FKey                                        Key;                                                      // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseButtonDown
struct UWB_ClassicMappingItem_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_SecondaryButton_bIsEnabled_1
struct UWB_ClassicMappingItem_C_Get_SecondaryButton_bIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Get_KeyButton_bIsEnabled_1
struct UWB_ClassicMappingItem_C_Get_KeyButton_bIsEnabled_1_Params
{
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnKeyDown
struct UWB_ClassicMappingItem_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.Construct
struct UWB_ClassicMappingItem_C_Construct_Params
{
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature
struct UWB_ClassicMappingItem_C_BndEvt__KeyButton_K2Node_ComponentBoundEvent_216_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature
struct UWB_ClassicMappingItem_C_BndEvt__SecondaryButton_K2Node_ComponentBoundEvent_243_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnFocusLost
struct UWB_ClassicMappingItem_C_OnFocusLost_Params
{
	struct FFocusEvent*                                InFocusEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.OnMouseLeave
struct UWB_ClassicMappingItem_C_OnMouseLeave_Params
{
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.CancelRebind
struct UWB_ClassicMappingItem_C_CancelRebind_Params
{
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.PreConstruct
struct UWB_ClassicMappingItem_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WB_ClassicMappingItem.WB_ClassicMappingItem_C.ExecuteUbergraph_WB_ClassicMappingItem
struct UWB_ClassicMappingItem_C_ExecuteUbergraph_WB_ClassicMappingItem_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
