// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnGetMenuContent_1
// (Public, HasOutParms, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_LeaderboardEntry_C::OnGetMenuContent_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnGetMenuContent_1");

	UWBP_LeaderboardEntry_C_OnGetMenuContent_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.InitLeaderboardEntry
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 PlayerName                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// struct FString                 GameSparksID                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// int                            Position                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FText                   Score                          (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_LeaderboardEntry_C::InitLeaderboardEntry(const struct FString& PlayerName, const struct FString& GameSparksID, int Position, const struct FText& Score)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.InitLeaderboardEntry");

	UWBP_LeaderboardEntry_C_InitLeaderboardEntry_Params params;
	params.PlayerName = PlayerName;
	params.GameSparksID = GameSparksID;
	params.Position = Position;
	params.Score = Score;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_LeaderboardEntry_C::BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature");

	UWBP_LeaderboardEntry_C_BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnPopupClicked
// (BlueprintCallable, BlueprintEvent)

void UWBP_LeaderboardEntry_C::OnPopupClicked()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnPopupClicked");

	UWBP_LeaderboardEntry_C_OnPopupClicked_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.ExecuteUbergraph_WBP_LeaderboardEntry
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_LeaderboardEntry_C::ExecuteUbergraph_WBP_LeaderboardEntry(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.ExecuteUbergraph_WBP_LeaderboardEntry");

	UWBP_LeaderboardEntry_C_ExecuteUbergraph_WBP_LeaderboardEntry_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
