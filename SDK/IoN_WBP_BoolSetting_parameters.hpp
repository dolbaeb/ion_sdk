#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_BoolSetting.WBP_BoolSetting_C.Get_CheckBox_CheckedState_1
struct UWBP_BoolSetting_C_Get_CheckBox_CheckedState_1_Params
{
	ECheckBoxState                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_BoolSetting.WBP_BoolSetting_C.BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature
struct UWBP_BoolSetting_C_BndEvt__InvertCheckBox_K2Node_ComponentBoundEvent_30_OnCheckBoxComponentStateChanged__DelegateSignature_Params
{
	bool                                               bIsChecked;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_BoolSetting.WBP_BoolSetting_C.PreConstruct
struct UWBP_BoolSetting_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_BoolSetting.WBP_BoolSetting_C.Construct
struct UWBP_BoolSetting_C_Construct_Params
{
};

// Function WBP_BoolSetting.WBP_BoolSetting_C.ExecuteUbergraph_WBP_BoolSetting
struct UWBP_BoolSetting_C_ExecuteUbergraph_WBP_BoolSetting_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
