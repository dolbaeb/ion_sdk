// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_AK47.ABP_AK47_C.ExecuteUbergraph_ABP_AK47
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_AK47_C::ExecuteUbergraph_ABP_AK47(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_AK47.ABP_AK47_C.ExecuteUbergraph_ABP_AK47");

	UABP_AK47_C_ExecuteUbergraph_ABP_AK47_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
