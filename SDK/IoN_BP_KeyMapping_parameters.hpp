#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function BP_KeyMapping.BP_KeyMapping_C.Revert To Default KeyMapping
struct UBP_KeyMapping_C_Revert_To_Default_KeyMapping_Params
{
};

// Function BP_KeyMapping.BP_KeyMapping_C.Load Key Mapping
struct UBP_KeyMapping_C_Load_Key_Mapping_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FString                                     Action_Name;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     Category;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function BP_KeyMapping.BP_KeyMapping_C.Save Key Mapping
struct UBP_KeyMapping_C_Save_Key_Mapping_Params
{
	class UBP_GameSettings_C*                          Game_Settings;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FSKeyActionSave                             KeySave;                                                  // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function BP_KeyMapping.BP_KeyMapping_C.Key Mapping Current State
struct UBP_KeyMapping_C_Key_Mapping_Current_State_Params
{
	class APlayerController*                           Player_Controller;                                        // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              Mapping_Value;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Is_Active;                                                // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Pressed;                                             // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
	bool                                               Just_Released;                                            // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

// Function BP_KeyMapping.BP_KeyMapping_C.Init Key Mapping
struct UBP_KeyMapping_C_Init_Key_Mapping_Params
{
	struct FSKeyMapping                                Key_Mapping;                                              // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UBP_KeyMapping_C*                            Mapping;                                                  // (Parm, OutParm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
