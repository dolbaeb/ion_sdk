#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_GrenadesLabel_Text_1
struct UWBP_Inventory_C_Get_TextBlock_GrenadesLabel_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.Get_TextBlock_BoostersLabel_Text_1
struct UWBP_Inventory_C_Get_TextBlock_BoostersLabel_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDoubleClick
struct UWBP_Inventory_C_OnMouseButtonDoubleClick_Params
{
	struct FGeometry*                                  InMyGeometry;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              InMouseEvent;                                             // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.OnPaint
struct UWBP_Inventory_C_OnPaint_Params
{
	struct FPaintContext                               Context;                                                  // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
};

// Function WBP_Inventory.WBP_Inventory_C.GetText_1
struct UWBP_Inventory_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.OnKeyUp
struct UWBP_Inventory_C_OnKeyUp_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.Get_AmmoCountText_Text_1
struct UWBP_Inventory_C_Get_AmmoCountText_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.UpdateExtraCapacitySlotVisibility
struct UWBP_Inventory_C_UpdateExtraCapacitySlotVisibility_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.OnMouseButtonDown
struct UWBP_Inventory_C_OnMouseButtonDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              MouseEvent;                                               // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.OnKeyDown
struct UWBP_Inventory_C_OnKeyDown_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FKeyEvent*                                  InKeyEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_Inventory.WBP_Inventory_C.OnDrop
struct UWBP_Inventory_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_Inventory.WBP_Inventory_C.Tick
struct UWBP_Inventory_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Inventory.WBP_Inventory_C.CloseInventory
struct UWBP_Inventory_C_CloseInventory_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.ShowInventory
struct UWBP_Inventory_C_ShowInventory_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.HideInventory
struct UWBP_Inventory_C_HideInventory_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.ShowInventoryBar
struct UWBP_Inventory_C_ShowInventoryBar_Params
{
	bool*                                              bInventoryFull;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_Inventory.WBP_Inventory_C.OnCollapsedInventory
struct UWBP_Inventory_C_OnCollapsedInventory_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.Init Attachment Nodes
struct UWBP_Inventory_C_Init_Attachment_Nodes_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.Clear Attachment Nodes
struct UWBP_Inventory_C_Clear_Attachment_Nodes_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.OnInventoryOpened
struct UWBP_Inventory_C_OnInventoryOpened_Params
{
};

// Function WBP_Inventory.WBP_Inventory_C.ExecuteUbergraph_WBP_Inventory
struct UWBP_Inventory_C_ExecuteUbergraph_WBP_Inventory_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
