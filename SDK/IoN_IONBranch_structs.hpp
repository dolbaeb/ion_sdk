#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// Enum IONBranch.EInventoryType
enum class EInventoryType : uint8_t
{
	EInventoryType__Ammo           = 0,
	EInventoryType__Attachments    = 1,
	EInventoryType__Firearms       = 2,
	EInventoryType__Boosters       = 3,
	EInventoryType__Grenades       = 4,
	EInventoryType__Wearables      = 5,
	EInventoryType__Max            = 6
};


// Enum IONBranch.EWeaponTask
enum class EWeaponTask : uint8_t
{
	EWeaponTask__None              = 0,
	EWeaponTask__Equip             = 1,
	EWeaponTask__Unequip           = 2,
	EWeaponTask__Fire              = 3,
	EWeaponTask__DryFire           = 4,
	EWeaponTask__Reload            = 5,
	EWeaponTask__ReloadEmpty       = 6,
	EWeaponTask__ReloadChamber     = 7,
	EWeaponTask__SwitchFireMode    = 8,
	EWeaponTask__ReloadSingle      = 9,
	EWeaponTask__ReloadEnd         = 10,
	EWeaponTask__ReloadCancel      = 11,
	EWeaponTask__Activate          = 12,
	EWeaponTask__Slash             = 13,
	EWeaponTask__Throw             = 14,
	EWeaponTask__Recharge          = 15,
	EWeaponTask__Cook              = 16,
	EWeaponTask__Hold              = 17,
	EWeaponTask__AltCook           = 18,
	EWeaponTask__AltHold           = 19,
	EWeaponTask__AltThrow          = 20,
	EWeaponTask__EWeaponTask_MAX   = 21
};


// Enum IONBranch.EFirearmType
enum class EFirearmType : uint8_t
{
	EFirearmType__AssaultRifle     = 0,
	EFirearmType__SniperRifle      = 1,
	EFirearmType__SMG              = 2,
	EFirearmType__Shotgun          = 3,
	EFirearmType__Pistol           = 4,
	EFirearmType__Launcher         = 5,
	EFirearmType__Melee            = 6,
	EFirearmType__MAX              = 7
};


// Enum IONBranch.ECharacterStance
enum class ECharacterStance : uint8_t
{
	ECharacterStance__Stand        = 0,
	ECharacterStance__Crouch       = 1,
	ECharacterStance__Prone        = 2,
	ECharacterStance__Down         = 3,
	ECharacterStance__ECharacterStance_MAX = 4
};


// Enum IONBranch.EItemStatusIndicator
enum class EItemStatusIndicator : uint8_t
{
	EItemStatusIndicator__Addition = 0,
	EItemStatusIndicator__Improvement = 1,
	EItemStatusIndicator__Swap     = 2,
	EItemStatusIndicator__Degradation = 3,
	EItemStatusIndicator__Neutral  = 4,
	EItemStatusIndicator__EItemStatusIndicator_MAX = 5
};


// Enum IONBranch.ECharacterSignificanceLevel
enum class ECharacterSignificanceLevel : uint8_t
{
	ECharacterSignificanceLevel__None = 0,
	ECharacterSignificanceLevel__Low = 1,
	ECharacterSignificanceLevel__Medium = 2,
	ECharacterSignificanceLevel__High = 3,
	ECharacterSignificanceLevel__Critical = 4,
	ECharacterSignificanceLevel__Num = 5,
	ECharacterSignificanceLevel__ECharacterSignificanceLevel_MAX = 6
};


// Enum IONBranch.EScoreEvent
enum class EScoreEvent : uint8_t
{
	EScoreEvent__Kill              = 0,
	EScoreEvent__Headshot          = 1,
	EScoreEvent__Knockout          = 2,
	EScoreEvent__MidAirKill        = 3,
	EScoreEvent__KnifeKill         = 4,
	EScoreEvent__DoubleKill        = 5,
	EScoreEvent__TripleKill        = 6,
	EScoreEvent__QuadKill          = 7,
	EScoreEvent__LongShot100       = 8,
	EScoreEvent__LongShot200       = 9,
	EScoreEvent__LongShot300       = 10,
	EScoreEvent__LongShot500       = 11,
	EScoreEvent__GrenadeKill       = 12,
	EScoreEvent__KillStreak        = 13,
	EScoreEvent__Damage            = 14,
	EScoreEvent__Placement         = 15,
	EScoreEvent__MAX               = 16
};


// Enum IONBranch.EIONPartyTypes
enum class EIONPartyTypes : uint8_t
{
	EIONPartyTypes__Solo           = 0,
	EIONPartyTypes__Duo            = 1,
	EIONPartyTypes__Squad          = 2,
	EIONPartyTypes__EIONPartyTypes_MAX = 3
};


// Enum IONBranch.EIONPartyRegion
enum class EIONPartyRegion : uint8_t
{
	EIONPartyRegion__None          = 0,
	EIONPartyRegion__NorthAmerica  = 1,
	EIONPartyRegion__Europe        = 2,
	EIONPartyRegion__SouthEastAsia = 3,
	EIONPartyRegion__Oceania       = 4,
	EIONPartyRegion__SouthAmerica  = 5,
	EIONPartyRegion__EIONPartyRegion_MAX = 6
};


// Enum IONBranch.EBattleRoyaleMatchState
enum class EBattleRoyaleMatchState : uint8_t
{
	EBattleRoyaleMatchState__PreQueue = 0,
	EBattleRoyaleMatchState__Countdown = 1,
	EBattleRoyaleMatchState__InProgress = 2,
	EBattleRoyaleMatchState__Ended = 3,
	EBattleRoyaleMatchState__EBattleRoyaleMatchState_MAX = 4
};


// Enum IONBranch.EBlueprintInviteStatus
enum class EBlueprintInviteStatus : uint8_t
{
	EBlueprintInviteStatus__Unknown = 0,
	EBlueprintInviteStatus__Accepted = 1,
	EBlueprintInviteStatus__PendingInbound = 2,
	EBlueprintInviteStatus__PendingOutbound = 3,
	EBlueprintInviteStatus__Blocked = 4,
	EBlueprintInviteStatus__EBlueprintInviteStatus_MAX = 5
};


// Enum IONBranch.EBarrelType
enum class EBarrelType : uint8_t
{
	EBarrelType__Silencer          = 0,
	EBarrelType__Compensator       = 1,
	EBarrelType__HeavyBarrel       = 2,
	EBarrelType__EBarrelType_MAX   = 3
};


// Enum IONBranch.EMaterialLocation
enum class EMaterialLocation : uint8_t
{
	EMaterialLocation__MaterialLocationLocation_Undefined = 0,
	EMaterialLocation__MaterialLocationLocation_Interior = 1,
	EMaterialLocation__MaterialLocationLocation_Exterior = 2,
	EMaterialLocation__MaterialLocationLocation_MAX = 3
};


// Enum IONBranch.EFirearmSoundType
enum class EFirearmSoundType : uint8_t
{
	EFirearmSoundType__DefaultSound = 0,
	EFirearmSoundType__KnifeSound  = 1,
	EFirearmSoundType__KnifeProjectiveSound = 2,
	EFirearmSoundType__SakoSound   = 3,
	EFirearmSoundType__EFirearmSoundType_MAX = 4
};


// Enum IONBranch.EMagazineType
enum class EMagazineType : uint8_t
{
	EMagazineType__None            = 0,
	EMagazineType__Box             = 1,
	EMagazineType__Tubular         = 2,
	EMagazineType__EMagazineType_MAX = 3
};


// Enum IONBranch.EFireMode
enum class EFireMode : uint8_t
{
	EFireMode__Semi                = 0,
	EFireMode__Burst               = 1,
	EFireMode__Auto                = 2,
	EFireMode__EFireMode_MAX       = 3
};


// Enum IONBranch.EAttachmentSlot
enum class EAttachmentSlot : uint8_t
{
	EAttachmentSlot__Barrel        = 0,
	EAttachmentSlot__Siderail      = 1,
	EAttachmentSlot__Sight         = 2,
	EAttachmentSlot__UnderBarrelRail = 3,
	EAttachmentSlot__ExtendedMagazine = 4,
	EAttachmentSlot__Max           = 5
};


// Enum IONBranch.EInputMappingType
enum class EInputMappingType : uint8_t
{
	EInputMappingType__Action      = 0,
	EInputMappingType__Axis        = 1,
	EInputMappingType__NegativeAxis = 2,
	EInputMappingType__EInputMappingType_MAX = 3
};


// Enum IONBranch.EInputKeyType
enum class EInputKeyType : uint8_t
{
	EInputKeyType__None            = 0,
	EInputKeyType__Primary         = 1,
	EInputKeyType__Secondary       = 2,
	EInputKeyType__EInputKeyType_MAX = 3
};


// Enum IONBranch.EInventoryActionType
enum class EInventoryActionType : uint8_t
{
	EInventoryActionType__Removed  = 0,
	EInventoryActionType__Added    = 1,
	EInventoryActionType__Swapped  = 2,
	EInventoryActionType__EInventoryActionType_MAX = 3
};


// Enum IONBranch.EKnifeMode
enum class EKnifeMode : uint8_t
{
	EKnifeMode__Melee              = 0,
	EKnifeMode__Throw              = 1,
	EKnifeMode__EKnifeMode_MAX     = 2
};


// Enum IONBranch.EIONVoiceActivity
enum class EIONVoiceActivity : uint8_t
{
	EIONVoiceActivity__Proximity   = 0,
	EIONVoiceActivity__Squad       = 1,
	EIONVoiceActivity__None        = 2,
	EIONVoiceActivity__EIONVoiceActivity_MAX = 3
};


// Enum IONBranch.EItemSkinRarity
enum class EItemSkinRarity : uint8_t
{
	EItemSkinRarity__Common        = 0,
	EItemSkinRarity__Uncommon      = 1,
	EItemSkinRarity__Rare          = 2,
	EItemSkinRarity__Epic          = 3,
	EItemSkinRarity__Limited       = 4,
	EItemSkinRarity__Promotional   = 5,
	EItemSkinRarity__Legendary     = 6,
	EItemSkinRarity__Special       = 7,
	EItemSkinRarity__Crate         = 8,
	EItemSkinRarity__EItemSkinRarity_MAX = 9
};


// Enum IONBranch.EIONPlayerLogEvent
enum class EIONPlayerLogEvent : uint8_t
{
	EIONPlayerLogEvent__None       = 0,
	EIONPlayerLogEvent__Login      = 1,
	EIONPlayerLogEvent__Logout     = 2,
	EIONPlayerLogEvent__Kick       = 3,
	EIONPlayerLogEvent__EIONPlayerLogEvent_MAX = 4
};


// Enum IONBranch.EPlayerCardMode
enum class EPlayerCardMode : uint8_t
{
	EPlayerCardMode__None          = 0,
	EPlayerCardMode__Minimal       = 1,
	EPlayerCardMode__Detailed      = 2,
	EPlayerCardMode__EPlayerCardMode_MAX = 3
};


// Enum IONBranch.EIONSpectateMode
enum class EIONSpectateMode : uint8_t
{
	EIONSpectateMode__Follow       = 0,
	EIONSpectateMode__FreeRoam     = 1,
	EIONSpectateMode__FirstPerson  = 2,
	EIONSpectateMode__EIONSpectateMode_MAX = 3
};


// Enum IONBranch.EIONAdminAccessLevels
enum class EIONAdminAccessLevels : uint8_t
{
	EIONAdminAccessLevels__Public  = 0,
	EIONAdminAccessLevels__Start   = 1,
	EIONAdminAccessLevels__Chat    = 2,
	EIONAdminAccessLevels__Cheat   = 3,
	EIONAdminAccessLevels__Mute    = 4,
	EIONAdminAccessLevels__Kick    = 5,
	EIONAdminAccessLevels__Config  = 6,
	EIONAdminAccessLevels__ObserverCam = 7,
	EIONAdminAccessLevels__Debug   = 8,
	EIONAdminAccessLevels__Demos   = 9,
	EIONAdminAccessLevels__Developer = 10,
	EIONAdminAccessLevels__QA      = 11,
	EIONAdminAccessLevels__ServerExec = 12,
	EIONAdminAccessLevels__EIONAdminAccessLevels_MAX = 13
};


// Enum IONBranch.EIONHUDView
enum class EIONHUDView : uint8_t
{
	EIONHUDView__None              = 0,
	EIONHUDView__AllPawns          = 1,
	EIONHUDView__SameTeam          = 2,
	EIONHUDView__OnlyEnemy         = 3,
	EIONHUDView__EIONHUDView_MAX   = 4
};


// Enum IONBranch.EIONCameraMode
enum class EIONCameraMode : uint8_t
{
	EIONCameraMode__FlySpeed       = 0,
	EIONCameraMode__FOV            = 1,
	EIONCameraMode__DOFFocalDistance = 2,
	EIONCameraMode__DOFBlurSize    = 3,
	EIONCameraMode__EIONCameraMode_MAX = 4
};


// Enum IONBranch.EScoreEventGroup
enum class EScoreEventGroup : uint8_t
{
	EScoreEventGroup__Kills        = 0,
	EScoreEventGroup__Damage       = 1,
	EScoreEventGroup__Survival     = 2,
	EScoreEventGroup__EScoreEventGroup_MAX = 3
};


// Enum IONBranch.ETaskConfirmationMode
enum class ETaskConfirmationMode : uint8_t
{
	ETaskConfirmationMode__None    = 0,
	ETaskConfirmationMode__Owner   = 1,
	ETaskConfirmationMode__Broadcast = 2,
	ETaskConfirmationMode__ETaskConfirmationMode_MAX = 3
};


// Enum IONBranch.ETaskRepMode
enum class ETaskRepMode : uint8_t
{
	ETaskRepMode__Local            = 0,
	ETaskRepMode__Authority        = 1,
	ETaskRepMode__AuthorityBroadcast = 2,
	ETaskRepMode__Predicted        = 3,
	ETaskRepMode__PredictedBroadcast = 4,
	ETaskRepMode__ETaskRepMode_MAX = 5
};


// Enum IONBranch.EWearableSlot
enum class EWearableSlot : uint8_t
{
	EWearableSlot__Helmet          = 0,
	EWearableSlot__Chest           = 1,
	EWearableSlot__Arms            = 2,
	EWearableSlot__Legs            = 3,
	EWearableSlot__Shield          = 4,
	EWearableSlot__Modifier        = 5,
	EWearableSlot__EWearableSlot_MAX = 6
};


// Enum IONBranch.EIONAudioFadeModel
enum class EIONAudioFadeModel : uint8_t
{
	EIONAudioFadeModel__InverseByDistance = 0,
	EIONAudioFadeModel__LinearByDistance = 1,
	EIONAudioFadeModel__ExponentialByDistance = 2,
	EIONAudioFadeModel__EIONAudioFadeModel_MAX = 3
};


// Enum IONBranch.EOnlinePlatformType
enum class EOnlinePlatformType : uint8_t
{
	EOnlinePlatformType__Editor    = 0,
	EOnlinePlatformType__Steam     = 1,
	EOnlinePlatformType__EOnlinePlatformType_MAX = 2
};


// Enum IONBranch.EPlasmaStageType
enum class EPlasmaStageType : uint8_t
{
	EPlasmaStageType__Normal       = 0,
	EPlasmaStageType__FinalZone    = 1,
	EPlasmaStageType__FinalConvergence = 2,
	EPlasmaStageType__EPlasmaStageType_MAX = 3
};


// Enum IONBranch.EPlasmaState
enum class EPlasmaState : uint8_t
{
	EPlasmaState__PreGameQueue     = 0,
	EPlasmaState__InitWaiting      = 1,
	EPlasmaState__Waiting          = 2,
	EPlasmaState__Shrinking        = 3,
	EPlasmaState__Finished         = 4,
	EPlasmaState__EPlasmaState_MAX = 5
};


// Enum IONBranch.ESteamAvatarSize
enum class ESteamAvatarSize : uint8_t
{
	ESteamAvatarSize__Small        = 0,
	ESteamAvatarSize__Medium       = 1,
	ESteamAvatarSize__Large        = 2,
	ESteamAvatarSize__ESteamAvatarSize_MAX = 3
};



//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// ScriptStruct IONBranch.ItemIdentifier
// 0x000C (0x0018 - 0x000C)
struct FItemIdentifier : public FFastArraySerializerItem
{
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
	struct FName                                       Identifier;                                               // 0x0010(0x0008) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.SpawnedItemArray
// 0x0018 (0x00C8 - 0x00B0)
struct FSpawnedItemArray : public FFastArraySerializer
{
	TArray<struct FItemIdentifier>                     Array;                                                    // 0x00B0(0x0010) (ZeroConstructor)
	class AIONGameState*                               OwningGameState;                                          // 0x00C0(0x0008) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.PlasmaStage
// 0x001C
struct FPlasmaStage
{
	float                                              FleeTime;                                                 // 0x0000(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              ShrinkTime;                                               // 0x0004(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	struct FVector                                     Location;                                                 // 0x0008(0x000C) (IsPlainOldData)
	float                                              Radius;                                                   // 0x0014(0x0004) (ZeroConstructor, IsPlainOldData)
	EPlasmaStageType                                   Type;                                                     // 0x0018(0x0001) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0019(0x0003) MISSED OFFSET
};

// ScriptStruct IONBranch.IONSteamInventoryItem
// 0x0010
struct FIONSteamInventoryItem
{
	unsigned char                                      UnknownData00[0x8];                                       // 0x0000(0x0008) MISSED OFFSET
	class UIONSteamItem*                               Item;                                                     // 0x0008(0x0008) (Edit, BlueprintVisible, ZeroConstructor, EditConst, IsPlainOldData)
};

// ScriptStruct IONBranch.LoadoutData
// 0x0160
struct FLoadoutData
{
	TMap<class UClass*, class UIONSteamItem*>          WeaponLoadout;                                            // 0x0000(0x0050) (Edit, BlueprintVisible, ZeroConstructor, EditConst)
	unsigned char                                      UnknownData00[0x50];                                      // 0x0050(0x0050) MISSED OFFSET
	TMap<class UClass*, class UIONSteamItem*>          WearableLoadout;                                          // 0x00A0(0x0050) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst)
	unsigned char                                      UnknownData01[0x50];                                      // 0x00F0(0x0050) MISSED OFFSET
	class UIONSteamItem*                               DropsuitLoadout;                                          // 0x0140(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      FaceIndex;                                                // 0x0148(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      SkinColorIndex;                                           // 0x0149(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, EditConst, IsPlainOldData)
	unsigned char                                      UnknownData02[0x6];                                       // 0x014A(0x0006) MISSED OFFSET
	struct FIONSteamInventoryItem                      LoadoutDropsuit;                                          // 0x0150(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, EditConst)
};

// ScriptStruct IONBranch.LeaderboardDataEntry
// 0x0030
struct FLeaderboardDataEntry
{
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0000(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     PlayerName;                                               // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     PlayerId;                                                 // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                PlayerRank;                                               // 0x0028(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x002C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.PlayerMatchResult
// 0x0168
struct FPlayerMatchResult
{
	unsigned char                                      UnknownData00[0x1];                                       // 0x0000(0x0001) MISSED OFFSET
	bool                                               bSquadFinished;                                           // 0x0001(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x6];                                       // 0x0002(0x0006) MISSED OFFSET
	struct FString                                     MatchId;                                                  // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	unsigned char                                      GameMode;                                                 // 0x0018(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0019(0x0003) MISSED OFFSET
	int                                                Place;                                                    // 0x001C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumParticipants;                                          // 0x0020(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                Kills;                                                    // 0x0024(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     KilledBy;                                                 // 0x0028(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     KilledByUsername;                                         // 0x0038(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     KillDamageCauser;                                         // 0x0048(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	float                                              DamageDealt;                                              // 0x0058(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                ShotsFired;                                               // 0x005C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                ShotsHit;                                                 // 0x0060(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                HeadshotsHit;                                             // 0x0064(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     FavoriteWeapon;                                           // 0x0068(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	float                                              TimeSurvived;                                             // 0x0078(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                MatchScore;                                               // 0x007C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                KillScore;                                                // 0x0080(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                DamageScore;                                              // 0x0084(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                PlacementScore;                                           // 0x0088(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x008C(0x0004) MISSED OFFSET
	struct FString                                     UserName;                                                 // 0x0090(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                SquadIndex;                                               // 0x00A0(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FLinearColor                                SquadColor;                                               // 0x00A4(0x0010) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	unsigned char                                      UnknownData04[0xB4];                                      // 0x00B4(0x00B4) MISSED OFFSET
};

// ScriptStruct IONBranch.MatchResults
// 0x0010
struct FMatchResults
{
	TArray<struct FPlayerMatchResult>                  PlayerResults;                                            // 0x0000(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.ProximityDebugInfo
// 0x0030
struct FProximityDebugInfo
{
	struct FString                                     Name;                                                     // 0x0000(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	float                                              Distance;                                                 // 0x0010(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0014(0x0004) MISSED OFFSET
	struct FString                                     ProximityKey;                                             // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                Volume;                                                   // 0x0028(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x002C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.CachedProxData
// 0x0024
struct FCachedProxData
{
	struct FVector                                     Position;                                                 // 0x0000(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	struct FVector                                     ForwardVector;                                            // 0x000C(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
	struct FVector                                     UpVector;                                                 // 0x0018(0x000C) (BlueprintVisible, BlueprintReadOnly, IsPlainOldData)
};

// ScriptStruct IONBranch.ScoreEvent
// 0x0008
struct FScoreEvent
{
	EScoreEvent                                        Type;                                                     // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0001(0x0003) MISSED OFFSET
	int                                                Points;                                                   // 0x0004(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.PlayerMatchHistory
// 0x00F0
struct FPlayerMatchHistory
{
	bool                                               bMatchFound;                                              // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	struct FString                                     ErrorMessage;                                             // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     MatchId;                                                  // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     PlayerId;                                                 // 0x0028(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     PlayerName;                                               // 0x0038(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	EIONPartyTypes                                     GameMode;                                                 // 0x0048(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0049(0x0007) MISSED OFFSET
	struct FDateTime                                   MatchDate;                                                // 0x0050(0x0008) (BlueprintVisible, BlueprintReadOnly)
	int                                                Place;                                                    // 0x0058(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumParticipants;                                          // 0x005C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                Kills;                                                    // 0x0060(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                GameDuration;                                             // 0x0064(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                MatchScore;                                               // 0x0068(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                KillScore;                                                // 0x006C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                DamageScore;                                              // 0x0070(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                PlacementScore;                                           // 0x0074(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              DamageDealt;                                              // 0x0078(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x007C(0x0004) MISSED OFFSET
	TArray<int>                                        KillEventCounts;                                          // 0x0080(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	TArray<struct FScoreEvent>                         ScoreEvents;                                              // 0x0090(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                CurrentLevel;                                             // 0x00A0(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                CurrentXP;                                                // 0x00A4(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	TArray<int>                                        LevelThresholds;                                          // 0x00A8(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                RankingPointsChange;                                      // 0x00B8(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                CurrentRank;                                              // 0x00BC(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                CurrentRankingPoints;                                     // 0x00C0(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NewRank;                                                  // 0x00C4(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NewRankingPoints;                                         // 0x00C8(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData03[0x4];                                       // 0x00CC(0x0004) MISSED OFFSET
	struct FString                                     KilledBy;                                                 // 0x00D0(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     DeathContext;                                             // 0x00E0(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.HitInfo
// 0x0020
struct FHitInfo
{
	class APlayerState*                                Target;                                                   // 0x0000(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class APlayerState*                                Instigator;                                               // 0x0008(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class AActor*                                      DamageCauser;                                             // 0x0010(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bHeadshot;                                                // 0x0018(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0019(0x0003) MISSED OFFSET
	int                                                DistanceInMeters;                                         // 0x001C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.BlueprintOnlineFriend
// 0x0010
struct FBlueprintOnlineFriend
{
	unsigned char                                      UnknownData00[0x10];                                      // 0x0000(0x0010) MISSED OFFSET
};

// ScriptStruct IONBranch.GunGameStage
// 0x0020
struct FGunGameStage
{
	class UClass*                                      WeaponClass;                                              // 0x0000(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<class UClass*>                              AttachmentClasses;                                        // 0x0008(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
	int                                                NumKills;                                                 // 0x0018(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x001C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.ItemIcon
// 0x0010
struct FItemIcon
{
	class UTexture2D*                                  Standard;                                                 // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  Silhouette;                                               // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.ItemDescription
// 0x0078
struct FItemDescription
{
	struct FItemIcon                                   Icon;                                                     // 0x0000(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	EInventoryType                                     ItemType;                                                 // 0x0010(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0011(0x0007) MISSED OFFSET
	struct FText                                       DisplayName;                                              // 0x0018(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FText                                       Origin;                                                   // 0x0030(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FText                                       Era;                                                      // 0x0048(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FText                                       Description;                                              // 0x0060(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
};

// ScriptStruct IONBranch.DropConfig
// 0x0010
struct FDropConfig
{
	struct FRotator                                    RotationOffset;                                           // 0x0000(0x000C) (Edit, DisableEditOnInstance, IsPlainOldData)
	float                                              HeightOffset;                                             // 0x000C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.InventoryAttributes
// 0x0068
struct FInventoryAttributes
{
	TMap<EFirearmType, int>                            MaxAmmo;                                                  // 0x0000(0x0050) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	int                                                MaxAttachments;                                           // 0x0050(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxFirearms;                                              // 0x0054(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxBoosters;                                              // 0x0058(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxGrenades;                                              // 0x005C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                MaxWearables;                                             // 0x0060(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0064(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.FirearmModifiers
// 0x00C8
struct FFirearmModifiers
{
	float                                              DamageFactor;                                             // 0x0000(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bOverrideDamageCurve;                                     // 0x0004(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0005(0x0003) MISSED OFFSET
	struct FRuntimeFloatCurve                          DamageCurveOverride;                                      // 0x0008(0x0078) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	class UClass*                                      DamageTypeClassOverride;                                  // 0x0080(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadHipFactor;                                          // 0x0088(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADSFactor;                                          // 0x008C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadMovementPenaltyFactor;                              // 0x0090(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              HorizontalRecoilFactor;                                   // 0x0094(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              VerticalRecoilFactor;                                     // 0x0098(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimTimeFactor;                                            // 0x009C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwaySpeedAddition;                                        // 0x00A0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwayRadiusAddition;                                       // 0x00A4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSSwaySpeedAddition;                                     // 0x00A8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSSwayRadiusAddition;                                    // 0x00AC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      MagazineCapacityAdd;                                      // 0x00B0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      MaxRecoilRecoverBulletsAdd;                               // 0x00B1(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x2];                                       // 0x00B2(0x0002) MISSED OFFSET
	float                                              AimMovementSpeedPenaltyFactor;                            // 0x00B4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     VisualRecoilFactor;                                       // 0x00B8(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	float                                              VisualRecoilXHipFactor;                                   // 0x00C4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.DownedConfig
// 0x001C
struct FDownedConfig
{
	float                                              RevivedHealth;                                            // 0x0000(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              NormalDamage;                                             // 0x0004(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              TimeBetweenNormalDamage;                                  // 0x0008(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SlowDamage;                                               // 0x000C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              TimeBetweenSlowDamage;                                    // 0x0010(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ReviveTime;                                               // 0x0014(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              InvulnerableTime;                                         // 0x0018(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.DamageInfo
// 0x0010
struct FDamageInfo
{
	class APlayerState*                                Instigator;                                               // 0x0000(0x0008) (ZeroConstructor, IsPlainOldData)
	float                                              Damage;                                                   // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.BoneGroup
// 0x0018
struct FBoneGroup
{
	struct FName                                       GroupID;                                                  // 0x0000(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	TArray<struct FString>                             Bones;                                                    // 0x0008(0x0010) (Edit, ZeroConstructor, DisableEditOnInstance)
};

// ScriptStruct IONBranch.BoneDamageFactor
// 0x0010
struct FBoneDamageFactor
{
	struct FName                                       BoneName;                                                 // 0x0000(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DamageFactor;                                             // 0x0008(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bHead;                                                    // 0x000C(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x000D(0x0003) MISSED OFFSET
};

// ScriptStruct IONBranch.InspectSkinAnimation
// 0x0028
struct FInspectSkinAnimation
{
	class UAnimSequence*                               Start;                                                    // 0x0000(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               HoldRight;                                                // 0x0008(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               Transition;                                               // 0x0010(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               HoldLeft;                                                 // 0x0018(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               End;                                                      // 0x0020(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.ScoreEventConfig
// 0x0020
struct FScoreEventConfig
{
	struct FText                                       Label;                                                    // 0x0000(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	EScoreEventGroup                                   Group;                                                    // 0x0018(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0019(0x0003) MISSED OFFSET
	int                                                DefaultPoints;                                            // 0x001C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.PhysicalSurfaceProperties
// 0x0070
struct FPhysicalSurfaceProperties
{
	EMaterialLocation                                  MaterialLocation;                                         // 0x0000(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	class UParticleSystem*                             ImpactEffect;                                             // 0x0008(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               ImpactSound;                                              // 0x0010(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UMaterialInterface*                          DecalMaterial;                                            // 0x0018(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UParticleSystem*                             KnifeImpactEffect;                                        // 0x0020(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               KnifeImpactSound;                                         // 0x0028(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               SakoImpactSound;                                          // 0x0030(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UMaterialInterface*                          KnifeDecalMaterial;                                       // 0x0038(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               FootstepEvent1p;                                          // 0x0040(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               FootstepEvent3p;                                          // 0x0048(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               LandingEvent1p;                                           // 0x0050(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               LandingEvent3p;                                           // 0x0058(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               EpicLandingEvent1p;                                       // 0x0060(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               EpicLandingEvent3p;                                       // 0x0068(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.ItemSpawnRateData
// 0x0028
struct FItemSpawnRateData
{
	class UClass*                                      ItemClass;                                                // 0x0000(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region1;                                                  // 0x0008(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region2;                                                  // 0x000C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region3;                                                  // 0x0010(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region4;                                                  // 0x0014(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region5;                                                  // 0x0018(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region6;                                                  // 0x001C(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region7;                                                  // 0x0020(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              Region8;                                                  // 0x0024(0x0004) (Edit, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.Rank
// 0x0030
struct FRank
{
	class UTexture2D*                                  Icon;                                                     // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UTexture2D*                                  SmallIcon;                                                // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FText                                       DisplayText;                                              // 0x0010(0x0018) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	int                                                ScoreThreshold;                                           // 0x0028(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x002C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.SmokeEffectConfig
// 0x0010
struct FSmokeEffectConfig
{
	float                                              Strength;                                                 // 0x0000(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0004(0x0004) MISSED OFFSET
	class UParticleSystem*                             Effect;                                                   // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.LootTableEntry
// 0x0010
struct FLootTableEntry
{
	float                                              ChanceToSpawn;                                            // 0x0000(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0004(0x0004) MISSED OFFSET
	class UClass*                                      ItemClass;                                                // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.TargetData
// 0x0010
struct FTargetData
{
	struct FVector                                     Location;                                                 // 0x0000(0x000C) (BlueprintVisible, IsPlainOldData)
	float                                              TimeAtLocation;                                           // 0x000C(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.WeaponLoopAnimation
// 0x0010
struct FWeaponLoopAnimation
{
	class UAnimSequence*                               CharacterSequence;                                        // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               CharacterFirstPersonSequence;                             // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.IdleAnimation
// 0x0008 (0x0018 - 0x0010)
struct FIdleAnimation : public FWeaponLoopAnimation
{
	class UAnimSequence*                               CharacterProneSequence;                                   // 0x0010(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.WeaponTask
// 0x0068
struct FWeaponTask
{
	unsigned char                                      UnknownData00[0x8];                                       // 0x0000(0x0008) MISSED OFFSET
	class UAnimSequenceBase*                           CharacterSequence1P;                                      // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           CharacterSequence3P;                                      // 0x0010(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           WeaponSequence1P;                                         // 0x0018(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimSequenceBase*                           WeaponSequence3P;                                         // 0x0020(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAnimMontage*                                FirstPersonMontage;                                       // 0x0028(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x0030(0x0004) MISSED OFFSET
	float                                              Length;                                                   // 0x0034(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bScaleAnimationPlayRate;                                  // 0x0038(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x3];                                       // 0x0039(0x0003) MISSED OFFSET
	float                                              PlayRate;                                                 // 0x003C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BlendInTime;                                              // 0x0040(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              BlendOutTime;                                             // 0x0044(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ActionTime;                                               // 0x0048(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData03[0x1C];                                      // 0x004C(0x001C) MISSED OFFSET
};

// ScriptStruct IONBranch.WeaponAnimation
// 0x0008 (0x0070 - 0x0068)
struct FWeaponAnimation : public FWeaponTask
{
	class UAnimSequenceBase*                           CharacterFirstPersonSequence;                             // 0x0068(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.FirearmAttributes
// 0x00F0
struct FFirearmAttributes
{
	float                                              EyeHeightAdjustment;                                      // 0x0000(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              DamageFactor;                                             // 0x0004(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FRuntimeFloatCurve                          DamageCurve;                                              // 0x0008(0x0078) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	class UClass*                                      DamageTypeClass;                                          // 0x0080(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x28];                                      // 0x0088(0x0028) MISSED OFFSET
	float                                              SpreadHip;                                                // 0x00B0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADS;                                                // 0x00B4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadHipMovementPenalty;                                 // 0x00B8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADSMovementPenalty;                                 // 0x00BC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadHipJumpingPenalty;                                  // 0x00C0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADSJumpingPenalty;                                  // 0x00C4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimTime;                                                  // 0x00C8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwaySpeed;                                                // 0x00CC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SwayRadius;                                               // 0x00D0(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSSwaySpeed;                                             // 0x00D4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ADSSwayRadius;                                            // 0x00D8(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimMovementSpeedPenalty;                                  // 0x00DC(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      MagazineCapacity;                                         // 0x00E0(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x00E1(0x0003) MISSED OFFSET
	float                                              CrosshairOffset;                                          // 0x00E4(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bCanBeUsedWhenDowned;                                     // 0x00E8(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x7];                                       // 0x00E9(0x0007) MISSED OFFSET
};

// ScriptStruct IONBranch.RecoilAttributes
// 0x0050
struct FRecoilAttributes
{
	class UCurveVector*                                HorizontalRecoil;                                         // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UCurveVector*                                VerticalRecoil;                                           // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UClass*                                      BulletShake;                                              // 0x0010(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              CrouchedMultiplier;                                       // 0x0018(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ProneMultiplier;                                          // 0x001C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     ADSVisualRecoil;                                          // 0x0020(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	struct FVector                                     HipVisualRecoil;                                          // 0x002C(0x000C) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0038(0x0004) MISSED OFFSET
	float                                              FireRateOffset;                                           // 0x003C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bRecoveryProgression;                                     // 0x0040(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x1];                                       // 0x0041(0x0001) MISSED OFFSET
	bool                                               bRecoilReturn;                                            // 0x0042(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bAllowRecoilReturnCancel;                                 // 0x0043(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              RecoilReturnSpeed;                                        // 0x0044(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bApplyCompensationToFinalPitch;                           // 0x0048(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	bool                                               bRestrictBottomPitchReturn;                               // 0x0049(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x6];                                       // 0x004A(0x0006) MISSED OFFSET
};

// ScriptStruct IONBranch.BarrelProperties
// 0x0048
struct FBarrelProperties
{
	class UStaticMesh*                                 Mesh;                                                     // 0x0000(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UParticleSystem*                             Emitter;                                                  // 0x0008(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	class UAkAudioEvent*                               FireEvent;                                                // 0x0010(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               FireLoopEvent;                                            // 0x0018(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               BreakEvent;                                               // 0x0020(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               FireEventTP;                                              // 0x0028(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               FireLoopEventTP;                                          // 0x0030(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAkAudioEvent*                               BreakEventTP;                                             // 0x0038(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	float                                              MuzzleSpeed;                                              // 0x0040(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0044(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.FireAnimation
// 0x0010 (0x0080 - 0x0070)
struct FFireAnimation : public FWeaponAnimation
{
	class UAnimSequence*                               FireEmptySequence;                                        // 0x0070(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
	class UAnimSequence*                               CharacterFirstPersonAimSequence;                          // 0x0078(0x0008) (Edit, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.CircleScale
// 0x0008
struct FCircleScale
{
	float                                              Min;                                                      // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              Max;                                                      // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.IONCircleConfig
// 0x0054
struct FIONCircleConfig
{
	float                                              FleeTime;                                                 // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              ShrinkTime;                                               // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FinalZoneFleeTime;                                        // 0x0008(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FinalZoneShrinkTime;                                      // 0x000C(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FinalConvergenceFleeTime;                                 // 0x0010(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FinalConvergenceShrinkTime;                               // 0x0014(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FinalStageMagnetism;                                      // 0x0018(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FCircleScale                                EdgeLimits;                                               // 0x001C(0x0008)
	float                                              AngleLimit;                                               // 0x0024(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              StageMinFraction;                                         // 0x0028(0x0004) (ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x002C(0x0004) MISSED OFFSET
	float                                              MaxCircleDistance;                                        // 0x0030(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              SquadFactor;                                              // 0x0034(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              PlayerFactor;                                             // 0x0038(0x0004) (ZeroConstructor, IsPlainOldData)
	struct FCircleScale                                PlayerScaleRadius;                                        // 0x003C(0x0008)
	struct FCircleScale                                PlayerScaleFleeTime;                                      // 0x0044(0x0008)
	struct FCircleScale                                PlayerScaleShrinkTime;                                    // 0x004C(0x0008)
};

// ScriptStruct IONBranch.StageRulesModifier
// 0x0028
struct FStageRulesModifier
{
	struct FString                                     GameMode;                                                 // 0x0000(0x0010) (ZeroConstructor)
	int                                                MaxPlayers;                                               // 0x0010(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NumPlayableStages;                                        // 0x0014(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              SafeZoneScale;                                            // 0x0018(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              FleeTimeScale;                                            // 0x001C(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              ShrinkTimeScale;                                          // 0x0020(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              SpawnHeightScale;                                         // 0x0024(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.BeamDropEventConfig
// 0x0010
struct FBeamDropEventConfig
{
	float                                              TriggersPerMinute;                                        // 0x0000(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      MaxSimultaneousEvents;                                    // 0x0004(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bCanSpawnDuringConvergence;                               // 0x0005(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x2];                                       // 0x0006(0x0002) MISSED OFFSET
	float                                              MinDistance;                                              // 0x0008(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              MinSafeZoneRadius;                                        // 0x000C(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.InputMapping
// 0x0048
struct FInputMapping
{
	struct FName                                       MappingName;                                              // 0x0000(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FName                                       ActionName;                                               // 0x0008(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EInputMappingType                                  Type;                                                     // 0x0010(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0011(0x0007) MISSED OFFSET
	struct FKey                                        Key;                                                      // 0x0018(0x0018) (BlueprintVisible, BlueprintReadOnly)
	struct FKey                                        GamepadKey;                                               // 0x0030(0x0018) (BlueprintVisible, BlueprintReadOnly)
};

// ScriptStruct IONBranch.InputCategory
// 0x0028
struct FInputCategory
{
	struct FText                                       CategoryName;                                             // 0x0000(0x0018) (BlueprintVisible, BlueprintReadOnly)
	TArray<struct FInputMapping>                       InputMappings;                                            // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.InputKeyPreferences
// 0x0020
struct FInputKeyPreferences
{
	struct FKey                                        Key;                                                      // 0x0000(0x0018) (Edit)
	EInputKeyType                                      Type;                                                     // 0x0018(0x0001) (Edit, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0019(0x0007) MISSED OFFSET
};

// ScriptStruct IONBranch.MatchmakingPlayer
// 0x0048
struct FMatchmakingPlayer
{
	struct FString                                     SteamID;                                                  // 0x0000(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     PersonaName;                                              // 0x0010(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     AvatarUrl;                                                // 0x0020(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     GameSparksID;                                             // 0x0030(0x0010) (BlueprintVisible, ZeroConstructor)
	bool                                               bReady;                                                   // 0x0040(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0041(0x0007) MISSED OFFSET
};

// ScriptStruct IONBranch.MatchmakingParty
// 0x0068
struct FMatchmakingParty
{
	TMap<struct FString, struct FMatchmakingPlayer>    Members;                                                  // 0x0000(0x0050) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     Leader;                                                   // 0x0050(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	bool                                               bAutomatch;                                               // 0x0060(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EIONPartyTypes                                     Type;                                                     // 0x0061(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	EIONPartyRegion                                    region;                                                   // 0x0062(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x5];                                       // 0x0063(0x0005) MISSED OFFSET
};

// ScriptStruct IONBranch.MatchmakingInvite
// 0x0040
struct FMatchmakingInvite
{
	struct FString                                     SteamID;                                                  // 0x0000(0x0010) (ZeroConstructor)
	struct FString                                     PartyID;                                                  // 0x0010(0x0010) (ZeroConstructor)
	struct FString                                     PersonaName;                                              // 0x0020(0x0010) (ZeroConstructor)
	struct FString                                     Avatar;                                                   // 0x0030(0x0010) (ZeroConstructor)
};

// ScriptStruct IONBranch.AnimatedLevelProgress
// 0x0014
struct FAnimatedLevelProgress
{
	unsigned char                                      UnknownData00[0x8];                                       // 0x0000(0x0008) MISSED OFFSET
	int                                                DisplayLevel;                                             // 0x0008(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                PointsRequired;                                           // 0x000C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              PointsGained;                                             // 0x0010(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.InteractionOption
// 0x0058
struct FInteractionOption
{
	unsigned char                                      UnknownData00[0x10];                                      // 0x0000(0x0010) MISSED OFFSET
	class UObject*                                     Object;                                                   // 0x0010(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x8];                                       // 0x0018(0x0008) MISSED OFFSET
	struct FText                                       PressFunctionText;                                        // 0x0020(0x0018) (BlueprintVisible, BlueprintReadOnly)
	unsigned char                                      UnknownData02[0x8];                                       // 0x0038(0x0008) MISSED OFFSET
	struct FText                                       ReleaseFunctionText;                                      // 0x0040(0x0018) (BlueprintVisible, BlueprintReadOnly)
};

// ScriptStruct IONBranch.SightFirearmModifiers
// 0x0060
struct FSightFirearmModifiers
{
	bool                                               bOverrideEyeAdjustment;                                   // 0x0000(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0xF];                                       // 0x0001(0x000F) MISSED OFFSET
	struct FTransform                                  EyeAdjustmentOverride;                                    // 0x0010(0x0030) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance, IsPlainOldData)
	float                                              MagnificationOverride;                                    // 0x0040(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              LenseRadiusOverride;                                      // 0x0044(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              ViewRadiusOffsetOverride;                                 // 0x0048(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              SpreadADSOverride;                                        // 0x004C(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              AimTimeOverride;                                          // 0x0050(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bUseADSMesh;                                              // 0x0054(0x0001) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0055(0x0003) MISSED OFFSET
	float                                              ADSMeshMinAimRatio;                                       // 0x0058(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData02[0x4];                                       // 0x005C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.IONSpecialEvent
// 0x0030
struct FIONSpecialEvent
{
	struct FDateTime                                   Start;                                                    // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	struct FDateTime                                   End;                                                      // 0x0008(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, DisableEditOnInstance)
	TArray<class UClass*>                              Rulesets;                                                 // 0x0010(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
	struct FString                                     Tag;                                                      // 0x0020(0x0010) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance)
};

// ScriptStruct IONBranch.IONBundleItem
// 0x0010
struct FIONBundleItem
{
	class UDataAsset*                                  Item;                                                     // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                ItemCount;                                                // 0x0008(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.IONGeneratorItem
// 0x0010
struct FIONGeneratorItem
{
	class UDataAsset*                                  Item;                                                     // 0x0000(0x0008) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	int                                                DropRate;                                                 // 0x0008(0x0004) (Edit, BlueprintVisible, BlueprintReadOnly, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x000C(0x0004) MISSED OFFSET
};

// ScriptStruct IONBranch.MainMenuPartySettings
// 0x0030
struct FMainMenuPartySettings
{
	struct FString                                     region;                                                   // 0x0000(0x0010) (BlueprintVisible, ZeroConstructor)
	bool                                               bAutomatch;                                               // 0x0010(0x0001) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0011(0x0003) MISSED OFFSET
	int                                                Type;                                                     // 0x0014(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                ServerId;                                                 // 0x0018(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x001C(0x0004) MISSED OFFSET
	struct FString                                     ServerPassword;                                           // 0x0020(0x0010) (BlueprintVisible, ZeroConstructor)
};

// ScriptStruct IONBranch.MatchStats
// 0x0050
struct FMatchStats
{
	int                                                Place;                                                    // 0x0000(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                Participants;                                             // 0x0004(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                Kills;                                                    // 0x0008(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                Points;                                                   // 0x000C(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                Duration;                                                 // 0x0010(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0014(0x0004) MISSED OFFSET
	struct FString                                     WinnerName;                                               // 0x0018(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     KillerName;                                               // 0x0028(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     DeathCauserContext;                                       // 0x0038(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FDateTime                                   Date;                                                     // 0x0048(0x0008) (BlueprintVisible)
};

// ScriptStruct IONBranch.MatchmakingServerFoundResult
// 0x0040
struct FMatchmakingServerFoundResult
{
	struct FString                                     HostName;                                                 // 0x0000(0x0010) (ZeroConstructor)
	unsigned char                                      UnknownData00[0x30];                                      // 0x0010(0x0030) MISSED OFFSET
};

// ScriptStruct IONBranch.IONServerInfo
// 0x0088
struct FIONServerInfo
{
	int                                                ID;                                                       // 0x0000(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0004(0x0004) MISSED OFFSET
	struct FString                                     ip;                                                       // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                Port;                                                     // 0x0018(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                query_port;                                               // 0x001C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     Location;                                                 // 0x0020(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                max_players;                                              // 0x0030(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                player_count;                                             // 0x0034(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     server_name;                                              // 0x0038(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     game_version;                                             // 0x0048(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     game_state;                                               // 0x0058(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     region;                                                   // 0x0068(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                match_type;                                               // 0x0078(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData01[0x4];                                       // 0x007C(0x0004) MISSED OFFSET
	uint64_t                                           steam_id;                                                 // 0x0080(0x0008) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.MainMenuPartyMember
// 0x0040
struct FMainMenuPartyMember
{
	struct FString                                     SteamID;                                                  // 0x0000(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     PlayerName;                                               // 0x0010(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     AvatarUrl;                                                // 0x0020(0x0010) (BlueprintVisible, ZeroConstructor)
	struct FString                                     GameSparksID;                                             // 0x0030(0x0010) (BlueprintVisible, ZeroConstructor)
};

// ScriptStruct IONBranch.IONInventoryItem
// 0x0010
struct FIONInventoryItem
{
	unsigned char                                      UnknownData00[0x10];                                      // 0x0000(0x0010) MISSED OFFSET
};

// ScriptStruct IONBranch.GameConfig
// 0x0010
struct FGameConfig
{
	TArray<EIONPartyTypes>                             DisabledModes;                                            // 0x0000(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.PlayerProfileModeInfo
// 0x0014
struct FPlayerProfileModeInfo
{
	int                                                Rank;                                                     // 0x0000(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                Rating;                                                   // 0x0004(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumMatches;                                               // 0x0008(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumVictories;                                             // 0x000C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                TotalKills;                                               // 0x0010(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.PlayerProfile
// 0x0040
struct FPlayerProfile
{
	bool                                               bHasValidData;                                            // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	struct FString                                     ErrorMessage;                                             // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     DisplayName;                                              // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	int                                                Level;                                                    // 0x0028(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	float                                              LevelProgress;                                            // 0x002C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	TArray<struct FPlayerProfileModeInfo>              ModeInfos;                                                // 0x0030(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.PlayerStats
// 0x001C
struct FPlayerStats
{
	int                                                NumVictories;                                             // 0x0000(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumSecondPlace;                                           // 0x0004(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumThirdPlace;                                            // 0x0008(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                NumMatches;                                               // 0x000C(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                TotalKills;                                               // 0x0010(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                Rating;                                                   // 0x0014(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                RankScore;                                                // 0x0018(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.AttachmentMeshOverride
// 0x0010
struct FAttachmentMeshOverride
{
	EFirearmType                                       FirearmType;                                              // 0x0000(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	class UStaticMesh*                                 Mesh;                                                     // 0x0008(0x0008) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.CharacterAudioState
// 0x0018
struct FCharacterAudioState
{
	unsigned char                                      UnknownData00[0x18];                                      // 0x0000(0x0018) MISSED OFFSET
};

// ScriptStruct IONBranch.InputTableRow
// 0x0088 (0x0090 - 0x0008)
struct FInputTableRow : public FTableRowBase
{
	struct FName                                       Interaction;                                              // 0x0008(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	struct FText                                       DisplayName;                                              // 0x0010(0x0018) (Edit, BlueprintVisible)
	struct FText                                       Category;                                                 // 0x0028(0x0018) (Edit, BlueprintVisible)
	struct FKey                                        PrimaryDefaultKey;                                        // 0x0040(0x0018) (Edit, BlueprintVisible)
	struct FKey                                        SecondaryDefaultKey;                                      // 0x0058(0x0018) (Edit, BlueprintVisible)
	struct FKey                                        GamepadDefaultKey;                                        // 0x0070(0x0018) (Edit, BlueprintVisible)
	bool                                               bAxis;                                                    // 0x0088(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               bNegative;                                                // 0x0089(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x6];                                       // 0x008A(0x0006) MISSED OFFSET
};

// ScriptStruct IONBranch.InventoryAction
// 0x000C
struct FInventoryAction
{
	EInventoryActionType                               ActionType;                                               // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0001(0x0003) MISSED OFFSET
	int                                                Location;                                                 // 0x0004(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	int                                                OldLocation;                                              // 0x0008(0x0004) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.MatchmakingServerRuleset
// 0x0018
struct FMatchmakingServerRuleset
{
	EIONPartyTypes                                     GameType;                                                 // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	struct FString                                     MatchId;                                                  // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.DisplayedScoreEvent
// 0x000C
struct FDisplayedScoreEvent
{
	unsigned char                                      UnknownData00[0xC];                                       // 0x0000(0x000C) MISSED OFFSET
};

// ScriptStruct IONBranch.IONStoreItem
// 0x0010
struct FIONStoreItem
{
	unsigned char                                      UnknownData00[0x10];                                      // 0x0000(0x0010) MISSED OFFSET
};

// ScriptStruct IONBranch.IONPlayerLogEntry
// 0x0050
struct FIONPlayerLogEntry
{
	class UGameSparksScriptData*                       ScriptData;                                               // 0x0000(0x0008) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	struct FString                                     PlayerName;                                               // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     GameSparksID;                                             // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	struct FString                                     SteamID;                                                  // 0x0028(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	EIONPlayerLogEvent                                 EventType;                                                // 0x0038(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0039(0x0007) MISSED OFFSET
	struct FString                                     Server;                                                   // 0x0040(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.PlayerStatEntry
// 0x0028
struct FPlayerStatEntry
{
	struct FText                                       StatName;                                                 // 0x0000(0x0018) (BlueprintVisible, BlueprintReadOnly)
	struct FString                                     StatValue;                                                // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.SpawnHeightRule
// 0x0008
struct FSpawnHeightRule
{
	int                                                NumPlayers;                                               // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	float                                              SpawnHeight;                                              // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.SkipStageRule
// 0x0008
struct FSkipStageRule
{
	int                                                NumPlayers;                                               // 0x0000(0x0004) (ZeroConstructor, IsPlainOldData)
	int                                                NumPlayableStages;                                        // 0x0004(0x0004) (ZeroConstructor, IsPlainOldData)
};

// ScriptStruct IONBranch.IONHighlightEvent
// 0x0010
struct FIONHighlightEvent
{
	unsigned char                                      UnknownData00[0x10];                                      // 0x0000(0x0010) MISSED OFFSET
};

// ScriptStruct IONBranch.IONServerUpdateInfo
// 0x0018
struct FIONServerUpdateInfo
{
	bool                                               ok;                                                       // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	struct FString                                     Error;                                                    // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.IONServerListInfo
// 0x0028
struct FIONServerListInfo
{
	bool                                               ok;                                                       // 0x0000(0x0001) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x7];                                       // 0x0001(0x0007) MISSED OFFSET
	struct FString                                     Error;                                                    // 0x0008(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
	TArray<struct FIONServerInfo>                      servers;                                                  // 0x0018(0x0010) (BlueprintVisible, BlueprintReadOnly, ZeroConstructor)
};

// ScriptStruct IONBranch.GeneralDebugConfig
// 0x0018
struct FGeneralDebugConfig
{
	bool                                               bDroneFlyLocal;                                           // 0x0000(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0001(0x0003) MISSED OFFSET
	int                                                SquadSize;                                                // 0x0004(0x0004) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bForceStartBattleRoyale;                                  // 0x0008(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x3];                                       // 0x0009(0x0003) MISSED OFFSET
	int                                                NumPlayersToDraw;                                         // 0x000C(0x0004) (Edit, ZeroConstructor, Transient, IsPlainOldData)
	bool                                               bDrawSpawnRegions;                                        // 0x0010(0x0001) (Edit, ZeroConstructor, Transient, IsPlainOldData)
	bool                                               bDrawMapGrid;                                             // 0x0011(0x0001) (Edit, ZeroConstructor, Transient, IsPlainOldData)
	unsigned char                                      UnknownData02[0x2];                                       // 0x0012(0x0002) MISSED OFFSET
	float                                              DrawHeight;                                               // 0x0014(0x0004) (Edit, ZeroConstructor, Transient, IsPlainOldData)
};

// ScriptStruct IONBranch.PreQueueDebugConfig
// 0x0004
struct FPreQueueDebugConfig
{
	bool                                               bDestroyDeadPawns;                                        // 0x0000(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bAllowDowning;                                            // 0x0001(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bKillRestOfTeam;                                          // 0x0002(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               bSpawnDrones;                                             // 0x0003(0x0001) (Edit, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

// ScriptStruct IONBranch.UserProfile
// 0x0024
struct FUserProfile
{
	int                                                Rank;                                                     // 0x0000(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                Points;                                                   // 0x0004(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              AveragePlace;                                             // 0x0008(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                NumMatchesPlayed;                                         // 0x000C(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                TotalKills;                                               // 0x0010(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                TotalDeaths;                                              // 0x0014(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                NumFirstPlace;                                            // 0x0018(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                NumSecondPlace;                                           // 0x001C(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
	int                                                NumThirdPlace;                                            // 0x0020(0x0004) (BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
