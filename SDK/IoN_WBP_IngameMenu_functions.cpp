// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_IngameMenu.WBP_IngameMenu_C.OnMouseButtonDown_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry               MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FPointerEvent           MouseEvent                     (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_IngameMenu_C::OnMouseButtonDown_1(const struct FGeometry& MyGeometry, struct FPointerEvent* MouseEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.OnMouseButtonDown_1");

	UWBP_IngameMenu_C_OnMouseButtonDown_1_Params params;
	params.MyGeometry = MyGeometry;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (MouseEvent != nullptr)
		*MouseEvent = params.MouseEvent;

	return params.ReturnValue;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.OnKeyDown
// (BlueprintCosmetic, Event, Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// struct FKeyEvent*              InKeyEvent                     (BlueprintVisible, BlueprintReadOnly, Parm)
// struct FEventReply             ReturnValue                    (Parm, OutParm, ReturnParm)

struct FEventReply UWBP_IngameMenu_C::OnKeyDown(struct FGeometry* MyGeometry, struct FKeyEvent* InKeyEvent)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.OnKeyDown");

	UWBP_IngameMenu_C_OnKeyDown_Params params;
	params.MyGeometry = MyGeometry;
	params.InKeyEvent = InKeyEvent;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.OnSettingsMenuClosed
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_IngameMenu_C::OnSettingsMenuClosed()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.OnSettingsMenuClosed");

	UWBP_IngameMenu_C_OnSettingsMenuClosed_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameMenu_C::BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature");

	UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_C_1_K2Node_ComponentBoundEvent_222_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameMenu_C::BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature");

	UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_127_K2Node_ComponentBoundEvent_148_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameMenu_C::BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature");

	UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_255_K2Node_ComponentBoundEvent_145_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.OnAnimationFinished
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// class UWidgetAnimation**       Animation                      (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_IngameMenu_C::OnAnimationFinished(class UWidgetAnimation** Animation)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.OnAnimationFinished");

	UWBP_IngameMenu_C_OnAnimationFinished_Params params;
	params.Animation = Animation;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature
// (BlueprintEvent)

void UWBP_IngameMenu_C::BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature");

	UWBP_IngameMenu_C_BndEvt__WBP_IngameButton_C_0_K2Node_ComponentBoundEvent_143_OnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_IngameMenu_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.Construct");

	UWBP_IngameMenu_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.On Close Menu
// (BlueprintCallable, BlueprintEvent)

void UWBP_IngameMenu_C::On_Close_Menu()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.On Close Menu");

	UWBP_IngameMenu_C_On_Close_Menu_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_IngameMenu.WBP_IngameMenu_C.ExecuteUbergraph_WBP_IngameMenu
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_IngameMenu_C::ExecuteUbergraph_WBP_IngameMenu(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_IngameMenu.WBP_IngameMenu_C.ExecuteUbergraph_WBP_IngameMenu");

	UWBP_IngameMenu_C_ExecuteUbergraph_WBP_IngameMenu_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
