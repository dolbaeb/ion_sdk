#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.AcceptsDropForItem
struct UWBP_AttachmentNode_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Get_EmptyBG_Visibility_1
struct UWBP_AttachmentNode_C_Get_EmptyBG_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnDrop
struct UWBP_AttachmentNode_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_2
struct UWBP_AttachmentNode_C_OnMouseButtonDown_2_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnMouseButtonDown_1
struct UWBP_AttachmentNode_C_OnMouseButtonDown_1_Params
{
	struct FGeometry                                   MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent                               MouseEvent;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	struct FEventReply                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetText_1
struct UWBP_AttachmentNode_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.GetCurrentItem
struct UWBP_AttachmentNode_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.OnItemChanged
struct UWBP_AttachmentNode_C_OnItemChanged_Params
{
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.PreConstruct
struct UWBP_AttachmentNode_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Tick
struct UWBP_AttachmentNode_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.Construct
struct UWBP_AttachmentNode_C_Construct_Params
{
};

// Function WBP_AttachmentNode.WBP_AttachmentNode_C.ExecuteUbergraph_WBP_AttachmentNode
struct UWBP_AttachmentNode_C_ExecuteUbergraph_WBP_AttachmentNode_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
