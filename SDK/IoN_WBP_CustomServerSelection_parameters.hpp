#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Construct
struct UWBP_CustomServerSelection_C_Construct_Params
{
};

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature
struct UWBP_CustomServerSelection_C_BndEvt__Btn_K2Node_ComponentBoundEvent_70_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.Update Selected
struct UWBP_CustomServerSelection_C_Update_Selected_Params
{
};

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.PreConstruct
struct UWBP_CustomServerSelection_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature
struct UWBP_CustomServerSelection_C_BndEvt__EditableText_0_K2Node_ComponentBoundEvent_91_OnEditableTextCommittedEvent__DelegateSignature_Params
{
	struct FText                                       Text;                                                     // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ReferenceParm)
	TEnumAsByte<ETextCommit>                           CommitMethod;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CustomServerSelection.WBP_CustomServerSelection_C.ExecuteUbergraph_WBP_CustomServerSelection
struct UWBP_CustomServerSelection_C_ExecuteUbergraph_WBP_CustomServerSelection_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
