// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_AIController.BP_AIController_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_AIController_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AIController.BP_AIController_C.UserConstructionScript");

	ABP_AIController_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AIController.BP_AIController_C.ReceiveBeginPlay
// (Event, Protected, BlueprintEvent)

void ABP_AIController_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AIController.BP_AIController_C.ReceiveBeginPlay");

	ABP_AIController_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AIController.BP_AIController_C.Move to random spot
// (BlueprintCallable, BlueprintEvent)

void ABP_AIController_C::Move_to_random_spot()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AIController.BP_AIController_C.Move to random spot");

	ABP_AIController_C_Move_to_random_spot_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_AIController.BP_AIController_C.ExecuteUbergraph_BP_AIController
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_AIController_C::ExecuteUbergraph_BP_AIController(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_AIController.BP_AIController_C.ExecuteUbergraph_BP_AIController");

	ABP_AIController_C_ExecuteUbergraph_BP_AIController_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
