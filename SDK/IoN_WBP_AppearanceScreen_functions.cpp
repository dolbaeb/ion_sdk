// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face1_Btn_K2Node_ComponentBoundEvent_257_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__Face1_Btn_K2Node_ComponentBoundEvent_257_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face1_Btn_K2Node_ComponentBoundEvent_257_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__Face1_Btn_K2Node_ComponentBoundEvent_257_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face2_Btn_K2Node_ComponentBoundEvent_277_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__Face2_Btn_K2Node_ComponentBoundEvent_277_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face2_Btn_K2Node_ComponentBoundEvent_277_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__Face2_Btn_K2Node_ComponentBoundEvent_277_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_0_Btn_K2Node_ComponentBoundEvent_331_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__SkinColor_0_Btn_K2Node_ComponentBoundEvent_331_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_0_Btn_K2Node_ComponentBoundEvent_331_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__SkinColor_0_Btn_K2Node_ComponentBoundEvent_331_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_1_Btn_K2Node_ComponentBoundEvent_351_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__SkinColor_1_Btn_K2Node_ComponentBoundEvent_351_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_1_Btn_K2Node_ComponentBoundEvent_351_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__SkinColor_1_Btn_K2Node_ComponentBoundEvent_351_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_2_Btn_K2Node_ComponentBoundEvent_372_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__SkinColor_2_Btn_K2Node_ComponentBoundEvent_372_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_2_Btn_K2Node_ComponentBoundEvent_372_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__SkinColor_2_Btn_K2Node_ComponentBoundEvent_372_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_3_Btn_K2Node_ComponentBoundEvent_394_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__SkinColor_3_Btn_K2Node_ComponentBoundEvent_394_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_3_Btn_K2Node_ComponentBoundEvent_394_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__SkinColor_3_Btn_K2Node_ComponentBoundEvent_394_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_4_Btn_K2Node_ComponentBoundEvent_417_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__SkinColor_4_Btn_K2Node_ComponentBoundEvent_417_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__SkinColor_4_Btn_K2Node_ComponentBoundEvent_417_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__SkinColor_4_Btn_K2Node_ComponentBoundEvent_417_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face0_Btn_K2Node_ComponentBoundEvent_240_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__Face0_Btn_K2Node_ComponentBoundEvent_240_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__Face0_Btn_K2Node_ComponentBoundEvent_240_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__Face0_Btn_K2Node_ComponentBoundEvent_240_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__HairBtn_0_K2Node_ComponentBoundEvent_547_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__HairBtn_0_K2Node_ComponentBoundEvent_547_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__HairBtn_0_K2Node_ComponentBoundEvent_547_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__HairBtn_0_K2Node_ComponentBoundEvent_547_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__HairBtn_1_K2Node_ComponentBoundEvent_564_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_AppearanceScreen_C::BndEvt__HairBtn_1_K2Node_ComponentBoundEvent_564_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.BndEvt__HairBtn_1_K2Node_ComponentBoundEvent_564_OnButtonClickedEvent__DelegateSignature");

	UWBP_AppearanceScreen_C_BndEvt__HairBtn_1_K2Node_ComponentBoundEvent_564_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.ExecuteUbergraph_WBP_AppearanceScreen
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_AppearanceScreen_C::ExecuteUbergraph_WBP_AppearanceScreen(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_AppearanceScreen.WBP_AppearanceScreen_C.ExecuteUbergraph_WBP_AppearanceScreen");

	UWBP_AppearanceScreen_C_ExecuteUbergraph_WBP_AppearanceScreen_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
