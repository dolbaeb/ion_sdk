// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_Scope12x.BP_Scope12x_C.UserConstructionScript
// (Event, Public, BlueprintCallable, BlueprintEvent)

void ABP_Scope12x_C::UserConstructionScript()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Scope12x.BP_Scope12x_C.UserConstructionScript");

	ABP_Scope12x_C_UserConstructionScript_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Scope12x.BP_Scope12x_C.UpdateMaterialAimState
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float*                         AimRatio                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Scope12x_C::UpdateMaterialAimState(class AIONFirearm** Firearm, float* AimRatio)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Scope12x.BP_Scope12x_C.UpdateMaterialAimState");

	ABP_Scope12x_C_UpdateMaterialAimState_Params params;
	params.Firearm = Firearm;
	params.AimRatio = AimRatio;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Scope12x.BP_Scope12x_C.DetachFromFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Scope12x_C::DetachFromFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Scope12x.BP_Scope12x_C.DetachFromFirearmBlueprint");

	ABP_Scope12x_C_DetachFromFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Scope12x.BP_Scope12x_C.AttachToFirearmBlueprint
// (Event, Public, BlueprintEvent)
// Parameters:
// class AIONFirearm**            Firearm                        (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Scope12x_C::AttachToFirearmBlueprint(class AIONFirearm** Firearm)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Scope12x.BP_Scope12x_C.AttachToFirearmBlueprint");

	ABP_Scope12x_C_AttachToFirearmBlueprint_Params params;
	params.Firearm = Firearm;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_Scope12x.BP_Scope12x_C.ExecuteUbergraph_BP_Scope12x
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void ABP_Scope12x_C::ExecuteUbergraph_BP_Scope12x(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_Scope12x.BP_Scope12x_C.ExecuteUbergraph_BP_Scope12x");

	ABP_Scope12x_C_ExecuteUbergraph_BP_Scope12x_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
