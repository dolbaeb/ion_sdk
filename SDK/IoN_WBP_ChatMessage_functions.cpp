// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ChatMessage.WBP_ChatMessage_C.InitChatMessage
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// class AIONPlayerState*         From                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// struct FString                 Message                        (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
// struct FName                   Type                           (BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm, IsPlainOldData)

void UWBP_ChatMessage_C::InitChatMessage(class AIONPlayerState* From, struct FString* Message, struct FName* Type)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatMessage.WBP_ChatMessage_C.InitChatMessage");

	UWBP_ChatMessage_C_InitChatMessage_Params params;
	params.From = From;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	if (Message != nullptr)
		*Message = params.Message;
	if (Type != nullptr)
		*Type = params.Type;
}


// Function WBP_ChatMessage.WBP_ChatMessage_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_ChatMessage_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatMessage.WBP_ChatMessage_C.Construct");

	UWBP_ChatMessage_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ChatMessage.WBP_ChatMessage_C.ExecuteUbergraph_WBP_ChatMessage
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ChatMessage_C::ExecuteUbergraph_WBP_ChatMessage(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ChatMessage.WBP_ChatMessage_C.ExecuteUbergraph_WBP_ChatMessage");

	UWBP_ChatMessage_C_ExecuteUbergraph_WBP_ChatMessage_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
