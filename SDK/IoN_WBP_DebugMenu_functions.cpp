// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_DebugMenu.WBP_DebugMenu_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_DebugMenu_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenu.WBP_DebugMenu_C.Construct");

	UWBP_DebugMenu_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenu.WBP_DebugMenu_C.Button Clicked
// (BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWBP_DebugMenuButton_C*  Object_To_Ignore               (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)

void UWBP_DebugMenu_C::Button_Clicked(class UWBP_DebugMenuButton_C* Object_To_Ignore)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenu.WBP_DebugMenu_C.Button Clicked");

	UWBP_DebugMenu_C_Button_Clicked_Params params;
	params.Object_To_Ignore = Object_To_Ignore;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenu.WBP_DebugMenu_C.Toggle Visibility
// (BlueprintCallable, BlueprintEvent)

void UWBP_DebugMenu_C::Toggle_Visibility()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenu.WBP_DebugMenu_C.Toggle Visibility");

	UWBP_DebugMenu_C_Toggle_Visibility_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_DebugMenu.WBP_DebugMenu_C.ExecuteUbergraph_WBP_DebugMenu
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_DebugMenu_C::ExecuteUbergraph_WBP_DebugMenu(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_DebugMenu.WBP_DebugMenu_C.ExecuteUbergraph_WBP_DebugMenu");

	UWBP_DebugMenu_C_ExecuteUbergraph_WBP_DebugMenu_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
