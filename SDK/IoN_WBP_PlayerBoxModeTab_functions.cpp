// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.Get_SelectedBorder_BrushColor_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FLinearColor            ReturnValue                    (Parm, OutParm, ReturnParm, IsPlainOldData)

struct FLinearColor UWBP_PlayerBoxModeTab_C::Get_SelectedBorder_BrushColor_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.Get_SelectedBorder_BrushColor_1");

	UWBP_PlayerBoxModeTab_C_Get_SelectedBorder_BrushColor_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBoxModeTab_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.PreConstruct");

	UWBP_PlayerBoxModeTab_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_PlayerBoxModeTab_C::BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature");

	UWBP_PlayerBoxModeTab_C_BndEvt__Button_0_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.ExecuteUbergraph_WBP_PlayerBoxModeTab
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBoxModeTab_C::ExecuteUbergraph_WBP_PlayerBoxModeTab(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.ExecuteUbergraph_WBP_PlayerBoxModeTab");

	UWBP_PlayerBoxModeTab_C_ExecuteUbergraph_WBP_PlayerBoxModeTab_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.OnModeButtonClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// int                            Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_PlayerBoxModeTab_C::OnModeButtonClicked__DelegateSignature(int Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_PlayerBoxModeTab.WBP_PlayerBoxModeTab_C.OnModeButtonClicked__DelegateSignature");

	UWBP_PlayerBoxModeTab_C_OnModeButtonClicked__DelegateSignature_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
