#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SKeyConflict.SKeyConflict
// 0x0009
struct FSKeyConflict
{
	class UBP_KeyCombination_C*                        ConflictingCombination_12_3DB110B5406338347E3A6897032E00F8;// 0x0000(0x0008) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	TEnumAsByte<EKeyConflict>                          ConflictType_7_4EB561DE4E1187B4D868FAB6B58BCAE3;          // 0x0008(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
