#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.SetItemPrice
struct UWBP_StoreItem_Weapon_C_SetItemPrice_Params
{
	struct FString                                     Price;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__OpenCrateBtn_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__Button_3_K2Node_ComponentBoundEvent_89_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_117_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__CrateBtn_K2Node_ComponentBoundEvent_130_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
struct UWBP_StoreItem_Weapon_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PreConstruct
struct UWBP_StoreItem_Weapon_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.Construct
struct UWBP_StoreItem_Weapon_C_Construct_Params
{
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ExecuteUbergraph_WBP_StoreItem_Weapon
struct UWBP_StoreItem_Weapon_C_ExecuteUbergraph_WBP_StoreItem_Weapon_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.ViewItemDetails__DelegateSignature
struct UWBP_StoreItem_Weapon_C_ViewItemDetails__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_StoreItem_Weapon.WBP_StoreItem_Weapon_C.PurchaseItem__DelegateSignature
struct UWBP_StoreItem_Weapon_C_PurchaseItem__DelegateSignature_Params
{
	class UIONSteamItem*                               Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
