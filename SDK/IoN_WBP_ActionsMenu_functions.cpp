// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetEquipped
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// bool                           Equipped                       (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ActionsMenu_C::SetEquipped(bool Equipped)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetEquipped");

	UWBP_ActionsMenu_C_SetEquipped_Params params;
	params.Equipped = Equipped;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetSteamItem
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FIONSteamInventoryItem  Item                           (BlueprintVisible, BlueprintReadOnly, Parm)

void UWBP_ActionsMenu_C::SetSteamItem(const struct FIONSteamInventoryItem& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.SetSteamItem");

	UWBP_ActionsMenu_C_SetSteamItem_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ActionsMenu_C::BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature");

	UWBP_ActionsMenu_C_BndEvt__EquipButton_K2Node_ComponentBoundEvent_0_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ActionsMenu_C::BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature");

	UWBP_ActionsMenu_C_BndEvt__InspectButton_K2Node_ComponentBoundEvent_1_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ActionsMenu_C::BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature");

	UWBP_ActionsMenu_C_BndEvt__MarketplaceButton_K2Node_ComponentBoundEvent_2_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_ActionsMenu_C::BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature");

	UWBP_ActionsMenu_C_BndEvt__RecycleButton_K2Node_ComponentBoundEvent_3_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ExecuteUbergraph_WBP_ActionsMenu
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ActionsMenu_C::ExecuteUbergraph_WBP_ActionsMenu(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.ExecuteUbergraph_WBP_ActionsMenu");

	UWBP_ActionsMenu_C_ExecuteUbergraph_WBP_ActionsMenu_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ScrapBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ActionsMenu_C::ScrapBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.ScrapBtnClicked__DelegateSignature");

	UWBP_ActionsMenu_C_ScrapBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.ResetBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ActionsMenu_C::ResetBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.ResetBtnClicked__DelegateSignature");

	UWBP_ActionsMenu_C_ResetBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.InspectBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ActionsMenu_C::InspectBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.InspectBtnClicked__DelegateSignature");

	UWBP_ActionsMenu_C_InspectBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ActionsMenu.WBP_ActionsMenu_C.EquipBtnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_ActionsMenu_C::EquipBtnClicked__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ActionsMenu.WBP_ActionsMenu_C.EquipBtnClicked__DelegateSignature");

	UWBP_ActionsMenu_C_EquipBtnClicked__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
