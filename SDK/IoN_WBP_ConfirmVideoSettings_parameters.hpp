#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.GetText_1
struct UWBP_ConfirmVideoSettings_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.Construct
struct UWBP_ConfirmVideoSettings_C_Construct_Params
{
};

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.RevertSettings
struct UWBP_ConfirmVideoSettings_C_RevertSettings_Params
{
};

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature
struct UWBP_ConfirmVideoSettings_C_BndEvt__RevertButton_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature
struct UWBP_ConfirmVideoSettings_C_BndEvt__ApplyButton_K2Node_ComponentBoundEvent_53_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_ConfirmVideoSettings.WBP_ConfirmVideoSettings_C.ExecuteUbergraph_WBP_ConfirmVideoSettings
struct UWBP_ConfirmVideoSettings_C_ExecuteUbergraph_WBP_ConfirmVideoSettings_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
