#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SKeyMapping.SKeyMapping
// 0x0038
struct FSKeyMapping
{
	struct FString                                     Name_47_08EFD38348F7DCF7A978B48A231BE99A;                 // 0x0000(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	float                                              Scale_34_289CBB724D0D417AFBCABCB49A6C3F69;                // 0x0010(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	bool                                               CantBeNone_117_2C54633F41F83AFF31CE7DB0A0248175;          // 0x0014(0x0001) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0015(0x0003) MISSED OFFSET
	TArray<struct FSKeyInput>                          PrimaryCombination_108_2AC9E34F451AA6057A0AA4BC145374A3;  // 0x0018(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
	TArray<struct FSKeyInput>                          SecondaryCombination_109_A8394862473F9E9DAA35718F45B72892;// 0x0028(0x0010) (Edit, BlueprintVisible, ZeroConstructor)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
