#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Ring Size
struct UWBP_CircleProgressBar_C_Update_Ring_Size_Params
{
	float                                              New_Ring_Size;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Clockwise
struct UWBP_CircleProgressBar_C_Update_Clockwise_Params
{
	bool                                               New_Clockwise;                                            // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Step
struct UWBP_CircleProgressBar_C_Update_Step_Params
{
	bool                                               New_Step;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              New_Step_Ammount;                                         // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Space
struct UWBP_CircleProgressBar_C_Update_Space_Params
{
	bool                                               New_Space;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	float                                              New_Space_Size;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Empty Color
struct UWBP_CircleProgressBar_C_Update_Empty_Color_Params
{
	struct FLinearColor                                New_Color;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Fill Color
struct UWBP_CircleProgressBar_C_Update_Fill_Color_Params
{
	struct FLinearColor                                New_Color;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Update Percentage
struct UWBP_CircleProgressBar_C_Update_Percentage_Params
{
	float                                              New_Percentage;                                           // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.PreConstruct
struct UWBP_CircleProgressBar_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.Construct
struct UWBP_CircleProgressBar_C_Construct_Params
{
};

// Function WBP_CircleProgressBar.WBP_CircleProgressBar_C.ExecuteUbergraph_WBP_CircleProgressBar
struct UWBP_CircleProgressBar_C_ExecuteUbergraph_WBP_CircleProgressBar_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
