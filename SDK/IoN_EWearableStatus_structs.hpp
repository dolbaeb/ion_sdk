#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum EWearableStatus.EWearableStatus
enum class EWearableStatus : uint8_t
{
	EWearableStatus__NewEnumerator0 = 0,
	EWearableStatus__NewEnumerator1 = 1,
	EWearableStatus__EWearableStatus_MAX = 2
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
