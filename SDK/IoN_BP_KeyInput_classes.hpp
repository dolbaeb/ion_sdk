#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_KeyInput.BP_KeyInput_C
// 0x0040 (0x0070 - 0x0030)
class UBP_KeyInput_C : public UObject
{
public:
	struct FKey                                        Key_Input;                                                // 0x0030(0x0018) (Edit, BlueprintVisible, DisableEditOnInstance)
	bool                                               Analog_Use_Negative_Axis;                                 // 0x0048(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x3];                                       // 0x0049(0x0003) MISSED OFFSET
	float                                              Analog_Previous_Axis_Value;                               // 0x004C(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              Analog_Current_Axis_Value;                                // 0x0050(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	float                                              World_Delta_Seconds;                                      // 0x0054(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	bool                                               Input_is_Using_Delta;                                     // 0x0058(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData01[0x7];                                       // 0x0059(0x0007) MISSED OFFSET
	struct FString                                     Display_Name;                                             // 0x0060(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_KeyInput.BP_KeyInput_C");
		return ptr;
	}


	void Generate_Display_Name();
	void Save_Key_Input(class UBP_GameSettings_C* Game_Settings, const struct FSKeyActionSave& KeySave);
	void Update_Analog_Axis_Value(float World_Delta_Seconds, class APlayerController* Player_Controller);
	void Key_Input_Current_State(class APlayerController* Controller, float* Axis_Value, bool* Down, bool* Just_Pressed, bool* Just_Released);
	void Init_Key_Input(const struct FSKeyInput& Key_Input, class UBP_KeyInput_C** Input);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
