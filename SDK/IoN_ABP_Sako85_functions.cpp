// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function ABP_Sako85.ABP_Sako85_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105
// (BlueprintEvent)

void UABP_Sako85_C::EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105()
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_Sako85.ABP_Sako85_C.EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105");

	UABP_Sako85_C_EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_Sako85_AnimGraphNode_BlendListByBool_3B31B6BA40EA75B7EE74A3A29BB60105_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_Sako85.ABP_Sako85_C.BlueprintUpdateAnimation
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaTimeX                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_Sako85_C::BlueprintUpdateAnimation(float* DeltaTimeX)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_Sako85.ABP_Sako85_C.BlueprintUpdateAnimation");

	UABP_Sako85_C_BlueprintUpdateAnimation_Params params;
	params.DeltaTimeX = DeltaTimeX;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function ABP_Sako85.ABP_Sako85_C.ExecuteUbergraph_ABP_Sako85
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UABP_Sako85_C::ExecuteUbergraph_ABP_Sako85(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function ABP_Sako85.ABP_Sako85_C.ExecuteUbergraph_ABP_Sako85");

	UABP_Sako85_C_ExecuteUbergraph_ABP_Sako85_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
