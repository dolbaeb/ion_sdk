#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MMBtn.WBP_MMBtn_C.SetPrice
struct UWBP_MMBtn_C_SetPrice_Params
{
	int                                                NewPrice;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MMBtn.WBP_MMBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature
struct UWBP_MMBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_17_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_MMBtn.WBP_MMBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature
struct UWBP_MMBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_415_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MMBtn.WBP_MMBtn_C.BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature
struct UWBP_MMBtn_C_BndEvt__BuyCrateBtn_K2Node_ComponentBoundEvent_430_OnButtonHoverEvent__DelegateSignature_Params
{
};

// Function WBP_MMBtn.WBP_MMBtn_C.PreConstruct
struct UWBP_MMBtn_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MMBtn.WBP_MMBtn_C.ExecuteUbergraph_WBP_MMBtn
struct UWBP_MMBtn_C_ExecuteUbergraph_WBP_MMBtn_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MMBtn.WBP_MMBtn_C.BtnPressed__DelegateSignature
struct UWBP_MMBtn_C_BtnPressed__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
