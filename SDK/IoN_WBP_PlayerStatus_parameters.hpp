#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Overlay_ShieldBar_Visibility_1
struct UWBP_PlayerStatus_C_Get_Overlay_ShieldBar_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_Image_TeamColor_ColorAndOpacity_1
struct UWBP_PlayerStatus_C_Get_Image_TeamColor_ColorAndOpacity_1_Params
{
	struct FLinearColor                                ReturnValue;                                              // (Parm, OutParm, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Get_ProgressBar_Health_Percent_1
struct UWBP_PlayerStatus_C_Get_ProgressBar_Health_Percent_1_Params
{
	float                                              ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.GetText_1
struct UWBP_PlayerStatus_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Construct
struct UWBP_PlayerStatus_C_Construct_Params
{
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.Tick
struct UWBP_PlayerStatus_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PlayerStatus.WBP_PlayerStatus_C.ExecuteUbergraph_WBP_PlayerStatus
struct UWBP_PlayerStatus_C_ExecuteUbergraph_WBP_PlayerStatus_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
