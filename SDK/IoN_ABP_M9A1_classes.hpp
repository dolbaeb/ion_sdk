#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_M9A1.ABP_M9A1_C
// 0x01D9 (0x05A9 - 0x03D0)
class UABP_M9A1_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_B0A2D18D493327EE7CC3A29A21169F97;      // 0x03D8(0x0048)
	struct FAnimNode_TwoWayBlend                       AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26;// 0x0420(0x0078)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_70E083E048B3DA5B9AED91803F14E5BC;// 0x0498(0x0038)
	struct FAnimNode_SequencePlayer                    AnimGraphNode_SequencePlayer_6EE20CE54F72F74FE385258EE998879A;// 0x04D0(0x0070)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_FE6457E840D02C1473EE8F859024EA41;      // 0x0540(0x0068)
	bool                                               bRoundChambered;                                          // 0x05A8(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_M9A1.ABP_M9A1_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_M9A1_AnimGraphNode_TwoWayBlend_22A3157E4CE932969E05E994B76BFA26();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void ExecuteUbergraph_ABP_M9A1(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
