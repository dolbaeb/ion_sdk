#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass BP_IonLegArmor.BP_IonLegArmor_C
// 0x0008 (0x0488 - 0x0480)
class ABP_IonLegArmor_C : public ABP_ArmorLegs_C
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0480(0x0008) (Transient, DuplicateTransient)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("BlueprintGeneratedClass BP_IonLegArmor.BP_IonLegArmor_C");
		return ptr;
	}


	void UserConstructionScript();
	void BPEvent_AttachWearable();
	void BPEvent_DetachWearable();
	void ExecuteUbergraph_BP_IonLegArmor(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
