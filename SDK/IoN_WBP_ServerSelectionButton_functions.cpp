// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ServerSelectionButton_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.PreConstruct");

	UWBP_ServerSelectionButton_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.ExecuteUbergraph_WBP_ServerSelectionButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ServerSelectionButton_C::ExecuteUbergraph_WBP_ServerSelectionButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.ExecuteUbergraph_WBP_ServerSelectionButton");

	UWBP_ServerSelectionButton_C_ExecuteUbergraph_WBP_ServerSelectionButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.OnClicked__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// EIONPartyTypes                 Mode                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_ServerSelectionButton_C::OnClicked__DelegateSignature(EIONPartyTypes Mode)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.OnClicked__DelegateSignature");

	UWBP_ServerSelectionButton_C_OnClicked__DelegateSignature_Params params;
	params.Mode = Mode;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
