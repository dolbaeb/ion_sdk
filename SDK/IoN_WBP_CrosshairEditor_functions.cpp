// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__BlueSlider_K2Node_ComponentBoundEvent_0_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__RedSlider_K2Node_ComponentBoundEvent_1_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__GreenSlider_K2Node_ComponentBoundEvent_2_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_3_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_4_OnValueCommitted__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__RedSlider_K2Node_ComponentBoundEvent_5_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__GreenSlider_K2Node_ComponentBoundEvent_6_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__BlueSlider_K2Node_ComponentBoundEvent_7_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__OpacitySlider_K2Node_ComponentBoundEvent_8_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// float                          Value                          (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature(float Value)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature");

	UWBP_CrosshairEditor_C_BndEvt__ScaleSlider_K2Node_ComponentBoundEvent_9_OnValueChanged__DelegateSignature_Params params;
	params.Value = Value;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.PreConstruct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// bool*                          IsDesignTime                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::PreConstruct(bool* IsDesignTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.PreConstruct");

	UWBP_CrosshairEditor_C_PreConstruct_Params params;
	params.IsDesignTime = IsDesignTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CrosshairEditor_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.Construct");

	UWBP_CrosshairEditor_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.UpdateCrosshairPreview
// (BlueprintCallable, BlueprintEvent)

void UWBP_CrosshairEditor_C::UpdateCrosshairPreview()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.UpdateCrosshairPreview");

	UWBP_CrosshairEditor_C_UpdateCrosshairPreview_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.ExecuteUbergraph_WBP_CrosshairEditor
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CrosshairEditor_C::ExecuteUbergraph_WBP_CrosshairEditor(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CrosshairEditor.WBP_CrosshairEditor_C.ExecuteUbergraph_WBP_CrosshairEditor");

	UWBP_CrosshairEditor_C_ExecuteUbergraph_WBP_CrosshairEditor_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
