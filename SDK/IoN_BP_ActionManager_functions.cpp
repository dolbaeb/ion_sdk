// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_ActionManager.BP_ActionManager_C.Any Key Press
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FKey                    Any_Key                        (BlueprintVisible, BlueprintReadOnly, Parm)

void UBP_ActionManager_C::Any_Key_Press(const struct FKey& Any_Key)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.Any Key Press");

	UBP_ActionManager_C_Any_Key_Press_Params params;
	params.Any_Key = Any_Key;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.Input Analog Update
// (Private, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::Input_Analog_Update()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.Input Analog Update");

	UBP_ActionManager_C_Input_Analog_Update_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.Listens New Mouse Bindings
// (Private, HasDefaults, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::Listens_New_Mouse_Bindings()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.Listens New Mouse Bindings");

	UBP_ActionManager_C_Listens_New_Mouse_Bindings_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.Register New Binding
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FSKeyInput              New_Keybinding                 (BlueprintVisible, BlueprintReadOnly, Parm)

void UBP_ActionManager_C::Register_New_Binding(const struct FSKeyInput& New_Keybinding)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.Register New Binding");

	UBP_ActionManager_C_Register_New_Binding_Params params;
	params.New_Keybinding = New_Keybinding;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.ReceiveBeginPlay
// (Event, Public, BlueprintEvent)

void UBP_ActionManager_C::ReceiveBeginPlay()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.ReceiveBeginPlay");

	UBP_ActionManager_C_ReceiveBeginPlay_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.Tick Action Manager
// (BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::Tick_Action_Manager()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.Tick Action Manager");

	UBP_ActionManager_C_Tick_Action_Manager_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.ReceiveTick
// (Event, Public, BlueprintEvent)
// Parameters:
// float*                         DeltaSeconds                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::ReceiveTick(float* DeltaSeconds)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.ReceiveTick");

	UBP_ActionManager_C_ReceiveTick_Params params;
	params.DeltaSeconds = DeltaSeconds;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.ExecuteUbergraph_BP_ActionManager
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::ExecuteUbergraph_BP_ActionManager(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.ExecuteUbergraph_BP_ActionManager");

	UBP_ActionManager_C_ExecuteUbergraph_BP_ActionManager_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.MenuPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::MenuPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.MenuPressed__DelegateSignature");

	UBP_ActionManager_C_MenuPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.RunReleased__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::RunReleased__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.RunReleased__DelegateSignature");

	UBP_ActionManager_C_RunReleased__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.RunPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::RunPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.RunPressed__DelegateSignature");

	UBP_ActionManager_C_RunPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.JumpPressed__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_ActionManager_C::JumpPressed__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.JumpPressed__DelegateSignature");

	UBP_ActionManager_C_JumpPressed__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.LookRight__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Axis                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::LookRight__DelegateSignature(float Axis)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.LookRight__DelegateSignature");

	UBP_ActionManager_C_LookRight__DelegateSignature_Params params;
	params.Axis = Axis;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.LookUp__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Axis                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::LookUp__DelegateSignature(float Axis)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.LookUp__DelegateSignature");

	UBP_ActionManager_C_LookUp__DelegateSignature_Params params;
	params.Axis = Axis;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.MoveRight__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Axis                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::MoveRight__DelegateSignature(float Axis)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.MoveRight__DelegateSignature");

	UBP_ActionManager_C_MoveRight__DelegateSignature_Params params;
	params.Axis = Axis;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_ActionManager.BP_ActionManager_C.MoveForward__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)
// Parameters:
// float                          Axis                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UBP_ActionManager_C::MoveForward__DelegateSignature(float Axis)
{
	static auto fn = UObject::FindObject<UFunction>("Function BP_ActionManager.BP_ActionManager_C.MoveForward__DelegateSignature");

	UBP_ActionManager_C_MoveForward__DelegateSignature_Params params;
	params.Axis = Axis;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
