// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.On_ComboBoxResolutions_GenerateWidget_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent)
// Parameters:
// struct FString                 Item                           (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// class UWidget*                 ReturnValue                    (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)

class UWidget* UWBP_SettingsPage_Video_C::On_ComboBoxResolutions_GenerateWidget_1(const struct FString& Item)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.On_ComboBoxResolutions_GenerateWidget_1");

	UWBP_SettingsPage_Video_C_On_ComboBoxResolutions_GenerateWidget_1_Params params;
	params.Item = Item;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SettingsPage_Video_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.Construct");

	UWBP_SettingsPage_Video_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Video_C::BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature");

	UWBP_SettingsPage_Video_C_BndEvt__ComboBoxResolutions_K2Node_ComponentBoundEvent_38_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature
// (BlueprintEvent)
// Parameters:
// struct FString                 SelectedItem                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
// TEnumAsByte<ESelectInfo>       SelectionType                  (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Video_C::BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature(const struct FString& SelectedItem, TEnumAsByte<ESelectInfo> SelectionType)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature");

	UWBP_SettingsPage_Video_C_BndEvt__ComboBoxString_0_K2Node_ComponentBoundEvent_47_OnSelectionChangedEvent__DelegateSignature_Params params;
	params.SelectedItem = SelectedItem;
	params.SelectionType = SelectionType;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.ExecuteUbergraph_WBP_SettingsPage_Video
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SettingsPage_Video_C::ExecuteUbergraph_WBP_SettingsPage_Video(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SettingsPage_Video.WBP_SettingsPage_Video_C.ExecuteUbergraph_WBP_SettingsPage_Video");

	UWBP_SettingsPage_Video_C_ExecuteUbergraph_WBP_SettingsPage_Video_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
