// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_CreditsReward.WBP_CreditsReward_C.GetText_1
// (Public, HasOutParms, HasDefaults, BlueprintCallable, BlueprintEvent, BlueprintPure)
// Parameters:
// struct FText                   ReturnValue                    (Parm, OutParm, ReturnParm)

struct FText UWBP_CreditsReward_C::GetText_1()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CreditsReward.WBP_CreditsReward_C.GetText_1");

	UWBP_CreditsReward_C_GetText_1_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;

	return params.ReturnValue;
}


// Function WBP_CreditsReward.WBP_CreditsReward_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_CreditsReward_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CreditsReward.WBP_CreditsReward_C.Construct");

	UWBP_CreditsReward_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_CreditsReward.WBP_CreditsReward_C.ExecuteUbergraph_WBP_CreditsReward
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_CreditsReward_C::ExecuteUbergraph_WBP_CreditsReward(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_CreditsReward.WBP_CreditsReward_C.ExecuteUbergraph_WBP_CreditsReward");

	UWBP_CreditsReward_C_ExecuteUbergraph_WBP_CreditsReward_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
