// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.RotateWidget
// (Public, BlueprintCallable, BlueprintEvent)
// Parameters:
// class UWidget*                 Widget                         (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
// float                          DeltaTime                      (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
// float                          RotationRate                   (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BattleRoyaleMap_C::RotateWidget(class UWidget* Widget, float DeltaTime, float RotationRate)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.RotateWidget");

	UWBP_BattleRoyaleMap_C_RotateWidget_Params params;
	params.Widget = Widget;
	params.DeltaTime = DeltaTime;
	params.RotationRate = RotationRate;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Tick
// (BlueprintCosmetic, Event, Public, BlueprintEvent)
// Parameters:
// struct FGeometry*              MyGeometry                     (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
// float*                         InDeltaTime                    (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BattleRoyaleMap_C::Tick(struct FGeometry* MyGeometry, float* InDeltaTime)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Tick");

	UWBP_BattleRoyaleMap_C_Tick_Params params;
	params.MyGeometry = MyGeometry;
	params.InDeltaTime = InDeltaTime;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_BattleRoyaleMap_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.Construct");

	UWBP_BattleRoyaleMap_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.ExecuteUbergraph_WBP_BattleRoyaleMap
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_BattleRoyaleMap_C::ExecuteUbergraph_WBP_BattleRoyaleMap(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_BattleRoyaleMap.WBP_BattleRoyaleMap_C.ExecuteUbergraph_WBP_BattleRoyaleMap");

	UWBP_BattleRoyaleMap_C_ExecuteUbergraph_WBP_BattleRoyaleMap_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
