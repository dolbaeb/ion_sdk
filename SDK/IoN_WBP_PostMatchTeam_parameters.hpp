#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature
struct UWBP_PostMatchTeam_C_BndEvt__ExpandButton_K2Node_ComponentBoundEvent_25_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Set Minimized
struct UWBP_PostMatchTeam_C_Set_Minimized_Params
{
	bool                                               Minimize;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Construct
struct UWBP_PostMatchTeam_C_Construct_Params
{
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Fill With Players
struct UWBP_PostMatchTeam_C_Fill_With_Players_Params
{
	TArray<struct FPlayerMatchResult>                  Players;                                                  // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, OutParm, ZeroConstructor, ReferenceParm)
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Calculate Team Totals
struct UWBP_PostMatchTeam_C_Calculate_Team_Totals_Params
{
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.Update Team Values
struct UWBP_PostMatchTeam_C_Update_Team_Values_Params
{
};

// Function WBP_PostMatchTeam.WBP_PostMatchTeam_C.ExecuteUbergraph_WBP_PostMatchTeam
struct UWBP_PostMatchTeam_C_ExecuteUbergraph_WBP_PostMatchTeam_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
