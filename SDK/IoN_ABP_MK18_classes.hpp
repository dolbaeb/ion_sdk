#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// AnimBlueprintGeneratedClass ABP_MK18.ABP_MK18_C
// 0x03A9 (0x0779 - 0x03D0)
class UABP_MK18_C : public UAnimInstance
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x03D0(0x0008) (Transient, DuplicateTransient)
	struct FAnimNode_Root                              AnimGraphNode_Root_5A9B5ADA4F7D6AD36500B983B0D74045;      // 0x03D8(0x0048)
	struct FAnimNode_ConvertComponentToLocalSpace      AnimGraphNode_ComponentToLocalSpace_2BA3D7DE4E6CDDCFD01C778AD988647D;// 0x0420(0x0048)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C;// 0x0468(0x00B8)
	struct FAnimNode_Slot                              AnimGraphNode_Slot_2363B7884F48A59BF91DD581E20A41E6;      // 0x0520(0x0068)
	struct FAnimNode_ConvertLocalToComponentSpace      AnimGraphNode_LocalToComponentSpace_2796F9B840037BE1231DEB81BCE50003;// 0x0588(0x0048)
	struct FAnimNode_RefPose                           AnimGraphNode_LocalRefPose_40324DB9429D882D94E648BDB07CFFD1;// 0x05D0(0x0038)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19;// 0x0608(0x00B8)
	struct FAnimNode_ModifyBone                        AnimGraphNode_ModifyBone_563CD0CE4CA2DD037E99258192112E1D;// 0x06C0(0x00B8)
	bool                                               bHasSight;                                                // 0x0778(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static auto ptr = UObject::FindClass("AnimBlueprintGeneratedClass ABP_MK18.ABP_MK18_C");
		return ptr;
	}


	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_AC70C2A54A4493A09099D2B7E5292A19();
	void EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_MK18_AnimGraphNode_ModifyBone_353D4A974A8E436E779728B7C5E6208C();
	void BlueprintUpdateAnimation(float* DeltaTimeX);
	void ExecuteUbergraph_ABP_MK18(int EntryPoint);
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
