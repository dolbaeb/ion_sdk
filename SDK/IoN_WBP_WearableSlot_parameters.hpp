#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_WearableSlot.WBP_WearableSlot_C.GetColorAndOpacity_1
struct UWBP_WearableSlot_C_GetColorAndOpacity_1_Params
{
	struct FSlateColor                                 ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.GetText_1
struct UWBP_WearableSlot_C_GetText_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.GetVisibility_1
struct UWBP_WearableSlot_C_GetVisibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.Get_BG_Visibility_1
struct UWBP_WearableSlot_C_Get_BG_Visibility_1_Params
{
	ESlateVisibility                                   ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.OnDrop
struct UWBP_WearableSlot_C_OnDrop_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	struct FPointerEvent*                              PointerEvent;                                             // (BlueprintVisible, BlueprintReadOnly, Parm)
	class UDragDropOperation**                         Operation;                                                // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.GetCurrentItem
struct UWBP_WearableSlot_C_GetCurrentItem_Params
{
	class AIONItem*                                    ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.AcceptsDropForItem
struct UWBP_WearableSlot_C_AcceptsDropForItem_Params
{
	class AIONItem**                                   Item;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	bool                                               ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.PreConstruct
struct UWBP_WearableSlot_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.Construct
struct UWBP_WearableSlot_C_Construct_Params
{
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.Tick
struct UWBP_WearableSlot_C_Tick_Params
{
	struct FGeometry*                                  MyGeometry;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, IsPlainOldData)
	float*                                             InDeltaTime;                                              // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.OnItemChanged
struct UWBP_WearableSlot_C_OnItemChanged_Params
{
};

// Function WBP_WearableSlot.WBP_WearableSlot_C.ExecuteUbergraph_WBP_WearableSlot
struct UWBP_WearableSlot_C_ExecuteUbergraph_WBP_WearableSlot_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
