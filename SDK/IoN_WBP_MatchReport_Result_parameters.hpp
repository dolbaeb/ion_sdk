#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_TotalKills_Text_1
struct UWBP_MatchReport_Result_C_Get_TextBlock_TotalKills_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_SquadType_Text_1
struct UWBP_MatchReport_Result_C_Get_TextBlock_SquadType_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_1_Text_1
struct UWBP_MatchReport_Result_C_Get_TextBlock_1_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_TextBlock_Participants_Text_1
struct UWBP_MatchReport_Result_C_Get_TextBlock_Participants_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Get_Text_Placement_Text_1
struct UWBP_MatchReport_Result_C_Get_Text_Placement_Text_1_Params
{
	struct FText                                       ReturnValue;                                              // (Parm, OutParm, ReturnParm)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Construct
struct UWBP_MatchReport_Result_C_Construct_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnAnimationFinished
struct UWBP_MatchReport_Result_C_OnAnimationFinished_Params
{
	class UWidgetAnimation**                           Animation;                                                // (ConstParm, BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, InstancedReference, IsPlainOldData)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnReportClicked
struct UWBP_MatchReport_Result_C_OnReportClicked_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.OnMinimize
struct UWBP_MatchReport_Result_C_OnMinimize_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Appear
struct UWBP_MatchReport_Result_C_Appear_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Disappear
struct UWBP_MatchReport_Result_C_Disappear_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.Begin Animation
struct UWBP_MatchReport_Result_C_Begin_Animation_Params
{
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.ExecuteUbergraph_WBP_MatchReport_Result
struct UWBP_MatchReport_Result_C_ExecuteUbergraph_WBP_MatchReport_Result_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_MatchReport_Result.WBP_MatchReport_Result_C.AnimationFinished__DelegateSignature
struct UWBP_MatchReport_Result_C_AnimationFinished__DelegateSignature_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
