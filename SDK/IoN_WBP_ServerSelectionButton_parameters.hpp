#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.PreConstruct
struct UWBP_ServerSelectionButton_C_PreConstruct_Params
{
	bool*                                              IsDesignTime;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.ExecuteUbergraph_WBP_ServerSelectionButton
struct UWBP_ServerSelectionButton_C_ExecuteUbergraph_WBP_ServerSelectionButton_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

// Function WBP_ServerSelectionButton.WBP_ServerSelectionButton_C.OnClicked__DelegateSignature
struct UWBP_ServerSelectionButton_C_OnClicked__DelegateSignature_Params
{
	EIONPartyTypes                                     Mode;                                                     // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
