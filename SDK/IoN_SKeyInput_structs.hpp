#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SKeyInput.SKeyInput
// 0x0019
struct FSKeyInput
{
	struct FKey                                        Input_3_484CF56342A117E3CBA042A394CBD033;                 // 0x0000(0x0018) (Edit, BlueprintVisible)
	bool                                               UseNegativeAxis_9_CA016842430D7150D94F0AABC0B31CAE;       // 0x0018(0x0001) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
