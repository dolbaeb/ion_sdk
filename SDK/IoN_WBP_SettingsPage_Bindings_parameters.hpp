#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.RefreshInputMappings
struct UWBP_SettingsPage_Bindings_C_RefreshInputMappings_Params
{
};

// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.Construct
struct UWBP_SettingsPage_Bindings_C_Construct_Params
{
};

// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature
struct UWBP_SettingsPage_Bindings_C_BndEvt__Button_2_K2Node_ComponentBoundEvent_30_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_SettingsPage_Bindings.WBP_SettingsPage_Bindings_C.ExecuteUbergraph_WBP_SettingsPage_Bindings
struct UWBP_SettingsPage_Bindings_C_ExecuteUbergraph_WBP_SettingsPage_Bindings_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
