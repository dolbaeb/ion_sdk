// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_SpectatorButton.WBP_SpectatorButton_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_SpectatorButton_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorButton.WBP_SpectatorButton_C.Construct");

	UWBP_SpectatorButton_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorButton.WBP_SpectatorButton_C.BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature
// (BlueprintEvent)

void UWBP_SpectatorButton_C::BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorButton.WBP_SpectatorButton_C.BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature");

	UWBP_SpectatorButton_C_BndEvt__Button_36_K2Node_ComponentBoundEvent_20_OnButtonClickedEvent__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_SpectatorButton.WBP_SpectatorButton_C.ExecuteUbergraph_WBP_SpectatorButton
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_SpectatorButton_C::ExecuteUbergraph_WBP_SpectatorButton(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_SpectatorButton.WBP_SpectatorButton_C.ExecuteUbergraph_WBP_SpectatorButton");

	UWBP_SpectatorButton_C_ExecuteUbergraph_WBP_SpectatorButton_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
