#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace Classes
{
//---------------------------------------------------------------------------
//Script Structs
//---------------------------------------------------------------------------

// UserDefinedStruct SZoomData.SZoomData
// 0x0028
struct FSZoomData
{
	float                                              SpringArmTargetLength_2_C3E9637F407A474AC8FE9E83888CC1B0; // 0x0000(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SpringArmTargetLength_Min_13_43F101A94DF67B41B86FFBBE9FE0E8BD;// 0x0004(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	float                                              SpringArmTargetLength_Max_11_571E3B3540F960653E80619DD88531FD;// 0x0008(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	struct FVector                                     SpringArmTargetOffset_5_A39D85EC44826BDB338B9DA27243479F; // 0x000C(0x000C) (Edit, BlueprintVisible, IsPlainOldData)
	float                                              FocusDistance_8_AE4571744AA5F88D59B0F5B8AE206586;         // 0x0018(0x0004) (Edit, BlueprintVisible, ZeroConstructor, IsPlainOldData)
	struct FRotator                                    Rotation_16_628022CE4AFA8998E574A684F90F1695;             // 0x001C(0x000C) (Edit, BlueprintVisible, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
