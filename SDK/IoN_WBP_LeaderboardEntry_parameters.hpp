#pragma once

// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnGetMenuContent_1
struct UWBP_LeaderboardEntry_C_OnGetMenuContent_1_Params
{
	class UWidget*                                     ReturnValue;                                              // (Parm, OutParm, ZeroConstructor, ReturnParm, InstancedReference, IsPlainOldData)
};

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.InitLeaderboardEntry
struct UWBP_LeaderboardEntry_C_InitLeaderboardEntry_Params
{
	struct FString                                     PlayerName;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	struct FString                                     GameSparksID;                                             // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor)
	int                                                Position;                                                 // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
	struct FText                                       Score;                                                    // (BlueprintVisible, BlueprintReadOnly, Parm)
};

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature
struct UWBP_LeaderboardEntry_C_BndEvt__PlayerButton_K2Node_ComponentBoundEvent_5_OnButtonClickedEvent__DelegateSignature_Params
{
};

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.OnPopupClicked
struct UWBP_LeaderboardEntry_C_OnPopupClicked_Params
{
};

// Function WBP_LeaderboardEntry.WBP_LeaderboardEntry_C.ExecuteUbergraph_WBP_LeaderboardEntry
struct UWBP_LeaderboardEntry_C_ExecuteUbergraph_WBP_LeaderboardEntry_Params
{
	int                                                EntryPoint;                                               // (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
