// Islands Of Nyne: Battle Royal (4.19.0) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

#include "../SDK.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function WBP_Loadout.WBP_Loadout_C.UpdateWeaponSkins
// (Public, HasDefaults, BlueprintCallable, BlueprintEvent)

void UWBP_Loadout_C::UpdateWeaponSkins()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.UpdateWeaponSkins");

	UWBP_Loadout_C_UpdateWeaponSkins_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.RefreshLoadout
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Loadout_C::RefreshLoadout()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.RefreshLoadout");

	UWBP_Loadout_C_RefreshLoadout_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.UpdateArmorIcons
// (Public, BlueprintCallable, BlueprintEvent)

void UWBP_Loadout_C::UpdateArmorIcons()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.UpdateArmorIcons");

	UWBP_Loadout_C_UpdateArmorIcons_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UWBP_Loadout_C::Construct()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.Construct");

	UWBP_Loadout_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Loadout_C::BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_Loadout_C_BndEvt__HelmetCheckbox_K2Node_ComponentBoundEvent_0_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature
// (BlueprintEvent)
// Parameters:
// bool                           bIsChecked                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Loadout_C::BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature(bool bIsChecked)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature");

	UWBP_Loadout_C_BndEvt__ArmorCheckbox_K2Node_ComponentBoundEvent_1_OnCheckBoxComponentStateChanged__DelegateSignature_Params params;
	params.bIsChecked = bIsChecked;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.ExecuteUbergraph_WBP_Loadout
// ()
// Parameters:
// int                            EntryPoint                     (BlueprintVisible, BlueprintReadOnly, Parm, ZeroConstructor, IsPlainOldData)

void UWBP_Loadout_C::ExecuteUbergraph_WBP_Loadout(int EntryPoint)
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.ExecuteUbergraph_WBP_Loadout");

	UWBP_Loadout_C_ExecuteUbergraph_WBP_Loadout_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function WBP_Loadout.WBP_Loadout_C.ClearActionsMenu__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UWBP_Loadout_C::ClearActionsMenu__DelegateSignature()
{
	static auto fn = UObject::FindObject<UFunction>("Function WBP_Loadout.WBP_Loadout_C.ClearActionsMenu__DelegateSignature");

	UWBP_Loadout_C_ClearActionsMenu__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
